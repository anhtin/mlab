import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import no.furst.mLab.decisions.tasks.autocommentation.runtime.CodedInterpretativeComment;
import no.uio.mLab.decisions.core.runtime.LisResponse;
import no.uio.mLab.decisions.core.runtime.LisValue;
import no.uio.mLab.decisions.data.laboratoryTest.numberResult.runtime.LaboratoryTestWithNumberResultDataValueVisitor;
import no.uio.mLab.decisions.data.laboratoryTest.numberResult.runtime.TestPreviousResultAsNumber;
import no.uio.mLab.decisions.data.laboratoryTest.numberResult.runtime.TestResultAsNumber;
import no.uio.mLab.decisions.data.laboratoryTest.numberResult.runtime.TestResultHasNumberValue;
import no.uio.mLab.decisions.data.laboratoryTest.runtime.LaboratoryTestDataValueVisitor;
import no.uio.mLab.decisions.data.laboratoryTest.runtime.TestHasBeenPerformed;
import no.uio.mLab.decisions.data.laboratoryTest.runtime.TestHasPreviouslyBeenPerformed;
import no.uio.mLab.decisions.data.laboratoryTest.runtime.TestHasRequest;
import no.uio.mLab.decisions.data.laboratoryTest.runtime.TestPreviousResultIsValid;
import no.uio.mLab.decisions.data.laboratoryTest.runtime.TestResultIsValid;
import no.uio.mLab.decisions.data.laboratoryTest.runtime.TestTimeSinceLastRequested;
import no.uio.mLab.decisions.data.laboratoryTest.textualResult.runtime.LaboratoryTestWithTextResultDataValueVisitor;
import no.uio.mLab.decisions.data.laboratoryTest.textualResult.runtime.TestPreviousResultAsText;
import no.uio.mLab.decisions.data.laboratoryTest.textualResult.runtime.TestResultAsText;
import no.uio.mLab.decisions.data.laboratoryTest.textualResult.runtime.TestResultHasTextValue;
import no.uio.mLab.decisions.data.patient.runtime.FemaleGender;
import no.uio.mLab.decisions.data.patient.runtime.GenderLiteral;
import no.uio.mLab.decisions.data.patient.runtime.MaleGender;
import no.uio.mLab.decisions.data.patient.runtime.PatientAge;
import no.uio.mLab.decisions.data.patient.runtime.PatientDataValueVisitor;
import no.uio.mLab.decisions.data.patient.runtime.PatientGender;
import no.uio.mLab.decisions.tasks.autocommentation.runtime.AutoCommentationTaskDataValueVisitor;
import no.uio.mLab.decisions.tasks.autocommentation.runtime.HasInterpretativeComment;
import no.uio.mLab.decisions.tasks.autocommentation.runtime.InterpretativeComment;
import org.apache.http.HttpStatus;

import java.time.Duration;

public class FurstDataValueVisitor implements
        LaboratoryTestDataValueVisitor,
        LaboratoryTestWithNumberResultDataValueVisitor,
        LaboratoryTestWithTextResultDataValueVisitor,
        PatientDataValueVisitor,
        AutoCommentationTaskDataValueVisitor
{
    private static String BASE_URL = "http://*REDACTED*/request/";

    private String REQUEST_URL;

    public FurstDataValueVisitor(String requestId) {
        this.REQUEST_URL = BASE_URL + requestId;
    }

    @Override
    public LisResponse<Boolean> visit(TestResultHasNumberValue dataValue) {
        String test = dataValue.getLaboratoryTestName();
        Object result = fetchTestResult(test);
        return new LisValue<>(result instanceof Double);
    }

    @Override
    public LisResponse<Double> visit(TestPreviousResultAsNumber dataValue) {
        String test = dataValue.getLaboratoryTestName();
        Object result = fetchPreviousTestResult(test);
        if (result instanceof Double) {
            Double r = (Double) result;
            return new LisValue<>(r);
        }
        return null;
    }

    @Override
    public LisResponse<Double> visit(TestResultAsNumber dataValue) {
        String test = dataValue.getLaboratoryTestName();
        Object result = fetchTestResult(test);
        if (result instanceof Double) {
            return new LisValue<>((Double) result);
        }
        return null;
    }

    @Override
    public LisResponse<Boolean> visit(TestHasBeenPerformed dataValue) {
        String test = dataValue.getLaboratoryTestName();
        Object result = fetchTestResult(test);
        return new LisValue<>(result != null);
    }

    @Override
    public LisResponse<Boolean> visit(TestHasPreviouslyBeenPerformed dataValue) {
        String test = dataValue.getLaboratoryTestName();
        Object result = fetchPreviousTestResult(test);
        return new LisValue<>(result != null);
    }

    @Override
    public LisResponse<Boolean> visit(TestHasRequest testHasRequest) {
        return new LisValue<>(true);
    }

    @Override
    public LisResponse<Boolean> visit(TestPreviousResultIsValid testPreviousResultIsValid) {
        return null;
    }

    @Override
    public LisResponse<Boolean> visit(TestResultIsValid testResultIsValid) {
        return new LisValue<>(true);
    }

    @Override
    public LisResponse<Duration> visit(TestTimeSinceLastRequested testTimeSinceLastRequested) {
        return new LisValue<>(Duration.ofDays(60));
    }

    @Override
    public LisResponse<Boolean> visit(TestResultHasTextValue dataValue) {
        String test = dataValue.getTest();
        Object result = fetchTestResult(test);
        return new LisValue<>(result instanceof String);
    }

    @Override
    public LisResponse<String> visit(TestPreviousResultAsText dataValue) {
        String test = dataValue.getLaboratoryTestName();
        Object result = fetchPreviousTestResult(test);
        if (result instanceof String) {
            String r = (String) result;
            return new LisValue<>(r);
        }
        return null;
    }

    @Override
    public LisResponse<String> visit(TestResultAsText dataValue) {
        String test = dataValue.getLaboratoryTestName();
        Object result = fetchTestResult(test);
        if (result instanceof String) {
            return new LisValue<>((String) result);
        }
        return null;
    }

    @Override
    public LisResponse<Duration> visit(PatientAge dataValue) {
        Double result = request("patient/age", Double.class);
        if (result == null) return null;

        Duration age = Duration.ofDays((long) (365 * result));
        return new LisValue<>(age);
    }

    @Override
    public LisResponse<GenderLiteral> visit(PatientGender patientGender) {
        String result = request("patient/gender", String.class);
        if (result == null) return null;

        switch (result) {
            case "male":
                return new LisValue<>(MaleGender.getInstance());
            case "female":
                return new LisValue<>(FemaleGender.getInstance());
        }
        return null;
    }

    @Override
    public LisResponse<Boolean> visit(HasInterpretativeComment dataValue) {
        Boolean result = false;
        InterpretativeComment comment = dataValue.getComment();
        if (comment instanceof CodedInterpretativeComment) {
            result = request(String.format("comment/%s/exists", ((CodedInterpretativeComment)comment).getCode()), Boolean.class);
        }
        return new LisValue<>(result);
    }


    private <T> T request(String url, Class<T> c) {
        try {
            HttpResponse<T> response = Unirest.get(String.format("%s/%s", REQUEST_URL, url)).asObject(c);
            if (response.getStatus() == HttpStatus.SC_OK) {
                return response.getBody();
            }
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Object fetchPreviousTestResult(String test) {
        FurstTestResult response = request(String.format("test/%s/previous/result", test), FurstTestResult.class);
        return response == null ? null : response.result;
    }

    private Object fetchTestResult(String test) {
        FurstTestResult response = request(String.format("test/%s/result", test), FurstTestResult.class);
        return response == null ? null : response.result;
    }

}
