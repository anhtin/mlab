import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.ObjectMapper;
import com.mashape.unirest.http.Unirest;
import no.furst.mLab.regelsystem.rules.ExampleTask1;
import no.uio.mLab.decisions.core.runtime.Action;
import no.uio.mLab.decisions.core.runtime.ActionJustification;
import no.uio.mLab.decisions.core.runtime.DataValue;
import no.uio.mLab.decisions.core.runtime.Fact;
import no.uio.mLab.decisions.core.runtime.Knowledge;
import no.uio.mLab.decisions.core.runtime.Rule;
import no.uio.mLab.decisions.core.runtime.RuleSet;
import no.uio.mLab.decisions.core.runtime.RuleSetResult;

import java.io.IOException;
import java.util.Collection;
import java.util.Comparator;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        initializeJsonMapper();

        RuleSet test = new ExampleTask1();
        RuleSetResult result = test.execute(dataValues ->
                dataValues.stream().map(it ->
                        new Knowledge(it, it.accept(new FurstDataValueVisitor("RQIS")))
                ).collect(Collectors.toSet())
        );


        // Get string for mapping data values and actions to mLab expressions
        Map<DataValue, String> dataValueExprMap = test.getDataValueExpressions();
        Map<Action, String> actionExprMap = test.getActionExpressions();

        System.out.println(String.format("Justifications for evaluation of %s", test.getName()));

        // Output evaluation context
        System.out.println("\nFacts:");
        printFactsInAlphaNumericOrder(result.getContext(), dataValueExprMap, "  ");

        // Output unknown values
        if (result.getUnknownValues().size() > 0) {
            System.out.println("\nUnknown values:");
            result.getUnknownValues().forEach(it -> System.out.println("  - " + dataValueExprMap.get(it)));
        }

        // Check and output pre-condition justifications
        if (!assertPrintPreCondition(result, dataValueExprMap)) return;

        // Output action justifications
        System.out.println("\nActions:");
        printActionJustificationsInAlphaNumericOrder(result.getJustificationsForActions(), actionExprMap, dataValueExprMap, "  ");

        System.out.println("\nActive rules:");
        printRuleJustificationsInAlphaNumericOrder(result.getJustificationsForActiveRules(), dataValueExprMap, "  ");

        System.out.println("\nInactive rules:");
        printRuleJustificationsInAlphaNumericOrder(result.getJustificationsForInactiveRule(), dataValueExprMap, "  ");
    }

    private static boolean assertPrintPreCondition(RuleSetResult result, Map<DataValue, String> exprMap) {
        if (result.isPreconditionSatisfied()) {
            System.out.println("\nPre-condition is satisfied:");
        } else {
            System.out.println("\nPre-condition is not satisfied:");
        }
        printFactsInAlphaNumericOrder(result.getPreConditionCauses(), exprMap, "  ");
        return result.isPreconditionSatisfied();
    }

    private static void printFactsInAlphaNumericOrder(
            Collection<Fact> facts,
            Map<DataValue, String> exprMap,
            String indent)
    {
        Comparator<Fact> factComparator = Comparator.comparing((Fact a) -> exprMap.get(a.getDataValue()));
        facts.stream().sorted(factComparator).forEach(fact -> {
            String dataValue = exprMap.get(fact.getDataValue());
            Object concreteValue = fact.getConcreteValue();
            System.out.println(String.format("%s- %s = %s", indent, dataValue, concreteValue));
        });
    }

    private static void printActionJustificationsInAlphaNumericOrder(
            Collection<ActionJustification> justifications,
            Map<Action, String> actionExprMap,
            Map<DataValue, String> dataValueExprMap,
            String indent)
    {
        justifications.forEach(justification -> {
            System.out.println(String.format("%s- %s:", indent, actionExprMap.get(justification.getAction())));

            System.out.println(indent + "    Caused by rules:");
            justification.getRuleCauses().forEach(it -> System.out.println(indent + "      - " + it.getName()));

            System.out.println(indent + "    Caused by facts:");
            printFactsInAlphaNumericOrder(justification.getFactCauses(), dataValueExprMap, indent + "      ");
        });
    }

    private static void printRuleJustificationsInAlphaNumericOrder(
            Map<Rule, Set<Fact>> ruleJustifications,
            Map<DataValue, String> exprMap,
            String indent)
    {
        Comparator<Map.Entry<Rule, Set<Fact>>> ruleComparator =
                Comparator.comparing((Map.Entry<Rule, Set<Fact>> a) -> a.getKey().getName());
        ruleJustifications.entrySet().stream().sorted(ruleComparator).forEach(entry -> {
            Rule rule = entry.getKey();
            Set<Fact> facts = entry.getValue();
            System.out.println(String.format("%s- %s", indent, rule.getName()));
            printFactsInAlphaNumericOrder(facts, exprMap, indent + "    ");
        });
    }

    // Map HTTP respone to JSON
    private static void initializeJsonMapper() {
        Unirest.setObjectMapper(new ObjectMapper() {
            private com.fasterxml.jackson.databind.ObjectMapper jacksonObjectMapper
                    = new com.fasterxml.jackson.databind.ObjectMapper();

            public <T> T readValue(String value, Class<T> valueType) {
                try {
                    return jacksonObjectMapper.readValue(value, valueType);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }

            public String writeValue(Object value) {
                try {
                    return jacksonObjectMapper.writeValueAsString(value);
                } catch (JsonProcessingException e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }

}
