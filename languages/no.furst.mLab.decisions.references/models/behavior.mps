<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:af787781-0e66-492b-856d-4fc5ddf1decf(no.furst.mLab.decisions.references.behavior)">
  <persistence version="9" />
  <languages>
    <use id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior" version="1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="1yj" ref="r:0c887d67-81ab-420f-9a35-a9f6bf96c975(no.uio.mLab.shared.behavior)" />
    <import index="wb6c" ref="r:19d15037-83c1-4ae2-b66a-763828cb8279(no.uio.mLab.decisions.core.behavior)" />
    <import index="7f9y" ref="r:790ac37e-42d3-4dd3-977c-e8fe9cc56a45(no.uio.mLab.decisions.core.structure)" />
    <import index="nlc7" ref="r:9d3523f4-3478-4519-bfbb-caf8e981a9d2(no.furst.mLab.decisions.references.structure)" />
    <import index="kkto" ref="r:01f3534b-874e-42dc-a9b5-7540591f45bb(no.uio.mLab.entities.core.structure)" />
    <import index="9qnw" ref="r:2f6d4fa8-e4da-4f59-bd6f-30425e1dd68f(no.furst.mLab.entities.structure)" implicit="true" />
    <import index="4v7t" ref="r:3abf6acc-a4c5-4ee7-8b22-79c4941d18e6(no.uio.mLab.entities.biochemistry.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior">
      <concept id="1225194240794" name="jetbrains.mps.lang.behavior.structure.ConceptBehavior" flags="ng" index="13h7C7">
        <reference id="1225194240799" name="concept" index="13h7C2" />
        <child id="1225194240805" name="method" index="13h7CS" />
        <child id="1225194240801" name="constructor" index="13h7CW" />
      </concept>
      <concept id="1225194413805" name="jetbrains.mps.lang.behavior.structure.ConceptConstructorDeclaration" flags="in" index="13hLZK" />
      <concept id="1225194472830" name="jetbrains.mps.lang.behavior.structure.ConceptMethodDeclaration" flags="ng" index="13i0hz">
        <property id="5864038008284099149" name="isStatic" index="2Ki8OM" />
        <property id="1225194472832" name="isVirtual" index="13i0it" />
        <property id="1225194472834" name="isAbstract" index="13i0iv" />
        <reference id="1225194472831" name="overriddenMethod" index="13i0hy" />
      </concept>
      <concept id="1225194691553" name="jetbrains.mps.lang.behavior.structure.ThisNodeExpression" flags="nn" index="13iPFW" />
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1070534644030" name="jetbrains.mps.baseLanguage.structure.BooleanType" flags="in" index="10P_77" />
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123137" name="jetbrains.mps.baseLanguage.structure.BooleanConstant" flags="nn" index="3clFbT">
        <property id="1068580123138" name="value" index="3clFbU" />
      </concept>
      <concept id="1068581517677" name="jetbrains.mps.baseLanguage.structure.VoidType" flags="in" index="3cqZAl" />
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
    </language>
    <language id="3a13115c-633c-4c5c-bbcc-75c4219e9555" name="jetbrains.mps.lang.quotation">
      <concept id="5455284157994012186" name="jetbrains.mps.lang.quotation.structure.NodeBuilderInitLink" flags="ng" index="2pIpSj">
        <reference id="5455284157994012188" name="link" index="2pIpSl" />
      </concept>
      <concept id="5455284157993911097" name="jetbrains.mps.lang.quotation.structure.NodeBuilderInitPart" flags="ng" index="2pJxcK">
        <child id="5455284157993911094" name="expression" index="2pJxcZ" />
      </concept>
      <concept id="5455284157993863837" name="jetbrains.mps.lang.quotation.structure.NodeBuilder" flags="nn" index="2pJPEk">
        <child id="5455284157993863838" name="quotedNode" index="2pJPEn" />
      </concept>
      <concept id="5455284157993863840" name="jetbrains.mps.lang.quotation.structure.NodeBuilderNode" flags="nn" index="2pJPED">
        <reference id="5455284157993910961" name="concept" index="2pJxaS" />
        <child id="5455284157993911099" name="values" index="2pJxcM" />
      </concept>
      <concept id="8182547171709752110" name="jetbrains.mps.lang.quotation.structure.NodeBuilderExpression" flags="nn" index="36biLy">
        <child id="8182547171709752112" name="expression" index="36biLW" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="2396822768958367367" name="jetbrains.mps.lang.smodel.structure.AbstractTypeCastExpression" flags="nn" index="$5XWr">
        <child id="6733348108486823193" name="leftExpression" index="1m5AlR" />
        <child id="3906496115198199033" name="conceptArgument" index="3oSUPX" />
      </concept>
      <concept id="2644386474300074836" name="jetbrains.mps.lang.smodel.structure.ConceptIdRefExpression" flags="nn" index="35c_gC">
        <reference id="2644386474300074837" name="conceptDeclaration" index="35c_gD" />
      </concept>
      <concept id="6677504323281689838" name="jetbrains.mps.lang.smodel.structure.SConceptType" flags="in" index="3bZ5Sz">
        <reference id="6677504323281689839" name="conceptDeclaraton" index="3bZ5Sy" />
      </concept>
      <concept id="1140131837776" name="jetbrains.mps.lang.smodel.structure.Node_ReplaceWithAnotherOperation" flags="nn" index="1P9Npp">
        <child id="1140131861877" name="replacementNode" index="1P9ThW" />
      </concept>
      <concept id="1140137987495" name="jetbrains.mps.lang.smodel.structure.SNodeTypeCastExpression" flags="nn" index="1PxgMI" />
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="13h7C7" id="4B5aqq217tV">
    <property role="3GE5qa" value="aspects.variables" />
    <ref role="13h7C2" to="nlc7:6khVix$FDOi" resolve="CodedInterpretativeCommentForRequestorVariable" />
    <node concept="13hLZK" id="4B5aqq217tW" role="13h7CW">
      <node concept="3clFbS" id="4B5aqq217tX" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6LTgXmNWQUc" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="6LTgXmNWQUd" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNWQUi" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNWQUn" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNWR2w" role="3clFbG">
            <property role="Xl_RC" value="coded interpretative comment" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmNWQUj" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmNWQUo" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="6LTgXmNWQUp" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNWQUu" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNWQUz" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNWR3b" role="3clFbG">
            <property role="Xl_RC" value="" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmNWQUv" role="3clF45" />
    </node>
    <node concept="13i0hz" id="4B5aqq217ur" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getReferenceDescription" />
      <property role="2Ki8OM" value="false" />
      <ref role="13i0hy" to="1yj:4V3GMfXuP$X" resolve="getReferenceDescription" />
      <node concept="3Tm1VV" id="4B5aqq217us" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq217uB" role="3clF47">
        <node concept="3clFbF" id="4B5aqq217_1" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq217_0" role="3clFbG">
            <property role="Xl_RC" value="coded interpretative comment" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="4B5aqq217uC" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="6LTgXmNWLFG">
    <property role="3GE5qa" value="aspects.patterns.codedInterpretativeComment" />
    <ref role="13h7C2" to="nlc7:6LTgXmNWLEO" resolve="WildcardCodedInterpretativeForRequestorComment" />
    <node concept="13hLZK" id="6LTgXmNWLFH" role="13h7CW">
      <node concept="3clFbS" id="6LTgXmNWLFI" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6LTgXmNWLFR" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="6LTgXmNWLFS" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNWLFX" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNWLRE" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNWLRD" role="3clFbG">
            <property role="Xl_RC" value="*" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmNWLFY" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmNWLG3" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="6LTgXmNWLG4" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNWLG9" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNWLGe" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNWLSk" role="3clFbG">
            <property role="Xl_RC" value="any coded interpretative comment" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmNWLGa" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmNWLGf" role="13h7CS">
      <property role="TrG5h" value="hasPattern" />
      <property role="13i0it" value="false" />
      <property role="13i0iv" value="false" />
      <ref role="13i0hy" to="wb6c:6LTgXmNRv0n" resolve="hasPattern" />
      <node concept="3Tm1VV" id="6LTgXmNWLGg" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNWLGl" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNWLGq" role="3cqZAp">
          <node concept="3clFbT" id="6LTgXmNWLTA" role="3clFbG">
            <property role="3clFbU" value="true" />
          </node>
        </node>
      </node>
      <node concept="10P_77" id="6LTgXmNWLGm" role="3clF45" />
    </node>
    <node concept="13i0hz" id="2FjKBCOoNMr" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getMatchEntityConcept" />
      <ref role="13i0hy" to="wb6c:65epL7MnLJr" resolve="getMatchEntityConcept" />
      <node concept="3Tm1VV" id="2FjKBCOoNMs" role="1B3o_S" />
      <node concept="3clFbS" id="2FjKBCOoNMx" role="3clF47">
        <node concept="3clFbF" id="2FjKBCOoNW$" role="3cqZAp">
          <node concept="35c_gC" id="2FjKBCOoNWv" role="3clFbG">
            <ref role="35c_gD" to="9qnw:1mAGFBLG5RX" resolve="Autokommentar" />
          </node>
        </node>
      </node>
      <node concept="3bZ5Sz" id="2FjKBCOoNMy" role="3clF45">
        <ref role="3bZ5Sy" to="kkto:4QUW3edDqMR" resolve="Entity" />
      </node>
    </node>
  </node>
  <node concept="13h7C7" id="1I84Bf8kf0Q">
    <property role="3GE5qa" value="aspects.references" />
    <ref role="13h7C2" to="nlc7:4V3GMfXsBti" resolve="AspectBiochemistryTestWithTextResultReference" />
    <node concept="13hLZK" id="1I84Bf8kf0R" role="13h7CW">
      <node concept="3clFbS" id="1I84Bf8kf0S" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="1I84Bf8kf11" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="replaceWith" />
      <ref role="13i0hy" to="wb6c:1I84Bf8ilTw" resolve="replaceWith" />
      <node concept="3Tm1VV" id="1I84Bf8kf12" role="1B3o_S" />
      <node concept="3clFbS" id="1I84Bf8kf1c" role="3clF47">
        <node concept="3clFbF" id="1I84Bf8kf6M" role="3cqZAp">
          <node concept="2OqwBi" id="1I84Bf8kfkI" role="3clFbG">
            <node concept="13iPFW" id="1I84Bf8kf6L" role="2Oq$k0" />
            <node concept="1P9Npp" id="1I84Bf8ky7w" role="2OqNvi">
              <node concept="2pJPEk" id="1I84Bf8kfHM" role="1P9ThW">
                <node concept="2pJPED" id="1I84Bf8kfLy" role="2pJPEn">
                  <ref role="2pJxaS" to="nlc7:4V3GMfXszwl" resolve="EntityBiochemistryTestWithTextResultReference" />
                  <node concept="2pIpSj" id="1I84Bf8kfPx" role="2pJxcM">
                    <ref role="2pIpSl" to="nlc7:4V3GMfXszwm" resolve="target" />
                    <node concept="36biLy" id="1I84Bf8kfQB" role="2pJxcZ">
                      <node concept="1PxgMI" id="1I84Bf8kgap" role="36biLW">
                        <node concept="chp4Y" id="1I84Bf8kgaW" role="3oSUPX">
                          <ref role="cht4Q" to="4v7t:5Wfdz$0qWIu" resolve="BiochemistryTest" />
                        </node>
                        <node concept="37vLTw" id="1I84Bf8kfTR" role="1m5AlR">
                          <ref role="3cqZAo" node="1I84Bf8kf1d" resolve="node" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="1I84Bf8kf1d" role="3clF46">
        <property role="TrG5h" value="node" />
        <node concept="3Tqbb2" id="1I84Bf8kf1e" role="1tU5fm" />
      </node>
      <node concept="3cqZAl" id="1I84Bf8kf1f" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="1I84Bf8kzfz">
    <property role="3GE5qa" value="aspects.references" />
    <ref role="13h7C2" to="nlc7:6khVix$FDOg" resolve="AspectCodedInterpretativeCommentForRequestorReference" />
    <node concept="13hLZK" id="1I84Bf8kzf$" role="13h7CW">
      <node concept="3clFbS" id="1I84Bf8kzf_" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="1I84Bf8kzfI" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="replaceWith" />
      <ref role="13i0hy" to="wb6c:1I84Bf8ilTw" resolve="replaceWith" />
      <node concept="3Tm1VV" id="1I84Bf8kzfJ" role="1B3o_S" />
      <node concept="3clFbS" id="1I84Bf8kzfT" role="3clF47">
        <node concept="3clFbF" id="1I84Bf8kznb" role="3cqZAp">
          <node concept="2OqwBi" id="1I84Bf8kzzH" role="3clFbG">
            <node concept="13iPFW" id="1I84Bf8kzna" role="2Oq$k0" />
            <node concept="1P9Npp" id="1I84Bf8k$ba" role="2OqNvi">
              <node concept="2pJPEk" id="1I84Bf8k$dm" role="1P9ThW">
                <node concept="2pJPED" id="1I84Bf8k$f$" role="2pJPEn">
                  <ref role="2pJxaS" to="nlc7:6khVix$FAGJ" resolve="EntityCodedInterpretativeCommentForRequestorReference" />
                  <node concept="2pIpSj" id="1I84Bf8k$gA" role="2pJxcM">
                    <ref role="2pIpSl" to="nlc7:6khVix$FAGK" resolve="target" />
                    <node concept="36biLy" id="1I84Bf8k$ko" role="2pJxcZ">
                      <node concept="1PxgMI" id="1I84Bf8k$vv" role="36biLW">
                        <node concept="chp4Y" id="1I84Bf8k$w2" role="3oSUPX">
                          <ref role="cht4Q" to="9qnw:1mAGFBLG5RX" resolve="Autokommentar" />
                        </node>
                        <node concept="37vLTw" id="1I84Bf8k$nn" role="1m5AlR">
                          <ref role="3cqZAo" node="1I84Bf8kzfU" resolve="node" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="1I84Bf8kzfU" role="3clF46">
        <property role="TrG5h" value="node" />
        <node concept="3Tqbb2" id="1I84Bf8kzfV" role="1tU5fm" />
      </node>
      <node concept="3cqZAl" id="1I84Bf8kzfW" role="3clF45" />
    </node>
  </node>
</model>

