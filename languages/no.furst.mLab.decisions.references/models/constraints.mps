<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:505e5219-83f7-4f3d-9470-1fcfeef8f5db(no.furst.mLab.decisions.references.constraints)">
  <persistence version="9" />
  <languages>
    <use id="3f4bc5f5-c6c1-4a28-8b10-c83066ffa4a1" name="jetbrains.mps.lang.constraints" version="4" />
    <devkit ref="00000000-0000-4000-0000-5604ebd4f22c(jetbrains.mps.devkit.aspect.constraints)" />
  </languages>
  <imports>
    <import index="nlc7" ref="r:9d3523f4-3478-4519-bfbb-caf8e981a9d2(no.furst.mLab.decisions.references.structure)" implicit="true" />
    <import index="flit" ref="r:a6507f20-2eae-4f1e-aa64-edce1d2dcb45(no.uio.mLab.decisions.references.biochemistry.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="3f4bc5f5-c6c1-4a28-8b10-c83066ffa4a1" name="jetbrains.mps.lang.constraints">
      <concept id="8401916545537438642" name="jetbrains.mps.lang.constraints.structure.InheritedNodeScopeFactory" flags="ng" index="1dDu$B">
        <reference id="8401916545537438643" name="kind" index="1dDu$A" />
      </concept>
      <concept id="1213093968558" name="jetbrains.mps.lang.constraints.structure.ConceptConstraints" flags="ng" index="1M2fIO">
        <reference id="1213093996982" name="concept" index="1M2myG" />
        <child id="1213100494875" name="referent" index="1Mr941" />
      </concept>
      <concept id="1148687176410" name="jetbrains.mps.lang.constraints.structure.NodeReferentConstraint" flags="ng" index="1N5Pfh">
        <reference id="1148687202698" name="applicableLink" index="1N5Vy1" />
        <child id="1148687345559" name="searchScopeFactory" index="1N6uqs" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
    </language>
  </registry>
  <node concept="1M2fIO" id="4V3GMfXOD72">
    <property role="3GE5qa" value="aspects.references" />
    <ref role="1M2myG" to="nlc7:4V3GMfXsBti" resolve="AspectBiochemistryTestWithTextResultReference" />
    <node concept="1N5Pfh" id="4V3GMfXOD73" role="1Mr941">
      <ref role="1N5Vy1" to="nlc7:4V3GMfXunra" resolve="target" />
      <node concept="1dDu$B" id="4V3GMfXOD75" role="1N6uqs">
        <ref role="1dDu$A" to="flit:4QUW3edEuTG" resolve="BiochemistryTestVariable" />
      </node>
    </node>
  </node>
  <node concept="1M2fIO" id="6khVix$FEW2">
    <property role="3GE5qa" value="aspects.references" />
    <ref role="1M2myG" to="nlc7:6khVix$FDOg" resolve="AspectCodedInterpretativeCommentForRequestorReference" />
    <node concept="1N5Pfh" id="6khVix$FEW3" role="1Mr941">
      <ref role="1N5Vy1" to="nlc7:6khVix$FDOh" resolve="target" />
      <node concept="1dDu$B" id="6khVix$FEW5" role="1N6uqs">
        <ref role="1dDu$A" to="nlc7:6khVix$FDOi" resolve="CodedInterpretativeCommentForRequestorVariable" />
      </node>
    </node>
  </node>
</model>

