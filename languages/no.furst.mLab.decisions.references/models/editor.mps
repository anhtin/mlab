<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:d39515b7-05bb-4977-ad0c-450169ff1461(no.furst.mLab.decisions.references.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="11" />
    <use id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions" version="4" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="i7z6" ref="r:515ff38d-92f9-43f3-aa4c-c5bec29efe22(no.uio.mLab.decisions.core.editor)" />
    <import index="nlc7" ref="r:9d3523f4-3478-4519-bfbb-caf8e981a9d2(no.furst.mLab.decisions.references.structure)" implicit="true" />
    <import index="1yj" ref="r:0c887d67-81ab-420f-9a35-a9f6bf96c975(no.uio.mLab.shared.behavior)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="2000375450116454183" name="jetbrains.mps.lang.editor.structure.ISubstituteMenu" flags="ng" index="22mbnS">
        <child id="414384289274416996" name="parts" index="3ft7WO" />
      </concept>
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi" />
      <concept id="6089045305654894366" name="jetbrains.mps.lang.editor.structure.SubstituteMenuReference_Default" flags="ng" index="2kknPJ" />
      <concept id="1177327570013" name="jetbrains.mps.lang.editor.structure.QueryFunction_SubstituteMenu_Substitute" flags="in" index="ucgPf" />
      <concept id="8478191136883534237" name="jetbrains.mps.lang.editor.structure.IExtensibleSubstituteMenuPart" flags="ng" index="upBLQ">
        <child id="8478191136883534238" name="features" index="upBLP" />
      </concept>
      <concept id="1177335944525" name="jetbrains.mps.lang.editor.structure.QueryFunction_SubstituteMenu_SubstituteString" flags="in" index="uGdhv" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="1078938745671" name="jetbrains.mps.lang.editor.structure.EditorComponentDeclaration" flags="ig" index="PKFIW">
        <child id="7033942394258392116" name="overridenEditorComponent" index="1PM95z" />
      </concept>
      <concept id="615427434521884870" name="jetbrains.mps.lang.editor.structure.SubstituteMenuPart_Subconcepts" flags="ng" index="2VfDsV" />
      <concept id="1630016958697718209" name="jetbrains.mps.lang.editor.structure.IMenuReference_Default" flags="ng" index="2Z_bC8">
        <reference id="1630016958698373342" name="concept" index="2ZyFGn" />
      </concept>
      <concept id="1630016958697344083" name="jetbrains.mps.lang.editor.structure.IMenu_Concept" flags="ng" index="2ZABuq">
        <reference id="6591946374543067572" name="conceptDeclaration" index="aqKnT" />
      </concept>
      <concept id="8998492695583125082" name="jetbrains.mps.lang.editor.structure.SubstituteFeature_MatchingText" flags="ng" index="16NfWO">
        <child id="8998492695583129244" name="query" index="16NeZM" />
      </concept>
      <concept id="8998492695583129971" name="jetbrains.mps.lang.editor.structure.SubstituteFeature_DescriptionText" flags="ng" index="16NL0t">
        <child id="8998492695583129972" name="query" index="16NL0q" />
      </concept>
      <concept id="7342352913006985483" name="jetbrains.mps.lang.editor.structure.SubstituteMenuPart_Action" flags="ng" index="3eGOop">
        <child id="8612453216082699922" name="substituteHandler" index="3aKz83" />
      </concept>
      <concept id="3308396621974580100" name="jetbrains.mps.lang.editor.structure.SubstituteMenu_Default" flags="ng" index="3p36aQ" />
      <concept id="730181322658904464" name="jetbrains.mps.lang.editor.structure.SubstituteMenuPart_IncludeMenu" flags="ng" index="1s_PAr">
        <child id="730181322658904467" name="menuReference" index="1s_PAo" />
      </concept>
      <concept id="5425882385312046132" name="jetbrains.mps.lang.editor.structure.QueryFunctionParameter_SubstituteMenu_CurrentTargetNode" flags="nn" index="1yR$tW" />
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <property id="1139852716018" name="noTargetText" index="1$x2rV" />
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389577006" name="jetbrains.mps.lang.editor.structure.CellModel_Constant" flags="sn" stub="3610246225209162225" index="3F0ifn">
        <property id="1073389577007" name="text" index="3F0ifm" />
      </concept>
      <concept id="1073389882823" name="jetbrains.mps.lang.editor.structure.CellModel_RefNode" flags="sg" stub="730538219795960754" index="3F1sOY" />
      <concept id="7033942394256351208" name="jetbrains.mps.lang.editor.structure.EditorComponentDeclarationReference" flags="ng" index="1PE4EZ">
        <reference id="7033942394256351817" name="editorComponent" index="1PE7su" />
      </concept>
      <concept id="4307758654696938365" name="jetbrains.mps.lang.editor.structure.QueryFunction_SubstituteMenu_RefPresentation" flags="ig" index="1WAQ3h" />
      <concept id="4307758654696952957" name="jetbrains.mps.lang.editor.structure.QueryFunctionParameter_SubstituteMenu_ReferencedNode" flags="ng" index="1WAUZh" />
      <concept id="8428109087107030357" name="jetbrains.mps.lang.editor.structure.SubstituteMenuPart_ReferenceScope" flags="ng" index="3XHNnq">
        <reference id="8428109087107339113" name="reference" index="3XGfJA" />
        <child id="4307758654694907855" name="descriptionTextFunction" index="1WZ6hz" />
      </concept>
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
      </concept>
    </language>
    <language id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions">
      <concept id="5979988948250981289" name="jetbrains.mps.lang.actions.structure.SNodeCreatorAndInitializer" flags="nn" index="2fJWfE">
        <child id="3757480014665187678" name="prototype" index="1wAG5O" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="2644386474300074836" name="jetbrains.mps.lang.smodel.structure.ConceptIdRefExpression" flags="nn" index="35c_gC">
        <reference id="2644386474300074837" name="conceptDeclaration" index="35c_gD" />
      </concept>
      <concept id="1180636770613" name="jetbrains.mps.lang.smodel.structure.SNodeCreator" flags="nn" index="3zrR0B">
        <child id="1180636770616" name="createdType" index="3zrR0E" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="3p36aQ" id="4V3GMfXO_Wy">
    <property role="3GE5qa" value="base.references" />
    <ref role="aqKnT" to="nlc7:4V3GMfXszwl" resolve="EntityBiochemistryTestWithTextResultReference" />
    <node concept="3XHNnq" id="4V3GMfX$du$" role="3ft7WO">
      <ref role="3XGfJA" to="nlc7:4V3GMfXszwm" resolve="target" />
      <node concept="1WAQ3h" id="4V3GMfX$duA" role="1WZ6hz">
        <node concept="3clFbS" id="4V3GMfX$duB" role="2VODD2">
          <node concept="3clFbF" id="4V3GMfX$dBc" role="3cqZAp">
            <node concept="2OqwBi" id="4V3GMfX$dY6" role="3clFbG">
              <node concept="1WAUZh" id="4V3GMfXOAJs" role="2Oq$k0" />
              <node concept="2qgKlT" id="6_a_4w5obh6" role="2OqNvi">
                <ref role="37wK5l" to="1yj:4V3GMfXuP$X" resolve="getReferenceDescription" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="4V3GMfXOCOb">
    <property role="3GE5qa" value="aspects.references" />
    <ref role="aqKnT" to="nlc7:4V3GMfXsBti" resolve="AspectBiochemistryTestWithTextResultReference" />
    <node concept="3XHNnq" id="4V3GMfXOczw" role="3ft7WO">
      <ref role="3XGfJA" to="nlc7:4V3GMfXunra" resolve="target" />
      <node concept="1WAQ3h" id="4V3GMfXOczH" role="1WZ6hz">
        <node concept="3clFbS" id="4V3GMfXOczI" role="2VODD2">
          <node concept="3clFbF" id="4V3GMfXOcGj" role="3cqZAp">
            <node concept="2OqwBi" id="4V3GMfXOd3d" role="3clFbG">
              <node concept="1WAUZh" id="4V3GMfXOcGi" role="2Oq$k0" />
              <node concept="2qgKlT" id="6_a_4w5ocmW" role="2OqNvi">
                <ref role="37wK5l" to="1yj:4V3GMfXuP$X" resolve="getReferenceDescription" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="6khVix$FAHa">
    <property role="3GE5qa" value="base.references" />
    <ref role="aqKnT" to="nlc7:6khVix$FAGJ" resolve="EntityCodedInterpretativeCommentForRequestorReference" />
    <node concept="3XHNnq" id="6khVix$FAHb" role="3ft7WO">
      <ref role="3XGfJA" to="nlc7:6khVix$FAGK" resolve="target" />
      <node concept="1WAQ3h" id="6khVix$FAHd" role="1WZ6hz">
        <node concept="3clFbS" id="6khVix$FAHe" role="2VODD2">
          <node concept="3clFbF" id="6khVix$FAPN" role="3cqZAp">
            <node concept="2OqwBi" id="6khVix$FBcV" role="3clFbG">
              <node concept="1WAUZh" id="6khVix$FAPM" role="2Oq$k0" />
              <node concept="2qgKlT" id="6khVix$FBEI" role="2OqNvi">
                <ref role="37wK5l" to="1yj:4V3GMfXuP$X" resolve="getReferenceDescription" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="6khVix$FDOG">
    <property role="3GE5qa" value="aspects.references" />
    <ref role="aqKnT" to="nlc7:6khVix$FDOg" resolve="AspectCodedInterpretativeCommentForRequestorReference" />
    <node concept="3XHNnq" id="6khVix$FDOH" role="3ft7WO">
      <ref role="3XGfJA" to="nlc7:6khVix$FDOh" resolve="target" />
      <node concept="1WAQ3h" id="6khVix$FDOJ" role="1WZ6hz">
        <node concept="3clFbS" id="6khVix$FDOK" role="2VODD2">
          <node concept="3clFbF" id="6khVix$FDXl" role="3cqZAp">
            <node concept="2OqwBi" id="6khVix$FEiE" role="3clFbG">
              <node concept="1WAUZh" id="6khVix$FDXk" role="2Oq$k0" />
              <node concept="2qgKlT" id="6khVix$FEGr" role="2OqNvi">
                <ref role="37wK5l" to="1yj:4V3GMfXuP$X" resolve="getReferenceDescription" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="4B5aqq207Cy">
    <property role="3GE5qa" value="shared" />
    <ref role="aqKnT" to="nlc7:2TpFF5_2Hdd" resolve="CodedInterpretativeCommentForRequestorReference" />
    <node concept="1s_PAr" id="4B5aqq20ndU" role="3ft7WO">
      <node concept="2kknPJ" id="4B5aqq20ndZ" role="1s_PAo">
        <ref role="2ZyFGn" to="nlc7:6khVix$FDOg" resolve="AspectCodedInterpretativeCommentForRequestorReference" />
      </node>
    </node>
    <node concept="1s_PAr" id="4B5aqq207Cz" role="3ft7WO">
      <node concept="2kknPJ" id="4B5aqq207C_" role="1s_PAo">
        <ref role="2ZyFGn" to="nlc7:6khVix$FAGJ" resolve="EntityCodedInterpretativeCommentForRequestorReference" />
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="4B5aqq2$pmG">
    <property role="3GE5qa" value="shared" />
    <ref role="aqKnT" to="nlc7:4V3GMfXJ6OL" resolve="BiochemistryTestWithTextResultReference" />
    <node concept="1s_PAr" id="4B5aqq2$pmP" role="3ft7WO">
      <node concept="2kknPJ" id="4B5aqq2$pmU" role="1s_PAo">
        <ref role="2ZyFGn" to="nlc7:4V3GMfXsBti" resolve="AspectBiochemistryTestWithTextResultReference" />
      </node>
    </node>
    <node concept="1s_PAr" id="4B5aqq2$pmH" role="3ft7WO">
      <node concept="2kknPJ" id="4B5aqq2$pmJ" role="1s_PAo">
        <ref role="2ZyFGn" to="nlc7:4V3GMfXszwl" resolve="EntityBiochemistryTestWithTextResultReference" />
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="6LTgXmNWLEL">
    <property role="3GE5qa" value="aspects.patterns.codedInterpretativeComment" />
    <ref role="aqKnT" to="nlc7:6LTgXmNWLEn" resolve="CodedInterpretativeCommentForRequestorPattern" />
    <node concept="2VfDsV" id="6LTgXmNWLEM" role="3ft7WO" />
  </node>
  <node concept="24kQdi" id="6LTgXmNWLFe">
    <property role="3GE5qa" value="aspects.patterns.codedInterpretativeComment" />
    <ref role="1XX52x" to="nlc7:6LTgXmNWLEO" resolve="WildcardCodedInterpretativeForRequestorComment" />
    <node concept="3F0ifn" id="6LTgXmNWLFg" role="2wV5jI">
      <property role="3F0ifm" value="*" />
    </node>
  </node>
  <node concept="PKFIW" id="6LTgXmNWQ$W">
    <property role="3GE5qa" value="aspects.variables" />
    <property role="TrG5h" value="Pattern_CodedInterpretativeCommentForRequestorVariable_EditorComponent" />
    <ref role="1XX52x" to="nlc7:6khVix$FDOi" resolve="CodedInterpretativeCommentForRequestorVariable" />
    <node concept="3F1sOY" id="6LTgXmNWQTO" role="2wV5jI">
      <property role="1$x2rV" value="&lt;&lt; ... &gt;&gt;" />
      <ref role="1NtTu8" to="nlc7:6LTgXmNWQ$v" resolve="pattern" />
    </node>
    <node concept="1PE4EZ" id="6LTgXmNWQTM" role="1PM95z">
      <ref role="1PE7su" to="i7z6:6LTgXmNpPu9" resolve="Pattern_AspectVariable_EditorComponent" />
    </node>
  </node>
  <node concept="3p36aQ" id="6LTgXmNWR3N">
    <property role="3GE5qa" value="aspects.variables" />
    <ref role="aqKnT" to="nlc7:6khVix$FDOi" resolve="CodedInterpretativeCommentForRequestorVariable" />
    <node concept="3eGOop" id="6LTgXmNWR3O" role="3ft7WO">
      <node concept="ucgPf" id="6LTgXmNWR3P" role="3aKz83">
        <node concept="3clFbS" id="6LTgXmNWR3Q" role="2VODD2">
          <node concept="3clFbF" id="6LTgXmNWR8L" role="3cqZAp">
            <node concept="2ShNRf" id="6LTgXmNWR8J" role="3clFbG">
              <node concept="2fJWfE" id="6LTgXmNWRgP" role="2ShVmc">
                <node concept="3Tqbb2" id="6LTgXmNWRgR" role="3zrR0E">
                  <ref role="ehGHo" to="nlc7:6khVix$FDOi" resolve="CodedInterpretativeCommentForRequestorVariable" />
                </node>
                <node concept="1yR$tW" id="6LTgXmNWRqS" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="6LTgXmNWRvZ" role="upBLP">
        <node concept="uGdhv" id="6LTgXmNWR_8" role="16NeZM">
          <node concept="3clFbS" id="6LTgXmNWR_a" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmNWRHK" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmNWSdJ" role="3clFbG">
                <node concept="35c_gC" id="6LTgXmNWRHJ" role="2Oq$k0">
                  <ref role="35c_gD" to="nlc7:6khVix$FDOi" resolve="CodedInterpretativeCommentForRequestorVariable" />
                </node>
                <node concept="2qgKlT" id="6LTgXmNWSHc" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="6LTgXmNWT0j" role="upBLP">
        <node concept="uGdhv" id="6LTgXmNWT5D" role="16NL0q">
          <node concept="3clFbS" id="6LTgXmNWT5F" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmNWTeh" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmNWTIg" role="3clFbG">
                <node concept="35c_gC" id="6LTgXmNWTeg" role="2Oq$k0">
                  <ref role="35c_gD" to="nlc7:6khVix$FDOi" resolve="CodedInterpretativeCommentForRequestorVariable" />
                </node>
                <node concept="2qgKlT" id="6LTgXmNWUdH" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="6LTgXmNWXRk">
    <property role="3GE5qa" value="aspects.patterns.codedInterpretativeComment" />
    <ref role="aqKnT" to="nlc7:6LTgXmNWLEO" resolve="WildcardCodedInterpretativeForRequestorComment" />
    <node concept="3eGOop" id="6LTgXmNWXRl" role="3ft7WO">
      <node concept="ucgPf" id="6LTgXmNWXRm" role="3aKz83">
        <node concept="3clFbS" id="6LTgXmNWXRn" role="2VODD2">
          <node concept="3clFbF" id="6LTgXmNWXW2" role="3cqZAp">
            <node concept="2ShNRf" id="6LTgXmNWXW0" role="3clFbG">
              <node concept="2fJWfE" id="6LTgXmNWY3Q" role="2ShVmc">
                <node concept="3Tqbb2" id="6LTgXmNWY3S" role="3zrR0E">
                  <ref role="ehGHo" to="nlc7:6LTgXmNWLEO" resolve="WildcardCodedInterpretativeForRequestorComment" />
                </node>
                <node concept="1yR$tW" id="6LTgXmNWYdp" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="6LTgXmNWYig" role="upBLP">
        <node concept="uGdhv" id="6LTgXmNWYn9" role="16NeZM">
          <node concept="3clFbS" id="6LTgXmNWYnb" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmNWYvL" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmNWZ28" role="3clFbG">
                <node concept="35c_gC" id="6LTgXmNWYvK" role="2Oq$k0">
                  <ref role="35c_gD" to="nlc7:6LTgXmNWLEO" resolve="WildcardCodedInterpretativeForRequestorComment" />
                </node>
                <node concept="2qgKlT" id="6LTgXmNWZwA" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="6LTgXmNWZNm" role="upBLP">
        <node concept="uGdhv" id="6LTgXmNWZSs" role="16NL0q">
          <node concept="3clFbS" id="6LTgXmNWZSu" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmNX014" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmNX0Bp" role="3clFbG">
                <node concept="35c_gC" id="6LTgXmNX013" role="2Oq$k0">
                  <ref role="35c_gD" to="nlc7:6LTgXmNWLEO" resolve="WildcardCodedInterpretativeForRequestorComment" />
                </node>
                <node concept="2qgKlT" id="6LTgXmNX15O" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

