<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:9d3523f4-3478-4519-bfbb-caf8e981a9d2(no.furst.mLab.decisions.references.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="6" />
    <devkit ref="78434eb8-b0e5-444b-850d-e7c4ad2da9ab(jetbrains.mps.devkit.aspect.structure)" />
  </languages>
  <imports>
    <import index="4v7t" ref="r:3abf6acc-a4c5-4ee7-8b22-79c4941d18e6(no.uio.mLab.entities.biochemistry.structure)" />
    <import index="ruww" ref="r:d7787915-5b9a-4713-a6ab-6345c5e1ec03(no.uio.mLab.decisions.references.laboratoryTest.structure)" />
    <import index="9qnw" ref="r:2f6d4fa8-e4da-4f59-bd6f-30425e1dd68f(no.furst.mLab.entities.structure)" />
    <import index="7f9y" ref="r:790ac37e-42d3-4dd3-977c-e8fe9cc56a45(no.uio.mLab.decisions.core.structure)" />
    <import index="flit" ref="r:a6507f20-2eae-4f1e-aa64-edce1d2dcb45(no.uio.mLab.decisions.references.biochemistry.structure)" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <property id="4628067390765956807" name="final" index="R5$K2" />
        <property id="4628067390765956802" name="abstract" index="R5$K7" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599698500" name="specializedLink" index="20ksaX" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="4V3GMfXszwl">
    <property role="EcuMT" value="5675576922574305301" />
    <property role="3GE5qa" value="base.references" />
    <property role="TrG5h" value="EntityBiochemistryTestWithTextResultReference" />
    <ref role="1TJDcQ" node="4V3GMfXJ6OL" resolve="BiochemistryTestWithTextResultReference" />
    <node concept="1TJgyj" id="4V3GMfXszwm" role="1TKVEi">
      <property role="IQ2ns" value="5675576922574305302" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="target" />
      <ref role="20lvS9" to="4v7t:5Wfdz$0qWIu" resolve="BiochemistryTest" />
      <ref role="20ksaX" to="7f9y:4QUW3efv2jv" resolve="target" />
    </node>
    <node concept="PrWs8" id="6LTgXmMDI4T" role="PzmwI">
      <ref role="PrY4T" to="7f9y:6LTgXmMvpq9" resolve="IEntityReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="4V3GMfXsBti">
    <property role="EcuMT" value="5675576922574321490" />
    <property role="3GE5qa" value="aspects.references" />
    <property role="TrG5h" value="AspectBiochemistryTestWithTextResultReference" />
    <ref role="1TJDcQ" node="4V3GMfXJ6OL" resolve="BiochemistryTestWithTextResultReference" />
    <node concept="1TJgyj" id="4V3GMfXunra" role="1TKVEi">
      <property role="IQ2ns" value="5675576922574780106" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="target" />
      <ref role="20ksaX" to="7f9y:6LTgXmMAS1z" resolve="target" />
      <ref role="20lvS9" to="flit:4QUW3edEuTG" resolve="BiochemistryTestVariable" />
    </node>
    <node concept="PrWs8" id="6LTgXmMDI4K" role="PzmwI">
      <ref role="PrY4T" to="7f9y:6LTgXmMAn$t" resolve="IAspectVariableReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="2TpFF5_2Hdd">
    <property role="EcuMT" value="3339892675599586125" />
    <property role="3GE5qa" value="shared" />
    <property role="TrG5h" value="CodedInterpretativeCommentForRequestorReference" />
    <ref role="1TJDcQ" to="7f9y:4QUW3efv2iG" resolve="Reference" />
  </node>
  <node concept="1TIwiD" id="6khVix$FAGJ">
    <property role="EcuMT" value="7282862830178429743" />
    <property role="3GE5qa" value="base.references" />
    <property role="TrG5h" value="EntityCodedInterpretativeCommentForRequestorReference" />
    <ref role="1TJDcQ" node="2TpFF5_2Hdd" resolve="CodedInterpretativeCommentForRequestorReference" />
    <node concept="1TJgyj" id="6khVix$FAGK" role="1TKVEi">
      <property role="IQ2ns" value="7282862830178429744" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="target" />
      <ref role="20lvS9" to="9qnw:1mAGFBLG5RX" resolve="Autokommentar" />
      <ref role="20ksaX" to="7f9y:4QUW3efv2jv" resolve="target" />
    </node>
    <node concept="PrWs8" id="6LTgXmME_HX" role="PzmwI">
      <ref role="PrY4T" to="7f9y:6LTgXmMvpq9" resolve="IEntityReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="6khVix$FDOg">
    <property role="EcuMT" value="7282862830178442512" />
    <property role="3GE5qa" value="aspects.references" />
    <property role="TrG5h" value="AspectCodedInterpretativeCommentForRequestorReference" />
    <ref role="1TJDcQ" node="2TpFF5_2Hdd" resolve="CodedInterpretativeCommentForRequestorReference" />
    <node concept="1TJgyj" id="6khVix$FDOh" role="1TKVEi">
      <property role="IQ2ns" value="7282862830178442513" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="target" />
      <ref role="20lvS9" node="6khVix$FDOi" resolve="CodedInterpretativeCommentForRequestorVariable" />
      <ref role="20ksaX" to="7f9y:6LTgXmMAS1z" resolve="target" />
    </node>
    <node concept="PrWs8" id="6LTgXmMDI4N" role="PzmwI">
      <ref role="PrY4T" to="7f9y:6LTgXmMAn$t" resolve="IAspectVariableReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="6khVix$FDOi">
    <property role="EcuMT" value="7282862830178442514" />
    <property role="3GE5qa" value="aspects.variables" />
    <property role="TrG5h" value="CodedInterpretativeCommentForRequestorVariable" />
    <ref role="1TJDcQ" to="7f9y:4QUW3edDXqS" resolve="AspectVariable" />
    <node concept="1TJgyj" id="6LTgXmNWQ$v" role="1TKVEi">
      <property role="IQ2ns" value="7816353213404375327" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="pattern" />
      <ref role="20lvS9" node="6LTgXmNWLEn" resolve="CodedInterpretativeCommentForRequestorPattern" />
      <ref role="20ksaX" to="7f9y:6LTgXmNnjBG" resolve="pattern" />
    </node>
  </node>
  <node concept="1TIwiD" id="4V3GMfXJ6OL">
    <property role="EcuMT" value="5675576922579168561" />
    <property role="3GE5qa" value="shared" />
    <property role="TrG5h" value="BiochemistryTestWithTextResultReference" />
    <property role="R5$K7" value="true" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" to="ruww:4V3GMfXHyx1" resolve="LaboratoryTestWithTextResultReference" />
  </node>
  <node concept="1TIwiD" id="6LTgXmNWLEn">
    <property role="EcuMT" value="7816353213404355223" />
    <property role="3GE5qa" value="aspects.patterns.codedInterpretativeComment" />
    <property role="TrG5h" value="CodedInterpretativeCommentForRequestorPattern" />
    <ref role="1TJDcQ" to="7f9y:6LTgXmNrfh_" resolve="AspectVariablePattern" />
  </node>
  <node concept="1TIwiD" id="6LTgXmNWLEO">
    <property role="EcuMT" value="7816353213404355252" />
    <property role="3GE5qa" value="aspects.patterns.codedInterpretativeComment" />
    <property role="TrG5h" value="WildcardCodedInterpretativeForRequestorComment" />
    <ref role="1TJDcQ" node="6LTgXmNWLEn" resolve="CodedInterpretativeCommentForRequestorPattern" />
    <node concept="PrWs8" id="2FjKBCOoNKV" role="PzmwI">
      <ref role="PrY4T" to="7f9y:65epL7Ml5Dl" resolve="IWildcardPattern" />
    </node>
  </node>
</model>

