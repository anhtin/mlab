package no.furst.mLab.decisions.references.behavior;

/*Generated by MPS */

import jetbrains.mps.core.aspects.behaviour.BaseBHDescriptor;
import org.jetbrains.mps.openapi.language.SAbstractConcept;
import jetbrains.mps.smodel.adapter.structure.MetaAdapterFactory;
import jetbrains.mps.core.aspects.behaviour.api.BehaviorRegistry;
import jetbrains.mps.smodel.language.ConceptRegistry;
import jetbrains.mps.core.aspects.behaviour.api.SMethod;
import jetbrains.mps.core.aspects.behaviour.SMethodBuilder;
import jetbrains.mps.core.aspects.behaviour.SJavaCompoundTypeImpl;
import jetbrains.mps.core.aspects.behaviour.SModifiersImpl;
import jetbrains.mps.core.aspects.behaviour.AccessPrivileges;
import org.jetbrains.mps.openapi.model.SNode;
import java.util.List;
import java.util.Arrays;
import org.jetbrains.annotations.NotNull;
import jetbrains.mps.lang.smodel.generator.smodelAdapter.SNodeOperations;
import jetbrains.mps.core.aspects.behaviour.api.SConstructor;
import org.jetbrains.annotations.Nullable;
import jetbrains.mps.core.aspects.behaviour.api.BHMethodNotFoundException;
import org.jetbrains.mps.openapi.persistence.PersistenceFacade;
import jetbrains.mps.smodel.SModelUtil_new;

public final class AspectBiochemistryTestWithTextResultReference__BehaviorDescriptor extends BaseBHDescriptor {
  private static final SAbstractConcept CONCEPT = MetaAdapterFactory.getConcept(0x8fc9d2a591694fbeL, 0xa02d1c7f83e86b8eL, 0x4ec3b323fd727752L, "no.furst.mLab.decisions.references.structure.AspectBiochemistryTestWithTextResultReference");
  private static final BehaviorRegistry REGISTRY = ConceptRegistry.getInstance().getBehaviorRegistry();

  public static final SMethod<Void> replaceWith_id1I84Bf8ilTw = new SMethodBuilder<Void>(new SJavaCompoundTypeImpl(Void.class)).name("replaceWith").modifiers(SModifiersImpl.create(8, AccessPrivileges.PUBLIC)).concept(CONCEPT).id("1I84Bf8ilTw").registry(REGISTRY).build(SMethodBuilder.createJavaParameter((Class<SNode>) ((Class) Object.class), ""));

  private static final List<SMethod<?>> BH_METHODS = Arrays.<SMethod<?>>asList(replaceWith_id1I84Bf8ilTw);

  private static void ___init___(@NotNull SNode __thisNode__) {
  }

  /*package*/ static void replaceWith_id1I84Bf8ilTw(@NotNull SNode __thisNode__, SNode node) {
    SNodeOperations.replaceWithAnother(__thisNode__, createEntityBiochemistryTestWithTextResultReference_pbyuq6_a0a0a0(SNodeOperations.cast(node, MetaAdapterFactory.getConcept(0x284ce324ec914245L, 0xa186fefe6b9a1ea8L, 0x5f0f3639006bcb9eL, "no.uio.mLab.entities.biochemistry.structure.BiochemistryTest"))));
  }

  /*package*/ AspectBiochemistryTestWithTextResultReference__BehaviorDescriptor() {
    super(REGISTRY);
  }

  @Override
  protected void initNode(@NotNull SNode node, @NotNull SConstructor constructor, @Nullable Object[] parameters) {
    ___init___(node);
  }

  @Override
  protected <T> T invokeSpecial0(@NotNull SNode node, @NotNull SMethod<T> method, @Nullable Object[] parameters) {
    int methodIndex = BH_METHODS.indexOf(method);
    if (methodIndex < 0) {
      throw new BHMethodNotFoundException(this, method);
    }
    switch (methodIndex) {
      case 0:
        replaceWith_id1I84Bf8ilTw(node, (SNode) parameters[0]);
        return null;
      default:
        throw new BHMethodNotFoundException(this, method);
    }
  }

  @Override
  protected <T> T invokeSpecial0(@NotNull SAbstractConcept concept, @NotNull SMethod<T> method, @Nullable Object[] parameters) {
    int methodIndex = BH_METHODS.indexOf(method);
    if (methodIndex < 0) {
      throw new BHMethodNotFoundException(this, method);
    }
    switch (methodIndex) {
      default:
        throw new BHMethodNotFoundException(this, method);
    }
  }

  @NotNull
  @Override
  public List<SMethod<?>> getDeclaredMethods() {
    return BH_METHODS;
  }

  @NotNull
  @Override
  public SAbstractConcept getConcept() {
    return CONCEPT;
  }
  private static SNode createEntityBiochemistryTestWithTextResultReference_pbyuq6_a0a0a0(Object p0) {
    PersistenceFacade facade = PersistenceFacade.getInstance();
    SNode n1 = SModelUtil_new.instantiateConceptDeclaration(MetaAdapterFactory.getConcept(0x8fc9d2a591694fbeL, 0xa02d1c7f83e86b8eL, 0x4ec3b323fd723815L, "no.furst.mLab.decisions.references.structure.EntityBiochemistryTestWithTextResultReference"), null, null, false);
    n1.setReferenceTarget(MetaAdapterFactory.getReferenceLink(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x6c7943d5b27d9689L, 0x4dbaf0338f7c24dfL, "target"), (SNode) p0);
    return n1;
  }
}
