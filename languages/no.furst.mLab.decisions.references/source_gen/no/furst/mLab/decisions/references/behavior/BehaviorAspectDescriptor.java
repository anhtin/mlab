package no.furst.mLab.decisions.references.behavior;

/*Generated by MPS */

import jetbrains.mps.core.aspects.behaviour.BaseBehaviorAspectDescriptor;
import jetbrains.mps.core.aspects.behaviour.api.BHDescriptor;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.mps.openapi.language.SAbstractConcept;
import jetbrains.mps.lang.smodel.ConceptSwitchIndex;
import jetbrains.mps.lang.smodel.ConceptSwitchIndexBuilder;
import jetbrains.mps.smodel.adapter.ids.MetaIdFactory;

public final class BehaviorAspectDescriptor extends BaseBehaviorAspectDescriptor {
  private final BHDescriptor myAspectBiochemistryTestWithTextResultReference__BehaviorDescriptor = new AspectBiochemistryTestWithTextResultReference__BehaviorDescriptor();
  private final BHDescriptor myAspectCodedInterpretativeCommentForRequestorReference__BehaviorDescriptor = new AspectCodedInterpretativeCommentForRequestorReference__BehaviorDescriptor();
  private final BHDescriptor myCodedInterpretativeCommentForRequestorVariable__BehaviorDescriptor = new CodedInterpretativeCommentForRequestorVariable__BehaviorDescriptor();
  private final BHDescriptor myWildcardCodedInterpretativeForRequestorComment__BehaviorDescriptor = new WildcardCodedInterpretativeForRequestorComment__BehaviorDescriptor();

  public BehaviorAspectDescriptor() {
  }

  @Nullable
  public BHDescriptor getDescriptor(@NotNull SAbstractConcept concept) {
    SAbstractConcept cncpt = concept;
    switch (conceptIndex.index(cncpt)) {
      case 0:
        return myAspectBiochemistryTestWithTextResultReference__BehaviorDescriptor;
      case 1:
        return myAspectCodedInterpretativeCommentForRequestorReference__BehaviorDescriptor;
      case 2:
        return myCodedInterpretativeCommentForRequestorVariable__BehaviorDescriptor;
      case 3:
        return myWildcardCodedInterpretativeForRequestorComment__BehaviorDescriptor;
      default:
    }
    return null;
  }
  private static final ConceptSwitchIndex conceptIndex = new ConceptSwitchIndexBuilder().put(MetaIdFactory.conceptId(0x8fc9d2a591694fbeL, 0xa02d1c7f83e86b8eL, 0x4ec3b323fd727752L), MetaIdFactory.conceptId(0x8fc9d2a591694fbeL, 0xa02d1c7f83e86b8eL, 0x6511ed2864ae9d10L), MetaIdFactory.conceptId(0x8fc9d2a591694fbeL, 0xa02d1c7f83e86b8eL, 0x6511ed2864ae9d12L), MetaIdFactory.conceptId(0x8fc9d2a591694fbeL, 0xa02d1c7f83e86b8eL, 0x6c7943d5b3f31ab4L)).seal();
}
