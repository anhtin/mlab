<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:cbef5a58-8e18-4ddb-bc89-cb8af9e79ad1(main@generator)">
  <persistence version="9" />
  <languages>
    <devkit ref="a2eb3a43-fcc2-4200-80dc-c60110c4862d(jetbrains.mps.devkit.templates)" />
  </languages>
  <imports>
    <import index="iyzu" ref="r:4775ac76-6570-4dc9-a115-fe361cc83f23(no.furst.mLab.decisions.tasks.autocommentation.structure)" />
    <import index="80lt" ref="r:9508070b-94a6-47cc-a279-7b9d2ab44f4b(no.furst.mLab.decisions.tasks.autocommentation.runtime)" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="2820489544401957797" name="jetbrains.mps.baseLanguage.structure.DefaultClassCreator" flags="nn" index="HV5vD">
        <reference id="2820489544401957798" name="classifier" index="HV5vE" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1212685548494" name="jetbrains.mps.baseLanguage.structure.ClassCreator" flags="nn" index="1pGfFk" />
    </language>
    <language id="b401a680-8325-4110-8fd3-84331ff25bef" name="jetbrains.mps.lang.generator">
      <concept id="1114706874351" name="jetbrains.mps.lang.generator.structure.CopySrcNodeMacro" flags="ln" index="29HgVG">
        <child id="1168024447342" name="sourceNodeQuery" index="3NFExx" />
      </concept>
      <concept id="1095416546421" name="jetbrains.mps.lang.generator.structure.MappingConfiguration" flags="ig" index="bUwia">
        <child id="1167328349397" name="reductionMappingRule" index="3acgRq" />
        <child id="1195502100749" name="preMappingScript" index="1puA0r" />
      </concept>
      <concept id="1177093525992" name="jetbrains.mps.lang.generator.structure.InlineTemplate_RuleConsequence" flags="lg" index="gft3U">
        <child id="1177093586806" name="templateNode" index="gfFT$" />
      </concept>
      <concept id="1167169188348" name="jetbrains.mps.lang.generator.structure.TemplateFunctionParameter_sourceNode" flags="nn" index="30H73N" />
      <concept id="1167169308231" name="jetbrains.mps.lang.generator.structure.BaseMappingRule" flags="ng" index="30H$t8">
        <reference id="1167169349424" name="applicableConcept" index="30HIoZ" />
      </concept>
      <concept id="1087833241328" name="jetbrains.mps.lang.generator.structure.PropertyMacro" flags="ln" index="17Uvod">
        <child id="1167756362303" name="propertyValueFunction" index="3zH0cK" />
      </concept>
      <concept id="1167327847730" name="jetbrains.mps.lang.generator.structure.Reduction_MappingRule" flags="lg" index="3aamgX">
        <child id="1169672767469" name="ruleConsequence" index="1lVwrX" />
      </concept>
      <concept id="1195499912406" name="jetbrains.mps.lang.generator.structure.MappingScript" flags="lg" index="1pmfR0">
        <property id="1195595592106" name="scriptKind" index="1v3f2W" />
        <child id="1195501105008" name="codeBlock" index="1pqMTA" />
      </concept>
      <concept id="1195500722856" name="jetbrains.mps.lang.generator.structure.MappingScript_CodeBlock" flags="in" index="1pplIY" />
      <concept id="1195502151594" name="jetbrains.mps.lang.generator.structure.MappingScriptReference" flags="lg" index="1puMqW">
        <reference id="1195502167610" name="mappingScript" index="1puQsG" />
      </concept>
      <concept id="1167756080639" name="jetbrains.mps.lang.generator.structure.PropertyMacro_GetPropertyValue" flags="in" index="3zFVjK" />
      <concept id="1168024337012" name="jetbrains.mps.lang.generator.structure.SourceSubstituteMacro_SourceNodeQuery" flags="in" index="3NFfHV" />
    </language>
    <language id="760a0a8c-eabb-4521-8bfd-65db761a9ba3" name="jetbrains.mps.baseLanguage.logging">
      <concept id="6332851714983831325" name="jetbrains.mps.baseLanguage.logging.structure.MsgStatement" flags="ng" index="2xdQw9">
        <property id="6332851714983843871" name="severity" index="2xdLsb" />
        <child id="5721587534047265374" name="message" index="9lYJi" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="3364660638048049750" name="jetbrains.mps.lang.core.structure.PropertyAttribute" flags="ng" index="A9Btg">
        <property id="1757699476691236117" name="propertyName" index="2qtEX9" />
        <property id="1341860900487648621" name="propertyId" index="P4ACc" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="bUwia" id="ofcHXfN8ri">
    <property role="TrG5h" value="furstTaskAutocommentationMain" />
    <node concept="1puMqW" id="7lYCqhuhO8n" role="1puA0r">
      <ref role="1puQsG" node="7lYCqhui0lL" resolve="furstTaskAutocommentationScript" />
    </node>
    <node concept="3aamgX" id="2FjKBCRqWCt" role="3acgRq">
      <ref role="30HIoZ" to="iyzu:2FjKBCRoN3t" resolve="LaboratoryTestResultVariance" />
      <node concept="gft3U" id="2FjKBCRqWDb" role="1lVwrX">
        <node concept="2ShNRf" id="2FjKBCRqWDh" role="gfFT$">
          <node concept="1pGfFk" id="2FjKBCRqWGv" role="2ShVmc">
            <ref role="37wK5l" to="80lt:2FjKBCRqNCC" resolve="TestResultVariance" />
            <node concept="Xl_RD" id="2FjKBCRqWGE" role="37wK5m">
              <property role="Xl_RC" value="test" />
              <node concept="29HgVG" id="2FjKBCRqWH8" role="lGtFl">
                <node concept="3NFfHV" id="2FjKBCRqWH9" role="3NFExx">
                  <node concept="3clFbS" id="2FjKBCRqWHa" role="2VODD2">
                    <node concept="3clFbF" id="2FjKBCRqWHg" role="3cqZAp">
                      <node concept="2OqwBi" id="2FjKBCRqWHb" role="3clFbG">
                        <node concept="3TrEf2" id="2FjKBCRqWHe" role="2OqNvi">
                          <ref role="3Tt5mk" to="iyzu:2FjKBCRpX2Y" resolve="testReference" />
                        </node>
                        <node concept="30H73N" id="2FjKBCRqWHf" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="2FjKBCRr1KC" role="3acgRq">
      <ref role="30HIoZ" to="iyzu:2FjKBCRpzOI" resolve="TestResultVarianceEqualTo" />
      <node concept="gft3U" id="2FjKBCRr1LY" role="1lVwrX">
        <node concept="2ShNRf" id="2FjKBCRr1M4" role="gfFT$">
          <node concept="1pGfFk" id="2FjKBCRr4PN" role="2ShVmc">
            <ref role="37wK5l" to="80lt:2FjKBCRr1QU" resolve="TestResultVarianceEqualTo" />
            <node concept="2ShNRf" id="2FjKBCRr4PW" role="37wK5m">
              <node concept="HV5vD" id="2FjKBCRr4UZ" role="2ShVmc">
                <ref role="HV5vE" to="80lt:2FjKBCRqCki" resolve="TestResultVarianceLiteral" />
              </node>
              <node concept="29HgVG" id="2FjKBCRr4Vi" role="lGtFl">
                <node concept="3NFfHV" id="2FjKBCRr4Vj" role="3NFExx">
                  <node concept="3clFbS" id="2FjKBCRr4Vk" role="2VODD2">
                    <node concept="3clFbF" id="2FjKBCRr4Vq" role="3cqZAp">
                      <node concept="2OqwBi" id="2FjKBCRr4Vl" role="3clFbG">
                        <node concept="3TrEf2" id="2FjKBCRr4Vo" role="2OqNvi">
                          <ref role="3Tt5mk" to="iyzu:2FjKBCRpHpc" resolve="variance" />
                        </node>
                        <node concept="30H73N" id="2FjKBCRr4Vp" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="2FjKBCRqWOy" role="3acgRq">
      <ref role="30HIoZ" to="iyzu:2FjKBCRoN3v" resolve="SignificantTestResultVariance" />
      <node concept="gft3U" id="2FjKBCRqWPu" role="1lVwrX">
        <node concept="2YIFZM" id="2FjKBCRqYKc" role="gfFT$">
          <ref role="37wK5l" to="80lt:2FjKBCRqGEn" resolve="getInstance" />
          <ref role="1Pybhc" to="80lt:2FjKBCRqGEi" resolve="SignificantTestResultVariance" />
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="2FjKBCRqYKm" role="3acgRq">
      <ref role="30HIoZ" to="iyzu:2FjKBCRoN3w" resolve="InsignificantTestResultVariance" />
      <node concept="gft3U" id="2FjKBCRqYLv" role="1lVwrX">
        <node concept="2YIFZM" id="2FjKBCRqYMb" role="gfFT$">
          <ref role="37wK5l" to="80lt:2FjKBCRqGhv" resolve="getInstance" />
          <ref role="1Pybhc" to="80lt:2FjKBCRqCld" resolve="InsignificantTestResultVariance" />
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="7lYCqhv59qr" role="3acgRq">
      <ref role="30HIoZ" to="iyzu:7lYCqhv527q" resolve="AddStatisticalLabel" />
      <node concept="gft3U" id="7lYCqhv59sU" role="1lVwrX">
        <node concept="2ShNRf" id="7lYCqhv59t6" role="gfFT$">
          <node concept="1pGfFk" id="7lYCqhv59wa" role="2ShVmc">
            <ref role="37wK5l" to="80lt:7lYCqhv529b" resolve="AddStatisticalLabel" />
            <node concept="Xl_RD" id="7lYCqhv59$T" role="37wK5m">
              <property role="Xl_RC" value="label" />
              <node concept="17Uvod" id="7lYCqhv59_i" role="lGtFl">
                <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1070475926800/1070475926801" />
                <property role="2qtEX9" value="value" />
                <node concept="3zFVjK" id="7lYCqhv59_l" role="3zH0cK">
                  <node concept="3clFbS" id="7lYCqhv59_m" role="2VODD2">
                    <node concept="3clFbF" id="7lYCqhv59_s" role="3cqZAp">
                      <node concept="2OqwBi" id="7lYCqhv59_n" role="3clFbG">
                        <node concept="3TrcHB" id="7lYCqhv59_q" role="2OqNvi">
                          <ref role="3TsBF5" to="iyzu:7lYCqhv59wj" resolve="label" />
                        </node>
                        <node concept="30H73N" id="7lYCqhv59_r" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="1Hxyv4DSzUw" role="3acgRq">
      <ref role="30HIoZ" to="iyzu:1Hxyv4DRzM5" resolve="CodedInterpretativeCommentForRequestor" />
      <node concept="gft3U" id="1Hxyv4DSzUU" role="1lVwrX">
        <node concept="2ShNRf" id="1Hxyv4DSzV0" role="gfFT$">
          <node concept="1pGfFk" id="1Hxyv4DSCBE" role="2ShVmc">
            <ref role="37wK5l" to="80lt:ofcHXfNN5f" resolve="CodedInterpretativeComment" />
            <node concept="Xl_RD" id="7lYCqhuhThl" role="37wK5m">
              <property role="Xl_RC" value="comment" />
              <node concept="29HgVG" id="7lYCqhuhTh$" role="lGtFl">
                <node concept="3NFfHV" id="7lYCqhuhTh_" role="3NFExx">
                  <node concept="3clFbS" id="7lYCqhuhThA" role="2VODD2">
                    <node concept="3clFbF" id="7lYCqhuhThG" role="3cqZAp">
                      <node concept="2OqwBi" id="7lYCqhuhThB" role="3clFbG">
                        <node concept="3TrEf2" id="7lYCqhuhThE" role="2OqNvi">
                          <ref role="3Tt5mk" to="iyzu:2TpFF5_2NuO" resolve="commentReference" />
                        </node>
                        <node concept="30H73N" id="7lYCqhuhThF" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="2FjKBCR6ZFP" role="3acgRq">
      <ref role="30HIoZ" to="iyzu:2FjKBCR5FI6" resolve="SuggestAdditionalTestToRequestor" />
      <node concept="gft3U" id="2FjKBCR78CV" role="1lVwrX">
        <node concept="2ShNRf" id="2FjKBCR78D1" role="gfFT$">
          <node concept="1pGfFk" id="2FjKBCR78G6" role="2ShVmc">
            <ref role="37wK5l" to="80lt:2FjKBCR6ZHd" resolve="SuggestAdditionalTestToRequestor" />
            <node concept="Xl_RD" id="2FjKBCR78Gf" role="37wK5m">
              <property role="Xl_RC" value="test" />
              <node concept="29HgVG" id="2FjKBCR78Nx" role="lGtFl">
                <node concept="3NFfHV" id="2FjKBCR78Ny" role="3NFExx">
                  <node concept="3clFbS" id="2FjKBCR78Nz" role="2VODD2">
                    <node concept="3clFbF" id="2FjKBCR78ND" role="3cqZAp">
                      <node concept="2OqwBi" id="2FjKBCR78N$" role="3clFbG">
                        <node concept="3TrEf2" id="2FjKBCR78NB" role="2OqNvi">
                          <ref role="3Tt5mk" to="iyzu:2FjKBCR5FI7" resolve="testReference" />
                        </node>
                        <node concept="30H73N" id="2FjKBCR78NC" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="1pmfR0" id="7lYCqhui0lL">
    <property role="TrG5h" value="furstTaskAutocommentationScript" />
    <property role="1v3f2W" value="pre_processing" />
    <node concept="1pplIY" id="7lYCqhui0lM" role="1pqMTA">
      <node concept="3clFbS" id="7lYCqhui0lN" role="2VODD2">
        <node concept="2xdQw9" id="7lYCqhui0lX" role="3cqZAp">
          <property role="2xdLsb" value="debug" />
          <node concept="Xl_RD" id="7lYCqhui0lZ" role="9lYJi">
            <property role="Xl_RC" value="furst.tasks.autocommentation" />
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

