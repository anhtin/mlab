<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:3c706741-632f-434d-8086-06ab0d72f42a(no.furst.mLab.decisions.tasks.autocommentation.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="11" />
    <use id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions" version="4" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="iyzu" ref="r:4775ac76-6570-4dc9-a115-fe361cc83f23(no.furst.mLab.decisions.tasks.autocommentation.structure)" implicit="true" />
    <import index="nlc7" ref="r:9d3523f4-3478-4519-bfbb-caf8e981a9d2(no.furst.mLab.decisions.references.structure)" implicit="true" />
    <import index="1yj" ref="r:0c887d67-81ab-420f-9a35-a9f6bf96c975(no.uio.mLab.shared.behavior)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="2000375450116454183" name="jetbrains.mps.lang.editor.structure.ISubstituteMenu" flags="ng" index="22mbnS">
        <child id="414384289274416996" name="parts" index="3ft7WO" />
      </concept>
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi" />
      <concept id="1106270549637" name="jetbrains.mps.lang.editor.structure.CellLayout_Horizontal" flags="nn" index="2iRfu4" />
      <concept id="6089045305654894366" name="jetbrains.mps.lang.editor.structure.SubstituteMenuReference_Default" flags="ng" index="2kknPJ" />
      <concept id="1177327570013" name="jetbrains.mps.lang.editor.structure.QueryFunction_SubstituteMenu_Substitute" flags="in" index="ucgPf" />
      <concept id="8478191136883534237" name="jetbrains.mps.lang.editor.structure.IExtensibleSubstituteMenuPart" flags="ng" index="upBLQ">
        <child id="8478191136883534238" name="features" index="upBLP" />
      </concept>
      <concept id="1177335944525" name="jetbrains.mps.lang.editor.structure.QueryFunction_SubstituteMenu_SubstituteString" flags="in" index="uGdhv" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="8383079901754291618" name="jetbrains.mps.lang.editor.structure.CellModel_NextEditor" flags="ng" index="B$lHz" />
      <concept id="615427434521884870" name="jetbrains.mps.lang.editor.structure.SubstituteMenuPart_Subconcepts" flags="ng" index="2VfDsV" />
      <concept id="1630016958697718209" name="jetbrains.mps.lang.editor.structure.IMenuReference_Default" flags="ng" index="2Z_bC8">
        <reference id="1630016958698373342" name="concept" index="2ZyFGn" />
      </concept>
      <concept id="1630016958697344083" name="jetbrains.mps.lang.editor.structure.IMenu_Concept" flags="ng" index="2ZABuq">
        <reference id="6591946374543067572" name="conceptDeclaration" index="aqKnT" />
      </concept>
      <concept id="8998492695583125082" name="jetbrains.mps.lang.editor.structure.SubstituteFeature_MatchingText" flags="ng" index="16NfWO">
        <child id="8998492695583129244" name="query" index="16NeZM" />
      </concept>
      <concept id="8998492695583129971" name="jetbrains.mps.lang.editor.structure.SubstituteFeature_DescriptionText" flags="ng" index="16NL0t">
        <child id="8998492695583129972" name="query" index="16NL0q" />
      </concept>
      <concept id="7342352913006985483" name="jetbrains.mps.lang.editor.structure.SubstituteMenuPart_Action" flags="ng" index="3eGOop">
        <child id="8612453216082699922" name="substituteHandler" index="3aKz83" />
      </concept>
      <concept id="3308396621974580100" name="jetbrains.mps.lang.editor.structure.SubstituteMenu_Default" flags="ng" index="3p36aQ" />
      <concept id="5425882385312046132" name="jetbrains.mps.lang.editor.structure.QueryFunctionParameter_SubstituteMenu_CurrentTargetNode" flags="nn" index="1yR$tW" />
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <property id="1139852716018" name="noTargetText" index="1$x2rV" />
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389446423" name="jetbrains.mps.lang.editor.structure.CellModel_Collection" flags="sn" stub="3013115976261988961" index="3EZMnI">
        <child id="1106270802874" name="cellLayout" index="2iSdaV" />
        <child id="1073389446424" name="childCellModel" index="3EZMnx" />
      </concept>
      <concept id="1073389658414" name="jetbrains.mps.lang.editor.structure.CellModel_Property" flags="sg" stub="730538219796134133" index="3F0A7n" />
      <concept id="1073389882823" name="jetbrains.mps.lang.editor.structure.CellModel_RefNode" flags="sg" stub="730538219795960754" index="3F1sOY" />
      <concept id="6684862045052272180" name="jetbrains.mps.lang.editor.structure.QueryFunctionParameter_SubstituteMenu_NodeToWrap" flags="ng" index="3N4pyC" />
      <concept id="6684862045052059649" name="jetbrains.mps.lang.editor.structure.QueryFunction_SubstituteMenu_WrapperHandler" flags="ig" index="3N5aqt" />
      <concept id="6684862045052059291" name="jetbrains.mps.lang.editor.structure.SubstituteMenuPart_Wrapper" flags="ng" index="3N5dw7">
        <child id="6089045305655104958" name="reference" index="2klrvf" />
        <child id="6684862045053873740" name="handler" index="3Na0zg" />
      </concept>
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
      </concept>
    </language>
    <language id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions">
      <concept id="5979988948250981289" name="jetbrains.mps.lang.actions.structure.SNodeCreatorAndInitializer" flags="nn" index="2fJWfE">
        <child id="3757480014665187678" name="prototype" index="1wAG5O" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="2644386474300074836" name="jetbrains.mps.lang.smodel.structure.ConceptIdRefExpression" flags="nn" index="35c_gC">
        <reference id="2644386474300074837" name="conceptDeclaration" index="35c_gD" />
      </concept>
      <concept id="1180636770613" name="jetbrains.mps.lang.smodel.structure.SNodeCreator" flags="nn" index="3zrR0B">
        <child id="1180636770616" name="createdType" index="3zrR0E" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="24kQdi" id="1Hxyv4DRzMW">
    <property role="3GE5qa" value="base.parameters.interpretativeComment" />
    <ref role="1XX52x" to="iyzu:1Hxyv4DRzM5" resolve="CodedInterpretativeCommentForRequestor" />
    <node concept="3F1sOY" id="2TpFF5_2NSz" role="2wV5jI">
      <ref role="1NtTu8" to="iyzu:2TpFF5_2NuO" resolve="commentReference" />
    </node>
  </node>
  <node concept="3p36aQ" id="1Hxyv4DSpF1">
    <property role="3GE5qa" value="base.parameters.interpretativeComment" />
    <ref role="aqKnT" to="iyzu:1Hxyv4DRzM5" resolve="CodedInterpretativeCommentForRequestor" />
    <node concept="3N5dw7" id="2TpFF5_2OTC" role="3ft7WO">
      <node concept="3N5aqt" id="2TpFF5_2OTD" role="3Na0zg">
        <node concept="3clFbS" id="2TpFF5_2OTE" role="2VODD2">
          <node concept="3cpWs8" id="2TpFF5_2VuA" role="3cqZAp">
            <node concept="3cpWsn" id="2TpFF5_2VuD" role="3cpWs9">
              <property role="TrG5h" value="newNode" />
              <node concept="3Tqbb2" id="2TpFF5_2Vu$" role="1tU5fm">
                <ref role="ehGHo" to="iyzu:1Hxyv4DRzM5" resolve="CodedInterpretativeCommentForRequestor" />
              </node>
              <node concept="2ShNRf" id="2TpFF5_2WgA" role="33vP2m">
                <node concept="2fJWfE" id="2TpFF5_2UtL" role="2ShVmc">
                  <node concept="3Tqbb2" id="2TpFF5_2UtN" role="3zrR0E">
                    <ref role="ehGHo" to="iyzu:1Hxyv4DRzM5" resolve="CodedInterpretativeCommentForRequestor" />
                  </node>
                  <node concept="1yR$tW" id="2TpFF5_2UO6" role="1wAG5O" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="2TpFF5_2WE1" role="3cqZAp">
            <node concept="37vLTI" id="2TpFF5_2XNU" role="3clFbG">
              <node concept="2OqwBi" id="2TpFF5_2WT$" role="37vLTJ">
                <node concept="37vLTw" id="2TpFF5_2WDZ" role="2Oq$k0">
                  <ref role="3cqZAo" node="2TpFF5_2VuD" resolve="newNode" />
                </node>
                <node concept="3TrEf2" id="2TpFF5_2XfC" role="2OqNvi">
                  <ref role="3Tt5mk" to="iyzu:2TpFF5_2NuO" resolve="commentReference" />
                </node>
              </node>
              <node concept="3N4pyC" id="2TpFF5_32h3" role="37vLTx" />
            </node>
          </node>
          <node concept="3clFbF" id="2TpFF5_2YhV" role="3cqZAp">
            <node concept="37vLTw" id="2TpFF5_2YhT" role="3clFbG">
              <ref role="3cqZAo" node="2TpFF5_2VuD" resolve="newNode" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2kknPJ" id="2TpFF5_2OY_" role="2klrvf">
        <ref role="2ZyFGn" to="nlc7:2TpFF5_2Hdd" resolve="CodedInterpretativeCommentForRequestorReference" />
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="7lYCqhv5fjj">
    <property role="3GE5qa" value="base.actions" />
    <ref role="aqKnT" to="iyzu:7lYCqhv527q" resolve="AddStatisticalLabel" />
    <node concept="3eGOop" id="7lYCqhv5fjk" role="3ft7WO">
      <node concept="ucgPf" id="7lYCqhv5fjl" role="3aKz83">
        <node concept="3clFbS" id="7lYCqhv5fjm" role="2VODD2">
          <node concept="3clFbF" id="7lYCqhv5fnL" role="3cqZAp">
            <node concept="2ShNRf" id="7lYCqhv5fnJ" role="3clFbG">
              <node concept="2fJWfE" id="7lYCqhv5fvl" role="2ShVmc">
                <node concept="3Tqbb2" id="7lYCqhv5fvn" role="3zrR0E">
                  <ref role="ehGHo" to="iyzu:7lYCqhv527q" resolve="AddStatisticalLabel" />
                </node>
                <node concept="1yR$tW" id="7lYCqhv5fEr" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="7lYCqhv5hgw" role="upBLP">
        <node concept="uGdhv" id="7lYCqhv5hlm" role="16NeZM">
          <node concept="3clFbS" id="7lYCqhv5hlo" role="2VODD2">
            <node concept="3clFbF" id="7lYCqhv5htY" role="3cqZAp">
              <node concept="2OqwBi" id="7lYCqhv5i6z" role="3clFbG">
                <node concept="35c_gC" id="7lYCqhv5htX" role="2Oq$k0">
                  <ref role="35c_gD" to="iyzu:7lYCqhv527q" resolve="AddStatisticalLabel" />
                </node>
                <node concept="2qgKlT" id="7lYCqhv5i$3" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="7lYCqhv5fJ2" role="upBLP">
        <node concept="uGdhv" id="7lYCqhv5fNF" role="16NL0q">
          <node concept="3clFbS" id="7lYCqhv5fNH" role="2VODD2">
            <node concept="3clFbF" id="7lYCqhv5fWj" role="3cqZAp">
              <node concept="2OqwBi" id="7lYCqhv5gwz" role="3clFbG">
                <node concept="35c_gC" id="7lYCqhv5fWi" role="2Oq$k0">
                  <ref role="35c_gD" to="iyzu:7lYCqhv527q" resolve="AddStatisticalLabel" />
                </node>
                <node concept="2qgKlT" id="7lYCqhv5gY3" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="7lYCqhv5j4$">
    <property role="3GE5qa" value="base.actions" />
    <ref role="1XX52x" to="iyzu:7lYCqhv527q" resolve="AddStatisticalLabel" />
    <node concept="3EZMnI" id="7lYCqhv5j4A" role="2wV5jI">
      <node concept="2iRfu4" id="7lYCqhv5j4B" role="2iSdaV" />
      <node concept="B$lHz" id="2XLt5KUvoQU" role="3EZMnx" />
      <node concept="3F0A7n" id="7lYCqhv5j4L" role="3EZMnx">
        <property role="1$x2rV" value="..." />
        <ref role="1NtTu8" to="iyzu:7lYCqhv59wj" resolve="label" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="2FjKBCR5FIy">
    <property role="3GE5qa" value="base.actions" />
    <ref role="1XX52x" to="iyzu:2FjKBCR5FI6" resolve="SuggestAdditionalTestToRequestor" />
    <node concept="3EZMnI" id="2FjKBCR5FI$" role="2wV5jI">
      <node concept="2iRfu4" id="2FjKBCR5FI_" role="2iSdaV" />
      <node concept="B$lHz" id="2FjKBCR5FIE" role="3EZMnx" />
      <node concept="3F1sOY" id="2FjKBCR5FIK" role="3EZMnx">
        <property role="1$x2rV" value="&lt;&lt; ... &gt;&gt;" />
        <ref role="1NtTu8" to="iyzu:2FjKBCR5FI7" resolve="testReference" />
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="2FjKBCR5SrP">
    <property role="3GE5qa" value="base.actions" />
    <ref role="aqKnT" to="iyzu:2FjKBCR5FI6" resolve="SuggestAdditionalTestToRequestor" />
    <node concept="3eGOop" id="2FjKBCR5SrQ" role="3ft7WO">
      <node concept="ucgPf" id="2FjKBCR5SrR" role="3aKz83">
        <node concept="3clFbS" id="2FjKBCR5SrS" role="2VODD2">
          <node concept="3clFbF" id="2FjKBCR5Sx5" role="3cqZAp">
            <node concept="2ShNRf" id="2FjKBCR5Sx3" role="3clFbG">
              <node concept="2fJWfE" id="2FjKBCR5SDo" role="2ShVmc">
                <node concept="3Tqbb2" id="2FjKBCR5SDq" role="3zrR0E">
                  <ref role="ehGHo" to="iyzu:2FjKBCR5FI6" resolve="SuggestAdditionalTestToRequestor" />
                </node>
                <node concept="1yR$tW" id="2FjKBCR5SNZ" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="2FjKBCR5STo" role="upBLP">
        <node concept="uGdhv" id="2FjKBCR5SYN" role="16NeZM">
          <node concept="3clFbS" id="2FjKBCR5SYP" role="2VODD2">
            <node concept="3clFbF" id="2FjKBCR5T7r" role="3cqZAp">
              <node concept="2OqwBi" id="2FjKBCR5TES" role="3clFbG">
                <node concept="35c_gC" id="2FjKBCR5T7q" role="2Oq$k0">
                  <ref role="35c_gD" to="iyzu:2FjKBCR5FI6" resolve="SuggestAdditionalTestToRequestor" />
                </node>
                <node concept="2qgKlT" id="2FjKBCR5UbM" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="2FjKBCR5Uvo" role="upBLP">
        <node concept="uGdhv" id="2FjKBCR5U_0" role="16NL0q">
          <node concept="3clFbS" id="2FjKBCR5U_2" role="2VODD2">
            <node concept="3clFbF" id="2FjKBCR5UHC" role="3cqZAp">
              <node concept="2OqwBi" id="2FjKBCR5VnG" role="3clFbG">
                <node concept="35c_gC" id="2FjKBCR5UHB" role="2Oq$k0">
                  <ref role="35c_gD" to="iyzu:2FjKBCR5FI6" resolve="SuggestAdditionalTestToRequestor" />
                </node>
                <node concept="2qgKlT" id="2FjKBCR5VSA" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="2FjKBCRpJKa">
    <property role="3GE5qa" value="base.constraints.variance" />
    <ref role="1XX52x" to="iyzu:2FjKBCRpzOI" resolve="TestResultVarianceEqualTo" />
    <node concept="3EZMnI" id="2FjKBCRpJWI" role="2wV5jI">
      <node concept="2iRfu4" id="2FjKBCRpJWJ" role="2iSdaV" />
      <node concept="B$lHz" id="2FjKBCRpJWF" role="3EZMnx" />
      <node concept="3F1sOY" id="2FjKBCRpJWR" role="3EZMnx">
        <property role="1$x2rV" value="&lt;&lt; .. &gt;&gt;" />
        <ref role="1NtTu8" to="iyzu:2FjKBCRpHpc" resolve="variance" />
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="2FjKBCRpOlH">
    <property role="3GE5qa" value="base.constraints.variance" />
    <ref role="aqKnT" to="iyzu:2FjKBCRpzOI" resolve="TestResultVarianceEqualTo" />
    <node concept="3eGOop" id="2FjKBCRpOlI" role="3ft7WO">
      <node concept="ucgPf" id="2FjKBCRpOlJ" role="3aKz83">
        <node concept="3clFbS" id="2FjKBCRpOlK" role="2VODD2">
          <node concept="3clFbF" id="2FjKBCRpOqX" role="3cqZAp">
            <node concept="2ShNRf" id="2FjKBCRpOqV" role="3clFbG">
              <node concept="2fJWfE" id="2FjKBCRpOzg" role="2ShVmc">
                <node concept="3Tqbb2" id="2FjKBCRpOzi" role="3zrR0E">
                  <ref role="ehGHo" to="iyzu:2FjKBCRpzOI" resolve="TestResultVarianceEqualTo" />
                </node>
                <node concept="1yR$tW" id="2FjKBCRpOHR" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="2FjKBCRpONg" role="upBLP">
        <node concept="uGdhv" id="2FjKBCRpP3J" role="16NeZM">
          <node concept="3clFbS" id="2FjKBCRpP3L" role="2VODD2">
            <node concept="3clFbF" id="2FjKBCRpPcn" role="3cqZAp">
              <node concept="2OqwBi" id="2FjKBCRpPGM" role="3clFbG">
                <node concept="35c_gC" id="2FjKBCRpPcm" role="2Oq$k0">
                  <ref role="35c_gD" to="iyzu:2FjKBCRpzOI" resolve="TestResultVarianceEqualTo" />
                </node>
                <node concept="2qgKlT" id="2FjKBCRpQdE" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="2FjKBCRpQxg" role="upBLP">
        <node concept="uGdhv" id="2FjKBCRpQAS" role="16NL0q">
          <node concept="3clFbS" id="2FjKBCRpQAU" role="2VODD2">
            <node concept="3clFbF" id="2FjKBCRpQJw" role="3cqZAp">
              <node concept="2OqwBi" id="2FjKBCRpRiC" role="3clFbG">
                <node concept="35c_gC" id="2FjKBCRpQJv" role="2Oq$k0">
                  <ref role="35c_gD" to="iyzu:2FjKBCRpzOI" resolve="TestResultVarianceEqualTo" />
                </node>
                <node concept="2qgKlT" id="2FjKBCRpRNw" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="2FjKBCRq65h">
    <property role="3GE5qa" value="base.data" />
    <ref role="1XX52x" to="iyzu:2FjKBCRoN3t" resolve="LaboratoryTestResultVariance" />
    <node concept="3EZMnI" id="2FjKBCRq65j" role="2wV5jI">
      <node concept="2iRfu4" id="2FjKBCRq65k" role="2iSdaV" />
      <node concept="B$lHz" id="2FjKBCRq65p" role="3EZMnx" />
      <node concept="3F1sOY" id="2FjKBCRq65v" role="3EZMnx">
        <property role="1$x2rV" value="&lt;&lt; ... &gt;&gt;" />
        <ref role="1NtTu8" to="iyzu:2FjKBCRpX2Y" resolve="testReference" />
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="2FjKBCRq8Gt">
    <property role="3GE5qa" value="base.data" />
    <ref role="aqKnT" to="iyzu:2FjKBCRoN3t" resolve="LaboratoryTestResultVariance" />
    <node concept="3eGOop" id="2FjKBCRq8Gu" role="3ft7WO">
      <node concept="ucgPf" id="2FjKBCRq8Gv" role="3aKz83">
        <node concept="3clFbS" id="2FjKBCRq8Gw" role="2VODD2">
          <node concept="3clFbF" id="2FjKBCRq8LX" role="3cqZAp">
            <node concept="2ShNRf" id="2FjKBCRq8LV" role="3clFbG">
              <node concept="2fJWfE" id="2FjKBCRq8Uw" role="2ShVmc">
                <node concept="3Tqbb2" id="2FjKBCRq8Uy" role="3zrR0E">
                  <ref role="ehGHo" to="iyzu:2FjKBCRoN3t" resolve="LaboratoryTestResultVariance" />
                </node>
                <node concept="1yR$tW" id="2FjKBCRq95B" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="2FjKBCRq9bg" role="upBLP">
        <node concept="uGdhv" id="2FjKBCRq9gV" role="16NeZM">
          <node concept="3clFbS" id="2FjKBCRq9gX" role="2VODD2">
            <node concept="3clFbF" id="2FjKBCRq9pz" role="3cqZAp">
              <node concept="2OqwBi" id="2FjKBCRq9Xo" role="3clFbG">
                <node concept="35c_gC" id="2FjKBCRq9py" role="2Oq$k0">
                  <ref role="35c_gD" to="iyzu:2FjKBCRoN3t" resolve="LaboratoryTestResultVariance" />
                </node>
                <node concept="2qgKlT" id="2FjKBCRqavh" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="2FjKBCRqaNd" role="upBLP">
        <node concept="uGdhv" id="2FjKBCRqaT5" role="16NL0q">
          <node concept="3clFbS" id="2FjKBCRqaT7" role="2VODD2">
            <node concept="3clFbF" id="2FjKBCRqb1H" role="3cqZAp">
              <node concept="2OqwBi" id="2FjKBCRqbyt" role="3clFbG">
                <node concept="35c_gC" id="2FjKBCRqb1G" role="2Oq$k0">
                  <ref role="35c_gD" to="iyzu:2FjKBCRoN3t" resolve="LaboratoryTestResultVariance" />
                </node>
                <node concept="2qgKlT" id="2FjKBCRqc4m" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="2FjKBCRqq3D">
    <property role="3GE5qa" value="base.literals.variance" />
    <ref role="aqKnT" to="iyzu:2FjKBCRoN3w" resolve="InsignificantTestResultVariance" />
    <node concept="3eGOop" id="2FjKBCRqq3E" role="3ft7WO">
      <node concept="ucgPf" id="2FjKBCRqq3F" role="3aKz83">
        <node concept="3clFbS" id="2FjKBCRqq3G" role="2VODD2">
          <node concept="3clFbF" id="2FjKBCRqq8T" role="3cqZAp">
            <node concept="2ShNRf" id="2FjKBCRqq8R" role="3clFbG">
              <node concept="2fJWfE" id="2FjKBCRqqhc" role="2ShVmc">
                <node concept="3Tqbb2" id="2FjKBCRqqhe" role="3zrR0E">
                  <ref role="ehGHo" to="iyzu:2FjKBCRoN3w" resolve="InsignificantTestResultVariance" />
                </node>
                <node concept="1yR$tW" id="2FjKBCRqqrN" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="2FjKBCRqqxc" role="upBLP">
        <node concept="uGdhv" id="2FjKBCRqqAB" role="16NeZM">
          <node concept="3clFbS" id="2FjKBCRqqAD" role="2VODD2">
            <node concept="3clFbF" id="2FjKBCRqqJf" role="3cqZAp">
              <node concept="2OqwBi" id="2FjKBCRqrfE" role="3clFbG">
                <node concept="35c_gC" id="2FjKBCRqqJe" role="2Oq$k0">
                  <ref role="35c_gD" to="iyzu:2FjKBCRoN3w" resolve="InsignificantTestResultVariance" />
                </node>
                <node concept="2qgKlT" id="2FjKBCRqrKy" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="2FjKBCRqs46" role="upBLP">
        <node concept="uGdhv" id="2FjKBCRqs9I" role="16NL0q">
          <node concept="3clFbS" id="2FjKBCRqs9K" role="2VODD2">
            <node concept="3clFbF" id="2FjKBCRqsim" role="3cqZAp">
              <node concept="2OqwBi" id="2FjKBCRqsML" role="3clFbG">
                <node concept="35c_gC" id="2FjKBCRqsil" role="2Oq$k0">
                  <ref role="35c_gD" to="iyzu:2FjKBCRoN3w" resolve="InsignificantTestResultVariance" />
                </node>
                <node concept="2qgKlT" id="2FjKBCRqtjD" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="2FjKBCRqty2">
    <property role="3GE5qa" value="base.literals.variance" />
    <ref role="aqKnT" to="iyzu:2FjKBCRoN3v" resolve="SignificantTestResultVariance" />
    <node concept="3eGOop" id="2FjKBCRqty3" role="3ft7WO">
      <node concept="ucgPf" id="2FjKBCRqty4" role="3aKz83">
        <node concept="3clFbS" id="2FjKBCRqty5" role="2VODD2">
          <node concept="3clFbF" id="2FjKBCRqtBi" role="3cqZAp">
            <node concept="2ShNRf" id="2FjKBCRqtBg" role="3clFbG">
              <node concept="2fJWfE" id="2FjKBCRqtJ_" role="2ShVmc">
                <node concept="3Tqbb2" id="2FjKBCRqtJB" role="3zrR0E">
                  <ref role="ehGHo" to="iyzu:2FjKBCRoN3v" resolve="SignificantTestResultVariance" />
                </node>
                <node concept="1yR$tW" id="2FjKBCRqtUG" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="2FjKBCRqu05" role="upBLP">
        <node concept="uGdhv" id="2FjKBCRqu5w" role="16NeZM">
          <node concept="3clFbS" id="2FjKBCRqu5y" role="2VODD2">
            <node concept="3clFbF" id="2FjKBCRque8" role="3cqZAp">
              <node concept="2OqwBi" id="2FjKBCRquIz" role="3clFbG">
                <node concept="35c_gC" id="2FjKBCRque7" role="2Oq$k0">
                  <ref role="35c_gD" to="iyzu:2FjKBCRoN3v" resolve="SignificantTestResultVariance" />
                </node>
                <node concept="2qgKlT" id="2FjKBCRqvfr" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="2FjKBCRqvyZ" role="upBLP">
        <node concept="uGdhv" id="2FjKBCRqvCB" role="16NL0q">
          <node concept="3clFbS" id="2FjKBCRqvCD" role="2VODD2">
            <node concept="3clFbF" id="2FjKBCRqvLf" role="3cqZAp">
              <node concept="2OqwBi" id="2FjKBCRqwhE" role="3clFbG">
                <node concept="35c_gC" id="2FjKBCRqvLe" role="2Oq$k0">
                  <ref role="35c_gD" to="iyzu:2FjKBCRoN3v" resolve="SignificantTestResultVariance" />
                </node>
                <node concept="2qgKlT" id="2FjKBCRqwMy" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="2FjKBCRq$It">
    <property role="3GE5qa" value="base.literals.variance" />
    <ref role="aqKnT" to="iyzu:2FjKBCRoN3u" resolve="TestResultVarianceLiteral" />
    <node concept="2VfDsV" id="2FjKBCRq$Iu" role="3ft7WO" />
  </node>
</model>

