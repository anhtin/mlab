<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:4775ac76-6570-4dc9-a115-fe361cc83f23(no.furst.mLab.decisions.tasks.autocommentation.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="6" />
    <devkit ref="78434eb8-b0e5-444b-850d-e7c4ad2da9ab(jetbrains.mps.devkit.aspect.structure)" />
  </languages>
  <imports>
    <import index="7f9y" ref="r:790ac37e-42d3-4dd3-977c-e8fe9cc56a45(no.uio.mLab.decisions.core.structure)" />
    <import index="vbok" ref="r:6cc208d3-a370-49ca-a6ac-ded97805a924(no.uio.mLab.shared.structure)" />
    <import index="jn6m" ref="r:8d706f6f-a342-4d4f-96d8-7be452515eda(no.uio.mLab.decisions.tasks.autocommentation.structure)" />
    <import index="nlc7" ref="r:9d3523f4-3478-4519-bfbb-caf8e981a9d2(no.furst.mLab.decisions.references.structure)" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
    <import index="ruww" ref="r:d7787915-5b9a-4713-a6ab-6345c5e1ec03(no.uio.mLab.decisions.references.laboratoryTest.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <property id="4628067390765956807" name="final" index="R5$K2" />
        <property id="4628067390765956802" name="abstract" index="R5$K7" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
        <child id="1071489727084" name="propertyDeclaration" index="1TKVEl" />
      </concept>
      <concept id="1169125989551" name="jetbrains.mps.lang.structure.structure.InterfaceConceptDeclaration" flags="ig" index="PlHQZ">
        <child id="1169127546356" name="extends" index="PrDN$" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288299" name="jetbrains.mps.lang.structure.structure.PropertyDeclaration" flags="ig" index="1TJgyi">
        <property id="241647608299431129" name="propertyId" index="IQ2nx" />
        <reference id="1082985295845" name="dataType" index="AX2Wp" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="1Hxyv4DRzM5">
    <property role="EcuMT" value="1973009780647541893" />
    <property role="TrG5h" value="CodedInterpretativeCommentForRequestor" />
    <property role="3GE5qa" value="base.parameters.interpretativeComment" />
    <ref role="1TJDcQ" to="jn6m:4QUW3ee0Jac" resolve="InterpretativeCommentForRequestor" />
    <node concept="1TJgyj" id="2TpFF5_2NuO" role="1TKVEi">
      <property role="IQ2ns" value="3339892675599611828" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="commentReference" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="nlc7:2TpFF5_2Hdd" resolve="CodedInterpretativeCommentForRequestorReference" />
    </node>
  </node>
  <node concept="PlHQZ" id="1Hxyv4EVACd">
    <property role="EcuMT" value="1973009780665379341" />
    <property role="TrG5h" value="ITranslatableFurstConcept" />
    <property role="3GE5qa" value="shared" />
    <node concept="PrWs8" id="1Hxyv4EVACo" role="PrDN$">
      <ref role="PrY4T" to="vbok:5Wfdz$0vc1Z" resolve="ITranslatableConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="7lYCqhv527q">
    <property role="EcuMT" value="8466382076844974554" />
    <property role="3GE5qa" value="base.actions" />
    <property role="TrG5h" value="AddStatisticalLabel" />
    <ref role="1TJDcQ" to="7f9y:1mAGFBKnGHq" resolve="Action" />
    <node concept="1TJgyi" id="7lYCqhv59wj" role="1TKVEl">
      <property role="IQ2nx" value="8466382076845004819" />
      <property role="TrG5h" value="label" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="PrWs8" id="7lYCqhv5aVv" role="PzmwI">
      <ref role="PrY4T" node="1Hxyv4EVACd" resolve="ITranslatableFurstConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="2FjKBCR5FI6">
    <property role="EcuMT" value="3086023999844957062" />
    <property role="3GE5qa" value="base.actions" />
    <property role="TrG5h" value="SuggestAdditionalTestToRequestor" />
    <ref role="1TJDcQ" to="7f9y:1mAGFBKnGHq" resolve="Action" />
    <node concept="1TJgyj" id="2FjKBCR5FI7" role="1TKVEi">
      <property role="IQ2ns" value="3086023999844957063" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="testReference" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="ruww:4QUW3efwB30" resolve="LaboratoryTestReference" />
    </node>
    <node concept="PrWs8" id="2FjKBCR5IGj" role="PzmwI">
      <ref role="PrY4T" node="1Hxyv4EVACd" resolve="ITranslatableFurstConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="2FjKBCRoN3t">
    <property role="EcuMT" value="3086023999849967837" />
    <property role="3GE5qa" value="base.data" />
    <property role="TrG5h" value="LaboratoryTestResultVariance" />
    <ref role="1TJDcQ" to="7f9y:1mAGFBLqAeU" resolve="NonBooleanDataValue" />
    <node concept="PrWs8" id="2FjKBCRpVey" role="PzmwI">
      <ref role="PrY4T" node="1Hxyv4EVACd" resolve="ITranslatableFurstConcept" />
    </node>
    <node concept="1TJgyj" id="2FjKBCRpX2Y" role="1TKVEi">
      <property role="IQ2ns" value="3086023999850270910" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="testReference" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="ruww:4QUW3efwB30" resolve="LaboratoryTestReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="2FjKBCRoN3u">
    <property role="EcuMT" value="3086023999849967838" />
    <property role="3GE5qa" value="base.literals.variance" />
    <property role="TrG5h" value="TestResultVarianceLiteral" />
    <ref role="1TJDcQ" to="7f9y:5Wfdz$0qdiH" resolve="Literal" />
  </node>
  <node concept="1TIwiD" id="2FjKBCRoN3v">
    <property role="EcuMT" value="3086023999849967839" />
    <property role="3GE5qa" value="base.literals.variance" />
    <property role="TrG5h" value="SignificantTestResultVariance" />
    <ref role="1TJDcQ" node="2FjKBCRoN3u" resolve="TestResultVarianceLiteral" />
    <node concept="PrWs8" id="2FjKBCRqfQ8" role="PzmwI">
      <ref role="PrY4T" node="1Hxyv4EVACd" resolve="ITranslatableFurstConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="2FjKBCRoN3w">
    <property role="EcuMT" value="3086023999849967840" />
    <property role="3GE5qa" value="base.literals.variance" />
    <property role="TrG5h" value="InsignificantTestResultVariance" />
    <ref role="1TJDcQ" node="2FjKBCRoN3u" resolve="TestResultVarianceLiteral" />
    <node concept="PrWs8" id="2FjKBCRqmh0" role="PzmwI">
      <ref role="PrY4T" node="1Hxyv4EVACd" resolve="ITranslatableFurstConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="2FjKBCRpzOH">
    <property role="EcuMT" value="3086023999850167597" />
    <property role="3GE5qa" value="base.constraints.variance" />
    <property role="TrG5h" value="TestResultVarianceConstraint" />
    <property role="R5$K7" value="true" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" to="7f9y:1mAGFBKqQs5" resolve="Constraint" />
  </node>
  <node concept="1TIwiD" id="2FjKBCRpzOI">
    <property role="EcuMT" value="3086023999850167598" />
    <property role="3GE5qa" value="base.constraints.variance" />
    <property role="TrG5h" value="TestResultVarianceEqualTo" />
    <ref role="1TJDcQ" node="2FjKBCRpzOH" resolve="TestResultVarianceConstraint" />
    <node concept="PrWs8" id="2FjKBCRpFMs" role="PzmwI">
      <ref role="PrY4T" node="1Hxyv4EVACd" resolve="ITranslatableFurstConcept" />
    </node>
    <node concept="1TJgyj" id="2FjKBCRpHpc" role="1TKVEi">
      <property role="IQ2ns" value="3086023999850206796" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="variance" />
      <ref role="20lvS9" node="2FjKBCRoN3u" resolve="TestResultVarianceLiteral" />
    </node>
  </node>
</model>

