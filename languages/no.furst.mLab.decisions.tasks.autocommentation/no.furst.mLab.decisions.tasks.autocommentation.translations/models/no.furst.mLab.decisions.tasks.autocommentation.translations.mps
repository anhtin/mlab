<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:004ad04f-2a63-408c-946c-fb0cd63b2840(no.furst.mLab.decisions.tasks.autocommentation.translations)">
  <persistence version="9" />
  <languages>
    <use id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage" version="6" />
  </languages>
  <imports>
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" />
    <import index="9igz" ref="r:0ba33904-c124-449b-bde4-640f55a71953(no.uio.mLab.shared.translations)" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1465982738277781862" name="jetbrains.mps.baseLanguage.structure.PlaceholderMember" flags="ng" index="2tJIrI" />
      <concept id="1188207840427" name="jetbrains.mps.baseLanguage.structure.AnnotationInstance" flags="nn" index="2AHcQZ">
        <reference id="1188208074048" name="annotation" index="2AI5Lk" />
      </concept>
      <concept id="1188208481402" name="jetbrains.mps.baseLanguage.structure.HasAnnotation" flags="ng" index="2AJDlI">
        <child id="1188208488637" name="annotation" index="2AJF6D" />
      </concept>
      <concept id="2820489544401957797" name="jetbrains.mps.baseLanguage.structure.DefaultClassCreator" flags="nn" index="HV5vD">
        <reference id="2820489544401957798" name="classifier" index="HV5vE" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1070462154015" name="jetbrains.mps.baseLanguage.structure.StaticFieldDeclaration" flags="ig" index="Wx3nA">
        <property id="6468716278899126575" name="isVolatile" index="2dlcS1" />
        <property id="6468716278899125786" name="isTransient" index="2dld4O" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1081236700938" name="jetbrains.mps.baseLanguage.structure.StaticMethodDeclaration" flags="ig" index="2YIFZL" />
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu">
        <property id="1221565133444" name="isFinal" index="1EXbeo" />
        <child id="1095933932569" name="implementedInterface" index="EKbjA" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <property id="1176718929932" name="isFinal" index="3TUv4t" />
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <property id="4276006055363816570" name="isSynchronized" index="od$2w" />
        <property id="1181808852946" name="isFinal" index="DiZV1" />
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123165" name="jetbrains.mps.baseLanguage.structure.InstanceMethodDeclaration" flags="ig" index="3clFb_">
        <property id="1178608670077" name="isAbstract" index="1EzhhJ" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1107461130800" name="jetbrains.mps.baseLanguage.structure.Classifier" flags="ng" index="3pOWGL">
        <child id="5375687026011219971" name="member" index="jymVt" unordered="true" />
      </concept>
      <concept id="7812454656619025416" name="jetbrains.mps.baseLanguage.structure.MethodDeclaration" flags="ng" index="1rXfSm">
        <property id="8355037393041754995" name="isNative" index="2aFKle" />
      </concept>
      <concept id="7812454656619025412" name="jetbrains.mps.baseLanguage.structure.LocalMethodCall" flags="nn" index="1rXfSq" />
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1107796713796" name="jetbrains.mps.baseLanguage.structure.Interface" flags="ig" index="3HP615" />
      <concept id="1163670490218" name="jetbrains.mps.baseLanguage.structure.SwitchStatement" flags="nn" index="3KaCP$">
        <child id="1163670592366" name="defaultBlock" index="3Kb1Dw" />
        <child id="1163670766145" name="expression" index="3KbGdf" />
        <child id="1163670772911" name="case" index="3KbHQx" />
      </concept>
      <concept id="1163670641947" name="jetbrains.mps.baseLanguage.structure.SwitchCase" flags="ng" index="3KbdKl">
        <child id="1163670677455" name="expression" index="3Kbmr1" />
        <child id="1163670683720" name="body" index="3Kbo56" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
      <concept id="1146644623116" name="jetbrains.mps.baseLanguage.structure.PrivateVisibility" flags="nn" index="3Tm6S6" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="312cEu" id="5ZQBr_XMo3g">
    <property role="TrG5h" value="FurstAutocommentationTaskTranslationProvider" />
    <property role="1EXbeo" value="true" />
    <property role="3GE5qa" value="internationalization" />
    <node concept="Wx3nA" id="5ZQBr_XMprO" role="jymVt">
      <property role="TrG5h" value="displayTranslations" />
      <property role="2dlcS1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="2dld4O" value="false" />
      <node concept="3uibUv" id="5ZQBr_XMprQ" role="1tU5fm">
        <ref role="3uigEE" node="5ZQBr_XMo3W" resolve="IFurstAutocommentationTaskTranslations" />
      </node>
      <node concept="3Tm1VV" id="5ZQBr_XMprR" role="1B3o_S" />
      <node concept="1rXfSq" id="5Wfdz$0v$Nz" role="33vP2m">
        <ref role="37wK5l" node="5Wfdz$0v$KX" resolve="init" />
        <node concept="10M0yZ" id="ofcHXfRkX0" role="37wK5m">
          <ref role="3cqZAo" to="9igz:5fXsrCVmzVm" resolve="displayLanguage" />
          <ref role="1PxDUh" to="9igz:5fXsrCVmxTN" resolve="TranslationConfiguration" />
        </node>
      </node>
    </node>
    <node concept="Wx3nA" id="1Hxyv4DUGoA" role="jymVt">
      <property role="TrG5h" value="generationTranslations" />
      <property role="2dlcS1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="2dld4O" value="false" />
      <node concept="3uibUv" id="1Hxyv4DUGoB" role="1tU5fm">
        <ref role="3uigEE" node="5ZQBr_XMo3W" resolve="IFurstAutocommentationTaskTranslations" />
      </node>
      <node concept="3Tm1VV" id="1Hxyv4DUGoC" role="1B3o_S" />
      <node concept="1rXfSq" id="1Hxyv4DUGoD" role="33vP2m">
        <ref role="37wK5l" node="5Wfdz$0v$KX" resolve="init" />
        <node concept="10M0yZ" id="ofcHXfRkXM" role="37wK5m">
          <ref role="3cqZAo" to="9igz:5fXsrCVmzVN" resolve="generationLanguage" />
          <ref role="1PxDUh" to="9igz:5fXsrCVmxTN" resolve="TranslationConfiguration" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="5fXsrCVpf1R" role="jymVt" />
    <node concept="2YIFZL" id="5Wfdz$0v$KX" role="jymVt">
      <property role="TrG5h" value="init" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3clFbS" id="5fXsrCVpf2J" role="3clF47">
        <node concept="3KaCP$" id="5fXsrCVpf3u" role="3cqZAp">
          <node concept="3KbdKl" id="5fXsrCVpfye" role="3KbHQx">
            <node concept="Xl_RD" id="5fXsrCVpfyH" role="3Kbmr1">
              <property role="Xl_RC" value="no" />
            </node>
            <node concept="3clFbS" id="5fXsrCVpfyg" role="3Kbo56">
              <node concept="3cpWs6" id="5fXsrCVpfzh" role="3cqZAp">
                <node concept="2ShNRf" id="5fXsrCVpf_6" role="3cqZAk">
                  <node concept="HV5vD" id="5fXsrCVpfEG" role="2ShVmc">
                    <ref role="HV5vE" node="5ZQBr_XMobA" resolve="NoFurstAutocommentationTaskTranslations" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="5fXsrCVpfFN" role="3Kb1Dw">
            <node concept="3cpWs6" id="5fXsrCVpfHd" role="3cqZAp">
              <node concept="2ShNRf" id="5fXsrCVpfJo" role="3cqZAk">
                <node concept="HV5vD" id="5fXsrCVpfPK" role="2ShVmc">
                  <ref role="HV5vE" node="17XAtu8bhIo" resolve="EnFurstAutocommentationTaskTranslations" />
                </node>
              </node>
            </node>
          </node>
          <node concept="37vLTw" id="1Hxyv4DUGmq" role="3KbGdf">
            <ref role="3cqZAo" node="1Hxyv4DUGgF" resolve="language" />
          </node>
        </node>
      </node>
      <node concept="3uibUv" id="5fXsrCVpf2z" role="3clF45">
        <ref role="3uigEE" node="5ZQBr_XMo3W" resolve="IFurstAutocommentationTaskTranslations" />
      </node>
      <node concept="3Tm6S6" id="5fXsrCVpf2i" role="1B3o_S" />
      <node concept="37vLTG" id="1Hxyv4DUGgF" role="3clF46">
        <property role="TrG5h" value="language" />
        <node concept="17QB3L" id="1Hxyv4DUGgE" role="1tU5fm" />
      </node>
    </node>
    <node concept="3Tm1VV" id="5ZQBr_XMo3h" role="1B3o_S" />
  </node>
  <node concept="3HP615" id="5ZQBr_XMo3W">
    <property role="TrG5h" value="IFurstAutocommentationTaskTranslations" />
    <property role="3GE5qa" value="internationalization" />
    <node concept="3clFb_" id="2FjKBCRpANp" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getTestResultVarianceAlias" />
      <node concept="3clFbS" id="2FjKBCRpANs" role="3clF47" />
      <node concept="3Tm1VV" id="2FjKBCRpANt" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCRpAJg" role="3clF45" />
    </node>
    <node concept="3clFb_" id="2FjKBCRpAPZ" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getTestResultVarianceDescription" />
      <node concept="3clFbS" id="2FjKBCRpAQ2" role="3clF47" />
      <node concept="3Tm1VV" id="2FjKBCRpAQ3" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCRpAPc" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="2FjKBCRpBen" role="jymVt" />
    <node concept="3clFb_" id="2FjKBCRpBgw" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getTestResultVarianceEqualToAlias" />
      <node concept="3clFbS" id="2FjKBCRpBgx" role="3clF47" />
      <node concept="3Tm1VV" id="2FjKBCRpBgy" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCRpBgz" role="3clF45" />
    </node>
    <node concept="3clFb_" id="2FjKBCRpBuV" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getTestResultVarianceEqualToDescription" />
      <node concept="3clFbS" id="2FjKBCRpBuW" role="3clF47" />
      <node concept="3Tm1VV" id="2FjKBCRpBuX" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCRpBuY" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="2FjKBCRpARy" role="jymVt" />
    <node concept="3clFb_" id="2FjKBCRpAW_" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getSignificantTestResultVarianceAlias" />
      <node concept="3clFbS" id="2FjKBCRpAWC" role="3clF47" />
      <node concept="3Tm1VV" id="2FjKBCRpAWD" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCRpASR" role="3clF45" />
    </node>
    <node concept="3clFb_" id="2FjKBCRpB24" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getSignificantTestResultVarianceDescription" />
      <node concept="3clFbS" id="2FjKBCRpB27" role="3clF47" />
      <node concept="3Tm1VV" id="2FjKBCRpB28" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCRpB10" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="2FjKBCRpBcY" role="jymVt" />
    <node concept="3clFb_" id="2FjKBCRpB7D" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getInsignificantTestResultVarianceAlias" />
      <node concept="3clFbS" id="2FjKBCRpB7E" role="3clF47" />
      <node concept="3Tm1VV" id="2FjKBCRpB7F" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCRpB7G" role="3clF45" />
    </node>
    <node concept="3clFb_" id="2FjKBCRpB5H" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getInsignificantTestResultVarianceDescription" />
      <node concept="3clFbS" id="2FjKBCRpB5K" role="3clF47" />
      <node concept="3Tm1VV" id="2FjKBCRpB5L" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCRpB51" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="2FjKBCRpAM7" role="jymVt" />
    <node concept="3clFb_" id="7lYCqhv5bry" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getAddStatisticalLabelAlias" />
      <node concept="3clFbS" id="7lYCqhv5br_" role="3clF47" />
      <node concept="3Tm1VV" id="7lYCqhv5brA" role="1B3o_S" />
      <node concept="17QB3L" id="7lYCqhv5brp" role="3clF45" />
    </node>
    <node concept="3clFb_" id="7lYCqhv5btz" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getAddStatisticalLabelDescription" />
      <node concept="3clFbS" id="7lYCqhv5btA" role="3clF47" />
      <node concept="3Tm1VV" id="7lYCqhv5btB" role="1B3o_S" />
      <node concept="17QB3L" id="7lYCqhv5bti" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="2FjKBCR5H$S" role="jymVt" />
    <node concept="3clFb_" id="2FjKBCR5H_N" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getSuggestAdditionalLaboratoryTestAlias" />
      <node concept="3clFbS" id="2FjKBCR5H_Q" role="3clF47" />
      <node concept="3Tm1VV" id="2FjKBCR5H_R" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCR5H_p" role="3clF45" />
    </node>
    <node concept="3clFb_" id="2FjKBCR5HBI" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getSuggestAdditionalLaboratoryTestDescription" />
      <node concept="3clFbS" id="2FjKBCR5HBL" role="3clF47" />
      <node concept="3Tm1VV" id="2FjKBCR5HBM" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCR5HBc" role="3clF45" />
    </node>
    <node concept="3Tm1VV" id="5ZQBr_XMo3X" role="1B3o_S" />
  </node>
  <node concept="312cEu" id="17XAtu8bhIo">
    <property role="TrG5h" value="EnFurstAutocommentationTaskTranslations" />
    <property role="3GE5qa" value="localizations" />
    <node concept="3clFb_" id="2FjKBCRpBW2" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getTestResultVarianceAlias" />
      <node concept="3Tm1VV" id="2FjKBCRpBW4" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCRpBW5" role="3clF45" />
      <node concept="3clFbS" id="2FjKBCRpBW6" role="3clF47">
        <node concept="3clFbF" id="2FjKBCRpCp3" role="3cqZAp">
          <node concept="Xl_RD" id="2FjKBCRpCp2" role="3clFbG">
            <property role="Xl_RC" value="test result variance" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="2FjKBCRpBW7" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="2FjKBCRpBW8" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getTestResultVarianceDescription" />
      <node concept="3Tm1VV" id="2FjKBCRpBWa" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCRpBWb" role="3clF45" />
      <node concept="3clFbS" id="2FjKBCRpBWc" role="3clF47">
        <node concept="3clFbF" id="2FjKBCRpCpW" role="3cqZAp">
          <node concept="Xl_RD" id="2FjKBCRpCpV" role="3clFbG">
            <property role="Xl_RC" value="variance between previous and current test result" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="2FjKBCRpBWd" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="2FjKBCRpCrC" role="jymVt" />
    <node concept="3clFb_" id="2FjKBCRpBWe" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getTestResultVarianceEqualToAlias" />
      <node concept="3Tm1VV" id="2FjKBCRpBWg" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCRpBWh" role="3clF45" />
      <node concept="3clFbS" id="2FjKBCRpBWi" role="3clF47">
        <node concept="3clFbF" id="2FjKBCRpCC_" role="3cqZAp">
          <node concept="Xl_RD" id="2FjKBCRpCC$" role="3clFbG">
            <property role="Xl_RC" value="=" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="2FjKBCRpBWj" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="2FjKBCRpBWk" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getTestResultVarianceEqualToDescription" />
      <node concept="3Tm1VV" id="2FjKBCRpBWm" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCRpBWn" role="3clF45" />
      <node concept="3clFbS" id="2FjKBCRpBWo" role="3clF47">
        <node concept="3clFbF" id="2FjKBCRpCDh" role="3cqZAp">
          <node concept="Xl_RD" id="2FjKBCRpCDg" role="3clFbG">
            <property role="Xl_RC" value="check that variance is equal to a category" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="2FjKBCRpBWp" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="2FjKBCRpCEX" role="jymVt" />
    <node concept="3clFb_" id="2FjKBCRpBWq" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getSignificantTestResultVarianceAlias" />
      <node concept="3Tm1VV" id="2FjKBCRpBWs" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCRpBWt" role="3clF45" />
      <node concept="3clFbS" id="2FjKBCRpBWu" role="3clF47">
        <node concept="3clFbF" id="2FjKBCRpCRZ" role="3cqZAp">
          <node concept="Xl_RD" id="2FjKBCRpCRY" role="3clFbG">
            <property role="Xl_RC" value="SIGNIFICANT" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="2FjKBCRpBWv" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="2FjKBCRpBWw" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getSignificantTestResultVarianceDescription" />
      <node concept="3Tm1VV" id="2FjKBCRpBWy" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCRpBWz" role="3clF45" />
      <node concept="3clFbS" id="2FjKBCRpBW$" role="3clF47">
        <node concept="3clFbF" id="2FjKBCRpCTi" role="3cqZAp">
          <node concept="Xl_RD" id="2FjKBCRpCTh" role="3clFbG">
            <property role="Xl_RC" value="" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="2FjKBCRpBW_" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="2FjKBCRpCTz" role="jymVt" />
    <node concept="3clFb_" id="2FjKBCRpBWA" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getInsignificantTestResultVarianceAlias" />
      <node concept="3Tm1VV" id="2FjKBCRpBWC" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCRpBWD" role="3clF45" />
      <node concept="3clFbS" id="2FjKBCRpBWE" role="3clF47">
        <node concept="3clFbF" id="2FjKBCRpD6E" role="3cqZAp">
          <node concept="Xl_RD" id="2FjKBCRpD6D" role="3clFbG">
            <property role="Xl_RC" value="INSIGNIFICANT" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="2FjKBCRpBWF" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="2FjKBCRpBWG" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getInsignificantTestResultVarianceDescription" />
      <node concept="3Tm1VV" id="2FjKBCRpBWI" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCRpBWJ" role="3clF45" />
      <node concept="3clFbS" id="2FjKBCRpBWK" role="3clF47">
        <node concept="3clFbF" id="2FjKBCRpD7X" role="3cqZAp">
          <node concept="Xl_RD" id="2FjKBCRpD7W" role="3clFbG">
            <property role="Xl_RC" value="" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="2FjKBCRpBWL" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="2FjKBCRpBRw" role="jymVt" />
    <node concept="3Tm1VV" id="17XAtu8bhIp" role="1B3o_S" />
    <node concept="3uibUv" id="17XAtu8bhJg" role="EKbjA">
      <ref role="3uigEE" node="5ZQBr_XMo3W" resolve="IFurstAutocommentationTaskTranslations" />
    </node>
    <node concept="3clFb_" id="7lYCqhv5bC1" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getAddStatisticalLabelAlias" />
      <node concept="3Tm1VV" id="7lYCqhv5bC3" role="1B3o_S" />
      <node concept="17QB3L" id="7lYCqhv5bC4" role="3clF45" />
      <node concept="3clFbS" id="7lYCqhv5bC5" role="3clF47">
        <node concept="3clFbF" id="7lYCqhv5bI7" role="3cqZAp">
          <node concept="Xl_RD" id="7lYCqhv5bI6" role="3clFbG">
            <property role="Xl_RC" value="add statistical label" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="7lYCqhv5bC6" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="7lYCqhv5bC7" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getAddStatisticalLabelDescription" />
      <node concept="3Tm1VV" id="7lYCqhv5bC9" role="1B3o_S" />
      <node concept="17QB3L" id="7lYCqhv5bCa" role="3clF45" />
      <node concept="3clFbS" id="7lYCqhv5bCb" role="3clF47">
        <node concept="3clFbF" id="7lYCqhv5bJ0" role="3cqZAp">
          <node concept="Xl_RD" id="7lYCqhv5bIZ" role="3clFbG">
            <property role="Xl_RC" value="add label to request for collecting analytics" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="7lYCqhv5bCc" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="2FjKBCR5HG5" role="jymVt" />
    <node concept="3clFb_" id="2FjKBCR5HIt" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getSuggestAdditionalLaboratoryTestAlias" />
      <node concept="3Tm1VV" id="2FjKBCR5HIv" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCR5HIw" role="3clF45" />
      <node concept="3clFbS" id="2FjKBCR5HIx" role="3clF47">
        <node concept="3clFbF" id="2FjKBCR5HO3" role="3cqZAp">
          <node concept="Xl_RD" id="2FjKBCR5HO2" role="3clFbG">
            <property role="Xl_RC" value="suggest test" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="2FjKBCR5HIy" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="2FjKBCR5HIz" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getSuggestAdditionalLaboratoryTestDescription" />
      <node concept="3Tm1VV" id="2FjKBCR5HI_" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCR5HIA" role="3clF45" />
      <node concept="3clFbS" id="2FjKBCR5HIB" role="3clF47">
        <node concept="3clFbF" id="2FjKBCR5HOJ" role="3cqZAp">
          <node concept="Xl_RD" id="2FjKBCR5HOI" role="3clFbG">
            <property role="Xl_RC" value="suggest additional test to the requestor" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="2FjKBCR5HIC" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="5ZQBr_XMobA">
    <property role="TrG5h" value="NoFurstAutocommentationTaskTranslations" />
    <property role="3GE5qa" value="localizations" />
    <node concept="3clFb_" id="2FjKBCRpDit" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getTestResultVarianceAlias" />
      <node concept="3Tm1VV" id="2FjKBCRpDiv" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCRpDiw" role="3clF45" />
      <node concept="3clFbS" id="2FjKBCRpDix" role="3clF47">
        <node concept="3clFbF" id="2FjKBCRpDJw" role="3cqZAp">
          <node concept="Xl_RD" id="2FjKBCRpDJv" role="3clFbG">
            <property role="Xl_RC" value="analysevarianse" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="2FjKBCRpDiy" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="2FjKBCRpDiz" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getTestResultVarianceDescription" />
      <node concept="3Tm1VV" id="2FjKBCRpDi_" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCRpDiA" role="3clF45" />
      <node concept="3clFbS" id="2FjKBCRpDiB" role="3clF47">
        <node concept="3clFbF" id="2FjKBCRpDL0" role="3cqZAp">
          <node concept="Xl_RD" id="2FjKBCRpDKZ" role="3clFbG">
            <property role="Xl_RC" value="variansen mellom forrige og nåværende analysesvar" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="2FjKBCRpDiC" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="2FjKBCRpDMi" role="jymVt" />
    <node concept="3clFb_" id="2FjKBCRpDiD" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getTestResultVarianceEqualToAlias" />
      <node concept="3Tm1VV" id="2FjKBCRpDiF" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCRpDiG" role="3clF45" />
      <node concept="3clFbS" id="2FjKBCRpDiH" role="3clF47">
        <node concept="3clFbF" id="2FjKBCRpDZg" role="3cqZAp">
          <node concept="Xl_RD" id="2FjKBCRpDZf" role="3clFbG">
            <property role="Xl_RC" value="=" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="2FjKBCRpDiI" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="2FjKBCRpDiJ" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getTestResultVarianceEqualToDescription" />
      <node concept="3Tm1VV" id="2FjKBCRpDiL" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCRpDiM" role="3clF45" />
      <node concept="3clFbS" id="2FjKBCRpDiN" role="3clF47">
        <node concept="3clFbF" id="2FjKBCRpDZW" role="3cqZAp">
          <node concept="Xl_RD" id="2FjKBCRpDZV" role="3clFbG">
            <property role="Xl_RC" value="sjekk at varianse er av en viss kategori" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="2FjKBCRpDiO" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="2FjKBCRpE2s" role="jymVt" />
    <node concept="3clFb_" id="2FjKBCRpDiP" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getSignificantTestResultVarianceAlias" />
      <node concept="3Tm1VV" id="2FjKBCRpDiR" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCRpDiS" role="3clF45" />
      <node concept="3clFbS" id="2FjKBCRpDiT" role="3clF47">
        <node concept="3clFbF" id="2FjKBCRpEfv" role="3cqZAp">
          <node concept="Xl_RD" id="2FjKBCRpEfu" role="3clFbG">
            <property role="Xl_RC" value="SIGNIFIKANT" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="2FjKBCRpDiU" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="2FjKBCRpDiV" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getSignificantTestResultVarianceDescription" />
      <node concept="3Tm1VV" id="2FjKBCRpDiX" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCRpDiY" role="3clF45" />
      <node concept="3clFbS" id="2FjKBCRpDiZ" role="3clF47">
        <node concept="3clFbF" id="2FjKBCRpEgM" role="3cqZAp">
          <node concept="Xl_RD" id="2FjKBCRpEgL" role="3clFbG" />
        </node>
      </node>
      <node concept="2AHcQZ" id="2FjKBCRpDj0" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="2FjKBCRpEih" role="jymVt" />
    <node concept="3clFb_" id="2FjKBCRpDj1" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getInsignificantTestResultVarianceAlias" />
      <node concept="3Tm1VV" id="2FjKBCRpDj3" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCRpDj4" role="3clF45" />
      <node concept="3clFbS" id="2FjKBCRpDj5" role="3clF47">
        <node concept="3clFbF" id="2FjKBCRpEvp" role="3cqZAp">
          <node concept="Xl_RD" id="2FjKBCRpEvo" role="3clFbG">
            <property role="Xl_RC" value="UBETYDELIG" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="2FjKBCRpDj6" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="2FjKBCRpDj7" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getInsignificantTestResultVarianceDescription" />
      <node concept="3Tm1VV" id="2FjKBCRpDj9" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCRpDja" role="3clF45" />
      <node concept="3clFbS" id="2FjKBCRpDjb" role="3clF47">
        <node concept="3clFbF" id="2FjKBCRpEwi" role="3cqZAp">
          <node concept="Xl_RD" id="2FjKBCRpEwh" role="3clFbG">
            <property role="Xl_RC" value="" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="2FjKBCRpDjc" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="2FjKBCRpDdU" role="jymVt" />
    <node concept="3Tm1VV" id="5ZQBr_XMobB" role="1B3o_S" />
    <node concept="3uibUv" id="5ZQBr_XMocM" role="EKbjA">
      <ref role="3uigEE" node="5ZQBr_XMo3W" resolve="IFurstAutocommentationTaskTranslations" />
    </node>
    <node concept="3clFb_" id="7lYCqhv5bTi" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getAddStatisticalLabelAlias" />
      <node concept="3Tm1VV" id="7lYCqhv5bTk" role="1B3o_S" />
      <node concept="17QB3L" id="7lYCqhv5bTl" role="3clF45" />
      <node concept="3clFbS" id="7lYCqhv5bTm" role="3clF47">
        <node concept="3clFbF" id="7lYCqhv5bZo" role="3cqZAp">
          <node concept="Xl_RD" id="7lYCqhv5bZn" role="3clFbG">
            <property role="Xl_RC" value="legg til autokode" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="7lYCqhv5bTn" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="7lYCqhv5c0g" role="jymVt" />
    <node concept="3clFb_" id="7lYCqhv5bTo" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getAddStatisticalLabelDescription" />
      <node concept="3Tm1VV" id="7lYCqhv5bTq" role="1B3o_S" />
      <node concept="17QB3L" id="7lYCqhv5bTr" role="3clF45" />
      <node concept="3clFbS" id="7lYCqhv5bTs" role="3clF47">
        <node concept="3clFbF" id="7lYCqhv5c2H" role="3cqZAp">
          <node concept="Xl_RD" id="7lYCqhv5c2G" role="3clFbG">
            <property role="Xl_RC" value="legg til autokode for innsamling av statistikk" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="7lYCqhv5bTt" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="2FjKBCR5HU6" role="jymVt" />
    <node concept="3clFb_" id="2FjKBCR5HWv" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getSuggestAdditionalLaboratoryTestAlias" />
      <node concept="3Tm1VV" id="2FjKBCR5HWx" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCR5HWy" role="3clF45" />
      <node concept="3clFbS" id="2FjKBCR5HWz" role="3clF47">
        <node concept="3clFbF" id="2FjKBCR5I26" role="3cqZAp">
          <node concept="Xl_RD" id="2FjKBCR5I25" role="3clFbG">
            <property role="Xl_RC" value="foreslå tilleggsanalyse" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="2FjKBCR5HW$" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="2FjKBCR5HW_" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getSuggestAdditionalLaboratoryTestDescription" />
      <node concept="3Tm1VV" id="2FjKBCR5HWB" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCR5HWC" role="3clF45" />
      <node concept="3clFbS" id="2FjKBCR5HWD" role="3clF47">
        <node concept="3clFbF" id="2FjKBCR5I40" role="3cqZAp">
          <node concept="Xl_RD" id="2FjKBCR5I3Z" role="3clFbG">
            <property role="Xl_RC" value="foreslå tilleggsanalyse til rekvirent" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="2FjKBCR5HWE" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
  </node>
</model>

