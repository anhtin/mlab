package no.furst.mLab.decisions.tasks.autocommentation.translations;

/*Generated by MPS */


public interface IFurstAutocommentationTaskTranslations {
  String getTestResultVarianceAlias();
  String getTestResultVarianceDescription();

  String getTestResultVarianceEqualToAlias();
  String getTestResultVarianceEqualToDescription();

  String getSignificantTestResultVarianceAlias();
  String getSignificantTestResultVarianceDescription();

  String getInsignificantTestResultVarianceAlias();
  String getInsignificantTestResultVarianceDescription();

  String getAddStatisticalLabelAlias();
  String getAddStatisticalLabelDescription();

  String getSuggestAdditionalLaboratoryTestAlias();
  String getSuggestAdditionalLaboratoryTestDescription();
}
