package no.furst.mLab.decisions.tasks.autocommentation.runtime;

/*Generated by MPS */

import no.uio.mLab.decisions.core.runtime.Action;
import java.util.Objects;

public class AddStatisticalLabel extends Action {
  private String label;

  public AddStatisticalLabel(String label) {
    this.label = label;
  }

  public String getLabel() {
    return this.label;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.getClass(), this.label);
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
      return true;
    }
    if (object == null || this.getClass() != object.getClass()) {
      return false;
    }
    AddStatisticalLabel other = (AddStatisticalLabel) object;
    return Objects.equals(this.label, other.label);
  }

  @Override
  public String toString() {
    return String.format("AddStatisticalLabel{label=%s}", this.label);
  }
}
