<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:e1c964e1-9386-404c-84c2-d30940486115(no.furst.mLab.entities.behavior)">
  <persistence version="9" />
  <languages>
    <use id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior" version="1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="1yj" ref="r:0c887d67-81ab-420f-9a35-a9f6bf96c975(no.uio.mLab.shared.behavior)" />
    <import index="y1qd" ref="r:afa259ed-3214-4c1d-8163-22436ed0d8f6(no.furst.mLab.entities.translations)" />
    <import index="vbok" ref="r:6cc208d3-a370-49ca-a6ac-ded97805a924(no.uio.mLab.shared.structure)" />
    <import index="9qnw" ref="r:2f6d4fa8-e4da-4f59-bd6f-30425e1dd68f(no.furst.mLab.entities.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior">
      <concept id="6496299201655527393" name="jetbrains.mps.lang.behavior.structure.LocalBehaviorMethodCall" flags="nn" index="BsUDl" />
      <concept id="1225194240794" name="jetbrains.mps.lang.behavior.structure.ConceptBehavior" flags="ng" index="13h7C7">
        <reference id="1225194240799" name="concept" index="13h7C2" />
        <child id="1225194240805" name="method" index="13h7CS" />
        <child id="1225194240801" name="constructor" index="13h7CW" />
      </concept>
      <concept id="1225194413805" name="jetbrains.mps.lang.behavior.structure.ConceptConstructorDeclaration" flags="in" index="13hLZK" />
      <concept id="1225194472830" name="jetbrains.mps.lang.behavior.structure.ConceptMethodDeclaration" flags="ng" index="13i0hz">
        <property id="5864038008284099149" name="isStatic" index="2Ki8OM" />
        <property id="1225194472832" name="isVirtual" index="13i0it" />
        <property id="1225194472834" name="isAbstract" index="13i0iv" />
        <reference id="1225194472831" name="overriddenMethod" index="13i0hy" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
      </concept>
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="13h7C7" id="6khVixxTe8I">
    <ref role="13h7C2" to="9qnw:1mAGFBLG5RX" resolve="Autokommentar" />
    <node concept="13hLZK" id="6khVixxTe8J" role="13h7CW">
      <node concept="3clFbS" id="6khVixxTe8K" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6khVixxTtdR" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="6khVixxTtdS" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixxTtdX" role="3clF47">
        <node concept="3clFbF" id="6khVixxTte2" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixxTtsG" role="3clFbG">
            <node concept="BsUDl" id="6khVixxTtlL" role="2Oq$k0">
              <ref role="37wK5l" node="6khVixxTsTh" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixxTtyt" role="2OqNvi">
              <ref role="37wK5l" to="y1qd:6khVixxTrEJ" resolve="getAutokommentarAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixxTtdY" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixxTte3" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="6khVixxTte4" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixxTte9" role="3clF47">
        <node concept="3clFbF" id="6khVixxTtBf" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixxTtBg" role="3clFbG">
            <node concept="BsUDl" id="6khVixxTtBh" role="2Oq$k0">
              <ref role="37wK5l" node="6khVixxTsTh" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixxTtBi" role="2OqNvi">
              <ref role="37wK5l" to="y1qd:6khVixxTrFB" resolve="getAutokommentarDescription" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixxTtea" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="6khVixxTsT6">
    <ref role="13h7C2" to="9qnw:6khVixxTt19" resolve="IFurstEntitiesTranslatableConcept" />
    <node concept="13i0hz" id="6khVixxTsTh" role="13h7CS">
      <property role="TrG5h" value="getDisplayTranslations" />
      <property role="2Ki8OM" value="true" />
      <node concept="3Tm1VV" id="6khVixxTsTi" role="1B3o_S" />
      <node concept="3uibUv" id="6khVixxTsUx" role="3clF45">
        <ref role="3uigEE" to="y1qd:5ZQBr_XMo3W" resolve="IFurstEntitiesTranslations" />
      </node>
      <node concept="3clFbS" id="6khVixxTsTk" role="3clF47">
        <node concept="3clFbF" id="6khVixxTsVh" role="3cqZAp">
          <node concept="10M0yZ" id="6khVixxTsVT" role="3clFbG">
            <ref role="3cqZAo" to="y1qd:5ZQBr_XMprO" resolve="displayTranslations" />
            <ref role="1PxDUh" to="y1qd:5ZQBr_XMo3g" resolve="FurstEntitiesTranslationProvider" />
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="6khVixxTsWu" role="13h7CS">
      <property role="TrG5h" value="getGenerationTranslations" />
      <property role="2Ki8OM" value="true" />
      <node concept="3Tm1VV" id="6khVixxTsWv" role="1B3o_S" />
      <node concept="3uibUv" id="6khVixxTsWw" role="3clF45">
        <ref role="3uigEE" to="y1qd:5ZQBr_XMo3W" resolve="IFurstEntitiesTranslations" />
      </node>
      <node concept="3clFbS" id="6khVixxTsWx" role="3clF47">
        <node concept="3clFbF" id="6khVixxTsWy" role="3cqZAp">
          <node concept="10M0yZ" id="6khVixxTsZ7" role="3clFbG">
            <ref role="3cqZAo" to="y1qd:1Hxyv4DUGoA" resolve="generationTranslations" />
            <ref role="1PxDUh" to="y1qd:5ZQBr_XMo3g" resolve="FurstEntitiesTranslationProvider" />
          </node>
        </node>
      </node>
    </node>
    <node concept="13hLZK" id="6khVixxTsT7" role="13h7CW">
      <node concept="3clFbS" id="6khVixxTsT8" role="2VODD2" />
    </node>
  </node>
</model>

