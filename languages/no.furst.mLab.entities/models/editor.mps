<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:6a66f5fa-6276-4d6a-9450-09d67d25c7c9(no.furst.mLab.entities.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="11" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports />
  <registry />
</model>

