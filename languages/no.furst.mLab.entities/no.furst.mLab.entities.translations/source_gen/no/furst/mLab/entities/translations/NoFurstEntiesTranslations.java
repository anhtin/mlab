package no.furst.mLab.entities.translations;

/*Generated by MPS */


public class NoFurstEntiesTranslations implements IFurstEntitiesTranslations {
  @Override
  public String getAutokommentarAlias() {
    return "autokommentar";
  }
  @Override
  public String getAutokommentarDescription() {
    return "kodet kommentar for automatisk kommentering";
  }
}
