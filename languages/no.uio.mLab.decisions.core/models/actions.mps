<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:64979d87-c309-4ab9-a53d-1b2d0e0d912e(no.uio.mLab.decisions.core.actions)">
  <persistence version="9" />
  <languages>
    <use id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions" version="4" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="7f9y" ref="r:790ac37e-42d3-4dd3-977c-e8fe9cc56a45(no.uio.mLab.decisions.core.structure)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="4836112446988635817" name="jetbrains.mps.baseLanguage.structure.UndefinedType" flags="in" index="2jxLKc" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6" />
      <concept id="5497648299878491908" name="jetbrains.mps.baseLanguage.structure.BaseVariableReference" flags="nn" index="1M0zk4">
        <reference id="5497648299878491909" name="baseVariableDeclaration" index="1M0zk5" />
      </concept>
    </language>
    <language id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions">
      <concept id="1158700664498" name="jetbrains.mps.lang.actions.structure.NodeFactories" flags="ng" index="37WguZ">
        <child id="1158700779049" name="nodeFactory" index="37WGs$" />
      </concept>
      <concept id="1158700725281" name="jetbrains.mps.lang.actions.structure.NodeFactory" flags="ig" index="37WvkG">
        <reference id="1158700943156" name="applicableConcept" index="37XkoT" />
        <child id="1158701448518" name="setupFunction" index="37ZfLb" />
      </concept>
      <concept id="1158701162220" name="jetbrains.mps.lang.actions.structure.NodeSetupFunction" flags="in" index="37Y9Zx" />
      <concept id="5584396657084912703" name="jetbrains.mps.lang.actions.structure.NodeSetupFunction_NewNode" flags="nn" index="1r4Lsj" />
      <concept id="5584396657084920413" name="jetbrains.mps.lang.actions.structure.NodeSetupFunction_SampleNode" flags="nn" index="1r4N5L" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1883223317721008708" name="jetbrains.mps.lang.smodel.structure.IfInstanceOfStatement" flags="nn" index="Jncv_">
        <reference id="1883223317721008712" name="nodeConcept" index="JncvD" />
        <child id="1883223317721008709" name="body" index="Jncv$" />
        <child id="1883223317721008711" name="variable" index="JncvA" />
        <child id="1883223317721008710" name="nodeExpression" index="JncvB" />
      </concept>
      <concept id="1883223317721008713" name="jetbrains.mps.lang.smodel.structure.IfInstanceOfVariable" flags="ng" index="JncvC" />
      <concept id="1883223317721107059" name="jetbrains.mps.lang.smodel.structure.IfInstanceOfVarReference" flags="nn" index="Jnkvi" />
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="540871147943773365" name="jetbrains.mps.baseLanguage.collections.structure.SingleArgumentSequenceOperation" flags="nn" index="25WWJ4">
        <child id="540871147943773366" name="argument" index="25WWJ7" />
      </concept>
      <concept id="1160612413312" name="jetbrains.mps.baseLanguage.collections.structure.AddElementOperation" flags="nn" index="TSZUe" />
      <concept id="1160666733551" name="jetbrains.mps.baseLanguage.collections.structure.AddAllElementsOperation" flags="nn" index="X8dFx" />
    </language>
  </registry>
  <node concept="37WguZ" id="1mAGFBJU2Dh">
    <property role="3GE5qa" value="base.conditions" />
    <property role="TrG5h" value="Not_NodeFactory" />
    <node concept="37WvkG" id="1mAGFBJU2Di" role="37WGs$">
      <ref role="37XkoT" to="7f9y:1mAGFBJTAhg" resolve="Not" />
      <node concept="37Y9Zx" id="1mAGFBJU2Dj" role="37ZfLb">
        <node concept="3clFbS" id="1mAGFBJU2Dk" role="2VODD2">
          <node concept="Jncv_" id="1mAGFBJU2Dv" role="3cqZAp">
            <ref role="JncvD" to="7f9y:5Wfdz$0ooZy" resolve="Condition" />
            <node concept="1r4N5L" id="1mAGFBJU2ET" role="JncvB" />
            <node concept="3clFbS" id="1mAGFBJU2Dx" role="Jncv$">
              <node concept="3clFbF" id="1mAGFBJU2FQ" role="3cqZAp">
                <node concept="37vLTI" id="1mAGFBJU3j_" role="3clFbG">
                  <node concept="Jnkvi" id="1mAGFBJU3o3" role="37vLTx">
                    <ref role="1M0zk5" node="1mAGFBJU2Dy" resolve="condition" />
                  </node>
                  <node concept="2OqwBi" id="1mAGFBJU2Og" role="37vLTJ">
                    <node concept="1r4Lsj" id="1mAGFBJU2FP" role="2Oq$k0" />
                    <node concept="3TrEf2" id="1mAGFBJU2Ye" role="2OqNvi">
                      <ref role="3Tt5mk" to="7f9y:1mAGFBJTAhh" resolve="condition" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="1mAGFBJU5gh" role="3cqZAp" />
            </node>
            <node concept="JncvC" id="1mAGFBJU2Dy" role="JncvA">
              <property role="TrG5h" value="condition" />
              <node concept="2jxLKc" id="1mAGFBJU2Dz" role="1tU5fm" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="37WguZ" id="1mAGFBK9SeW">
    <property role="3GE5qa" value="base.constraints.number.atomic" />
    <property role="TrG5h" value="AtomicNumber_NodeFactory" />
    <node concept="37WvkG" id="1mAGFBK9SeX" role="37WGs$">
      <ref role="37XkoT" to="7f9y:1mAGFBJ9goY" resolve="AtomicNumberConstraint" />
      <node concept="37Y9Zx" id="1mAGFBK9SeY" role="37ZfLb">
        <node concept="3clFbS" id="1mAGFBK9SeZ" role="2VODD2">
          <node concept="Jncv_" id="1mAGFBK9Sfa" role="3cqZAp">
            <ref role="JncvD" to="7f9y:1mAGFBJ9goY" resolve="AtomicNumberConstraint" />
            <node concept="1r4N5L" id="1mAGFBK9SfB" role="JncvB" />
            <node concept="3clFbS" id="1mAGFBK9Sfc" role="Jncv$">
              <node concept="3clFbF" id="1mAGFBK9Sim" role="3cqZAp">
                <node concept="37vLTI" id="1mAGFBK9SWS" role="3clFbG">
                  <node concept="2OqwBi" id="1mAGFBK9TcS" role="37vLTx">
                    <node concept="Jnkvi" id="1mAGFBK9T1_" role="2Oq$k0">
                      <ref role="1M0zk5" node="1mAGFBK9Sfd" resolve="constraint" />
                    </node>
                    <node concept="3TrEf2" id="1mAGFBK9Tr0" role="2OqNvi">
                      <ref role="3Tt5mk" to="7f9y:1mAGFBJ6NLB" resolve="number" />
                    </node>
                  </node>
                  <node concept="2OqwBi" id="1mAGFBK9Sry" role="37vLTJ">
                    <node concept="1r4Lsj" id="1mAGFBK9Sil" role="2Oq$k0" />
                    <node concept="3TrEf2" id="1mAGFBK9SB2" role="2OqNvi">
                      <ref role="3Tt5mk" to="7f9y:1mAGFBJ6NLB" resolve="number" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="1mAGFBK9TyK" role="3cqZAp" />
            </node>
            <node concept="JncvC" id="1mAGFBK9Sfd" role="JncvA">
              <property role="TrG5h" value="constraint" />
              <node concept="2jxLKc" id="1mAGFBK9Sfe" role="1tU5fm" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="37WguZ" id="1mAGFBKb1K5">
    <property role="3GE5qa" value="base.conditions" />
    <property role="TrG5h" value="CompositeCondition_NodeFactory" />
    <node concept="37WvkG" id="1mAGFBKb1K6" role="37WGs$">
      <ref role="37XkoT" to="7f9y:5Wfdz$0ooZz" resolve="CompositeCondition" />
      <node concept="37Y9Zx" id="1mAGFBKb1K7" role="37ZfLb">
        <node concept="3clFbS" id="1mAGFBKb1K8" role="2VODD2">
          <node concept="Jncv_" id="1mAGFBKb1Kj" role="3cqZAp">
            <ref role="JncvD" to="7f9y:5Wfdz$0ooZz" resolve="CompositeCondition" />
            <node concept="1r4N5L" id="1mAGFBKb1KK" role="JncvB" />
            <node concept="3clFbS" id="1mAGFBKb1Kl" role="Jncv$">
              <node concept="3clFbF" id="1mAGFBKb1LH" role="3cqZAp">
                <node concept="2OqwBi" id="1mAGFBKb5Ey" role="3clFbG">
                  <node concept="2OqwBi" id="1mAGFBKb1Uf" role="2Oq$k0">
                    <node concept="1r4Lsj" id="1mAGFBKb1LG" role="2Oq$k0" />
                    <node concept="3Tsc0h" id="1mAGFBKb24d" role="2OqNvi">
                      <ref role="3TtcxE" to="7f9y:5Wfdz$0ooZ$" resolve="conditions" />
                    </node>
                  </node>
                  <node concept="X8dFx" id="1mAGFBKb73Z" role="2OqNvi">
                    <node concept="2OqwBi" id="1mAGFBKbeWu" role="25WWJ7">
                      <node concept="Jnkvi" id="1mAGFBKb9Po" role="2Oq$k0">
                        <ref role="1M0zk5" node="1mAGFBKb1Km" resolve="composite" />
                      </node>
                      <node concept="3Tsc0h" id="1mAGFBKbhK8" role="2OqNvi">
                        <ref role="3TtcxE" to="7f9y:5Wfdz$0ooZ$" resolve="conditions" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="1mAGFBKbkUu" role="3cqZAp" />
            </node>
            <node concept="JncvC" id="1mAGFBKb1Km" role="JncvA">
              <property role="TrG5h" value="composite" />
              <node concept="2jxLKc" id="1mAGFBKb1Kn" role="1tU5fm" />
            </node>
          </node>
          <node concept="Jncv_" id="1mAGFBKcO5o" role="3cqZAp">
            <ref role="JncvD" to="7f9y:5Wfdz$0ooZy" resolve="Condition" />
            <node concept="1r4N5L" id="1mAGFBKcQ7V" role="JncvB" />
            <node concept="3clFbS" id="1mAGFBKcO5s" role="Jncv$">
              <node concept="3clFbF" id="1mAGFBKcS8Y" role="3cqZAp">
                <node concept="2OqwBi" id="1mAGFBKcU7k" role="3clFbG">
                  <node concept="2OqwBi" id="1mAGFBKcShw" role="2Oq$k0">
                    <node concept="1r4Lsj" id="1mAGFBKcS8X" role="2Oq$k0" />
                    <node concept="3Tsc0h" id="1mAGFBKcSrw" role="2OqNvi">
                      <ref role="3TtcxE" to="7f9y:5Wfdz$0ooZ$" resolve="conditions" />
                    </node>
                  </node>
                  <node concept="TSZUe" id="1mAGFBKcVwJ" role="2OqNvi">
                    <node concept="Jnkvi" id="1mAGFBKcVIz" role="25WWJ7">
                      <ref role="1M0zk5" node="1mAGFBKcO5u" resolve="condition" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="1mAGFBKcW7m" role="3cqZAp" />
            </node>
            <node concept="JncvC" id="1mAGFBKcO5u" role="JncvA">
              <property role="TrG5h" value="condition" />
              <node concept="2jxLKc" id="1mAGFBKcO5v" role="1tU5fm" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="37WguZ" id="4B5aqq2zQSA">
    <property role="3GE5qa" value="aspect.variables" />
    <property role="TrG5h" value="AspectVariable_NodeFactory" />
    <node concept="37WvkG" id="4B5aqq2zQSB" role="37WGs$">
      <ref role="37XkoT" to="7f9y:4QUW3edDXqS" resolve="AspectVariable" />
      <node concept="37Y9Zx" id="4B5aqq2zQSC" role="37ZfLb">
        <node concept="3clFbS" id="4B5aqq2zQSD" role="2VODD2">
          <node concept="Jncv_" id="4B5aqq2zQSO" role="3cqZAp">
            <ref role="JncvD" to="7f9y:4QUW3edDXqS" resolve="AspectVariable" />
            <node concept="1r4N5L" id="4B5aqq2zQTh" role="JncvB" />
            <node concept="3clFbS" id="4B5aqq2zQSQ" role="Jncv$">
              <node concept="3clFbF" id="4B5aqq2zQUe" role="3cqZAp">
                <node concept="37vLTI" id="4B5aqq2zRLe" role="3clFbG">
                  <node concept="2OqwBi" id="4B5aqq2zS4c" role="37vLTx">
                    <node concept="Jnkvi" id="4B5aqq2zRS9" role="2Oq$k0">
                      <ref role="1M0zk5" node="4B5aqq2zQSR" resolve="variable" />
                    </node>
                    <node concept="3TrcHB" id="4B5aqq2zSjS" role="2OqNvi">
                      <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                    </node>
                  </node>
                  <node concept="2OqwBi" id="4B5aqq2zR3W" role="37vLTJ">
                    <node concept="1r4Lsj" id="4B5aqq2zQUd" role="2Oq$k0" />
                    <node concept="3TrcHB" id="4B5aqq2zRgW" role="2OqNvi">
                      <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="JncvC" id="4B5aqq2zQSR" role="JncvA">
              <property role="TrG5h" value="variable" />
              <node concept="2jxLKc" id="4B5aqq2zQSS" role="1tU5fm" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="37WguZ" id="2FjKBCQczQo">
    <property role="3GE5qa" value="aspect.pointcuts.composite" />
    <property role="TrG5h" value="CompositePointcut_NodeFactory" />
    <node concept="37WvkG" id="2FjKBCQczQp" role="37WGs$">
      <ref role="37XkoT" to="7f9y:2FjKBCQ332Q" resolve="CompositePointcut" />
      <node concept="37Y9Zx" id="2FjKBCQczQq" role="37ZfLb">
        <node concept="3clFbS" id="2FjKBCQczQr" role="2VODD2">
          <node concept="Jncv_" id="2FjKBCQczR2" role="3cqZAp">
            <ref role="JncvD" to="7f9y:2FjKBCOu_xP" resolve="Pointcut" />
            <node concept="1r4N5L" id="2FjKBCQczRv" role="JncvB" />
            <node concept="3clFbS" id="2FjKBCQczR4" role="Jncv$">
              <node concept="3clFbF" id="2FjKBCQczSR" role="3cqZAp">
                <node concept="2OqwBi" id="2FjKBCQcAtM" role="3clFbG">
                  <node concept="2OqwBi" id="2FjKBCQc$2H" role="2Oq$k0">
                    <node concept="1r4Lsj" id="2FjKBCQczSQ" role="2Oq$k0" />
                    <node concept="3Tsc0h" id="2FjKBCQc$fH" role="2OqNvi">
                      <ref role="3TtcxE" to="7f9y:2FjKBCQ4hNj" resolve="pointcuts" />
                    </node>
                  </node>
                  <node concept="TSZUe" id="2FjKBCQcCcw" role="2OqNvi">
                    <node concept="Jnkvi" id="2FjKBCQcCrw" role="25WWJ7">
                      <ref role="1M0zk5" node="2FjKBCQczR5" resolve="pointcut" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="JncvC" id="2FjKBCQczR5" role="JncvA">
              <property role="TrG5h" value="pointcut" />
              <node concept="2jxLKc" id="2FjKBCQczR6" role="1tU5fm" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

