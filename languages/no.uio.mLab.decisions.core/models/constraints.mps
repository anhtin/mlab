<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:ac11bdd6-c85d-40e0-910f-d45067cda53d(no.uio.mLab.decisions.core.constraints)">
  <persistence version="9" />
  <languages>
    <use id="3f4bc5f5-c6c1-4a28-8b10-c83066ffa4a1" name="jetbrains.mps.lang.constraints" version="4" />
    <devkit ref="00000000-0000-4000-0000-5604ebd4f22c(jetbrains.mps.devkit.aspect.constraints)" />
  </languages>
  <imports>
    <import index="o8zo" ref="r:314576fc-3aee-4386-a0a5-a38348ac317d(jetbrains.mps.scope)" />
    <import index="7f9y" ref="r:790ac37e-42d3-4dd3-977c-e8fe9cc56a45(no.uio.mLab.decisions.core.structure)" implicit="true" />
    <import index="wb6c" ref="r:19d15037-83c1-4ae2-b66a-763828cb8279(no.uio.mLab.decisions.core.behavior)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="4836112446988635817" name="jetbrains.mps.baseLanguage.structure.UndefinedType" flags="in" index="2jxLKc" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123137" name="jetbrains.mps.baseLanguage.structure.BooleanConstant" flags="nn" index="3clFbT">
        <property id="1068580123138" name="value" index="3clFbU" />
      </concept>
      <concept id="1068581242875" name="jetbrains.mps.baseLanguage.structure.PlusExpression" flags="nn" index="3cpWs3" />
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="5497648299878491908" name="jetbrains.mps.baseLanguage.structure.BaseVariableReference" flags="nn" index="1M0zk4">
        <reference id="5497648299878491909" name="baseVariableDeclaration" index="1M0zk5" />
      </concept>
    </language>
    <language id="3f4bc5f5-c6c1-4a28-8b10-c83066ffa4a1" name="jetbrains.mps.lang.constraints">
      <concept id="6702802731807351367" name="jetbrains.mps.lang.constraints.structure.ConstraintFunction_CanBeAChild" flags="in" index="9S07l" />
      <concept id="6702802731807420587" name="jetbrains.mps.lang.constraints.structure.ConstraintFunction_CanBeAParent" flags="ig" index="9SLcT" />
      <concept id="1202989658459" name="jetbrains.mps.lang.constraints.structure.ConstraintFunctionParameter_parentNode" flags="nn" index="nLn13" />
      <concept id="4303308395523096213" name="jetbrains.mps.lang.constraints.structure.ConstraintFunctionParameter_childConcept" flags="ng" index="2DD5aU" />
      <concept id="6738154313879680265" name="jetbrains.mps.lang.constraints.structure.ConstraintFunctionParameter_childNode" flags="nn" index="2H4GUG" />
      <concept id="8401916545537438642" name="jetbrains.mps.lang.constraints.structure.InheritedNodeScopeFactory" flags="ng" index="1dDu$B">
        <reference id="8401916545537438643" name="kind" index="1dDu$A" />
      </concept>
      <concept id="1213093968558" name="jetbrains.mps.lang.constraints.structure.ConceptConstraints" flags="ng" index="1M2fIO">
        <reference id="1213093996982" name="concept" index="1M2myG" />
        <child id="6702802731807532712" name="canBeParent" index="9SGkU" />
        <child id="6702802731807737306" name="canBeChild" index="9Vyp8" />
        <child id="1213100494875" name="referent" index="1Mr941" />
      </concept>
      <concept id="1148687176410" name="jetbrains.mps.lang.constraints.structure.NodeReferentConstraint" flags="ng" index="1N5Pfh">
        <reference id="1148687202698" name="applicableLink" index="1N5Vy1" />
        <child id="1148687345559" name="searchScopeFactory" index="1N6uqs" />
      </concept>
    </language>
    <language id="760a0a8c-eabb-4521-8bfd-65db761a9ba3" name="jetbrains.mps.baseLanguage.logging">
      <concept id="6332851714983831325" name="jetbrains.mps.baseLanguage.logging.structure.MsgStatement" flags="ng" index="2xdQw9">
        <property id="6332851714983843871" name="severity" index="2xdLsb" />
        <child id="5721587534047265374" name="message" index="9lYJi" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="7453996997717780434" name="jetbrains.mps.lang.smodel.structure.Node_GetSConceptOperation" flags="nn" index="2yIwOk" />
      <concept id="2396822768958367367" name="jetbrains.mps.lang.smodel.structure.AbstractTypeCastExpression" flags="nn" index="$5XWr">
        <child id="6733348108486823193" name="leftExpression" index="1m5AlR" />
        <child id="3906496115198199033" name="conceptArgument" index="3oSUPX" />
      </concept>
      <concept id="1761385620274348152" name="jetbrains.mps.lang.smodel.structure.SConceptTypeCastExpression" flags="nn" index="2CBFar" />
      <concept id="1883223317721008708" name="jetbrains.mps.lang.smodel.structure.IfInstanceOfStatement" flags="nn" index="Jncv_">
        <reference id="1883223317721008712" name="nodeConcept" index="JncvD" />
        <child id="1883223317721008709" name="body" index="Jncv$" />
        <child id="1883223317721008711" name="variable" index="JncvA" />
        <child id="1883223317721008710" name="nodeExpression" index="JncvB" />
      </concept>
      <concept id="1883223317721008713" name="jetbrains.mps.lang.smodel.structure.IfInstanceOfVariable" flags="ng" index="JncvC" />
      <concept id="1883223317721107059" name="jetbrains.mps.lang.smodel.structure.IfInstanceOfVarReference" flags="nn" index="Jnkvi" />
      <concept id="1180031783296" name="jetbrains.mps.lang.smodel.structure.Concept_IsSubConceptOfOperation" flags="nn" index="2Zo12i">
        <child id="1180031783297" name="conceptArgument" index="2Zo12j" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1M2fIO" id="1mAGFBK7hUY">
    <property role="3GE5qa" value="base.conditions" />
    <ref role="1M2myG" to="7f9y:1mAGFBJL9Gh" resolve="NamedConditionReference" />
    <node concept="1N5Pfh" id="1mAGFBK7hUZ" role="1Mr941">
      <ref role="1N5Vy1" to="7f9y:1mAGFBJL9Gi" resolve="target" />
      <node concept="1dDu$B" id="1mAGFBK7hV1" role="1N6uqs">
        <ref role="1dDu$A" to="7f9y:1mAGFBJeyRH" resolve="NamedCondition" />
      </node>
    </node>
  </node>
  <node concept="1M2fIO" id="1mAGFBKNv4C">
    <property role="3GE5qa" value="base.constraints" />
    <ref role="1M2myG" to="7f9y:1mAGFBKqQs5" resolve="Constraint" />
    <node concept="9S07l" id="1mAGFBKNv4D" role="9Vyp8">
      <node concept="3clFbS" id="1mAGFBKNv4E" role="2VODD2">
        <node concept="Jncv_" id="1mAGFBKNvbW" role="3cqZAp">
          <ref role="JncvD" to="7f9y:1mAGFBJRO8A" resolve="NonBooleanDataValueCondition" />
          <node concept="nLn13" id="1mAGFBKNvjw" role="JncvB" />
          <node concept="3clFbS" id="1mAGFBKNvbY" role="Jncv$">
            <node concept="3cpWs6" id="1mAGFBKNyfn" role="3cqZAp">
              <node concept="2OqwBi" id="1mAGFBKOSTz" role="3cqZAk">
                <node concept="2qgKlT" id="1mAGFBKOTi0" role="2OqNvi">
                  <ref role="37wK5l" to="wb6c:1mAGFBKNuSW" resolve="isAllowedToConstrain" />
                  <node concept="2OqwBi" id="1mAGFBKOUIB" role="37wK5m">
                    <node concept="2OqwBi" id="1mAGFBKOTNQ" role="2Oq$k0">
                      <node concept="Jnkvi" id="1mAGFBKOTtM" role="2Oq$k0">
                        <ref role="1M0zk5" node="1mAGFBKNvbZ" resolve="condition" />
                      </node>
                      <node concept="3TrEf2" id="1mAGFBLrD8k" role="2OqNvi">
                        <ref role="3Tt5mk" to="7f9y:1mAGFBLqAeR" resolve="data" />
                      </node>
                    </node>
                    <node concept="2yIwOk" id="1mAGFBKOVad" role="2OqNvi" />
                  </node>
                </node>
                <node concept="2CBFar" id="4QUW3ed96zx" role="2Oq$k0">
                  <node concept="chp4Y" id="4QUW3ed987t" role="3oSUPX">
                    <ref role="cht4Q" to="7f9y:1mAGFBKqQs5" resolve="Constraint" />
                  </node>
                  <node concept="2DD5aU" id="4QUW3ed96gV" role="1m5AlR" />
                </node>
              </node>
            </node>
          </node>
          <node concept="JncvC" id="1mAGFBKNvbZ" role="JncvA">
            <property role="TrG5h" value="condition" />
            <node concept="2jxLKc" id="1mAGFBKNvc0" role="1tU5fm" />
          </node>
        </node>
        <node concept="3clFbF" id="1mAGFBKNB8P" role="3cqZAp">
          <node concept="3clFbT" id="1mAGFBKNB8O" role="3clFbG">
            <property role="3clFbU" value="false" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="1M2fIO" id="65epL7MCgJk">
    <property role="3GE5qa" value="aspect.patterns.aspect.stereotypes" />
    <ref role="1M2myG" to="7f9y:65epL7Mprkt" resolve="ISelectedEntityPattern" />
    <node concept="9SLcT" id="65epL7MCgJo" role="9SGkU">
      <node concept="3clFbS" id="65epL7MCgJp" role="2VODD2">
        <node concept="2xdQw9" id="65epL7MIHsv" role="3cqZAp">
          <property role="2xdLsb" value="debug" />
          <node concept="3cpWs3" id="65epL7MIHRC" role="9lYJi">
            <node concept="Xl_RD" id="65epL7MIHsx" role="3uHU7B" />
            <node concept="2H4GUG" id="65epL7MIJFW" role="3uHU7w" />
          </node>
        </node>
        <node concept="3clFbF" id="65epL7MCgQG" role="3cqZAp">
          <node concept="2OqwBi" id="65epL7MCh8v" role="3clFbG">
            <node concept="2DD5aU" id="65epL7MCgQF" role="2Oq$k0" />
            <node concept="2Zo12i" id="65epL7MChob" role="2OqNvi">
              <node concept="chp4Y" id="65epL7MCh_n" role="2Zo12j">
                <ref role="cht4Q" to="7f9y:6LTgXmMvpq9" resolve="IEntityReference" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

