<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:1cc0fc09-a07c-4fad-b141-cb5e2d2d74ec(no.uio.mLab.decisions.core.intentions)">
  <persistence version="9" />
  <languages>
    <use id="d7a92d38-f7db-40d0-8431-763b0c3c9f20" name="jetbrains.mps.lang.intentions" version="0" />
    <use id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions" version="4" />
    <use id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel" version="11" />
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="11" />
    <use id="13744753-c81f-424a-9c1b-cf8943bf4e86" name="jetbrains.mps.lang.sharedConcepts" version="0" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="7f9y" ref="r:790ac37e-42d3-4dd3-977c-e8fe9cc56a45(no.uio.mLab.decisions.core.structure)" />
    <import index="wb6c" ref="r:19d15037-83c1-4ae2-b66a-763828cb8279(no.uio.mLab.decisions.core.behavior)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
    <import index="1yj" ref="r:0c887d67-81ab-420f-9a35-a9f6bf96c975(no.uio.mLab.shared.behavior)" implicit="true" />
  </imports>
  <registry>
    <language id="13744753-c81f-424a-9c1b-cf8943bf4e86" name="jetbrains.mps.lang.sharedConcepts">
      <concept id="1194033889146" name="jetbrains.mps.lang.sharedConcepts.structure.ConceptFunctionParameter_editorContext" flags="nn" index="1XNTG" />
    </language>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="4510086454722552739" name="jetbrains.mps.lang.editor.structure.PropertyDeclarationCellSelector" flags="ng" index="eBIwv">
        <reference id="4510086454740628767" name="propertyDeclaration" index="fyFUz" />
      </concept>
      <concept id="3647146066980922272" name="jetbrains.mps.lang.editor.structure.SelectInEditorOperation" flags="nn" index="1OKiuA">
        <child id="1948540814633499358" name="editorContext" index="lBI5i" />
        <child id="1948540814635895774" name="cellSelector" index="lGT1i" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1082485599095" name="jetbrains.mps.baseLanguage.structure.BlockStatement" flags="nn" index="9aQIb">
        <child id="1082485599096" name="statements" index="9aQI4" />
      </concept>
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="4836112446988635817" name="jetbrains.mps.baseLanguage.structure.UndefinedType" flags="in" index="2jxLKc" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070534058343" name="jetbrains.mps.baseLanguage.structure.NullLiteral" flags="nn" index="10Nm6u" />
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1082485599094" name="ifFalseStatement" index="9aQIa" />
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1081516740877" name="jetbrains.mps.baseLanguage.structure.NotExpression" flags="nn" index="3fqX7Q">
        <child id="1081516765348" name="expression" index="3fr31v" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="5497648299878491908" name="jetbrains.mps.baseLanguage.structure.BaseVariableReference" flags="nn" index="1M0zk4">
        <reference id="5497648299878491909" name="baseVariableDeclaration" index="1M0zk5" />
      </concept>
      <concept id="6329021646629104957" name="jetbrains.mps.baseLanguage.structure.TextCommentPart" flags="nn" index="3SKdUq">
        <property id="6329021646629104958" name="text" index="3SKdUp" />
      </concept>
      <concept id="6329021646629104954" name="jetbrains.mps.baseLanguage.structure.SingleLineComment" flags="nn" index="3SKdUt">
        <child id="6329021646629175155" name="commentPart" index="3SKWNk" />
      </concept>
    </language>
    <language id="fd392034-7849-419d-9071-12563d152375" name="jetbrains.mps.baseLanguage.closures">
      <concept id="1199569711397" name="jetbrains.mps.baseLanguage.closures.structure.ClosureLiteral" flags="nn" index="1bVj0M">
        <child id="1199569906740" name="parameter" index="1bW2Oz" />
        <child id="1199569916463" name="body" index="1bW5cS" />
      </concept>
    </language>
    <language id="d7a92d38-f7db-40d0-8431-763b0c3c9f20" name="jetbrains.mps.lang.intentions">
      <concept id="1192794744107" name="jetbrains.mps.lang.intentions.structure.IntentionDeclaration" flags="ig" index="2S6QgY" />
      <concept id="1192794782375" name="jetbrains.mps.lang.intentions.structure.DescriptionBlock" flags="in" index="2S6ZIM" />
      <concept id="1192795771125" name="jetbrains.mps.lang.intentions.structure.IsApplicableBlock" flags="in" index="2SaL7w" />
      <concept id="1192795911897" name="jetbrains.mps.lang.intentions.structure.ExecuteBlock" flags="in" index="2Sbjvc" />
      <concept id="1192796902958" name="jetbrains.mps.lang.intentions.structure.ConceptFunctionParameter_node" flags="nn" index="2Sf5sV" />
      <concept id="2522969319638091381" name="jetbrains.mps.lang.intentions.structure.BaseIntentionDeclaration" flags="ig" index="2ZfUlf">
        <property id="2522969319638091386" name="isAvailableInChildNodes" index="2ZfUl0" />
        <reference id="2522969319638198290" name="forConcept" index="2ZfgGC" />
        <child id="2522969319638198291" name="executeFunction" index="2ZfgGD" />
        <child id="2522969319638093995" name="isApplicableFunction" index="2ZfVeh" />
        <child id="2522969319638093993" name="descriptionFunction" index="2ZfVej" />
      </concept>
      <concept id="1240316299033" name="jetbrains.mps.lang.intentions.structure.QueryBlock" flags="in" index="38BcoT">
        <child id="1240393479918" name="paramType" index="3ddBve" />
      </concept>
      <concept id="1240322627579" name="jetbrains.mps.lang.intentions.structure.IntentionParameter" flags="nn" index="38Zlrr" />
      <concept id="1240395258925" name="jetbrains.mps.lang.intentions.structure.ParameterizedIntentionDeclaration" flags="ig" index="3dkpOd">
        <child id="1240395532443" name="queryBlock" index="3dlsAV" />
      </concept>
    </language>
    <language id="3a13115c-633c-4c5c-bbcc-75c4219e9555" name="jetbrains.mps.lang.quotation">
      <concept id="5455284157994012186" name="jetbrains.mps.lang.quotation.structure.NodeBuilderInitLink" flags="ng" index="2pIpSj">
        <reference id="5455284157994012188" name="link" index="2pIpSl" />
      </concept>
      <concept id="5455284157993911077" name="jetbrains.mps.lang.quotation.structure.NodeBuilderInitProperty" flags="ng" index="2pJxcG">
        <reference id="5455284157993911078" name="property" index="2pJxcJ" />
      </concept>
      <concept id="5455284157993911097" name="jetbrains.mps.lang.quotation.structure.NodeBuilderInitPart" flags="ng" index="2pJxcK">
        <child id="5455284157993911094" name="expression" index="2pJxcZ" />
      </concept>
      <concept id="5455284157993863837" name="jetbrains.mps.lang.quotation.structure.NodeBuilder" flags="nn" index="2pJPEk">
        <child id="5455284157993863838" name="quotedNode" index="2pJPEn" />
      </concept>
      <concept id="5455284157993863840" name="jetbrains.mps.lang.quotation.structure.NodeBuilderNode" flags="nn" index="2pJPED">
        <reference id="5455284157993910961" name="concept" index="2pJxaS" />
        <child id="5455284157993911099" name="values" index="2pJxcM" />
      </concept>
      <concept id="8182547171709752110" name="jetbrains.mps.lang.quotation.structure.NodeBuilderExpression" flags="nn" index="36biLy">
        <child id="8182547171709752112" name="expression" index="36biLW" />
      </concept>
    </language>
    <language id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions">
      <concept id="7776141288922801652" name="jetbrains.mps.lang.actions.structure.NF_Concept_NewInstance" flags="nn" index="q_SaT" />
      <concept id="767145758118872828" name="jetbrains.mps.lang.actions.structure.NF_Node_ReplaceWithNewOperation" flags="nn" index="2DeJnW" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1204834851141" name="jetbrains.mps.lang.smodel.structure.PoundExpression" flags="ng" index="25Kdxt">
        <child id="1204834868751" name="expression" index="25KhWn" />
      </concept>
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1138411891628" name="jetbrains.mps.lang.smodel.structure.SNodeOperation" flags="nn" index="eCIE_">
        <child id="1144104376918" name="parameter" index="1xVPHs" />
      </concept>
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="7453996997717780434" name="jetbrains.mps.lang.smodel.structure.Node_GetSConceptOperation" flags="nn" index="2yIwOk" />
      <concept id="1883223317721008708" name="jetbrains.mps.lang.smodel.structure.IfInstanceOfStatement" flags="nn" index="Jncv_">
        <reference id="1883223317721008712" name="nodeConcept" index="JncvD" />
        <child id="1883223317721008709" name="body" index="Jncv$" />
        <child id="1883223317721008711" name="variable" index="JncvA" />
        <child id="1883223317721008710" name="nodeExpression" index="JncvB" />
      </concept>
      <concept id="1883223317721008713" name="jetbrains.mps.lang.smodel.structure.IfInstanceOfVariable" flags="ng" index="JncvC" />
      <concept id="1883223317721107059" name="jetbrains.mps.lang.smodel.structure.IfInstanceOfVarReference" flags="nn" index="Jnkvi" />
      <concept id="1171305280644" name="jetbrains.mps.lang.smodel.structure.Node_GetDescendantsOperation" flags="nn" index="2Rf3mk" />
      <concept id="1171407110247" name="jetbrains.mps.lang.smodel.structure.Node_GetAncestorOperation" flags="nn" index="2Xjw5R" />
      <concept id="1227264722563" name="jetbrains.mps.lang.smodel.structure.EqualsStructurallyExpression" flags="nn" index="2YFouu" />
      <concept id="1180031783296" name="jetbrains.mps.lang.smodel.structure.Concept_IsSubConceptOfOperation" flags="nn" index="2Zo12i">
        <child id="1180031783297" name="conceptArgument" index="2Zo12j" />
      </concept>
      <concept id="2644386474300074836" name="jetbrains.mps.lang.smodel.structure.ConceptIdRefExpression" flags="nn" index="35c_gC">
        <reference id="2644386474300074837" name="conceptDeclaration" index="35c_gD" />
      </concept>
      <concept id="6677504323281689838" name="jetbrains.mps.lang.smodel.structure.SConceptType" flags="in" index="3bZ5Sz">
        <reference id="6677504323281689839" name="conceptDeclaraton" index="3bZ5Sy" />
      </concept>
      <concept id="1139613262185" name="jetbrains.mps.lang.smodel.structure.Node_GetParentOperation" flags="nn" index="1mfA1w" />
      <concept id="1139621453865" name="jetbrains.mps.lang.smodel.structure.Node_IsInstanceOfOperation" flags="nn" index="1mIQ4w">
        <child id="1177027386292" name="conceptArgument" index="cj9EA" />
      </concept>
      <concept id="1172008320231" name="jetbrains.mps.lang.smodel.structure.Node_IsNotNullOperation" flags="nn" index="3x8VRR" />
      <concept id="1144101972840" name="jetbrains.mps.lang.smodel.structure.OperationParm_Concept" flags="ng" index="1xMEDy">
        <child id="1207343664468" name="conceptArgument" index="ri$Ld" />
      </concept>
      <concept id="1144146199828" name="jetbrains.mps.lang.smodel.structure.Node_CopyOperation" flags="nn" index="1$rogu" />
      <concept id="1139867745658" name="jetbrains.mps.lang.smodel.structure.Node_ReplaceWithNewOperation" flags="nn" index="1_qnLN">
        <reference id="1139867957129" name="concept" index="1_rbq0" />
      </concept>
      <concept id="1172326502327" name="jetbrains.mps.lang.smodel.structure.Concept_IsExactlyOperation" flags="nn" index="3O6GUB">
        <child id="1206733650006" name="conceptArgument" index="3QVz_e" />
      </concept>
      <concept id="1140131837776" name="jetbrains.mps.lang.smodel.structure.Node_ReplaceWithAnotherOperation" flags="nn" index="1P9Npp">
        <child id="1140131861877" name="replacementNode" index="1P9ThW" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1204796164442" name="jetbrains.mps.baseLanguage.collections.structure.InternalSequenceOperation" flags="nn" index="23sCx2">
        <child id="1204796294226" name="closure" index="23t8la" />
      </concept>
      <concept id="540871147943773365" name="jetbrains.mps.baseLanguage.collections.structure.SingleArgumentSequenceOperation" flags="nn" index="25WWJ4">
        <child id="540871147943773366" name="argument" index="25WWJ7" />
      </concept>
      <concept id="1204980550705" name="jetbrains.mps.baseLanguage.collections.structure.VisitAllOperation" flags="nn" index="2es0OD" />
      <concept id="1237721394592" name="jetbrains.mps.baseLanguage.collections.structure.AbstractContainerCreator" flags="nn" index="HWqM0">
        <child id="1237721435808" name="initValue" index="HW$Y0" />
        <child id="1237721435807" name="elementType" index="HW$YZ" />
      </concept>
      <concept id="1203518072036" name="jetbrains.mps.baseLanguage.collections.structure.SmartClosureParameterDeclaration" flags="ig" index="Rh6nW" />
      <concept id="1160600644654" name="jetbrains.mps.baseLanguage.collections.structure.ListCreatorWithInit" flags="nn" index="Tc6Ow" />
      <concept id="1160612413312" name="jetbrains.mps.baseLanguage.collections.structure.AddElementOperation" flags="nn" index="TSZUe" />
      <concept id="1202120902084" name="jetbrains.mps.baseLanguage.collections.structure.WhereOperation" flags="nn" index="3zZkjj" />
    </language>
  </registry>
  <node concept="2S6QgY" id="1mAGFBJfIH9">
    <property role="3GE5qa" value="base.constraints.number" />
    <property role="TrG5h" value="SwapLowerLimit_Intention" />
    <property role="2ZfUl0" value="true" />
    <ref role="2ZfgGC" to="7f9y:PDjyzkfMCr" resolve="IRangeConstraint" />
    <node concept="2S6ZIM" id="1mAGFBJfIHa" role="2ZfVej">
      <node concept="3clFbS" id="1mAGFBJfIHb" role="2VODD2">
        <node concept="3clFbJ" id="1mAGFBJkzTU" role="3cqZAp">
          <node concept="3clFbS" id="1mAGFBJkzTV" role="3clFbx">
            <node concept="3cpWs6" id="1mAGFBJkzTW" role="3cqZAp">
              <node concept="2OqwBi" id="1mAGFBJkdfT" role="3cqZAk">
                <node concept="2OqwBi" id="1mAGFBJkctb" role="2Oq$k0">
                  <node concept="2Sf5sV" id="1mAGFBJkcdJ" role="2Oq$k0" />
                  <node concept="2yIwOk" id="1mAGFBJkcLb" role="2OqNvi" />
                </node>
                <node concept="2qgKlT" id="PDjyzkiGgP" role="2OqNvi">
                  <ref role="37wK5l" to="wb6c:PDjyzkfNMD" resolve="getCloseLowerIntentionDescription" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="1mAGFBJkzU2" role="3clFbw">
            <node concept="2Sf5sV" id="1mAGFBJkzU3" role="2Oq$k0" />
            <node concept="3TrcHB" id="PDjyzkl8zq" role="2OqNvi">
              <ref role="3TsBF5" to="7f9y:PDjyzkfOPG" resolve="isLowerOpen" />
            </node>
          </node>
          <node concept="9aQIb" id="1mAGFBJkzU5" role="9aQIa">
            <node concept="3clFbS" id="1mAGFBJkzU6" role="9aQI4">
              <node concept="3cpWs6" id="1mAGFBJkzU7" role="3cqZAp">
                <node concept="2OqwBi" id="1mAGFBJkzU8" role="3cqZAk">
                  <node concept="2OqwBi" id="1mAGFBJkzU9" role="2Oq$k0">
                    <node concept="2Sf5sV" id="1mAGFBJkzUa" role="2Oq$k0" />
                    <node concept="2yIwOk" id="1mAGFBJkzUb" role="2OqNvi" />
                  </node>
                  <node concept="2qgKlT" id="PDjyzkiGHK" role="2OqNvi">
                    <ref role="37wK5l" to="wb6c:PDjyzkfNMT" resolve="getOpenLowerIntentionDescription" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2Sbjvc" id="1mAGFBJfIHc" role="2ZfgGD">
      <node concept="3clFbS" id="1mAGFBJfIHd" role="2VODD2">
        <node concept="3clFbF" id="1mAGFBJkfT2" role="3cqZAp">
          <node concept="37vLTI" id="1mAGFBJkgH1" role="3clFbG">
            <node concept="3fqX7Q" id="1mAGFBJkAIY" role="37vLTx">
              <node concept="2OqwBi" id="1mAGFBJkBgC" role="3fr31v">
                <node concept="2Sf5sV" id="1mAGFBJkB3c" role="2Oq$k0" />
                <node concept="3TrcHB" id="PDjyzkl9fy" role="2OqNvi">
                  <ref role="3TsBF5" to="7f9y:PDjyzkfOPG" resolve="isLowerOpen" />
                </node>
              </node>
            </node>
            <node concept="2OqwBi" id="1mAGFBJkg2a" role="37vLTJ">
              <node concept="2Sf5sV" id="1mAGFBJkfT1" role="2Oq$k0" />
              <node concept="3TrcHB" id="PDjyzkl8Zz" role="2OqNvi">
                <ref role="3TsBF5" to="7f9y:PDjyzkfOPG" resolve="isLowerOpen" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="2S6QgY" id="1mAGFBJkgO7">
    <property role="3GE5qa" value="base.constraints.number" />
    <property role="TrG5h" value="SwapUpperLimit_Intention" />
    <property role="2ZfUl0" value="true" />
    <ref role="2ZfgGC" to="7f9y:PDjyzkfMCr" resolve="IRangeConstraint" />
    <node concept="2S6ZIM" id="1mAGFBJkgO8" role="2ZfVej">
      <node concept="3clFbS" id="1mAGFBJkgO9" role="2VODD2">
        <node concept="3clFbJ" id="1mAGFBJkkTO" role="3cqZAp">
          <node concept="3clFbS" id="1mAGFBJkkTQ" role="3clFbx">
            <node concept="3cpWs6" id="1mAGFBJkmej" role="3cqZAp">
              <node concept="2OqwBi" id="1mAGFBJkhZd" role="3cqZAk">
                <node concept="2OqwBi" id="1mAGFBJkhcz" role="2Oq$k0">
                  <node concept="2Sf5sV" id="1mAGFBJkgX5" role="2Oq$k0" />
                  <node concept="2yIwOk" id="1mAGFBJkhwv" role="2OqNvi" />
                </node>
                <node concept="2qgKlT" id="PDjyzkiExu" role="2OqNvi">
                  <ref role="37wK5l" to="wb6c:PDjyzkfNML" resolve="getCloseUpperIntentionDescription" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="1mAGFBJkln1" role="3clFbw">
            <node concept="2Sf5sV" id="1mAGFBJkl7g" role="2Oq$k0" />
            <node concept="3TrcHB" id="PDjyzkl9MO" role="2OqNvi">
              <ref role="3TsBF5" to="7f9y:PDjyzkfOPI" resolve="isUpperOpen" />
            </node>
          </node>
          <node concept="9aQIb" id="1mAGFBJklYY" role="9aQIa">
            <node concept="3clFbS" id="1mAGFBJklYZ" role="9aQI4">
              <node concept="3cpWs6" id="1mAGFBJkoVy" role="3cqZAp">
                <node concept="2OqwBi" id="1mAGFBJkrBr" role="3cqZAk">
                  <node concept="2OqwBi" id="1mAGFBJkpVP" role="2Oq$k0">
                    <node concept="2Sf5sV" id="1mAGFBJkpb8" role="2Oq$k0" />
                    <node concept="2yIwOk" id="1mAGFBJkqmz" role="2OqNvi" />
                  </node>
                  <node concept="2qgKlT" id="PDjyzkiEYp" role="2OqNvi">
                    <ref role="37wK5l" to="wb6c:PDjyzkfNN1" resolve="getOpenUpperIntentionDescription" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2Sbjvc" id="1mAGFBJkgOa" role="2ZfgGD">
      <node concept="3clFbS" id="1mAGFBJkgOb" role="2VODD2">
        <node concept="3clFbF" id="1mAGFBJktxq" role="3cqZAp">
          <node concept="37vLTI" id="1mAGFBJkvi1" role="3clFbG">
            <node concept="3fqX7Q" id="1mAGFBJkvkS" role="37vLTx">
              <node concept="2OqwBi" id="1mAGFBJkvwr" role="3fr31v">
                <node concept="2Sf5sV" id="1mAGFBJkvkZ" role="2Oq$k0" />
                <node concept="3TrcHB" id="PDjyzklauW" role="2OqNvi">
                  <ref role="3TsBF5" to="7f9y:PDjyzkfOPI" resolve="isUpperOpen" />
                </node>
              </node>
            </node>
            <node concept="2OqwBi" id="1mAGFBJktEy" role="37vLTJ">
              <node concept="2Sf5sV" id="1mAGFBJktxp" role="2Oq$k0" />
              <node concept="3TrcHB" id="PDjyzklaeX" role="2OqNvi">
                <ref role="3TsBF5" to="7f9y:PDjyzkfOPI" resolve="isUpperOpen" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="2S6QgY" id="1mAGFBJLHw2">
    <property role="3GE5qa" value="base.conditions" />
    <property role="TrG5h" value="Negate_Condition_Intention" />
    <property role="2ZfUl0" value="true" />
    <ref role="2ZfgGC" to="7f9y:5Wfdz$0ooZy" resolve="Condition" />
    <node concept="2S6ZIM" id="1mAGFBJLHw3" role="2ZfVej">
      <node concept="3clFbS" id="1mAGFBJLHw4" role="2VODD2">
        <node concept="3clFbF" id="1mAGFBJLHD5" role="3cqZAp">
          <node concept="2OqwBi" id="1mAGFBJP48I" role="3clFbG">
            <node concept="2OqwBi" id="1mAGFBJP3sI" role="2Oq$k0">
              <node concept="2Sf5sV" id="1mAGFBJP3eQ" role="2Oq$k0" />
              <node concept="2yIwOk" id="1mAGFBJP3HE" role="2OqNvi" />
            </node>
            <node concept="2qgKlT" id="1mAGFBJP4V8" role="2OqNvi">
              <ref role="37wK5l" to="wb6c:1mAGFBJP2Nx" resolve="getInvertIntentionDescription" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2Sbjvc" id="1mAGFBJLHw5" role="2ZfgGD">
      <node concept="3clFbS" id="1mAGFBJLHw6" role="2VODD2">
        <node concept="Jncv_" id="1mAGFBJTZbq" role="3cqZAp">
          <ref role="JncvD" to="7f9y:1mAGFBJTAhg" resolve="Not" />
          <node concept="2Sf5sV" id="1mAGFBJTZcs" role="JncvB" />
          <node concept="3clFbS" id="1mAGFBJTZbu" role="Jncv$">
            <node concept="3clFbF" id="1mAGFBJTZhq" role="3cqZAp">
              <node concept="2OqwBi" id="1mAGFBJTZpe" role="3clFbG">
                <node concept="2Sf5sV" id="1mAGFBJTZhp" role="2Oq$k0" />
                <node concept="1P9Npp" id="1mAGFBJTZxW" role="2OqNvi">
                  <node concept="2OqwBi" id="1mAGFBJU01g" role="1P9ThW">
                    <node concept="Jnkvi" id="1mAGFBJTZRU" role="2Oq$k0">
                      <ref role="1M0zk5" node="1mAGFBJTZbw" resolve="not" />
                    </node>
                    <node concept="3TrEf2" id="1mAGFBJU0cq" role="2OqNvi">
                      <ref role="3Tt5mk" to="7f9y:1mAGFBJTAhh" resolve="condition" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3cpWs6" id="1mAGFBJU3Uu" role="3cqZAp" />
          </node>
          <node concept="JncvC" id="1mAGFBJTZbw" role="JncvA">
            <property role="TrG5h" value="not" />
            <node concept="2jxLKc" id="1mAGFBJTZbx" role="1tU5fm" />
          </node>
        </node>
        <node concept="Jncv_" id="1mAGFBJU0wa" role="3cqZAp">
          <ref role="JncvD" to="7f9y:1mAGFBJTAhg" resolve="Not" />
          <node concept="2OqwBi" id="1mAGFBJU0Gr" role="JncvB">
            <node concept="2Sf5sV" id="1mAGFBJU0$8" role="2Oq$k0" />
            <node concept="1mfA1w" id="1mAGFBJU0Pb" role="2OqNvi" />
          </node>
          <node concept="3clFbS" id="1mAGFBJU0we" role="Jncv$">
            <node concept="3clFbF" id="1mAGFBJU0U8" role="3cqZAp">
              <node concept="2OqwBi" id="1mAGFBJU1bV" role="3clFbG">
                <node concept="Jnkvi" id="1mAGFBJU13e" role="2Oq$k0">
                  <ref role="1M0zk5" node="1mAGFBJU0wg" resolve="not" />
                </node>
                <node concept="1P9Npp" id="1mAGFBJU1mC" role="2OqNvi">
                  <node concept="2Sf5sV" id="1mAGFBJU1oS" role="1P9ThW" />
                </node>
              </node>
            </node>
            <node concept="3cpWs6" id="1mAGFBJU3Y2" role="3cqZAp" />
          </node>
          <node concept="JncvC" id="1mAGFBJU0wg" role="JncvA">
            <property role="TrG5h" value="not" />
            <node concept="2jxLKc" id="1mAGFBJU0wh" role="1tU5fm" />
          </node>
        </node>
        <node concept="3clFbF" id="1mAGFBJNzju" role="3cqZAp">
          <node concept="2OqwBi" id="1mAGFBJTWrt" role="3clFbG">
            <node concept="2Sf5sV" id="1mAGFBJTWjD" role="2Oq$k0" />
            <node concept="2DeJnW" id="1mAGFBJU7A7" role="2OqNvi">
              <ref role="1_rbq0" to="7f9y:1mAGFBJTAhg" resolve="Not" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="2S6QgY" id="1mAGFBKe1zW">
    <property role="3GE5qa" value="base.conditions.composites" />
    <property role="TrG5h" value="Invert_CompositeCondition_Intention" />
    <ref role="2ZfgGC" to="7f9y:5Wfdz$0ooZz" resolve="CompositeCondition" />
    <node concept="2S6ZIM" id="1mAGFBKe1zX" role="2ZfVej">
      <node concept="3clFbS" id="1mAGFBKe1zY" role="2VODD2">
        <node concept="3clFbJ" id="1mAGFBKe4vM" role="3cqZAp">
          <node concept="2OqwBi" id="1mAGFBKe4R5" role="3clFbw">
            <node concept="2Sf5sV" id="1mAGFBKe4Cv" role="2Oq$k0" />
            <node concept="1mIQ4w" id="1mAGFBKe59w" role="2OqNvi">
              <node concept="chp4Y" id="1mAGFBKe5ks" role="cj9EA">
                <ref role="cht4Q" to="7f9y:5Wfdz$0ooZB" resolve="AllOf" />
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="1mAGFBKe4vO" role="3clFbx">
            <node concept="3cpWs6" id="1mAGFBKejyk" role="3cqZAp">
              <node concept="2OqwBi" id="1mAGFBKehUH" role="3cqZAk">
                <node concept="2OqwBi" id="1mAGFBKeh1R" role="2Oq$k0">
                  <node concept="2Sf5sV" id="1mAGFBKegM7" role="2Oq$k0" />
                  <node concept="2yIwOk" id="1mAGFBKehpp" role="2OqNvi" />
                </node>
                <node concept="2qgKlT" id="1mAGFBKeiP7" role="2OqNvi">
                  <ref role="37wK5l" to="wb6c:1mAGFBKegjs" resolve="getChangeToAnyofIntentionDescription" />
                </node>
              </node>
            </node>
          </node>
          <node concept="9aQIb" id="1mAGFBKek2i" role="9aQIa">
            <node concept="3clFbS" id="1mAGFBKek2j" role="9aQI4">
              <node concept="3cpWs6" id="1mAGFBKekkm" role="3cqZAp">
                <node concept="2OqwBi" id="1mAGFBKekAA" role="3cqZAk">
                  <node concept="2OqwBi" id="1mAGFBKekAB" role="2Oq$k0">
                    <node concept="2Sf5sV" id="1mAGFBKekAC" role="2Oq$k0" />
                    <node concept="2yIwOk" id="1mAGFBKekAD" role="2OqNvi" />
                  </node>
                  <node concept="2qgKlT" id="1mAGFBKel7e" role="2OqNvi">
                    <ref role="37wK5l" to="wb6c:1mAGFBKe6Pr" resolve="getChangeToAllOfIntentionDescription" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2Sbjvc" id="1mAGFBKe1zZ" role="2ZfgGD">
      <node concept="3clFbS" id="1mAGFBKe1$0" role="2VODD2">
        <node concept="3clFbJ" id="1mAGFBKelpJ" role="3cqZAp">
          <node concept="2OqwBi" id="1mAGFBKel$z" role="3clFbw">
            <node concept="2Sf5sV" id="1mAGFBKelqa" role="2Oq$k0" />
            <node concept="1mIQ4w" id="1mAGFBKelIG" role="2OqNvi">
              <node concept="chp4Y" id="1mAGFBKelLm" role="cj9EA">
                <ref role="cht4Q" to="7f9y:5Wfdz$0ooZB" resolve="AllOf" />
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="1mAGFBKelpL" role="3clFbx">
            <node concept="3clFbF" id="1mAGFBKelQL" role="3cqZAp">
              <node concept="2OqwBi" id="1mAGFBKelZf" role="3clFbG">
                <node concept="2Sf5sV" id="1mAGFBKelQK" role="2Oq$k0" />
                <node concept="2DeJnW" id="1mAGFBKem9r" role="2OqNvi">
                  <ref role="1_rbq0" to="7f9y:5Wfdz$0ooZA" resolve="AnyOf" />
                </node>
              </node>
            </node>
          </node>
          <node concept="9aQIb" id="1mAGFBKemcc" role="9aQIa">
            <node concept="3clFbS" id="1mAGFBKemcd" role="9aQI4">
              <node concept="3clFbF" id="1mAGFBKemhI" role="3cqZAp">
                <node concept="2OqwBi" id="1mAGFBKemqc" role="3clFbG">
                  <node concept="2Sf5sV" id="1mAGFBKemhH" role="2Oq$k0" />
                  <node concept="2DeJnW" id="1mAGFBKem$o" role="2OqNvi">
                    <ref role="1_rbq0" to="7f9y:5Wfdz$0ooZB" resolve="AllOf" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3dkpOd" id="1mAGFBKevXs">
    <property role="3GE5qa" value="base.conditions" />
    <property role="TrG5h" value="SurroundWith_Condition_Intention" />
    <property role="2ZfUl0" value="true" />
    <ref role="2ZfgGC" to="7f9y:5Wfdz$0ooZy" resolve="Condition" />
    <node concept="2S6ZIM" id="1mAGFBKevXt" role="2ZfVej">
      <node concept="3clFbS" id="1mAGFBKevXu" role="2VODD2">
        <node concept="3clFbJ" id="1mAGFBKe$Xv" role="3cqZAp">
          <node concept="2OqwBi" id="1mAGFBKeB4b" role="3clFbw">
            <node concept="38Zlrr" id="1mAGFBKe_av" role="2Oq$k0" />
            <node concept="3O6GUB" id="1mAGFBKeBv5" role="2OqNvi">
              <node concept="chp4Y" id="1mAGFBKeBIe" role="3QVz_e">
                <ref role="cht4Q" to="7f9y:5Wfdz$0ooZB" resolve="AllOf" />
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="1mAGFBKe$Xx" role="3clFbx">
            <node concept="3cpWs6" id="1mAGFBKeBY4" role="3cqZAp">
              <node concept="2OqwBi" id="1mAGFBKeDdJ" role="3cqZAk">
                <node concept="2OqwBi" id="1mAGFBKeCrf" role="2Oq$k0">
                  <node concept="2Sf5sV" id="1mAGFBKeCdc" role="2Oq$k0" />
                  <node concept="2yIwOk" id="1mAGFBKeCMy" role="2OqNvi" />
                </node>
                <node concept="2qgKlT" id="1mAGFBKeDEQ" role="2OqNvi">
                  <ref role="37wK5l" to="wb6c:1mAGFBKcoOC" resolve="getSurroundWithAllOfIntentionDescription" />
                </node>
              </node>
            </node>
          </node>
          <node concept="9aQIb" id="1mAGFBKeDYp" role="9aQIa">
            <node concept="3clFbS" id="1mAGFBKeDYq" role="9aQI4">
              <node concept="3cpWs6" id="1mAGFBKeEhS" role="3cqZAp">
                <node concept="2OqwBi" id="1mAGFBKeGmr" role="3cqZAk">
                  <node concept="2OqwBi" id="1mAGFBKeES$" role="2Oq$k0">
                    <node concept="2Sf5sV" id="1mAGFBKeEi4" role="2Oq$k0" />
                    <node concept="2yIwOk" id="1mAGFBKeFks" role="2OqNvi" />
                  </node>
                  <node concept="2qgKlT" id="1mAGFBKeGRT" role="2OqNvi">
                    <ref role="37wK5l" to="wb6c:1mAGFBKcp5c" resolve="getSurroundWithAnyOfIntentionDescription" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2Sbjvc" id="1mAGFBKevXv" role="2ZfgGD">
      <node concept="3clFbS" id="1mAGFBKevXw" role="2VODD2">
        <node concept="3cpWs8" id="1mAGFBKePwr" role="3cqZAp">
          <node concept="3cpWsn" id="1mAGFBKePwu" role="3cpWs9">
            <property role="TrG5h" value="newNode" />
            <node concept="3Tqbb2" id="1mAGFBKePwq" role="1tU5fm">
              <ref role="ehGHo" to="7f9y:5Wfdz$0ooZz" resolve="CompositeCondition" />
            </node>
            <node concept="2OqwBi" id="1mAGFBKf$Lv" role="33vP2m">
              <node concept="2Sf5sV" id="1mAGFBKf$ti" role="2Oq$k0" />
              <node concept="1P9Npp" id="1mAGFBKf_73" role="2OqNvi">
                <node concept="2OqwBi" id="1mAGFBKf_MA" role="1P9ThW">
                  <node concept="38Zlrr" id="1mAGFBKf_m7" role="2Oq$k0" />
                  <node concept="q_SaT" id="1mAGFBKfA$Z" role="2OqNvi" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="1mAGFBKfii9" role="3cqZAp">
          <node concept="2OqwBi" id="1mAGFBKfkkL" role="3clFbG">
            <node concept="2OqwBi" id="1mAGFBKfiuL" role="2Oq$k0">
              <node concept="37vLTw" id="1mAGFBKfii7" role="2Oq$k0">
                <ref role="3cqZAo" node="1mAGFBKePwu" resolve="newNode" />
              </node>
              <node concept="3Tsc0h" id="1mAGFBKfiCU" role="2OqNvi">
                <ref role="3TtcxE" to="7f9y:5Wfdz$0ooZ$" resolve="conditions" />
              </node>
            </node>
            <node concept="TSZUe" id="1mAGFBKflIn" role="2OqNvi">
              <node concept="2Sf5sV" id="1mAGFBKflUn" role="25WWJ7" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="38BcoT" id="1mAGFBKew6k" role="3dlsAV">
      <node concept="3clFbS" id="1mAGFBKew6l" role="2VODD2">
        <node concept="3clFbF" id="1mAGFBKfJVB" role="3cqZAp">
          <node concept="2ShNRf" id="1mAGFBKfJV_" role="3clFbG">
            <node concept="Tc6Ow" id="1mAGFBKfKHf" role="2ShVmc">
              <node concept="35c_gC" id="1mAGFBKfMtM" role="HW$Y0">
                <ref role="35c_gD" to="7f9y:5Wfdz$0ooZB" resolve="AllOf" />
              </node>
              <node concept="35c_gC" id="1mAGFBKfOZs" role="HW$Y0">
                <ref role="35c_gD" to="7f9y:5Wfdz$0ooZA" resolve="AnyOf" />
              </node>
              <node concept="3bZ5Sz" id="1mAGFBKfSDY" role="HW$YZ">
                <ref role="3bZ5Sy" to="7f9y:5Wfdz$0ooZz" resolve="CompositeCondition" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3bZ5Sz" id="1mAGFBKewhL" role="3ddBve">
        <ref role="3bZ5Sy" to="7f9y:5Wfdz$0ooZz" resolve="CompositeCondition" />
      </node>
    </node>
  </node>
  <node concept="2S6QgY" id="4B5aqq8fIsW">
    <property role="3GE5qa" value="base.conditions" />
    <property role="TrG5h" value="ExtractIntoNamedCondition" />
    <property role="2ZfUl0" value="true" />
    <ref role="2ZfgGC" to="7f9y:5Wfdz$0ooZy" resolve="Condition" />
    <node concept="2S6ZIM" id="4B5aqq8fIsX" role="2ZfVej">
      <node concept="3clFbS" id="4B5aqq8fIsY" role="2VODD2">
        <node concept="3clFbF" id="4B5aqq8ylL6" role="3cqZAp">
          <node concept="2OqwBi" id="4B5aqq8ymS3" role="3clFbG">
            <node concept="2OqwBi" id="4B5aqq8ym0z" role="2Oq$k0">
              <node concept="2Sf5sV" id="4B5aqq8ylL5" role="2Oq$k0" />
              <node concept="2yIwOk" id="4B5aqq8ymmU" role="2OqNvi" />
            </node>
            <node concept="2qgKlT" id="4B5aqq8ynjt" role="2OqNvi">
              <ref role="37wK5l" to="wb6c:4B5aqq8gwAk" resolve="getExtractIntoNamedConditionIntentionDescription" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2Sbjvc" id="4B5aqq8fIsZ" role="2ZfgGD">
      <node concept="3clFbS" id="4B5aqq8fIt0" role="2VODD2">
        <node concept="3SKdUt" id="4B5aqq8gpOJ" role="3cqZAp">
          <node concept="3SKdUq" id="4B5aqq8gpOL" role="3SKWNk">
            <property role="3SKdUp" value="Create new NamedCondition node" />
          </node>
        </node>
        <node concept="3cpWs8" id="4B5aqq8fVsD" role="3cqZAp">
          <node concept="3cpWsn" id="4B5aqq8fVsG" role="3cpWs9">
            <property role="TrG5h" value="namedCondition" />
            <node concept="3Tqbb2" id="4B5aqq8fVsB" role="1tU5fm">
              <ref role="ehGHo" to="7f9y:1mAGFBJeyRH" resolve="NamedCondition" />
            </node>
            <node concept="2pJPEk" id="4B5aqq8fWCe" role="33vP2m">
              <node concept="2pJPED" id="4B5aqq8fTCa" role="2pJPEn">
                <ref role="2pJxaS" to="7f9y:1mAGFBJeyRH" resolve="NamedCondition" />
                <node concept="2pJxcG" id="4B5aqq8g6Dn" role="2pJxcM">
                  <ref role="2pJxcJ" to="tpck:h0TrG11" resolve="name" />
                  <node concept="2OqwBi" id="4B5aqq8g7bU" role="2pJxcZ">
                    <node concept="35c_gC" id="4B5aqq8g6SE" role="2Oq$k0">
                      <ref role="35c_gD" to="7f9y:1mAGFBJeyRH" resolve="NamedCondition" />
                    </node>
                    <node concept="2qgKlT" id="4B5aqq8g7Ny" role="2OqNvi">
                      <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                    </node>
                  </node>
                </node>
                <node concept="2pIpSj" id="4B5aqq8fTQz" role="2pJxcM">
                  <ref role="2pIpSl" to="7f9y:1mAGFBJeyRK" resolve="condition" />
                  <node concept="36biLy" id="4B5aqq8fU72" role="2pJxcZ">
                    <node concept="2OqwBi" id="4B5aqq8fYGE" role="36biLW">
                      <node concept="2Sf5sV" id="4B5aqq8fU9D" role="2Oq$k0" />
                      <node concept="1$rogu" id="4B5aqq8fYSk" role="2OqNvi" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="4B5aqq8gq9W" role="3cqZAp" />
        <node concept="3SKdUt" id="4B5aqq8gqIT" role="3cqZAp">
          <node concept="3SKdUq" id="4B5aqq8gqIV" role="3SKWNk">
            <property role="3SKdUp" value="Replace all occurences of condition with reference to NamedCondition node" />
          </node>
        </node>
        <node concept="3cpWs8" id="4B5aqq8i_Vy" role="3cqZAp">
          <node concept="3cpWsn" id="4B5aqq8i_V_" role="3cpWs9">
            <property role="TrG5h" value="ruleSet" />
            <node concept="3Tqbb2" id="4B5aqq8i_Vw" role="1tU5fm">
              <ref role="ehGHo" to="7f9y:5Wfdz$0omZf" resolve="RuleSet" />
            </node>
            <node concept="2OqwBi" id="4B5aqq8iAqG" role="33vP2m">
              <node concept="2Sf5sV" id="4B5aqq8iAfe" role="2Oq$k0" />
              <node concept="2Xjw5R" id="4B5aqq8iACK" role="2OqNvi">
                <node concept="1xMEDy" id="4B5aqq8iACM" role="1xVPHs">
                  <node concept="chp4Y" id="4B5aqq8iAF2" role="ri$Ld">
                    <ref role="cht4Q" to="7f9y:5Wfdz$0omZf" resolve="RuleSet" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4B5aqq8iB_d" role="3cqZAp">
          <node concept="2OqwBi" id="4B5aqq8iFmW" role="3clFbG">
            <node concept="2OqwBi" id="4B5aqq8iRNF" role="2Oq$k0">
              <node concept="2OqwBi" id="4B5aqq8iBUS" role="2Oq$k0">
                <node concept="37vLTw" id="4B5aqq8iB_b" role="2Oq$k0">
                  <ref role="3cqZAo" node="4B5aqq8i_V_" resolve="ruleSet" />
                </node>
                <node concept="2Rf3mk" id="4B5aqq8iCnX" role="2OqNvi">
                  <node concept="1xMEDy" id="4B5aqq8iCnZ" role="1xVPHs">
                    <node concept="25Kdxt" id="4B5aqq8iCI$" role="ri$Ld">
                      <node concept="2OqwBi" id="4B5aqq8iD2_" role="25KhWn">
                        <node concept="2Sf5sV" id="4B5aqq8iCPe" role="2Oq$k0" />
                        <node concept="2yIwOk" id="4B5aqq8iDmS" role="2OqNvi" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3zZkjj" id="4B5aqq8iU00" role="2OqNvi">
                <node concept="1bVj0M" id="4B5aqq8iU02" role="23t8la">
                  <node concept="3clFbS" id="4B5aqq8iU03" role="1bW5cS">
                    <node concept="3clFbF" id="4B5aqq8iU7K" role="3cqZAp">
                      <node concept="2YFouu" id="4B5aqq8iURK" role="3clFbG">
                        <node concept="2Sf5sV" id="4B5aqq8iV9y" role="3uHU7w" />
                        <node concept="37vLTw" id="4B5aqq8iV0C" role="3uHU7B">
                          <ref role="3cqZAo" node="4B5aqq8iU04" resolve="it" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="Rh6nW" id="4B5aqq8iU04" role="1bW2Oz">
                    <property role="TrG5h" value="it" />
                    <node concept="2jxLKc" id="4B5aqq8iU05" role="1tU5fm" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="2es0OD" id="4B5aqq8iHBl" role="2OqNvi">
              <node concept="1bVj0M" id="4B5aqq8iHBn" role="23t8la">
                <node concept="3clFbS" id="4B5aqq8iHBo" role="1bW5cS">
                  <node concept="3clFbF" id="4B5aqq8iHG0" role="3cqZAp">
                    <node concept="2OqwBi" id="4B5aqq8iHR6" role="3clFbG">
                      <node concept="37vLTw" id="4B5aqq8iHFZ" role="2Oq$k0">
                        <ref role="3cqZAo" node="4B5aqq8iHBp" resolve="it" />
                      </node>
                      <node concept="1P9Npp" id="4B5aqq8iI8z" role="2OqNvi">
                        <node concept="2pJPEk" id="4B5aqq8iIfN" role="1P9ThW">
                          <node concept="2pJPED" id="4B5aqq8iIfO" role="2pJPEn">
                            <ref role="2pJxaS" to="7f9y:1mAGFBJL9Gh" resolve="NamedConditionReference" />
                            <node concept="2pIpSj" id="4B5aqq8iIfP" role="2pJxcM">
                              <ref role="2pIpSl" to="7f9y:1mAGFBJL9Gi" resolve="target" />
                              <node concept="36biLy" id="4B5aqq8iIfQ" role="2pJxcZ">
                                <node concept="37vLTw" id="4B5aqq8iIfR" role="36biLW">
                                  <ref role="3cqZAo" node="4B5aqq8fVsG" resolve="namedCondition" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="Rh6nW" id="4B5aqq8iHBp" role="1bW2Oz">
                  <property role="TrG5h" value="it" />
                  <node concept="2jxLKc" id="4B5aqq8iHBq" role="1tU5fm" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="4B5aqq8gr4i" role="3cqZAp" />
        <node concept="3SKdUt" id="4B5aqq8jaSe" role="3cqZAp">
          <node concept="3SKdUq" id="4B5aqq8jaSg" role="3SKWNk">
            <property role="3SKdUp" value="Add NamedCondition node to rule set and focus on its name" />
          </node>
        </node>
        <node concept="3clFbF" id="4B5aqq8fJyK" role="3cqZAp">
          <node concept="2OqwBi" id="4B5aqq8fOXe" role="3clFbG">
            <node concept="2OqwBi" id="4B5aqq8fLWR" role="2Oq$k0">
              <node concept="3Tsc0h" id="4B5aqq8fMvU" role="2OqNvi">
                <ref role="3TtcxE" to="7f9y:1mAGFBJeFF$" resolve="namedConditions" />
              </node>
              <node concept="37vLTw" id="4B5aqq8iBos" role="2Oq$k0">
                <ref role="3cqZAo" node="4B5aqq8i_V_" resolve="ruleSet" />
              </node>
            </node>
            <node concept="TSZUe" id="4B5aqq8fT7E" role="2OqNvi">
              <node concept="37vLTw" id="4B5aqq8fVZa" role="25WWJ7">
                <ref role="3cqZAo" node="4B5aqq8fVsG" resolve="namedCondition" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4B5aqq8ggAA" role="3cqZAp">
          <node concept="2OqwBi" id="4B5aqq8ggU3" role="3clFbG">
            <node concept="1OKiuA" id="4B5aqq8gioD" role="2OqNvi">
              <node concept="1XNTG" id="4B5aqq8gisB" role="lBI5i" />
              <node concept="eBIwv" id="4B5aqq8gjGR" role="lGT1i">
                <ref role="fyFUz" to="tpck:h0TrG11" resolve="name" />
              </node>
            </node>
            <node concept="37vLTw" id="4B5aqq8giTf" role="2Oq$k0">
              <ref role="3cqZAo" node="4B5aqq8fVsG" resolve="namedCondition" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2SaL7w" id="4B5aqq8fJRN" role="2ZfVeh">
      <node concept="3clFbS" id="4B5aqq8fJRO" role="2VODD2">
        <node concept="3clFbF" id="4B5aqq8fJZz" role="3cqZAp">
          <node concept="2OqwBi" id="4B5aqq8fL1k" role="3clFbG">
            <node concept="2OqwBi" id="4B5aqq8fKel" role="2Oq$k0">
              <node concept="2Sf5sV" id="4B5aqq8fJZy" role="2Oq$k0" />
              <node concept="2Xjw5R" id="4B5aqq8fKx3" role="2OqNvi">
                <node concept="1xMEDy" id="4B5aqq8fKx5" role="1xVPHs">
                  <node concept="chp4Y" id="4B5aqq8fKFV" role="ri$Ld">
                    <ref role="cht4Q" to="7f9y:5Wfdz$0omZf" resolve="RuleSet" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3x8VRR" id="4B5aqq8fLpn" role="2OqNvi" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3dkpOd" id="2FjKBCQb3sv">
    <property role="3GE5qa" value="aspect.pointcuts" />
    <property role="TrG5h" value="SurroundWith_Pointcut_Intention" />
    <ref role="2ZfgGC" to="7f9y:2FjKBCOu_xP" resolve="Pointcut" />
    <node concept="2S6ZIM" id="2FjKBCQb3sw" role="2ZfVej">
      <node concept="3clFbS" id="2FjKBCQb3sx" role="2VODD2">
        <node concept="3clFbJ" id="2FjKBCQbjYP" role="3cqZAp">
          <node concept="2OqwBi" id="2FjKBCQbkrk" role="3clFbw">
            <node concept="38Zlrr" id="2FjKBCQbk7y" role="2Oq$k0" />
            <node concept="2Zo12i" id="2FjKBCQbkQO" role="2OqNvi">
              <node concept="chp4Y" id="2FjKBCQbl3l" role="2Zo12j">
                <ref role="cht4Q" to="7f9y:2FjKBCPCCot" resolve="AllOfPointcut" />
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="2FjKBCQbjYR" role="3clFbx">
            <node concept="3cpWs6" id="2FjKBCQbtgs" role="3cqZAp">
              <node concept="2OqwBi" id="2FjKBCQbnLI" role="3cqZAk">
                <node concept="35c_gC" id="2FjKBCQbn18" role="2Oq$k0">
                  <ref role="35c_gD" to="7f9y:2FjKBCOu_xP" resolve="Pointcut" />
                </node>
                <node concept="2qgKlT" id="2FjKBCQbql6" role="2OqNvi">
                  <ref role="37wK5l" to="wb6c:2FjKBCQbciW" resolve="getSurroundWithAllOfIntentionDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="2FjKBCQbqRK" role="3cqZAp">
          <node concept="3clFbS" id="2FjKBCQbqRM" role="3clFbx">
            <node concept="3cpWs6" id="2FjKBCQbtV2" role="3cqZAp">
              <node concept="2OqwBi" id="2FjKBCQbw6n" role="3cqZAk">
                <node concept="35c_gC" id="2FjKBCQbucN" role="2Oq$k0">
                  <ref role="35c_gD" to="7f9y:2FjKBCOu_xP" resolve="Pointcut" />
                </node>
                <node concept="2qgKlT" id="2FjKBCQbwEC" role="2OqNvi">
                  <ref role="37wK5l" to="wb6c:2FjKBCQbcms" resolve="getSurroundWithAnyOfIntentionDescription" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="2FjKBCQbrtl" role="3clFbw">
            <node concept="38Zlrr" id="2FjKBCQbr9l" role="2Oq$k0" />
            <node concept="2Zo12i" id="2FjKBCQbs1w" role="2OqNvi">
              <node concept="chp4Y" id="2FjKBCQbs_j" role="2Zo12j">
                <ref role="cht4Q" to="7f9y:2FjKBCPJRZI" resolve="AnyOfPointcut" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2FjKBCQbxeF" role="3cqZAp">
          <node concept="10Nm6u" id="2FjKBCQbxeD" role="3clFbG" />
        </node>
      </node>
    </node>
    <node concept="2Sbjvc" id="2FjKBCQb3sy" role="2ZfgGD">
      <node concept="3clFbS" id="2FjKBCQb3sz" role="2VODD2">
        <node concept="3clFbJ" id="2FjKBCQcyDC" role="3cqZAp">
          <node concept="2OqwBi" id="2FjKBCQcyDD" role="3clFbw">
            <node concept="38Zlrr" id="2FjKBCQcyDE" role="2Oq$k0" />
            <node concept="2Zo12i" id="2FjKBCQcyDF" role="2OqNvi">
              <node concept="chp4Y" id="2FjKBCQcyDG" role="2Zo12j">
                <ref role="cht4Q" to="7f9y:2FjKBCPCCot" resolve="AllOfPointcut" />
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="2FjKBCQcyDH" role="3clFbx">
            <node concept="3clFbF" id="2FjKBCQczFQ" role="3cqZAp">
              <node concept="2OqwBi" id="2FjKBCQczFR" role="3clFbG">
                <node concept="2Sf5sV" id="2FjKBCQczFS" role="2Oq$k0" />
                <node concept="2DeJnW" id="2FjKBCQcXLj" role="2OqNvi">
                  <ref role="1_rbq0" to="7f9y:2FjKBCPCCot" resolve="AllOfPointcut" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="2FjKBCQcyDM" role="3cqZAp">
          <node concept="3clFbS" id="2FjKBCQcyDN" role="3clFbx">
            <node concept="3clFbF" id="2FjKBCQcz8S" role="3cqZAp">
              <node concept="2OqwBi" id="2FjKBCQczis" role="3clFbG">
                <node concept="2Sf5sV" id="2FjKBCQcz8R" role="2Oq$k0" />
                <node concept="2DeJnW" id="2FjKBCQcXPW" role="2OqNvi">
                  <ref role="1_rbq0" to="7f9y:2FjKBCPJRZI" resolve="AnyOfPointcut" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="2FjKBCQcyDS" role="3clFbw">
            <node concept="38Zlrr" id="2FjKBCQcyDT" role="2Oq$k0" />
            <node concept="2Zo12i" id="2FjKBCQcyDU" role="2OqNvi">
              <node concept="chp4Y" id="2FjKBCQcyDV" role="2Zo12j">
                <ref role="cht4Q" to="7f9y:2FjKBCPJRZI" resolve="AnyOfPointcut" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="38BcoT" id="2FjKBCQb406" role="3dlsAV">
      <node concept="3clFbS" id="2FjKBCQb407" role="2VODD2">
        <node concept="3clFbF" id="2FjKBCQbdjP" role="3cqZAp">
          <node concept="2ShNRf" id="2FjKBCQbdjN" role="3clFbG">
            <node concept="Tc6Ow" id="2FjKBCQbdK5" role="2ShVmc">
              <node concept="35c_gC" id="2FjKBCQbeOf" role="HW$Y0">
                <ref role="35c_gD" to="7f9y:2FjKBCPCCot" resolve="AllOfPointcut" />
              </node>
              <node concept="35c_gC" id="2FjKBCQbgQa" role="HW$Y0">
                <ref role="35c_gD" to="7f9y:2FjKBCPJRZI" resolve="AnyOfPointcut" />
              </node>
              <node concept="3bZ5Sz" id="2FjKBCQbiP0" role="HW$YZ">
                <ref role="3bZ5Sy" to="7f9y:2FjKBCQ332Q" resolve="CompositePointcut" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3bZ5Sz" id="2FjKBCQb4b$" role="3ddBve">
        <ref role="3bZ5Sy" to="7f9y:2FjKBCQ332Q" resolve="CompositePointcut" />
      </node>
    </node>
  </node>
</model>

