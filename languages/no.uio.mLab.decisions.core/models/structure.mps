<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:790ac37e-42d3-4dd3-977c-e8fe9cc56a45(no.uio.mLab.decisions.core.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="6" />
    <devkit ref="78434eb8-b0e5-444b-850d-e7c4ad2da9ab(jetbrains.mps.devkit.aspect.structure)" />
  </languages>
  <imports>
    <import index="tpee" ref="r:00000000-0000-4000-0000-011c895902ca(jetbrains.mps.baseLanguage.structure)" />
    <import index="vbok" ref="r:6cc208d3-a370-49ca-a6ac-ded97805a924(no.uio.mLab.shared.structure)" />
    <import index="kwxp" ref="b4d28e19-7d2d-47e9-943e-3a41f97a0e52/r:4903509f-5416-46ff-9a8b-44b5a178b568(com.mbeddr.mpsutil.plantuml.node/com.mbeddr.mpsutil.plantuml.node.structure)" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
    <import index="kkto" ref="r:01f3534b-874e-42dc-a9b5-7540591f45bb(no.uio.mLab.entities.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <property id="4628067390765956807" name="final" index="R5$K2" />
        <property id="4628067390765956802" name="abstract" index="R5$K7" />
        <property id="5092175715804935370" name="conceptAlias" index="34LRSv" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
        <child id="1071489727084" name="propertyDeclaration" index="1TKVEl" />
      </concept>
      <concept id="1169125989551" name="jetbrains.mps.lang.structure.structure.InterfaceConceptDeclaration" flags="ig" index="PlHQZ">
        <child id="1169127546356" name="extends" index="PrDN$" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <property id="1096454100552" name="rootable" index="19KtqR" />
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288299" name="jetbrains.mps.lang.structure.structure.PropertyDeclaration" flags="ig" index="1TJgyi">
        <property id="241647608299431129" name="propertyId" index="IQ2nx" />
        <reference id="1082985295845" name="dataType" index="AX2Wp" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599698500" name="specializedLink" index="20ksaX" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="5Wfdz$0omZf">
    <property role="EcuMT" value="6849753176701235151" />
    <property role="TrG5h" value="RuleSet" />
    <property role="19KtqR" value="true" />
    <property role="3GE5qa" value="base" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="2$xY$aF35Fj" role="PzmwI">
      <ref role="PrY4T" node="1Hxyv4EQSvU" resolve="ITranslatableCoreConcept" />
    </node>
    <node concept="PrWs8" id="4B5aqq7nPh8" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="PrWs8" id="1mAGFBK6J1q" role="PzmwI">
      <ref role="PrY4T" to="tpck:3fifI_xCcJN" resolve="ScopeProvider" />
    </node>
    <node concept="PrWs8" id="6oczKrNVCvk" role="PzmwI">
      <ref role="PrY4T" to="kwxp:2N1CSrzPN_8" resolve="IVisualizable" />
    </node>
    <node concept="1TJgyj" id="1mAGFBJeagS" role="1TKVEi">
      <property role="IQ2ns" value="1560130832582616120" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="preCondition" />
      <ref role="20lvS9" node="5Wfdz$0ooZy" resolve="Condition" />
    </node>
    <node concept="1TJgyj" id="1mAGFBJeFF$" role="1TKVEi">
      <property role="IQ2ns" value="1560130832582752996" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="namedConditions" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="1mAGFBJeyRH" resolve="NamedCondition" />
    </node>
    <node concept="1TJgyj" id="5Wfdz$0onq6" role="1TKVEi">
      <property role="IQ2ns" value="6849753176701236870" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="rules" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="5Wfdz$0omZg" resolve="Rule" />
    </node>
    <node concept="1TJgyi" id="2XLt5KTPWGm" role="1TKVEl">
      <property role="IQ2nx" value="3418641531607173910" />
      <property role="TrG5h" value="description" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="5Wfdz$0omZg">
    <property role="EcuMT" value="6849753176701235152" />
    <property role="TrG5h" value="Rule" />
    <property role="3GE5qa" value="base" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="2$xY$aF5T2v" role="PzmwI">
      <ref role="PrY4T" node="1Hxyv4EQSvU" resolve="ITranslatableCoreConcept" />
    </node>
    <node concept="PrWs8" id="2$xY$aF6xNf" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="1TJgyj" id="5Wfdz$0op0O" role="1TKVEi">
      <property role="IQ2ns" value="6849753176701243444" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="condition" />
      <property role="20lbJX" value="0..1" />
      <ref role="20lvS9" node="5Wfdz$0ooZy" resolve="Condition" />
    </node>
    <node concept="1TJgyj" id="1mAGFBL9ZsO" role="1TKVEi">
      <property role="IQ2ns" value="1560130832615077684" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="actions" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="1mAGFBKnGHq" resolve="Action" />
    </node>
    <node concept="1TJgyi" id="1mAGFBJKlyi" role="1TKVEl">
      <property role="IQ2nx" value="1560130832591575186" />
      <property role="TrG5h" value="description" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="5Wfdz$0ooZy">
    <property role="EcuMT" value="6849753176701243362" />
    <property role="TrG5h" value="Condition" />
    <property role="R5$K7" value="false" />
    <property role="R5$K2" value="false" />
    <property role="3GE5qa" value="base.conditions" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="1Hxyv4EXAER" role="PzmwI">
      <ref role="PrY4T" node="1Hxyv4EQSvU" resolve="ITranslatableCoreConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="5Wfdz$0ooZz">
    <property role="EcuMT" value="6849753176701243363" />
    <property role="TrG5h" value="CompositeCondition" />
    <property role="R5$K7" value="true" />
    <property role="R5$K2" value="false" />
    <property role="3GE5qa" value="base.conditions.composites" />
    <ref role="1TJDcQ" node="5Wfdz$0ooZy" resolve="Condition" />
    <node concept="1TJgyj" id="5Wfdz$0ooZ$" role="1TKVEi">
      <property role="IQ2ns" value="6849753176701243364" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="conditions" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="5Wfdz$0ooZy" resolve="Condition" />
    </node>
  </node>
  <node concept="1TIwiD" id="5Wfdz$0ooZA">
    <property role="EcuMT" value="6849753176701243366" />
    <property role="TrG5h" value="AnyOf" />
    <property role="3GE5qa" value="base.conditions.composites" />
    <ref role="1TJDcQ" node="5Wfdz$0ooZz" resolve="CompositeCondition" />
  </node>
  <node concept="1TIwiD" id="5Wfdz$0ooZB">
    <property role="EcuMT" value="6849753176701243367" />
    <property role="TrG5h" value="AllOf" />
    <property role="3GE5qa" value="base.conditions.composites" />
    <ref role="1TJDcQ" node="5Wfdz$0ooZz" resolve="CompositeCondition" />
  </node>
  <node concept="1TIwiD" id="5Wfdz$0qdif">
    <property role="EcuMT" value="6849753176701719695" />
    <property role="3GE5qa" value="base.constraints.number.atomic" />
    <property role="TrG5h" value="NumberEqualTo" />
    <ref role="1TJDcQ" node="1mAGFBJ9goY" resolve="AtomicNumberConstraint" />
  </node>
  <node concept="1TIwiD" id="5Wfdz$0qdiH">
    <property role="EcuMT" value="6849753176701719725" />
    <property role="TrG5h" value="Literal" />
    <property role="R5$K7" value="true" />
    <property role="R5$K2" value="false" />
    <property role="3GE5qa" value="base.literals" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="1Hxyv4EXAEZ" role="PzmwI">
      <ref role="PrY4T" node="1Hxyv4EQSvU" resolve="ITranslatableCoreConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="5Wfdz$0qdiI">
    <property role="EcuMT" value="6849753176701719726" />
    <property role="3GE5qa" value="base.literals" />
    <property role="TrG5h" value="NumberLiteral" />
    <ref role="1TJDcQ" node="5Wfdz$0qdiH" resolve="Literal" />
    <node concept="1TJgyi" id="5Wfdz$0qdiJ" role="1TKVEl">
      <property role="IQ2nx" value="6849753176701719727" />
      <property role="TrG5h" value="value" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="5Wfdz$0rGXx">
    <property role="EcuMT" value="6849753176702111585" />
    <property role="3GE5qa" value="base.constraints.number.atomic" />
    <property role="TrG5h" value="NumberLessThan" />
    <ref role="1TJDcQ" node="1mAGFBJ9goY" resolve="AtomicNumberConstraint" />
  </node>
  <node concept="1TIwiD" id="5Wfdz$0rGYE">
    <property role="EcuMT" value="6849753176702111658" />
    <property role="3GE5qa" value="base.constraints.number.atomic" />
    <property role="TrG5h" value="NumberGreaterThan" />
    <ref role="1TJDcQ" node="1mAGFBJ9goY" resolve="AtomicNumberConstraint" />
  </node>
  <node concept="1TIwiD" id="5Wfdz$0rGYF">
    <property role="EcuMT" value="6849753176702111659" />
    <property role="3GE5qa" value="base.constraints.number.atomic" />
    <property role="TrG5h" value="NumberLessThanOrEqualTo" />
    <ref role="1TJDcQ" node="1mAGFBJ9goY" resolve="AtomicNumberConstraint" />
  </node>
  <node concept="1TIwiD" id="5Wfdz$0rGYI">
    <property role="EcuMT" value="6849753176702111662" />
    <property role="3GE5qa" value="base.constraints.number.atomic" />
    <property role="TrG5h" value="NumberGreaterThanOrEqualTo" />
    <ref role="1TJDcQ" node="1mAGFBJ9goY" resolve="AtomicNumberConstraint" />
  </node>
  <node concept="1TIwiD" id="5Wfdz$0rLYU">
    <property role="EcuMT" value="6849753176702132154" />
    <property role="3GE5qa" value="base.constraints.number" />
    <property role="TrG5h" value="NumberInRange" />
    <ref role="1TJDcQ" node="1mAGFBJ9osA" resolve="NumberConstraint" />
    <node concept="1TJgyj" id="5Wfdz$0w28U" role="1TKVEi">
      <property role="IQ2ns" value="6849753176703246906" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="lower" />
      <property role="20lbJX" value="0..1" />
      <ref role="20lvS9" node="5Wfdz$0qdiI" resolve="NumberLiteral" />
      <ref role="20ksaX" node="PDjyzkfMCs" resolve="lower" />
    </node>
    <node concept="1TJgyj" id="5Wfdz$0w28W" role="1TKVEi">
      <property role="IQ2ns" value="6849753176703246908" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="upper" />
      <property role="20lbJX" value="0..1" />
      <ref role="20lvS9" node="5Wfdz$0qdiI" resolve="NumberLiteral" />
      <ref role="20ksaX" node="PDjyzkfMCu" resolve="upper" />
    </node>
    <node concept="PrWs8" id="PDjyzkg6Et" role="PzmwI">
      <ref role="PrY4T" node="PDjyzkfMCr" resolve="IRangeConstraint" />
    </node>
  </node>
  <node concept="1TIwiD" id="5Wfdz$0v7yK">
    <property role="EcuMT" value="6849753176703006896" />
    <property role="3GE5qa" value="base.literals" />
    <property role="TrG5h" value="TextLiteral" />
    <ref role="1TJDcQ" node="5Wfdz$0qdiH" resolve="Literal" />
    <node concept="1TJgyi" id="5Wfdz$0v7yL" role="1TKVEl">
      <property role="IQ2nx" value="6849753176703006897" />
      <property role="TrG5h" value="value" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="1mAGFBJ9goY">
    <property role="EcuMT" value="1560130832581330494" />
    <property role="3GE5qa" value="base.constraints.number.atomic" />
    <property role="TrG5h" value="AtomicNumberConstraint" />
    <property role="R5$K7" value="true" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" node="1mAGFBJ9osA" resolve="NumberConstraint" />
    <node concept="1TJgyj" id="1mAGFBJ6NLB" role="1TKVEi">
      <property role="IQ2ns" value="1560130832580688999" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="number" />
      <property role="20lbJX" value="0..1" />
      <ref role="20lvS9" node="5Wfdz$0qdiI" resolve="NumberLiteral" />
    </node>
  </node>
  <node concept="1TIwiD" id="1mAGFBJ9osA">
    <property role="EcuMT" value="1560130832581363494" />
    <property role="3GE5qa" value="base.constraints.number" />
    <property role="TrG5h" value="NumberConstraint" />
    <property role="R5$K7" value="true" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" node="1mAGFBKqQs5" resolve="Constraint" />
  </node>
  <node concept="1TIwiD" id="1mAGFBJeyRH">
    <property role="EcuMT" value="1560130832582716909" />
    <property role="TrG5h" value="NamedCondition" />
    <property role="3GE5qa" value="base.definitions" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="1mAGFBJeyRK" role="1TKVEi">
      <property role="IQ2ns" value="1560130832582716912" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="condition" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="5Wfdz$0ooZy" resolve="Condition" />
    </node>
    <node concept="PrWs8" id="499Gn2DcgUd" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="PrWs8" id="1Hxyv4ETP$f" role="PzmwI">
      <ref role="PrY4T" node="1Hxyv4EQSvU" resolve="ITranslatableCoreConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="1mAGFBJlPnZ">
    <property role="EcuMT" value="1560130832584627711" />
    <property role="3GE5qa" value="base.constraints.timespan" />
    <property role="TrG5h" value="TimeSpanConstraint" />
    <property role="R5$K7" value="true" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" node="1mAGFBKqQs5" resolve="Constraint" />
  </node>
  <node concept="1TIwiD" id="1mAGFBJm3bz">
    <property role="EcuMT" value="1560130832584684259" />
    <property role="3GE5qa" value="base.constraints.timespan.atomic" />
    <property role="TrG5h" value="TimeSpanGreaterThanOrEqualTo" />
    <ref role="1TJDcQ" node="1mAGFBJmyec" resolve="AtomicTimeSpanConstraint" />
  </node>
  <node concept="1TIwiD" id="1mAGFBJm3b$">
    <property role="EcuMT" value="1560130832584684260" />
    <property role="3GE5qa" value="base.literals.timespan" />
    <property role="TrG5h" value="TimeSpanLiteral" />
    <property role="R5$K7" value="true" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" node="5Wfdz$0qdiH" resolve="Literal" />
    <node concept="1TJgyi" id="1mAGFBJm3c3" role="1TKVEl">
      <property role="IQ2nx" value="1560130832584684291" />
      <property role="TrG5h" value="value" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
  </node>
  <node concept="1TIwiD" id="1mAGFBJm3b_">
    <property role="EcuMT" value="1560130832584684261" />
    <property role="3GE5qa" value="base.literals.timespan" />
    <property role="TrG5h" value="Week" />
    <ref role="1TJDcQ" node="1mAGFBJm3b$" resolve="TimeSpanLiteral" />
  </node>
  <node concept="1TIwiD" id="1mAGFBJmyec">
    <property role="EcuMT" value="1560130832584811404" />
    <property role="3GE5qa" value="base.constraints.timespan.atomic" />
    <property role="TrG5h" value="AtomicTimeSpanConstraint" />
    <property role="R5$K7" value="true" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" node="1mAGFBJlPnZ" resolve="TimeSpanConstraint" />
    <node concept="1TJgyj" id="1mAGFBJmyed" role="1TKVEi">
      <property role="IQ2ns" value="1560130832584811405" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="timeSpan" />
      <property role="20lbJX" value="0..1" />
      <ref role="20lvS9" node="1mAGFBJm3b$" resolve="TimeSpanLiteral" />
    </node>
  </node>
  <node concept="1TIwiD" id="1mAGFBJyTqr">
    <property role="EcuMT" value="1560130832588052123" />
    <property role="3GE5qa" value="base.literals.timespan" />
    <property role="TrG5h" value="Month" />
    <ref role="1TJDcQ" node="1mAGFBJm3b$" resolve="TimeSpanLiteral" />
  </node>
  <node concept="1TIwiD" id="1mAGFBJ$nHq">
    <property role="EcuMT" value="1560130832588438362" />
    <property role="3GE5qa" value="base.constraints.timespan.atomic" />
    <property role="TrG5h" value="TimeSpanLessThanOrEqualTo" />
    <ref role="1TJDcQ" node="1mAGFBJmyec" resolve="AtomicTimeSpanConstraint" />
  </node>
  <node concept="1TIwiD" id="1mAGFBJL9Gh">
    <property role="EcuMT" value="1560130832591788817" />
    <property role="3GE5qa" value="base.conditions" />
    <property role="TrG5h" value="NamedConditionReference" />
    <ref role="1TJDcQ" node="5Wfdz$0ooZy" resolve="Condition" />
    <node concept="1TJgyj" id="1mAGFBJL9Gi" role="1TKVEi">
      <property role="IQ2ns" value="1560130832591788818" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="target" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="1mAGFBJeyRH" resolve="NamedCondition" />
    </node>
  </node>
  <node concept="1TIwiD" id="1mAGFBJTAhg">
    <property role="EcuMT" value="1560130832594003024" />
    <property role="3GE5qa" value="base.conditions" />
    <property role="TrG5h" value="Not" />
    <ref role="1TJDcQ" node="5Wfdz$0ooZy" resolve="Condition" />
    <node concept="1TJgyj" id="1mAGFBJTAhh" role="1TKVEi">
      <property role="IQ2ns" value="1560130832594003025" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="condition" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="5Wfdz$0ooZy" resolve="Condition" />
    </node>
  </node>
  <node concept="1TIwiD" id="1mAGFBKkASk">
    <property role="EcuMT" value="1560130832601083412" />
    <property role="3GE5qa" value="base.constraints.text" />
    <property role="TrG5h" value="TextConstraint" />
    <property role="R5$K7" value="true" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" node="1mAGFBKqQs5" resolve="Constraint" />
  </node>
  <node concept="1TIwiD" id="1mAGFBKkASl">
    <property role="EcuMT" value="1560130832601083413" />
    <property role="3GE5qa" value="base.constraints.text" />
    <property role="TrG5h" value="TextEqualTo" />
    <ref role="1TJDcQ" node="1mAGFBKkASk" resolve="TextConstraint" />
    <node concept="1TJgyj" id="1mAGFBKkASm" role="1TKVEi">
      <property role="IQ2ns" value="1560130832601083414" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="text" />
      <ref role="20lvS9" node="5Wfdz$0v7yK" resolve="TextLiteral" />
    </node>
  </node>
  <node concept="1TIwiD" id="1mAGFBKnGHq">
    <property role="EcuMT" value="1560130832601893722" />
    <property role="TrG5h" value="Action" />
    <property role="3GE5qa" value="base.actions" />
    <property role="R5$K7" value="true" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="2XLt5KTMDMn" role="PzmwI">
      <ref role="PrY4T" node="1Hxyv4EQSvU" resolve="ITranslatableCoreConcept" />
    </node>
    <node concept="PrWs8" id="6LTgXmMu6bD" role="PzmwI">
      <ref role="PrY4T" node="6LTgXmMs$_4" resolve="IBasePattern" />
    </node>
  </node>
  <node concept="1TIwiD" id="1mAGFBKqQrZ">
    <property role="EcuMT" value="1560130832602719999" />
    <property role="TrG5h" value="DataValue" />
    <property role="3GE5qa" value="base.dataValues" />
    <property role="R5$K7" value="false" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="1Hxyv4EXAEW" role="PzmwI">
      <ref role="PrY4T" node="1Hxyv4EQSvU" resolve="ITranslatableCoreConcept" />
    </node>
    <node concept="PrWs8" id="6LTgXmMu4jC" role="PzmwI">
      <ref role="PrY4T" node="6LTgXmMs$_4" resolve="IBasePattern" />
    </node>
  </node>
  <node concept="1TIwiD" id="1mAGFBKqQs0">
    <property role="EcuMT" value="1560130832602720000" />
    <property role="3GE5qa" value="base.dataValues.nonBoolean.text" />
    <property role="TrG5h" value="TextDataValue" />
    <property role="R5$K7" value="true" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" node="1mAGFBLqAeU" resolve="NonBooleanDataValue" />
  </node>
  <node concept="1TIwiD" id="1mAGFBKqQs1">
    <property role="EcuMT" value="1560130832602720001" />
    <property role="3GE5qa" value="base.dataValues.nonBoolean.numbers" />
    <property role="TrG5h" value="NumberDataValue" />
    <property role="R5$K7" value="true" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" node="1mAGFBLqAeU" resolve="NonBooleanDataValue" />
  </node>
  <node concept="1TIwiD" id="1mAGFBKqQs5">
    <property role="EcuMT" value="1560130832602720005" />
    <property role="3GE5qa" value="base.constraints" />
    <property role="TrG5h" value="Constraint" />
    <property role="R5$K7" value="false" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="1Hxyv4EXAEU" role="PzmwI">
      <ref role="PrY4T" node="1Hxyv4EQSvU" resolve="ITranslatableCoreConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="1mAGFBKuqjA">
    <property role="EcuMT" value="1560130832603653350" />
    <property role="3GE5qa" value="base.dataValues.boolean" />
    <property role="TrG5h" value="BooleanDataValue" />
    <property role="R5$K7" value="true" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" node="1mAGFBKqQrZ" resolve="DataValue" />
  </node>
  <node concept="1TIwiD" id="1mAGFBKuWHF">
    <property role="EcuMT" value="1560130832603794283" />
    <property role="3GE5qa" value="base.dataValues.nonBoolean.timespans" />
    <property role="TrG5h" value="TimeSpanData" />
    <property role="R5$K7" value="true" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" node="1mAGFBLqAeU" resolve="NonBooleanDataValue" />
  </node>
  <node concept="PlHQZ" id="1mAGFBKFXT_">
    <property role="EcuMT" value="1560130832607207013" />
    <property role="3GE5qa" value="base.dataValues" />
    <property role="TrG5h" value="IDataValueWithUnit" />
  </node>
  <node concept="1TIwiD" id="1mAGFBKu11W">
    <property role="EcuMT" value="1560130832603549820" />
    <property role="3GE5qa" value="base.conditions.simples" />
    <property role="TrG5h" value="SimpleCondition" />
    <property role="R5$K7" value="true" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" node="5Wfdz$0ooZy" resolve="Condition" />
    <node concept="1TJgyj" id="4QUW3ed7qmA" role="1TKVEi">
      <property role="IQ2ns" value="5601053190790161830" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="data" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="1mAGFBKqQrZ" resolve="DataValue" />
    </node>
  </node>
  <node concept="1TIwiD" id="1mAGFBLqAeU">
    <property role="EcuMT" value="1560130832619430842" />
    <property role="3GE5qa" value="base.dataValues.nonBoolean" />
    <property role="TrG5h" value="NonBooleanDataValue" />
    <property role="R5$K7" value="true" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" node="1mAGFBKqQrZ" resolve="DataValue" />
  </node>
  <node concept="1TIwiD" id="1mAGFBJRO8A">
    <property role="EcuMT" value="1560130832593535526" />
    <property role="3GE5qa" value="base.conditions.simples" />
    <property role="TrG5h" value="NonBooleanDataValueCondition" />
    <property role="R5$K7" value="false" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" node="1mAGFBKu11W" resolve="SimpleCondition" />
    <node concept="1TJgyj" id="1mAGFBLqAeR" role="1TKVEi">
      <property role="IQ2ns" value="1560130832619430839" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="data" />
      <ref role="20lvS9" node="1mAGFBLqAeU" resolve="NonBooleanDataValue" />
      <ref role="20ksaX" node="4QUW3ed7qmA" resolve="data" />
    </node>
    <node concept="1TJgyj" id="1mAGFBKqQs2" role="1TKVEi">
      <property role="IQ2ns" value="1560130832602720002" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="constraint" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="1mAGFBKqQs5" resolve="Constraint" />
    </node>
  </node>
  <node concept="1TIwiD" id="1mAGFBJRO8_">
    <property role="EcuMT" value="1560130832593535525" />
    <property role="3GE5qa" value="base.conditions.simples" />
    <property role="TrG5h" value="BooleanDataValueCondition" />
    <property role="R5$K7" value="false" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" node="1mAGFBKu11W" resolve="SimpleCondition" />
    <node concept="1TJgyj" id="1mAGFBLqAeO" role="1TKVEi">
      <property role="IQ2ns" value="1560130832619430836" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="data" />
      <ref role="20lvS9" node="1mAGFBKuqjA" resolve="BooleanDataValue" />
      <ref role="20ksaX" node="4QUW3ed7qmA" resolve="data" />
    </node>
  </node>
  <node concept="PlHQZ" id="1Hxyv4EQSvU">
    <property role="EcuMT" value="1973009780664141818" />
    <property role="TrG5h" value="ITranslatableCoreConcept" />
    <property role="3GE5qa" value="" />
    <node concept="PrWs8" id="1Hxyv4EQSw7" role="PrDN$">
      <ref role="PrY4T" to="vbok:5Wfdz$0vc1Z" resolve="ITranslatableConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="CxH2r_SlM0">
    <property role="EcuMT" value="730062693769239680" />
    <property role="TrG5h" value="Advice" />
    <property role="3GE5qa" value="aspect.advices" />
    <property role="34LRSv" value="for each rule set" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="2FjKBCOyLj$" role="1TKVEi">
      <property role="IQ2ns" value="3086023999802250468" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="parameters" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="4QUW3edDXqS" resolve="AspectVariable" />
    </node>
    <node concept="1TJgyj" id="6khVixyYxjq" role="1TKVEi">
      <property role="IQ2ns" value="7282862830149833946" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="pointcut" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="2FjKBCOu_xP" resolve="Pointcut" />
    </node>
    <node concept="1TJgyj" id="6khVixz98js" role="1TKVEi">
      <property role="IQ2ns" value="7282862830152615132" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="effect" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="CxH2rBlQYG" resolve="Change" />
    </node>
    <node concept="PrWs8" id="2FjKBCOvpqB" role="PzmwI">
      <ref role="PrY4T" to="tpck:3fifI_xCcJN" resolve="ScopeProvider" />
    </node>
    <node concept="PrWs8" id="2FjKBCOvpqG" role="PzmwI">
      <ref role="PrY4T" node="1Hxyv4EQSvU" resolve="ITranslatableCoreConcept" />
    </node>
    <node concept="1TJgyi" id="2FjKBCOyMUe" role="1TKVEl">
      <property role="IQ2nx" value="3086023999802257038" />
      <property role="TrG5h" value="description" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="CxH2rBlQYG">
    <property role="EcuMT" value="730062693793755052" />
    <property role="3GE5qa" value="aspect.changes" />
    <property role="TrG5h" value="Change" />
    <property role="R5$K7" value="false" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="2FjKBCOCced" role="PzmwI">
      <ref role="PrY4T" node="1Hxyv4EQSvU" resolve="ITranslatableCoreConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="CxH2rBlQYF">
    <property role="EcuMT" value="730062693793755051" />
    <property role="3GE5qa" value="aspect.changes" />
    <property role="TrG5h" value="AddRuleChange" />
    <ref role="1TJDcQ" node="CxH2rBlQYG" resolve="Change" />
    <node concept="1TJgyj" id="CxH2rBlQZv" role="1TKVEi">
      <property role="IQ2ns" value="730062693793755103" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="rule" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="5Wfdz$0omZg" resolve="Rule" />
    </node>
  </node>
  <node concept="1TIwiD" id="WP50h3DQVE">
    <property role="EcuMT" value="1095804092882054890" />
    <property role="TrG5h" value="Aspect" />
    <property role="19KtqR" value="true" />
    <property role="3GE5qa" value="aspect" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="WP50h3DQVF" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="PrWs8" id="4QUW3edMZls" role="PzmwI">
      <ref role="PrY4T" to="tpck:3fifI_xCcJN" resolve="ScopeProvider" />
    </node>
    <node concept="1TJgyj" id="WP50h3DQVH" role="1TKVEi">
      <property role="IQ2ns" value="1095804092882054893" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="advices" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="CxH2r_SlM0" resolve="Advice" />
    </node>
    <node concept="1TJgyi" id="5uQLXCsOJw0" role="1TKVEl">
      <property role="IQ2nx" value="6320458866661455872" />
      <property role="TrG5h" value="description" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="4QUW3edDXqS">
    <property role="EcuMT" value="5601053190799218360" />
    <property role="3GE5qa" value="aspect.variables" />
    <property role="TrG5h" value="AspectVariable" />
    <property role="R5$K7" value="false" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="4QUW3efG$hf" role="PzmwI">
      <ref role="PrY4T" to="vbok:4QUW3efGuc3" resolve="IReferrableConcept" />
    </node>
    <node concept="1TJgyj" id="6LTgXmNnjBG" role="1TKVEi">
      <property role="IQ2ns" value="7816353213394532844" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="pattern" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="6LTgXmNrfh_" resolve="AspectVariablePattern" />
    </node>
  </node>
  <node concept="1TIwiD" id="4QUW3efv2iG">
    <property role="EcuMT" value="5601053190829909164" />
    <property role="3GE5qa" value="base.parameters.references" />
    <property role="TrG5h" value="Reference" />
    <property role="R5$K7" value="true" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" node="2FjKBCOFjHt" resolve="BaseParameter" />
    <node concept="PrWs8" id="6LTgXmN1I6X" role="PzmwI">
      <ref role="PrY4T" node="6LTgXmMZbIT" resolve="IReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="6khVixyQayu">
    <property role="EcuMT" value="7282862830147643550" />
    <property role="3GE5qa" value="aspect.pointcuts.atomic" />
    <property role="TrG5h" value="ContainingDataValuePointcut" />
    <ref role="1TJDcQ" node="2FjKBCQ332P" resolve="AtomicPointcut" />
    <node concept="1TJgyj" id="6khVixyZhSn" role="1TKVEi">
      <property role="IQ2ns" value="7282862830150032919" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="patterns" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="1mAGFBKqQrZ" resolve="DataValue" />
    </node>
  </node>
  <node concept="1TIwiD" id="6khVixz9RtK">
    <property role="EcuMT" value="7282862830152808304" />
    <property role="3GE5qa" value="aspect.changes" />
    <property role="TrG5h" value="AddConjunctivePreConditionChange" />
    <ref role="1TJDcQ" node="CxH2rBlQYG" resolve="Change" />
    <node concept="1TJgyj" id="6khVixz9RtL" role="1TKVEi">
      <property role="IQ2ns" value="7282862830152808305" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="condition" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="5Wfdz$0ooZy" resolve="Condition" />
    </node>
  </node>
  <node concept="1TIwiD" id="6khVix$CgE2">
    <property role="EcuMT" value="7282862830177553026" />
    <property role="3GE5qa" value="aspect.pointcuts.atomic" />
    <property role="TrG5h" value="ContainingActionPointcut" />
    <ref role="1TJDcQ" node="2FjKBCQ332P" resolve="AtomicPointcut" />
    <node concept="1TJgyj" id="6khVix$CgE4" role="1TKVEi">
      <property role="IQ2ns" value="7282862830177553028" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="patterns" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="1mAGFBKnGHq" resolve="Action" />
    </node>
  </node>
  <node concept="1TIwiD" id="PDjyzjRLRa">
    <property role="EcuMT" value="966389532307955146" />
    <property role="3GE5qa" value="base.literals.timespan" />
    <property role="TrG5h" value="Year" />
    <ref role="1TJDcQ" node="1mAGFBJm3b$" resolve="TimeSpanLiteral" />
  </node>
  <node concept="1TIwiD" id="PDjyzjVahG">
    <property role="EcuMT" value="966389532308841580" />
    <property role="3GE5qa" value="base.literals.timespan" />
    <property role="TrG5h" value="Day" />
    <ref role="1TJDcQ" node="1mAGFBJm3b$" resolve="TimeSpanLiteral" />
  </node>
  <node concept="1TIwiD" id="PDjyzkbfEs">
    <property role="EcuMT" value="966389532313057948" />
    <property role="3GE5qa" value="base.constraints.timespan.atomic" />
    <property role="TrG5h" value="TimeSpanGreaterThan" />
    <ref role="1TJDcQ" node="1mAGFBJmyec" resolve="AtomicTimeSpanConstraint" />
  </node>
  <node concept="1TIwiD" id="PDjyzkdJz2">
    <property role="EcuMT" value="966389532313712834" />
    <property role="3GE5qa" value="base.constraints.timespan.atomic" />
    <property role="TrG5h" value="TimeSpanLessThan" />
    <ref role="1TJDcQ" node="1mAGFBJmyec" resolve="AtomicTimeSpanConstraint" />
  </node>
  <node concept="1TIwiD" id="PDjyzkfMCm">
    <property role="EcuMT" value="966389532314249750" />
    <property role="3GE5qa" value="base.constraints.timespan" />
    <property role="TrG5h" value="TimeSpanInRange" />
    <ref role="1TJDcQ" node="1mAGFBJlPnZ" resolve="TimeSpanConstraint" />
    <node concept="PrWs8" id="PDjyzkjwOh" role="PzmwI">
      <ref role="PrY4T" node="PDjyzkfMCr" resolve="IRangeConstraint" />
    </node>
    <node concept="1TJgyj" id="PDjyzkrQW9" role="1TKVEi">
      <property role="IQ2ns" value="966389532317413129" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="lower" />
      <ref role="20lvS9" node="1mAGFBJm3b$" resolve="TimeSpanLiteral" />
      <ref role="20ksaX" node="PDjyzkfMCs" resolve="lower" />
    </node>
    <node concept="1TJgyj" id="PDjyzkrQWd" role="1TKVEi">
      <property role="IQ2ns" value="966389532317413133" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="upper" />
      <ref role="20lvS9" node="1mAGFBJm3b$" resolve="TimeSpanLiteral" />
      <ref role="20ksaX" node="PDjyzkfMCu" resolve="upper" />
    </node>
  </node>
  <node concept="PlHQZ" id="PDjyzkfMCr">
    <property role="EcuMT" value="966389532314249755" />
    <property role="3GE5qa" value="base.constraints" />
    <property role="TrG5h" value="IRangeConstraint" />
    <node concept="1TJgyj" id="PDjyzkfMCs" role="1TKVEi">
      <property role="IQ2ns" value="966389532314249756" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="lower" />
      <ref role="20lvS9" node="5Wfdz$0qdiH" resolve="Literal" />
    </node>
    <node concept="1TJgyj" id="PDjyzkfMCu" role="1TKVEi">
      <property role="IQ2ns" value="966389532314249758" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="upper" />
      <ref role="20lvS9" node="5Wfdz$0qdiH" resolve="Literal" />
    </node>
    <node concept="PrWs8" id="PDjyzkfOgX" role="PrDN$">
      <ref role="PrY4T" node="1Hxyv4EQSvU" resolve="ITranslatableCoreConcept" />
    </node>
    <node concept="1TJgyi" id="PDjyzkfOPG" role="1TKVEl">
      <property role="IQ2nx" value="966389532314258796" />
      <property role="TrG5h" value="isLowerOpen" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
    <node concept="1TJgyi" id="PDjyzkfOPI" role="1TKVEl">
      <property role="IQ2nx" value="966389532314258798" />
      <property role="TrG5h" value="isUpperOpen" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
  </node>
  <node concept="PlHQZ" id="6LTgXmMs$_4">
    <property role="EcuMT" value="7816353213379135812" />
    <property role="3GE5qa" value="aspect.patterns.base" />
    <property role="TrG5h" value="IBasePattern" />
  </node>
  <node concept="PlHQZ" id="6LTgXmMvpq9">
    <property role="EcuMT" value="7816353213379876489" />
    <property role="3GE5qa" value="base.parameters.references" />
    <property role="TrG5h" value="IEntityReference" />
    <node concept="1TJgyj" id="4QUW3efv2jv" role="1TKVEi">
      <property role="IQ2ns" value="5601053190829909215" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="target" />
      <ref role="20lvS9" to="kkto:4QUW3edDqMR" resolve="Entity" />
    </node>
    <node concept="PrWs8" id="6LTgXmMZbIX" role="PrDN$">
      <ref role="PrY4T" node="6LTgXmMZbIT" resolve="IReference" />
    </node>
  </node>
  <node concept="PlHQZ" id="6LTgXmMAn$t">
    <property role="EcuMT" value="7816353213381703965" />
    <property role="3GE5qa" value="aspect.references" />
    <property role="TrG5h" value="IAspectVariableReference" />
    <node concept="1TJgyj" id="6LTgXmMAS1z" role="1TKVEi">
      <property role="IQ2ns" value="7816353213381836899" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="target" />
      <ref role="20lvS9" node="4QUW3edDXqS" resolve="AspectVariable" />
    </node>
    <node concept="PrWs8" id="6LTgXmMZbJ0" role="PrDN$">
      <ref role="PrY4T" node="6LTgXmMZbIT" resolve="IReference" />
    </node>
  </node>
  <node concept="PlHQZ" id="6LTgXmMZbIT">
    <property role="EcuMT" value="7816353213388209081" />
    <property role="3GE5qa" value="base.parameters.references" />
    <property role="TrG5h" value="IReference" />
    <node concept="PrWs8" id="6LTgXmMZbIU" role="PrDN$">
      <ref role="PrY4T" node="1Hxyv4EQSvU" resolve="ITranslatableCoreConcept" />
    </node>
    <node concept="PrWs8" id="1I84Bf7nVJS" role="PrDN$">
      <ref role="PrY4T" node="6LTgXmMs$_4" resolve="IBasePattern" />
    </node>
  </node>
  <node concept="1TIwiD" id="6LTgXmNrfh_">
    <property role="EcuMT" value="7816353213395563621" />
    <property role="3GE5qa" value="aspect.patterns.aspect" />
    <property role="TrG5h" value="AspectVariablePattern" />
    <property role="R5$K7" value="false" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="6LTgXmNu3R_" role="PzmwI">
      <ref role="PrY4T" to="vbok:5Wfdz$0vc1Z" resolve="ITranslatableConcept" />
    </node>
    <node concept="PrWs8" id="65epL7MmDje" role="PzmwI">
      <ref role="PrY4T" node="65epL7Ml5P6" resolve="IAspectPattern" />
    </node>
  </node>
  <node concept="1TIwiD" id="1I84Bf95szg">
    <property role="EcuMT" value="1983855924360890576" />
    <property role="3GE5qa" value="aspect" />
    <property role="TrG5h" value="AspectOrdering" />
    <property role="19KtqR" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="1I84Bf95tmA" role="1TKVEi">
      <property role="IQ2ns" value="1983855924360893862" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="aspects" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="1I84Bf95szt" resolve="AspectReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="1I84Bf95szt">
    <property role="EcuMT" value="1983855924360890589" />
    <property role="3GE5qa" value="aspect" />
    <property role="TrG5h" value="AspectReference" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="1I84Bf95szu" role="1TKVEi">
      <property role="IQ2ns" value="1983855924360890590" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="target" />
      <ref role="20lvS9" node="WP50h3DQVE" resolve="Aspect" />
    </node>
  </node>
  <node concept="PlHQZ" id="65epL7Ml5Dl">
    <property role="EcuMT" value="7011654996640160341" />
    <property role="3GE5qa" value="aspect.patterns.aspect.stereotypes" />
    <property role="TrG5h" value="IWildcardPattern" />
    <node concept="PrWs8" id="65epL7Mnzzx" role="PrDN$">
      <ref role="PrY4T" node="65epL7Ml5P6" resolve="IAspectPattern" />
    </node>
  </node>
  <node concept="PlHQZ" id="65epL7Ml5P6">
    <property role="EcuMT" value="7011654996640161094" />
    <property role="3GE5qa" value="aspect.patterns.aspect" />
    <property role="TrG5h" value="IAspectPattern" />
    <node concept="PrWs8" id="65epL7M_6E3" role="PrDN$">
      <ref role="PrY4T" to="vbok:5Wfdz$0vc1Z" resolve="ITranslatableConcept" />
    </node>
  </node>
  <node concept="PlHQZ" id="65epL7Mprkt">
    <property role="EcuMT" value="7011654996641297693" />
    <property role="3GE5qa" value="aspect.patterns.aspect.stereotypes" />
    <property role="TrG5h" value="ISelectedEntityPattern" />
    <node concept="PrWs8" id="65epL7Mprku" role="PrDN$">
      <ref role="PrY4T" node="65epL7Ml5P6" resolve="IAspectPattern" />
    </node>
    <node concept="1TJgyj" id="65epL7MqHQ9" role="1TKVEi">
      <property role="IQ2ns" value="7011654996641635721" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="entities" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="6LTgXmMZbIT" resolve="IReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="2FjKBCOu_xP">
    <property role="EcuMT" value="3086023999801153653" />
    <property role="3GE5qa" value="aspect.pointcuts" />
    <property role="TrG5h" value="Pointcut" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="2FjKBCOvR7R" role="PzmwI">
      <ref role="PrY4T" node="1Hxyv4EQSvU" resolve="ITranslatableCoreConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="2FjKBCOFjHr">
    <property role="EcuMT" value="3086023999804488539" />
    <property role="3GE5qa" value="aspect.pointcuts.atomic" />
    <property role="TrG5h" value="ContainingBaseParameterPointcut" />
    <ref role="1TJDcQ" node="2FjKBCQ332P" resolve="AtomicPointcut" />
    <node concept="1TJgyj" id="2FjKBCOFjHs" role="1TKVEi">
      <property role="IQ2ns" value="3086023999804488540" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="patterns" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="2FjKBCOFjHt" resolve="BaseParameter" />
    </node>
  </node>
  <node concept="1TIwiD" id="2FjKBCOFjHt">
    <property role="EcuMT" value="3086023999804488541" />
    <property role="3GE5qa" value="base.parameters" />
    <property role="TrG5h" value="BaseParameter" />
    <property role="R5$K7" value="false" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="2FjKBCOFjHv" role="PzmwI">
      <ref role="PrY4T" to="vbok:5Wfdz$0vc1Z" resolve="ITranslatableConcept" />
    </node>
    <node concept="PrWs8" id="2FjKBCOFjH$" role="PzmwI">
      <ref role="PrY4T" node="6LTgXmMs$_4" resolve="IBasePattern" />
    </node>
  </node>
  <node concept="1TIwiD" id="2FjKBCOFjHC">
    <property role="EcuMT" value="3086023999804488552" />
    <property role="3GE5qa" value="base.parameters.action" />
    <property role="TrG5h" value="ActionParameter" />
    <ref role="1TJDcQ" node="2FjKBCOFjHt" resolve="BaseParameter" />
  </node>
  <node concept="1TIwiD" id="2FjKBCOKKJW">
    <property role="EcuMT" value="3086023999805918204" />
    <property role="3GE5qa" value="aspect.changes" />
    <property role="TrG5h" value="AddDisjunctivePreConditionChange" />
    <ref role="1TJDcQ" node="CxH2rBlQYG" resolve="Change" />
    <node concept="1TJgyj" id="2FjKBCOKKJX" role="1TKVEi">
      <property role="IQ2ns" value="3086023999805918205" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="condition" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="5Wfdz$0ooZy" resolve="Condition" />
    </node>
  </node>
  <node concept="1TIwiD" id="2FjKBCPCCot">
    <property role="EcuMT" value="3086023999820563997" />
    <property role="3GE5qa" value="aspect.pointcuts.composite" />
    <property role="TrG5h" value="AllOfPointcut" />
    <ref role="1TJDcQ" node="2FjKBCQ332Q" resolve="CompositePointcut" />
    <node concept="1TJgyj" id="2FjKBCPCCoD" role="1TKVEi">
      <property role="IQ2ns" value="3086023999820564009" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="pointcuts" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="2FjKBCOu_xP" resolve="Pointcut" />
      <ref role="20ksaX" node="2FjKBCQ4hNj" resolve="pointcuts" />
    </node>
  </node>
  <node concept="1TIwiD" id="2FjKBCPJRZI">
    <property role="EcuMT" value="3086023999822462958" />
    <property role="3GE5qa" value="aspect.pointcuts.composite" />
    <property role="TrG5h" value="AnyOfPointcut" />
    <ref role="1TJDcQ" node="2FjKBCQ332Q" resolve="CompositePointcut" />
    <node concept="1TJgyj" id="2FjKBCPJRZJ" role="1TKVEi">
      <property role="IQ2ns" value="3086023999822462959" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="pointcuts" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="2FjKBCOu_xP" resolve="Pointcut" />
      <ref role="20ksaX" node="2FjKBCQ4hNj" resolve="pointcuts" />
    </node>
  </node>
  <node concept="1TIwiD" id="2FjKBCQ332P">
    <property role="EcuMT" value="3086023999827488949" />
    <property role="3GE5qa" value="aspect.pointcuts.atomic" />
    <property role="TrG5h" value="AtomicPointcut" />
    <property role="R5$K7" value="true" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" node="2FjKBCOu_xP" resolve="Pointcut" />
  </node>
  <node concept="1TIwiD" id="2FjKBCQ332Q">
    <property role="EcuMT" value="3086023999827488950" />
    <property role="3GE5qa" value="aspect.pointcuts.composite" />
    <property role="TrG5h" value="CompositePointcut" />
    <property role="R5$K7" value="true" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" node="2FjKBCOu_xP" resolve="Pointcut" />
    <node concept="1TJgyj" id="2FjKBCQ4hNj" role="1TKVEi">
      <property role="IQ2ns" value="3086023999827811539" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="pointcuts" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="2FjKBCOu_xP" resolve="Pointcut" />
    </node>
  </node>
</model>

