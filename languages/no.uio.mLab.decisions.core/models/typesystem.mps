<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:cd35ac82-689e-45a0-979d-b3548f5d88f1(no.uio.mLab.decisions.core.typesystem)">
  <persistence version="9" />
  <languages>
    <use id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem" version="1" />
    <devkit ref="00000000-0000-4000-0000-1de82b3a4936(jetbrains.mps.devkit.aspect.typesystem)" />
  </languages>
  <imports>
    <import index="kkto" ref="r:01f3534b-874e-42dc-a9b5-7540591f45bb(no.uio.mLab.entities.core.structure)" />
    <import index="wb6c" ref="r:19d15037-83c1-4ae2-b66a-763828cb8279(no.uio.mLab.decisions.core.behavior)" implicit="true" />
    <import index="7f9y" ref="r:790ac37e-42d3-4dd3-977c-e8fe9cc56a45(no.uio.mLab.decisions.core.structure)" implicit="true" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
    <import index="33ny" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.util(JDK/)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1082485599095" name="jetbrains.mps.baseLanguage.structure.BlockStatement" flags="nn" index="9aQIb">
        <child id="1082485599096" name="statements" index="9aQI4" />
      </concept>
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="1153422305557" name="jetbrains.mps.baseLanguage.structure.LessThanOrEqualsExpression" flags="nn" index="2dkUwp" />
      <concept id="4836112446988635817" name="jetbrains.mps.baseLanguage.structure.UndefinedType" flags="in" index="2jxLKc" />
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1070534370425" name="jetbrains.mps.baseLanguage.structure.IntegerType" flags="in" index="10Oyi0" />
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="1225271369338" name="jetbrains.mps.baseLanguage.structure.IsEmptyOperation" flags="nn" index="17RlXB" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123152" name="jetbrains.mps.baseLanguage.structure.EqualsExpression" flags="nn" index="3clFbC" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1082485599094" name="ifFalseStatement" index="9aQIa" />
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6" />
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1081506762703" name="jetbrains.mps.baseLanguage.structure.GreaterThanExpression" flags="nn" index="3eOSWO" />
      <concept id="1081506773034" name="jetbrains.mps.baseLanguage.structure.LessThanExpression" flags="nn" index="3eOVzh" />
      <concept id="1081516740877" name="jetbrains.mps.baseLanguage.structure.NotExpression" flags="nn" index="3fqX7Q">
        <child id="1081516765348" name="expression" index="3fr31v" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1073239437375" name="jetbrains.mps.baseLanguage.structure.NotEqualsExpression" flags="nn" index="3y3z36" />
      <concept id="5497648299878491908" name="jetbrains.mps.baseLanguage.structure.BaseVariableReference" flags="nn" index="1M0zk4">
        <reference id="5497648299878491909" name="baseVariableDeclaration" index="1M0zk5" />
      </concept>
      <concept id="6329021646629104957" name="jetbrains.mps.baseLanguage.structure.TextCommentPart" flags="nn" index="3SKdUq">
        <property id="6329021646629104958" name="text" index="3SKdUp" />
      </concept>
      <concept id="6329021646629104954" name="jetbrains.mps.baseLanguage.structure.SingleLineComment" flags="nn" index="3SKdUt">
        <child id="6329021646629175155" name="commentPart" index="3SKWNk" />
      </concept>
      <concept id="1080120340718" name="jetbrains.mps.baseLanguage.structure.AndExpression" flags="nn" index="1Wc70l" />
    </language>
    <language id="fd392034-7849-419d-9071-12563d152375" name="jetbrains.mps.baseLanguage.closures">
      <concept id="1199569711397" name="jetbrains.mps.baseLanguage.closures.structure.ClosureLiteral" flags="nn" index="1bVj0M">
        <child id="1199569906740" name="parameter" index="1bW2Oz" />
        <child id="1199569916463" name="body" index="1bW5cS" />
      </concept>
    </language>
    <language id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem">
      <concept id="1207055528241" name="jetbrains.mps.lang.typesystem.structure.WarningStatement" flags="nn" index="a7r0C">
        <child id="1207055552304" name="warningText" index="a7wSD" />
      </concept>
      <concept id="1175517767210" name="jetbrains.mps.lang.typesystem.structure.ReportErrorStatement" flags="nn" index="2MkqsV">
        <child id="1175517851849" name="errorString" index="2MkJ7o" />
      </concept>
      <concept id="1227096498176" name="jetbrains.mps.lang.typesystem.structure.PropertyMessageTarget" flags="ng" index="2ODE4t">
        <reference id="1227096521710" name="propertyDeclaration" index="2ODJFN" />
      </concept>
      <concept id="1227096620180" name="jetbrains.mps.lang.typesystem.structure.ReferenceMessageTarget" flags="ng" index="2OE7Q9">
        <reference id="1227096645744" name="linkDeclaration" index="2OEe5H" />
      </concept>
      <concept id="1227096774658" name="jetbrains.mps.lang.typesystem.structure.MessageStatement" flags="ng" index="2OEH$v">
        <child id="1227096802791" name="helginsIntention" index="2OEOjU" />
        <child id="1227096802790" name="nodeToReport" index="2OEOjV" />
        <child id="1227096836496" name="messageTarget" index="2OEWyd" />
      </concept>
      <concept id="1216383170661" name="jetbrains.mps.lang.typesystem.structure.TypesystemQuickFix" flags="ng" index="Q5z_Y">
        <child id="1216383424566" name="executeBlock" index="Q6x$H" />
        <child id="1216383476350" name="quickFixArgument" index="Q6Id_" />
        <child id="1216391046856" name="descriptionBlock" index="QzAvj" />
      </concept>
      <concept id="1216383287005" name="jetbrains.mps.lang.typesystem.structure.QuickFixExecuteBlock" flags="in" index="Q5ZZ6" />
      <concept id="1216383482742" name="jetbrains.mps.lang.typesystem.structure.QuickFixArgument" flags="ng" index="Q6JDH">
        <child id="1216383511839" name="argumentType" index="Q6QK4" />
      </concept>
      <concept id="1216390348809" name="jetbrains.mps.lang.typesystem.structure.QuickFixArgumentReference" flags="nn" index="QwW4i">
        <reference id="1216390348810" name="quickFixArgument" index="QwW4h" />
      </concept>
      <concept id="1216390987552" name="jetbrains.mps.lang.typesystem.structure.QuickFixDescriptionBlock" flags="in" index="QznSV" />
      <concept id="1195213580585" name="jetbrains.mps.lang.typesystem.structure.AbstractCheckingRule" flags="ig" index="18hYwZ">
        <child id="1195213635060" name="body" index="18ibNy" />
      </concept>
      <concept id="1195214364922" name="jetbrains.mps.lang.typesystem.structure.NonTypesystemRule" flags="ig" index="18kY7G" />
      <concept id="1210784285454" name="jetbrains.mps.lang.typesystem.structure.TypesystemIntention" flags="ng" index="3Cnw8n">
        <reference id="1216388525179" name="quickFix" index="QpYPw" />
        <child id="1210784493590" name="actualArgument" index="3Coj4f" />
      </concept>
      <concept id="1210784384552" name="jetbrains.mps.lang.typesystem.structure.TypesystemIntentionArgument" flags="ng" index="3CnSsL">
        <reference id="1216386999476" name="quickFixArgument" index="QkamJ" />
        <child id="1210784642750" name="value" index="3CoRuB" />
      </concept>
      <concept id="1174642788531" name="jetbrains.mps.lang.typesystem.structure.ConceptReference" flags="ig" index="1YaCAy">
        <reference id="1174642800329" name="concept" index="1YaFvo" />
      </concept>
      <concept id="1174648085619" name="jetbrains.mps.lang.typesystem.structure.AbstractRule" flags="ng" index="1YuPPy">
        <child id="1174648101952" name="applicableNode" index="1YuTPh" />
      </concept>
      <concept id="1174650418652" name="jetbrains.mps.lang.typesystem.structure.ApplicableNodeReference" flags="nn" index="1YBJjd">
        <reference id="1174650432090" name="applicableNode" index="1YBMHb" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1204834851141" name="jetbrains.mps.lang.smodel.structure.PoundExpression" flags="ng" index="25Kdxt">
        <child id="1204834868751" name="expression" index="25KhWn" />
      </concept>
      <concept id="1179168000618" name="jetbrains.mps.lang.smodel.structure.Node_GetIndexInParentOperation" flags="nn" index="2bSWHS" />
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1138411891628" name="jetbrains.mps.lang.smodel.structure.SNodeOperation" flags="nn" index="eCIE_">
        <child id="1144104376918" name="parameter" index="1xVPHs" />
      </concept>
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="7453996997717780434" name="jetbrains.mps.lang.smodel.structure.Node_GetSConceptOperation" flags="nn" index="2yIwOk" />
      <concept id="2396822768958367367" name="jetbrains.mps.lang.smodel.structure.AbstractTypeCastExpression" flags="nn" index="$5XWr">
        <child id="6733348108486823193" name="leftExpression" index="1m5AlR" />
        <child id="3906496115198199033" name="conceptArgument" index="3oSUPX" />
      </concept>
      <concept id="1143234257716" name="jetbrains.mps.lang.smodel.structure.Node_GetModelOperation" flags="nn" index="I4A8Y" />
      <concept id="1883223317721008708" name="jetbrains.mps.lang.smodel.structure.IfInstanceOfStatement" flags="nn" index="Jncv_">
        <reference id="1883223317721008712" name="nodeConcept" index="JncvD" />
        <child id="1883223317721008709" name="body" index="Jncv$" />
        <child id="1883223317721008711" name="variable" index="JncvA" />
        <child id="1883223317721008710" name="nodeExpression" index="JncvB" />
      </concept>
      <concept id="1883223317721008713" name="jetbrains.mps.lang.smodel.structure.IfInstanceOfVariable" flags="ng" index="JncvC" />
      <concept id="1883223317721107059" name="jetbrains.mps.lang.smodel.structure.IfInstanceOfVarReference" flags="nn" index="Jnkvi" />
      <concept id="1171407110247" name="jetbrains.mps.lang.smodel.structure.Node_GetAncestorOperation" flags="nn" index="2Xjw5R" />
      <concept id="1227264722563" name="jetbrains.mps.lang.smodel.structure.EqualsStructurallyExpression" flags="nn" index="2YFouu" />
      <concept id="2644386474300074836" name="jetbrains.mps.lang.smodel.structure.ConceptIdRefExpression" flags="nn" index="35c_gC">
        <reference id="2644386474300074837" name="conceptDeclaration" index="35c_gD" />
      </concept>
      <concept id="1182511038748" name="jetbrains.mps.lang.smodel.structure.Model_NodesIncludingImportedOperation" flags="nn" index="1j9C0f">
        <reference id="1182511038750" name="concept" index="1j9C0d" />
      </concept>
      <concept id="1139613262185" name="jetbrains.mps.lang.smodel.structure.Node_GetParentOperation" flags="nn" index="1mfA1w" />
      <concept id="1139621453865" name="jetbrains.mps.lang.smodel.structure.Node_IsInstanceOfOperation" flags="nn" index="1mIQ4w">
        <child id="1177027386292" name="conceptArgument" index="cj9EA" />
      </concept>
      <concept id="1171999116870" name="jetbrains.mps.lang.smodel.structure.Node_IsNullOperation" flags="nn" index="3w_OXm" />
      <concept id="1172008320231" name="jetbrains.mps.lang.smodel.structure.Node_IsNotNullOperation" flags="nn" index="3x8VRR" />
      <concept id="1144101972840" name="jetbrains.mps.lang.smodel.structure.OperationParm_Concept" flags="ng" index="1xMEDy">
        <child id="1207343664468" name="conceptArgument" index="ri$Ld" />
      </concept>
      <concept id="1172326502327" name="jetbrains.mps.lang.smodel.structure.Concept_IsExactlyOperation" flags="nn" index="3O6GUB">
        <child id="1206733650006" name="conceptArgument" index="3QVz_e" />
      </concept>
      <concept id="1140131837776" name="jetbrains.mps.lang.smodel.structure.Node_ReplaceWithAnotherOperation" flags="nn" index="1P9Npp">
        <child id="1140131861877" name="replacementNode" index="1P9ThW" />
      </concept>
      <concept id="1140137987495" name="jetbrains.mps.lang.smodel.structure.SNodeTypeCastExpression" flags="nn" index="1PxgMI" />
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
      <concept id="1228341669568" name="jetbrains.mps.lang.smodel.structure.Node_DetachOperation" flags="nn" index="3YRAZt" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1204796164442" name="jetbrains.mps.baseLanguage.collections.structure.InternalSequenceOperation" flags="nn" index="23sCx2">
        <child id="1204796294226" name="closure" index="23t8la" />
      </concept>
      <concept id="1172664342967" name="jetbrains.mps.baseLanguage.collections.structure.TakeOperation" flags="nn" index="8ftyA">
        <child id="1172664372046" name="elementsToTake" index="8f$Dv" />
      </concept>
      <concept id="1204980550705" name="jetbrains.mps.baseLanguage.collections.structure.VisitAllOperation" flags="nn" index="2es0OD" />
      <concept id="1235566554328" name="jetbrains.mps.baseLanguage.collections.structure.AnyOperation" flags="nn" index="2HwmR7" />
      <concept id="1203518072036" name="jetbrains.mps.baseLanguage.collections.structure.SmartClosureParameterDeclaration" flags="ig" index="Rh6nW" />
      <concept id="1162935959151" name="jetbrains.mps.baseLanguage.collections.structure.GetSizeOperation" flags="nn" index="34oBXx" />
      <concept id="1165525191778" name="jetbrains.mps.baseLanguage.collections.structure.GetFirstOperation" flags="nn" index="1uHKPH" />
      <concept id="1165530316231" name="jetbrains.mps.baseLanguage.collections.structure.IsEmptyOperation" flags="nn" index="1v1jN8" />
      <concept id="1202120902084" name="jetbrains.mps.baseLanguage.collections.structure.WhereOperation" flags="nn" index="3zZkjj" />
    </language>
  </registry>
  <node concept="18kY7G" id="1mAGFBJ3SA6">
    <property role="TrG5h" value="check_NumberLiteral" />
    <property role="3GE5qa" value="base.literals" />
    <node concept="3clFbS" id="1mAGFBJ3SA7" role="18ibNy">
      <node concept="3clFbJ" id="1mAGFBJ3SAd" role="3cqZAp">
        <node concept="3fqX7Q" id="1mAGFBJ3Uad" role="3clFbw">
          <node concept="2OqwBi" id="1mAGFBJ3Uaf" role="3fr31v">
            <node concept="2OqwBi" id="1mAGFBJ3Uag" role="2Oq$k0">
              <node concept="1YBJjd" id="1mAGFBJ3Uah" role="2Oq$k0">
                <ref role="1YBMHb" node="1mAGFBJ3SA9" resolve="numberLiteral" />
              </node>
              <node concept="2yIwOk" id="1mAGFBJ3Uai" role="2OqNvi" />
            </node>
            <node concept="2qgKlT" id="1mAGFBJ3Uaj" role="2OqNvi">
              <ref role="37wK5l" to="wb6c:1mAGFBJ3CCM" resolve="isValid" />
              <node concept="2OqwBi" id="1mAGFBJ3Uak" role="37wK5m">
                <node concept="1YBJjd" id="1mAGFBJ3Ual" role="2Oq$k0">
                  <ref role="1YBMHb" node="1mAGFBJ3SA9" resolve="numberLiteral" />
                </node>
                <node concept="3TrcHB" id="1mAGFBJ3Uam" role="2OqNvi">
                  <ref role="3TsBF5" to="7f9y:5Wfdz$0qdiJ" resolve="value" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbS" id="1mAGFBJ3SAf" role="3clFbx">
          <node concept="2MkqsV" id="1mAGFBJ3Uh7" role="3cqZAp">
            <node concept="2OqwBi" id="1mAGFBJ3VvY" role="2MkJ7o">
              <node concept="2OqwBi" id="1mAGFBJ3UQW" role="2Oq$k0">
                <node concept="1YBJjd" id="1mAGFBJ3UGA" role="2Oq$k0">
                  <ref role="1YBMHb" node="1mAGFBJ3SA9" resolve="numberLiteral" />
                </node>
                <node concept="2yIwOk" id="1mAGFBJ3V7r" role="2OqNvi" />
              </node>
              <node concept="2qgKlT" id="1mAGFBJ3W_T" role="2OqNvi">
                <ref role="37wK5l" to="wb6c:1mAGFBJ3UlF" resolve="getInvalidValueError" />
              </node>
            </node>
            <node concept="1YBJjd" id="1mAGFBJ3WE1" role="2OEOjV">
              <ref role="1YBMHb" node="1mAGFBJ3SA9" resolve="numberLiteral" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="1mAGFBJ3SA9" role="1YuTPh">
      <property role="TrG5h" value="numberLiteral" />
      <ref role="1YaFvo" to="7f9y:5Wfdz$0qdiI" resolve="NumberLiteral" />
    </node>
  </node>
  <node concept="18kY7G" id="1mAGFBJfg9C">
    <property role="TrG5h" value="check_InRange" />
    <property role="3GE5qa" value="base.constraints.number" />
    <node concept="3clFbS" id="1mAGFBJfg9D" role="18ibNy">
      <node concept="3clFbJ" id="1mAGFBJfg9J" role="3cqZAp">
        <node concept="2OqwBi" id="1mAGFBJfuOZ" role="3clFbw">
          <node concept="2OqwBi" id="1mAGFBJfuk6" role="2Oq$k0">
            <node concept="1YBJjd" id="1mAGFBJfu91" role="2Oq$k0">
              <ref role="1YBMHb" node="1mAGFBJfg9F" resolve="iRangeConstraint" />
            </node>
            <node concept="3TrEf2" id="PDjyzkmHfq" role="2OqNvi">
              <ref role="3Tt5mk" to="7f9y:PDjyzkfMCs" resolve="lower" />
            </node>
          </node>
          <node concept="3w_OXm" id="1mAGFBJfv2S" role="2OqNvi" />
        </node>
        <node concept="3clFbS" id="1mAGFBJfg9L" role="3clFbx">
          <node concept="2MkqsV" id="1mAGFBJfv9J" role="3cqZAp">
            <node concept="2OqwBi" id="1mAGFBJfvZj" role="2MkJ7o">
              <node concept="2OqwBi" id="1mAGFBJfvl3" role="2Oq$k0">
                <node concept="1YBJjd" id="1mAGFBJfv9V" role="2Oq$k0">
                  <ref role="1YBMHb" node="1mAGFBJfg9F" resolve="iRangeConstraint" />
                </node>
                <node concept="2yIwOk" id="1mAGFBJfv$V" role="2OqNvi" />
              </node>
              <node concept="2qgKlT" id="PDjyzkiDb6" role="2OqNvi">
                <ref role="37wK5l" to="wb6c:PDjyzkfNMp" resolve="getMissingLowerError" />
              </node>
            </node>
            <node concept="1YBJjd" id="1mAGFBJfwnp" role="2OEOjV">
              <ref role="1YBMHb" node="1mAGFBJfg9F" resolve="iRangeConstraint" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3clFbJ" id="1mAGFBJfw_g" role="3cqZAp">
        <node concept="3clFbS" id="1mAGFBJfw_i" role="3clFbx">
          <node concept="2MkqsV" id="1mAGFBJfxQe" role="3cqZAp">
            <node concept="2OqwBi" id="1mAGFBJfyKR" role="2MkJ7o">
              <node concept="2OqwBi" id="1mAGFBJfy1_" role="2Oq$k0">
                <node concept="1YBJjd" id="1mAGFBJfxQt" role="2Oq$k0">
                  <ref role="1YBMHb" node="1mAGFBJfg9F" resolve="iRangeConstraint" />
                </node>
                <node concept="2yIwOk" id="1mAGFBJfymk" role="2OqNvi" />
              </node>
              <node concept="2qgKlT" id="PDjyzkiDqn" role="2OqNvi">
                <ref role="37wK5l" to="wb6c:PDjyzkfNMx" resolve="getMissingUpperError" />
              </node>
            </node>
            <node concept="1YBJjd" id="1mAGFBJfzdO" role="2OEOjV">
              <ref role="1YBMHb" node="1mAGFBJfg9F" resolve="iRangeConstraint" />
            </node>
          </node>
        </node>
        <node concept="2OqwBi" id="1mAGFBJfxvb" role="3clFbw">
          <node concept="2OqwBi" id="1mAGFBJfwPe" role="2Oq$k0">
            <node concept="1YBJjd" id="1mAGFBJfwE9" role="2Oq$k0">
              <ref role="1YBMHb" node="1mAGFBJfg9F" resolve="iRangeConstraint" />
            </node>
            <node concept="3TrEf2" id="PDjyzkmHuC" role="2OqNvi">
              <ref role="3Tt5mk" to="7f9y:PDjyzkfMCu" resolve="upper" />
            </node>
          </node>
          <node concept="3w_OXm" id="1mAGFBJfxNY" role="2OqNvi" />
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="1mAGFBJfg9F" role="1YuTPh">
      <property role="TrG5h" value="iRangeConstraint" />
      <ref role="1YaFvo" to="7f9y:PDjyzkfMCr" resolve="IRangeConstraint" />
    </node>
  </node>
  <node concept="18kY7G" id="1mAGFBJnLYE">
    <property role="TrG5h" value="check_AtomicTimeSpanConstraint" />
    <property role="3GE5qa" value="base.constraints.timespan.atomic" />
    <node concept="3clFbS" id="1mAGFBJnLYF" role="18ibNy">
      <node concept="3clFbJ" id="1mAGFBJnLYL" role="3cqZAp">
        <node concept="2OqwBi" id="1mAGFBJnMEV" role="3clFbw">
          <node concept="2OqwBi" id="1mAGFBJnMa2" role="2Oq$k0">
            <node concept="1YBJjd" id="1mAGFBJnLYX" role="2Oq$k0">
              <ref role="1YBMHb" node="1mAGFBJnLYH" resolve="atomicTimeSpanConstraint" />
            </node>
            <node concept="3TrEf2" id="1mAGFBJnMlw" role="2OqNvi">
              <ref role="3Tt5mk" to="7f9y:1mAGFBJmyed" resolve="timeSpan" />
            </node>
          </node>
          <node concept="3w_OXm" id="1mAGFBJnMSO" role="2OqNvi" />
        </node>
        <node concept="3clFbS" id="1mAGFBJnLYN" role="3clFbx">
          <node concept="2MkqsV" id="1mAGFBJnMV4" role="3cqZAp">
            <node concept="2OqwBi" id="1mAGFBJo5ig" role="2MkJ7o">
              <node concept="2OqwBi" id="1mAGFBJo4C0" role="2Oq$k0">
                <node concept="1YBJjd" id="1mAGFBJo4sS" role="2Oq$k0">
                  <ref role="1YBMHb" node="1mAGFBJnLYH" resolve="atomicTimeSpanConstraint" />
                </node>
                <node concept="2yIwOk" id="1mAGFBJo4RS" role="2OqNvi" />
              </node>
              <node concept="2qgKlT" id="1mAGFBJo5A9" role="2OqNvi">
                <ref role="37wK5l" to="wb6c:1mAGFBJnMVO" resolve="getMissingTimeSpanError" />
              </node>
            </node>
            <node concept="1YBJjd" id="1mAGFBJo5Em" role="2OEOjV">
              <ref role="1YBMHb" node="1mAGFBJnLYH" resolve="atomicTimeSpanConstraint" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="1mAGFBJnLYH" role="1YuTPh">
      <property role="TrG5h" value="atomicTimeSpanConstraint" />
      <ref role="1YaFvo" to="7f9y:1mAGFBJmyec" resolve="AtomicTimeSpanConstraint" />
    </node>
  </node>
  <node concept="18kY7G" id="1mAGFBJG0lS">
    <property role="TrG5h" value="check_CompositeCondition" />
    <property role="3GE5qa" value="base.conditions.composites" />
    <node concept="3clFbS" id="1mAGFBJG0lT" role="18ibNy">
      <node concept="3clFbJ" id="1mAGFBJG0lZ" role="3cqZAp">
        <node concept="2OqwBi" id="1mAGFBJG2Dq" role="3clFbw">
          <node concept="2OqwBi" id="1mAGFBJG0wu" role="2Oq$k0">
            <node concept="1YBJjd" id="1mAGFBJG0mb" role="2Oq$k0">
              <ref role="1YBMHb" node="1mAGFBJG0lV" resolve="compositeCondition" />
            </node>
            <node concept="3Tsc0h" id="1mAGFBJG0Es" role="2OqNvi">
              <ref role="3TtcxE" to="7f9y:5Wfdz$0ooZ$" resolve="conditions" />
            </node>
          </node>
          <node concept="1v1jN8" id="1mAGFBJG42N" role="2OqNvi" />
        </node>
        <node concept="3clFbS" id="1mAGFBJG0m1" role="3clFbx">
          <node concept="2MkqsV" id="1mAGFBJG43y" role="3cqZAp">
            <node concept="2OqwBi" id="1mAGFBJG4NA" role="2MkJ7o">
              <node concept="2OqwBi" id="1mAGFBJG4e4" role="2Oq$k0">
                <node concept="1YBJjd" id="1mAGFBJG43I" role="2Oq$k0">
                  <ref role="1YBMHb" node="1mAGFBJG0lV" resolve="compositeCondition" />
                </node>
                <node concept="2yIwOk" id="1mAGFBJG4qR" role="2OqNvi" />
              </node>
              <node concept="2qgKlT" id="1mAGFBJG558" role="2OqNvi">
                <ref role="37wK5l" to="wb6c:3bOTPdWRMMD" resolve="getMissingConditionError" />
              </node>
            </node>
            <node concept="1YBJjd" id="1mAGFBJG59g" role="2OEOjV">
              <ref role="1YBMHb" node="1mAGFBJG0lV" resolve="compositeCondition" />
            </node>
          </node>
          <node concept="3cpWs6" id="6khVixxPDqe" role="3cqZAp" />
        </node>
      </node>
      <node concept="3clFbJ" id="6khVixxIziS" role="3cqZAp">
        <node concept="3clFbS" id="6khVixxIziU" role="3clFbx">
          <node concept="a7r0C" id="6khVixxIFDS" role="3cqZAp">
            <node concept="1YBJjd" id="6khVixxIFHE" role="2OEOjV">
              <ref role="1YBMHb" node="1mAGFBJG0lV" resolve="compositeCondition" />
            </node>
            <node concept="2OqwBi" id="6khVixxNX8q" role="a7wSD">
              <node concept="2OqwBi" id="6khVixxNW5N" role="2Oq$k0">
                <node concept="1YBJjd" id="6khVixxNVQs" role="2Oq$k0">
                  <ref role="1YBMHb" node="1mAGFBJG0lV" resolve="compositeCondition" />
                </node>
                <node concept="2yIwOk" id="6khVixxNWG3" role="2OqNvi" />
              </node>
              <node concept="2qgKlT" id="6khVixxNXLX" role="2OqNvi">
                <ref role="37wK5l" to="wb6c:6khVixxNOIA" resolve="getUnnecessaryCompositeError" />
              </node>
            </node>
            <node concept="3Cnw8n" id="6khVixxPLB1" role="2OEOjU">
              <ref role="QpYPw" node="6khVixxPrru" resolve="quickfix_RemoveUnnecessaryComposite" />
              <node concept="3CnSsL" id="6khVixxPOlb" role="3Coj4f">
                <ref role="QkamJ" node="6khVixxPrX2" resolve="composite" />
                <node concept="1YBJjd" id="6khVixxPOlo" role="3CoRuB">
                  <ref role="1YBMHb" node="1mAGFBJG0lV" resolve="compositeCondition" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbC" id="6khVixxIEVZ" role="3clFbw">
          <node concept="3cmrfG" id="6khVixxIFlu" role="3uHU7w">
            <property role="3cmrfH" value="1" />
          </node>
          <node concept="2OqwBi" id="6khVixxIAtB" role="3uHU7B">
            <node concept="2OqwBi" id="6khVixxIzyY" role="2Oq$k0">
              <node concept="1YBJjd" id="6khVixxIzn7" role="2Oq$k0">
                <ref role="1YBMHb" node="1mAGFBJG0lV" resolve="compositeCondition" />
              </node>
              <node concept="3Tsc0h" id="6khVixxIzQW" role="2OqNvi">
                <ref role="3TtcxE" to="7f9y:5Wfdz$0ooZ$" resolve="conditions" />
              </node>
            </node>
            <node concept="34oBXx" id="6khVixxICjj" role="2OqNvi" />
          </node>
        </node>
      </node>
      <node concept="3clFbJ" id="4B5aqq8rOGI" role="3cqZAp">
        <node concept="3clFbS" id="4B5aqq8rOGK" role="3clFbx">
          <node concept="a7r0C" id="4B5aqq8sff4" role="3cqZAp">
            <node concept="2OqwBi" id="4B5aqq8shTE" role="a7wSD">
              <node concept="2OqwBi" id="4B5aqq8sfrj" role="2Oq$k0">
                <node concept="1YBJjd" id="4B5aqq8sffm" role="2Oq$k0">
                  <ref role="1YBMHb" node="1mAGFBJG0lV" resolve="compositeCondition" />
                </node>
                <node concept="2yIwOk" id="4B5aqq8sgGc" role="2OqNvi" />
              </node>
              <node concept="2qgKlT" id="4B5aqq8si_P" role="2OqNvi">
                <ref role="37wK5l" to="wb6c:4B5aqq8rRz9" resolve="getRedundantCompositeError" />
              </node>
            </node>
            <node concept="1YBJjd" id="4B5aqq8siDy" role="2OEOjV">
              <ref role="1YBMHb" node="1mAGFBJG0lV" resolve="compositeCondition" />
            </node>
            <node concept="3Cnw8n" id="4B5aqq8tZpq" role="2OEOjU">
              <ref role="QpYPw" node="4B5aqq8siHC" resolve="quickfix_RemoveRedundantComposite" />
              <node concept="3CnSsL" id="4B5aqq8tZPL" role="3Coj4f">
                <ref role="QkamJ" node="4B5aqq8siHX" resolve="composite" />
                <node concept="1YBJjd" id="4B5aqq8tZPY" role="3CoRuB">
                  <ref role="1YBMHb" node="1mAGFBJG0lV" resolve="compositeCondition" />
                </node>
              </node>
              <node concept="3CnSsL" id="4B5aqq8tZQ9" role="3Coj4f">
                <ref role="QkamJ" node="4B5aqq8siIc" resolve="parent" />
                <node concept="1PxgMI" id="4B5aqq8u1u1" role="3CoRuB">
                  <node concept="chp4Y" id="4B5aqq8u1Vd" role="3oSUPX">
                    <ref role="cht4Q" to="7f9y:5Wfdz$0ooZz" resolve="CompositeCondition" />
                  </node>
                  <node concept="2OqwBi" id="4B5aqq8u00d" role="1m5AlR">
                    <node concept="1YBJjd" id="4B5aqq8tZQr" role="2Oq$k0">
                      <ref role="1YBMHb" node="1mAGFBJG0lV" resolve="compositeCondition" />
                    </node>
                    <node concept="1mfA1w" id="4B5aqq8u0GS" role="2OqNvi" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2OqwBi" id="4B5aqq8rPSI" role="3clFbw">
          <node concept="2OqwBi" id="4B5aqq8rOXo" role="2Oq$k0">
            <node concept="1YBJjd" id="4B5aqq8rOLx" role="2Oq$k0">
              <ref role="1YBMHb" node="1mAGFBJG0lV" resolve="compositeCondition" />
            </node>
            <node concept="1mfA1w" id="4B5aqq8rP$9" role="2OqNvi" />
          </node>
          <node concept="1mIQ4w" id="4B5aqq8rQod" role="2OqNvi">
            <node concept="25Kdxt" id="4B5aqq8rQq2" role="cj9EA">
              <node concept="2OqwBi" id="4B5aqq8rQBV" role="25KhWn">
                <node concept="1YBJjd" id="4B5aqq8rQrU" role="2Oq$k0">
                  <ref role="1YBMHb" node="1mAGFBJG0lV" resolve="compositeCondition" />
                </node>
                <node concept="2yIwOk" id="4B5aqq8rRgn" role="2OqNvi" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3clFbF" id="4B5aqq8ue7b" role="3cqZAp">
        <node concept="2OqwBi" id="4B5aqq8uhdC" role="3clFbG">
          <node concept="2OqwBi" id="4B5aqq8uem7" role="2Oq$k0">
            <node concept="1YBJjd" id="4B5aqq8ue79" role="2Oq$k0">
              <ref role="1YBMHb" node="1mAGFBJG0lV" resolve="compositeCondition" />
            </node>
            <node concept="3Tsc0h" id="4B5aqq8ueZM" role="2OqNvi">
              <ref role="3TtcxE" to="7f9y:5Wfdz$0ooZ$" resolve="conditions" />
            </node>
          </node>
          <node concept="2es0OD" id="4B5aqq8ujpd" role="2OqNvi">
            <node concept="1bVj0M" id="4B5aqq8ujpf" role="23t8la">
              <node concept="3clFbS" id="4B5aqq8ujpg" role="1bW5cS">
                <node concept="3clFbJ" id="4B5aqq8u$$Y" role="3cqZAp">
                  <node concept="3clFbS" id="4B5aqq8u$_0" role="3clFbx">
                    <node concept="a7r0C" id="4B5aqq8u_Ba" role="3cqZAp">
                      <node concept="2OqwBi" id="4B5aqq8vvmc" role="a7wSD">
                        <node concept="2OqwBi" id="4B5aqq8vu6X" role="2Oq$k0">
                          <node concept="1YBJjd" id="4B5aqq8uA9Q" role="2Oq$k0">
                            <ref role="1YBMHb" node="1mAGFBJG0lV" resolve="compositeCondition" />
                          </node>
                          <node concept="2yIwOk" id="4B5aqq8vuQc" role="2OqNvi" />
                        </node>
                        <node concept="2qgKlT" id="4B5aqq8vw8U" role="2OqNvi">
                          <ref role="37wK5l" to="wb6c:4B5aqq8uB06" resolve="getRedundantConstituentConditionError" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="4B5aqq8vxq2" role="2OEOjV">
                        <ref role="3cqZAo" node="4B5aqq8ujph" resolve="condition" />
                      </node>
                      <node concept="3Cnw8n" id="4B5aqq8wWa8" role="2OEOjU">
                        <ref role="QpYPw" node="4B5aqq8vxWX" resolve="quickfix_RemoveRedundantConstituentCondition" />
                        <node concept="3CnSsL" id="4B5aqq8wWH2" role="3Coj4f">
                          <ref role="QkamJ" node="4B5aqq8vxXk" resolve="condition" />
                          <node concept="37vLTw" id="4B5aqq8wWSw" role="3CoRuB">
                            <ref role="3cqZAo" node="4B5aqq8ujph" resolve="condition" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="2OqwBi" id="4B5aqq8utmN" role="3clFbw">
                    <node concept="2OqwBi" id="4B5aqq8uoY4" role="2Oq$k0">
                      <node concept="2OqwBi" id="4B5aqq8um1Y" role="2Oq$k0">
                        <node concept="1YBJjd" id="4B5aqq8ulNP" role="2Oq$k0">
                          <ref role="1YBMHb" node="1mAGFBJG0lV" resolve="compositeCondition" />
                        </node>
                        <node concept="3Tsc0h" id="4B5aqq8umHw" role="2OqNvi">
                          <ref role="3TtcxE" to="7f9y:5Wfdz$0ooZ$" resolve="conditions" />
                        </node>
                      </node>
                      <node concept="8ftyA" id="4B5aqq8urbk" role="2OqNvi">
                        <node concept="2OqwBi" id="4B5aqq8urX_" role="8f$Dv">
                          <node concept="37vLTw" id="4B5aqq8urGu" role="2Oq$k0">
                            <ref role="3cqZAo" node="4B5aqq8ujph" resolve="condition" />
                          </node>
                          <node concept="2bSWHS" id="4B5aqq8usEw" role="2OqNvi" />
                        </node>
                      </node>
                    </node>
                    <node concept="2HwmR7" id="4B5aqq8utZq" role="2OqNvi">
                      <node concept="1bVj0M" id="4B5aqq8utZs" role="23t8la">
                        <node concept="3clFbS" id="4B5aqq8utZt" role="1bW5cS">
                          <node concept="3clFbF" id="4B5aqq8uwBU" role="3cqZAp">
                            <node concept="2YFouu" id="4B5aqq8uwBR" role="3clFbG">
                              <node concept="37vLTw" id="4B5aqq8uyZN" role="3uHU7w">
                                <ref role="3cqZAo" node="4B5aqq8ujph" resolve="condition" />
                              </node>
                              <node concept="37vLTw" id="4B5aqq8uxdQ" role="3uHU7B">
                                <ref role="3cqZAo" node="4B5aqq8utZu" resolve="it" />
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="Rh6nW" id="4B5aqq8utZu" role="1bW2Oz">
                          <property role="TrG5h" value="it" />
                          <node concept="2jxLKc" id="4B5aqq8utZv" role="1tU5fm" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="Rh6nW" id="4B5aqq8ujph" role="1bW2Oz">
                <property role="TrG5h" value="condition" />
                <node concept="2jxLKc" id="4B5aqq8ujpi" role="1tU5fm" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="1mAGFBJG0lV" role="1YuTPh">
      <property role="TrG5h" value="compositeCondition" />
      <ref role="1YaFvo" to="7f9y:5Wfdz$0ooZz" resolve="CompositeCondition" />
    </node>
  </node>
  <node concept="18kY7G" id="1mAGFBJJ43q">
    <property role="TrG5h" value="check_AtomicNumberConstraint" />
    <property role="3GE5qa" value="base.constraints.number.atomic" />
    <node concept="3clFbS" id="1mAGFBJJ43r" role="18ibNy">
      <node concept="3clFbJ" id="1mAGFBJJ43x" role="3cqZAp">
        <node concept="2OqwBi" id="1mAGFBJJ4LR" role="3clFbw">
          <node concept="2OqwBi" id="1mAGFBJJ4eM" role="2Oq$k0">
            <node concept="1YBJjd" id="1mAGFBJJ43H" role="2Oq$k0">
              <ref role="1YBMHb" node="1mAGFBJJ43t" resolve="atomicNumberConstraint" />
            </node>
            <node concept="3TrEf2" id="1mAGFBJJ4qg" role="2OqNvi">
              <ref role="3Tt5mk" to="7f9y:1mAGFBJ6NLB" resolve="number" />
            </node>
          </node>
          <node concept="3w_OXm" id="1mAGFBJJ4ZK" role="2OqNvi" />
        </node>
        <node concept="3clFbS" id="1mAGFBJJ43z" role="3clFbx">
          <node concept="2MkqsV" id="1mAGFBJJ520" role="3cqZAp">
            <node concept="2OqwBi" id="1mAGFBJJ60C" role="2MkJ7o">
              <node concept="2OqwBi" id="1mAGFBJJ5md" role="2Oq$k0">
                <node concept="1YBJjd" id="1mAGFBJJ52c" role="2Oq$k0">
                  <ref role="1YBMHb" node="1mAGFBJJ43t" resolve="atomicNumberConstraint" />
                </node>
                <node concept="2yIwOk" id="1mAGFBJJ5A5" role="2OqNvi" />
              </node>
              <node concept="2qgKlT" id="1mAGFBJJ6kx" role="2OqNvi">
                <ref role="37wK5l" to="wb6c:1mAGFBJINRW" resolve="getMissingValueError" />
              </node>
            </node>
            <node concept="1YBJjd" id="1mAGFBJJ6oI" role="2OEOjV">
              <ref role="1YBMHb" node="1mAGFBJJ43t" resolve="atomicNumberConstraint" />
            </node>
            <node concept="2OE7Q9" id="4QUW3ecUAV8" role="2OEWyd">
              <ref role="2OEe5H" to="7f9y:1mAGFBJ6NLB" resolve="number" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="1mAGFBJJ43t" role="1YuTPh">
      <property role="TrG5h" value="atomicNumberConstraint" />
      <ref role="1YaFvo" to="7f9y:1mAGFBJ9goY" resolve="AtomicNumberConstraint" />
    </node>
  </node>
  <node concept="18kY7G" id="4QUW3edHkX7">
    <property role="TrG5h" value="check_IEntityReference" />
    <property role="3GE5qa" value="base.parameters.references" />
    <node concept="3clFbS" id="4QUW3edHkX8" role="18ibNy">
      <node concept="Jncv_" id="4QUW3edEwFk" role="3cqZAp">
        <ref role="JncvD" to="kkto:4QUW3edDqMR" resolve="Entity" />
        <node concept="2OqwBi" id="4QUW3edEwZZ" role="JncvB">
          <node concept="1YBJjd" id="4QUW3edEwS1" role="2Oq$k0">
            <ref role="1YBMHb" node="4QUW3edHkXa" resolve="iEntityReference" />
          </node>
          <node concept="3TrEf2" id="4QUW3efv5Yd" role="2OqNvi">
            <ref role="3Tt5mk" to="7f9y:4QUW3efv2jv" resolve="target" />
          </node>
        </node>
        <node concept="3clFbS" id="4QUW3edEwFo" role="Jncv$">
          <node concept="3clFbJ" id="4QUW3edDJKa" role="3cqZAp">
            <node concept="3clFbS" id="4QUW3edDJKc" role="3clFbx">
              <node concept="2MkqsV" id="4QUW3edDLv1" role="3cqZAp">
                <node concept="1YBJjd" id="4QUW3edJxV8" role="2OEOjV">
                  <ref role="1YBMHb" node="4QUW3edHkXa" resolve="iEntityReference" />
                </node>
                <node concept="2YIFZM" id="4QUW3edDNnh" role="2MkJ7o">
                  <ref role="1Pybhc" to="wyt6:~String" resolve="String" />
                  <ref role="37wK5l" to="wyt6:~String.format(java.lang.String,java.lang.Object...):java.lang.String" resolve="format" />
                  <node concept="Xl_RD" id="4QUW3edDNzs" role="37wK5m">
                    <property role="Xl_RC" value="%s %s" />
                  </node>
                  <node concept="2OqwBi" id="4QUW3edDMhy" role="37wK5m">
                    <node concept="2OqwBi" id="4QUW3edDLBV" role="2Oq$k0">
                      <node concept="1YBJjd" id="4QUW3edJwmX" role="2Oq$k0">
                        <ref role="1YBMHb" node="4QUW3edHkXa" resolve="iEntityReference" />
                      </node>
                      <node concept="2yIwOk" id="4QUW3edJwNw" role="2OqNvi" />
                    </node>
                    <node concept="2qgKlT" id="4QUW3edJxbp" role="2OqNvi">
                      <ref role="37wK5l" to="wb6c:4QUW3edDAjH" resolve="getTargetDeprecatedByError" />
                    </node>
                  </node>
                  <node concept="2OqwBi" id="4QUW3efSxhD" role="37wK5m">
                    <node concept="2OqwBi" id="4QUW3edECBu" role="2Oq$k0">
                      <node concept="Jnkvi" id="4QUW3edJxnn" role="2Oq$k0">
                        <ref role="1M0zk5" node="4QUW3edEwFq" resolve="entity" />
                      </node>
                      <node concept="3TrEf2" id="4QUW3edJxJ7" role="2OqNvi">
                        <ref role="3Tt5mk" to="kkto:4QUW3edDrpg" resolve="deprecatedBy" />
                      </node>
                    </node>
                    <node concept="3TrcHB" id="4QUW3efSxRc" role="2OqNvi">
                      <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                    </node>
                  </node>
                </node>
                <node concept="3Cnw8n" id="4V3GMfXFK9i" role="2OEOjU">
                  <ref role="QpYPw" node="4QUW3edHmb3" resolve="quickfix_ReplaceDeprecatedReference" />
                  <node concept="3CnSsL" id="4V3GMfXFKq4" role="3Coj4f">
                    <ref role="QkamJ" node="4QUW3edHmbo" resolve="reference" />
                    <node concept="1YBJjd" id="6LTgXmMvrew" role="3CoRuB">
                      <ref role="1YBMHb" node="4QUW3edHkXa" resolve="iEntityReference" />
                    </node>
                  </node>
                  <node concept="3CnSsL" id="4V3GMfXFKq_" role="3Coj4f">
                    <ref role="QkamJ" node="4QUW3edHmbB" resolve="replacement" />
                    <node concept="2OqwBi" id="4B5aqq8y6de" role="3CoRuB">
                      <node concept="Jnkvi" id="4V3GMfXFKqR" role="2Oq$k0">
                        <ref role="1M0zk5" node="4QUW3edEwFq" resolve="entity" />
                      </node>
                      <node concept="3TrEf2" id="4B5aqq8y6LE" role="2OqNvi">
                        <ref role="3Tt5mk" to="kkto:4QUW3edDrpg" resolve="deprecatedBy" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="2OqwBi" id="4QUW3edDL1r" role="3clFbw">
              <node concept="2OqwBi" id="4QUW3edDKqF" role="2Oq$k0">
                <node concept="3TrEf2" id="4QUW3edDKGn" role="2OqNvi">
                  <ref role="3Tt5mk" to="kkto:4QUW3edDrpg" resolve="deprecatedBy" />
                </node>
                <node concept="Jnkvi" id="4QUW3edEzlc" role="2Oq$k0">
                  <ref role="1M0zk5" node="4QUW3edEwFq" resolve="entity" />
                </node>
              </node>
              <node concept="3x8VRR" id="4QUW3edDLjm" role="2OqNvi" />
            </node>
          </node>
        </node>
        <node concept="JncvC" id="4QUW3edEwFq" role="JncvA">
          <property role="TrG5h" value="entity" />
          <node concept="2jxLKc" id="4QUW3edEwFr" role="1tU5fm" />
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="4QUW3edHkXa" role="1YuTPh">
      <property role="TrG5h" value="iEntityReference" />
      <ref role="1YaFvo" to="7f9y:6LTgXmMvpq9" resolve="IEntityReference" />
    </node>
  </node>
  <node concept="Q5z_Y" id="4QUW3edHmb3">
    <property role="3GE5qa" value="base.parameters.references" />
    <property role="TrG5h" value="quickfix_ReplaceDeprecatedReference" />
    <node concept="Q6JDH" id="4QUW3edHmbo" role="Q6Id_">
      <property role="TrG5h" value="reference" />
      <node concept="3Tqbb2" id="4QUW3edHmbu" role="Q6QK4">
        <ref role="ehGHo" to="7f9y:6LTgXmMvpq9" resolve="IEntityReference" />
      </node>
    </node>
    <node concept="Q6JDH" id="4QUW3edHmbB" role="Q6Id_">
      <property role="TrG5h" value="replacement" />
      <node concept="3Tqbb2" id="4QUW3edHmbJ" role="Q6QK4">
        <ref role="ehGHo" to="kkto:4QUW3edDqMR" resolve="Entity" />
      </node>
    </node>
    <node concept="Q5ZZ6" id="4QUW3edHmb4" role="Q6x$H">
      <node concept="3clFbS" id="4QUW3edHmb5" role="2VODD2">
        <node concept="3clFbF" id="4QUW3edHmcb" role="3cqZAp">
          <node concept="37vLTI" id="4QUW3edHmPN" role="3clFbG">
            <node concept="QwW4i" id="4QUW3eeXS60" role="37vLTx">
              <ref role="QwW4h" node="4QUW3edHmbB" resolve="replacement" />
            </node>
            <node concept="2OqwBi" id="4QUW3edHmlz" role="37vLTJ">
              <node concept="QwW4i" id="4QUW3eeXSa_" role="2Oq$k0">
                <ref role="QwW4h" node="4QUW3edHmbo" resolve="reference" />
              </node>
              <node concept="3TrEf2" id="4V3GMfXFLWz" role="2OqNvi">
                <ref role="3Tt5mk" to="7f9y:4QUW3efv2jv" resolve="target" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="QznSV" id="4QUW3edHrW8" role="QzAvj">
      <node concept="3clFbS" id="4QUW3edHrW9" role="2VODD2">
        <node concept="3clFbF" id="4QUW3edHs4I" role="3cqZAp">
          <node concept="2OqwBi" id="4QUW3edHtn2" role="3clFbG">
            <node concept="2OqwBi" id="4QUW3edHsoF" role="2Oq$k0">
              <node concept="QwW4i" id="4QUW3edHs4H" role="2Oq$k0">
                <ref role="QwW4h" node="4QUW3edHmbo" resolve="reference" />
              </node>
              <node concept="2yIwOk" id="4QUW3edHsKj" role="2OqNvi" />
            </node>
            <node concept="2qgKlT" id="6LTgXmMvs61" role="2OqNvi">
              <ref role="37wK5l" to="wb6c:4QUW3edDBer" resolve="getReplaceDeprecatedTargetQuickFixDescription" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="18kY7G" id="2XLt5KU_lZG">
    <property role="TrG5h" value="check_Rule" />
    <property role="3GE5qa" value="base" />
    <node concept="3clFbS" id="2XLt5KU_lZH" role="18ibNy">
      <node concept="3clFbJ" id="2XLt5KUAOK7" role="3cqZAp">
        <node concept="3clFbS" id="2XLt5KUAOK9" role="3clFbx">
          <node concept="2MkqsV" id="2XLt5KUBb5e" role="3cqZAp">
            <node concept="2OqwBi" id="2XLt5KUBdyy" role="2MkJ7o">
              <node concept="2OqwBi" id="2XLt5KUBbhn" role="2Oq$k0">
                <node concept="1YBJjd" id="2XLt5KUBb5t" role="2Oq$k0">
                  <ref role="1YBMHb" node="2XLt5KU_lZJ" resolve="rule" />
                </node>
                <node concept="2yIwOk" id="2XLt5KUBd6l" role="2OqNvi" />
              </node>
              <node concept="2qgKlT" id="2XLt5KUBdVf" role="2OqNvi">
                <ref role="37wK5l" to="wb6c:2XLt5KUB66k" resolve="getMissingNameError" />
              </node>
            </node>
            <node concept="1YBJjd" id="2XLt5KUBdZC" role="2OEOjV">
              <ref role="1YBMHb" node="2XLt5KU_lZJ" resolve="rule" />
            </node>
          </node>
        </node>
        <node concept="2OqwBi" id="2XLt5KUAPU0" role="3clFbw">
          <node concept="2OqwBi" id="2XLt5KUAP0Y" role="2Oq$k0">
            <node concept="1YBJjd" id="2XLt5KUAOP7" role="2Oq$k0">
              <ref role="1YBMHb" node="2XLt5KU_lZJ" resolve="rule" />
            </node>
            <node concept="3TrcHB" id="2XLt5KUAPlI" role="2OqNvi">
              <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
            </node>
          </node>
          <node concept="17RlXB" id="2XLt5KUAQgT" role="2OqNvi" />
        </node>
      </node>
      <node concept="3clFbJ" id="2XLt5KU_lZN" role="3cqZAp">
        <node concept="2OqwBi" id="2XLt5KU_poj" role="3clFbw">
          <node concept="2OqwBi" id="2XLt5KU_mbQ" role="2Oq$k0">
            <node concept="1YBJjd" id="2XLt5KU_lZZ" role="2Oq$k0">
              <ref role="1YBMHb" node="2XLt5KU_lZJ" resolve="rule" />
            </node>
            <node concept="3Tsc0h" id="2XLt5KU_moO" role="2OqNvi">
              <ref role="3TtcxE" to="7f9y:1mAGFBL9ZsO" resolve="actions" />
            </node>
          </node>
          <node concept="1v1jN8" id="2XLt5KU_r9e" role="2OqNvi" />
        </node>
        <node concept="3clFbS" id="2XLt5KU_lZP" role="3clFbx">
          <node concept="2MkqsV" id="2XLt5KU_GPy" role="3cqZAp">
            <node concept="2OqwBi" id="2XLt5KU_HHP" role="2MkJ7o">
              <node concept="2OqwBi" id="2XLt5KU_H1C" role="2Oq$k0">
                <node concept="1YBJjd" id="2XLt5KU_GPI" role="2Oq$k0">
                  <ref role="1YBMHb" node="2XLt5KU_lZJ" resolve="rule" />
                </node>
                <node concept="2yIwOk" id="2XLt5KU_HhC" role="2OqNvi" />
              </node>
              <node concept="2qgKlT" id="2XLt5KU_I1x" role="2OqNvi">
                <ref role="37wK5l" to="wb6c:2XLt5KU_rbM" resolve="getMissingActionsError" />
              </node>
            </node>
            <node concept="1YBJjd" id="2XLt5KU_I5U" role="2OEOjV">
              <ref role="1YBMHb" node="2XLt5KU_lZJ" resolve="rule" />
            </node>
            <node concept="2OE7Q9" id="2XLt5KU_IeC" role="2OEWyd">
              <ref role="2OEe5H" to="7f9y:1mAGFBL9ZsO" resolve="actions" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="2XLt5KU_lZJ" role="1YuTPh">
      <property role="TrG5h" value="rule" />
      <ref role="1YaFvo" to="7f9y:5Wfdz$0omZg" resolve="Rule" />
    </node>
  </node>
  <node concept="18kY7G" id="2XLt5KUFvhb">
    <property role="TrG5h" value="check_Condition" />
    <property role="3GE5qa" value="base.conditions" />
    <node concept="3clFbS" id="2XLt5KUFvhc" role="18ibNy">
      <node concept="3clFbJ" id="2XLt5KUFvC4" role="3cqZAp">
        <node concept="2OqwBi" id="2XLt5KUFwpa" role="3clFbw">
          <node concept="2OqwBi" id="2XLt5KUFvNl" role="2Oq$k0">
            <node concept="1YBJjd" id="2XLt5KUFvCg" role="2Oq$k0">
              <ref role="1YBMHb" node="2XLt5KUFvhe" resolve="condition" />
            </node>
            <node concept="2yIwOk" id="2XLt5KUFvYN" role="2OqNvi" />
          </node>
          <node concept="3O6GUB" id="2XLt5KUFwCV" role="2OqNvi">
            <node concept="chp4Y" id="2XLt5KUFwGS" role="3QVz_e">
              <ref role="cht4Q" to="7f9y:5Wfdz$0ooZy" resolve="Condition" />
            </node>
          </node>
        </node>
        <node concept="3clFbS" id="2XLt5KUFvC6" role="3clFbx">
          <node concept="2MkqsV" id="2XLt5KUFwL_" role="3cqZAp">
            <node concept="2OqwBi" id="2XLt5KUFxAR" role="2MkJ7o">
              <node concept="2OqwBi" id="2XLt5KUFwWT" role="2Oq$k0">
                <node concept="1YBJjd" id="2XLt5KUFwLL" role="2Oq$k0">
                  <ref role="1YBMHb" node="2XLt5KUFvhe" resolve="condition" />
                </node>
                <node concept="2yIwOk" id="2XLt5KUFxco" role="2OqNvi" />
              </node>
              <node concept="2qgKlT" id="2XLt5KUFxUz" role="2OqNvi">
                <ref role="37wK5l" to="wb6c:3bOTPdWRMMD" resolve="getMissingConditionError" />
              </node>
            </node>
            <node concept="1YBJjd" id="2XLt5KUFxYN" role="2OEOjV">
              <ref role="1YBMHb" node="2XLt5KUFvhe" resolve="condition" />
            </node>
          </node>
          <node concept="3cpWs6" id="4B5aqq7LxWw" role="3cqZAp" />
        </node>
      </node>
      <node concept="3clFbH" id="6khVixxBKIZ" role="3cqZAp" />
      <node concept="3SKdUt" id="6khVixxBKAO" role="3cqZAp">
        <node concept="3SKdUq" id="6khVixxBKAQ" role="3SKWNk">
          <property role="3SKdUp" value="Check contradictions in AllOf conditions" />
        </node>
      </node>
      <node concept="3clFbJ" id="6khVixxthAH" role="3cqZAp">
        <node concept="3clFbS" id="6khVixxthAJ" role="3clFbx">
          <node concept="3clFbF" id="6khVixxtnst" role="3cqZAp">
            <node concept="2OqwBi" id="6khVixxtqvN" role="3clFbG">
              <node concept="2OqwBi" id="6khVixxzYos" role="2Oq$k0">
                <node concept="3zZkjj" id="6khVixxzZbK" role="2OqNvi">
                  <node concept="1bVj0M" id="6khVixxzZbM" role="23t8la">
                    <node concept="3clFbS" id="6khVixxzZbN" role="1bW5cS">
                      <node concept="3clFbF" id="6khVixxzZIl" role="3cqZAp">
                        <node concept="2OqwBi" id="6khVixx$0gb" role="3clFbG">
                          <node concept="37vLTw" id="6khVixxzZIk" role="2Oq$k0">
                            <ref role="3cqZAo" node="6khVixxzZbO" resolve="it" />
                          </node>
                          <node concept="2qgKlT" id="6khVixx$19d" role="2OqNvi">
                            <ref role="37wK5l" to="wb6c:6khVixxtxTv" resolve="isNegationOf" />
                            <node concept="1YBJjd" id="6khVixx$1Ey" role="37wK5m">
                              <ref role="1YBMHb" node="2XLt5KUFvhe" resolve="condition" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="Rh6nW" id="6khVixxzZbO" role="1bW2Oz">
                      <property role="TrG5h" value="it" />
                      <node concept="2jxLKc" id="6khVixxzZbP" role="1tU5fm" />
                    </node>
                  </node>
                </node>
                <node concept="2OqwBi" id="6khVixxGOAZ" role="2Oq$k0">
                  <node concept="1YBJjd" id="6khVixxGNVJ" role="2Oq$k0">
                    <ref role="1YBMHb" node="2XLt5KUFvhe" resolve="condition" />
                  </node>
                  <node concept="2qgKlT" id="6khVixxKQ3B" role="2OqNvi">
                    <ref role="37wK5l" to="wb6c:6khVixxCqR5" resolve="findRuleConjugates" />
                  </node>
                </node>
              </node>
              <node concept="2es0OD" id="6khVixxtqUp" role="2OqNvi">
                <node concept="1bVj0M" id="6khVixxtqUr" role="23t8la">
                  <node concept="3clFbS" id="6khVixxtqUs" role="1bW5cS">
                    <node concept="3cpWs8" id="6khVixxAZZI" role="3cqZAp">
                      <node concept="3cpWsn" id="6khVixxAZZL" role="3cpWs9">
                        <property role="TrG5h" value="parent" />
                        <node concept="3Tqbb2" id="6khVixxAZZG" role="1tU5fm">
                          <ref role="ehGHo" to="7f9y:1mAGFBJeyRH" resolve="NamedCondition" />
                        </node>
                        <node concept="2OqwBi" id="6khVixxB3dT" role="33vP2m">
                          <node concept="37vLTw" id="6khVixxB2H4" role="2Oq$k0">
                            <ref role="3cqZAo" node="6khVixxtqUt" resolve="it" />
                          </node>
                          <node concept="2Xjw5R" id="6khVixxB48s" role="2OqNvi">
                            <node concept="1xMEDy" id="6khVixxB48u" role="1xVPHs">
                              <node concept="chp4Y" id="6khVixxB4GK" role="ri$Ld">
                                <ref role="cht4Q" to="7f9y:1mAGFBJeyRH" resolve="NamedCondition" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbJ" id="6khVixxB9sz" role="3cqZAp">
                      <node concept="3clFbS" id="6khVixxB9s_" role="3clFbx">
                        <node concept="2MkqsV" id="6khVixxIykc" role="3cqZAp">
                          <node concept="1YBJjd" id="6khVixxIykf" role="2OEOjV">
                            <ref role="1YBMHb" node="2XLt5KUFvhe" resolve="condition" />
                          </node>
                          <node concept="2OqwBi" id="6khVixxLI6l" role="2MkJ7o">
                            <node concept="2OqwBi" id="6khVixxLG_g" role="2Oq$k0">
                              <node concept="1YBJjd" id="6khVixxLG8i" role="2Oq$k0">
                                <ref role="1YBMHb" node="2XLt5KUFvhe" resolve="condition" />
                              </node>
                              <node concept="2yIwOk" id="6khVixxLHq3" role="2OqNvi" />
                            </node>
                            <node concept="2qgKlT" id="6khVixxLINy" role="2OqNvi">
                              <ref role="37wK5l" to="wb6c:6khVixxLDQJ" resolve="getContradictoryConditionInRuleError" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="2OqwBi" id="6khVixxBaCy" role="3clFbw">
                        <node concept="37vLTw" id="6khVixxB9ZZ" role="2Oq$k0">
                          <ref role="3cqZAo" node="6khVixxAZZL" resolve="parent" />
                        </node>
                        <node concept="3w_OXm" id="6khVixxBbom" role="2OqNvi" />
                      </node>
                      <node concept="9aQIb" id="6khVixxBd2S" role="9aQIa">
                        <node concept="3clFbS" id="6khVixxBd2T" role="9aQI4">
                          <node concept="2MkqsV" id="6khVixxIyHD" role="3cqZAp">
                            <node concept="2YIFZM" id="6khVixxIyHF" role="2MkJ7o">
                              <ref role="1Pybhc" to="wyt6:~String" resolve="String" />
                              <ref role="37wK5l" to="wyt6:~String.format(java.lang.String,java.lang.Object...):java.lang.String" resolve="format" />
                              <node concept="Xl_RD" id="6khVixxIyHG" role="37wK5m">
                                <property role="Xl_RC" value="%s '%s'" />
                              </node>
                              <node concept="2OqwBi" id="6khVixxLN2U" role="37wK5m">
                                <node concept="2OqwBi" id="6khVixxLLPW" role="2Oq$k0">
                                  <node concept="1YBJjd" id="6khVixxLLtS" role="2Oq$k0">
                                    <ref role="1YBMHb" node="2XLt5KUFvhe" resolve="condition" />
                                  </node>
                                  <node concept="2yIwOk" id="6khVixxLMrp" role="2OqNvi" />
                                </node>
                                <node concept="2qgKlT" id="6khVixxLNGp" role="2OqNvi">
                                  <ref role="37wK5l" to="wb6c:6khVixxLDbT" resolve="getContradictoryConditionInNamedConditionError" />
                                </node>
                              </node>
                              <node concept="2OqwBi" id="6khVixxIyHH" role="37wK5m">
                                <node concept="37vLTw" id="6khVixxIyHI" role="2Oq$k0">
                                  <ref role="3cqZAo" node="6khVixxAZZL" resolve="parent" />
                                </node>
                                <node concept="3TrcHB" id="6khVixxIyHJ" role="2OqNvi">
                                  <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                                </node>
                              </node>
                            </node>
                            <node concept="1YBJjd" id="6khVixxIyHK" role="2OEOjV">
                              <ref role="1YBMHb" node="2XLt5KUFvhe" resolve="condition" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="Rh6nW" id="6khVixxtqUt" role="1bW2Oz">
                    <property role="TrG5h" value="it" />
                    <node concept="2jxLKc" id="6khVixxtqUu" role="1tU5fm" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="6khVixxIMgV" role="3cqZAp">
            <node concept="2OqwBi" id="6khVixxIU$X" role="3clFbG">
              <node concept="2OqwBi" id="6khVixxIRiK" role="2Oq$k0">
                <node concept="3zZkjj" id="6khVixxITvS" role="2OqNvi">
                  <node concept="1bVj0M" id="6khVixxITvU" role="23t8la">
                    <node concept="3clFbS" id="6khVixxITvV" role="1bW5cS">
                      <node concept="3clFbF" id="6khVixxITBV" role="3cqZAp">
                        <node concept="2OqwBi" id="6khVixxITQ2" role="3clFbG">
                          <node concept="37vLTw" id="6khVixxITBU" role="2Oq$k0">
                            <ref role="3cqZAo" node="6khVixxITvW" resolve="it" />
                          </node>
                          <node concept="2qgKlT" id="6khVixxIU8h" role="2OqNvi">
                            <ref role="37wK5l" to="wb6c:6khVixxtxTv" resolve="isNegationOf" />
                            <node concept="1YBJjd" id="6khVixxIUhN" role="37wK5m">
                              <ref role="1YBMHb" node="2XLt5KUFvhe" resolve="condition" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="Rh6nW" id="6khVixxITvW" role="1bW2Oz">
                      <property role="TrG5h" value="it" />
                      <node concept="2jxLKc" id="6khVixxITvX" role="1tU5fm" />
                    </node>
                  </node>
                </node>
                <node concept="2OqwBi" id="6khVixxKRvq" role="2Oq$k0">
                  <node concept="1YBJjd" id="6khVixxKR26" role="2Oq$k0">
                    <ref role="1YBMHb" node="2XLt5KUFvhe" resolve="condition" />
                  </node>
                  <node concept="2qgKlT" id="6khVixxKS7A" role="2OqNvi">
                    <ref role="37wK5l" to="wb6c:6khVixxFsLU" resolve="findPreconditionConjungates" />
                  </node>
                </node>
              </node>
              <node concept="2es0OD" id="6khVixxIVf6" role="2OqNvi">
                <node concept="1bVj0M" id="6khVixxIVf8" role="23t8la">
                  <node concept="3clFbS" id="6khVixxIVf9" role="1bW5cS">
                    <node concept="2MkqsV" id="6khVixxL1kS" role="3cqZAp">
                      <node concept="1YBJjd" id="6khVixxL1kV" role="2OEOjV">
                        <ref role="1YBMHb" node="2XLt5KUFvhe" resolve="condition" />
                      </node>
                      <node concept="2OqwBi" id="6khVixxLQoB" role="2MkJ7o">
                        <node concept="2OqwBi" id="6khVixxLOYX" role="2Oq$k0">
                          <node concept="1YBJjd" id="6khVixxLO_Q" role="2Oq$k0">
                            <ref role="1YBMHb" node="2XLt5KUFvhe" resolve="condition" />
                          </node>
                          <node concept="2yIwOk" id="6khVixxLP$h" role="2OqNvi" />
                        </node>
                        <node concept="2qgKlT" id="6khVixxLR26" role="2OqNvi">
                          <ref role="37wK5l" to="wb6c:6khVixxLEjw" resolve="getContradictoryConditionInPreconditionError" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="Rh6nW" id="6khVixxIVfa" role="1bW2Oz">
                    <property role="TrG5h" value="it" />
                    <node concept="2jxLKc" id="6khVixxIVfb" role="1tU5fm" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3fqX7Q" id="6khVixxtixW" role="3clFbw">
          <node concept="2OqwBi" id="6khVixxtixY" role="3fr31v">
            <node concept="1YBJjd" id="6khVixxtixZ" role="2Oq$k0">
              <ref role="1YBMHb" node="2XLt5KUFvhe" resolve="condition" />
            </node>
            <node concept="1mIQ4w" id="6khVixxtiy0" role="2OqNvi">
              <node concept="chp4Y" id="6khVixxtiy1" role="cj9EA">
                <ref role="cht4Q" to="7f9y:5Wfdz$0ooZz" resolve="CompositeCondition" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="2XLt5KUFvhe" role="1YuTPh">
      <property role="TrG5h" value="condition" />
      <ref role="1YaFvo" to="7f9y:5Wfdz$0ooZy" resolve="Condition" />
    </node>
  </node>
  <node concept="Q5z_Y" id="6khVixxPrru">
    <property role="3GE5qa" value="base.conditions.composites" />
    <property role="TrG5h" value="quickfix_RemoveUnnecessaryComposite" />
    <node concept="Q5ZZ6" id="6khVixxPrrv" role="Q6x$H">
      <node concept="3clFbS" id="6khVixxPrrw" role="2VODD2">
        <node concept="3clFbF" id="6khVixxPrrO" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixxPs7q" role="3clFbG">
            <node concept="QwW4i" id="6khVixxPrXC" role="2Oq$k0">
              <ref role="QwW4h" node="6khVixxPrX2" resolve="composite" />
            </node>
            <node concept="1P9Npp" id="6khVixxPsk_" role="2OqNvi">
              <node concept="2OqwBi" id="6khVixxPviM" role="1P9ThW">
                <node concept="2OqwBi" id="6khVixxPswK" role="2Oq$k0">
                  <node concept="QwW4i" id="6khVixxPsmJ" role="2Oq$k0">
                    <ref role="QwW4h" node="6khVixxPrX2" resolve="composite" />
                  </node>
                  <node concept="3Tsc0h" id="6khVixxPsI8" role="2OqNvi">
                    <ref role="3TtcxE" to="7f9y:5Wfdz$0ooZ$" resolve="conditions" />
                  </node>
                </node>
                <node concept="1uHKPH" id="6khVixxPx5_" role="2OqNvi" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="Q6JDH" id="6khVixxPrX2" role="Q6Id_">
      <property role="TrG5h" value="composite" />
      <node concept="3Tqbb2" id="6khVixxPrX8" role="Q6QK4">
        <ref role="ehGHo" to="7f9y:5Wfdz$0ooZz" resolve="CompositeCondition" />
      </node>
    </node>
    <node concept="QznSV" id="6khVixxPQD0" role="QzAvj">
      <node concept="3clFbS" id="6khVixxPQD1" role="2VODD2">
        <node concept="3clFbF" id="6khVixxQ8rf" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixxQ9_L" role="3clFbG">
            <node concept="2OqwBi" id="6khVixxQ8Ft" role="2Oq$k0">
              <node concept="QwW4i" id="6khVixxQ8re" role="2Oq$k0">
                <ref role="QwW4h" node="6khVixxPrX2" resolve="composite" />
              </node>
              <node concept="2yIwOk" id="6khVixxQ94_" role="2OqNvi" />
            </node>
            <node concept="2qgKlT" id="6khVixxQxEj" role="2OqNvi">
              <ref role="37wK5l" to="wb6c:6khVixxQaQd" resolve="getRemoveUnnecessaryCompositeQuickFixDescription" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="18kY7G" id="4B5aqq7n9Fr">
    <property role="TrG5h" value="check_RuleSet" />
    <property role="3GE5qa" value="base" />
    <node concept="3clFbS" id="4B5aqq7n9Fs" role="18ibNy">
      <node concept="3clFbJ" id="4B5aqq7n9Fy" role="3cqZAp">
        <node concept="3clFbS" id="4B5aqq7n9F$" role="3clFbx">
          <node concept="2MkqsV" id="4B5aqq7ndRy" role="3cqZAp">
            <node concept="2OqwBi" id="4B5aqq7nhfY" role="2MkJ7o">
              <node concept="2OqwBi" id="4B5aqq7ngoM" role="2Oq$k0">
                <node concept="1YBJjd" id="4B5aqq7ndRI" role="2Oq$k0">
                  <ref role="1YBMHb" node="4B5aqq7n9Fu" resolve="ruleSet" />
                </node>
                <node concept="2yIwOk" id="4B5aqq7ngI4" role="2OqNvi" />
              </node>
              <node concept="2qgKlT" id="4B5aqq7nhBc" role="2OqNvi">
                <ref role="37wK5l" to="wb6c:4B5aqq7nfr2" resolve="getInvalidNameError" />
              </node>
            </node>
            <node concept="1YBJjd" id="4B5aqq7nexk" role="2OEOjV">
              <ref role="1YBMHb" node="4B5aqq7n9Fu" resolve="ruleSet" />
            </node>
            <node concept="2ODE4t" id="4B5aqq7nhH8" role="2OEWyd">
              <ref role="2ODJFN" to="tpck:h0TrG11" resolve="name" />
            </node>
          </node>
          <node concept="3cpWs6" id="4B5aqq7EikZ" role="3cqZAp" />
        </node>
        <node concept="3fqX7Q" id="4B5aqq7pMtz" role="3clFbw">
          <node concept="2OqwBi" id="4B5aqq7pMJx" role="3fr31v">
            <node concept="1YBJjd" id="4B5aqq7pMy0" role="2Oq$k0">
              <ref role="1YBMHb" node="4B5aqq7n9Fu" resolve="ruleSet" />
            </node>
            <node concept="2qgKlT" id="4B5aqq7pN49" role="2OqNvi">
              <ref role="37wK5l" to="wb6c:4B5aqq7pKpa" resolve="hasValidName" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3clFbJ" id="4B5aqq7Eipo" role="3cqZAp">
        <node concept="3clFbS" id="4B5aqq7Eipq" role="3clFbx">
          <node concept="2MkqsV" id="4B5aqq7Et9p" role="3cqZAp">
            <node concept="2OqwBi" id="4B5aqq7EukB" role="2MkJ7o">
              <node concept="2OqwBi" id="4B5aqq7Etn7" role="2Oq$k0">
                <node concept="1YBJjd" id="4B5aqq7Et9C" role="2Oq$k0">
                  <ref role="1YBMHb" node="4B5aqq7n9Fu" resolve="ruleSet" />
                </node>
                <node concept="2yIwOk" id="4B5aqq7EtOA" role="2OqNvi" />
              </node>
              <node concept="2qgKlT" id="4B5aqq7FU6b" role="2OqNvi">
                <ref role="37wK5l" to="wb6c:4B5aqq7ECES" resolve="getDuplicateNameError" />
              </node>
            </node>
            <node concept="1YBJjd" id="4B5aqq7EuSK" role="2OEOjV">
              <ref role="1YBMHb" node="4B5aqq7n9Fu" resolve="ruleSet" />
            </node>
            <node concept="2ODE4t" id="4B5aqq7Evb7" role="2OEWyd">
              <ref role="2ODJFN" to="tpck:h0TrG11" resolve="name" />
            </node>
          </node>
        </node>
        <node concept="2OqwBi" id="4B5aqq7KGIk" role="3clFbw">
          <node concept="1YBJjd" id="4B5aqq7KGv$" role="2Oq$k0">
            <ref role="1YBMHb" node="4B5aqq7n9Fu" resolve="ruleSet" />
          </node>
          <node concept="2qgKlT" id="4B5aqq7KH8p" role="2OqNvi">
            <ref role="37wK5l" to="wb6c:4B5aqq7JjCD" resolve="hasNonUniqueName" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="4B5aqq7n9Fu" role="1YuTPh">
      <property role="TrG5h" value="ruleSet" />
      <ref role="1YaFvo" to="7f9y:5Wfdz$0omZf" resolve="RuleSet" />
    </node>
  </node>
  <node concept="18kY7G" id="4B5aqq7LDRh">
    <property role="TrG5h" value="check_Constraint" />
    <property role="3GE5qa" value="base.constraints" />
    <node concept="3clFbS" id="4B5aqq7LDRi" role="18ibNy">
      <node concept="3clFbJ" id="4B5aqq7LDRo" role="3cqZAp">
        <node concept="2OqwBi" id="4B5aqq7LENz" role="3clFbw">
          <node concept="2OqwBi" id="4B5aqq7LE2D" role="2Oq$k0">
            <node concept="1YBJjd" id="4B5aqq7LDR$" role="2Oq$k0">
              <ref role="1YBMHb" node="4B5aqq7LDRk" resolve="constraint" />
            </node>
            <node concept="2yIwOk" id="4B5aqq7LEpc" role="2OqNvi" />
          </node>
          <node concept="3O6GUB" id="4B5aqq7LF3i" role="2OqNvi">
            <node concept="chp4Y" id="4B5aqq7LF6A" role="3QVz_e">
              <ref role="cht4Q" to="7f9y:1mAGFBKqQs5" resolve="Constraint" />
            </node>
          </node>
        </node>
        <node concept="3clFbS" id="4B5aqq7LDRq" role="3clFbx">
          <node concept="2MkqsV" id="4B5aqq7LFsv" role="3cqZAp">
            <node concept="2OqwBi" id="4B5aqq7LFsw" role="2MkJ7o">
              <node concept="2OqwBi" id="4B5aqq7LFsx" role="2Oq$k0">
                <node concept="2yIwOk" id="4B5aqq7MKXV" role="2OqNvi" />
                <node concept="1YBJjd" id="4B5aqq7MKIq" role="2Oq$k0">
                  <ref role="1YBMHb" node="4B5aqq7LDRk" resolve="constraint" />
                </node>
              </node>
              <node concept="2qgKlT" id="4B5aqq7MLgV" role="2OqNvi">
                <ref role="37wK5l" to="wb6c:4B5aqq7LFRg" resolve="getMissingConstraintError" />
              </node>
            </node>
            <node concept="1YBJjd" id="4B5aqq7MeDP" role="2OEOjV">
              <ref role="1YBMHb" node="4B5aqq7LDRk" resolve="constraint" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="4B5aqq7LDRk" role="1YuTPh">
      <property role="TrG5h" value="constraint" />
      <ref role="1YaFvo" to="7f9y:1mAGFBKqQs5" resolve="Constraint" />
    </node>
  </node>
  <node concept="Q5z_Y" id="4B5aqq8siHC">
    <property role="3GE5qa" value="base.conditions.composites" />
    <property role="TrG5h" value="quickfix_RemoveRedundantComposite" />
    <node concept="Q5ZZ6" id="4B5aqq8siHD" role="Q6x$H">
      <node concept="3clFbS" id="4B5aqq8siHE" role="2VODD2">
        <node concept="3cpWs8" id="4B5aqq8snIg" role="3cqZAp">
          <node concept="3cpWsn" id="4B5aqq8snIj" role="3cpWs9">
            <property role="TrG5h" value="index" />
            <node concept="10Oyi0" id="4B5aqq8snIe" role="1tU5fm" />
            <node concept="2OqwBi" id="4B5aqq8snVg" role="33vP2m">
              <node concept="QwW4i" id="4B5aqq8snJh" role="2Oq$k0">
                <ref role="QwW4h" node="4B5aqq8siHX" resolve="composite" />
              </node>
              <node concept="2bSWHS" id="4B5aqq8so8F" role="2OqNvi" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4B5aqq8siIE" role="3cqZAp">
          <node concept="2OqwBi" id="4B5aqq8siSG" role="3clFbG">
            <node concept="QwW4i" id="4B5aqq8siID" role="2Oq$k0">
              <ref role="QwW4h" node="4B5aqq8siHX" resolve="composite" />
            </node>
            <node concept="3YRAZt" id="4B5aqq8sjkc" role="2OqNvi" />
          </node>
        </node>
        <node concept="3clFbF" id="4B5aqq8sjl8" role="3cqZAp">
          <node concept="2OqwBi" id="4B5aqq8slWD" role="3clFbG">
            <node concept="2OqwBi" id="4B5aqq8sjxm" role="2Oq$k0">
              <node concept="QwW4i" id="4B5aqq8sjl6" role="2Oq$k0">
                <ref role="QwW4h" node="4B5aqq8siIc" resolve="parent" />
              </node>
              <node concept="3Tsc0h" id="4B5aqq8sjIx" role="2OqNvi">
                <ref role="3TtcxE" to="7f9y:5Wfdz$0ooZ$" resolve="conditions" />
              </node>
            </node>
            <node concept="liA8E" id="4B5aqq8spRl" role="2OqNvi">
              <ref role="37wK5l" to="33ny:~List.addAll(int,java.util.Collection):boolean" resolve="addAll" />
              <node concept="37vLTw" id="4B5aqq8spWX" role="37wK5m">
                <ref role="3cqZAo" node="4B5aqq8snIj" resolve="index" />
              </node>
              <node concept="2OqwBi" id="4B5aqq8sql7" role="37wK5m">
                <node concept="QwW4i" id="4B5aqq8sq6G" role="2Oq$k0">
                  <ref role="QwW4h" node="4B5aqq8siHX" resolve="composite" />
                </node>
                <node concept="3Tsc0h" id="4B5aqq8sqDC" role="2OqNvi">
                  <ref role="3TtcxE" to="7f9y:5Wfdz$0ooZ$" resolve="conditions" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="Q6JDH" id="4B5aqq8siHX" role="Q6Id_">
      <property role="TrG5h" value="composite" />
      <node concept="3Tqbb2" id="4B5aqq8siI3" role="Q6QK4">
        <ref role="ehGHo" to="7f9y:5Wfdz$0ooZz" resolve="CompositeCondition" />
      </node>
    </node>
    <node concept="Q6JDH" id="4B5aqq8siIc" role="Q6Id_">
      <property role="TrG5h" value="parent" />
      <node concept="3Tqbb2" id="4B5aqq8siIk" role="Q6QK4">
        <ref role="ehGHo" to="7f9y:5Wfdz$0ooZz" resolve="CompositeCondition" />
      </node>
    </node>
    <node concept="QznSV" id="4B5aqq8tPo8" role="QzAvj">
      <node concept="3clFbS" id="4B5aqq8tPo9" role="2VODD2">
        <node concept="3clFbF" id="4B5aqq8tPwL" role="3cqZAp">
          <node concept="2OqwBi" id="4B5aqq8tQ7t" role="3clFbG">
            <node concept="35c_gC" id="4B5aqq8tPwK" role="2Oq$k0">
              <ref role="35c_gD" to="7f9y:5Wfdz$0ooZz" resolve="CompositeCondition" />
            </node>
            <node concept="2qgKlT" id="4B5aqq8tQwf" role="2OqNvi">
              <ref role="37wK5l" to="wb6c:4B5aqq8seie" resolve="getRemoveRedundantCompositeQuickFixDescription" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="Q5z_Y" id="4B5aqq8vxWX">
    <property role="3GE5qa" value="base.conditions.composites" />
    <property role="TrG5h" value="quickfix_RemoveRedundantConstituentCondition" />
    <node concept="Q5ZZ6" id="4B5aqq8vxWY" role="Q6x$H">
      <node concept="3clFbS" id="4B5aqq8vxWZ" role="2VODD2">
        <node concept="3clFbF" id="4B5aqq8vxX6" role="3cqZAp">
          <node concept="2OqwBi" id="4B5aqq8vxX7" role="3clFbG">
            <node concept="QwW4i" id="4B5aqq8vxX8" role="2Oq$k0">
              <ref role="QwW4h" node="4B5aqq8vxXk" resolve="condition" />
            </node>
            <node concept="3YRAZt" id="4B5aqq8vxX9" role="2OqNvi" />
          </node>
        </node>
      </node>
    </node>
    <node concept="Q6JDH" id="4B5aqq8vxXk" role="Q6Id_">
      <property role="TrG5h" value="condition" />
      <node concept="3Tqbb2" id="4B5aqq8vxXl" role="Q6QK4">
        <ref role="ehGHo" to="7f9y:5Wfdz$0ooZy" resolve="Condition" />
      </node>
    </node>
    <node concept="QznSV" id="4B5aqq8vxXo" role="QzAvj">
      <node concept="3clFbS" id="4B5aqq8vxXp" role="2VODD2">
        <node concept="3clFbF" id="4B5aqq8vxXq" role="3cqZAp">
          <node concept="2OqwBi" id="4B5aqq8vxXr" role="3clFbG">
            <node concept="35c_gC" id="4B5aqq8vxXs" role="2Oq$k0">
              <ref role="35c_gD" to="7f9y:5Wfdz$0ooZz" resolve="CompositeCondition" />
            </node>
            <node concept="2qgKlT" id="4B5aqq8vzdz" role="2OqNvi">
              <ref role="37wK5l" to="wb6c:4B5aqq8uWrA" resolve="getRemoveRedundantConstituentConditionQuickFixDescription" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="18kY7G" id="PDjyzjVNo4">
    <property role="TrG5h" value="check_TimeSpanLiteral" />
    <property role="3GE5qa" value="base.literals.timespan" />
    <node concept="3clFbS" id="PDjyzjVNo5" role="18ibNy">
      <node concept="3clFbJ" id="PDjyzjVNob" role="3cqZAp">
        <node concept="2dkUwp" id="PDjyzjVPlq" role="3clFbw">
          <node concept="3cmrfG" id="PDjyzjVPo6" role="3uHU7w">
            <property role="3cmrfH" value="0" />
          </node>
          <node concept="2OqwBi" id="PDjyzjVN$e" role="3uHU7B">
            <node concept="1YBJjd" id="PDjyzjVNon" role="2Oq$k0">
              <ref role="1YBMHb" node="PDjyzjVNo7" resolve="timeSpanLiteral" />
            </node>
            <node concept="3TrcHB" id="PDjyzjVNLc" role="2OqNvi">
              <ref role="3TsBF5" to="7f9y:1mAGFBJm3c3" resolve="value" />
            </node>
          </node>
        </node>
        <node concept="3clFbS" id="PDjyzjVNod" role="3clFbx">
          <node concept="2MkqsV" id="PDjyzjVPx2" role="3cqZAp">
            <node concept="2OqwBi" id="PDjyzjVQvf" role="2MkJ7o">
              <node concept="2OqwBi" id="PDjyzjVPH8" role="2Oq$k0">
                <node concept="1YBJjd" id="PDjyzjVPxe" role="2Oq$k0">
                  <ref role="1YBMHb" node="PDjyzjVNo7" resolve="timeSpanLiteral" />
                </node>
                <node concept="2yIwOk" id="PDjyzjVQ32" role="2OqNvi" />
              </node>
              <node concept="2qgKlT" id="PDjyzjVQSu" role="2OqNvi">
                <ref role="37wK5l" to="wb6c:PDjyzjVJ6U" resolve="getInvalidValue" />
              </node>
            </node>
            <node concept="1YBJjd" id="PDjyzjVQWM" role="2OEOjV">
              <ref role="1YBMHb" node="PDjyzjVNo7" resolve="timeSpanLiteral" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="PDjyzjVNo7" role="1YuTPh">
      <property role="TrG5h" value="timeSpanLiteral" />
      <ref role="1YaFvo" to="7f9y:1mAGFBJm3b$" resolve="TimeSpanLiteral" />
    </node>
  </node>
  <node concept="18kY7G" id="6LTgXmMT7Po">
    <property role="TrG5h" value="check_Reference" />
    <property role="3GE5qa" value="base.parameters.references" />
    <node concept="3clFbS" id="6LTgXmMT7Pp" role="18ibNy">
      <node concept="3clFbJ" id="6LTgXmMTKR3" role="3cqZAp">
        <node concept="3clFbS" id="6LTgXmMTKR5" role="3clFbx">
          <node concept="2MkqsV" id="4QUW3edDCH1" role="3cqZAp">
            <node concept="1YBJjd" id="6LTgXmMT8FK" role="2OEOjV">
              <ref role="1YBMHb" node="6LTgXmMT7Pr" resolve="reference" />
            </node>
            <node concept="2OqwBi" id="4QUW3efRmpm" role="2MkJ7o">
              <node concept="2OqwBi" id="4QUW3efRljB" role="2Oq$k0">
                <node concept="1YBJjd" id="6LTgXmMT8ye" role="2Oq$k0">
                  <ref role="1YBMHb" node="6LTgXmMT7Pr" resolve="reference" />
                </node>
                <node concept="2yIwOk" id="4QUW3efRlMW" role="2OqNvi" />
              </node>
              <node concept="2qgKlT" id="6LTgXmMT95q" role="2OqNvi">
                <ref role="37wK5l" to="wb6c:4QUW3edDA0E" resolve="getMissingTargetError" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3fqX7Q" id="6LTgXmMVYx7" role="3clFbw">
          <node concept="2OqwBi" id="6LTgXmMVYLA" role="3fr31v">
            <node concept="1YBJjd" id="6LTgXmMVY_C" role="2Oq$k0">
              <ref role="1YBMHb" node="6LTgXmMT7Pr" resolve="reference" />
            </node>
            <node concept="2qgKlT" id="6LTgXmMVZ3w" role="2OqNvi">
              <ref role="37wK5l" to="wb6c:6LTgXmMVYhf" resolve="hasTarget" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="6LTgXmMT7Pr" role="1YuTPh">
      <property role="TrG5h" value="reference" />
      <ref role="1YaFvo" to="7f9y:4QUW3efv2iG" resolve="Reference" />
    </node>
  </node>
  <node concept="18kY7G" id="6LTgXmNLqll">
    <property role="TrG5h" value="check_AspectVariablePattern" />
    <property role="3GE5qa" value="aspect.patterns.aspect" />
    <node concept="3clFbS" id="6LTgXmNLqlm" role="18ibNy">
      <node concept="3clFbJ" id="6LTgXmNLqls" role="3cqZAp">
        <node concept="3clFbS" id="6LTgXmNLqlu" role="3clFbx">
          <node concept="2MkqsV" id="6LTgXmNLrqn" role="3cqZAp">
            <node concept="Xl_RD" id="6LTgXmNLrqA" role="2MkJ7o">
              <property role="Xl_RC" value="missing pattern" />
            </node>
            <node concept="1YBJjd" id="6LTgXmNLrre" role="2OEOjV">
              <ref role="1YBMHb" node="6LTgXmNLqlo" resolve="newPattern" />
            </node>
          </node>
        </node>
        <node concept="3fqX7Q" id="6LTgXmNRviv" role="3clFbw">
          <node concept="2OqwBi" id="6LTgXmNRvv0" role="3fr31v">
            <node concept="1YBJjd" id="6LTgXmNRviJ" role="2Oq$k0">
              <ref role="1YBMHb" node="6LTgXmNLqlo" resolve="newPattern" />
            </node>
            <node concept="2qgKlT" id="6LTgXmNRvD$" role="2OqNvi">
              <ref role="37wK5l" to="wb6c:6LTgXmNRv0n" resolve="hasPattern" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="6LTgXmNLqlo" role="1YuTPh">
      <property role="TrG5h" value="newPattern" />
      <ref role="1YaFvo" to="7f9y:6LTgXmNrfh_" resolve="AspectVariablePattern" />
    </node>
  </node>
  <node concept="18kY7G" id="6LTgXmOiz8e">
    <property role="TrG5h" value="check_Template" />
    <property role="3GE5qa" value="aspect.changes" />
    <node concept="3clFbS" id="6LTgXmOiz8f" role="18ibNy">
      <node concept="3clFbJ" id="6LTgXmOiz8l" role="3cqZAp">
        <node concept="2OqwBi" id="6LTgXmOizH6" role="3clFbw">
          <node concept="2OqwBi" id="6LTgXmOizhg" role="2Oq$k0">
            <node concept="1YBJjd" id="6LTgXmOiz8x" role="2Oq$k0">
              <ref role="1YBMHb" node="6LTgXmOiz8h" resolve="template" />
            </node>
            <node concept="2yIwOk" id="6LTgXmOizoe" role="2OqNvi" />
          </node>
          <node concept="3O6GUB" id="6LTgXmOizUi" role="2OqNvi">
            <node concept="chp4Y" id="6LTgXmOizXp" role="3QVz_e">
              <ref role="cht4Q" to="7f9y:CxH2rBlQYG" resolve="Change" />
            </node>
          </node>
        </node>
        <node concept="3clFbS" id="6LTgXmOiz8n" role="3clFbx">
          <node concept="2MkqsV" id="6LTgXmOi$0z" role="3cqZAp">
            <node concept="Xl_RD" id="6LTgXmOi$0J" role="2MkJ7o">
              <property role="Xl_RC" value="missing change" />
            </node>
            <node concept="1YBJjd" id="6LTgXmOi$1f" role="2OEOjV">
              <ref role="1YBMHb" node="6LTgXmOiz8h" resolve="template" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="6LTgXmOiz8h" role="1YuTPh">
      <property role="TrG5h" value="template" />
      <ref role="1YaFvo" to="7f9y:CxH2rBlQYG" resolve="Change" />
    </node>
  </node>
  <node concept="18kY7G" id="1I84Bf95szT">
    <property role="TrG5h" value="check_AspectReference" />
    <property role="3GE5qa" value="aspect" />
    <node concept="3clFbS" id="1I84Bf95szU" role="18ibNy">
      <node concept="3clFbJ" id="1I84Bf95s$3" role="3cqZAp">
        <node concept="2OqwBi" id="1I84Bf95t4R" role="3clFbw">
          <node concept="2OqwBi" id="1I84Bf95sGY" role="2Oq$k0">
            <node concept="1YBJjd" id="1I84Bf95s$f" role="2Oq$k0">
              <ref role="1YBMHb" node="1I84Bf95szW" resolve="aspectReference" />
            </node>
            <node concept="3TrEf2" id="1I84Bf95sNW" role="2OqNvi">
              <ref role="3Tt5mk" to="7f9y:1I84Bf95szu" resolve="target" />
            </node>
          </node>
          <node concept="3w_OXm" id="1I84Bf95tin" role="2OqNvi" />
        </node>
        <node concept="3clFbS" id="1I84Bf95s$5" role="3clFbx">
          <node concept="2MkqsV" id="1I84Bf95tkJ" role="3cqZAp">
            <node concept="Xl_RD" id="1I84Bf95tkV" role="2MkJ7o">
              <property role="Xl_RC" value="missing aspect reference" />
            </node>
            <node concept="1YBJjd" id="1I84Bf95tly" role="2OEOjV">
              <ref role="1YBMHb" node="1I84Bf95szW" resolve="aspectReference" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="1I84Bf95szW" role="1YuTPh">
      <property role="TrG5h" value="aspectReference" />
      <ref role="1YaFvo" to="7f9y:1I84Bf95szt" resolve="AspectReference" />
    </node>
  </node>
  <node concept="18kY7G" id="1I84Bf96n_v">
    <property role="TrG5h" value="check_AspectOrdering" />
    <property role="3GE5qa" value="aspect" />
    <node concept="3clFbS" id="1I84Bf96n_w" role="18ibNy">
      <node concept="3clFbJ" id="1I84Bf96sYk" role="3cqZAp">
        <node concept="3clFbS" id="1I84Bf96sYm" role="3clFbx">
          <node concept="2MkqsV" id="1I84Bf96t6y" role="3cqZAp">
            <node concept="Xl_RD" id="1I84Bf96t6I" role="2MkJ7o">
              <property role="Xl_RC" value="there can only be one aspect ordering" />
            </node>
            <node concept="1YBJjd" id="1I84Bf96t86" role="2OEOjV">
              <ref role="1YBMHb" node="1I84Bf96n_y" resolve="aspectOrdering" />
            </node>
          </node>
        </node>
        <node concept="3eOSWO" id="1I84Bf96sPb" role="3clFbw">
          <node concept="3cmrfG" id="1I84Bf96sSk" role="3uHU7w">
            <property role="3cmrfH" value="1" />
          </node>
          <node concept="2OqwBi" id="1I84Bf96pDo" role="3uHU7B">
            <node concept="2OqwBi" id="1I84Bf96o2V" role="2Oq$k0">
              <node concept="2OqwBi" id="1I84Bf96nGQ" role="2Oq$k0">
                <node concept="1YBJjd" id="1I84Bf96n_K" role="2Oq$k0">
                  <ref role="1YBMHb" node="1I84Bf96n_y" resolve="aspectOrdering" />
                </node>
                <node concept="I4A8Y" id="1I84Bf96nNK" role="2OqNvi" />
              </node>
              <node concept="1j9C0f" id="1I84Bf96o9z" role="2OqNvi">
                <ref role="1j9C0d" to="7f9y:1I84Bf95szg" resolve="AspectOrdering" />
              </node>
            </node>
            <node concept="34oBXx" id="1I84Bf96qQt" role="2OqNvi" />
          </node>
        </node>
      </node>
      <node concept="3clFbF" id="5uQLXCsPJQV" role="3cqZAp">
        <node concept="2OqwBi" id="5uQLXCsPLJr" role="3clFbG">
          <node concept="2OqwBi" id="5uQLXCsPJYe" role="2Oq$k0">
            <node concept="1YBJjd" id="5uQLXCsPJQT" role="2Oq$k0">
              <ref role="1YBMHb" node="1I84Bf96n_y" resolve="aspectOrdering" />
            </node>
            <node concept="3Tsc0h" id="5uQLXCsPKjl" role="2OqNvi">
              <ref role="3TtcxE" to="7f9y:1I84Bf95tmA" resolve="aspects" />
            </node>
          </node>
          <node concept="2es0OD" id="5uQLXCsPN27" role="2OqNvi">
            <node concept="1bVj0M" id="5uQLXCsPN29" role="23t8la">
              <node concept="3clFbS" id="5uQLXCsPN2a" role="1bW5cS">
                <node concept="3clFbJ" id="5uQLXCsPNP1" role="3cqZAp">
                  <node concept="2OqwBi" id="5uQLXCsPQgy" role="3clFbw">
                    <node concept="2OqwBi" id="5uQLXCsPOes" role="2Oq$k0">
                      <node concept="1YBJjd" id="5uQLXCsPNYU" role="2Oq$k0">
                        <ref role="1YBMHb" node="1I84Bf96n_y" resolve="aspectOrdering" />
                      </node>
                      <node concept="3Tsc0h" id="5uQLXCsPOv4" role="2OqNvi">
                        <ref role="3TtcxE" to="7f9y:1I84Bf95tmA" resolve="aspects" />
                      </node>
                    </node>
                    <node concept="2HwmR7" id="5uQLXCsPXCt" role="2OqNvi">
                      <node concept="1bVj0M" id="5uQLXCsPXCv" role="23t8la">
                        <node concept="3clFbS" id="5uQLXCsPXCw" role="1bW5cS">
                          <node concept="3clFbF" id="5uQLXCsPYqn" role="3cqZAp">
                            <node concept="1Wc70l" id="5uQLXCsQ6Wl" role="3clFbG">
                              <node concept="3y3z36" id="5uQLXCsQ7De" role="3uHU7B">
                                <node concept="37vLTw" id="5uQLXCsQ7YO" role="3uHU7w">
                                  <ref role="3cqZAo" node="5uQLXCsPN2b" resolve="aspect" />
                                </node>
                                <node concept="37vLTw" id="5uQLXCsQ7eD" role="3uHU7B">
                                  <ref role="3cqZAo" node="5uQLXCsPXCx" resolve="it" />
                                </node>
                              </node>
                              <node concept="3clFbC" id="5uQLXCsPZw0" role="3uHU7w">
                                <node concept="2OqwBi" id="5uQLXCsQ0Jd" role="3uHU7w">
                                  <node concept="37vLTw" id="5uQLXCsQ0s9" role="2Oq$k0">
                                    <ref role="3cqZAo" node="5uQLXCsPN2b" resolve="aspect" />
                                  </node>
                                  <node concept="3TrEf2" id="5uQLXCsQ18d" role="2OqNvi">
                                    <ref role="3Tt5mk" to="7f9y:1I84Bf95szu" resolve="target" />
                                  </node>
                                </node>
                                <node concept="2OqwBi" id="5uQLXCsPYGI" role="3uHU7B">
                                  <node concept="37vLTw" id="5uQLXCsPYqm" role="2Oq$k0">
                                    <ref role="3cqZAo" node="5uQLXCsPXCx" resolve="it" />
                                  </node>
                                  <node concept="3TrEf2" id="5uQLXCsPYZx" role="2OqNvi">
                                    <ref role="3Tt5mk" to="7f9y:1I84Bf95szu" resolve="target" />
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="Rh6nW" id="5uQLXCsPXCx" role="1bW2Oz">
                          <property role="TrG5h" value="it" />
                          <node concept="2jxLKc" id="5uQLXCsPXCy" role="1tU5fm" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbS" id="5uQLXCsPNP3" role="3clFbx">
                    <node concept="2MkqsV" id="5uQLXCsQ1q2" role="3cqZAp">
                      <node concept="Xl_RD" id="5uQLXCsQ8gS" role="2MkJ7o">
                        <property role="Xl_RC" value="duplicate aspects" />
                      </node>
                      <node concept="37vLTw" id="5uQLXCsQb$Q" role="2OEOjV">
                        <ref role="3cqZAo" node="5uQLXCsPN2b" resolve="aspect" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="Rh6nW" id="5uQLXCsPN2b" role="1bW2Oz">
                <property role="TrG5h" value="aspect" />
                <node concept="2jxLKc" id="5uQLXCsPN2c" role="1tU5fm" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="1I84Bf96n_y" role="1YuTPh">
      <property role="TrG5h" value="aspectOrdering" />
      <ref role="1YaFvo" to="7f9y:1I84Bf95szg" resolve="AspectOrdering" />
    </node>
  </node>
  <node concept="18kY7G" id="65epL7MsXp8">
    <property role="TrG5h" value="check_ISelectedEntityPattern" />
    <property role="3GE5qa" value="aspect.patterns.aspect.stereotypes" />
    <node concept="3clFbS" id="65epL7MsXp9" role="18ibNy">
      <node concept="3clFbJ" id="65epL7MsXpf" role="3cqZAp">
        <node concept="2OqwBi" id="65epL7Mt0rO" role="3clFbw">
          <node concept="2OqwBi" id="65epL7MsXyP" role="2Oq$k0">
            <node concept="1YBJjd" id="65epL7MsXpr" role="2Oq$k0">
              <ref role="1YBMHb" node="65epL7MsXpb" resolve="iSelectedEntityPattern" />
            </node>
            <node concept="3Tsc0h" id="65epL7MsXF1" role="2OqNvi">
              <ref role="3TtcxE" to="7f9y:65epL7MqHQ9" resolve="entities" />
            </node>
          </node>
          <node concept="1v1jN8" id="65epL7Mt2nr" role="2OqNvi" />
        </node>
        <node concept="3clFbS" id="65epL7MsXph" role="3clFbx">
          <node concept="2MkqsV" id="65epL7Mt2o6" role="3cqZAp">
            <node concept="2OqwBi" id="65epL7Mtm$$" role="2MkJ7o">
              <node concept="2OqwBi" id="65epL7Mt2xJ" role="2Oq$k0">
                <node concept="1YBJjd" id="65epL7Mt2oi" role="2Oq$k0">
                  <ref role="1YBMHb" node="65epL7MsXpb" resolve="iSelectedEntityPattern" />
                </node>
                <node concept="2yIwOk" id="65epL7Mtmht" role="2OqNvi" />
              </node>
              <node concept="2qgKlT" id="65epL7M$hPF" role="2OqNvi">
                <ref role="37wK5l" to="wb6c:65epL7MsXaV" resolve="getMissingEntitiesError" />
              </node>
            </node>
            <node concept="1YBJjd" id="65epL7Mt2Kp" role="2OEOjV">
              <ref role="1YBMHb" node="65epL7MsXpb" resolve="iSelectedEntityPattern" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="65epL7MsXpb" role="1YuTPh">
      <property role="TrG5h" value="iSelectedEntityPattern" />
      <ref role="1YaFvo" to="7f9y:65epL7Mprkt" resolve="ISelectedEntityPattern" />
    </node>
  </node>
  <node concept="18kY7G" id="2FjKBCOwkub">
    <property role="TrG5h" value="check_Pointcut" />
    <property role="3GE5qa" value="aspect.pointcuts" />
    <node concept="3clFbS" id="2FjKBCOwkuc" role="18ibNy">
      <node concept="3clFbJ" id="2FjKBCOwkui" role="3cqZAp">
        <node concept="2OqwBi" id="2FjKBCOwlfo" role="3clFbw">
          <node concept="2OqwBi" id="2FjKBCOwkDz" role="2Oq$k0">
            <node concept="1YBJjd" id="2FjKBCOwkuu" role="2Oq$k0">
              <ref role="1YBMHb" node="2FjKBCOwkue" resolve="pointcut" />
            </node>
            <node concept="2yIwOk" id="2FjKBCOwkP1" role="2OqNvi" />
          </node>
          <node concept="3O6GUB" id="2FjKBCOwlvi" role="2OqNvi">
            <node concept="chp4Y" id="2FjKBCOwlzf" role="3QVz_e">
              <ref role="cht4Q" to="7f9y:2FjKBCOu_xP" resolve="Pointcut" />
            </node>
          </node>
        </node>
        <node concept="3clFbS" id="2FjKBCOwkuk" role="3clFbx">
          <node concept="2MkqsV" id="2FjKBCOwlBW" role="3cqZAp">
            <node concept="2OqwBi" id="2FjKBCOwmFb" role="2MkJ7o">
              <node concept="2OqwBi" id="2FjKBCOwlNg" role="2Oq$k0">
                <node concept="1YBJjd" id="2FjKBCOwlC8" role="2Oq$k0">
                  <ref role="1YBMHb" node="2FjKBCOwkue" resolve="pointcut" />
                </node>
                <node concept="2yIwOk" id="2FjKBCOwmdX" role="2OqNvi" />
              </node>
              <node concept="2qgKlT" id="2FjKBCOwmZ0" role="2OqNvi">
                <ref role="37wK5l" to="wb6c:2FjKBCOwkqr" resolve="getMissingPointcutError" />
              </node>
            </node>
            <node concept="1YBJjd" id="2FjKBCOwn3g" role="2OEOjV">
              <ref role="1YBMHb" node="2FjKBCOwkue" resolve="pointcut" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="2FjKBCOwkue" role="1YuTPh">
      <property role="TrG5h" value="pointcut" />
      <ref role="1YaFvo" to="7f9y:2FjKBCOu_xP" resolve="Pointcut" />
    </node>
  </node>
  <node concept="18kY7G" id="2FjKBCOHCDx">
    <property role="TrG5h" value="check_Advice" />
    <property role="3GE5qa" value="aspect.advices" />
    <node concept="3clFbS" id="2FjKBCOHCDy" role="18ibNy">
      <node concept="3SKdUt" id="2FjKBCOHXDc" role="3cqZAp">
        <node concept="3SKdUq" id="2FjKBCOHXDe" role="3SKWNk">
          <property role="3SKdUp" value="Check for duplicate parameter names" />
        </node>
      </node>
      <node concept="3clFbF" id="2FjKBCOHI4q" role="3cqZAp">
        <node concept="2OqwBi" id="2FjKBCOHKUe" role="3clFbG">
          <node concept="2OqwBi" id="2FjKBCOHIee" role="2Oq$k0">
            <node concept="1YBJjd" id="2FjKBCOHI4o" role="2Oq$k0">
              <ref role="1YBMHb" node="2FjKBCOHCD$" resolve="advice" />
            </node>
            <node concept="3Tsc0h" id="2FjKBCOHItJ" role="2OqNvi">
              <ref role="3TtcxE" to="7f9y:2FjKBCOyLj$" resolve="parameters" />
            </node>
          </node>
          <node concept="2es0OD" id="2FjKBCOHMRf" role="2OqNvi">
            <node concept="1bVj0M" id="2FjKBCOHMRh" role="23t8la">
              <node concept="3clFbS" id="2FjKBCOHMRi" role="1bW5cS">
                <node concept="3clFbF" id="2FjKBCOHMUQ" role="3cqZAp">
                  <node concept="2OqwBi" id="2FjKBCOHQ1F" role="3clFbG">
                    <node concept="2OqwBi" id="2FjKBCOISQP" role="2Oq$k0">
                      <node concept="2OqwBi" id="2FjKBCOHNkG" role="2Oq$k0">
                        <node concept="1YBJjd" id="2FjKBCOHMUP" role="2Oq$k0">
                          <ref role="1YBMHb" node="2FjKBCOHCD$" resolve="advice" />
                        </node>
                        <node concept="3Tsc0h" id="2FjKBCOHN_V" role="2OqNvi">
                          <ref role="3TtcxE" to="7f9y:2FjKBCOyLj$" resolve="parameters" />
                        </node>
                      </node>
                      <node concept="3zZkjj" id="2FjKBCOIVml" role="2OqNvi">
                        <node concept="1bVj0M" id="2FjKBCOIVmn" role="23t8la">
                          <node concept="3clFbS" id="2FjKBCOIVmo" role="1bW5cS">
                            <node concept="3clFbF" id="2FjKBCOIVYz" role="3cqZAp">
                              <node concept="3y3z36" id="2FjKBCOIX6R" role="3clFbG">
                                <node concept="37vLTw" id="2FjKBCOIXLp" role="3uHU7w">
                                  <ref role="3cqZAo" node="2FjKBCOHMRj" resolve="parameter" />
                                </node>
                                <node concept="37vLTw" id="2FjKBCOIVYy" role="3uHU7B">
                                  <ref role="3cqZAo" node="2FjKBCOIVmp" resolve="it" />
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="Rh6nW" id="2FjKBCOIVmp" role="1bW2Oz">
                            <property role="TrG5h" value="it" />
                            <node concept="2jxLKc" id="2FjKBCOIVmq" role="1tU5fm" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="2es0OD" id="2FjKBCOHS0o" role="2OqNvi">
                      <node concept="1bVj0M" id="2FjKBCOHS0q" role="23t8la">
                        <node concept="3clFbS" id="2FjKBCOHS0r" role="1bW5cS">
                          <node concept="3clFbJ" id="2FjKBCOHS91" role="3cqZAp">
                            <node concept="3clFbC" id="2FjKBCOHTJv" role="3clFbw">
                              <node concept="2OqwBi" id="2FjKBCOHUzv" role="3uHU7w">
                                <node concept="37vLTw" id="2FjKBCOHU1D" role="2Oq$k0">
                                  <ref role="3cqZAo" node="2FjKBCOHMRj" resolve="parameter" />
                                </node>
                                <node concept="3TrcHB" id="2FjKBCOHV2I" role="2OqNvi">
                                  <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                                </node>
                              </node>
                              <node concept="2OqwBi" id="2FjKBCOHSIU" role="3uHU7B">
                                <node concept="37vLTw" id="2FjKBCOHSvU" role="2Oq$k0">
                                  <ref role="3cqZAo" node="2FjKBCOHS0s" resolve="it" />
                                </node>
                                <node concept="3TrcHB" id="2FjKBCOHT3r" role="2OqNvi">
                                  <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                                </node>
                              </node>
                            </node>
                            <node concept="3clFbS" id="2FjKBCOHS93" role="3clFbx">
                              <node concept="2MkqsV" id="2FjKBCOHVkQ" role="3cqZAp">
                                <node concept="Xl_RD" id="2FjKBCOHVB4" role="2MkJ7o">
                                  <property role="Xl_RC" value="duplicate parameter name" />
                                </node>
                                <node concept="37vLTw" id="2FjKBCOHXk_" role="2OEOjV">
                                  <ref role="3cqZAo" node="2FjKBCOHMRj" resolve="parameter" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="Rh6nW" id="2FjKBCOHS0s" role="1bW2Oz">
                          <property role="TrG5h" value="it" />
                          <node concept="2jxLKc" id="2FjKBCOHS0t" role="1tU5fm" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="Rh6nW" id="2FjKBCOHMRj" role="1bW2Oz">
                <property role="TrG5h" value="parameter" />
                <node concept="2jxLKc" id="2FjKBCOHMRk" role="1tU5fm" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3clFbH" id="2FjKBCOHXE9" role="3cqZAp" />
      <node concept="3SKdUt" id="2FjKBCOHXFL" role="3cqZAp">
        <node concept="3SKdUq" id="2FjKBCOHXFN" role="3SKWNk">
          <property role="3SKdUp" value="Check for unbound parameters" />
        </node>
      </node>
      <node concept="3clFbF" id="2FjKBCOHZUs" role="3cqZAp">
        <node concept="2OqwBi" id="2FjKBCOI30u" role="3clFbG">
          <node concept="2OqwBi" id="2FjKBCOI04I" role="2Oq$k0">
            <node concept="1YBJjd" id="2FjKBCOHZUq" role="2Oq$k0">
              <ref role="1YBMHb" node="2FjKBCOHCD$" resolve="advice" />
            </node>
            <node concept="3Tsc0h" id="2FjKBCOI0zZ" role="2OqNvi">
              <ref role="3TtcxE" to="7f9y:2FjKBCOyLj$" resolve="parameters" />
            </node>
          </node>
          <node concept="2es0OD" id="2FjKBCOI5dd" role="2OqNvi">
            <node concept="1bVj0M" id="2FjKBCOI5df" role="23t8la">
              <node concept="3clFbS" id="2FjKBCOI5dg" role="1bW5cS">
                <node concept="3clFbJ" id="2FjKBCOIrhj" role="3cqZAp">
                  <node concept="3clFbS" id="2FjKBCOIrhl" role="3clFbx">
                    <node concept="2MkqsV" id="2FjKBCOIskC" role="3cqZAp">
                      <node concept="Xl_RD" id="2FjKBCOIsTO" role="2MkJ7o">
                        <property role="Xl_RC" value="unbound parameter" />
                      </node>
                      <node concept="37vLTw" id="2FjKBCOIuDd" role="2OEOjV">
                        <ref role="3cqZAo" node="2FjKBCOI5dh" resolve="parameter" />
                      </node>
                    </node>
                  </node>
                  <node concept="3fqX7Q" id="2FjKBCQ2FYG" role="3clFbw">
                    <node concept="2OqwBi" id="2FjKBCQ2FYI" role="3fr31v">
                      <node concept="2OqwBi" id="2FjKBCQ2FYJ" role="2Oq$k0">
                        <node concept="1YBJjd" id="2FjKBCQ2FYK" role="2Oq$k0">
                          <ref role="1YBMHb" node="2FjKBCOHCD$" resolve="advice" />
                        </node>
                        <node concept="3TrEf2" id="2FjKBCQ2FYL" role="2OqNvi">
                          <ref role="3Tt5mk" to="7f9y:6khVixyYxjq" resolve="pointcut" />
                        </node>
                      </node>
                      <node concept="2qgKlT" id="2FjKBCQ2FYM" role="2OqNvi">
                        <ref role="37wK5l" to="wb6c:2FjKBCPYSs6" resolve="hasBoundVariable" />
                        <node concept="37vLTw" id="2FjKBCQ2FYN" role="37wK5m">
                          <ref role="3cqZAo" node="2FjKBCOI5dh" resolve="parameter" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="Rh6nW" id="2FjKBCOI5dh" role="1bW2Oz">
                <property role="TrG5h" value="parameter" />
                <node concept="2jxLKc" id="2FjKBCOI5di" role="1tU5fm" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="2FjKBCOHCD$" role="1YuTPh">
      <property role="TrG5h" value="advice" />
      <ref role="1YaFvo" to="7f9y:CxH2r_SlM0" resolve="Advice" />
    </node>
  </node>
  <node concept="18kY7G" id="2FjKBCOK2HR">
    <property role="TrG5h" value="check_Element" />
    <property role="3GE5qa" value="base.parameters" />
    <node concept="3clFbS" id="2FjKBCOK2HS" role="18ibNy">
      <node concept="3clFbJ" id="2FjKBCOK2HY" role="3cqZAp">
        <node concept="2OqwBi" id="2FjKBCOK3vp" role="3clFbw">
          <node concept="2OqwBi" id="2FjKBCOK2Tf" role="2Oq$k0">
            <node concept="1YBJjd" id="2FjKBCOK2Ia" role="2Oq$k0">
              <ref role="1YBMHb" node="2FjKBCOK2HU" resolve="element" />
            </node>
            <node concept="2yIwOk" id="2FjKBCOK34H" role="2OqNvi" />
          </node>
          <node concept="3O6GUB" id="2FjKBCOK3J$" role="2OqNvi">
            <node concept="chp4Y" id="2FjKBCOK3N$" role="3QVz_e">
              <ref role="cht4Q" to="7f9y:2FjKBCOFjHt" resolve="BaseParameter" />
            </node>
          </node>
        </node>
        <node concept="3clFbS" id="2FjKBCOK2I0" role="3clFbx">
          <node concept="2MkqsV" id="2FjKBCOK3Sn" role="3cqZAp">
            <node concept="Xl_RD" id="2FjKBCOK3Sz" role="2MkJ7o">
              <property role="Xl_RC" value="missing element" />
            </node>
            <node concept="1YBJjd" id="2FjKBCOK3TA" role="2OEOjV">
              <ref role="1YBMHb" node="2FjKBCOK2HU" resolve="element" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="2FjKBCOK2HU" role="1YuTPh">
      <property role="TrG5h" value="element" />
      <ref role="1YaFvo" to="7f9y:2FjKBCOFjHt" resolve="BaseParameter" />
    </node>
  </node>
  <node concept="18kY7G" id="2FjKBCQ4hqb">
    <property role="TrG5h" value="check_CompositePointcut" />
    <property role="3GE5qa" value="aspect.pointcuts.composite" />
    <node concept="3clFbS" id="2FjKBCQ4hqc" role="18ibNy">
      <node concept="3clFbJ" id="2FjKBCQ4hqi" role="3cqZAp">
        <node concept="3eOVzh" id="2FjKBCQ4uZh" role="3clFbw">
          <node concept="3cmrfG" id="2FjKBCQ4v3o" role="3uHU7w">
            <property role="3cmrfH" value="2" />
          </node>
          <node concept="2OqwBi" id="2FjKBCQ4kW2" role="3uHU7B">
            <node concept="2OqwBi" id="2FjKBCQ4hAl" role="2Oq$k0">
              <node concept="1YBJjd" id="2FjKBCQ4hqu" role="2Oq$k0">
                <ref role="1YBMHb" node="2FjKBCQ4hqe" resolve="compositePointcut" />
              </node>
              <node concept="3Tsc0h" id="2FjKBCQ4ilh" role="2OqNvi">
                <ref role="3TtcxE" to="7f9y:2FjKBCQ4hNj" resolve="pointcuts" />
              </node>
            </node>
            <node concept="34oBXx" id="2FjKBCQ4sqw" role="2OqNvi" />
          </node>
        </node>
        <node concept="3clFbS" id="2FjKBCQ4hqk" role="3clFbx">
          <node concept="a7r0C" id="2FjKBCQ7OL0" role="3cqZAp">
            <node concept="Xl_RD" id="2FjKBCQ7OL3" role="a7wSD">
              <property role="Xl_RC" value="unnecessary composite pointcut" />
            </node>
            <node concept="1YBJjd" id="2FjKBCQ7OL2" role="2OEOjV">
              <ref role="1YBMHb" node="2FjKBCQ4hqe" resolve="compositePointcut" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="2FjKBCQ4hqe" role="1YuTPh">
      <property role="TrG5h" value="compositePointcut" />
      <ref role="1YaFvo" to="7f9y:2FjKBCQ332Q" resolve="CompositePointcut" />
    </node>
  </node>
</model>

