<?xml version="1.0" encoding="UTF-8"?>
<solution name="no.uio.mLab.decisions.core.translations" uuid="3d057672-91c8-42c2-82b2-0a0e5d5da4af" moduleVersion="0" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
  </models>
  <sourcePath />
  <dependencies>
    <dependency reexport="false">6354ebe7-c22a-4a0f-ac54-50b52ab9b065(JDK)</dependency>
    <dependency reexport="false">3d1e5220-5be7-4cad-97be-258d1d3e62dd(no.uio.mLab.shared.translations)</dependency>
  </dependencies>
  <languageVersions>
    <language slang="l:f3061a53-9226-4cc5-a443-f952ceaf5816:jetbrains.mps.baseLanguage" version="6" />
    <language slang="l:fd392034-7849-419d-9071-12563d152375:jetbrains.mps.baseLanguage.closures" version="0" />
    <language slang="l:83888646-71ce-4f1c-9c53-c54016f6ad4f:jetbrains.mps.baseLanguage.collections" version="0" />
    <language slang="l:ceab5195-25ea-4f22-9b92-103b95ca8c0c:jetbrains.mps.lang.core" version="1" />
    <language slang="l:446c26eb-2b7b-4bf0-9b35-f83fa582753e:jetbrains.mps.lang.modelapi" version="0" />
    <language slang="l:7866978e-a0f0-4cc7-81bc-4d213d9375e1:jetbrains.mps.lang.smodel" version="11" />
    <language slang="l:9ded098b-ad6a-4657-bfd9-48636cfe8bc3:jetbrains.mps.lang.traceable" version="0" />
  </languageVersions>
  <dependencyVersions>
    <module reference="6354ebe7-c22a-4a0f-ac54-50b52ab9b065(JDK)" version="0" />
    <module reference="3d057672-91c8-42c2-82b2-0a0e5d5da4af(no.uio.mLab.decisions.core.translations)" version="0" />
    <module reference="3d1e5220-5be7-4cad-97be-258d1d3e62dd(no.uio.mLab.shared.translations)" version="0" />
    <module reference="4eaf8bc4-bd15-4875-9ca5-b63e6ed0c9e4(no.uio.mlab.common)" version="0" />
    <module reference="e27d103d-c6e9-4671-8b31-141761bbe64d(no.uio.mlab.core.base)" version="0" />
  </dependencyVersions>
</solution>

