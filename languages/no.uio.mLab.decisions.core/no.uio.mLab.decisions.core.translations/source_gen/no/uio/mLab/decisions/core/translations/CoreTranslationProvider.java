package no.uio.mLab.decisions.core.translations;

/*Generated by MPS */

import no.uio.mLab.shared.translations.TranslationConfiguration;

public final class CoreTranslationProvider {
  public static final ICoreTranslations displayTranslations = init(TranslationConfiguration.displayLanguage);
  public static final ICoreTranslations generationTranslations = init(TranslationConfiguration.generationLanguage);

  private static ICoreTranslations init(String language) {
    switch (language) {
      case "no":
        return new NoCoreTranslations();
      default:
        return new EnCoreTranslations();
    }
  }
}
