package no.uio.mLab.decisions.core.runtime;

/*Generated by MPS */

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class KnowledgeBase {
  private HashMap<DataValue, LisResponse> repository;

  public KnowledgeBase() {
    this.repository = new HashMap<DataValue, LisResponse>();
  }

  public void addKnowledge(Knowledge knowledge) {
    this.repository.put(knowledge.getDataValue(), knowledge.getLisResponse());
  }

  public Map<DataValue, LisResponse> getRepository() {
    return this.repository;
  }

  public Object resolve(DataValue dataValue) {
    LisResponse response = repository.get(dataValue);
    Object concreteValue = (response == null ? null : response.getConcreteValue());
    return concreteValue;
  }

  public boolean isUnresolved(DataValue dataValue) {
    LisResponse response = repository.get(dataValue);
    if (response == null) {
      return false;
    }
    return !(response.isResolved());
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.getClass(), this.repository);
  }
  @Override
  public boolean equals(Object object) {
    if (this == object) {
      return true;
    }
    if (object == null || this.getClass() != object.getClass()) {
      return false;
    }
    KnowledgeBase other = (KnowledgeBase) object;
    return Objects.equals(this.repository, other.repository);
  }
}
