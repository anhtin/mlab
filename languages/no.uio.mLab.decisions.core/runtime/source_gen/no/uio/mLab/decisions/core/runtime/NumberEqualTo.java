package no.uio.mLab.decisions.core.runtime;

/*Generated by MPS */


public class NumberEqualTo extends AtomicNumberConstraint {
  public NumberEqualTo(double number) {
    super(number);
  }

  @Override
  public boolean isSatisfiedBy(Object value) {
    if (value instanceof Double) {
      return (double) value == 0;
    }
    return false;
  }
}
