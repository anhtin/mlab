package no.uio.mLab.decisions.core.runtime;

/*Generated by MPS */


public class NumberLessThanOrEqualTo extends AtomicNumberConstraint {
  public NumberLessThanOrEqualTo(double number) {
    super(number);
  }

  @Override
  public boolean isSatisfiedBy(Object value) {
    if (value instanceof Double) {
      return (double) value <= this.getNumber();
    }
    return false;
  }
}
