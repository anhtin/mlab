package no.uio.mLab.decisions.core.actions;

/*Generated by MPS */

import jetbrains.mps.openapi.actions.descriptor.NodeFactory;
import org.jetbrains.mps.openapi.model.SNode;
import org.jetbrains.mps.openapi.model.SModel;
import jetbrains.mps.lang.smodel.generator.smodelAdapter.SNodeOperations;
import jetbrains.mps.smodel.adapter.structure.MetaAdapterFactory;
import jetbrains.mps.internal.collections.runtime.ListSequence;
import jetbrains.mps.lang.smodel.generator.smodelAdapter.SLinkOperations;

public class CompositeCondition_NodeFactory {
  public static class NodeFactory_1560130832598572038 implements NodeFactory {
    public void setup(SNode newNode, SNode sampleNode, SNode enclosingNode, SModel model) {
      {
        final SNode composite = sampleNode;
        if (SNodeOperations.isInstanceOf(composite, MetaAdapterFactory.getConcept(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x5f0f363900618fe3L, "no.uio.mLab.decisions.core.structure.CompositeCondition"))) {
          ListSequence.fromList(SLinkOperations.getChildren(newNode, MetaAdapterFactory.getContainmentLink(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x5f0f363900618fe3L, 0x5f0f363900618fe4L, "conditions"))).addSequence(ListSequence.fromList(SLinkOperations.getChildren(composite, MetaAdapterFactory.getContainmentLink(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x5f0f363900618fe3L, 0x5f0f363900618fe4L, "conditions"))));
          return;
        }
      }
      {
        final SNode condition = sampleNode;
        if (SNodeOperations.isInstanceOf(condition, MetaAdapterFactory.getConcept(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x5f0f363900618fe2L, "no.uio.mLab.decisions.core.structure.Condition"))) {
          ListSequence.fromList(SLinkOperations.getChildren(newNode, MetaAdapterFactory.getContainmentLink(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x5f0f363900618fe3L, 0x5f0f363900618fe4L, "conditions"))).addElement(condition);
          return;
        }
      }
    }
  }
}
