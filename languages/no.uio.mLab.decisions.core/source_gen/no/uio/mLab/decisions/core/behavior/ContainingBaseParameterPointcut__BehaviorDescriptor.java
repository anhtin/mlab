package no.uio.mLab.decisions.core.behavior;

/*Generated by MPS */

import jetbrains.mps.core.aspects.behaviour.BaseBHDescriptor;
import org.jetbrains.mps.openapi.language.SAbstractConcept;
import jetbrains.mps.smodel.adapter.structure.MetaAdapterFactory;
import jetbrains.mps.core.aspects.behaviour.api.BehaviorRegistry;
import jetbrains.mps.smodel.language.ConceptRegistry;
import jetbrains.mps.core.aspects.behaviour.api.SMethod;
import jetbrains.mps.core.aspects.behaviour.SMethodBuilder;
import jetbrains.mps.core.aspects.behaviour.SJavaCompoundTypeImpl;
import jetbrains.mps.core.aspects.behaviour.SModifiersImpl;
import jetbrains.mps.core.aspects.behaviour.AccessPrivileges;
import no.uio.mLab.decisions.core.utils.JoinPointMatch;
import org.jetbrains.mps.openapi.model.SNode;
import java.util.List;
import java.util.Arrays;
import org.jetbrains.annotations.NotNull;
import no.uio.mLab.decisions.core.utils.NodeCollectionUtil;
import jetbrains.mps.lang.smodel.generator.smodelAdapter.SNodeOperations;
import jetbrains.mps.internal.collections.runtime.ListSequence;
import jetbrains.mps.lang.smodel.generator.smodelAdapter.SLinkOperations;
import jetbrains.mps.internal.collections.runtime.ILeftCombinator;
import jetbrains.mps.core.aspects.behaviour.api.SConstructor;
import org.jetbrains.annotations.Nullable;
import jetbrains.mps.core.aspects.behaviour.api.BHMethodNotFoundException;

public final class ContainingBaseParameterPointcut__BehaviorDescriptor extends BaseBHDescriptor {
  private static final SAbstractConcept CONCEPT = MetaAdapterFactory.getConcept(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x2ad3c27a34ad3b5bL, "no.uio.mLab.decisions.core.structure.ContainingBaseParameterPointcut");
  private static final BehaviorRegistry REGISTRY = ConceptRegistry.getInstance().getBehaviorRegistry();

  public static final SMethod<String> getDisplayAlias_id5Wfdz$0vc2$ = new SMethodBuilder<String>(new SJavaCompoundTypeImpl(String.class)).name("getDisplayAlias").modifiers(SModifiersImpl.create(9, AccessPrivileges.PUBLIC)).concept(CONCEPT).id("5Wfdz$0vc2$").registry(REGISTRY).build();
  public static final SMethod<String> getDisplayDescription_id5Wfdz$0vc3v = new SMethodBuilder<String>(new SJavaCompoundTypeImpl(String.class)).name("getDisplayDescription").modifiers(SModifiersImpl.create(9, AccessPrivileges.PUBLIC)).concept(CONCEPT).id("5Wfdz$0vc3v").registry(REGISTRY).build();
  public static final SMethod<JoinPointMatch> matches_id6khVixzleL4 = new SMethodBuilder<JoinPointMatch>(new SJavaCompoundTypeImpl(JoinPointMatch.class)).name("matches").modifiers(SModifiersImpl.create(8, AccessPrivileges.PUBLIC)).concept(CONCEPT).id("6khVixzleL4").registry(REGISTRY).build(SMethodBuilder.createJavaParameter((Class<SNode>) ((Class) Object.class), ""));
  /*package*/ static final SMethod<JoinPointMatch> matchParameter_id2FjKBCOG2yp = new SMethodBuilder<JoinPointMatch>(new SJavaCompoundTypeImpl(JoinPointMatch.class)).name("matchParameter").modifiers(SModifiersImpl.create(0, AccessPrivileges.PRIVATE)).concept(CONCEPT).id("2FjKBCOG2yp").registry(REGISTRY).build(SMethodBuilder.createJavaParameter((Class<SNode>) ((Class) Object.class), ""), SMethodBuilder.createJavaParameter((Class<List<SNode>>) ((Class) Object.class), ""));

  private static final List<SMethod<?>> BH_METHODS = Arrays.<SMethod<?>>asList(getDisplayAlias_id5Wfdz$0vc2$, getDisplayDescription_id5Wfdz$0vc3v, matches_id6khVixzleL4, matchParameter_id2FjKBCOG2yp);

  private static void ___init___(@NotNull SNode __thisNode__) {
  }

  /*package*/ static String getDisplayAlias_id5Wfdz$0vc2$(@NotNull SAbstractConcept __thisConcept__) {
    return "containing parameter";
  }
  /*package*/ static String getDisplayDescription_id5Wfdz$0vc3v(@NotNull SAbstractConcept __thisConcept__) {
    return "match rule sets that contain parameter";
  }
  /*package*/ static JoinPointMatch matches_id6khVixzleL4(@NotNull final SNode __thisNode__, SNode ruleSet) {
    // Gather data values in rule sets 
    final List<SNode> element = NodeCollectionUtil.getDistinctNodes(SNodeOperations.getNodeDescendants(ruleSet, MetaAdapterFactory.getConcept(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x2ad3c27a34ad3b5dL, "no.uio.mLab.decisions.core.structure.BaseParameter"), false, new SAbstractConcept[]{}));

    // Match against patterns and combine their results with `merge` 
    JoinPointMatch seed = ContainingBaseParameterPointcut__BehaviorDescriptor.matchParameter_id2FjKBCOG2yp.invoke(__thisNode__, ListSequence.fromList(SLinkOperations.getChildren(__thisNode__, MetaAdapterFactory.getContainmentLink(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x2ad3c27a34ad3b5bL, 0x2ad3c27a34ad3b5cL, "patterns"))).first(), element);
    return ListSequence.fromList(SLinkOperations.getChildren(__thisNode__, MetaAdapterFactory.getContainmentLink(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x2ad3c27a34ad3b5bL, 0x2ad3c27a34ad3b5cL, "patterns"))).skip(1).foldLeft(seed, new ILeftCombinator<SNode, JoinPointMatch>() {
      public JoinPointMatch combine(JoinPointMatch s, SNode it) {
        return s.merge(ContainingBaseParameterPointcut__BehaviorDescriptor.matchParameter_id2FjKBCOG2yp.invoke(__thisNode__, it, element));
      }
    });
  }
  /*package*/ static JoinPointMatch matchParameter_id2FjKBCOG2yp(@NotNull SNode __thisNode__, final SNode pattern, List<SNode> parameters) {
    // Match pattern against data values and combine their results with `union` 
    return ListSequence.fromList(parameters).skip(1).foldLeft(IBasePattern__BehaviorDescriptor.matches_id6LTgXmMs$_D.invoke(pattern, ListSequence.fromList(parameters).first()), new ILeftCombinator<SNode, JoinPointMatch>() {
      public JoinPointMatch combine(JoinPointMatch s, SNode it) {
        return s.union(IBasePattern__BehaviorDescriptor.matches_id6LTgXmMs$_D.invoke(pattern, it));
      }
    });
  }

  /*package*/ ContainingBaseParameterPointcut__BehaviorDescriptor() {
    super(REGISTRY);
  }

  @Override
  protected void initNode(@NotNull SNode node, @NotNull SConstructor constructor, @Nullable Object[] parameters) {
    ___init___(node);
  }

  @Override
  protected <T> T invokeSpecial0(@NotNull SNode node, @NotNull SMethod<T> method, @Nullable Object[] parameters) {
    int methodIndex = BH_METHODS.indexOf(method);
    if (methodIndex < 0) {
      throw new BHMethodNotFoundException(this, method);
    }
    switch (methodIndex) {
      case 2:
        return (T) ((JoinPointMatch) matches_id6khVixzleL4(node, (SNode) parameters[0]));
      case 3:
        return (T) ((JoinPointMatch) matchParameter_id2FjKBCOG2yp(node, (SNode) parameters[0], (List<SNode>) parameters[1]));
      default:
        throw new BHMethodNotFoundException(this, method);
    }
  }

  @Override
  protected <T> T invokeSpecial0(@NotNull SAbstractConcept concept, @NotNull SMethod<T> method, @Nullable Object[] parameters) {
    int methodIndex = BH_METHODS.indexOf(method);
    if (methodIndex < 0) {
      throw new BHMethodNotFoundException(this, method);
    }
    switch (methodIndex) {
      case 0:
        return (T) ((String) getDisplayAlias_id5Wfdz$0vc2$(concept));
      case 1:
        return (T) ((String) getDisplayDescription_id5Wfdz$0vc3v(concept));
      default:
        throw new BHMethodNotFoundException(this, method);
    }
  }

  @NotNull
  @Override
  public List<SMethod<?>> getDeclaredMethods() {
    return BH_METHODS;
  }

  @NotNull
  @Override
  public SAbstractConcept getConcept() {
    return CONCEPT;
  }
}
