package no.uio.mLab.decisions.core.behavior;

/*Generated by MPS */

import jetbrains.mps.core.aspects.behaviour.BaseBHDescriptor;
import org.jetbrains.mps.openapi.language.SAbstractConcept;
import jetbrains.mps.smodel.adapter.structure.MetaAdapterFactory;
import jetbrains.mps.core.aspects.behaviour.api.BehaviorRegistry;
import jetbrains.mps.smodel.language.ConceptRegistry;
import jetbrains.mps.core.aspects.behaviour.api.SMethod;
import jetbrains.mps.core.aspects.behaviour.SMethodBuilder;
import jetbrains.mps.core.aspects.behaviour.SJavaCompoundTypeImpl;
import jetbrains.mps.core.aspects.behaviour.SModifiersImpl;
import jetbrains.mps.core.aspects.behaviour.AccessPrivileges;
import java.util.List;
import java.util.Arrays;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.mps.openapi.model.SNode;
import jetbrains.mps.lang.smodel.generator.smodelAdapter.SPropertyOperations;
import jetbrains.mps.core.aspects.behaviour.api.SConstructor;
import org.jetbrains.annotations.Nullable;
import jetbrains.mps.core.aspects.behaviour.api.BHMethodNotFoundException;

public final class Day__BehaviorDescriptor extends BaseBHDescriptor {
  private static final SAbstractConcept CONCEPT = MetaAdapterFactory.getConcept(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0xd694e28d3eca46cL, "no.uio.mLab.decisions.core.structure.Day");
  private static final BehaviorRegistry REGISTRY = ConceptRegistry.getInstance().getBehaviorRegistry();

  public static final SMethod<String> getDisplayAlias_id5Wfdz$0vc2$ = new SMethodBuilder<String>(new SJavaCompoundTypeImpl(String.class)).name("getDisplayAlias").modifiers(SModifiersImpl.create(9, AccessPrivileges.PUBLIC)).concept(CONCEPT).id("5Wfdz$0vc2$").registry(REGISTRY).build();
  public static final SMethod<String> getDisplayDescription_id5Wfdz$0vc3v = new SMethodBuilder<String>(new SJavaCompoundTypeImpl(String.class)).name("getDisplayDescription").modifiers(SModifiersImpl.create(9, AccessPrivileges.PUBLIC)).concept(CONCEPT).id("5Wfdz$0vc3v").registry(REGISTRY).build();
  public static final SMethod<String> getSingularAlias_id1mAGFBJzhbW = new SMethodBuilder<String>(new SJavaCompoundTypeImpl(String.class)).name("getSingularAlias").modifiers(SModifiersImpl.create(9, AccessPrivileges.PUBLIC)).concept(CONCEPT).id("1mAGFBJzhbW").registry(REGISTRY).build();
  public static final SMethod<String> getPluralAlias_id1mAGFBJzh95 = new SMethodBuilder<String>(new SJavaCompoundTypeImpl(String.class)).name("getPluralAlias").modifiers(SModifiersImpl.create(9, AccessPrivileges.PUBLIC)).concept(CONCEPT).id("1mAGFBJzh95").registry(REGISTRY).build();
  public static final SMethod<String> getInvalidValue_idPDjyzjVJ6U = new SMethodBuilder<String>(new SJavaCompoundTypeImpl(String.class)).name("getInvalidValue").modifiers(SModifiersImpl.create(9, AccessPrivileges.PUBLIC)).concept(CONCEPT).id("PDjyzjVJ6U").registry(REGISTRY).build();
  public static final SMethod<String> toGenerationString_id499Gn2DGU_t = new SMethodBuilder<String>(new SJavaCompoundTypeImpl(String.class)).name("toGenerationString").modifiers(SModifiersImpl.create(8, AccessPrivileges.PUBLIC)).concept(CONCEPT).id("499Gn2DGU_t").registry(REGISTRY).build();

  private static final List<SMethod<?>> BH_METHODS = Arrays.<SMethod<?>>asList(getDisplayAlias_id5Wfdz$0vc2$, getDisplayDescription_id5Wfdz$0vc3v, getSingularAlias_id1mAGFBJzhbW, getPluralAlias_id1mAGFBJzh95, getInvalidValue_idPDjyzjVJ6U, toGenerationString_id499Gn2DGU_t);

  private static void ___init___(@NotNull SNode __thisNode__) {
  }

  /*package*/ static String getDisplayAlias_id5Wfdz$0vc2$(@NotNull SAbstractConcept __thisConcept__) {
    return ITranslatableCoreConcept__BehaviorDescriptor.getDisplayTranslations_id1Hxyv4EQSwS.invoke(__thisConcept__).getDayPluralAlias();
  }
  /*package*/ static String getDisplayDescription_id5Wfdz$0vc3v(@NotNull SAbstractConcept __thisConcept__) {
    return ITranslatableCoreConcept__BehaviorDescriptor.getDisplayTranslations_id1Hxyv4EQSwS.invoke(__thisConcept__).getDayDescription();
  }
  /*package*/ static String getSingularAlias_id1mAGFBJzhbW(@NotNull SAbstractConcept __thisConcept__) {
    return ITranslatableCoreConcept__BehaviorDescriptor.getDisplayTranslations_id1Hxyv4EQSwS.invoke(__thisConcept__).getDaySingularAlias();
  }
  /*package*/ static String getPluralAlias_id1mAGFBJzh95(@NotNull SAbstractConcept __thisConcept__) {
    return ITranslatableCoreConcept__BehaviorDescriptor.getDisplayTranslations_id1Hxyv4EQSwS.invoke(__thisConcept__).getDayPluralAlias();
  }
  /*package*/ static String getInvalidValue_idPDjyzjVJ6U(@NotNull SAbstractConcept __thisConcept__) {
    return ITranslatableCoreConcept__BehaviorDescriptor.getDisplayTranslations_id1Hxyv4EQSwS.invoke(__thisConcept__).getDayInvalidValueError();
  }
  /*package*/ static String toGenerationString_id499Gn2DGU_t(@NotNull SNode __thisNode__) {
    String alias = (((boolean) TimeSpanLiteral__BehaviorDescriptor.isPlural_id1Hxyv4DViok.invoke(__thisNode__.getConcept(), ((int) SPropertyOperations.getInteger(__thisNode__, MetaAdapterFactory.getProperty(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x15a6b2b9ef5832e4L, 0x15a6b2b9ef583303L, "value"))))) ? ITranslatableCoreConcept__BehaviorDescriptor.getGenerationTranslations_id1Hxyv4EQSBV.invoke(__thisNode__.getConcept()).getDaySingularAlias() : ITranslatableCoreConcept__BehaviorDescriptor.getGenerationTranslations_id1Hxyv4EQSBV.invoke(__thisNode__.getConcept()).getDayPluralAlias());
    return String.format("%s %s", SPropertyOperations.getInteger(__thisNode__, MetaAdapterFactory.getProperty(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x15a6b2b9ef5832e4L, 0x15a6b2b9ef583303L, "value")), alias);
  }

  /*package*/ Day__BehaviorDescriptor() {
    super(REGISTRY);
  }

  @Override
  protected void initNode(@NotNull SNode node, @NotNull SConstructor constructor, @Nullable Object[] parameters) {
    ___init___(node);
  }

  @Override
  protected <T> T invokeSpecial0(@NotNull SNode node, @NotNull SMethod<T> method, @Nullable Object[] parameters) {
    int methodIndex = BH_METHODS.indexOf(method);
    if (methodIndex < 0) {
      throw new BHMethodNotFoundException(this, method);
    }
    switch (methodIndex) {
      case 5:
        return (T) ((String) toGenerationString_id499Gn2DGU_t(node));
      default:
        throw new BHMethodNotFoundException(this, method);
    }
  }

  @Override
  protected <T> T invokeSpecial0(@NotNull SAbstractConcept concept, @NotNull SMethod<T> method, @Nullable Object[] parameters) {
    int methodIndex = BH_METHODS.indexOf(method);
    if (methodIndex < 0) {
      throw new BHMethodNotFoundException(this, method);
    }
    switch (methodIndex) {
      case 0:
        return (T) ((String) getDisplayAlias_id5Wfdz$0vc2$(concept));
      case 1:
        return (T) ((String) getDisplayDescription_id5Wfdz$0vc3v(concept));
      case 2:
        return (T) ((String) getSingularAlias_id1mAGFBJzhbW(concept));
      case 3:
        return (T) ((String) getPluralAlias_id1mAGFBJzh95(concept));
      case 4:
        return (T) ((String) getInvalidValue_idPDjyzjVJ6U(concept));
      default:
        throw new BHMethodNotFoundException(this, method);
    }
  }

  @NotNull
  @Override
  public List<SMethod<?>> getDeclaredMethods() {
    return BH_METHODS;
  }

  @NotNull
  @Override
  public SAbstractConcept getConcept() {
    return CONCEPT;
  }
}
