package no.uio.mLab.decisions.core.constraints;

/*Generated by MPS */

import jetbrains.mps.smodel.runtime.BaseConstraintsAspectDescriptor;
import jetbrains.mps.smodel.runtime.ConstraintsDescriptor;
import org.jetbrains.mps.openapi.language.SAbstractConcept;
import jetbrains.mps.smodel.runtime.base.BaseConstraintsDescriptor;
import jetbrains.mps.lang.smodel.ConceptSwitchIndex;
import jetbrains.mps.lang.smodel.ConceptSwitchIndexBuilder;
import jetbrains.mps.smodel.adapter.ids.MetaIdFactory;

public class ConstraintsAspectDescriptor extends BaseConstraintsAspectDescriptor {
  public ConstraintsAspectDescriptor() {
  }

  @Override
  public ConstraintsDescriptor getConstraints(SAbstractConcept concept) {
    SAbstractConcept cncpt = concept;
    switch (conceptIndex.index(cncpt)) {
      case 0:
        return new Constraint_Constraints();
      case 1:
        return new ISelectedEntityPattern_Constraints();
      case 2:
        return new NamedConditionReference_Constraints();
      default:
    }
    return new BaseConstraintsDescriptor(concept);
  }
  private static final ConceptSwitchIndex conceptIndex = new ConceptSwitchIndexBuilder().put(MetaIdFactory.conceptId(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x15a6b2b9f06b6705L), MetaIdFactory.conceptId(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x614e6711f265b51dL), MetaIdFactory.conceptId(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x15a6b2b9efc49b11L)).seal();
}
