<?xml version="1.0" encoding="UTF-8"?>
<model ref="00000000-0000-4000-5f02-5beb5f025beb/i:f16ddcc(checkpoints/no.uio.mLab.decisions.core.constraints@descriptorclasses)">
  <persistence version="9" />
  <attribute name="checkpoint" value="DescriptorClasses" />
  <attribute name="generation-plan" value="AspectCPS" />
  <languages />
  <imports>
    <import index="sw9n" ref="r:ac11bdd6-c85d-40e0-910f-d45067cda53d(no.uio.mLab.decisions.core.constraints)" />
    <import index="wb6c" ref="r:19d15037-83c1-4ae2-b66a-763828cb8279(no.uio.mLab.decisions.core.behavior)" />
    <import index="79pl" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.smodel.runtime.base(MPS.Core/)" />
    <import index="mhfm" ref="3f233e7f-b8a6-46d2-a57f-795d56775243/java:org.jetbrains.annotations(Annotations/)" />
    <import index="mhbf" ref="8865b7a8-5271-43d3-884c-6fd1d9cfdd34/java:org.jetbrains.mps.openapi.model(MPS.OpenAPI/)" />
    <import index="c17a" ref="8865b7a8-5271-43d3-884c-6fd1d9cfdd34/java:org.jetbrains.mps.openapi.language(MPS.OpenAPI/)" />
    <import index="2k9e" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.smodel.adapter.structure(MPS.Core/)" />
    <import index="ze1i" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.smodel.runtime(MPS.Core/)" />
    <import index="e8bb" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.smodel.adapter.ids(MPS.Core/)" />
    <import index="7f9y" ref="r:790ac37e-42d3-4dd3-977c-e8fe9cc56a45(no.uio.mLab.decisions.core.structure)" />
    <import index="33ny" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.util(JDK/)" />
    <import index="35tq" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.scope(MPS.Core/)" />
    <import index="w1kc" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.smodel(MPS.Core/)" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1082485599095" name="jetbrains.mps.baseLanguage.structure.BlockStatement" flags="nn" index="9aQIb">
        <child id="1082485599096" name="statements" index="9aQI4" />
      </concept>
      <concept id="4836112446988635817" name="jetbrains.mps.baseLanguage.structure.UndefinedType" flags="in" index="2jxLKc" />
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1465982738277781862" name="jetbrains.mps.baseLanguage.structure.PlaceholderMember" flags="ng" index="2tJIrI" />
      <concept id="1188207840427" name="jetbrains.mps.baseLanguage.structure.AnnotationInstance" flags="nn" index="2AHcQZ">
        <reference id="1188208074048" name="annotation" index="2AI5Lk" />
      </concept>
      <concept id="1188208481402" name="jetbrains.mps.baseLanguage.structure.HasAnnotation" flags="ng" index="2AJDlI">
        <child id="1188208488637" name="annotation" index="2AJF6D" />
      </concept>
      <concept id="1224848483129" name="jetbrains.mps.baseLanguage.structure.IBLDeprecatable" flags="ng" index="IEa8$">
        <property id="1224848525476" name="isDeprecated" index="IEkAT" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1070475354124" name="jetbrains.mps.baseLanguage.structure.ThisExpression" flags="nn" index="Xjq3P" />
      <concept id="1070475587102" name="jetbrains.mps.baseLanguage.structure.SuperConstructorInvocation" flags="nn" index="XkiVB" />
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1182160077978" name="jetbrains.mps.baseLanguage.structure.AnonymousClassCreator" flags="nn" index="YeOm9">
        <child id="1182160096073" name="cls" index="YeSDq" />
      </concept>
      <concept id="1081236700938" name="jetbrains.mps.baseLanguage.structure.StaticMethodDeclaration" flags="ig" index="2YIFZL" />
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1070534058343" name="jetbrains.mps.baseLanguage.structure.NullLiteral" flags="nn" index="10Nm6u" />
      <concept id="1070534644030" name="jetbrains.mps.baseLanguage.structure.BooleanType" flags="in" index="10P_77" />
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu">
        <child id="1165602531693" name="superclass" index="1zkMxy" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <property id="1176718929932" name="isFinal" index="3TUv4t" />
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="4269842503726207156" name="jetbrains.mps.baseLanguage.structure.LongLiteral" flags="nn" index="1adDum">
        <property id="4269842503726207157" name="value" index="1adDun" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <property id="4276006055363816570" name="isSynchronized" index="od$2w" />
        <property id="1181808852946" name="isFinal" index="DiZV1" />
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123165" name="jetbrains.mps.baseLanguage.structure.InstanceMethodDeclaration" flags="ig" index="3clFb_">
        <property id="1178608670077" name="isAbstract" index="1EzhhJ" />
      </concept>
      <concept id="1068580123152" name="jetbrains.mps.baseLanguage.structure.EqualsExpression" flags="nn" index="3clFbC" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123137" name="jetbrains.mps.baseLanguage.structure.BooleanConstant" flags="nn" index="3clFbT">
        <property id="1068580123138" name="value" index="3clFbU" />
      </concept>
      <concept id="1068580123140" name="jetbrains.mps.baseLanguage.structure.ConstructorDeclaration" flags="ig" index="3clFbW" />
      <concept id="1068581242875" name="jetbrains.mps.baseLanguage.structure.PlusExpression" flags="nn" index="3cpWs3" />
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1068581517677" name="jetbrains.mps.baseLanguage.structure.VoidType" flags="in" index="3cqZAl" />
      <concept id="1081516740877" name="jetbrains.mps.baseLanguage.structure.NotExpression" flags="nn" index="3fqX7Q">
        <child id="1081516765348" name="expression" index="3fr31v" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1212685548494" name="jetbrains.mps.baseLanguage.structure.ClassCreator" flags="nn" index="1pGfFk">
        <child id="1212687122400" name="typeParameter" index="1pMfVU" />
      </concept>
      <concept id="1107461130800" name="jetbrains.mps.baseLanguage.structure.Classifier" flags="ng" index="3pOWGL">
        <property id="521412098689998745" name="nonStatic" index="2bfB8j" />
        <child id="5375687026011219971" name="member" index="jymVt" unordered="true" />
      </concept>
      <concept id="7812454656619025412" name="jetbrains.mps.baseLanguage.structure.LocalMethodCall" flags="nn" index="1rXfSq" />
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
        <child id="1109201940907" name="parameter" index="11_B2D" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1073239437375" name="jetbrains.mps.baseLanguage.structure.NotEqualsExpression" flags="nn" index="3y3z36" />
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1163668896201" name="jetbrains.mps.baseLanguage.structure.TernaryOperatorExpression" flags="nn" index="3K4zz7">
        <child id="1163668914799" name="condition" index="3K4Cdx" />
        <child id="1163668922816" name="ifTrue" index="3K4E3e" />
        <child id="1163668934364" name="ifFalse" index="3K4GZi" />
      </concept>
      <concept id="5497648299878491908" name="jetbrains.mps.baseLanguage.structure.BaseVariableReference" flags="nn" index="1M0zk4">
        <reference id="5497648299878491909" name="baseVariableDeclaration" index="1M0zk5" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
      <concept id="1146644623116" name="jetbrains.mps.baseLanguage.structure.PrivateVisibility" flags="nn" index="3Tm6S6" />
      <concept id="1146644641414" name="jetbrains.mps.baseLanguage.structure.ProtectedVisibility" flags="nn" index="3Tmbuc" />
      <concept id="1080120340718" name="jetbrains.mps.baseLanguage.structure.AndExpression" flags="nn" index="1Wc70l" />
      <concept id="1170345865475" name="jetbrains.mps.baseLanguage.structure.AnonymousClass" flags="ig" index="1Y3b0j">
        <reference id="1170346070688" name="classifier" index="1Y3XeK" />
        <child id="1201186121363" name="typeParameter" index="2Ghqu4" />
      </concept>
    </language>
    <language id="b401a680-8325-4110-8fd3-84331ff25bef" name="jetbrains.mps.lang.generator">
      <concept id="7980339663309897032" name="jetbrains.mps.lang.generator.structure.OriginTrace" flags="ng" index="cd27G">
        <child id="7980339663309897037" name="origin" index="cd27D" />
      </concept>
      <concept id="3864140621129707969" name="jetbrains.mps.lang.generator.structure.GeneratorDebug_Mappings" flags="nn" index="39dXUE" />
      <concept id="3637169702552512264" name="jetbrains.mps.lang.generator.structure.ElementaryNodeId" flags="ng" index="3u3nmq">
        <property id="3637169702552512269" name="nodeId" index="3u3nmv" />
      </concept>
    </language>
    <language id="760a0a8c-eabb-4521-8bfd-65db761a9ba3" name="jetbrains.mps.baseLanguage.logging">
      <concept id="6332851714983831325" name="jetbrains.mps.baseLanguage.logging.structure.MsgStatement" flags="ng" index="2xdQw9">
        <property id="6332851714983843871" name="severity" index="2xdLsb" />
        <child id="5721587534047265374" name="message" index="9lYJi" />
      </concept>
    </language>
    <language id="df345b11-b8c7-4213-ac66-48d2a9b75d88" name="jetbrains.mps.baseLanguageInternal">
      <concept id="1238251434034" name="jetbrains.mps.baseLanguageInternal.structure.ExtractToConstantExpression" flags="ng" index="1dyn4i">
        <property id="1238251449050" name="fieldName" index="1dyqJU" />
        <property id="8835849473318867199" name="makeUnique" index="1zomUR" />
        <child id="1238251454130" name="expression" index="1dyrYi" />
      </concept>
      <concept id="1173996401517" name="jetbrains.mps.baseLanguageInternal.structure.InternalNewExpression" flags="nn" index="1nCR9W">
        <property id="1173996588177" name="fqClassName" index="1nD$Q0" />
        <child id="1179332974947" name="type" index="2lIhxL" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="7453996997717780434" name="jetbrains.mps.lang.smodel.structure.Node_GetSConceptOperation" flags="nn" index="2yIwOk" />
      <concept id="2396822768958367367" name="jetbrains.mps.lang.smodel.structure.AbstractTypeCastExpression" flags="nn" index="$5XWr">
        <child id="6733348108486823193" name="leftExpression" index="1m5AlR" />
        <child id="3906496115198199033" name="conceptArgument" index="3oSUPX" />
      </concept>
      <concept id="1761385620274348152" name="jetbrains.mps.lang.smodel.structure.SConceptTypeCastExpression" flags="nn" index="2CBFar" />
      <concept id="1883223317721008708" name="jetbrains.mps.lang.smodel.structure.IfInstanceOfStatement" flags="nn" index="Jncv_">
        <reference id="1883223317721008712" name="nodeConcept" index="JncvD" />
        <child id="1883223317721008709" name="body" index="Jncv$" />
        <child id="1883223317721008711" name="variable" index="JncvA" />
        <child id="1883223317721008710" name="nodeExpression" index="JncvB" />
      </concept>
      <concept id="1883223317721008713" name="jetbrains.mps.lang.smodel.structure.IfInstanceOfVariable" flags="ng" index="JncvC" />
      <concept id="1883223317721107059" name="jetbrains.mps.lang.smodel.structure.IfInstanceOfVarReference" flags="nn" index="Jnkvi" />
      <concept id="1180031783296" name="jetbrains.mps.lang.smodel.structure.Concept_IsSubConceptOfOperation" flags="nn" index="2Zo12i">
        <child id="1180031783297" name="conceptArgument" index="2Zo12j" />
      </concept>
      <concept id="2644386474300074836" name="jetbrains.mps.lang.smodel.structure.ConceptIdRefExpression" flags="nn" index="35c_gC">
        <reference id="2644386474300074837" name="conceptDeclaration" index="35c_gD" />
      </concept>
      <concept id="6677504323281689838" name="jetbrains.mps.lang.smodel.structure.SConceptType" flags="in" index="3bZ5Sz" />
      <concept id="1154546950173" name="jetbrains.mps.lang.smodel.structure.ConceptReference" flags="ng" index="3gn64h">
        <reference id="1154546997487" name="concept" index="3gnhBz" />
      </concept>
      <concept id="6039268229364358244" name="jetbrains.mps.lang.smodel.structure.ExactConceptCase" flags="ng" index="1pnPoh">
        <child id="6039268229364358388" name="body" index="1pnPq1" />
        <child id="6039268229364358387" name="concept" index="1pnPq6" />
      </concept>
      <concept id="5944356402132808749" name="jetbrains.mps.lang.smodel.structure.ConceptSwitchStatement" flags="nn" index="1_3QMa">
        <child id="6039268229365417680" name="defaultBlock" index="1prKM_" />
        <child id="5944356402132808753" name="case" index="1_3QMm" />
        <child id="5944356402132808752" name="expression" index="1_3QMn" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="312cEu" id="0">
    <property role="3GE5qa" value="base.constraints" />
    <property role="TrG5h" value="Constraint_Constraints" />
    <node concept="3Tm1VV" id="1" role="1B3o_S">
      <node concept="cd27G" id="8" role="lGtFl">
        <node concept="3u3nmq" id="9" role="cd27D">
          <property role="3u3nmv" value="1560130832609177896" />
        </node>
      </node>
    </node>
    <node concept="3uibUv" id="2" role="1zkMxy">
      <ref role="3uigEE" to="79pl:~BaseConstraintsDescriptor" resolve="BaseConstraintsDescriptor" />
      <node concept="cd27G" id="a" role="lGtFl">
        <node concept="3u3nmq" id="b" role="cd27D">
          <property role="3u3nmv" value="1560130832609177896" />
        </node>
      </node>
    </node>
    <node concept="3clFbW" id="3" role="jymVt">
      <node concept="3cqZAl" id="c" role="3clF45">
        <node concept="cd27G" id="g" role="lGtFl">
          <node concept="3u3nmq" id="h" role="cd27D">
            <property role="3u3nmv" value="1560130832609177896" />
          </node>
        </node>
      </node>
      <node concept="3clFbS" id="d" role="3clF47">
        <node concept="XkiVB" id="i" role="3cqZAp">
          <ref role="37wK5l" to="79pl:~BaseConstraintsDescriptor.&lt;init&gt;(org.jetbrains.mps.openapi.language.SAbstractConcept)" resolve="BaseConstraintsDescriptor" />
          <node concept="2YIFZM" id="k" role="37wK5m">
            <ref role="1Pybhc" to="2k9e:~MetaAdapterFactory" resolve="MetaAdapterFactory" />
            <ref role="37wK5l" to="2k9e:~MetaAdapterFactory.getConcept(long,long,long,java.lang.String):org.jetbrains.mps.openapi.language.SConcept" resolve="getConcept" />
            <node concept="1adDum" id="m" role="37wK5m">
              <property role="1adDun" value="0xc610a4ebc47c4f95L" />
              <node concept="cd27G" id="r" role="lGtFl">
                <node concept="3u3nmq" id="s" role="cd27D">
                  <property role="3u3nmv" value="1560130832609177896" />
                </node>
              </node>
            </node>
            <node concept="1adDum" id="n" role="37wK5m">
              <property role="1adDun" value="0xb56811236971769cL" />
              <node concept="cd27G" id="t" role="lGtFl">
                <node concept="3u3nmq" id="u" role="cd27D">
                  <property role="3u3nmv" value="1560130832609177896" />
                </node>
              </node>
            </node>
            <node concept="1adDum" id="o" role="37wK5m">
              <property role="1adDun" value="0x15a6b2b9f06b6705L" />
              <node concept="cd27G" id="v" role="lGtFl">
                <node concept="3u3nmq" id="w" role="cd27D">
                  <property role="3u3nmv" value="1560130832609177896" />
                </node>
              </node>
            </node>
            <node concept="Xl_RD" id="p" role="37wK5m">
              <property role="Xl_RC" value="no.uio.mLab.decisions.core.structure.Constraint" />
              <node concept="cd27G" id="x" role="lGtFl">
                <node concept="3u3nmq" id="y" role="cd27D">
                  <property role="3u3nmv" value="1560130832609177896" />
                </node>
              </node>
            </node>
            <node concept="cd27G" id="q" role="lGtFl">
              <node concept="3u3nmq" id="z" role="cd27D">
                <property role="3u3nmv" value="1560130832609177896" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="l" role="lGtFl">
            <node concept="3u3nmq" id="$" role="cd27D">
              <property role="3u3nmv" value="1560130832609177896" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="j" role="lGtFl">
          <node concept="3u3nmq" id="_" role="cd27D">
            <property role="3u3nmv" value="1560130832609177896" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="e" role="1B3o_S">
        <node concept="cd27G" id="A" role="lGtFl">
          <node concept="3u3nmq" id="B" role="cd27D">
            <property role="3u3nmv" value="1560130832609177896" />
          </node>
        </node>
      </node>
      <node concept="cd27G" id="f" role="lGtFl">
        <node concept="3u3nmq" id="C" role="cd27D">
          <property role="3u3nmv" value="1560130832609177896" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="4" role="jymVt">
      <node concept="cd27G" id="D" role="lGtFl">
        <node concept="3u3nmq" id="E" role="cd27D">
          <property role="3u3nmv" value="1560130832609177896" />
        </node>
      </node>
    </node>
    <node concept="3clFb_" id="5" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="calculateCanBeChildConstraint" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3Tmbuc" id="F" role="1B3o_S">
        <node concept="cd27G" id="K" role="lGtFl">
          <node concept="3u3nmq" id="L" role="cd27D">
            <property role="3u3nmv" value="1560130832609177896" />
          </node>
        </node>
      </node>
      <node concept="3uibUv" id="G" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConstraintFunction" resolve="ConstraintFunction" />
        <node concept="3uibUv" id="M" role="11_B2D">
          <ref role="3uigEE" to="ze1i:~ConstraintContext_CanBeChild" resolve="ConstraintContext_CanBeChild" />
          <node concept="cd27G" id="P" role="lGtFl">
            <node concept="3u3nmq" id="Q" role="cd27D">
              <property role="3u3nmv" value="1560130832609177896" />
            </node>
          </node>
        </node>
        <node concept="3uibUv" id="N" role="11_B2D">
          <ref role="3uigEE" to="wyt6:~Boolean" resolve="Boolean" />
          <node concept="cd27G" id="R" role="lGtFl">
            <node concept="3u3nmq" id="S" role="cd27D">
              <property role="3u3nmv" value="1560130832609177896" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="O" role="lGtFl">
          <node concept="3u3nmq" id="T" role="cd27D">
            <property role="3u3nmv" value="1560130832609177896" />
          </node>
        </node>
      </node>
      <node concept="3clFbS" id="H" role="3clF47">
        <node concept="3clFbF" id="U" role="3cqZAp">
          <node concept="2ShNRf" id="W" role="3clFbG">
            <node concept="YeOm9" id="Y" role="2ShVmc">
              <node concept="1Y3b0j" id="10" role="YeSDq">
                <property role="2bfB8j" value="true" />
                <ref role="1Y3XeK" to="ze1i:~ConstraintFunction" resolve="ConstraintFunction" />
                <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
                <node concept="3Tm1VV" id="12" role="1B3o_S">
                  <node concept="cd27G" id="17" role="lGtFl">
                    <node concept="3u3nmq" id="18" role="cd27D">
                      <property role="3u3nmv" value="1560130832609177896" />
                    </node>
                  </node>
                </node>
                <node concept="3clFb_" id="13" role="jymVt">
                  <property role="1EzhhJ" value="false" />
                  <property role="TrG5h" value="invoke" />
                  <property role="DiZV1" value="false" />
                  <property role="od$2w" value="false" />
                  <node concept="3Tm1VV" id="19" role="1B3o_S">
                    <node concept="cd27G" id="1g" role="lGtFl">
                      <node concept="3u3nmq" id="1h" role="cd27D">
                        <property role="3u3nmv" value="1560130832609177896" />
                      </node>
                    </node>
                  </node>
                  <node concept="2AHcQZ" id="1a" role="2AJF6D">
                    <ref role="2AI5Lk" to="mhfm:~NotNull" resolve="NotNull" />
                    <node concept="cd27G" id="1i" role="lGtFl">
                      <node concept="3u3nmq" id="1j" role="cd27D">
                        <property role="3u3nmv" value="1560130832609177896" />
                      </node>
                    </node>
                  </node>
                  <node concept="3uibUv" id="1b" role="3clF45">
                    <ref role="3uigEE" to="wyt6:~Boolean" resolve="Boolean" />
                    <node concept="cd27G" id="1k" role="lGtFl">
                      <node concept="3u3nmq" id="1l" role="cd27D">
                        <property role="3u3nmv" value="1560130832609177896" />
                      </node>
                    </node>
                  </node>
                  <node concept="37vLTG" id="1c" role="3clF46">
                    <property role="TrG5h" value="context" />
                    <node concept="3uibUv" id="1m" role="1tU5fm">
                      <ref role="3uigEE" to="ze1i:~ConstraintContext_CanBeChild" resolve="ConstraintContext_CanBeChild" />
                      <node concept="cd27G" id="1p" role="lGtFl">
                        <node concept="3u3nmq" id="1q" role="cd27D">
                          <property role="3u3nmv" value="1560130832609177896" />
                        </node>
                      </node>
                    </node>
                    <node concept="2AHcQZ" id="1n" role="2AJF6D">
                      <ref role="2AI5Lk" to="mhfm:~NotNull" resolve="NotNull" />
                      <node concept="cd27G" id="1r" role="lGtFl">
                        <node concept="3u3nmq" id="1s" role="cd27D">
                          <property role="3u3nmv" value="1560130832609177896" />
                        </node>
                      </node>
                    </node>
                    <node concept="cd27G" id="1o" role="lGtFl">
                      <node concept="3u3nmq" id="1t" role="cd27D">
                        <property role="3u3nmv" value="1560130832609177896" />
                      </node>
                    </node>
                  </node>
                  <node concept="37vLTG" id="1d" role="3clF46">
                    <property role="TrG5h" value="checkingNodeContext" />
                    <node concept="3uibUv" id="1u" role="1tU5fm">
                      <ref role="3uigEE" to="ze1i:~CheckingNodeContext" resolve="CheckingNodeContext" />
                      <node concept="cd27G" id="1x" role="lGtFl">
                        <node concept="3u3nmq" id="1y" role="cd27D">
                          <property role="3u3nmv" value="1560130832609177896" />
                        </node>
                      </node>
                    </node>
                    <node concept="2AHcQZ" id="1v" role="2AJF6D">
                      <ref role="2AI5Lk" to="mhfm:~Nullable" resolve="Nullable" />
                      <node concept="cd27G" id="1z" role="lGtFl">
                        <node concept="3u3nmq" id="1$" role="cd27D">
                          <property role="3u3nmv" value="1560130832609177896" />
                        </node>
                      </node>
                    </node>
                    <node concept="cd27G" id="1w" role="lGtFl">
                      <node concept="3u3nmq" id="1_" role="cd27D">
                        <property role="3u3nmv" value="1560130832609177896" />
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbS" id="1e" role="3clF47">
                    <node concept="3cpWs8" id="1A" role="3cqZAp">
                      <node concept="3cpWsn" id="1G" role="3cpWs9">
                        <property role="TrG5h" value="result" />
                        <node concept="10P_77" id="1I" role="1tU5fm">
                          <node concept="cd27G" id="1L" role="lGtFl">
                            <node concept="3u3nmq" id="1M" role="cd27D">
                              <property role="3u3nmv" value="1560130832609177896" />
                            </node>
                          </node>
                        </node>
                        <node concept="1rXfSq" id="1J" role="33vP2m">
                          <ref role="37wK5l" node="6" resolve="staticCanBeAChild" />
                          <node concept="2OqwBi" id="1N" role="37wK5m">
                            <node concept="37vLTw" id="1S" role="2Oq$k0">
                              <ref role="3cqZAo" node="1c" resolve="context" />
                              <node concept="cd27G" id="1V" role="lGtFl">
                                <node concept="3u3nmq" id="1W" role="cd27D">
                                  <property role="3u3nmv" value="1560130832609177896" />
                                </node>
                              </node>
                            </node>
                            <node concept="liA8E" id="1T" role="2OqNvi">
                              <ref role="37wK5l" to="ze1i:~ConstraintContext_CanBeChild.getNode():org.jetbrains.mps.openapi.model.SNode" resolve="getNode" />
                              <node concept="cd27G" id="1X" role="lGtFl">
                                <node concept="3u3nmq" id="1Y" role="cd27D">
                                  <property role="3u3nmv" value="1560130832609177896" />
                                </node>
                              </node>
                            </node>
                            <node concept="cd27G" id="1U" role="lGtFl">
                              <node concept="3u3nmq" id="1Z" role="cd27D">
                                <property role="3u3nmv" value="1560130832609177896" />
                              </node>
                            </node>
                          </node>
                          <node concept="2OqwBi" id="1O" role="37wK5m">
                            <node concept="37vLTw" id="20" role="2Oq$k0">
                              <ref role="3cqZAo" node="1c" resolve="context" />
                              <node concept="cd27G" id="23" role="lGtFl">
                                <node concept="3u3nmq" id="24" role="cd27D">
                                  <property role="3u3nmv" value="1560130832609177896" />
                                </node>
                              </node>
                            </node>
                            <node concept="liA8E" id="21" role="2OqNvi">
                              <ref role="37wK5l" to="ze1i:~ConstraintContext_CanBeChild.getParentNode():org.jetbrains.mps.openapi.model.SNode" resolve="getParentNode" />
                              <node concept="cd27G" id="25" role="lGtFl">
                                <node concept="3u3nmq" id="26" role="cd27D">
                                  <property role="3u3nmv" value="1560130832609177896" />
                                </node>
                              </node>
                            </node>
                            <node concept="cd27G" id="22" role="lGtFl">
                              <node concept="3u3nmq" id="27" role="cd27D">
                                <property role="3u3nmv" value="1560130832609177896" />
                              </node>
                            </node>
                          </node>
                          <node concept="2OqwBi" id="1P" role="37wK5m">
                            <node concept="37vLTw" id="28" role="2Oq$k0">
                              <ref role="3cqZAo" node="1c" resolve="context" />
                              <node concept="cd27G" id="2b" role="lGtFl">
                                <node concept="3u3nmq" id="2c" role="cd27D">
                                  <property role="3u3nmv" value="1560130832609177896" />
                                </node>
                              </node>
                            </node>
                            <node concept="liA8E" id="29" role="2OqNvi">
                              <ref role="37wK5l" to="ze1i:~ConstraintContext_CanBeChild.getConcept():org.jetbrains.mps.openapi.language.SAbstractConcept" resolve="getConcept" />
                              <node concept="cd27G" id="2d" role="lGtFl">
                                <node concept="3u3nmq" id="2e" role="cd27D">
                                  <property role="3u3nmv" value="1560130832609177896" />
                                </node>
                              </node>
                            </node>
                            <node concept="cd27G" id="2a" role="lGtFl">
                              <node concept="3u3nmq" id="2f" role="cd27D">
                                <property role="3u3nmv" value="1560130832609177896" />
                              </node>
                            </node>
                          </node>
                          <node concept="2OqwBi" id="1Q" role="37wK5m">
                            <node concept="37vLTw" id="2g" role="2Oq$k0">
                              <ref role="3cqZAo" node="1c" resolve="context" />
                              <node concept="cd27G" id="2j" role="lGtFl">
                                <node concept="3u3nmq" id="2k" role="cd27D">
                                  <property role="3u3nmv" value="1560130832609177896" />
                                </node>
                              </node>
                            </node>
                            <node concept="liA8E" id="2h" role="2OqNvi">
                              <ref role="37wK5l" to="ze1i:~ConstraintContext_CanBeChild.getLink():org.jetbrains.mps.openapi.language.SContainmentLink" resolve="getLink" />
                              <node concept="cd27G" id="2l" role="lGtFl">
                                <node concept="3u3nmq" id="2m" role="cd27D">
                                  <property role="3u3nmv" value="1560130832609177896" />
                                </node>
                              </node>
                            </node>
                            <node concept="cd27G" id="2i" role="lGtFl">
                              <node concept="3u3nmq" id="2n" role="cd27D">
                                <property role="3u3nmv" value="1560130832609177896" />
                              </node>
                            </node>
                          </node>
                          <node concept="cd27G" id="1R" role="lGtFl">
                            <node concept="3u3nmq" id="2o" role="cd27D">
                              <property role="3u3nmv" value="1560130832609177896" />
                            </node>
                          </node>
                        </node>
                        <node concept="cd27G" id="1K" role="lGtFl">
                          <node concept="3u3nmq" id="2p" role="cd27D">
                            <property role="3u3nmv" value="1560130832609177896" />
                          </node>
                        </node>
                      </node>
                      <node concept="cd27G" id="1H" role="lGtFl">
                        <node concept="3u3nmq" id="2q" role="cd27D">
                          <property role="3u3nmv" value="1560130832609177896" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbH" id="1B" role="3cqZAp">
                      <node concept="cd27G" id="2r" role="lGtFl">
                        <node concept="3u3nmq" id="2s" role="cd27D">
                          <property role="3u3nmv" value="1560130832609177896" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbJ" id="1C" role="3cqZAp">
                      <node concept="3clFbS" id="2t" role="3clFbx">
                        <node concept="3clFbF" id="2w" role="3cqZAp">
                          <node concept="2OqwBi" id="2y" role="3clFbG">
                            <node concept="37vLTw" id="2$" role="2Oq$k0">
                              <ref role="3cqZAo" node="1d" resolve="checkingNodeContext" />
                              <node concept="cd27G" id="2B" role="lGtFl">
                                <node concept="3u3nmq" id="2C" role="cd27D">
                                  <property role="3u3nmv" value="1560130832609177896" />
                                </node>
                              </node>
                            </node>
                            <node concept="liA8E" id="2_" role="2OqNvi">
                              <ref role="37wK5l" to="ze1i:~CheckingNodeContext.setBreakingNode(org.jetbrains.mps.openapi.model.SNodeReference):void" resolve="setBreakingNode" />
                              <node concept="1dyn4i" id="2D" role="37wK5m">
                                <property role="1dyqJU" value="canBeChildBreakingPoint" />
                                <node concept="2ShNRf" id="2F" role="1dyrYi">
                                  <node concept="1pGfFk" id="2H" role="2ShVmc">
                                    <ref role="37wK5l" to="w1kc:~SNodePointer.&lt;init&gt;(java.lang.String,java.lang.String)" resolve="SNodePointer" />
                                    <node concept="Xl_RD" id="2J" role="37wK5m">
                                      <property role="Xl_RC" value="r:ac11bdd6-c85d-40e0-910f-d45067cda53d(no.uio.mLab.decisions.core.constraints)" />
                                      <node concept="cd27G" id="2M" role="lGtFl">
                                        <node concept="3u3nmq" id="2N" role="cd27D">
                                          <property role="3u3nmv" value="1560130832609177896" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="Xl_RD" id="2K" role="37wK5m">
                                      <property role="Xl_RC" value="1560130832609177897" />
                                      <node concept="cd27G" id="2O" role="lGtFl">
                                        <node concept="3u3nmq" id="2P" role="cd27D">
                                          <property role="3u3nmv" value="1560130832609177896" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="cd27G" id="2L" role="lGtFl">
                                      <node concept="3u3nmq" id="2Q" role="cd27D">
                                        <property role="3u3nmv" value="1560130832609177896" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="cd27G" id="2I" role="lGtFl">
                                    <node concept="3u3nmq" id="2R" role="cd27D">
                                      <property role="3u3nmv" value="1560130832609177896" />
                                    </node>
                                  </node>
                                </node>
                                <node concept="cd27G" id="2G" role="lGtFl">
                                  <node concept="3u3nmq" id="2S" role="cd27D">
                                    <property role="3u3nmv" value="1560130832609177896" />
                                  </node>
                                </node>
                              </node>
                              <node concept="cd27G" id="2E" role="lGtFl">
                                <node concept="3u3nmq" id="2T" role="cd27D">
                                  <property role="3u3nmv" value="1560130832609177896" />
                                </node>
                              </node>
                            </node>
                            <node concept="cd27G" id="2A" role="lGtFl">
                              <node concept="3u3nmq" id="2U" role="cd27D">
                                <property role="3u3nmv" value="1560130832609177896" />
                              </node>
                            </node>
                          </node>
                          <node concept="cd27G" id="2z" role="lGtFl">
                            <node concept="3u3nmq" id="2V" role="cd27D">
                              <property role="3u3nmv" value="1560130832609177896" />
                            </node>
                          </node>
                        </node>
                        <node concept="cd27G" id="2x" role="lGtFl">
                          <node concept="3u3nmq" id="2W" role="cd27D">
                            <property role="3u3nmv" value="1560130832609177896" />
                          </node>
                        </node>
                      </node>
                      <node concept="1Wc70l" id="2u" role="3clFbw">
                        <node concept="3y3z36" id="2X" role="3uHU7w">
                          <node concept="10Nm6u" id="30" role="3uHU7w">
                            <node concept="cd27G" id="33" role="lGtFl">
                              <node concept="3u3nmq" id="34" role="cd27D">
                                <property role="3u3nmv" value="1560130832609177896" />
                              </node>
                            </node>
                          </node>
                          <node concept="37vLTw" id="31" role="3uHU7B">
                            <ref role="3cqZAo" node="1d" resolve="checkingNodeContext" />
                            <node concept="cd27G" id="35" role="lGtFl">
                              <node concept="3u3nmq" id="36" role="cd27D">
                                <property role="3u3nmv" value="1560130832609177896" />
                              </node>
                            </node>
                          </node>
                          <node concept="cd27G" id="32" role="lGtFl">
                            <node concept="3u3nmq" id="37" role="cd27D">
                              <property role="3u3nmv" value="1560130832609177896" />
                            </node>
                          </node>
                        </node>
                        <node concept="3fqX7Q" id="2Y" role="3uHU7B">
                          <node concept="37vLTw" id="38" role="3fr31v">
                            <ref role="3cqZAo" node="1G" resolve="result" />
                            <node concept="cd27G" id="3a" role="lGtFl">
                              <node concept="3u3nmq" id="3b" role="cd27D">
                                <property role="3u3nmv" value="1560130832609177896" />
                              </node>
                            </node>
                          </node>
                          <node concept="cd27G" id="39" role="lGtFl">
                            <node concept="3u3nmq" id="3c" role="cd27D">
                              <property role="3u3nmv" value="1560130832609177896" />
                            </node>
                          </node>
                        </node>
                        <node concept="cd27G" id="2Z" role="lGtFl">
                          <node concept="3u3nmq" id="3d" role="cd27D">
                            <property role="3u3nmv" value="1560130832609177896" />
                          </node>
                        </node>
                      </node>
                      <node concept="cd27G" id="2v" role="lGtFl">
                        <node concept="3u3nmq" id="3e" role="cd27D">
                          <property role="3u3nmv" value="1560130832609177896" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbH" id="1D" role="3cqZAp">
                      <node concept="cd27G" id="3f" role="lGtFl">
                        <node concept="3u3nmq" id="3g" role="cd27D">
                          <property role="3u3nmv" value="1560130832609177896" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbF" id="1E" role="3cqZAp">
                      <node concept="37vLTw" id="3h" role="3clFbG">
                        <ref role="3cqZAo" node="1G" resolve="result" />
                        <node concept="cd27G" id="3j" role="lGtFl">
                          <node concept="3u3nmq" id="3k" role="cd27D">
                            <property role="3u3nmv" value="1560130832609177896" />
                          </node>
                        </node>
                      </node>
                      <node concept="cd27G" id="3i" role="lGtFl">
                        <node concept="3u3nmq" id="3l" role="cd27D">
                          <property role="3u3nmv" value="1560130832609177896" />
                        </node>
                      </node>
                    </node>
                    <node concept="cd27G" id="1F" role="lGtFl">
                      <node concept="3u3nmq" id="3m" role="cd27D">
                        <property role="3u3nmv" value="1560130832609177896" />
                      </node>
                    </node>
                  </node>
                  <node concept="cd27G" id="1f" role="lGtFl">
                    <node concept="3u3nmq" id="3n" role="cd27D">
                      <property role="3u3nmv" value="1560130832609177896" />
                    </node>
                  </node>
                </node>
                <node concept="3uibUv" id="14" role="2Ghqu4">
                  <ref role="3uigEE" to="ze1i:~ConstraintContext_CanBeChild" resolve="ConstraintContext_CanBeChild" />
                  <node concept="cd27G" id="3o" role="lGtFl">
                    <node concept="3u3nmq" id="3p" role="cd27D">
                      <property role="3u3nmv" value="1560130832609177896" />
                    </node>
                  </node>
                </node>
                <node concept="3uibUv" id="15" role="2Ghqu4">
                  <ref role="3uigEE" to="wyt6:~Boolean" resolve="Boolean" />
                  <node concept="cd27G" id="3q" role="lGtFl">
                    <node concept="3u3nmq" id="3r" role="cd27D">
                      <property role="3u3nmv" value="1560130832609177896" />
                    </node>
                  </node>
                </node>
                <node concept="cd27G" id="16" role="lGtFl">
                  <node concept="3u3nmq" id="3s" role="cd27D">
                    <property role="3u3nmv" value="1560130832609177896" />
                  </node>
                </node>
              </node>
              <node concept="cd27G" id="11" role="lGtFl">
                <node concept="3u3nmq" id="3t" role="cd27D">
                  <property role="3u3nmv" value="1560130832609177896" />
                </node>
              </node>
            </node>
            <node concept="cd27G" id="Z" role="lGtFl">
              <node concept="3u3nmq" id="3u" role="cd27D">
                <property role="3u3nmv" value="1560130832609177896" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="X" role="lGtFl">
            <node concept="3u3nmq" id="3v" role="cd27D">
              <property role="3u3nmv" value="1560130832609177896" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="V" role="lGtFl">
          <node concept="3u3nmq" id="3w" role="cd27D">
            <property role="3u3nmv" value="1560130832609177896" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="I" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <node concept="cd27G" id="3x" role="lGtFl">
          <node concept="3u3nmq" id="3y" role="cd27D">
            <property role="3u3nmv" value="1560130832609177896" />
          </node>
        </node>
      </node>
      <node concept="cd27G" id="J" role="lGtFl">
        <node concept="3u3nmq" id="3z" role="cd27D">
          <property role="3u3nmv" value="1560130832609177896" />
        </node>
      </node>
    </node>
    <node concept="2YIFZL" id="6" role="jymVt">
      <property role="TrG5h" value="staticCanBeAChild" />
      <node concept="10P_77" id="3$" role="3clF45">
        <node concept="cd27G" id="3G" role="lGtFl">
          <node concept="3u3nmq" id="3H" role="cd27D">
            <property role="3u3nmv" value="1560130832609177896" />
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="3_" role="1B3o_S">
        <node concept="cd27G" id="3I" role="lGtFl">
          <node concept="3u3nmq" id="3J" role="cd27D">
            <property role="3u3nmv" value="1560130832609177896" />
          </node>
        </node>
      </node>
      <node concept="3clFbS" id="3A" role="3clF47">
        <node concept="Jncv_" id="3K" role="3cqZAp">
          <ref role="JncvD" to="7f9y:1mAGFBJRO8A" resolve="NonBooleanDataValueCondition" />
          <node concept="37vLTw" id="3N" role="JncvB">
            <ref role="3cqZAo" node="3C" resolve="parentNode" />
            <node concept="cd27G" id="3R" role="lGtFl">
              <node concept="3u3nmq" id="3S" role="cd27D">
                <property role="3u3nmv" value="1560130832609178848" />
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="3O" role="Jncv$">
            <node concept="3cpWs6" id="3T" role="3cqZAp">
              <node concept="2OqwBi" id="3V" role="3cqZAk">
                <node concept="2qgKlT" id="3X" role="2OqNvi">
                  <ref role="37wK5l" to="wb6c:1mAGFBKNuSW" resolve="isAllowedToConstrain" />
                  <node concept="2OqwBi" id="40" role="37wK5m">
                    <node concept="2OqwBi" id="42" role="2Oq$k0">
                      <node concept="Jnkvi" id="45" role="2Oq$k0">
                        <ref role="1M0zk5" node="3P" resolve="condition" />
                        <node concept="cd27G" id="48" role="lGtFl">
                          <node concept="3u3nmq" id="49" role="cd27D">
                            <property role="3u3nmv" value="1560130832609548146" />
                          </node>
                        </node>
                      </node>
                      <node concept="3TrEf2" id="46" role="2OqNvi">
                        <ref role="3Tt5mk" to="7f9y:1mAGFBLqAeR" resolve="data" />
                        <node concept="cd27G" id="4a" role="lGtFl">
                          <node concept="3u3nmq" id="4b" role="cd27D">
                            <property role="3u3nmv" value="1560130832619704852" />
                          </node>
                        </node>
                      </node>
                      <node concept="cd27G" id="47" role="lGtFl">
                        <node concept="3u3nmq" id="4c" role="cd27D">
                          <property role="3u3nmv" value="1560130832609549558" />
                        </node>
                      </node>
                    </node>
                    <node concept="2yIwOk" id="43" role="2OqNvi">
                      <node concept="cd27G" id="4d" role="lGtFl">
                        <node concept="3u3nmq" id="4e" role="cd27D">
                          <property role="3u3nmv" value="1560130832609555085" />
                        </node>
                      </node>
                    </node>
                    <node concept="cd27G" id="44" role="lGtFl">
                      <node concept="3u3nmq" id="4f" role="cd27D">
                        <property role="3u3nmv" value="1560130832609553319" />
                      </node>
                    </node>
                  </node>
                  <node concept="cd27G" id="41" role="lGtFl">
                    <node concept="3u3nmq" id="4g" role="cd27D">
                      <property role="3u3nmv" value="1560130832609547392" />
                    </node>
                  </node>
                </node>
                <node concept="2CBFar" id="3Y" role="2Oq$k0">
                  <node concept="chp4Y" id="4h" role="3oSUPX">
                    <ref role="cht4Q" to="7f9y:1mAGFBKqQs5" resolve="Constraint" />
                    <node concept="cd27G" id="4k" role="lGtFl">
                      <node concept="3u3nmq" id="4l" role="cd27D">
                        <property role="3u3nmv" value="5601053190790611421" />
                      </node>
                    </node>
                  </node>
                  <node concept="37vLTw" id="4i" role="1m5AlR">
                    <ref role="3cqZAo" node="3D" resolve="childConcept" />
                    <node concept="cd27G" id="4m" role="lGtFl">
                      <node concept="3u3nmq" id="4n" role="cd27D">
                        <property role="3u3nmv" value="5601053190790603835" />
                      </node>
                    </node>
                  </node>
                  <node concept="cd27G" id="4j" role="lGtFl">
                    <node concept="3u3nmq" id="4o" role="cd27D">
                      <property role="3u3nmv" value="5601053190790605025" />
                    </node>
                  </node>
                </node>
                <node concept="cd27G" id="3Z" role="lGtFl">
                  <node concept="3u3nmq" id="4p" role="cd27D">
                    <property role="3u3nmv" value="1560130832609545827" />
                  </node>
                </node>
              </node>
              <node concept="cd27G" id="3W" role="lGtFl">
                <node concept="3u3nmq" id="4q" role="cd27D">
                  <property role="3u3nmv" value="1560130832609190871" />
                </node>
              </node>
            </node>
            <node concept="cd27G" id="3U" role="lGtFl">
              <node concept="3u3nmq" id="4r" role="cd27D">
                <property role="3u3nmv" value="1560130832609178366" />
              </node>
            </node>
          </node>
          <node concept="JncvC" id="3P" role="JncvA">
            <property role="TrG5h" value="condition" />
            <node concept="2jxLKc" id="4s" role="1tU5fm">
              <node concept="cd27G" id="4u" role="lGtFl">
                <node concept="3u3nmq" id="4v" role="cd27D">
                  <property role="3u3nmv" value="1560130832609178368" />
                </node>
              </node>
            </node>
            <node concept="cd27G" id="4t" role="lGtFl">
              <node concept="3u3nmq" id="4w" role="cd27D">
                <property role="3u3nmv" value="1560130832609178367" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="3Q" role="lGtFl">
            <node concept="3u3nmq" id="4x" role="cd27D">
              <property role="3u3nmv" value="1560130832609178364" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="3L" role="3cqZAp">
          <node concept="3clFbT" id="4y" role="3clFbG">
            <property role="3clFbU" value="false" />
            <node concept="cd27G" id="4$" role="lGtFl">
              <node concept="3u3nmq" id="4_" role="cd27D">
                <property role="3u3nmv" value="1560130832609210932" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="4z" role="lGtFl">
            <node concept="3u3nmq" id="4A" role="cd27D">
              <property role="3u3nmv" value="1560130832609210933" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="3M" role="lGtFl">
          <node concept="3u3nmq" id="4B" role="cd27D">
            <property role="3u3nmv" value="1560130832609177898" />
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="3B" role="3clF46">
        <property role="TrG5h" value="node" />
        <node concept="3uibUv" id="4C" role="1tU5fm">
          <ref role="3uigEE" to="mhbf:~SNode" resolve="SNode" />
          <node concept="cd27G" id="4E" role="lGtFl">
            <node concept="3u3nmq" id="4F" role="cd27D">
              <property role="3u3nmv" value="1560130832609177896" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="4D" role="lGtFl">
          <node concept="3u3nmq" id="4G" role="cd27D">
            <property role="3u3nmv" value="1560130832609177896" />
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="3C" role="3clF46">
        <property role="TrG5h" value="parentNode" />
        <node concept="3uibUv" id="4H" role="1tU5fm">
          <ref role="3uigEE" to="mhbf:~SNode" resolve="SNode" />
          <node concept="cd27G" id="4J" role="lGtFl">
            <node concept="3u3nmq" id="4K" role="cd27D">
              <property role="3u3nmv" value="1560130832609177896" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="4I" role="lGtFl">
          <node concept="3u3nmq" id="4L" role="cd27D">
            <property role="3u3nmv" value="1560130832609177896" />
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="3D" role="3clF46">
        <property role="TrG5h" value="childConcept" />
        <node concept="3uibUv" id="4M" role="1tU5fm">
          <ref role="3uigEE" to="c17a:~SAbstractConcept" resolve="SAbstractConcept" />
          <node concept="cd27G" id="4O" role="lGtFl">
            <node concept="3u3nmq" id="4P" role="cd27D">
              <property role="3u3nmv" value="1560130832609177896" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="4N" role="lGtFl">
          <node concept="3u3nmq" id="4Q" role="cd27D">
            <property role="3u3nmv" value="1560130832609177896" />
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="3E" role="3clF46">
        <property role="TrG5h" value="link" />
        <node concept="3uibUv" id="4R" role="1tU5fm">
          <ref role="3uigEE" to="c17a:~SContainmentLink" resolve="SContainmentLink" />
          <node concept="cd27G" id="4T" role="lGtFl">
            <node concept="3u3nmq" id="4U" role="cd27D">
              <property role="3u3nmv" value="1560130832609177896" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="4S" role="lGtFl">
          <node concept="3u3nmq" id="4V" role="cd27D">
            <property role="3u3nmv" value="1560130832609177896" />
          </node>
        </node>
      </node>
      <node concept="cd27G" id="3F" role="lGtFl">
        <node concept="3u3nmq" id="4W" role="cd27D">
          <property role="3u3nmv" value="1560130832609177896" />
        </node>
      </node>
    </node>
    <node concept="cd27G" id="7" role="lGtFl">
      <node concept="3u3nmq" id="4X" role="cd27D">
        <property role="3u3nmv" value="1560130832609177896" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="4Y">
    <property role="TrG5h" value="ConstraintsAspectDescriptor" />
    <property role="3GE5qa" value="Constraints" />
    <node concept="3uibUv" id="4Z" role="1zkMxy">
      <ref role="3uigEE" to="ze1i:~BaseConstraintsAspectDescriptor" resolve="BaseConstraintsAspectDescriptor" />
    </node>
    <node concept="3Tm1VV" id="50" role="1B3o_S" />
    <node concept="3clFbW" id="51" role="jymVt">
      <node concept="3cqZAl" id="54" role="3clF45" />
      <node concept="3Tm1VV" id="55" role="1B3o_S" />
      <node concept="3clFbS" id="56" role="3clF47" />
    </node>
    <node concept="2tJIrI" id="52" role="jymVt" />
    <node concept="3clFb_" id="53" role="jymVt">
      <property role="IEkAT" value="false" />
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="getConstraints" />
      <property role="DiZV1" value="false" />
      <node concept="2AHcQZ" id="57" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
      <node concept="3Tm1VV" id="58" role="1B3o_S" />
      <node concept="3uibUv" id="59" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConstraintsDescriptor" resolve="ConstraintsDescriptor" />
      </node>
      <node concept="37vLTG" id="5a" role="3clF46">
        <property role="TrG5h" value="concept" />
        <node concept="3bZ5Sz" id="5c" role="1tU5fm" />
      </node>
      <node concept="3clFbS" id="5b" role="3clF47">
        <node concept="1_3QMa" id="5d" role="3cqZAp">
          <node concept="37vLTw" id="5f" role="1_3QMn">
            <ref role="3cqZAo" node="5a" resolve="concept" />
          </node>
          <node concept="1pnPoh" id="5g" role="1_3QMm">
            <node concept="3clFbS" id="5k" role="1pnPq1">
              <node concept="3cpWs6" id="5m" role="3cqZAp">
                <node concept="1nCR9W" id="5n" role="3cqZAk">
                  <property role="1nD$Q0" value="no.uio.mLab.decisions.core.constraints.NamedConditionReference_Constraints" />
                  <node concept="3uibUv" id="5o" role="2lIhxL">
                    <ref role="3uigEE" to="ze1i:~ConstraintsDescriptor" resolve="ConstraintsDescriptor" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3gn64h" id="5l" role="1pnPq6">
              <ref role="3gnhBz" to="7f9y:1mAGFBJL9Gh" resolve="NamedConditionReference" />
            </node>
          </node>
          <node concept="1pnPoh" id="5h" role="1_3QMm">
            <node concept="3clFbS" id="5p" role="1pnPq1">
              <node concept="3cpWs6" id="5r" role="3cqZAp">
                <node concept="1nCR9W" id="5s" role="3cqZAk">
                  <property role="1nD$Q0" value="no.uio.mLab.decisions.core.constraints.Constraint_Constraints" />
                  <node concept="3uibUv" id="5t" role="2lIhxL">
                    <ref role="3uigEE" to="ze1i:~ConstraintsDescriptor" resolve="ConstraintsDescriptor" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3gn64h" id="5q" role="1pnPq6">
              <ref role="3gnhBz" to="7f9y:1mAGFBKqQs5" resolve="Constraint" />
            </node>
          </node>
          <node concept="1pnPoh" id="5i" role="1_3QMm">
            <node concept="3clFbS" id="5u" role="1pnPq1">
              <node concept="3cpWs6" id="5w" role="3cqZAp">
                <node concept="1nCR9W" id="5x" role="3cqZAk">
                  <property role="1nD$Q0" value="no.uio.mLab.decisions.core.constraints.ISelectedEntityPattern_Constraints" />
                  <node concept="3uibUv" id="5y" role="2lIhxL">
                    <ref role="3uigEE" to="ze1i:~ConstraintsDescriptor" resolve="ConstraintsDescriptor" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3gn64h" id="5v" role="1pnPq6">
              <ref role="3gnhBz" to="7f9y:65epL7Mprkt" resolve="ISelectedEntityPattern" />
            </node>
          </node>
          <node concept="3clFbS" id="5j" role="1prKM_" />
        </node>
        <node concept="3cpWs6" id="5e" role="3cqZAp">
          <node concept="2ShNRf" id="5z" role="3cqZAk">
            <node concept="1pGfFk" id="5$" role="2ShVmc">
              <ref role="37wK5l" to="79pl:~BaseConstraintsDescriptor.&lt;init&gt;(org.jetbrains.mps.openapi.language.SAbstractConcept)" resolve="BaseConstraintsDescriptor" />
              <node concept="37vLTw" id="5_" role="37wK5m">
                <ref role="3cqZAo" node="5a" resolve="concept" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="39dXUE" id="5A" />
  <node concept="312cEu" id="5B">
    <property role="3GE5qa" value="aspect.patterns.aspect.stereotypes" />
    <property role="TrG5h" value="ISelectedEntityPattern_Constraints" />
    <node concept="3Tm1VV" id="5C" role="1B3o_S">
      <node concept="cd27G" id="5J" role="lGtFl">
        <node concept="3u3nmq" id="5K" role="cd27D">
          <property role="3u3nmv" value="7011654996645186516" />
        </node>
      </node>
    </node>
    <node concept="3uibUv" id="5D" role="1zkMxy">
      <ref role="3uigEE" to="79pl:~BaseConstraintsDescriptor" resolve="BaseConstraintsDescriptor" />
      <node concept="cd27G" id="5L" role="lGtFl">
        <node concept="3u3nmq" id="5M" role="cd27D">
          <property role="3u3nmv" value="7011654996645186516" />
        </node>
      </node>
    </node>
    <node concept="3clFbW" id="5E" role="jymVt">
      <node concept="3cqZAl" id="5N" role="3clF45">
        <node concept="cd27G" id="5R" role="lGtFl">
          <node concept="3u3nmq" id="5S" role="cd27D">
            <property role="3u3nmv" value="7011654996645186516" />
          </node>
        </node>
      </node>
      <node concept="3clFbS" id="5O" role="3clF47">
        <node concept="XkiVB" id="5T" role="3cqZAp">
          <ref role="37wK5l" to="79pl:~BaseConstraintsDescriptor.&lt;init&gt;(org.jetbrains.mps.openapi.language.SAbstractConcept)" resolve="BaseConstraintsDescriptor" />
          <node concept="2YIFZM" id="5V" role="37wK5m">
            <ref role="1Pybhc" to="2k9e:~MetaAdapterFactory" resolve="MetaAdapterFactory" />
            <ref role="37wK5l" to="2k9e:~MetaAdapterFactory.getInterfaceConcept(long,long,long,java.lang.String):org.jetbrains.mps.openapi.language.SInterfaceConcept" resolve="getInterfaceConcept" />
            <node concept="1adDum" id="5X" role="37wK5m">
              <property role="1adDun" value="0xc610a4ebc47c4f95L" />
              <node concept="cd27G" id="62" role="lGtFl">
                <node concept="3u3nmq" id="63" role="cd27D">
                  <property role="3u3nmv" value="7011654996645186516" />
                </node>
              </node>
            </node>
            <node concept="1adDum" id="5Y" role="37wK5m">
              <property role="1adDun" value="0xb56811236971769cL" />
              <node concept="cd27G" id="64" role="lGtFl">
                <node concept="3u3nmq" id="65" role="cd27D">
                  <property role="3u3nmv" value="7011654996645186516" />
                </node>
              </node>
            </node>
            <node concept="1adDum" id="5Z" role="37wK5m">
              <property role="1adDun" value="0x614e6711f265b51dL" />
              <node concept="cd27G" id="66" role="lGtFl">
                <node concept="3u3nmq" id="67" role="cd27D">
                  <property role="3u3nmv" value="7011654996645186516" />
                </node>
              </node>
            </node>
            <node concept="Xl_RD" id="60" role="37wK5m">
              <property role="Xl_RC" value="no.uio.mLab.decisions.core.structure.ISelectedEntityPattern" />
              <node concept="cd27G" id="68" role="lGtFl">
                <node concept="3u3nmq" id="69" role="cd27D">
                  <property role="3u3nmv" value="7011654996645186516" />
                </node>
              </node>
            </node>
            <node concept="cd27G" id="61" role="lGtFl">
              <node concept="3u3nmq" id="6a" role="cd27D">
                <property role="3u3nmv" value="7011654996645186516" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="5W" role="lGtFl">
            <node concept="3u3nmq" id="6b" role="cd27D">
              <property role="3u3nmv" value="7011654996645186516" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="5U" role="lGtFl">
          <node concept="3u3nmq" id="6c" role="cd27D">
            <property role="3u3nmv" value="7011654996645186516" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="5P" role="1B3o_S">
        <node concept="cd27G" id="6d" role="lGtFl">
          <node concept="3u3nmq" id="6e" role="cd27D">
            <property role="3u3nmv" value="7011654996645186516" />
          </node>
        </node>
      </node>
      <node concept="cd27G" id="5Q" role="lGtFl">
        <node concept="3u3nmq" id="6f" role="cd27D">
          <property role="3u3nmv" value="7011654996645186516" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="5F" role="jymVt">
      <node concept="cd27G" id="6g" role="lGtFl">
        <node concept="3u3nmq" id="6h" role="cd27D">
          <property role="3u3nmv" value="7011654996645186516" />
        </node>
      </node>
    </node>
    <node concept="3clFb_" id="5G" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="calculateCanBeParentConstraint" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3Tmbuc" id="6i" role="1B3o_S">
        <node concept="cd27G" id="6n" role="lGtFl">
          <node concept="3u3nmq" id="6o" role="cd27D">
            <property role="3u3nmv" value="7011654996645186516" />
          </node>
        </node>
      </node>
      <node concept="3uibUv" id="6j" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConstraintFunction" resolve="ConstraintFunction" />
        <node concept="3uibUv" id="6p" role="11_B2D">
          <ref role="3uigEE" to="ze1i:~ConstraintContext_CanBeParent" resolve="ConstraintContext_CanBeParent" />
          <node concept="cd27G" id="6s" role="lGtFl">
            <node concept="3u3nmq" id="6t" role="cd27D">
              <property role="3u3nmv" value="7011654996645186516" />
            </node>
          </node>
        </node>
        <node concept="3uibUv" id="6q" role="11_B2D">
          <ref role="3uigEE" to="wyt6:~Boolean" resolve="Boolean" />
          <node concept="cd27G" id="6u" role="lGtFl">
            <node concept="3u3nmq" id="6v" role="cd27D">
              <property role="3u3nmv" value="7011654996645186516" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="6r" role="lGtFl">
          <node concept="3u3nmq" id="6w" role="cd27D">
            <property role="3u3nmv" value="7011654996645186516" />
          </node>
        </node>
      </node>
      <node concept="3clFbS" id="6k" role="3clF47">
        <node concept="3clFbF" id="6x" role="3cqZAp">
          <node concept="2ShNRf" id="6z" role="3clFbG">
            <node concept="YeOm9" id="6_" role="2ShVmc">
              <node concept="1Y3b0j" id="6B" role="YeSDq">
                <property role="2bfB8j" value="true" />
                <ref role="1Y3XeK" to="ze1i:~ConstraintFunction" resolve="ConstraintFunction" />
                <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
                <node concept="3Tm1VV" id="6D" role="1B3o_S">
                  <node concept="cd27G" id="6I" role="lGtFl">
                    <node concept="3u3nmq" id="6J" role="cd27D">
                      <property role="3u3nmv" value="7011654996645186516" />
                    </node>
                  </node>
                </node>
                <node concept="3clFb_" id="6E" role="jymVt">
                  <property role="1EzhhJ" value="false" />
                  <property role="TrG5h" value="invoke" />
                  <property role="DiZV1" value="false" />
                  <property role="od$2w" value="false" />
                  <node concept="3Tm1VV" id="6K" role="1B3o_S">
                    <node concept="cd27G" id="6R" role="lGtFl">
                      <node concept="3u3nmq" id="6S" role="cd27D">
                        <property role="3u3nmv" value="7011654996645186516" />
                      </node>
                    </node>
                  </node>
                  <node concept="2AHcQZ" id="6L" role="2AJF6D">
                    <ref role="2AI5Lk" to="mhfm:~NotNull" resolve="NotNull" />
                    <node concept="cd27G" id="6T" role="lGtFl">
                      <node concept="3u3nmq" id="6U" role="cd27D">
                        <property role="3u3nmv" value="7011654996645186516" />
                      </node>
                    </node>
                  </node>
                  <node concept="3uibUv" id="6M" role="3clF45">
                    <ref role="3uigEE" to="wyt6:~Boolean" resolve="Boolean" />
                    <node concept="cd27G" id="6V" role="lGtFl">
                      <node concept="3u3nmq" id="6W" role="cd27D">
                        <property role="3u3nmv" value="7011654996645186516" />
                      </node>
                    </node>
                  </node>
                  <node concept="37vLTG" id="6N" role="3clF46">
                    <property role="TrG5h" value="context" />
                    <node concept="3uibUv" id="6X" role="1tU5fm">
                      <ref role="3uigEE" to="ze1i:~ConstraintContext_CanBeParent" resolve="ConstraintContext_CanBeParent" />
                      <node concept="cd27G" id="70" role="lGtFl">
                        <node concept="3u3nmq" id="71" role="cd27D">
                          <property role="3u3nmv" value="7011654996645186516" />
                        </node>
                      </node>
                    </node>
                    <node concept="2AHcQZ" id="6Y" role="2AJF6D">
                      <ref role="2AI5Lk" to="mhfm:~NotNull" resolve="NotNull" />
                      <node concept="cd27G" id="72" role="lGtFl">
                        <node concept="3u3nmq" id="73" role="cd27D">
                          <property role="3u3nmv" value="7011654996645186516" />
                        </node>
                      </node>
                    </node>
                    <node concept="cd27G" id="6Z" role="lGtFl">
                      <node concept="3u3nmq" id="74" role="cd27D">
                        <property role="3u3nmv" value="7011654996645186516" />
                      </node>
                    </node>
                  </node>
                  <node concept="37vLTG" id="6O" role="3clF46">
                    <property role="TrG5h" value="checkingNodeContext" />
                    <node concept="3uibUv" id="75" role="1tU5fm">
                      <ref role="3uigEE" to="ze1i:~CheckingNodeContext" resolve="CheckingNodeContext" />
                      <node concept="cd27G" id="78" role="lGtFl">
                        <node concept="3u3nmq" id="79" role="cd27D">
                          <property role="3u3nmv" value="7011654996645186516" />
                        </node>
                      </node>
                    </node>
                    <node concept="2AHcQZ" id="76" role="2AJF6D">
                      <ref role="2AI5Lk" to="mhfm:~Nullable" resolve="Nullable" />
                      <node concept="cd27G" id="7a" role="lGtFl">
                        <node concept="3u3nmq" id="7b" role="cd27D">
                          <property role="3u3nmv" value="7011654996645186516" />
                        </node>
                      </node>
                    </node>
                    <node concept="cd27G" id="77" role="lGtFl">
                      <node concept="3u3nmq" id="7c" role="cd27D">
                        <property role="3u3nmv" value="7011654996645186516" />
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbS" id="6P" role="3clF47">
                    <node concept="3cpWs8" id="7d" role="3cqZAp">
                      <node concept="3cpWsn" id="7j" role="3cpWs9">
                        <property role="TrG5h" value="result" />
                        <node concept="10P_77" id="7l" role="1tU5fm">
                          <node concept="cd27G" id="7o" role="lGtFl">
                            <node concept="3u3nmq" id="7p" role="cd27D">
                              <property role="3u3nmv" value="7011654996645186516" />
                            </node>
                          </node>
                        </node>
                        <node concept="1rXfSq" id="7m" role="33vP2m">
                          <ref role="37wK5l" node="5H" resolve="staticCanBeAParent" />
                          <node concept="2OqwBi" id="7q" role="37wK5m">
                            <node concept="37vLTw" id="7v" role="2Oq$k0">
                              <ref role="3cqZAo" node="6N" resolve="context" />
                              <node concept="cd27G" id="7y" role="lGtFl">
                                <node concept="3u3nmq" id="7z" role="cd27D">
                                  <property role="3u3nmv" value="7011654996645186516" />
                                </node>
                              </node>
                            </node>
                            <node concept="liA8E" id="7w" role="2OqNvi">
                              <ref role="37wK5l" to="ze1i:~ConstraintContext_CanBeParent.getNode():org.jetbrains.mps.openapi.model.SNode" resolve="getNode" />
                              <node concept="cd27G" id="7$" role="lGtFl">
                                <node concept="3u3nmq" id="7_" role="cd27D">
                                  <property role="3u3nmv" value="7011654996645186516" />
                                </node>
                              </node>
                            </node>
                            <node concept="cd27G" id="7x" role="lGtFl">
                              <node concept="3u3nmq" id="7A" role="cd27D">
                                <property role="3u3nmv" value="7011654996645186516" />
                              </node>
                            </node>
                          </node>
                          <node concept="2OqwBi" id="7r" role="37wK5m">
                            <node concept="37vLTw" id="7B" role="2Oq$k0">
                              <ref role="3cqZAo" node="6N" resolve="context" />
                              <node concept="cd27G" id="7E" role="lGtFl">
                                <node concept="3u3nmq" id="7F" role="cd27D">
                                  <property role="3u3nmv" value="7011654996645186516" />
                                </node>
                              </node>
                            </node>
                            <node concept="liA8E" id="7C" role="2OqNvi">
                              <ref role="37wK5l" to="ze1i:~ConstraintContext_CanBeParent.getChildNode():org.jetbrains.mps.openapi.model.SNode" resolve="getChildNode" />
                              <node concept="cd27G" id="7G" role="lGtFl">
                                <node concept="3u3nmq" id="7H" role="cd27D">
                                  <property role="3u3nmv" value="7011654996645186516" />
                                </node>
                              </node>
                            </node>
                            <node concept="cd27G" id="7D" role="lGtFl">
                              <node concept="3u3nmq" id="7I" role="cd27D">
                                <property role="3u3nmv" value="7011654996645186516" />
                              </node>
                            </node>
                          </node>
                          <node concept="2OqwBi" id="7s" role="37wK5m">
                            <node concept="37vLTw" id="7J" role="2Oq$k0">
                              <ref role="3cqZAo" node="6N" resolve="context" />
                              <node concept="cd27G" id="7M" role="lGtFl">
                                <node concept="3u3nmq" id="7N" role="cd27D">
                                  <property role="3u3nmv" value="7011654996645186516" />
                                </node>
                              </node>
                            </node>
                            <node concept="liA8E" id="7K" role="2OqNvi">
                              <ref role="37wK5l" to="ze1i:~ConstraintContext_CanBeParent.getChildConcept():org.jetbrains.mps.openapi.language.SAbstractConcept" resolve="getChildConcept" />
                              <node concept="cd27G" id="7O" role="lGtFl">
                                <node concept="3u3nmq" id="7P" role="cd27D">
                                  <property role="3u3nmv" value="7011654996645186516" />
                                </node>
                              </node>
                            </node>
                            <node concept="cd27G" id="7L" role="lGtFl">
                              <node concept="3u3nmq" id="7Q" role="cd27D">
                                <property role="3u3nmv" value="7011654996645186516" />
                              </node>
                            </node>
                          </node>
                          <node concept="2OqwBi" id="7t" role="37wK5m">
                            <node concept="37vLTw" id="7R" role="2Oq$k0">
                              <ref role="3cqZAo" node="6N" resolve="context" />
                              <node concept="cd27G" id="7U" role="lGtFl">
                                <node concept="3u3nmq" id="7V" role="cd27D">
                                  <property role="3u3nmv" value="7011654996645186516" />
                                </node>
                              </node>
                            </node>
                            <node concept="liA8E" id="7S" role="2OqNvi">
                              <ref role="37wK5l" to="ze1i:~ConstraintContext_CanBeParent.getLink():org.jetbrains.mps.openapi.language.SContainmentLink" resolve="getLink" />
                              <node concept="cd27G" id="7W" role="lGtFl">
                                <node concept="3u3nmq" id="7X" role="cd27D">
                                  <property role="3u3nmv" value="7011654996645186516" />
                                </node>
                              </node>
                            </node>
                            <node concept="cd27G" id="7T" role="lGtFl">
                              <node concept="3u3nmq" id="7Y" role="cd27D">
                                <property role="3u3nmv" value="7011654996645186516" />
                              </node>
                            </node>
                          </node>
                          <node concept="cd27G" id="7u" role="lGtFl">
                            <node concept="3u3nmq" id="7Z" role="cd27D">
                              <property role="3u3nmv" value="7011654996645186516" />
                            </node>
                          </node>
                        </node>
                        <node concept="cd27G" id="7n" role="lGtFl">
                          <node concept="3u3nmq" id="80" role="cd27D">
                            <property role="3u3nmv" value="7011654996645186516" />
                          </node>
                        </node>
                      </node>
                      <node concept="cd27G" id="7k" role="lGtFl">
                        <node concept="3u3nmq" id="81" role="cd27D">
                          <property role="3u3nmv" value="7011654996645186516" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbH" id="7e" role="3cqZAp">
                      <node concept="cd27G" id="82" role="lGtFl">
                        <node concept="3u3nmq" id="83" role="cd27D">
                          <property role="3u3nmv" value="7011654996645186516" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbJ" id="7f" role="3cqZAp">
                      <node concept="3clFbS" id="84" role="3clFbx">
                        <node concept="3clFbF" id="87" role="3cqZAp">
                          <node concept="2OqwBi" id="89" role="3clFbG">
                            <node concept="37vLTw" id="8b" role="2Oq$k0">
                              <ref role="3cqZAo" node="6O" resolve="checkingNodeContext" />
                              <node concept="cd27G" id="8e" role="lGtFl">
                                <node concept="3u3nmq" id="8f" role="cd27D">
                                  <property role="3u3nmv" value="7011654996645186516" />
                                </node>
                              </node>
                            </node>
                            <node concept="liA8E" id="8c" role="2OqNvi">
                              <ref role="37wK5l" to="ze1i:~CheckingNodeContext.setBreakingNode(org.jetbrains.mps.openapi.model.SNodeReference):void" resolve="setBreakingNode" />
                              <node concept="1dyn4i" id="8g" role="37wK5m">
                                <property role="1dyqJU" value="canBeParentBreakingPoint" />
                                <node concept="2ShNRf" id="8i" role="1dyrYi">
                                  <node concept="1pGfFk" id="8k" role="2ShVmc">
                                    <ref role="37wK5l" to="w1kc:~SNodePointer.&lt;init&gt;(java.lang.String,java.lang.String)" resolve="SNodePointer" />
                                    <node concept="Xl_RD" id="8m" role="37wK5m">
                                      <property role="Xl_RC" value="r:ac11bdd6-c85d-40e0-910f-d45067cda53d(no.uio.mLab.decisions.core.constraints)" />
                                      <node concept="cd27G" id="8p" role="lGtFl">
                                        <node concept="3u3nmq" id="8q" role="cd27D">
                                          <property role="3u3nmv" value="7011654996645186516" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="Xl_RD" id="8n" role="37wK5m">
                                      <property role="Xl_RC" value="7011654996645186520" />
                                      <node concept="cd27G" id="8r" role="lGtFl">
                                        <node concept="3u3nmq" id="8s" role="cd27D">
                                          <property role="3u3nmv" value="7011654996645186516" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="cd27G" id="8o" role="lGtFl">
                                      <node concept="3u3nmq" id="8t" role="cd27D">
                                        <property role="3u3nmv" value="7011654996645186516" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="cd27G" id="8l" role="lGtFl">
                                    <node concept="3u3nmq" id="8u" role="cd27D">
                                      <property role="3u3nmv" value="7011654996645186516" />
                                    </node>
                                  </node>
                                </node>
                                <node concept="cd27G" id="8j" role="lGtFl">
                                  <node concept="3u3nmq" id="8v" role="cd27D">
                                    <property role="3u3nmv" value="7011654996645186516" />
                                  </node>
                                </node>
                              </node>
                              <node concept="cd27G" id="8h" role="lGtFl">
                                <node concept="3u3nmq" id="8w" role="cd27D">
                                  <property role="3u3nmv" value="7011654996645186516" />
                                </node>
                              </node>
                            </node>
                            <node concept="cd27G" id="8d" role="lGtFl">
                              <node concept="3u3nmq" id="8x" role="cd27D">
                                <property role="3u3nmv" value="7011654996645186516" />
                              </node>
                            </node>
                          </node>
                          <node concept="cd27G" id="8a" role="lGtFl">
                            <node concept="3u3nmq" id="8y" role="cd27D">
                              <property role="3u3nmv" value="7011654996645186516" />
                            </node>
                          </node>
                        </node>
                        <node concept="cd27G" id="88" role="lGtFl">
                          <node concept="3u3nmq" id="8z" role="cd27D">
                            <property role="3u3nmv" value="7011654996645186516" />
                          </node>
                        </node>
                      </node>
                      <node concept="1Wc70l" id="85" role="3clFbw">
                        <node concept="3y3z36" id="8$" role="3uHU7w">
                          <node concept="10Nm6u" id="8B" role="3uHU7w">
                            <node concept="cd27G" id="8E" role="lGtFl">
                              <node concept="3u3nmq" id="8F" role="cd27D">
                                <property role="3u3nmv" value="7011654996645186516" />
                              </node>
                            </node>
                          </node>
                          <node concept="37vLTw" id="8C" role="3uHU7B">
                            <ref role="3cqZAo" node="6O" resolve="checkingNodeContext" />
                            <node concept="cd27G" id="8G" role="lGtFl">
                              <node concept="3u3nmq" id="8H" role="cd27D">
                                <property role="3u3nmv" value="7011654996645186516" />
                              </node>
                            </node>
                          </node>
                          <node concept="cd27G" id="8D" role="lGtFl">
                            <node concept="3u3nmq" id="8I" role="cd27D">
                              <property role="3u3nmv" value="7011654996645186516" />
                            </node>
                          </node>
                        </node>
                        <node concept="3fqX7Q" id="8_" role="3uHU7B">
                          <node concept="37vLTw" id="8J" role="3fr31v">
                            <ref role="3cqZAo" node="7j" resolve="result" />
                            <node concept="cd27G" id="8L" role="lGtFl">
                              <node concept="3u3nmq" id="8M" role="cd27D">
                                <property role="3u3nmv" value="7011654996645186516" />
                              </node>
                            </node>
                          </node>
                          <node concept="cd27G" id="8K" role="lGtFl">
                            <node concept="3u3nmq" id="8N" role="cd27D">
                              <property role="3u3nmv" value="7011654996645186516" />
                            </node>
                          </node>
                        </node>
                        <node concept="cd27G" id="8A" role="lGtFl">
                          <node concept="3u3nmq" id="8O" role="cd27D">
                            <property role="3u3nmv" value="7011654996645186516" />
                          </node>
                        </node>
                      </node>
                      <node concept="cd27G" id="86" role="lGtFl">
                        <node concept="3u3nmq" id="8P" role="cd27D">
                          <property role="3u3nmv" value="7011654996645186516" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbH" id="7g" role="3cqZAp">
                      <node concept="cd27G" id="8Q" role="lGtFl">
                        <node concept="3u3nmq" id="8R" role="cd27D">
                          <property role="3u3nmv" value="7011654996645186516" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbF" id="7h" role="3cqZAp">
                      <node concept="37vLTw" id="8S" role="3clFbG">
                        <ref role="3cqZAo" node="7j" resolve="result" />
                        <node concept="cd27G" id="8U" role="lGtFl">
                          <node concept="3u3nmq" id="8V" role="cd27D">
                            <property role="3u3nmv" value="7011654996645186516" />
                          </node>
                        </node>
                      </node>
                      <node concept="cd27G" id="8T" role="lGtFl">
                        <node concept="3u3nmq" id="8W" role="cd27D">
                          <property role="3u3nmv" value="7011654996645186516" />
                        </node>
                      </node>
                    </node>
                    <node concept="cd27G" id="7i" role="lGtFl">
                      <node concept="3u3nmq" id="8X" role="cd27D">
                        <property role="3u3nmv" value="7011654996645186516" />
                      </node>
                    </node>
                  </node>
                  <node concept="cd27G" id="6Q" role="lGtFl">
                    <node concept="3u3nmq" id="8Y" role="cd27D">
                      <property role="3u3nmv" value="7011654996645186516" />
                    </node>
                  </node>
                </node>
                <node concept="3uibUv" id="6F" role="2Ghqu4">
                  <ref role="3uigEE" to="ze1i:~ConstraintContext_CanBeParent" resolve="ConstraintContext_CanBeParent" />
                  <node concept="cd27G" id="8Z" role="lGtFl">
                    <node concept="3u3nmq" id="90" role="cd27D">
                      <property role="3u3nmv" value="7011654996645186516" />
                    </node>
                  </node>
                </node>
                <node concept="3uibUv" id="6G" role="2Ghqu4">
                  <ref role="3uigEE" to="wyt6:~Boolean" resolve="Boolean" />
                  <node concept="cd27G" id="91" role="lGtFl">
                    <node concept="3u3nmq" id="92" role="cd27D">
                      <property role="3u3nmv" value="7011654996645186516" />
                    </node>
                  </node>
                </node>
                <node concept="cd27G" id="6H" role="lGtFl">
                  <node concept="3u3nmq" id="93" role="cd27D">
                    <property role="3u3nmv" value="7011654996645186516" />
                  </node>
                </node>
              </node>
              <node concept="cd27G" id="6C" role="lGtFl">
                <node concept="3u3nmq" id="94" role="cd27D">
                  <property role="3u3nmv" value="7011654996645186516" />
                </node>
              </node>
            </node>
            <node concept="cd27G" id="6A" role="lGtFl">
              <node concept="3u3nmq" id="95" role="cd27D">
                <property role="3u3nmv" value="7011654996645186516" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="6$" role="lGtFl">
            <node concept="3u3nmq" id="96" role="cd27D">
              <property role="3u3nmv" value="7011654996645186516" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="6y" role="lGtFl">
          <node concept="3u3nmq" id="97" role="cd27D">
            <property role="3u3nmv" value="7011654996645186516" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6l" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <node concept="cd27G" id="98" role="lGtFl">
          <node concept="3u3nmq" id="99" role="cd27D">
            <property role="3u3nmv" value="7011654996645186516" />
          </node>
        </node>
      </node>
      <node concept="cd27G" id="6m" role="lGtFl">
        <node concept="3u3nmq" id="9a" role="cd27D">
          <property role="3u3nmv" value="7011654996645186516" />
        </node>
      </node>
    </node>
    <node concept="2YIFZL" id="5H" role="jymVt">
      <property role="TrG5h" value="staticCanBeAParent" />
      <node concept="10P_77" id="9b" role="3clF45">
        <node concept="cd27G" id="9j" role="lGtFl">
          <node concept="3u3nmq" id="9k" role="cd27D">
            <property role="3u3nmv" value="7011654996645186516" />
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="9c" role="1B3o_S">
        <node concept="cd27G" id="9l" role="lGtFl">
          <node concept="3u3nmq" id="9m" role="cd27D">
            <property role="3u3nmv" value="7011654996645186516" />
          </node>
        </node>
      </node>
      <node concept="3clFbS" id="9d" role="3clF47">
        <node concept="2xdQw9" id="9n" role="3cqZAp">
          <property role="2xdLsb" value="debug" />
          <node concept="3cpWs3" id="9q" role="9lYJi">
            <node concept="Xl_RD" id="9s" role="3uHU7B">
              <node concept="cd27G" id="9v" role="lGtFl">
                <node concept="3u3nmq" id="9w" role="cd27D">
                  <property role="3u3nmv" value="7011654996646876961" />
                </node>
              </node>
            </node>
            <node concept="37vLTw" id="9t" role="3uHU7w">
              <ref role="3cqZAo" node="9f" resolve="childNode" />
              <node concept="cd27G" id="9x" role="lGtFl">
                <node concept="3u3nmq" id="9y" role="cd27D">
                  <property role="3u3nmv" value="7011654996646886140" />
                </node>
              </node>
            </node>
            <node concept="cd27G" id="9u" role="lGtFl">
              <node concept="3u3nmq" id="9z" role="cd27D">
                <property role="3u3nmv" value="7011654996646878696" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="9r" role="lGtFl">
            <node concept="3u3nmq" id="9$" role="cd27D">
              <property role="3u3nmv" value="7011654996646876959" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="9o" role="3cqZAp">
          <node concept="2OqwBi" id="9_" role="3clFbG">
            <node concept="37vLTw" id="9B" role="2Oq$k0">
              <ref role="3cqZAo" node="9g" resolve="childConcept" />
              <node concept="cd27G" id="9E" role="lGtFl">
                <node concept="3u3nmq" id="9F" role="cd27D">
                  <property role="3u3nmv" value="7011654996645186987" />
                </node>
              </node>
            </node>
            <node concept="2Zo12i" id="9C" role="2OqNvi">
              <node concept="chp4Y" id="9G" role="2Zo12j">
                <ref role="cht4Q" to="7f9y:6LTgXmMvpq9" resolve="IEntityReference" />
                <node concept="cd27G" id="9I" role="lGtFl">
                  <node concept="3u3nmq" id="9J" role="cd27D">
                    <property role="3u3nmv" value="7011654996645189975" />
                  </node>
                </node>
              </node>
              <node concept="cd27G" id="9H" role="lGtFl">
                <node concept="3u3nmq" id="9K" role="cd27D">
                  <property role="3u3nmv" value="7011654996645189131" />
                </node>
              </node>
            </node>
            <node concept="cd27G" id="9D" role="lGtFl">
              <node concept="3u3nmq" id="9L" role="cd27D">
                <property role="3u3nmv" value="7011654996645188127" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="9A" role="lGtFl">
            <node concept="3u3nmq" id="9M" role="cd27D">
              <property role="3u3nmv" value="7011654996645186988" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="9p" role="lGtFl">
          <node concept="3u3nmq" id="9N" role="cd27D">
            <property role="3u3nmv" value="7011654996645186521" />
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="9e" role="3clF46">
        <property role="TrG5h" value="node" />
        <node concept="3uibUv" id="9O" role="1tU5fm">
          <ref role="3uigEE" to="mhbf:~SNode" resolve="SNode" />
          <node concept="cd27G" id="9Q" role="lGtFl">
            <node concept="3u3nmq" id="9R" role="cd27D">
              <property role="3u3nmv" value="7011654996645186516" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="9P" role="lGtFl">
          <node concept="3u3nmq" id="9S" role="cd27D">
            <property role="3u3nmv" value="7011654996645186516" />
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="9f" role="3clF46">
        <property role="TrG5h" value="childNode" />
        <node concept="3uibUv" id="9T" role="1tU5fm">
          <ref role="3uigEE" to="mhbf:~SNode" resolve="SNode" />
          <node concept="cd27G" id="9V" role="lGtFl">
            <node concept="3u3nmq" id="9W" role="cd27D">
              <property role="3u3nmv" value="7011654996645186516" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="9U" role="lGtFl">
          <node concept="3u3nmq" id="9X" role="cd27D">
            <property role="3u3nmv" value="7011654996645186516" />
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="9g" role="3clF46">
        <property role="TrG5h" value="childConcept" />
        <node concept="3uibUv" id="9Y" role="1tU5fm">
          <ref role="3uigEE" to="c17a:~SAbstractConcept" resolve="SAbstractConcept" />
          <node concept="cd27G" id="a0" role="lGtFl">
            <node concept="3u3nmq" id="a1" role="cd27D">
              <property role="3u3nmv" value="7011654996645186516" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="9Z" role="lGtFl">
          <node concept="3u3nmq" id="a2" role="cd27D">
            <property role="3u3nmv" value="7011654996645186516" />
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="9h" role="3clF46">
        <property role="TrG5h" value="link" />
        <node concept="3uibUv" id="a3" role="1tU5fm">
          <ref role="3uigEE" to="c17a:~SContainmentLink" resolve="SContainmentLink" />
          <node concept="cd27G" id="a5" role="lGtFl">
            <node concept="3u3nmq" id="a6" role="cd27D">
              <property role="3u3nmv" value="7011654996645186516" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="a4" role="lGtFl">
          <node concept="3u3nmq" id="a7" role="cd27D">
            <property role="3u3nmv" value="7011654996645186516" />
          </node>
        </node>
      </node>
      <node concept="cd27G" id="9i" role="lGtFl">
        <node concept="3u3nmq" id="a8" role="cd27D">
          <property role="3u3nmv" value="7011654996645186516" />
        </node>
      </node>
    </node>
    <node concept="cd27G" id="5I" role="lGtFl">
      <node concept="3u3nmq" id="a9" role="cd27D">
        <property role="3u3nmv" value="7011654996645186516" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="aa">
    <property role="3GE5qa" value="base.conditions" />
    <property role="TrG5h" value="NamedConditionReference_Constraints" />
    <node concept="3Tm1VV" id="ab" role="1B3o_S">
      <node concept="cd27G" id="ah" role="lGtFl">
        <node concept="3u3nmq" id="ai" role="cd27D">
          <property role="3u3nmv" value="1560130832597589694" />
        </node>
      </node>
    </node>
    <node concept="3uibUv" id="ac" role="1zkMxy">
      <ref role="3uigEE" to="79pl:~BaseConstraintsDescriptor" resolve="BaseConstraintsDescriptor" />
      <node concept="cd27G" id="aj" role="lGtFl">
        <node concept="3u3nmq" id="ak" role="cd27D">
          <property role="3u3nmv" value="1560130832597589694" />
        </node>
      </node>
    </node>
    <node concept="3clFbW" id="ad" role="jymVt">
      <node concept="3cqZAl" id="al" role="3clF45">
        <node concept="cd27G" id="ap" role="lGtFl">
          <node concept="3u3nmq" id="aq" role="cd27D">
            <property role="3u3nmv" value="1560130832597589694" />
          </node>
        </node>
      </node>
      <node concept="3clFbS" id="am" role="3clF47">
        <node concept="XkiVB" id="ar" role="3cqZAp">
          <ref role="37wK5l" to="79pl:~BaseConstraintsDescriptor.&lt;init&gt;(org.jetbrains.mps.openapi.language.SAbstractConcept)" resolve="BaseConstraintsDescriptor" />
          <node concept="2YIFZM" id="at" role="37wK5m">
            <ref role="1Pybhc" to="2k9e:~MetaAdapterFactory" resolve="MetaAdapterFactory" />
            <ref role="37wK5l" to="2k9e:~MetaAdapterFactory.getConcept(long,long,long,java.lang.String):org.jetbrains.mps.openapi.language.SConcept" resolve="getConcept" />
            <node concept="1adDum" id="av" role="37wK5m">
              <property role="1adDun" value="0xc610a4ebc47c4f95L" />
              <node concept="cd27G" id="a$" role="lGtFl">
                <node concept="3u3nmq" id="a_" role="cd27D">
                  <property role="3u3nmv" value="1560130832597589694" />
                </node>
              </node>
            </node>
            <node concept="1adDum" id="aw" role="37wK5m">
              <property role="1adDun" value="0xb56811236971769cL" />
              <node concept="cd27G" id="aA" role="lGtFl">
                <node concept="3u3nmq" id="aB" role="cd27D">
                  <property role="3u3nmv" value="1560130832597589694" />
                </node>
              </node>
            </node>
            <node concept="1adDum" id="ax" role="37wK5m">
              <property role="1adDun" value="0x15a6b2b9efc49b11L" />
              <node concept="cd27G" id="aC" role="lGtFl">
                <node concept="3u3nmq" id="aD" role="cd27D">
                  <property role="3u3nmv" value="1560130832597589694" />
                </node>
              </node>
            </node>
            <node concept="Xl_RD" id="ay" role="37wK5m">
              <property role="Xl_RC" value="no.uio.mLab.decisions.core.structure.NamedConditionReference" />
              <node concept="cd27G" id="aE" role="lGtFl">
                <node concept="3u3nmq" id="aF" role="cd27D">
                  <property role="3u3nmv" value="1560130832597589694" />
                </node>
              </node>
            </node>
            <node concept="cd27G" id="az" role="lGtFl">
              <node concept="3u3nmq" id="aG" role="cd27D">
                <property role="3u3nmv" value="1560130832597589694" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="au" role="lGtFl">
            <node concept="3u3nmq" id="aH" role="cd27D">
              <property role="3u3nmv" value="1560130832597589694" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="as" role="lGtFl">
          <node concept="3u3nmq" id="aI" role="cd27D">
            <property role="3u3nmv" value="1560130832597589694" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="an" role="1B3o_S">
        <node concept="cd27G" id="aJ" role="lGtFl">
          <node concept="3u3nmq" id="aK" role="cd27D">
            <property role="3u3nmv" value="1560130832597589694" />
          </node>
        </node>
      </node>
      <node concept="cd27G" id="ao" role="lGtFl">
        <node concept="3u3nmq" id="aL" role="cd27D">
          <property role="3u3nmv" value="1560130832597589694" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="ae" role="jymVt">
      <node concept="cd27G" id="aM" role="lGtFl">
        <node concept="3u3nmq" id="aN" role="cd27D">
          <property role="3u3nmv" value="1560130832597589694" />
        </node>
      </node>
    </node>
    <node concept="3clFb_" id="af" role="jymVt">
      <property role="IEkAT" value="false" />
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="getSpecifiedReferences" />
      <property role="DiZV1" value="false" />
      <node concept="3Tmbuc" id="aO" role="1B3o_S">
        <node concept="cd27G" id="aT" role="lGtFl">
          <node concept="3u3nmq" id="aU" role="cd27D">
            <property role="3u3nmv" value="1560130832597589694" />
          </node>
        </node>
      </node>
      <node concept="3uibUv" id="aP" role="3clF45">
        <ref role="3uigEE" to="33ny:~Map" resolve="Map" />
        <node concept="3uibUv" id="aV" role="11_B2D">
          <ref role="3uigEE" to="c17a:~SReferenceLink" resolve="SReferenceLink" />
          <node concept="cd27G" id="aY" role="lGtFl">
            <node concept="3u3nmq" id="aZ" role="cd27D">
              <property role="3u3nmv" value="1560130832597589694" />
            </node>
          </node>
        </node>
        <node concept="3uibUv" id="aW" role="11_B2D">
          <ref role="3uigEE" to="ze1i:~ReferenceConstraintsDescriptor" resolve="ReferenceConstraintsDescriptor" />
          <node concept="cd27G" id="b0" role="lGtFl">
            <node concept="3u3nmq" id="b1" role="cd27D">
              <property role="3u3nmv" value="1560130832597589694" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="aX" role="lGtFl">
          <node concept="3u3nmq" id="b2" role="cd27D">
            <property role="3u3nmv" value="1560130832597589694" />
          </node>
        </node>
      </node>
      <node concept="3clFbS" id="aQ" role="3clF47">
        <node concept="3cpWs8" id="b3" role="3cqZAp">
          <node concept="3cpWsn" id="b7" role="3cpWs9">
            <property role="TrG5h" value="references" />
            <node concept="3uibUv" id="b9" role="1tU5fm">
              <ref role="3uigEE" to="33ny:~Map" resolve="Map" />
              <node concept="3uibUv" id="bc" role="11_B2D">
                <ref role="3uigEE" to="c17a:~SReferenceLink" resolve="SReferenceLink" />
                <node concept="cd27G" id="bf" role="lGtFl">
                  <node concept="3u3nmq" id="bg" role="cd27D">
                    <property role="3u3nmv" value="1560130832597589694" />
                  </node>
                </node>
              </node>
              <node concept="3uibUv" id="bd" role="11_B2D">
                <ref role="3uigEE" to="ze1i:~ReferenceConstraintsDescriptor" resolve="ReferenceConstraintsDescriptor" />
                <node concept="cd27G" id="bh" role="lGtFl">
                  <node concept="3u3nmq" id="bi" role="cd27D">
                    <property role="3u3nmv" value="1560130832597589694" />
                  </node>
                </node>
              </node>
              <node concept="cd27G" id="be" role="lGtFl">
                <node concept="3u3nmq" id="bj" role="cd27D">
                  <property role="3u3nmv" value="1560130832597589694" />
                </node>
              </node>
            </node>
            <node concept="2ShNRf" id="ba" role="33vP2m">
              <node concept="1pGfFk" id="bk" role="2ShVmc">
                <ref role="37wK5l" to="33ny:~HashMap.&lt;init&gt;()" resolve="HashMap" />
                <node concept="3uibUv" id="bm" role="1pMfVU">
                  <ref role="3uigEE" to="c17a:~SReferenceLink" resolve="SReferenceLink" />
                  <node concept="cd27G" id="bp" role="lGtFl">
                    <node concept="3u3nmq" id="bq" role="cd27D">
                      <property role="3u3nmv" value="1560130832597589694" />
                    </node>
                  </node>
                </node>
                <node concept="3uibUv" id="bn" role="1pMfVU">
                  <ref role="3uigEE" to="ze1i:~ReferenceConstraintsDescriptor" resolve="ReferenceConstraintsDescriptor" />
                  <node concept="cd27G" id="br" role="lGtFl">
                    <node concept="3u3nmq" id="bs" role="cd27D">
                      <property role="3u3nmv" value="1560130832597589694" />
                    </node>
                  </node>
                </node>
                <node concept="cd27G" id="bo" role="lGtFl">
                  <node concept="3u3nmq" id="bt" role="cd27D">
                    <property role="3u3nmv" value="1560130832597589694" />
                  </node>
                </node>
              </node>
              <node concept="cd27G" id="bl" role="lGtFl">
                <node concept="3u3nmq" id="bu" role="cd27D">
                  <property role="3u3nmv" value="1560130832597589694" />
                </node>
              </node>
            </node>
            <node concept="cd27G" id="bb" role="lGtFl">
              <node concept="3u3nmq" id="bv" role="cd27D">
                <property role="3u3nmv" value="1560130832597589694" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="b8" role="lGtFl">
            <node concept="3u3nmq" id="bw" role="cd27D">
              <property role="3u3nmv" value="1560130832597589694" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="b4" role="3cqZAp">
          <node concept="2OqwBi" id="bx" role="3clFbG">
            <node concept="37vLTw" id="bz" role="2Oq$k0">
              <ref role="3cqZAo" node="b7" resolve="references" />
              <node concept="cd27G" id="bA" role="lGtFl">
                <node concept="3u3nmq" id="bB" role="cd27D">
                  <property role="3u3nmv" value="1560130832597589694" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="b$" role="2OqNvi">
              <ref role="37wK5l" to="33ny:~Map.put(java.lang.Object,java.lang.Object):java.lang.Object" resolve="put" />
              <node concept="2YIFZM" id="bC" role="37wK5m">
                <ref role="1Pybhc" to="2k9e:~MetaAdapterFactory" resolve="MetaAdapterFactory" />
                <ref role="37wK5l" to="2k9e:~MetaAdapterFactory.getReferenceLink(long,long,long,long,java.lang.String):org.jetbrains.mps.openapi.language.SReferenceLink" resolve="getReferenceLink" />
                <node concept="1adDum" id="bF" role="37wK5m">
                  <property role="1adDun" value="0xc610a4ebc47c4f95L" />
                  <node concept="cd27G" id="bL" role="lGtFl">
                    <node concept="3u3nmq" id="bM" role="cd27D">
                      <property role="3u3nmv" value="1560130832597589694" />
                    </node>
                  </node>
                </node>
                <node concept="1adDum" id="bG" role="37wK5m">
                  <property role="1adDun" value="0xb56811236971769cL" />
                  <node concept="cd27G" id="bN" role="lGtFl">
                    <node concept="3u3nmq" id="bO" role="cd27D">
                      <property role="3u3nmv" value="1560130832597589694" />
                    </node>
                  </node>
                </node>
                <node concept="1adDum" id="bH" role="37wK5m">
                  <property role="1adDun" value="0x15a6b2b9efc49b11L" />
                  <node concept="cd27G" id="bP" role="lGtFl">
                    <node concept="3u3nmq" id="bQ" role="cd27D">
                      <property role="3u3nmv" value="1560130832597589694" />
                    </node>
                  </node>
                </node>
                <node concept="1adDum" id="bI" role="37wK5m">
                  <property role="1adDun" value="0x15a6b2b9efc49b12L" />
                  <node concept="cd27G" id="bR" role="lGtFl">
                    <node concept="3u3nmq" id="bS" role="cd27D">
                      <property role="3u3nmv" value="1560130832597589694" />
                    </node>
                  </node>
                </node>
                <node concept="Xl_RD" id="bJ" role="37wK5m">
                  <property role="Xl_RC" value="target" />
                  <node concept="cd27G" id="bT" role="lGtFl">
                    <node concept="3u3nmq" id="bU" role="cd27D">
                      <property role="3u3nmv" value="1560130832597589694" />
                    </node>
                  </node>
                </node>
                <node concept="cd27G" id="bK" role="lGtFl">
                  <node concept="3u3nmq" id="bV" role="cd27D">
                    <property role="3u3nmv" value="1560130832597589694" />
                  </node>
                </node>
              </node>
              <node concept="2ShNRf" id="bD" role="37wK5m">
                <node concept="YeOm9" id="bW" role="2ShVmc">
                  <node concept="1Y3b0j" id="bY" role="YeSDq">
                    <property role="2bfB8j" value="true" />
                    <ref role="1Y3XeK" to="79pl:~BaseReferenceConstraintsDescriptor" resolve="BaseReferenceConstraintsDescriptor" />
                    <ref role="37wK5l" to="79pl:~BaseReferenceConstraintsDescriptor.&lt;init&gt;(jetbrains.mps.smodel.adapter.ids.SReferenceLinkId,jetbrains.mps.smodel.runtime.ConstraintsDescriptor)" resolve="BaseReferenceConstraintsDescriptor" />
                    <node concept="2YIFZM" id="c0" role="37wK5m">
                      <ref role="1Pybhc" to="e8bb:~MetaIdFactory" resolve="MetaIdFactory" />
                      <ref role="37wK5l" to="e8bb:~MetaIdFactory.refId(long,long,long,long):jetbrains.mps.smodel.adapter.ids.SReferenceLinkId" resolve="refId" />
                      <node concept="1adDum" id="c6" role="37wK5m">
                        <property role="1adDun" value="0xc610a4ebc47c4f95L" />
                        <node concept="cd27G" id="cb" role="lGtFl">
                          <node concept="3u3nmq" id="cc" role="cd27D">
                            <property role="3u3nmv" value="1560130832597589694" />
                          </node>
                        </node>
                      </node>
                      <node concept="1adDum" id="c7" role="37wK5m">
                        <property role="1adDun" value="0xb56811236971769cL" />
                        <node concept="cd27G" id="cd" role="lGtFl">
                          <node concept="3u3nmq" id="ce" role="cd27D">
                            <property role="3u3nmv" value="1560130832597589694" />
                          </node>
                        </node>
                      </node>
                      <node concept="1adDum" id="c8" role="37wK5m">
                        <property role="1adDun" value="0x15a6b2b9efc49b11L" />
                        <node concept="cd27G" id="cf" role="lGtFl">
                          <node concept="3u3nmq" id="cg" role="cd27D">
                            <property role="3u3nmv" value="1560130832597589694" />
                          </node>
                        </node>
                      </node>
                      <node concept="1adDum" id="c9" role="37wK5m">
                        <property role="1adDun" value="0x15a6b2b9efc49b12L" />
                        <node concept="cd27G" id="ch" role="lGtFl">
                          <node concept="3u3nmq" id="ci" role="cd27D">
                            <property role="3u3nmv" value="1560130832597589694" />
                          </node>
                        </node>
                      </node>
                      <node concept="cd27G" id="ca" role="lGtFl">
                        <node concept="3u3nmq" id="cj" role="cd27D">
                          <property role="3u3nmv" value="1560130832597589694" />
                        </node>
                      </node>
                    </node>
                    <node concept="3Tm1VV" id="c1" role="1B3o_S">
                      <node concept="cd27G" id="ck" role="lGtFl">
                        <node concept="3u3nmq" id="cl" role="cd27D">
                          <property role="3u3nmv" value="1560130832597589694" />
                        </node>
                      </node>
                    </node>
                    <node concept="Xjq3P" id="c2" role="37wK5m">
                      <node concept="cd27G" id="cm" role="lGtFl">
                        <node concept="3u3nmq" id="cn" role="cd27D">
                          <property role="3u3nmv" value="1560130832597589694" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFb_" id="c3" role="jymVt">
                      <property role="IEkAT" value="false" />
                      <property role="1EzhhJ" value="false" />
                      <property role="TrG5h" value="hasOwnScopeProvider" />
                      <property role="DiZV1" value="false" />
                      <node concept="3Tm1VV" id="co" role="1B3o_S">
                        <node concept="cd27G" id="ct" role="lGtFl">
                          <node concept="3u3nmq" id="cu" role="cd27D">
                            <property role="3u3nmv" value="1560130832597589694" />
                          </node>
                        </node>
                      </node>
                      <node concept="10P_77" id="cp" role="3clF45">
                        <node concept="cd27G" id="cv" role="lGtFl">
                          <node concept="3u3nmq" id="cw" role="cd27D">
                            <property role="3u3nmv" value="1560130832597589694" />
                          </node>
                        </node>
                      </node>
                      <node concept="3clFbS" id="cq" role="3clF47">
                        <node concept="3clFbF" id="cx" role="3cqZAp">
                          <node concept="3clFbT" id="cz" role="3clFbG">
                            <property role="3clFbU" value="true" />
                            <node concept="cd27G" id="c_" role="lGtFl">
                              <node concept="3u3nmq" id="cA" role="cd27D">
                                <property role="3u3nmv" value="1560130832597589694" />
                              </node>
                            </node>
                          </node>
                          <node concept="cd27G" id="c$" role="lGtFl">
                            <node concept="3u3nmq" id="cB" role="cd27D">
                              <property role="3u3nmv" value="1560130832597589694" />
                            </node>
                          </node>
                        </node>
                        <node concept="cd27G" id="cy" role="lGtFl">
                          <node concept="3u3nmq" id="cC" role="cd27D">
                            <property role="3u3nmv" value="1560130832597589694" />
                          </node>
                        </node>
                      </node>
                      <node concept="2AHcQZ" id="cr" role="2AJF6D">
                        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
                        <node concept="cd27G" id="cD" role="lGtFl">
                          <node concept="3u3nmq" id="cE" role="cd27D">
                            <property role="3u3nmv" value="1560130832597589694" />
                          </node>
                        </node>
                      </node>
                      <node concept="cd27G" id="cs" role="lGtFl">
                        <node concept="3u3nmq" id="cF" role="cd27D">
                          <property role="3u3nmv" value="1560130832597589694" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFb_" id="c4" role="jymVt">
                      <property role="IEkAT" value="false" />
                      <property role="1EzhhJ" value="false" />
                      <property role="TrG5h" value="getScopeProvider" />
                      <property role="DiZV1" value="false" />
                      <node concept="3Tm1VV" id="cG" role="1B3o_S">
                        <node concept="cd27G" id="cM" role="lGtFl">
                          <node concept="3u3nmq" id="cN" role="cd27D">
                            <property role="3u3nmv" value="1560130832597589694" />
                          </node>
                        </node>
                      </node>
                      <node concept="3uibUv" id="cH" role="3clF45">
                        <ref role="3uigEE" to="ze1i:~ReferenceScopeProvider" resolve="ReferenceScopeProvider" />
                        <node concept="cd27G" id="cO" role="lGtFl">
                          <node concept="3u3nmq" id="cP" role="cd27D">
                            <property role="3u3nmv" value="1560130832597589694" />
                          </node>
                        </node>
                      </node>
                      <node concept="2AHcQZ" id="cI" role="2AJF6D">
                        <ref role="2AI5Lk" to="mhfm:~Nullable" resolve="Nullable" />
                        <node concept="cd27G" id="cQ" role="lGtFl">
                          <node concept="3u3nmq" id="cR" role="cd27D">
                            <property role="3u3nmv" value="1560130832597589694" />
                          </node>
                        </node>
                      </node>
                      <node concept="3clFbS" id="cJ" role="3clF47">
                        <node concept="3cpWs6" id="cS" role="3cqZAp">
                          <node concept="2ShNRf" id="cU" role="3cqZAk">
                            <node concept="YeOm9" id="cW" role="2ShVmc">
                              <node concept="1Y3b0j" id="cY" role="YeSDq">
                                <property role="2bfB8j" value="true" />
                                <ref role="1Y3XeK" to="79pl:~BaseScopeProvider" resolve="BaseScopeProvider" />
                                <ref role="37wK5l" to="79pl:~BaseScopeProvider.&lt;init&gt;()" resolve="BaseScopeProvider" />
                                <node concept="3Tm1VV" id="d0" role="1B3o_S">
                                  <node concept="cd27G" id="d4" role="lGtFl">
                                    <node concept="3u3nmq" id="d5" role="cd27D">
                                      <property role="3u3nmv" value="1560130832597589694" />
                                    </node>
                                  </node>
                                </node>
                                <node concept="3clFb_" id="d1" role="jymVt">
                                  <property role="TrG5h" value="getSearchScopeValidatorNode" />
                                  <node concept="3Tm1VV" id="d6" role="1B3o_S">
                                    <node concept="cd27G" id="db" role="lGtFl">
                                      <node concept="3u3nmq" id="dc" role="cd27D">
                                        <property role="3u3nmv" value="1560130832597589694" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="3clFbS" id="d7" role="3clF47">
                                    <node concept="3cpWs6" id="dd" role="3cqZAp">
                                      <node concept="1dyn4i" id="df" role="3cqZAk">
                                        <property role="1zomUR" value="true" />
                                        <property role="1dyqJU" value="breakingNode" />
                                        <node concept="2ShNRf" id="dh" role="1dyrYi">
                                          <node concept="1pGfFk" id="dj" role="2ShVmc">
                                            <ref role="37wK5l" to="w1kc:~SNodePointer.&lt;init&gt;(java.lang.String,java.lang.String)" resolve="SNodePointer" />
                                            <node concept="Xl_RD" id="dl" role="37wK5m">
                                              <property role="Xl_RC" value="r:ac11bdd6-c85d-40e0-910f-d45067cda53d(no.uio.mLab.decisions.core.constraints)" />
                                              <node concept="cd27G" id="do" role="lGtFl">
                                                <node concept="3u3nmq" id="dp" role="cd27D">
                                                  <property role="3u3nmv" value="1560130832597589694" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="Xl_RD" id="dm" role="37wK5m">
                                              <property role="Xl_RC" value="1560130832597589697" />
                                              <node concept="cd27G" id="dq" role="lGtFl">
                                                <node concept="3u3nmq" id="dr" role="cd27D">
                                                  <property role="3u3nmv" value="1560130832597589694" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="cd27G" id="dn" role="lGtFl">
                                              <node concept="3u3nmq" id="ds" role="cd27D">
                                                <property role="3u3nmv" value="1560130832597589694" />
                                              </node>
                                            </node>
                                          </node>
                                          <node concept="cd27G" id="dk" role="lGtFl">
                                            <node concept="3u3nmq" id="dt" role="cd27D">
                                              <property role="3u3nmv" value="1560130832597589694" />
                                            </node>
                                          </node>
                                        </node>
                                        <node concept="cd27G" id="di" role="lGtFl">
                                          <node concept="3u3nmq" id="du" role="cd27D">
                                            <property role="3u3nmv" value="1560130832597589694" />
                                          </node>
                                        </node>
                                      </node>
                                      <node concept="cd27G" id="dg" role="lGtFl">
                                        <node concept="3u3nmq" id="dv" role="cd27D">
                                          <property role="3u3nmv" value="1560130832597589694" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="cd27G" id="de" role="lGtFl">
                                      <node concept="3u3nmq" id="dw" role="cd27D">
                                        <property role="3u3nmv" value="1560130832597589694" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="3uibUv" id="d8" role="3clF45">
                                    <ref role="3uigEE" to="mhbf:~SNodeReference" resolve="SNodeReference" />
                                    <node concept="cd27G" id="dx" role="lGtFl">
                                      <node concept="3u3nmq" id="dy" role="cd27D">
                                        <property role="3u3nmv" value="1560130832597589694" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="2AHcQZ" id="d9" role="2AJF6D">
                                    <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
                                    <node concept="cd27G" id="dz" role="lGtFl">
                                      <node concept="3u3nmq" id="d$" role="cd27D">
                                        <property role="3u3nmv" value="1560130832597589694" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="cd27G" id="da" role="lGtFl">
                                    <node concept="3u3nmq" id="d_" role="cd27D">
                                      <property role="3u3nmv" value="1560130832597589694" />
                                    </node>
                                  </node>
                                </node>
                                <node concept="3clFb_" id="d2" role="jymVt">
                                  <property role="IEkAT" value="false" />
                                  <property role="1EzhhJ" value="false" />
                                  <property role="TrG5h" value="createScope" />
                                  <property role="DiZV1" value="false" />
                                  <node concept="37vLTG" id="dA" role="3clF46">
                                    <property role="TrG5h" value="operationContext" />
                                    <property role="3TUv4t" value="true" />
                                    <node concept="3uibUv" id="dH" role="1tU5fm">
                                      <ref role="3uigEE" to="w1kc:~IOperationContext" resolve="IOperationContext" />
                                      <node concept="cd27G" id="dJ" role="lGtFl">
                                        <node concept="3u3nmq" id="dK" role="cd27D">
                                          <property role="3u3nmv" value="1560130832597589694" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="cd27G" id="dI" role="lGtFl">
                                      <node concept="3u3nmq" id="dL" role="cd27D">
                                        <property role="3u3nmv" value="1560130832597589694" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="37vLTG" id="dB" role="3clF46">
                                    <property role="TrG5h" value="_context" />
                                    <property role="3TUv4t" value="true" />
                                    <node concept="3uibUv" id="dM" role="1tU5fm">
                                      <ref role="3uigEE" to="ze1i:~ReferenceConstraintsContext" resolve="ReferenceConstraintsContext" />
                                      <node concept="cd27G" id="dO" role="lGtFl">
                                        <node concept="3u3nmq" id="dP" role="cd27D">
                                          <property role="3u3nmv" value="1560130832597589694" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="cd27G" id="dN" role="lGtFl">
                                      <node concept="3u3nmq" id="dQ" role="cd27D">
                                        <property role="3u3nmv" value="1560130832597589694" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="3Tm1VV" id="dC" role="1B3o_S">
                                    <node concept="cd27G" id="dR" role="lGtFl">
                                      <node concept="3u3nmq" id="dS" role="cd27D">
                                        <property role="3u3nmv" value="1560130832597589694" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="3uibUv" id="dD" role="3clF45">
                                    <ref role="3uigEE" to="35tq:~Scope" resolve="Scope" />
                                    <node concept="cd27G" id="dT" role="lGtFl">
                                      <node concept="3u3nmq" id="dU" role="cd27D">
                                        <property role="3u3nmv" value="1560130832597589694" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="3clFbS" id="dE" role="3clF47">
                                    <node concept="9aQIb" id="dV" role="3cqZAp">
                                      <node concept="3clFbS" id="dX" role="9aQI4">
                                        <node concept="3cpWs8" id="dZ" role="3cqZAp">
                                          <node concept="3cpWsn" id="e2" role="3cpWs9">
                                            <property role="TrG5h" value="scope" />
                                            <node concept="3uibUv" id="e4" role="1tU5fm">
                                              <ref role="3uigEE" to="35tq:~Scope" resolve="Scope" />
                                              <node concept="cd27G" id="e7" role="lGtFl">
                                                <node concept="3u3nmq" id="e8" role="cd27D">
                                                  <property role="3u3nmv" value="1560130832597589694" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="2YIFZM" id="e5" role="33vP2m">
                                              <ref role="1Pybhc" to="35tq:~Scope" resolve="Scope" />
                                              <ref role="37wK5l" to="35tq:~Scope.getScope(org.jetbrains.mps.openapi.model.SNode,org.jetbrains.mps.openapi.language.SContainmentLink,int,org.jetbrains.mps.openapi.language.SAbstractConcept):jetbrains.mps.scope.Scope" resolve="getScope" />
                                              <node concept="2OqwBi" id="e9" role="37wK5m">
                                                <node concept="37vLTw" id="ee" role="2Oq$k0">
                                                  <ref role="3cqZAo" node="dB" resolve="_context" />
                                                  <node concept="cd27G" id="eh" role="lGtFl">
                                                    <node concept="3u3nmq" id="ei" role="cd27D">
                                                      <property role="3u3nmv" value="1560130832597589694" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="liA8E" id="ef" role="2OqNvi">
                                                  <ref role="37wK5l" to="ze1i:~ReferenceConstraintsContext.getContextNode():org.jetbrains.mps.openapi.model.SNode" resolve="getContextNode" />
                                                  <node concept="cd27G" id="ej" role="lGtFl">
                                                    <node concept="3u3nmq" id="ek" role="cd27D">
                                                      <property role="3u3nmv" value="1560130832597589694" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="cd27G" id="eg" role="lGtFl">
                                                  <node concept="3u3nmq" id="el" role="cd27D">
                                                    <property role="3u3nmv" value="1560130832597589694" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="2OqwBi" id="ea" role="37wK5m">
                                                <node concept="liA8E" id="em" role="2OqNvi">
                                                  <ref role="37wK5l" to="ze1i:~ReferenceConstraintsContext.getContainmentLink():org.jetbrains.mps.openapi.language.SContainmentLink" resolve="getContainmentLink" />
                                                  <node concept="cd27G" id="ep" role="lGtFl">
                                                    <node concept="3u3nmq" id="eq" role="cd27D">
                                                      <property role="3u3nmv" value="1560130832597589694" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="37vLTw" id="en" role="2Oq$k0">
                                                  <ref role="3cqZAo" node="dB" resolve="_context" />
                                                  <node concept="cd27G" id="er" role="lGtFl">
                                                    <node concept="3u3nmq" id="es" role="cd27D">
                                                      <property role="3u3nmv" value="1560130832597589694" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="cd27G" id="eo" role="lGtFl">
                                                  <node concept="3u3nmq" id="et" role="cd27D">
                                                    <property role="3u3nmv" value="1560130832597589694" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="2OqwBi" id="eb" role="37wK5m">
                                                <node concept="37vLTw" id="eu" role="2Oq$k0">
                                                  <ref role="3cqZAo" node="dB" resolve="_context" />
                                                  <node concept="cd27G" id="ex" role="lGtFl">
                                                    <node concept="3u3nmq" id="ey" role="cd27D">
                                                      <property role="3u3nmv" value="1560130832597589694" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="liA8E" id="ev" role="2OqNvi">
                                                  <ref role="37wK5l" to="ze1i:~ReferenceConstraintsContext.getPosition():int" resolve="getPosition" />
                                                  <node concept="cd27G" id="ez" role="lGtFl">
                                                    <node concept="3u3nmq" id="e$" role="cd27D">
                                                      <property role="3u3nmv" value="1560130832597589694" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="cd27G" id="ew" role="lGtFl">
                                                  <node concept="3u3nmq" id="e_" role="cd27D">
                                                    <property role="3u3nmv" value="1560130832597589694" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="35c_gC" id="ec" role="37wK5m">
                                                <ref role="35c_gD" to="7f9y:1mAGFBJeyRH" resolve="NamedCondition" />
                                                <node concept="cd27G" id="eA" role="lGtFl">
                                                  <node concept="3u3nmq" id="eB" role="cd27D">
                                                    <property role="3u3nmv" value="1560130832597589694" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="cd27G" id="ed" role="lGtFl">
                                                <node concept="3u3nmq" id="eC" role="cd27D">
                                                  <property role="3u3nmv" value="1560130832597589694" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="cd27G" id="e6" role="lGtFl">
                                              <node concept="3u3nmq" id="eD" role="cd27D">
                                                <property role="3u3nmv" value="1560130832597589694" />
                                              </node>
                                            </node>
                                          </node>
                                          <node concept="cd27G" id="e3" role="lGtFl">
                                            <node concept="3u3nmq" id="eE" role="cd27D">
                                              <property role="3u3nmv" value="1560130832597589694" />
                                            </node>
                                          </node>
                                        </node>
                                        <node concept="3cpWs6" id="e0" role="3cqZAp">
                                          <node concept="3K4zz7" id="eF" role="3cqZAk">
                                            <node concept="2ShNRf" id="eH" role="3K4E3e">
                                              <node concept="1pGfFk" id="eL" role="2ShVmc">
                                                <ref role="37wK5l" to="35tq:~EmptyScope.&lt;init&gt;()" resolve="EmptyScope" />
                                                <node concept="cd27G" id="eN" role="lGtFl">
                                                  <node concept="3u3nmq" id="eO" role="cd27D">
                                                    <property role="3u3nmv" value="1560130832597589694" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="cd27G" id="eM" role="lGtFl">
                                                <node concept="3u3nmq" id="eP" role="cd27D">
                                                  <property role="3u3nmv" value="1560130832597589694" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="37vLTw" id="eI" role="3K4GZi">
                                              <ref role="3cqZAo" node="e2" resolve="scope" />
                                              <node concept="cd27G" id="eQ" role="lGtFl">
                                                <node concept="3u3nmq" id="eR" role="cd27D">
                                                  <property role="3u3nmv" value="1560130832597589694" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="3clFbC" id="eJ" role="3K4Cdx">
                                              <node concept="10Nm6u" id="eS" role="3uHU7w">
                                                <node concept="cd27G" id="eV" role="lGtFl">
                                                  <node concept="3u3nmq" id="eW" role="cd27D">
                                                    <property role="3u3nmv" value="1560130832597589694" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="37vLTw" id="eT" role="3uHU7B">
                                                <ref role="3cqZAo" node="e2" resolve="scope" />
                                                <node concept="cd27G" id="eX" role="lGtFl">
                                                  <node concept="3u3nmq" id="eY" role="cd27D">
                                                    <property role="3u3nmv" value="1560130832597589694" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="cd27G" id="eU" role="lGtFl">
                                                <node concept="3u3nmq" id="eZ" role="cd27D">
                                                  <property role="3u3nmv" value="1560130832597589694" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="cd27G" id="eK" role="lGtFl">
                                              <node concept="3u3nmq" id="f0" role="cd27D">
                                                <property role="3u3nmv" value="1560130832597589694" />
                                              </node>
                                            </node>
                                          </node>
                                          <node concept="cd27G" id="eG" role="lGtFl">
                                            <node concept="3u3nmq" id="f1" role="cd27D">
                                              <property role="3u3nmv" value="1560130832597589694" />
                                            </node>
                                          </node>
                                        </node>
                                        <node concept="cd27G" id="e1" role="lGtFl">
                                          <node concept="3u3nmq" id="f2" role="cd27D">
                                            <property role="3u3nmv" value="1560130832597589694" />
                                          </node>
                                        </node>
                                      </node>
                                      <node concept="cd27G" id="dY" role="lGtFl">
                                        <node concept="3u3nmq" id="f3" role="cd27D">
                                          <property role="3u3nmv" value="1560130832597589694" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="cd27G" id="dW" role="lGtFl">
                                      <node concept="3u3nmq" id="f4" role="cd27D">
                                        <property role="3u3nmv" value="1560130832597589694" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="2AHcQZ" id="dF" role="2AJF6D">
                                    <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
                                    <node concept="cd27G" id="f5" role="lGtFl">
                                      <node concept="3u3nmq" id="f6" role="cd27D">
                                        <property role="3u3nmv" value="1560130832597589694" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="cd27G" id="dG" role="lGtFl">
                                    <node concept="3u3nmq" id="f7" role="cd27D">
                                      <property role="3u3nmv" value="1560130832597589694" />
                                    </node>
                                  </node>
                                </node>
                                <node concept="cd27G" id="d3" role="lGtFl">
                                  <node concept="3u3nmq" id="f8" role="cd27D">
                                    <property role="3u3nmv" value="1560130832597589694" />
                                  </node>
                                </node>
                              </node>
                              <node concept="cd27G" id="cZ" role="lGtFl">
                                <node concept="3u3nmq" id="f9" role="cd27D">
                                  <property role="3u3nmv" value="1560130832597589694" />
                                </node>
                              </node>
                            </node>
                            <node concept="cd27G" id="cX" role="lGtFl">
                              <node concept="3u3nmq" id="fa" role="cd27D">
                                <property role="3u3nmv" value="1560130832597589694" />
                              </node>
                            </node>
                          </node>
                          <node concept="cd27G" id="cV" role="lGtFl">
                            <node concept="3u3nmq" id="fb" role="cd27D">
                              <property role="3u3nmv" value="1560130832597589694" />
                            </node>
                          </node>
                        </node>
                        <node concept="cd27G" id="cT" role="lGtFl">
                          <node concept="3u3nmq" id="fc" role="cd27D">
                            <property role="3u3nmv" value="1560130832597589694" />
                          </node>
                        </node>
                      </node>
                      <node concept="2AHcQZ" id="cK" role="2AJF6D">
                        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
                        <node concept="cd27G" id="fd" role="lGtFl">
                          <node concept="3u3nmq" id="fe" role="cd27D">
                            <property role="3u3nmv" value="1560130832597589694" />
                          </node>
                        </node>
                      </node>
                      <node concept="cd27G" id="cL" role="lGtFl">
                        <node concept="3u3nmq" id="ff" role="cd27D">
                          <property role="3u3nmv" value="1560130832597589694" />
                        </node>
                      </node>
                    </node>
                    <node concept="cd27G" id="c5" role="lGtFl">
                      <node concept="3u3nmq" id="fg" role="cd27D">
                        <property role="3u3nmv" value="1560130832597589694" />
                      </node>
                    </node>
                  </node>
                  <node concept="cd27G" id="bZ" role="lGtFl">
                    <node concept="3u3nmq" id="fh" role="cd27D">
                      <property role="3u3nmv" value="1560130832597589694" />
                    </node>
                  </node>
                </node>
                <node concept="cd27G" id="bX" role="lGtFl">
                  <node concept="3u3nmq" id="fi" role="cd27D">
                    <property role="3u3nmv" value="1560130832597589694" />
                  </node>
                </node>
              </node>
              <node concept="cd27G" id="bE" role="lGtFl">
                <node concept="3u3nmq" id="fj" role="cd27D">
                  <property role="3u3nmv" value="1560130832597589694" />
                </node>
              </node>
            </node>
            <node concept="cd27G" id="b_" role="lGtFl">
              <node concept="3u3nmq" id="fk" role="cd27D">
                <property role="3u3nmv" value="1560130832597589694" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="by" role="lGtFl">
            <node concept="3u3nmq" id="fl" role="cd27D">
              <property role="3u3nmv" value="1560130832597589694" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="b5" role="3cqZAp">
          <node concept="37vLTw" id="fm" role="3clFbG">
            <ref role="3cqZAo" node="b7" resolve="references" />
            <node concept="cd27G" id="fo" role="lGtFl">
              <node concept="3u3nmq" id="fp" role="cd27D">
                <property role="3u3nmv" value="1560130832597589694" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="fn" role="lGtFl">
            <node concept="3u3nmq" id="fq" role="cd27D">
              <property role="3u3nmv" value="1560130832597589694" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="b6" role="lGtFl">
          <node concept="3u3nmq" id="fr" role="cd27D">
            <property role="3u3nmv" value="1560130832597589694" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="aR" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <node concept="cd27G" id="fs" role="lGtFl">
          <node concept="3u3nmq" id="ft" role="cd27D">
            <property role="3u3nmv" value="1560130832597589694" />
          </node>
        </node>
      </node>
      <node concept="cd27G" id="aS" role="lGtFl">
        <node concept="3u3nmq" id="fu" role="cd27D">
          <property role="3u3nmv" value="1560130832597589694" />
        </node>
      </node>
    </node>
    <node concept="cd27G" id="ag" role="lGtFl">
      <node concept="3u3nmq" id="fv" role="cd27D">
        <property role="3u3nmv" value="1560130832597589694" />
      </node>
    </node>
  </node>
</model>

