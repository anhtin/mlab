package no.uio.mLab.decisions.core.editor;

/*Generated by MPS */

import jetbrains.mps.nodeEditor.menus.substitute.SubstituteMenuBase;
import org.jetbrains.annotations.NotNull;
import java.util.List;
import jetbrains.mps.lang.editor.menus.MenuPart;
import jetbrains.mps.openapi.editor.menus.substitute.SubstituteMenuItem;
import jetbrains.mps.openapi.editor.menus.substitute.SubstituteMenuContext;
import java.util.ArrayList;
import jetbrains.mps.lang.editor.menus.substitute.ConstraintsFilteringSubstituteMenuPartDecorator;
import jetbrains.mps.smodel.adapter.structure.MetaAdapterFactory;
import jetbrains.mps.lang.editor.menus.EditorMenuDescriptorBase;
import jetbrains.mps.smodel.SNodePointer;
import jetbrains.mps.lang.editor.menus.substitute.SingleItemSubstituteMenuPart;
import org.jetbrains.annotations.Nullable;
import org.apache.log4j.Logger;
import jetbrains.mps.lang.editor.menus.substitute.DefaultSubstituteMenuItem;
import jetbrains.mps.openapi.editor.menus.EditorMenuTraceInfo;
import org.jetbrains.mps.openapi.model.SNode;
import jetbrains.mps.smodel.action.SNodeFactoryOperations;
import jetbrains.mps.lang.smodel.generator.smodelAdapter.SConceptOperations;

public class Advice_SubstituteMenu extends SubstituteMenuBase {
  @NotNull
  @Override
  protected List<MenuPart<SubstituteMenuItem, SubstituteMenuContext>> getParts(final SubstituteMenuContext _context) {
    List<MenuPart<SubstituteMenuItem, SubstituteMenuContext>> result = new ArrayList<MenuPart<SubstituteMenuItem, SubstituteMenuContext>>();
    result.add(new ConstraintsFilteringSubstituteMenuPartDecorator(new Advice_SubstituteMenu.SMP_Action_8zzhr4_a(), MetaAdapterFactory.getConcept(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0xa21b426e5e15c80L, "no.uio.mLab.decisions.core.structure.Advice")));
    return result;
  }

  @NotNull
  @Override
  public List<SubstituteMenuItem> createMenuItems(@NotNull SubstituteMenuContext context) {
    context.getEditorMenuTrace().pushTraceInfo();
    context.getEditorMenuTrace().setDescriptor(new EditorMenuDescriptorBase("default substitute menu for " + "Advice", new SNodePointer("r:515ff38d-92f9-43f3-aa4c-c5bec29efe22(no.uio.mLab.decisions.core.editor)", "7282862830146850270")));
    try {
      return super.createMenuItems(context);
    } finally {
      context.getEditorMenuTrace().popTraceInfo();
    }
  }


  private class SMP_Action_8zzhr4_a extends SingleItemSubstituteMenuPart {

    @Nullable
    @Override
    protected SubstituteMenuItem createItem(SubstituteMenuContext _context) {
      Advice_SubstituteMenu.SMP_Action_8zzhr4_a.Item item = new Advice_SubstituteMenu.SMP_Action_8zzhr4_a.Item(_context);
      String description;
      try {
        description = "Substitute item: " + item.getMatchingText("");
      } catch (Throwable t) {
        Logger.getLogger(getClass()).error("Exception while executing getMatchingText() of the item " + item, t);
        return null;
      }

      _context.getEditorMenuTrace().pushTraceInfo();
      try {
        _context.getEditorMenuTrace().setDescriptor(new EditorMenuDescriptorBase(description, new SNodePointer("r:515ff38d-92f9-43f3-aa4c-c5bec29efe22(no.uio.mLab.decisions.core.editor)", "7282862830146850271")));
        item.setTraceInfo(_context.getEditorMenuTrace().getTraceInfo());
      } finally {
        _context.getEditorMenuTrace().popTraceInfo();
      }

      return item;
    }
    private class Item extends DefaultSubstituteMenuItem {
      private final SubstituteMenuContext _context;
      private EditorMenuTraceInfo myTraceInfo;
      public Item(SubstituteMenuContext context) {
        super(MetaAdapterFactory.getConcept(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0xa21b426e5e15c80L, "no.uio.mLab.decisions.core.structure.Advice"), context.getParentNode(), context.getCurrentTargetNode(), context.getEditorContext());
        _context = context;
      }

      private void setTraceInfo(EditorMenuTraceInfo traceInfo) {
        myTraceInfo = traceInfo;
      }

      @Nullable
      @Override
      public SNode createNode(@NotNull String pattern) {
        return SNodeFactoryOperations.createNewNode(SNodeFactoryOperations.asInstanceConcept(MetaAdapterFactory.getConcept(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0xa21b426e5e15c80L, "no.uio.mLab.decisions.core.structure.Advice")), null);
      }

      @Override
      public EditorMenuTraceInfo getTraceInfo() {
        return myTraceInfo;
      }
      @Nullable
      @Override
      public String getMatchingText(@NotNull String pattern) {
        return SConceptOperations.conceptAlias(MetaAdapterFactory.getConcept(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0xa21b426e5e15c80L, "no.uio.mLab.decisions.core.structure.Advice"));
      }
      @Nullable
      @Override
      public String getDescriptionText(@NotNull String pattern) {
        return SConceptOperations.shortDescription(MetaAdapterFactory.getConcept(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0xa21b426e5e15c80L, "no.uio.mLab.decisions.core.structure.Advice"));
      }
    }
  }
}
