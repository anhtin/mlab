package no.uio.mLab.decisions.core.editor;

/*Generated by MPS */

import jetbrains.mps.openapi.editor.cells.EditorCell;
import org.jetbrains.mps.openapi.model.SNode;
import jetbrains.mps.openapi.editor.EditorContext;
import jetbrains.mps.openapi.editor.cells.CellActionType;
import jetbrains.mps.editor.runtime.cells.AbstractCellAction;
import jetbrains.mps.editor.runtime.deletionApprover.DeletionApproverUtil;
import jetbrains.mps.lang.smodel.generator.smodelAdapter.SNodeOperations;
import jetbrains.mps.smodel.adapter.structure.MetaAdapterFactory;

public class Delete_DataValue_In_SimpleCondition {
  public static void setCellActions(EditorCell editorCell, SNode node, EditorContext context) {
    editorCell.setAction(CellActionType.DELETE, new Delete_DataValue_In_SimpleCondition.Delete_DataValue_In_SimpleCondition_DELETE(node));
    editorCell.setAction(CellActionType.BACKSPACE, new Delete_DataValue_In_SimpleCondition.Delete_DataValue_In_SimpleCondition_BACKSPACE(node));
  }
  public static class Delete_DataValue_In_SimpleCondition_DELETE extends AbstractCellAction {
    /*package*/ SNode myNode;
    public Delete_DataValue_In_SimpleCondition_DELETE(SNode node) {
      this.myNode = node;
    }
    public void execute(EditorContext editorContext) {
      this.execute_internal(editorContext, this.myNode);
    }
    public void execute_internal(EditorContext editorContext, SNode node) {
      if (DeletionApproverUtil.approve(editorContext, SNodeOperations.getParent(node))) {
        return;
      }
      SNodeOperations.deleteNode(SNodeOperations.getParent(node));
    }
    @Override
    public boolean canExecute(EditorContext editorContext) {
      return this.canExecute_internal(editorContext, this.myNode);
    }
    public boolean canExecute_internal(EditorContext editorContext, SNode node) {
      return SNodeOperations.isInstanceOf(SNodeOperations.getParent(node), MetaAdapterFactory.getConcept(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x15a6b2b9f078107cL, "no.uio.mLab.decisions.core.structure.SimpleCondition"));
    }
  }
  public static class Delete_DataValue_In_SimpleCondition_BACKSPACE extends AbstractCellAction {
    /*package*/ SNode myNode;
    public Delete_DataValue_In_SimpleCondition_BACKSPACE(SNode node) {
      this.myNode = node;
    }
    public void execute(EditorContext editorContext) {
      this.execute_internal(editorContext, this.myNode);
    }
    public void execute_internal(EditorContext editorContext, SNode node) {
      if (DeletionApproverUtil.approve(editorContext, SNodeOperations.getParent(node))) {
        return;
      }
      SNodeOperations.deleteNode(SNodeOperations.getParent(node));
    }
    @Override
    public boolean canExecute(EditorContext editorContext) {
      return this.canExecute_internal(editorContext, this.myNode);
    }
    public boolean canExecute_internal(EditorContext editorContext, SNode node) {
      return SNodeOperations.isInstanceOf(SNodeOperations.getParent(node), MetaAdapterFactory.getConcept(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x15a6b2b9f078107cL, "no.uio.mLab.decisions.core.structure.SimpleCondition"));
    }
  }
}
