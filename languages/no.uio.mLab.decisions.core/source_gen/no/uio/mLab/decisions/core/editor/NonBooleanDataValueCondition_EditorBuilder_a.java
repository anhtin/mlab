package no.uio.mLab.decisions.core.editor;

/*Generated by MPS */

import jetbrains.mps.editor.runtime.descriptor.AbstractEditorBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.mps.openapi.model.SNode;
import jetbrains.mps.openapi.editor.EditorContext;
import jetbrains.mps.openapi.editor.cells.EditorCell;
import jetbrains.mps.nodeEditor.cells.EditorCell_Collection;
import jetbrains.mps.nodeEditor.cellLayout.CellLayout_Horizontal;
import jetbrains.mps.lang.smodel.generator.smodelAdapter.SLinkOperations;
import jetbrains.mps.smodel.adapter.structure.MetaAdapterFactory;
import no.uio.mLab.decisions.core.behavior.NonBooleanDataValueCondition__BehaviorDescriptor;
import jetbrains.mps.lang.editor.cellProviders.SingleRoleCellProvider;
import org.jetbrains.mps.openapi.language.SContainmentLink;
import jetbrains.mps.openapi.editor.menus.transformation.SNodeLocation;
import jetbrains.mps.openapi.editor.cells.CellActionType;
import jetbrains.mps.editor.runtime.impl.cellActions.CellAction_DeleteSmart;
import jetbrains.mps.smodel.action.NodeFactoryManager;
import jetbrains.mps.openapi.editor.cells.DefaultSubstituteInfo;
import jetbrains.mps.nodeEditor.cellMenu.SEmptyContainmentSubstituteInfo;
import jetbrains.mps.nodeEditor.cellMenu.SChildSubstituteInfo;
import no.uio.mLab.shared.editor.DeleteSelf_ActionMap;
import jetbrains.mps.nodeEditor.cells.EditorCell_Property;
import jetbrains.mps.nodeEditor.cells.ModelAccessor;
import jetbrains.mps.util.EqualUtil;
import jetbrains.mps.editor.runtime.cells.EmptyCellAction;
import jetbrains.mps.openapi.editor.style.Style;
import jetbrains.mps.editor.runtime.style.StyleImpl;
import jetbrains.mps.editor.runtime.style.StyleAttributes;
import jetbrains.mps.openapi.editor.style.StyleRegistry;
import jetbrains.mps.nodeEditor.MPSColors;

/*package*/ class NonBooleanDataValueCondition_EditorBuilder_a extends AbstractEditorBuilder {
  @NotNull
  private SNode myNode;

  public NonBooleanDataValueCondition_EditorBuilder_a(@NotNull EditorContext context, @NotNull SNode node) {
    super(context);
    myNode = node;
  }

  @NotNull
  @Override
  public SNode getNode() {
    return myNode;
  }

  /*package*/ EditorCell createCell() {
    return createCollection_0();
  }

  private EditorCell createCollection_0() {
    EditorCell_Collection editorCell = new EditorCell_Collection(getEditorContext(), myNode, new CellLayout_Horizontal());
    editorCell.setCellId("Collection_rvl26_a");
    editorCell.setBig(true);
    setCellContext(editorCell);
    editorCell.addEditorCell(createRefNode_0());
    editorCell.addEditorCell(createRefNode_1());
    if (nodeCondition_rvl26_a2a()) {
      editorCell.addEditorCell(createReadOnlyModelAccessor_0());
    }
    return editorCell;
  }
  private boolean nodeCondition_rvl26_a2a() {
    return (SLinkOperations.getTarget(myNode, MetaAdapterFactory.getContainmentLink(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x15a6b2b9efdf4226L, 0x15a6b2b9f06b6702L, "constraint")) != null) && isNotEmptyString(NonBooleanDataValueCondition__BehaviorDescriptor.getDisplayUnit_id1mAGFBKFiEl.invoke(myNode));
  }
  private EditorCell createRefNode_0() {
    SingleRoleCellProvider provider = new NonBooleanDataValueCondition_EditorBuilder_a.dataSingleRoleHandler_rvl26_a0(myNode, MetaAdapterFactory.getContainmentLink(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x15a6b2b9f078107cL, 0x4dbaf0338d1da5a6L, "data"), getEditorContext());
    return provider.createCell();
  }
  private static class dataSingleRoleHandler_rvl26_a0 extends SingleRoleCellProvider {
    @NotNull
    private SNode myNode;

    public dataSingleRoleHandler_rvl26_a0(SNode ownerNode, SContainmentLink containmentLink, EditorContext context) {
      super(containmentLink, context);
      myNode = ownerNode;
    }

    @Override
    @NotNull
    public SNode getNode() {
      return myNode;
    }

    protected EditorCell createChildCell(SNode child) {
      EditorCell editorCell = getUpdateSession().updateChildNodeCell(child, new SNodeLocation.FromNode(child, MetaAdapterFactory.getConcept(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x15a6b2b9f16a63baL, "no.uio.mLab.decisions.core.structure.NonBooleanDataValue")));
      editorCell.setAction(CellActionType.DELETE, new CellAction_DeleteSmart(getNode(), MetaAdapterFactory.getContainmentLink(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x15a6b2b9f078107cL, 0x4dbaf0338d1da5a6L, "data"), child, MetaAdapterFactory.getConcept(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x15a6b2b9f16a63baL, "no.uio.mLab.decisions.core.structure.NonBooleanDataValue")));
      editorCell.setAction(CellActionType.BACKSPACE, new CellAction_DeleteSmart(getNode(), MetaAdapterFactory.getContainmentLink(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x15a6b2b9f078107cL, 0x4dbaf0338d1da5a6L, "data"), child, MetaAdapterFactory.getConcept(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x15a6b2b9f16a63baL, "no.uio.mLab.decisions.core.structure.NonBooleanDataValue")));
      installCellInfo(child, editorCell, false);
      return editorCell;
    }

    protected SNode createNodeToInsert() {
      return NodeFactoryManager.createNode(MetaAdapterFactory.getConcept(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x15a6b2b9f16a63baL, "no.uio.mLab.decisions.core.structure.NonBooleanDataValue"), null, getNode(), getNode().getModel());
    }


    private void installCellInfo(SNode child, EditorCell editorCell, boolean isEmpty) {
      if (editorCell.getSubstituteInfo() == null || editorCell.getSubstituteInfo() instanceof DefaultSubstituteInfo) {
        editorCell.setSubstituteInfo((isEmpty ? new SEmptyContainmentSubstituteInfo(editorCell) : new SChildSubstituteInfo(editorCell)));
      }
      if (editorCell.getSRole() == null) {
        editorCell.setSRole(MetaAdapterFactory.getContainmentLink(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x15a6b2b9f078107cL, 0x4dbaf0338d1da5a6L, "data"));
      }
      DeleteSelf_ActionMap.setCellActions(editorCell, getNode(), getEditorContext());
    }
    @Override
    protected EditorCell createEmptyCell() {
      getCellFactory().pushCellContext();
      getCellFactory().setNodeLocation(new SNodeLocation.FromParentAndLink(getNode(), MetaAdapterFactory.getContainmentLink(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x15a6b2b9f078107cL, 0x4dbaf0338d1da5a6L, "data"), MetaAdapterFactory.getConcept(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x15a6b2b9f16a63baL, "no.uio.mLab.decisions.core.structure.NonBooleanDataValue")));
      try {
        EditorCell editorCell = super.createEmptyCell();
        editorCell.setCellId("empty_data");
        installCellInfo(null, editorCell, true);
        setCellContext(editorCell);
        return editorCell;
      } finally {
        getCellFactory().popCellContext();
      }
    }
    protected String getNoTargetText() {
      return "<no data>";
    }
  }
  private EditorCell createRefNode_1() {
    SingleRoleCellProvider provider = new NonBooleanDataValueCondition_EditorBuilder_a.constraintSingleRoleHandler_rvl26_b0(myNode, MetaAdapterFactory.getContainmentLink(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x15a6b2b9efdf4226L, 0x15a6b2b9f06b6702L, "constraint"), getEditorContext());
    return provider.createCell();
  }
  private static class constraintSingleRoleHandler_rvl26_b0 extends SingleRoleCellProvider {
    @NotNull
    private SNode myNode;

    public constraintSingleRoleHandler_rvl26_b0(SNode ownerNode, SContainmentLink containmentLink, EditorContext context) {
      super(containmentLink, context);
      myNode = ownerNode;
    }

    @Override
    @NotNull
    public SNode getNode() {
      return myNode;
    }

    protected EditorCell createChildCell(SNode child) {
      EditorCell editorCell = getUpdateSession().updateChildNodeCell(child);
      editorCell.setAction(CellActionType.DELETE, new CellAction_DeleteSmart(getNode(), MetaAdapterFactory.getContainmentLink(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x15a6b2b9efdf4226L, 0x15a6b2b9f06b6702L, "constraint"), child));
      editorCell.setAction(CellActionType.BACKSPACE, new CellAction_DeleteSmart(getNode(), MetaAdapterFactory.getContainmentLink(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x15a6b2b9efdf4226L, 0x15a6b2b9f06b6702L, "constraint"), child));
      installCellInfo(child, editorCell, false);
      return editorCell;
    }



    private void installCellInfo(SNode child, EditorCell editorCell, boolean isEmpty) {
      if (editorCell.getSubstituteInfo() == null || editorCell.getSubstituteInfo() instanceof DefaultSubstituteInfo) {
        editorCell.setSubstituteInfo((isEmpty ? new SEmptyContainmentSubstituteInfo(editorCell) : new SChildSubstituteInfo(editorCell)));
      }
      if (editorCell.getSRole() == null) {
        editorCell.setSRole(MetaAdapterFactory.getContainmentLink(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x15a6b2b9efdf4226L, 0x15a6b2b9f06b6702L, "constraint"));
      }
    }
    @Override
    protected EditorCell createEmptyCell() {
      getCellFactory().pushCellContext();
      getCellFactory().setNodeLocation(new SNodeLocation.FromParentAndLink(getNode(), MetaAdapterFactory.getContainmentLink(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x15a6b2b9efdf4226L, 0x15a6b2b9f06b6702L, "constraint")));
      try {
        EditorCell editorCell = super.createEmptyCell();
        editorCell.setCellId("empty_constraint");
        installCellInfo(null, editorCell, true);
        setCellContext(editorCell);
        return editorCell;
      } finally {
        getCellFactory().popCellContext();
      }
    }
    protected String getNoTargetText() {
      return "<< ... >>";
    }
  }
  private EditorCell createReadOnlyModelAccessor_0() {
    EditorCell_Property editorCell = EditorCell_Property.create(getEditorContext(), new ModelAccessor() {
      public String getText() {
        return (String) NonBooleanDataValueCondition__BehaviorDescriptor.getDisplayUnit_id1mAGFBKFiEl.invoke(myNode);
      }
      public void setText(String s) {
      }
      public boolean isValidText(String s) {
        return EqualUtil.equals(s, getText());
      }
    }, myNode);
    editorCell.setAction(CellActionType.DELETE, EmptyCellAction.getInstance());
    editorCell.setAction(CellActionType.BACKSPACE, EmptyCellAction.getInstance());
    editorCell.setCellId("ReadOnlyModelAccessor_rvl26_c0");
    Style style = new StyleImpl();
    style.set(StyleAttributes.TEXT_COLOR, StyleRegistry.getInstance().getSimpleColor(MPSColors.gray));
    style.set(StyleAttributes.EDITABLE, false);
    editorCell.getStyle().putAll(style);
    DeleteSelf_ActionMap.setCellActions(editorCell, myNode, getEditorContext());
    return editorCell;
  }
  private static boolean isNotEmptyString(String str) {
    return str != null && str.length() > 0;
  }
}
