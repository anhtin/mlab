package no.uio.mLab.decisions.core.intentions;

/*Generated by MPS */

import jetbrains.mps.intentions.AbstractIntentionDescriptor;
import jetbrains.mps.openapi.intentions.IntentionFactory;
import jetbrains.mps.openapi.intentions.Kind;
import jetbrains.mps.smodel.SNodePointer;
import org.jetbrains.mps.openapi.model.SNode;
import jetbrains.mps.openapi.editor.EditorContext;
import java.util.Collection;
import jetbrains.mps.openapi.intentions.IntentionExecutable;
import java.util.List;
import jetbrains.mps.internal.collections.runtime.ListSequence;
import java.util.ArrayList;
import org.jetbrains.mps.openapi.language.SConcept;
import jetbrains.mps.smodel.adapter.structure.MetaAdapterFactory;
import jetbrains.mps.intentions.AbstractIntentionExecutable;
import jetbrains.mps.openapi.intentions.ParameterizedIntentionExecutable;
import jetbrains.mps.lang.smodel.generator.smodelAdapter.SConceptOperations;
import jetbrains.mps.lang.smodel.generator.smodelAdapter.SNodeOperations;
import no.uio.mLab.decisions.core.behavior.Condition__BehaviorDescriptor;
import jetbrains.mps.smodel.action.SNodeFactoryOperations;
import jetbrains.mps.lang.smodel.generator.smodelAdapter.SLinkOperations;
import jetbrains.mps.openapi.intentions.IntentionDescriptor;

public final class SurroundWith_Condition_Intention_Intention extends AbstractIntentionDescriptor implements IntentionFactory {
  public SurroundWith_Condition_Intention_Intention() {
    super(Kind.NORMAL, true, new SNodePointer("r:1cc0fc09-a07c-4fad-b141-cb5e2d2d74ec(no.uio.mLab.decisions.core.intentions)", "1560130832599482204"));
  }
  @Override
  public String getPresentation() {
    return "SurroundWith_Condition_Intention";
  }
  @Override
  public boolean isApplicable(final SNode node, final EditorContext editorContext) {
    return true;
  }
  @Override
  public boolean isSurroundWith() {
    return false;
  }
  public Collection<IntentionExecutable> instances(final SNode node, final EditorContext context) {
    List<IntentionExecutable> list = ListSequence.fromList(new ArrayList<IntentionExecutable>());
    List<SConcept> paramList = parameter(node, context);
    if (paramList != null) {
      for (SConcept param : paramList) {
        ListSequence.fromList(list).addElement(new SurroundWith_Condition_Intention_Intention.IntentionImplementation(param));
      }
    }
    return list;
  }
  private List<SConcept> parameter(final SNode node, final EditorContext editorContext) {
    return ListSequence.fromListAndArray(new ArrayList<SConcept>(), MetaAdapterFactory.getConcept(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x5f0f363900618fe7L, "no.uio.mLab.decisions.core.structure.AllOf"), MetaAdapterFactory.getConcept(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x5f0f363900618fe6L, "no.uio.mLab.decisions.core.structure.AnyOf"));
  }
  /*package*/ final class IntentionImplementation extends AbstractIntentionExecutable implements ParameterizedIntentionExecutable {
    private SConcept myParameter;
    public IntentionImplementation(SConcept parameter) {
      myParameter = parameter;
    }
    @Override
    public String getDescription(final SNode node, final EditorContext editorContext) {
      if (SConceptOperations.isExactly(SNodeOperations.asSConcept(myParameter), MetaAdapterFactory.getConcept(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x5f0f363900618fe7L, "no.uio.mLab.decisions.core.structure.AllOf"))) {
        return Condition__BehaviorDescriptor.getSurroundWithAllOfIntentionDescription_id1mAGFBKcoOC.invoke(SNodeOperations.asSConcept(SNodeOperations.getConcept(node)));
      } else {
        return Condition__BehaviorDescriptor.getSurroundWithAnyOfIntentionDescription_id1mAGFBKcp5c.invoke(SNodeOperations.asSConcept(SNodeOperations.getConcept(node)));
      }
    }
    @Override
    public void execute(final SNode node, final EditorContext editorContext) {
      SNode newNode = SNodeOperations.replaceWithAnother(node, SNodeFactoryOperations.createNewNode(SNodeFactoryOperations.asInstanceConcept(myParameter), null));
      ListSequence.fromList(SLinkOperations.getChildren(newNode, MetaAdapterFactory.getContainmentLink(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x5f0f363900618fe3L, 0x5f0f363900618fe4L, "conditions"))).addElement(node);
    }
    @Override
    public IntentionDescriptor getDescriptor() {
      return SurroundWith_Condition_Intention_Intention.this;
    }
    public Object getParameter() {
      return myParameter;
    }
  }
}
