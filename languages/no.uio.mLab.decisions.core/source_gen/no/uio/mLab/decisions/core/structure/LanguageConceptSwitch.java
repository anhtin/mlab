package no.uio.mLab.decisions.core.structure;

/*Generated by MPS */

import jetbrains.mps.lang.smodel.LanguageConceptIndex;
import jetbrains.mps.lang.smodel.LanguageConceptIndexBuilder;
import jetbrains.mps.smodel.adapter.ids.SConceptId;
import org.jetbrains.mps.openapi.language.SAbstractConcept;

public final class LanguageConceptSwitch {
  private final LanguageConceptIndex myIndex;
  public static final int Action = 0;
  public static final int ActionParameter = 1;
  public static final int AddConjunctivePreConditionChange = 2;
  public static final int AddDisjunctivePreConditionChange = 3;
  public static final int AddRuleChange = 4;
  public static final int Advice = 5;
  public static final int AllOf = 6;
  public static final int AllOfPointcut = 7;
  public static final int AnyOf = 8;
  public static final int AnyOfPointcut = 9;
  public static final int Aspect = 10;
  public static final int AspectOrdering = 11;
  public static final int AspectReference = 12;
  public static final int AspectVariable = 13;
  public static final int AspectVariablePattern = 14;
  public static final int AtomicNumberConstraint = 15;
  public static final int AtomicPointcut = 16;
  public static final int AtomicTimeSpanConstraint = 17;
  public static final int BaseParameter = 18;
  public static final int BooleanDataValue = 19;
  public static final int BooleanDataValueCondition = 20;
  public static final int Change = 21;
  public static final int CompositeCondition = 22;
  public static final int CompositePointcut = 23;
  public static final int Condition = 24;
  public static final int Constraint = 25;
  public static final int ContainingActionPointcut = 26;
  public static final int ContainingBaseParameterPointcut = 27;
  public static final int ContainingDataValuePointcut = 28;
  public static final int DataValue = 29;
  public static final int Day = 30;
  public static final int IAspectPattern = 31;
  public static final int IAspectVariableReference = 32;
  public static final int IBasePattern = 33;
  public static final int IDataValueWithUnit = 34;
  public static final int IEntityReference = 35;
  public static final int IRangeConstraint = 36;
  public static final int IReference = 37;
  public static final int ISelectedEntityPattern = 38;
  public static final int ITranslatableCoreConcept = 39;
  public static final int IWildcardPattern = 40;
  public static final int Literal = 41;
  public static final int Month = 42;
  public static final int NamedCondition = 43;
  public static final int NamedConditionReference = 44;
  public static final int NonBooleanDataValue = 45;
  public static final int NonBooleanDataValueCondition = 46;
  public static final int Not = 47;
  public static final int NumberConstraint = 48;
  public static final int NumberDataValue = 49;
  public static final int NumberEqualTo = 50;
  public static final int NumberGreaterThan = 51;
  public static final int NumberGreaterThanOrEqualTo = 52;
  public static final int NumberInRange = 53;
  public static final int NumberLessThan = 54;
  public static final int NumberLessThanOrEqualTo = 55;
  public static final int NumberLiteral = 56;
  public static final int Pointcut = 57;
  public static final int Reference = 58;
  public static final int Rule = 59;
  public static final int RuleSet = 60;
  public static final int SimpleCondition = 61;
  public static final int TextConstraint = 62;
  public static final int TextDataValue = 63;
  public static final int TextEqualTo = 64;
  public static final int TextLiteral = 65;
  public static final int TimeSpanConstraint = 66;
  public static final int TimeSpanData = 67;
  public static final int TimeSpanGreaterThan = 68;
  public static final int TimeSpanGreaterThanOrEqualTo = 69;
  public static final int TimeSpanInRange = 70;
  public static final int TimeSpanLessThan = 71;
  public static final int TimeSpanLessThanOrEqualTo = 72;
  public static final int TimeSpanLiteral = 73;
  public static final int Week = 74;
  public static final int Year = 75;

  public LanguageConceptSwitch() {
    LanguageConceptIndexBuilder builder = new LanguageConceptIndexBuilder(0xc610a4ebc47c4f95L, 0xb56811236971769cL);
    builder.put(0x15a6b2b9f05ecb5aL, Action);
    builder.put(0x2ad3c27a34ad3b68L, ActionParameter);
    builder.put(0x6511ed2863277770L, AddConjunctivePreConditionChange);
    builder.put(0x2ad3c27a34c30bfcL, AddDisjunctivePreConditionChange);
    builder.put(0xa21b426e7576fabL, AddRuleChange);
    builder.put(0xa21b426e5e15c80L, Advice);
    builder.put(0x5f0f363900618fe7L, AllOf);
    builder.put(0x2ad3c27a35a2861dL, AllOfPointcut);
    builder.put(0x5f0f363900618fe6L, AnyOf);
    builder.put(0x2ad3c27a35bf7feeL, AnyOfPointcut);
    builder.put(0xf35140443a76eeaL, Aspect);
    builder.put(0x1b881273c915c8d0L, AspectOrdering);
    builder.put(0x1b881273c915c8ddL, AspectReference);
    builder.put(0x4dbaf0338da7d6b8L, AspectVariable);
    builder.put(0x6c7943d5b36cf465L, AspectVariablePattern);
    builder.put(0x15a6b2b9ef25063eL, AtomicNumberConstraint);
    builder.put(0x2ad3c27a360c30b5L, AtomicPointcut);
    builder.put(0x15a6b2b9ef5a238cL, AtomicTimeSpanConstraint);
    builder.put(0x2ad3c27a34ad3b5dL, BaseParameter);
    builder.put(0x15a6b2b9f079a4e6L, BooleanDataValue);
    builder.put(0x15a6b2b9efdf4225L, BooleanDataValueCondition);
    builder.put(0xa21b426e7576facL, Change);
    builder.put(0x5f0f363900618fe3L, CompositeCondition);
    builder.put(0x2ad3c27a360c30b6L, CompositePointcut);
    builder.put(0x5f0f363900618fe2L, Condition);
    builder.put(0x15a6b2b9f06b6705L, Constraint);
    builder.put(0x6511ed2864a10a82L, ContainingActionPointcut);
    builder.put(0x2ad3c27a34ad3b5bL, ContainingBaseParameterPointcut);
    builder.put(0x6511ed2862d8a89eL, ContainingDataValuePointcut);
    builder.put(0x15a6b2b9f06b66ffL, DataValue);
    builder.put(0xd694e28d3eca46cL, Day);
    builder.put(0x614e6711f2545d46L, IAspectPattern);
    builder.put(0x6c7943d5b299791dL, IAspectVariableReference);
    builder.put(0x6c7943d5b2724944L, IBasePattern);
    builder.put(0x15a6b2b9f0afde65L, IDataValueWithUnit);
    builder.put(0x6c7943d5b27d9689L, IEntityReference);
    builder.put(0xd694e28d43f2a1bL, IRangeConstraint);
    builder.put(0x6c7943d5b2fcbbb9L, IReference);
    builder.put(0x614e6711f265b51dL, ISelectedEntityPattern);
    builder.put(0x1b6189f12adb87faL, ITranslatableCoreConcept);
    builder.put(0x614e6711f2545a55L, IWildcardPattern);
    builder.put(0x5f0f36390068d4adL, Literal);
    builder.put(0x15a6b2b9ef8b969bL, Month);
    builder.put(0x15a6b2b9ef3a2dedL, NamedCondition);
    builder.put(0x15a6b2b9efc49b11L, NamedConditionReference);
    builder.put(0x15a6b2b9f16a63baL, NonBooleanDataValue);
    builder.put(0x15a6b2b9efdf4226L, NonBooleanDataValueCondition);
    builder.put(0x15a6b2b9efe66450L, Not);
    builder.put(0x15a6b2b9ef258726L, NumberConstraint);
    builder.put(0x15a6b2b9f06b6701L, NumberDataValue);
    builder.put(0x5f0f36390068d48fL, NumberEqualTo);
    builder.put(0x5f0f3639006ecfaaL, NumberGreaterThan);
    builder.put(0x5f0f3639006ecfaeL, NumberGreaterThanOrEqualTo);
    builder.put(0x5f0f3639006f1fbaL, NumberInRange);
    builder.put(0x5f0f3639006ecf61L, NumberLessThan);
    builder.put(0x5f0f3639006ecfabL, NumberLessThanOrEqualTo);
    builder.put(0x5f0f36390068d4aeL, NumberLiteral);
    builder.put(0x2ad3c27a347a5875L, Pointcut);
    builder.put(0x4dbaf0338f7c24acL, Reference);
    builder.put(0x5f0f363900616fd0L, Rule);
    builder.put(0x5f0f363900616fcfL, RuleSet);
    builder.put(0x15a6b2b9f078107cL, SimpleCondition);
    builder.put(0x15a6b2b9f0526e14L, TextConstraint);
    builder.put(0x15a6b2b9f06b6700L, TextDataValue);
    builder.put(0x15a6b2b9f0526e15L, TextEqualTo);
    builder.put(0x5f0f3639007c78b0L, TextLiteral);
    builder.put(0x15a6b2b9ef5755ffL, TimeSpanConstraint);
    builder.put(0x15a6b2b9f07bcb6bL, TimeSpanData);
    builder.put(0xd694e28d42cfa9cL, TimeSpanGreaterThan);
    builder.put(0x15a6b2b9ef5832e3L, TimeSpanGreaterThanOrEqualTo);
    builder.put(0xd694e28d43f2a16L, TimeSpanInRange);
    builder.put(0xd694e28d436f8c2L, TimeSpanLessThan);
    builder.put(0x15a6b2b9ef917b5aL, TimeSpanLessThanOrEqualTo);
    builder.put(0x15a6b2b9ef5832e4L, TimeSpanLiteral);
    builder.put(0x15a6b2b9ef5832e5L, Week);
    builder.put(0xd694e28d3df1dcaL, Year);
    myIndex = builder.seal();
  }

  /*package*/ int index(SConceptId cid) {
    return myIndex.index(cid);
  }

  public int index(SAbstractConcept concept) {
    return myIndex.index(concept);
  }
}
