package no.uio.mLab.decisions.core.typesystem;

/*Generated by MPS */

import jetbrains.mps.lang.typesystem.runtime.AbstractNonTypesystemRule_Runtime;
import jetbrains.mps.lang.typesystem.runtime.NonTypesystemRule_Runtime;
import org.jetbrains.mps.openapi.model.SNode;
import jetbrains.mps.typesystem.inference.TypeCheckingContext;
import jetbrains.mps.lang.typesystem.runtime.IsApplicableStatus;
import jetbrains.mps.lang.smodel.generator.smodelAdapter.SConceptOperations;
import jetbrains.mps.lang.smodel.generator.smodelAdapter.SNodeOperations;
import jetbrains.mps.smodel.adapter.structure.MetaAdapterFactory;
import jetbrains.mps.errors.messageTargets.MessageTarget;
import jetbrains.mps.errors.messageTargets.NodeMessageTarget;
import jetbrains.mps.errors.IErrorReporter;
import org.jetbrains.mps.openapi.language.SAbstractConcept;

public class check_Element_NonTypesystemRule extends AbstractNonTypesystemRule_Runtime implements NonTypesystemRule_Runtime {
  public check_Element_NonTypesystemRule() {
  }
  public void applyRule(final SNode element, final TypeCheckingContext typeCheckingContext, IsApplicableStatus status) {
    if (SConceptOperations.isExactly(SNodeOperations.asSConcept(SNodeOperations.getConcept(element)), MetaAdapterFactory.getConcept(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x2ad3c27a34ad3b5dL, "no.uio.mLab.decisions.core.structure.BaseParameter"))) {
      {
        MessageTarget errorTarget = new NodeMessageTarget();
        IErrorReporter _reporter_2309309498 = typeCheckingContext.reportTypeError(element, "missing element", "r:cd35ac82-689e-45a0-979d-b3548f5d88f1(no.uio.mLab.decisions.core.typesystem)", "3086023999805734423", null, errorTarget);
      }
    }
  }
  public SAbstractConcept getApplicableConcept() {
    return MetaAdapterFactory.getConcept(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x2ad3c27a34ad3b5dL, "no.uio.mLab.decisions.core.structure.BaseParameter");
  }
  public IsApplicableStatus isApplicableAndPattern(SNode argument) {
    return new IsApplicableStatus(argument.getConcept().isSubConceptOf(getApplicableConcept()), null);
  }
  public boolean overrides() {
    return false;
  }
}
