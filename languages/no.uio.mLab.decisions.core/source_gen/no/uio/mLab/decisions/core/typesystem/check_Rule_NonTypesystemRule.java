package no.uio.mLab.decisions.core.typesystem;

/*Generated by MPS */

import jetbrains.mps.lang.typesystem.runtime.AbstractNonTypesystemRule_Runtime;
import jetbrains.mps.lang.typesystem.runtime.NonTypesystemRule_Runtime;
import org.jetbrains.mps.openapi.model.SNode;
import jetbrains.mps.typesystem.inference.TypeCheckingContext;
import jetbrains.mps.lang.typesystem.runtime.IsApplicableStatus;
import jetbrains.mps.lang.smodel.generator.smodelAdapter.SPropertyOperations;
import jetbrains.mps.smodel.adapter.structure.MetaAdapterFactory;
import jetbrains.mps.errors.messageTargets.MessageTarget;
import jetbrains.mps.errors.messageTargets.NodeMessageTarget;
import jetbrains.mps.errors.IErrorReporter;
import no.uio.mLab.decisions.core.behavior.Rule__BehaviorDescriptor;
import jetbrains.mps.lang.smodel.generator.smodelAdapter.SNodeOperations;
import jetbrains.mps.internal.collections.runtime.ListSequence;
import jetbrains.mps.lang.smodel.generator.smodelAdapter.SLinkOperations;
import jetbrains.mps.errors.messageTargets.ReferenceMessageTarget;
import org.jetbrains.mps.openapi.language.SAbstractConcept;

public class check_Rule_NonTypesystemRule extends AbstractNonTypesystemRule_Runtime implements NonTypesystemRule_Runtime {
  public check_Rule_NonTypesystemRule() {
  }
  public void applyRule(final SNode rule, final TypeCheckingContext typeCheckingContext, IsApplicableStatus status) {
    if (isEmptyString(SPropertyOperations.getString(rule, MetaAdapterFactory.getProperty(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x110396eaaa4L, 0x110396ec041L, "name")))) {
      {
        MessageTarget errorTarget = new NodeMessageTarget();
        IErrorReporter _reporter_2309309498 = typeCheckingContext.reportTypeError(rule, Rule__BehaviorDescriptor.getMissingNameError_id2XLt5KUB66k.invoke(SNodeOperations.asSConcept(SNodeOperations.getConcept(rule))), "r:cd35ac82-689e-45a0-979d-b3548f5d88f1(no.uio.mLab.decisions.core.typesystem)", "3418641531620077902", null, errorTarget);
      }
    }
    if (ListSequence.fromList(SLinkOperations.getChildren(rule, MetaAdapterFactory.getContainmentLink(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x5f0f363900616fd0L, 0x15a6b2b9f127f734L, "actions"))).isEmpty()) {
      {
        MessageTarget errorTarget = new NodeMessageTarget();
        errorTarget = new ReferenceMessageTarget("actions");
        IErrorReporter _reporter_2309309498 = typeCheckingContext.reportTypeError(rule, Rule__BehaviorDescriptor.getMissingActionsError_id2XLt5KU_rbM.invoke(SNodeOperations.asSConcept(SNodeOperations.getConcept(rule))), "r:cd35ac82-689e-45a0-979d-b3548f5d88f1(no.uio.mLab.decisions.core.typesystem)", "3418641531619691874", null, errorTarget);
      }
    }
  }
  public SAbstractConcept getApplicableConcept() {
    return MetaAdapterFactory.getConcept(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x5f0f363900616fd0L, "no.uio.mLab.decisions.core.structure.Rule");
  }
  public IsApplicableStatus isApplicableAndPattern(SNode argument) {
    return new IsApplicableStatus(argument.getConcept().isSubConceptOf(getApplicableConcept()), null);
  }
  public boolean overrides() {
    return false;
  }
  private static boolean isEmptyString(String str) {
    return str == null || str.length() == 0;
  }
}
