<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:68a64cb9-a933-40d7-adbd-50f52a94a845(main@generator)">
  <persistence version="9" />
  <languages>
    <devkit ref="a2eb3a43-fcc2-4200-80dc-c60110c4862d(jetbrains.mps.devkit.templates)" />
  </languages>
  <imports>
    <import index="ydq9" ref="r:1329ca29-1e66-45a8-882e-5bcc59a0ad4c(no.uio.mLab.decisions.data.laboratoryTest.numberResult.structure)" />
    <import index="o86p" ref="r:aed49bfb-d506-499d-a596-1865d8a4c972(no.uio.mLab.decisions.data.laboratoryTest.numberResult.runtime)" />
    <import index="aia3" ref="r:856e974b-6e42-49cc-99cd-c3adb4161c21(no.uio.mLab.decisions.data.laboratoryTest.structure)" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1070534058343" name="jetbrains.mps.baseLanguage.structure.NullLiteral" flags="nn" index="10Nm6u" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1212685548494" name="jetbrains.mps.baseLanguage.structure.ClassCreator" flags="nn" index="1pGfFk" />
    </language>
    <language id="b401a680-8325-4110-8fd3-84331ff25bef" name="jetbrains.mps.lang.generator">
      <concept id="1114706874351" name="jetbrains.mps.lang.generator.structure.CopySrcNodeMacro" flags="ln" index="29HgVG">
        <child id="1168024447342" name="sourceNodeQuery" index="3NFExx" />
      </concept>
      <concept id="1095416546421" name="jetbrains.mps.lang.generator.structure.MappingConfiguration" flags="ig" index="bUwia">
        <child id="1167328349397" name="reductionMappingRule" index="3acgRq" />
        <child id="1195502100749" name="preMappingScript" index="1puA0r" />
      </concept>
      <concept id="1177093525992" name="jetbrains.mps.lang.generator.structure.InlineTemplate_RuleConsequence" flags="lg" index="gft3U">
        <child id="1177093586806" name="templateNode" index="gfFT$" />
      </concept>
      <concept id="1167169188348" name="jetbrains.mps.lang.generator.structure.TemplateFunctionParameter_sourceNode" flags="nn" index="30H73N" />
      <concept id="1167169308231" name="jetbrains.mps.lang.generator.structure.BaseMappingRule" flags="ng" index="30H$t8">
        <reference id="1167169349424" name="applicableConcept" index="30HIoZ" />
      </concept>
      <concept id="1167327847730" name="jetbrains.mps.lang.generator.structure.Reduction_MappingRule" flags="lg" index="3aamgX">
        <child id="1169672767469" name="ruleConsequence" index="1lVwrX" />
      </concept>
      <concept id="1195499912406" name="jetbrains.mps.lang.generator.structure.MappingScript" flags="lg" index="1pmfR0">
        <property id="1195595592106" name="scriptKind" index="1v3f2W" />
        <child id="1195501105008" name="codeBlock" index="1pqMTA" />
      </concept>
      <concept id="1195500722856" name="jetbrains.mps.lang.generator.structure.MappingScript_CodeBlock" flags="in" index="1pplIY" />
      <concept id="1195502151594" name="jetbrains.mps.lang.generator.structure.MappingScriptReference" flags="lg" index="1puMqW">
        <reference id="1195502167610" name="mappingScript" index="1puQsG" />
      </concept>
      <concept id="1167945743726" name="jetbrains.mps.lang.generator.structure.IfMacro_Condition" flags="in" index="3IZrLx" />
      <concept id="1168024337012" name="jetbrains.mps.lang.generator.structure.SourceSubstituteMacro_SourceNodeQuery" flags="in" index="3NFfHV" />
      <concept id="1118773211870" name="jetbrains.mps.lang.generator.structure.IfMacro" flags="ln" index="1W57fq">
        <child id="1194989344771" name="alternativeConsequence" index="UU_$l" />
        <child id="1167945861827" name="conditionFunction" index="3IZSJc" />
      </concept>
    </language>
    <language id="760a0a8c-eabb-4521-8bfd-65db761a9ba3" name="jetbrains.mps.baseLanguage.logging">
      <concept id="6332851714983831325" name="jetbrains.mps.baseLanguage.logging.structure.MsgStatement" flags="ng" index="2xdQw9">
        <property id="6332851714983843871" name="severity" index="2xdLsb" />
        <child id="5721587534047265374" name="message" index="9lYJi" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1172008320231" name="jetbrains.mps.lang.smodel.structure.Node_IsNotNullOperation" flags="nn" index="3x8VRR" />
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="bUwia" id="4V3GMfXsC5U">
    <property role="TrG5h" value="dataLaboratoryTestNumberResultMain" />
    <node concept="1puMqW" id="7lYCqhudl2K" role="1puA0r">
      <ref role="1puQsG" node="7lYCqhudl2O" resolve="dataLaboratoryTestNumberResultScript" />
    </node>
    <node concept="3aamgX" id="4B5aqq4gM_c" role="3acgRq">
      <ref role="30HIoZ" to="ydq9:4B5aqq4g6Nf" resolve="LaboratoryTestPreviousResultAsNumber" />
      <node concept="gft3U" id="4B5aqq4gM_I" role="1lVwrX">
        <node concept="2ShNRf" id="4B5aqq4gM_O" role="gfFT$">
          <node concept="1pGfFk" id="4B5aqq4gM_P" role="2ShVmc">
            <ref role="37wK5l" to="o86p:1mAGFBLsNvu" resolve="TestPreviousResultAsNumber" />
            <node concept="Xl_RD" id="4B5aqq4gM_Q" role="37wK5m">
              <property role="Xl_RC" value="test" />
              <node concept="29HgVG" id="4B5aqq4gM_R" role="lGtFl">
                <node concept="3NFfHV" id="4B5aqq4gM_S" role="3NFExx">
                  <node concept="3clFbS" id="4B5aqq4gM_T" role="2VODD2">
                    <node concept="3clFbF" id="4B5aqq4gM_U" role="3cqZAp">
                      <node concept="2OqwBi" id="4B5aqq4gM_V" role="3clFbG">
                        <node concept="3TrEf2" id="1I84Bf76DnA" role="2OqNvi">
                          <ref role="3Tt5mk" to="ydq9:6LTgXmMj7Lm" resolve="testReference" />
                        </node>
                        <node concept="30H73N" id="4B5aqq4gM_X" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="Xl_RD" id="4B5aqq4gMHs" role="37wK5m">
              <property role="Xl_RC" value="specimen" />
              <node concept="1W57fq" id="6LTgXmMrZAc" role="lGtFl">
                <node concept="3IZrLx" id="6LTgXmMrZAd" role="3IZSJc">
                  <node concept="3clFbS" id="6LTgXmMrZAe" role="2VODD2">
                    <node concept="3clFbF" id="6LTgXmMrZQk" role="3cqZAp">
                      <node concept="2OqwBi" id="6LTgXmMs2ix" role="3clFbG">
                        <node concept="2OqwBi" id="6LTgXmMs0fq" role="2Oq$k0">
                          <node concept="30H73N" id="6LTgXmMrZQj" role="2Oq$k0" />
                          <node concept="3TrEf2" id="1I84Bf76EqJ" role="2OqNvi">
                            <ref role="3Tt5mk" to="ydq9:4B5aqq3QF7a" resolve="specimenTypeReference" />
                          </node>
                        </node>
                        <node concept="3x8VRR" id="6LTgXmMs2JS" role="2OqNvi" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="gft3U" id="6LTgXmMs2Xk" role="UU_$l">
                  <node concept="10Nm6u" id="6LTgXmMs3fI" role="gfFT$" />
                </node>
              </node>
              <node concept="29HgVG" id="4B5aqq4gMVN" role="lGtFl">
                <node concept="3NFfHV" id="4B5aqq4gMVO" role="3NFExx">
                  <node concept="3clFbS" id="4B5aqq4gMVP" role="2VODD2">
                    <node concept="3clFbF" id="4B5aqq4gMVV" role="3cqZAp">
                      <node concept="2OqwBi" id="4B5aqq4gMVQ" role="3clFbG">
                        <node concept="3TrEf2" id="1I84Bf76DWQ" role="2OqNvi">
                          <ref role="3Tt5mk" to="ydq9:4B5aqq3QF7a" resolve="specimenTypeReference" />
                        </node>
                        <node concept="30H73N" id="4B5aqq4gMVU" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="4B5aqq4gN3Z" role="3acgRq">
      <ref role="30HIoZ" to="ydq9:4B5aqq4g6Nj" resolve="LaboratoryTestResultAsNumber" />
      <node concept="gft3U" id="4B5aqq4gN4R" role="1lVwrX">
        <node concept="2ShNRf" id="4B5aqq4gN4X" role="gfFT$">
          <node concept="1pGfFk" id="4B5aqq4gN4Y" role="2ShVmc">
            <ref role="37wK5l" to="o86p:1mAGFBLnVbo" resolve="TestResultAsNumber" />
            <node concept="Xl_RD" id="1I84Bf76FyT" role="37wK5m">
              <property role="Xl_RC" value="test" />
              <node concept="29HgVG" id="1I84Bf76FyU" role="lGtFl">
                <node concept="3NFfHV" id="1I84Bf76FyV" role="3NFExx">
                  <node concept="3clFbS" id="1I84Bf76FyW" role="2VODD2">
                    <node concept="3clFbF" id="1I84Bf76FyX" role="3cqZAp">
                      <node concept="2OqwBi" id="1I84Bf76FyY" role="3clFbG">
                        <node concept="3TrEf2" id="1I84Bf76FyZ" role="2OqNvi">
                          <ref role="3Tt5mk" to="ydq9:6LTgXmMj7Lm" resolve="testReference" />
                        </node>
                        <node concept="30H73N" id="1I84Bf76Fz0" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="Xl_RD" id="1I84Bf76GhA" role="37wK5m">
              <property role="Xl_RC" value="specimen" />
              <node concept="1W57fq" id="1I84Bf76GhB" role="lGtFl">
                <node concept="3IZrLx" id="1I84Bf76GhC" role="3IZSJc">
                  <node concept="3clFbS" id="1I84Bf76GhD" role="2VODD2">
                    <node concept="3clFbF" id="1I84Bf76GhE" role="3cqZAp">
                      <node concept="2OqwBi" id="1I84Bf76GhF" role="3clFbG">
                        <node concept="2OqwBi" id="1I84Bf76GhG" role="2Oq$k0">
                          <node concept="30H73N" id="1I84Bf76GhH" role="2Oq$k0" />
                          <node concept="3TrEf2" id="1I84Bf76GhI" role="2OqNvi">
                            <ref role="3Tt5mk" to="ydq9:4B5aqq3QF7a" resolve="specimenTypeReference" />
                          </node>
                        </node>
                        <node concept="3x8VRR" id="1I84Bf76GhJ" role="2OqNvi" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="gft3U" id="1I84Bf76GhK" role="UU_$l">
                  <node concept="10Nm6u" id="1I84Bf76GhL" role="gfFT$" />
                </node>
              </node>
              <node concept="29HgVG" id="1I84Bf76GhM" role="lGtFl">
                <node concept="3NFfHV" id="1I84Bf76GhN" role="3NFExx">
                  <node concept="3clFbS" id="1I84Bf76GhO" role="2VODD2">
                    <node concept="3clFbF" id="1I84Bf76GhP" role="3cqZAp">
                      <node concept="2OqwBi" id="1I84Bf76GhQ" role="3clFbG">
                        <node concept="3TrEf2" id="1I84Bf76GhR" role="2OqNvi">
                          <ref role="3Tt5mk" to="ydq9:4B5aqq3QF7a" resolve="specimenTypeReference" />
                        </node>
                        <node concept="30H73N" id="1I84Bf76GhS" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="PDjyzkyUgT" role="3acgRq">
      <ref role="30HIoZ" to="ydq9:PDjyzkxNr$" resolve="LaboratoryTestHasResultAsNumber" />
      <node concept="gft3U" id="PDjyzkyUim" role="1lVwrX">
        <node concept="2ShNRf" id="PDjyzkyUis" role="gfFT$">
          <node concept="1pGfFk" id="PDjyzkyUlx" role="2ShVmc">
            <ref role="37wK5l" to="o86p:PDjyzkyPDR" resolve="TestResultHasNumberValue" />
            <node concept="Xl_RD" id="1I84Bf76FQK" role="37wK5m">
              <property role="Xl_RC" value="test" />
              <node concept="29HgVG" id="1I84Bf76FQL" role="lGtFl">
                <node concept="3NFfHV" id="1I84Bf76FQM" role="3NFExx">
                  <node concept="3clFbS" id="1I84Bf76FQN" role="2VODD2">
                    <node concept="3clFbF" id="1I84Bf76FQO" role="3cqZAp">
                      <node concept="2OqwBi" id="1I84Bf76FQP" role="3clFbG">
                        <node concept="3TrEf2" id="1I84Bf76FQQ" role="2OqNvi">
                          <ref role="3Tt5mk" to="ydq9:6LTgXmMj7Lm" resolve="testReference" />
                        </node>
                        <node concept="30H73N" id="1I84Bf76FQR" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="Xl_RD" id="1I84Bf76GGJ" role="37wK5m">
              <property role="Xl_RC" value="specimen" />
              <node concept="1W57fq" id="1I84Bf76GGK" role="lGtFl">
                <node concept="3IZrLx" id="1I84Bf76GGL" role="3IZSJc">
                  <node concept="3clFbS" id="1I84Bf76GGM" role="2VODD2">
                    <node concept="3clFbF" id="1I84Bf76GGN" role="3cqZAp">
                      <node concept="2OqwBi" id="1I84Bf76GGO" role="3clFbG">
                        <node concept="2OqwBi" id="1I84Bf76GGP" role="2Oq$k0">
                          <node concept="30H73N" id="1I84Bf76GGQ" role="2Oq$k0" />
                          <node concept="3TrEf2" id="1I84Bf76GGR" role="2OqNvi">
                            <ref role="3Tt5mk" to="ydq9:4B5aqq3QF7a" resolve="specimenTypeReference" />
                          </node>
                        </node>
                        <node concept="3x8VRR" id="1I84Bf76GGS" role="2OqNvi" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="gft3U" id="1I84Bf76GGT" role="UU_$l">
                  <node concept="10Nm6u" id="1I84Bf76GGU" role="gfFT$" />
                </node>
              </node>
              <node concept="29HgVG" id="1I84Bf76GGV" role="lGtFl">
                <node concept="3NFfHV" id="1I84Bf76GGW" role="3NFExx">
                  <node concept="3clFbS" id="1I84Bf76GGX" role="2VODD2">
                    <node concept="3clFbF" id="1I84Bf76GGY" role="3cqZAp">
                      <node concept="2OqwBi" id="1I84Bf76GGZ" role="3clFbG">
                        <node concept="3TrEf2" id="1I84Bf76GH0" role="2OqNvi">
                          <ref role="3Tt5mk" to="ydq9:4B5aqq3QF7a" resolve="specimenTypeReference" />
                        </node>
                        <node concept="30H73N" id="1I84Bf76GH1" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="1pmfR0" id="7lYCqhudl2O">
    <property role="TrG5h" value="dataLaboratoryTestNumberResultScript" />
    <property role="1v3f2W" value="pre_processing" />
    <node concept="1pplIY" id="7lYCqhudl2P" role="1pqMTA">
      <node concept="3clFbS" id="7lYCqhudl2Q" role="2VODD2">
        <node concept="2xdQw9" id="7lYCqhudl31" role="3cqZAp">
          <property role="2xdLsb" value="debug" />
          <node concept="Xl_RD" id="7lYCqhudl33" role="9lYJi">
            <property role="Xl_RC" value="data.laboratoryTests.numberResult" />
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

