<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:1329ca29-1e66-45a8-882e-5bcc59a0ad4c(no.uio.mLab.decisions.data.laboratoryTest.numberResult.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="6" />
    <devkit ref="78434eb8-b0e5-444b-850d-e7c4ad2da9ab(jetbrains.mps.devkit.aspect.structure)" />
  </languages>
  <imports>
    <import index="7f9y" ref="r:790ac37e-42d3-4dd3-977c-e8fe9cc56a45(no.uio.mLab.decisions.core.structure)" />
    <import index="ruww" ref="r:d7787915-5b9a-4713-a6ab-6345c5e1ec03(no.uio.mLab.decisions.references.laboratoryTest.structure)" />
    <import index="vbok" ref="r:6cc208d3-a370-49ca-a6ac-ded97805a924(no.uio.mLab.shared.structure)" />
    <import index="aia3" ref="r:856e974b-6e42-49cc-99cd-c3adb4161c21(no.uio.mLab.decisions.data.laboratoryTest.structure)" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <property id="4628067390765956807" name="final" index="R5$K2" />
        <property id="4628067390765956802" name="abstract" index="R5$K7" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
      </concept>
      <concept id="1169125989551" name="jetbrains.mps.lang.structure.structure.InterfaceConceptDeclaration" flags="ig" index="PlHQZ">
        <child id="1169127546356" name="extends" index="PrDN$" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="PlHQZ" id="4V3GMfXwlp$">
    <property role="EcuMT" value="5675576922575296100" />
    <property role="TrG5h" value="ITranslatableNumberResultLaboratoryTestDataConcept" />
    <property role="3GE5qa" value="shared" />
    <node concept="PrWs8" id="4V3GMfXwoSi" role="PrDN$">
      <ref role="PrY4T" to="vbok:5Wfdz$0vc1Z" resolve="ITranslatableConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="4B5aqq4g6Nf">
    <property role="EcuMT" value="5315700730340863183" />
    <property role="TrG5h" value="LaboratoryTestPreviousResultAsNumber" />
    <property role="3GE5qa" value="base.data.value" />
    <ref role="1TJDcQ" to="7f9y:1mAGFBKqQs1" resolve="NumberDataValue" />
    <node concept="PrWs8" id="4B5aqq4g6Ng" role="PzmwI">
      <ref role="PrY4T" node="4V3GMfXwlp$" resolve="ITranslatableNumberResultLaboratoryTestDataConcept" />
    </node>
    <node concept="PrWs8" id="4B5aqq4WpL5" role="PzmwI">
      <ref role="PrY4T" node="4B5aqq3QF78" resolve="ILaboratoryTestWithNumberResultDataValue" />
    </node>
    <node concept="PrWs8" id="4B5aqq4g6Ni" role="PzmwI">
      <ref role="PrY4T" to="7f9y:1mAGFBKFXT_" resolve="IDataValueWithUnit" />
    </node>
  </node>
  <node concept="1TIwiD" id="4B5aqq4g6Nj">
    <property role="EcuMT" value="5315700730340863187" />
    <property role="TrG5h" value="LaboratoryTestResultAsNumber" />
    <property role="R5$K7" value="false" />
    <property role="R5$K2" value="false" />
    <property role="3GE5qa" value="base.data.value" />
    <ref role="1TJDcQ" to="7f9y:1mAGFBKqQs1" resolve="NumberDataValue" />
    <node concept="PrWs8" id="4B5aqq4g6Nk" role="PzmwI">
      <ref role="PrY4T" node="4V3GMfXwlp$" resolve="ITranslatableNumberResultLaboratoryTestDataConcept" />
    </node>
    <node concept="PrWs8" id="4B5aqq4WpL0" role="PzmwI">
      <ref role="PrY4T" node="4B5aqq3QF78" resolve="ILaboratoryTestWithNumberResultDataValue" />
    </node>
    <node concept="PrWs8" id="4B5aqq4g6Nm" role="PzmwI">
      <ref role="PrY4T" to="7f9y:1mAGFBKFXT_" resolve="IDataValueWithUnit" />
    </node>
  </node>
  <node concept="PlHQZ" id="4B5aqq3QF78">
    <property role="EcuMT" value="5315700730334196168" />
    <property role="3GE5qa" value="base.data" />
    <property role="TrG5h" value="ILaboratoryTestWithNumberResultDataValue" />
    <node concept="1TJgyj" id="6LTgXmMj7Lm" role="1TKVEi">
      <property role="IQ2ns" value="7816353213376658518" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="testReference" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="ruww:4V3GMfXHy69" resolve="LaboratoryTestWithNumberResultReference" />
    </node>
    <node concept="1TJgyj" id="4B5aqq3QF7a" role="1TKVEi">
      <property role="IQ2ns" value="5315700730334196170" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="specimenTypeReference" />
      <property role="20lbJX" value="0..1" />
      <ref role="20lvS9" to="ruww:6khVixyauId" resolve="SpecimenTypeReference" />
    </node>
    <node concept="PrWs8" id="6LTgXmMq6$A" role="PrDN$">
      <ref role="PrY4T" node="4V3GMfXwlp$" resolve="ITranslatableNumberResultLaboratoryTestDataConcept" />
    </node>
    <node concept="PrWs8" id="1I84Bf76ttS" role="PrDN$">
      <ref role="PrY4T" to="7f9y:6LTgXmMs$_4" resolve="IPattern" />
    </node>
  </node>
  <node concept="1TIwiD" id="PDjyzkxNr$">
    <property role="EcuMT" value="966389532318971620" />
    <property role="3GE5qa" value="base.data.boolean" />
    <property role="TrG5h" value="LaboratoryTestHasResultAsNumber" />
    <ref role="1TJDcQ" to="7f9y:1mAGFBKuqjA" resolve="BooleanDataValue" />
    <node concept="PrWs8" id="PDjyzkxPhe" role="PzmwI">
      <ref role="PrY4T" node="4B5aqq3QF78" resolve="ILaboratoryTestWithNumberResultDataValue" />
    </node>
  </node>
</model>

