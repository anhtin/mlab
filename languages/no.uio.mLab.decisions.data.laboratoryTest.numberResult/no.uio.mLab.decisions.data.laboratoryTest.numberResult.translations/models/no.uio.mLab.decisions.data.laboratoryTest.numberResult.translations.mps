<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:167cd572-976c-4f6c-bdfb-f5acd43e7bff(no.uio.mLab.decisions.data.laboratoryTest.numberResult.translations)">
  <persistence version="9" />
  <languages>
    <use id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core" version="1" />
    <use id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage" version="6" />
  </languages>
  <imports>
    <import index="9igz" ref="r:0ba33904-c124-449b-bde4-640f55a71953(no.uio.mLab.shared.translations)" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1465982738277781862" name="jetbrains.mps.baseLanguage.structure.PlaceholderMember" flags="ng" index="2tJIrI" />
      <concept id="1188207840427" name="jetbrains.mps.baseLanguage.structure.AnnotationInstance" flags="nn" index="2AHcQZ">
        <reference id="1188208074048" name="annotation" index="2AI5Lk" />
      </concept>
      <concept id="1188208481402" name="jetbrains.mps.baseLanguage.structure.HasAnnotation" flags="ng" index="2AJDlI">
        <child id="1188208488637" name="annotation" index="2AJF6D" />
      </concept>
      <concept id="2820489544401957797" name="jetbrains.mps.baseLanguage.structure.DefaultClassCreator" flags="nn" index="HV5vD">
        <reference id="2820489544401957798" name="classifier" index="HV5vE" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1070462154015" name="jetbrains.mps.baseLanguage.structure.StaticFieldDeclaration" flags="ig" index="Wx3nA">
        <property id="6468716278899126575" name="isVolatile" index="2dlcS1" />
        <property id="6468716278899125786" name="isTransient" index="2dld4O" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1081236700938" name="jetbrains.mps.baseLanguage.structure.StaticMethodDeclaration" flags="ig" index="2YIFZL" />
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu">
        <child id="1095933932569" name="implementedInterface" index="EKbjA" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <property id="1176718929932" name="isFinal" index="3TUv4t" />
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <property id="4276006055363816570" name="isSynchronized" index="od$2w" />
        <property id="1181808852946" name="isFinal" index="DiZV1" />
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123165" name="jetbrains.mps.baseLanguage.structure.InstanceMethodDeclaration" flags="ig" index="3clFb_">
        <property id="1178608670077" name="isAbstract" index="1EzhhJ" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242875" name="jetbrains.mps.baseLanguage.structure.PlusExpression" flags="nn" index="3cpWs3" />
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1107461130800" name="jetbrains.mps.baseLanguage.structure.Classifier" flags="ng" index="3pOWGL">
        <child id="5375687026011219971" name="member" index="jymVt" unordered="true" />
      </concept>
      <concept id="7812454656619025416" name="jetbrains.mps.baseLanguage.structure.MethodDeclaration" flags="ng" index="1rXfSm">
        <property id="8355037393041754995" name="isNative" index="2aFKle" />
      </concept>
      <concept id="7812454656619025412" name="jetbrains.mps.baseLanguage.structure.LocalMethodCall" flags="nn" index="1rXfSq" />
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1107796713796" name="jetbrains.mps.baseLanguage.structure.Interface" flags="ig" index="3HP615" />
      <concept id="1163670490218" name="jetbrains.mps.baseLanguage.structure.SwitchStatement" flags="nn" index="3KaCP$">
        <child id="1163670592366" name="defaultBlock" index="3Kb1Dw" />
        <child id="1163670766145" name="expression" index="3KbGdf" />
        <child id="1163670772911" name="case" index="3KbHQx" />
      </concept>
      <concept id="1163670641947" name="jetbrains.mps.baseLanguage.structure.SwitchCase" flags="ng" index="3KbdKl">
        <child id="1163670677455" name="expression" index="3Kbmr1" />
        <child id="1163670683720" name="body" index="3Kbo56" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
      <concept id="1146644623116" name="jetbrains.mps.baseLanguage.structure.PrivateVisibility" flags="nn" index="3Tm6S6" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="312cEu" id="4zMac8rUNsN">
    <property role="TrG5h" value="NumberResultLaboratoryTestDataTranslationProvider" />
    <property role="3GE5qa" value="internationalization" />
    <node concept="Wx3nA" id="4zMac8rVBeK" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="TrG5h" value="displayTranslations" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm1VV" id="4zMac8rVBei" role="1B3o_S" />
      <node concept="3uibUv" id="4zMac8rVBeC" role="1tU5fm">
        <ref role="3uigEE" node="4zMac8rUNtP" resolve="INumberResultLaboratoryTestDataTranslations" />
      </node>
      <node concept="1rXfSq" id="4zMac8rVBfh" role="33vP2m">
        <ref role="37wK5l" node="4zMac8rUNuE" resolve="init" />
        <node concept="10M0yZ" id="ofcHXfTMGc" role="37wK5m">
          <ref role="3cqZAo" to="9igz:5fXsrCVmzVm" resolve="displayLanguage" />
          <ref role="1PxDUh" to="9igz:5fXsrCVmxTN" resolve="TranslationConfiguration" />
        </node>
      </node>
    </node>
    <node concept="Wx3nA" id="1Hxyv4DUmfq" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="TrG5h" value="generationTranslations" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm1VV" id="1Hxyv4DUmfr" role="1B3o_S" />
      <node concept="3uibUv" id="1Hxyv4DUmfs" role="1tU5fm">
        <ref role="3uigEE" node="4zMac8rUNtP" resolve="INumberResultLaboratoryTestDataTranslations" />
      </node>
      <node concept="1rXfSq" id="1Hxyv4DUmft" role="33vP2m">
        <ref role="37wK5l" node="4zMac8rUNuE" resolve="init" />
        <node concept="10M0yZ" id="ofcHXfTMH1" role="37wK5m">
          <ref role="3cqZAo" to="9igz:5fXsrCVmzVN" resolve="generationLanguage" />
          <ref role="1PxDUh" to="9igz:5fXsrCVmxTN" resolve="TranslationConfiguration" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="4zMac8rUNtp" role="jymVt" />
    <node concept="2YIFZL" id="4zMac8rUNuE" role="jymVt">
      <property role="TrG5h" value="init" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="4zMac8rUNuH" role="3clF47">
        <node concept="3KaCP$" id="4zMac8rUNv7" role="3cqZAp">
          <node concept="3KbdKl" id="4zMac8rUNXu" role="3KbHQx">
            <node concept="Xl_RD" id="4zMac8rUNXU" role="3Kbmr1">
              <property role="Xl_RC" value="no" />
            </node>
            <node concept="3clFbS" id="4zMac8rUNXw" role="3Kbo56">
              <node concept="3cpWs6" id="4zMac8rUNYt" role="3cqZAp">
                <node concept="2ShNRf" id="4zMac8rUO3y" role="3cqZAk">
                  <node concept="HV5vD" id="4zMac8rUP5z" role="2ShVmc">
                    <ref role="HV5vE" node="4zMac8rUO1O" resolve="NoNumberResultLaboratoryTestDataTranslations" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="4zMac8rUNYX" role="3Kb1Dw">
            <node concept="3cpWs6" id="4zMac8rUNZC" role="3cqZAp">
              <node concept="2ShNRf" id="4zMac8rUP6U" role="3cqZAk">
                <node concept="HV5vD" id="4zMac8rUPap" role="2ShVmc">
                  <ref role="HV5vE" node="4zMac8rUO0_" resolve="EnNumberResultLaboratoryTestDataTranslations" />
                </node>
              </node>
            </node>
          </node>
          <node concept="37vLTw" id="1Hxyv4DUmde" role="3KbGdf">
            <ref role="3cqZAo" node="1Hxyv4DUm7P" resolve="language" />
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="4zMac8rW809" role="1B3o_S" />
      <node concept="3uibUv" id="4zMac8rUNuz" role="3clF45">
        <ref role="3uigEE" node="4zMac8rUNtP" resolve="INumberResultLaboratoryTestDataTranslations" />
      </node>
      <node concept="37vLTG" id="1Hxyv4DUm7P" role="3clF46">
        <property role="TrG5h" value="language" />
        <node concept="17QB3L" id="1Hxyv4DUm7O" role="1tU5fm" />
      </node>
    </node>
    <node concept="3Tm1VV" id="4zMac8rUNsO" role="1B3o_S" />
  </node>
  <node concept="3HP615" id="4zMac8rUNtP">
    <property role="3GE5qa" value="internationalization" />
    <property role="TrG5h" value="INumberResultLaboratoryTestDataTranslations" />
    <node concept="3clFb_" id="6LTgXmMhnrg" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getILaboratoryTestWithNumberResultDataValueIncludeSpecimenTypeIntentionDescription" />
      <node concept="3clFbS" id="6LTgXmMhnrj" role="3clF47" />
      <node concept="3Tm1VV" id="6LTgXmMhnrk" role="1B3o_S" />
      <node concept="17QB3L" id="6LTgXmMhnk9" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="6LTgXmNf4h0" role="jymVt" />
    <node concept="3clFb_" id="4V3GMfXwkrq" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultAsNumberAlias" />
      <node concept="3clFbS" id="4V3GMfXwkrt" role="3clF47" />
      <node concept="3Tm1VV" id="4V3GMfXwkru" role="1B3o_S" />
      <node concept="17QB3L" id="4V3GMfXwkrm" role="3clF45" />
    </node>
    <node concept="3clFb_" id="4V3GMfXwkse" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultAsNumberDescription" />
      <node concept="3clFbS" id="4V3GMfXwksh" role="3clF47" />
      <node concept="3Tm1VV" id="4V3GMfXwksi" role="1B3o_S" />
      <node concept="17QB3L" id="4V3GMfXwkrX" role="3clF45" />
    </node>
    <node concept="3clFb_" id="4B5aqq4g6Ps" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultWithSpecimenAsNumberAlias" />
      <node concept="3clFbS" id="4B5aqq4g6Pt" role="3clF47" />
      <node concept="3Tm1VV" id="4B5aqq4g6Pu" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4g6Pv" role="3clF45" />
    </node>
    <node concept="3clFb_" id="4B5aqq4g6Pw" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultWithSpecimenAsNumberDescription" />
      <node concept="3clFbS" id="4B5aqq4g6Px" role="3clF47" />
      <node concept="3Tm1VV" id="4B5aqq4g6Py" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4g6Pz" role="3clF45" />
    </node>
    <node concept="3clFb_" id="4B5aqq4g79c" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultWithSpecimenAsNumberKeywordNumberResult" />
      <node concept="3clFbS" id="4B5aqq4g79d" role="3clF47" />
      <node concept="3Tm1VV" id="4B5aqq4g79e" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4g79f" role="3clF45" />
    </node>
    <node concept="3clFb_" id="4B5aqq4g79g" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultWithSpecimenAsNumberKeywordWithSpecimen" />
      <node concept="3clFbS" id="4B5aqq4g79h" role="3clF47" />
      <node concept="3Tm1VV" id="4B5aqq4g79i" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4g79j" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="4V3GMfXwGwq" role="jymVt" />
    <node concept="3clFb_" id="1mAGFBJlpfv" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultAsNumberAlias" />
      <node concept="3clFbS" id="1mAGFBJlpfy" role="3clF47" />
      <node concept="3Tm1VV" id="1mAGFBJlpfz" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBJlpcT" role="3clF45" />
    </node>
    <node concept="3clFb_" id="1mAGFBJlpsr" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultAsNumberDescription" />
      <node concept="3clFbS" id="1mAGFBJlpsu" role="3clF47" />
      <node concept="3Tm1VV" id="1mAGFBJlpsv" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBJlppG" role="3clF45" />
    </node>
    <node concept="3clFb_" id="4B5aqq4g9OG" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultWithSpecimenAsNumberAlias" />
      <node concept="3clFbS" id="4B5aqq4g9OH" role="3clF47" />
      <node concept="3Tm1VV" id="4B5aqq4g9OI" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4g9OJ" role="3clF45" />
    </node>
    <node concept="3clFb_" id="4B5aqq4g9OK" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultWithSpecimenAsNumberDescription" />
      <node concept="3clFbS" id="4B5aqq4g9OL" role="3clF47" />
      <node concept="3Tm1VV" id="4B5aqq4g9OM" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4g9ON" role="3clF45" />
    </node>
    <node concept="3clFb_" id="4B5aqq4g6P_" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultWithSpecimenAsNumberKeywordPreviousNumberResult" />
      <node concept="3clFbS" id="4B5aqq4g6PA" role="3clF47" />
      <node concept="3Tm1VV" id="4B5aqq4g6PB" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4g6PC" role="3clF45" />
    </node>
    <node concept="3clFb_" id="4B5aqq4g6PD" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultWithSpecimenAsNumberKeywordWithSpecimen" />
      <node concept="3clFbS" id="4B5aqq4g6PE" role="3clF47" />
      <node concept="3Tm1VV" id="4B5aqq4g6PF" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4g6PG" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="4B5aqq4g6OP" role="jymVt" />
    <node concept="3clFb_" id="PDjyzkxnmR" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasResultAsNumberAlias" />
      <node concept="3clFbS" id="PDjyzkxnmU" role="3clF47" />
      <node concept="3Tm1VV" id="PDjyzkxnmV" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkxnla" role="3clF45" />
    </node>
    <node concept="3clFb_" id="PDjyzkxnvJ" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasResultAsNumberDescription" />
      <node concept="3clFbS" id="PDjyzkxnvM" role="3clF47" />
      <node concept="3Tm1VV" id="PDjyzkxnvN" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkxntT" role="3clF45" />
    </node>
    <node concept="3clFb_" id="PDjyzkxoC8" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasResultWithSpecimenAsNumberAlias" />
      <node concept="3clFbS" id="PDjyzkxoC9" role="3clF47" />
      <node concept="3Tm1VV" id="PDjyzkxoCa" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkxoCb" role="3clF45" />
    </node>
    <node concept="3clFb_" id="PDjyzkxoCc" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasResultWithSpecimenAsNumberDescription" />
      <node concept="3clFbS" id="PDjyzkxoCd" role="3clF47" />
      <node concept="3Tm1VV" id="PDjyzkxoCe" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkxoCf" role="3clF45" />
    </node>
    <node concept="3clFb_" id="PDjyzkxpo7" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasResultWithSpecimenAsNumberKeywordHasResult" />
      <node concept="3clFbS" id="PDjyzkxpo8" role="3clF47" />
      <node concept="3Tm1VV" id="PDjyzkxpo9" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkxpoa" role="3clF45" />
    </node>
    <node concept="3clFb_" id="PDjyzkxpob" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasResultWithSpecimenAsNumberKeywordWithSpecimen" />
      <node concept="3clFbS" id="PDjyzkxpoc" role="3clF47" />
      <node concept="3Tm1VV" id="PDjyzkxpod" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkxpoe" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="PDjyzkxnzl" role="jymVt" />
    <node concept="3clFb_" id="PDjyzkxnDb" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasPreviousResultAsNumberAlias" />
      <node concept="3clFbS" id="PDjyzkxnDe" role="3clF47" />
      <node concept="3Tm1VV" id="PDjyzkxnDf" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkxnBc" role="3clF45" />
    </node>
    <node concept="3clFb_" id="PDjyzkxoje" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasPreviousResultAsNumberDescription" />
      <node concept="3clFbS" id="PDjyzkxojf" role="3clF47" />
      <node concept="3Tm1VV" id="PDjyzkxojg" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkxojh" role="3clF45" />
    </node>
    <node concept="3clFb_" id="PDjyzkxoCh" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestWithSpecimenHasPreviousResultAsNumberAlias" />
      <node concept="3clFbS" id="PDjyzkxoCi" role="3clF47" />
      <node concept="3Tm1VV" id="PDjyzkxoCj" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkxoCk" role="3clF45" />
    </node>
    <node concept="3clFb_" id="PDjyzkxoCl" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestWithSpecimenHasPreviousResultAsNumberDescription" />
      <node concept="3clFbS" id="PDjyzkxoCm" role="3clF47" />
      <node concept="3Tm1VV" id="PDjyzkxoCn" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkxoCo" role="3clF45" />
    </node>
    <node concept="3clFb_" id="PDjyzkxpj2" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasPreviousResultWithSpecimenAsNumberKeywordHasPreviousResult" />
      <node concept="3clFbS" id="PDjyzkxpj3" role="3clF47" />
      <node concept="3Tm1VV" id="PDjyzkxpj4" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkxpj5" role="3clF45" />
    </node>
    <node concept="3clFb_" id="PDjyzkxpj6" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasPreviousResultWithSpecimenAsNumberKeywordWithSpecimen" />
      <node concept="3clFbS" id="PDjyzkxpj7" role="3clF47" />
      <node concept="3Tm1VV" id="PDjyzkxpj8" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkxpj9" role="3clF45" />
    </node>
    <node concept="3Tm1VV" id="4zMac8rUNtQ" role="1B3o_S" />
  </node>
  <node concept="312cEu" id="4zMac8rUO0_">
    <property role="TrG5h" value="EnNumberResultLaboratoryTestDataTranslations" />
    <property role="3GE5qa" value="localization" />
    <node concept="3clFb_" id="6LTgXmNf7vo" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getILaboratoryTestWithNumberResultDataValueIncludeSpecimenTypeIntentionDescription" />
      <node concept="3Tm1VV" id="6LTgXmNf7vq" role="1B3o_S" />
      <node concept="17QB3L" id="6LTgXmNf7vr" role="3clF45" />
      <node concept="3clFbS" id="6LTgXmNf7vs" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNf9gy" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNf9gx" role="3clFbG">
            <property role="Xl_RC" value="Include Specimen Type" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6LTgXmNf7vt" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="6LTgXmNf87_" role="jymVt" />
    <node concept="3clFb_" id="4V3GMfXwkIt" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultAsNumberAlias" />
      <node concept="3Tm1VV" id="4V3GMfXwkIv" role="1B3o_S" />
      <node concept="17QB3L" id="4V3GMfXwkIw" role="3clF45" />
      <node concept="3clFbS" id="4V3GMfXwkIx" role="3clF47">
        <node concept="3clFbF" id="4V3GMfXwkNI" role="3cqZAp">
          <node concept="Xl_RD" id="4V3GMfXwkNH" role="3clFbG">
            <property role="Xl_RC" value="numeric test result" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4V3GMfXwkIy" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4V3GMfXwkIz" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultAsNumberDescription" />
      <node concept="3Tm1VV" id="4V3GMfXwkI_" role="1B3o_S" />
      <node concept="17QB3L" id="4V3GMfXwkIA" role="3clF45" />
      <node concept="3clFbS" id="4V3GMfXwkIB" role="3clF47">
        <node concept="3clFbF" id="4V3GMfXwkPe" role="3cqZAp">
          <node concept="Xl_RD" id="4V3GMfXwkPd" role="3clFbG">
            <property role="Xl_RC" value="check numeric value of current result of test" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4V3GMfXwkIC" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq4g866" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultWithSpecimenAsNumberAlias" />
      <node concept="3Tm1VV" id="4B5aqq4g868" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4g869" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq4g86a" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4g8jJ" role="3cqZAp">
          <node concept="3cpWs3" id="4B5aqq4g8xC" role="3clFbG">
            <node concept="Xl_RD" id="4B5aqq4g8xW" role="3uHU7w">
              <property role="Xl_RC" value=" with" />
            </node>
            <node concept="1rXfSq" id="4B5aqq4g8jI" role="3uHU7B">
              <ref role="37wK5l" node="4V3GMfXwkIt" resolve="getLaboratoryTestResultAsNumberAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq4g86b" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq4g86c" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultWithSpecimenAsNumberDescription" />
      <node concept="3Tm1VV" id="4B5aqq4g86e" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4g86f" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq4g86g" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4g8Xq" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq4g8Xp" role="3clFbG">
            <property role="Xl_RC" value="check numeric value of current result of test on specimen type" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq4g86h" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq4g86i" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultWithSpecimenAsNumberKeywordNumberResult" />
      <node concept="3Tm1VV" id="4B5aqq4g86k" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4g86l" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq4g86m" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4g92B" role="3cqZAp">
          <node concept="1rXfSq" id="4B5aqq4g92D" role="3clFbG">
            <ref role="37wK5l" node="4V3GMfXwkIt" resolve="getLaboratoryTestResultAsNumberAlias" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq4g86n" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq4g86o" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultWithSpecimenAsNumberKeywordWithSpecimen" />
      <node concept="3Tm1VV" id="4B5aqq4g86q" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4g86r" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq4g86s" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4g9cy" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq4g9cx" role="3clFbG">
            <property role="Xl_RC" value="with" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq4g86t" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="6LTgXmMqS1v" role="jymVt" />
    <node concept="2tJIrI" id="4V3GMfXwGF9" role="jymVt" />
    <node concept="3clFb_" id="4V3GMfXwG_M" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultAsNumberAlias" />
      <node concept="3Tm1VV" id="4V3GMfXwG_O" role="1B3o_S" />
      <node concept="17QB3L" id="4V3GMfXwG_P" role="3clF45" />
      <node concept="3clFbS" id="4V3GMfXwG_Q" role="3clF47">
        <node concept="3clFbF" id="4V3GMfXwGJG" role="3cqZAp">
          <node concept="Xl_RD" id="4V3GMfXwGJF" role="3clFbG">
            <property role="Xl_RC" value="previous numeric test result" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4V3GMfXwG_R" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4V3GMfXwG_S" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultAsNumberDescription" />
      <node concept="3Tm1VV" id="4V3GMfXwG_U" role="1B3o_S" />
      <node concept="17QB3L" id="4V3GMfXwG_V" role="3clF45" />
      <node concept="3clFbS" id="4V3GMfXwG_W" role="3clF47">
        <node concept="3clFbF" id="4V3GMfXwGKM" role="3cqZAp">
          <node concept="Xl_RD" id="4V3GMfXwGKL" role="3clFbG">
            <property role="Xl_RC" value="check numeric value of previous result of test" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4V3GMfXwG_X" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq4gaq8" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultWithSpecimenAsNumberAlias" />
      <node concept="3Tm1VV" id="4B5aqq4gaqa" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4gaqb" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq4gaqc" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4gaZL" role="3cqZAp">
          <node concept="3cpWs3" id="4B5aqq4gbrJ" role="3clFbG">
            <node concept="Xl_RD" id="4B5aqq4gbs3" role="3uHU7w">
              <property role="Xl_RC" value=" with" />
            </node>
            <node concept="1rXfSq" id="4B5aqq4gaZK" role="3uHU7B">
              <ref role="37wK5l" node="4V3GMfXwG_M" resolve="getLaboratoryTestPreviousResultAsNumberAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq4gaqd" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq4gaqe" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultWithSpecimenAsNumberDescription" />
      <node concept="3Tm1VV" id="4B5aqq4gaqg" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4gaqh" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq4gaqi" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4gbKr" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq4gbKq" role="3clFbG">
            <property role="Xl_RC" value="check numeric value of previous result of test on specimen type" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq4gaqj" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq4g86u" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultWithSpecimenAsNumberKeywordPreviousNumberResult" />
      <node concept="3Tm1VV" id="4B5aqq4g86w" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4g86x" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq4g86y" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4gbMk" role="3cqZAp">
          <node concept="1rXfSq" id="4B5aqq4gbMm" role="3clFbG">
            <ref role="37wK5l" node="4V3GMfXwG_M" resolve="getLaboratoryTestPreviousResultAsNumberAlias" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq4g86z" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq4g86$" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultWithSpecimenAsNumberKeywordWithSpecimen" />
      <node concept="3Tm1VV" id="4B5aqq4g86A" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4g86B" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq4g86C" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4gc29" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq4gc28" role="3clFbG">
            <property role="Xl_RC" value="with" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq4g86D" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="6LTgXmMqTDb" role="jymVt" />
    <node concept="2tJIrI" id="4B5aqq4g7Wy" role="jymVt" />
    <node concept="3clFb_" id="PDjyzkxylu" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasResultAsNumberAlias" />
      <node concept="3Tm1VV" id="PDjyzkxylw" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkxylx" role="3clF45" />
      <node concept="3clFbS" id="PDjyzkxyly" role="3clF47">
        <node concept="3clFbF" id="PDjyzkxz5I" role="3cqZAp">
          <node concept="Xl_RD" id="PDjyzkxz5H" role="3clFbG">
            <property role="Xl_RC" value="has numeric test result" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkxylz" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzkxyl$" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasResultAsNumberDescription" />
      <node concept="3Tm1VV" id="PDjyzkxylA" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkxylB" role="3clF45" />
      <node concept="3clFbS" id="PDjyzkxylC" role="3clF47">
        <node concept="3clFbF" id="PDjyzkxz71" role="3cqZAp">
          <node concept="Xl_RD" id="PDjyzkxz70" role="3clFbG">
            <property role="Xl_RC" value="check if result of test has numeric value" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkxylD" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzkxzch" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasResultWithSpecimenAsNumberAlias" />
      <node concept="3Tm1VV" id="PDjyzkxzcj" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkxzck" role="3clF45" />
      <node concept="3clFbS" id="PDjyzkxzcl" role="3clF47">
        <node concept="3clFbF" id="PDjyzkx_hv" role="3cqZAp">
          <node concept="3cpWs3" id="PDjyzkx_hw" role="3clFbG">
            <node concept="Xl_RD" id="PDjyzkx_hx" role="3uHU7w">
              <property role="Xl_RC" value=" with" />
            </node>
            <node concept="1rXfSq" id="PDjyzkx_hy" role="3uHU7B">
              <ref role="37wK5l" node="PDjyzkxylu" resolve="getLaboratoryTestHasResultAsNumberAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkxzcm" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzkxzcn" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasResultWithSpecimenAsNumberDescription" />
      <node concept="3Tm1VV" id="PDjyzkxzcp" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkxzcq" role="3clF45" />
      <node concept="3clFbS" id="PDjyzkxzcr" role="3clF47">
        <node concept="3clFbF" id="PDjyzkx_X7" role="3cqZAp">
          <node concept="Xl_RD" id="PDjyzkx_X9" role="3clFbG">
            <property role="Xl_RC" value="check if result of test with specimen type has numeric value" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkxzcs" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzkxzct" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasResultWithSpecimenAsNumberKeywordHasResult" />
      <node concept="3Tm1VV" id="PDjyzkxzcv" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkxzcw" role="3clF45" />
      <node concept="3clFbS" id="PDjyzkxzcx" role="3clF47">
        <node concept="3clFbF" id="PDjyzkxA2m" role="3cqZAp">
          <node concept="1rXfSq" id="PDjyzkxBHi" role="3clFbG">
            <ref role="37wK5l" node="PDjyzkxylu" resolve="getLaboratoryTestHasResultAsNumberAlias" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkxzcy" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzkxzcz" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasResultWithSpecimenAsNumberKeywordWithSpecimen" />
      <node concept="3Tm1VV" id="PDjyzkxzc_" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkxzcA" role="3clF45" />
      <node concept="3clFbS" id="PDjyzkxzcB" role="3clF47">
        <node concept="3clFbF" id="PDjyzkxA3D" role="3cqZAp">
          <node concept="Xl_RD" id="PDjyzkxA3C" role="3clFbG">
            <property role="Xl_RC" value="with" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkxzcC" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="6LTgXmMqWsU" role="jymVt" />
    <node concept="2tJIrI" id="PDjyzkxyJ2" role="jymVt" />
    <node concept="3clFb_" id="PDjyzkxylE" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasPreviousResultAsNumberAlias" />
      <node concept="3Tm1VV" id="PDjyzkxylG" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkxylH" role="3clF45" />
      <node concept="3clFbS" id="PDjyzkxylI" role="3clF47">
        <node concept="3clFbF" id="PDjyzkxz9l" role="3cqZAp">
          <node concept="Xl_RD" id="PDjyzkxz9k" role="3clFbG">
            <property role="Xl_RC" value="has previous numeric test result" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkxylJ" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzkxylK" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasPreviousResultAsNumberDescription" />
      <node concept="3Tm1VV" id="PDjyzkxylM" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkxylN" role="3clF45" />
      <node concept="3clFbS" id="PDjyzkxylO" role="3clF47">
        <node concept="3clFbF" id="PDjyzkxzbf" role="3cqZAp">
          <node concept="Xl_RD" id="PDjyzkxzbe" role="3clFbG">
            <property role="Xl_RC" value="check if result of previous test has numeric value" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkxylP" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzkxzcD" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestWithSpecimenHasPreviousResultAsNumberAlias" />
      <node concept="3Tm1VV" id="PDjyzkxzcF" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkxzcG" role="3clF45" />
      <node concept="3clFbS" id="PDjyzkxzcH" role="3clF47">
        <node concept="3clFbF" id="PDjyzkxAh1" role="3cqZAp">
          <node concept="3cpWs3" id="PDjyzkxAh2" role="3clFbG">
            <node concept="Xl_RD" id="PDjyzkxAh3" role="3uHU7w">
              <property role="Xl_RC" value=" with" />
            </node>
            <node concept="1rXfSq" id="PDjyzkxAh4" role="3uHU7B">
              <ref role="37wK5l" node="PDjyzkxylE" resolve="getLaboratoryTestHasPreviousResultAsNumberAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkxzcI" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzkxzcJ" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestWithSpecimenHasPreviousResultAsNumberDescription" />
      <node concept="3Tm1VV" id="PDjyzkxzcL" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkxzcM" role="3clF45" />
      <node concept="3clFbS" id="PDjyzkxzcN" role="3clF47">
        <node concept="3clFbF" id="PDjyzkxAWZ" role="3cqZAp">
          <node concept="Xl_RD" id="PDjyzkxAX1" role="3clFbG">
            <property role="Xl_RC" value="check if previous result of test with specimen type has numeric value" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkxzcO" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzkxzcP" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasPreviousResultWithSpecimenAsNumberKeywordHasPreviousResult" />
      <node concept="3Tm1VV" id="PDjyzkxzcR" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkxzcS" role="3clF45" />
      <node concept="3clFbS" id="PDjyzkxzcT" role="3clF47">
        <node concept="3clFbF" id="PDjyzkxB5I" role="3cqZAp">
          <node concept="1rXfSq" id="PDjyzkxB5K" role="3clFbG">
            <ref role="37wK5l" node="PDjyzkxylE" resolve="getLaboratoryTestHasPreviousResultAsNumberAlias" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkxzcU" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzkxzcV" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasPreviousResultWithSpecimenAsNumberKeywordWithSpecimen" />
      <node concept="3Tm1VV" id="PDjyzkxzcX" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkxzcY" role="3clF45" />
      <node concept="3clFbS" id="PDjyzkxzcZ" role="3clF47">
        <node concept="3clFbF" id="PDjyzkxB$n" role="3cqZAp">
          <node concept="Xl_RD" id="PDjyzkxB$m" role="3clFbG">
            <property role="Xl_RC" value="with" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkxzd0" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3Tm1VV" id="4zMac8rUO0A" role="1B3o_S" />
    <node concept="3uibUv" id="4zMac8rUO1F" role="EKbjA">
      <ref role="3uigEE" node="4zMac8rUNtP" resolve="INumberResultLaboratoryTestDataTranslations" />
    </node>
  </node>
  <node concept="312cEu" id="4zMac8rUO1O">
    <property role="TrG5h" value="NoNumberResultLaboratoryTestDataTranslations" />
    <property role="3GE5qa" value="localization" />
    <node concept="3clFb_" id="6LTgXmNfaWR" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getILaboratoryTestWithNumberResultDataValueIncludeSpecimenTypeIntentionDescription" />
      <node concept="3Tm1VV" id="6LTgXmNfaWT" role="1B3o_S" />
      <node concept="17QB3L" id="6LTgXmNfaWU" role="3clF45" />
      <node concept="3clFbS" id="6LTgXmNfaWV" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNfc9y" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNfc9x" role="3clFbG">
            <property role="Xl_RC" value="Inkluder Materiale" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6LTgXmNfaWW" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="6LTgXmNfapy" role="jymVt" />
    <node concept="3clFb_" id="4V3GMfXwl5S" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultAsNumberAlias" />
      <node concept="3Tm1VV" id="4V3GMfXwl5U" role="1B3o_S" />
      <node concept="17QB3L" id="4V3GMfXwl5V" role="3clF45" />
      <node concept="3clFbS" id="4V3GMfXwl5W" role="3clF47">
        <node concept="3clFbF" id="4V3GMfXwlb9" role="3cqZAp">
          <node concept="Xl_RD" id="4V3GMfXwlb8" role="3clFbG">
            <property role="Xl_RC" value="numerisk analysesvar" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4V3GMfXwl5X" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4V3GMfXwl5Y" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultAsNumberDescription" />
      <node concept="3Tm1VV" id="4V3GMfXwl60" role="1B3o_S" />
      <node concept="17QB3L" id="4V3GMfXwl61" role="3clF45" />
      <node concept="3clFbS" id="4V3GMfXwl62" role="3clF47">
        <node concept="3clFbF" id="4V3GMfXwlcs" role="3cqZAp">
          <node concept="Xl_RD" id="4V3GMfXwlcr" role="3clFbG">
            <property role="Xl_RC" value="sjekk numerisk verdi på gjeldende svar fra analyse" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4V3GMfXwl63" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq4gcf$" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultWithSpecimenAsNumberAlias" />
      <node concept="3Tm1VV" id="4B5aqq4gcfA" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4gcfB" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq4gcfC" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4gcvX" role="3cqZAp">
          <node concept="3cpWs3" id="4B5aqq4gcJx" role="3clFbG">
            <node concept="Xl_RD" id="4B5aqq4gcVf" role="3uHU7w">
              <property role="Xl_RC" value=" på" />
            </node>
            <node concept="1rXfSq" id="4B5aqq4gcvW" role="3uHU7B">
              <ref role="37wK5l" node="4V3GMfXwl5S" resolve="getLaboratoryTestResultAsNumberAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq4gcfD" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq4gcfE" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultWithSpecimenAsNumberDescription" />
      <node concept="3Tm1VV" id="4B5aqq4gcfG" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4gcfH" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq4gcfI" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4gd4v" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq4gd4u" role="3clFbG">
            <property role="Xl_RC" value="sjekk numerisk verdi på gjeldende svar fra analyse på materiale" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq4gcfJ" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq4gcfK" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultWithSpecimenAsNumberKeywordNumberResult" />
      <node concept="3Tm1VV" id="4B5aqq4gcfM" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4gcfN" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq4gcfO" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4gd5$" role="3cqZAp">
          <node concept="1rXfSq" id="4B5aqq4gd5A" role="3clFbG">
            <ref role="37wK5l" node="4V3GMfXwl5S" resolve="getLaboratoryTestResultAsNumberAlias" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq4gcfP" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq4gcfQ" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultWithSpecimenAsNumberKeywordWithSpecimen" />
      <node concept="3Tm1VV" id="4B5aqq4gcfS" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4gcfT" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq4gcfU" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4gdlp" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq4gdlo" role="3clFbG">
            <property role="Xl_RC" value="på" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq4gcfV" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="4V3GMfXwH7m" role="jymVt" />
    <node concept="3clFb_" id="4V3GMfXwH1Z" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultAsNumberAlias" />
      <node concept="3Tm1VV" id="4V3GMfXwH21" role="1B3o_S" />
      <node concept="17QB3L" id="4V3GMfXwH22" role="3clF45" />
      <node concept="3clFbS" id="4V3GMfXwH23" role="3clF47">
        <node concept="3clFbF" id="4V3GMfXwHbT" role="3cqZAp">
          <node concept="Xl_RD" id="4V3GMfXwHbS" role="3clFbG">
            <property role="Xl_RC" value="forrige numerisk analysesvar" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4V3GMfXwH24" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4V3GMfXwH25" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultAsNumberDescription" />
      <node concept="3Tm1VV" id="4V3GMfXwH27" role="1B3o_S" />
      <node concept="17QB3L" id="4V3GMfXwH28" role="3clF45" />
      <node concept="3clFbS" id="4V3GMfXwH29" role="3clF47">
        <node concept="3clFbF" id="4V3GMfXwHcM" role="3cqZAp">
          <node concept="Xl_RD" id="4V3GMfXwHcL" role="3clFbG">
            <property role="Xl_RC" value="sjekk numerisk verdi på forrige svar fra analyse" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4V3GMfXwH2a" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq4gcfW" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultWithSpecimenAsNumberAlias" />
      <node concept="3Tm1VV" id="4B5aqq4gcfY" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4gcfZ" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq4gcg0" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4gdBD" role="3cqZAp">
          <node concept="3cpWs3" id="4B5aqq4gdGl" role="3clFbG">
            <node concept="Xl_RD" id="4B5aqq4gdGS" role="3uHU7w">
              <property role="Xl_RC" value=" på" />
            </node>
            <node concept="1rXfSq" id="4B5aqq4ge5d" role="3uHU7B">
              <ref role="37wK5l" node="4V3GMfXwH1Z" resolve="getLaboratoryTestPreviousResultAsNumberAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq4gcg1" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq4gcg2" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultWithSpecimenAsNumberDescription" />
      <node concept="3Tm1VV" id="4B5aqq4gcg4" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4gcg5" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq4gcg6" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4gel0" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq4gekZ" role="3clFbG">
            <property role="Xl_RC" value="sjekk numerisk verdi på forrige svar fra analyse på materiale" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq4gcg7" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq4gcg8" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultWithSpecimenAsNumberKeywordPreviousNumberResult" />
      <node concept="3Tm1VV" id="4B5aqq4gcga" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4gcgb" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq4gcgc" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4gere" role="3cqZAp">
          <node concept="1rXfSq" id="4B5aqq4gerg" role="3clFbG">
            <ref role="37wK5l" node="4V3GMfXwH1Z" resolve="getLaboratoryTestPreviousResultAsNumberAlias" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq4gcgd" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq4gcge" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultWithSpecimenAsNumberKeywordWithSpecimen" />
      <node concept="3Tm1VV" id="4B5aqq4gcgg" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4gcgh" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq4gcgi" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4geF3" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq4geF2" role="3clFbG">
            <property role="Xl_RC" value="på" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq4gcgj" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="4B5aqq4gca2" role="jymVt" />
    <node concept="3clFb_" id="PDjyzkxrzf" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasResultAsNumberAlias" />
      <node concept="3Tm1VV" id="PDjyzkxrzh" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkxrzi" role="3clF45" />
      <node concept="3clFbS" id="PDjyzkxrzj" role="3clF47">
        <node concept="3clFbF" id="PDjyzkxsjv" role="3cqZAp">
          <node concept="Xl_RD" id="PDjyzkxsju" role="3clFbG">
            <property role="Xl_RC" value="har numerisk analysesvar" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkxrzk" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzkxrzl" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasResultAsNumberDescription" />
      <node concept="3Tm1VV" id="PDjyzkxrzn" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkxrzo" role="3clF45" />
      <node concept="3clFbS" id="PDjyzkxrzp" role="3clF47">
        <node concept="3clFbF" id="PDjyzkxslA" role="3cqZAp">
          <node concept="Xl_RD" id="PDjyzkxsl_" role="3clFbG">
            <property role="Xl_RC" value="sjekk om svar fra analyse har numerisk verdi" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkxrzq" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzkxsRw" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasResultWithSpecimenAsNumberAlias" />
      <node concept="3Tm1VV" id="PDjyzkxsRy" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkxsRz" role="3clF45" />
      <node concept="3clFbS" id="PDjyzkxsR$" role="3clF47">
        <node concept="3clFbF" id="PDjyzkxtUG" role="3cqZAp">
          <node concept="3cpWs3" id="PDjyzkxukW" role="3clFbG">
            <node concept="Xl_RD" id="PDjyzkxuFm" role="3uHU7w">
              <property role="Xl_RC" value=" med" />
            </node>
            <node concept="1rXfSq" id="PDjyzkxtUF" role="3uHU7B">
              <ref role="37wK5l" node="PDjyzkxrzf" resolve="getLaboratoryTestHasResultAsNumberAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkxsR_" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzkxsRA" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasResultWithSpecimenAsNumberDescription" />
      <node concept="3Tm1VV" id="PDjyzkxsRC" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkxsRD" role="3clF45" />
      <node concept="3clFbS" id="PDjyzkxsRE" role="3clF47">
        <node concept="3clFbF" id="PDjyzkxuX0" role="3cqZAp">
          <node concept="Xl_RD" id="PDjyzkxuX2" role="3clFbG">
            <property role="Xl_RC" value="sjekk numerisk verdi på svar fra analyse på materiale" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkxsRF" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzkxsRG" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasResultWithSpecimenAsNumberKeywordHasResult" />
      <node concept="3Tm1VV" id="PDjyzkxsRI" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkxsRJ" role="3clF45" />
      <node concept="3clFbS" id="PDjyzkxsRK" role="3clF47">
        <node concept="3clFbF" id="PDjyzkxv3g" role="3cqZAp">
          <node concept="1rXfSq" id="PDjyzkxv3T" role="3clFbG">
            <ref role="37wK5l" node="PDjyzkxrzf" resolve="getLaboratoryTestHasResultAsNumberAlias" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkxsRL" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzkxsRM" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasResultWithSpecimenAsNumberKeywordWithSpecimen" />
      <node concept="3Tm1VV" id="PDjyzkxsRO" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkxsRP" role="3clF45" />
      <node concept="3clFbS" id="PDjyzkxsRQ" role="3clF47">
        <node concept="3clFbF" id="PDjyzkxvqf" role="3cqZAp">
          <node concept="Xl_RD" id="PDjyzkxvqe" role="3clFbG">
            <property role="Xl_RC" value="med" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkxsRR" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="6LTgXmMr3xm" role="jymVt" />
    <node concept="2tJIrI" id="PDjyzkxrWN" role="jymVt" />
    <node concept="3clFb_" id="PDjyzkxrzr" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasPreviousResultAsNumberAlias" />
      <node concept="3Tm1VV" id="PDjyzkxrzt" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkxrzu" role="3clF45" />
      <node concept="3clFbS" id="PDjyzkxrzv" role="3clF47">
        <node concept="3clFbF" id="PDjyzkxsn6" role="3cqZAp">
          <node concept="Xl_RD" id="PDjyzkxsn5" role="3clFbG">
            <property role="Xl_RC" value="har forrige numerisk analysesvar" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkxrzw" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzkxrzx" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasPreviousResultAsNumberDescription" />
      <node concept="3Tm1VV" id="PDjyzkxrzz" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkxrz$" role="3clF45" />
      <node concept="3clFbS" id="PDjyzkxrz_" role="3clF47">
        <node concept="3clFbF" id="PDjyzkxsrQ" role="3cqZAp">
          <node concept="Xl_RD" id="PDjyzkxsrP" role="3clFbG">
            <property role="Xl_RC" value="sjekk om forrige svar fra analyse har numerisk verdi" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkxrzA" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzkxsRS" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestWithSpecimenHasPreviousResultAsNumberAlias" />
      <node concept="3Tm1VV" id="PDjyzkxsRU" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkxsRV" role="3clF45" />
      <node concept="3clFbS" id="PDjyzkxsRW" role="3clF47">
        <node concept="3clFbF" id="PDjyzkxvuP" role="3cqZAp">
          <node concept="3cpWs3" id="PDjyzkxvuQ" role="3clFbG">
            <node concept="Xl_RD" id="PDjyzkxvuR" role="3uHU7w">
              <property role="Xl_RC" value=" med" />
            </node>
            <node concept="1rXfSq" id="PDjyzkxvuS" role="3uHU7B">
              <ref role="37wK5l" node="PDjyzkxrzr" resolve="getLaboratoryTestHasPreviousResultAsNumberAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkxsRX" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzkxsRY" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestWithSpecimenHasPreviousResultAsNumberDescription" />
      <node concept="3Tm1VV" id="PDjyzkxsS0" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkxsS1" role="3clF45" />
      <node concept="3clFbS" id="PDjyzkxsS2" role="3clF47">
        <node concept="3clFbF" id="PDjyzkxw$b" role="3cqZAp">
          <node concept="Xl_RD" id="PDjyzkxw$d" role="3clFbG">
            <property role="Xl_RC" value="sjekk numerisk verdi på forrige svar fra analyse på materiale" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkxsS3" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzkxsS4" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasPreviousResultWithSpecimenAsNumberKeywordHasPreviousResult" />
      <node concept="3Tm1VV" id="PDjyzkxsS6" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkxsS7" role="3clF45" />
      <node concept="3clFbS" id="PDjyzkxsS8" role="3clF47">
        <node concept="3clFbF" id="PDjyzkxwHk" role="3cqZAp">
          <node concept="1rXfSq" id="PDjyzkxwHm" role="3clFbG">
            <ref role="37wK5l" node="PDjyzkxrzr" resolve="getLaboratoryTestHasPreviousResultAsNumberAlias" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkxsS9" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzkxsSa" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasPreviousResultWithSpecimenAsNumberKeywordWithSpecimen" />
      <node concept="3Tm1VV" id="PDjyzkxsSc" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkxsSd" role="3clF45" />
      <node concept="3clFbS" id="PDjyzkxsSe" role="3clF47">
        <node concept="3clFbF" id="PDjyzkxx3H" role="3cqZAp">
          <node concept="Xl_RD" id="PDjyzkxx3G" role="3clFbG">
            <property role="Xl_RC" value="med" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkxsSf" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3Tm1VV" id="4zMac8rUO1P" role="1B3o_S" />
    <node concept="3uibUv" id="4zMac8rUO2g" role="EKbjA">
      <ref role="3uigEE" node="4zMac8rUNtP" resolve="INumberResultLaboratoryTestDataTranslations" />
    </node>
  </node>
</model>

