<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:aed49bfb-d506-499d-a596-1865d8a4c972(no.uio.mLab.decisions.data.laboratoryTest.numberResult.runtime)">
  <persistence version="9" />
  <languages>
    <use id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage" version="6" />
  </languages>
  <imports>
    <import index="8r9s" ref="r:0e143259-7e64-4fad-906c-02a97b75504d(no.uio.mLab.decisions.core.runtime)" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" />
    <import index="mygi" ref="r:d014d2e6-828c-4ff5-a027-d2fa848d3c1b(no.uio.mLab.decisions.references.laboratoryTest.runtime)" />
    <import index="33ny" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.util(JDK/)" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1080223426719" name="jetbrains.mps.baseLanguage.structure.OrExpression" flags="nn" index="22lmx$" />
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1465982738277781862" name="jetbrains.mps.baseLanguage.structure.PlaceholderMember" flags="ng" index="2tJIrI" />
      <concept id="1188207840427" name="jetbrains.mps.baseLanguage.structure.AnnotationInstance" flags="nn" index="2AHcQZ">
        <reference id="1188208074048" name="annotation" index="2AI5Lk" />
      </concept>
      <concept id="1188208481402" name="jetbrains.mps.baseLanguage.structure.HasAnnotation" flags="ng" index="2AJDlI">
        <child id="1188208488637" name="annotation" index="2AJF6D" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1197029447546" name="jetbrains.mps.baseLanguage.structure.FieldReferenceOperation" flags="nn" index="2OwXpG">
        <reference id="1197029500499" name="fieldDeclaration" index="2Oxat5" />
      </concept>
      <concept id="1070475354124" name="jetbrains.mps.baseLanguage.structure.ThisExpression" flags="nn" index="Xjq3P" />
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1081256982272" name="jetbrains.mps.baseLanguage.structure.InstanceOfExpression" flags="nn" index="2ZW3vV">
        <child id="1081256993305" name="classType" index="2ZW6by" />
        <child id="1081256993304" name="leftExpression" index="2ZW6bz" />
      </concept>
      <concept id="1070534058343" name="jetbrains.mps.baseLanguage.structure.NullLiteral" flags="nn" index="10Nm6u" />
      <concept id="1070534370425" name="jetbrains.mps.baseLanguage.structure.IntegerType" flags="in" index="10Oyi0" />
      <concept id="1070534644030" name="jetbrains.mps.baseLanguage.structure.BooleanType" flags="in" index="10P_77" />
      <concept id="1070534934090" name="jetbrains.mps.baseLanguage.structure.CastExpression" flags="nn" index="10QFUN">
        <child id="1070534934091" name="type" index="10QFUM" />
        <child id="1070534934092" name="expression" index="10QFUP" />
      </concept>
      <concept id="1068390468200" name="jetbrains.mps.baseLanguage.structure.FieldDeclaration" flags="ig" index="312cEg">
        <property id="8606350594693632173" name="isTransient" index="eg7rD" />
        <property id="1240249534625" name="isVolatile" index="34CwA1" />
      </concept>
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu">
        <child id="1165602531693" name="superclass" index="1zkMxy" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <property id="1176718929932" name="isFinal" index="3TUv4t" />
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <property id="4276006055363816570" name="isSynchronized" index="od$2w" />
        <property id="1181808852946" name="isFinal" index="DiZV1" />
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123165" name="jetbrains.mps.baseLanguage.structure.InstanceMethodDeclaration" flags="ig" index="3clFb_">
        <property id="1178608670077" name="isAbstract" index="1EzhhJ" />
      </concept>
      <concept id="1068580123152" name="jetbrains.mps.baseLanguage.structure.EqualsExpression" flags="nn" index="3clFbC" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123137" name="jetbrains.mps.baseLanguage.structure.BooleanConstant" flags="nn" index="3clFbT">
        <property id="1068580123138" name="value" index="3clFbU" />
      </concept>
      <concept id="1068580123140" name="jetbrains.mps.baseLanguage.structure.ConstructorDeclaration" flags="ig" index="3clFbW" />
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1068581517677" name="jetbrains.mps.baseLanguage.structure.VoidType" flags="in" index="3cqZAl" />
      <concept id="1079359253375" name="jetbrains.mps.baseLanguage.structure.ParenthesizedExpression" flags="nn" index="1eOMI4">
        <child id="1079359253376" name="expression" index="1eOMHV" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1107461130800" name="jetbrains.mps.baseLanguage.structure.Classifier" flags="ng" index="3pOWGL">
        <child id="5375687026011219971" name="member" index="jymVt" unordered="true" />
      </concept>
      <concept id="7812454656619025416" name="jetbrains.mps.baseLanguage.structure.MethodDeclaration" flags="ng" index="1rXfSm">
        <property id="8355037393041754995" name="isNative" index="2aFKle" />
      </concept>
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
        <child id="1109201940907" name="parameter" index="11_B2D" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1073239437375" name="jetbrains.mps.baseLanguage.structure.NotEqualsExpression" flags="nn" index="3y3z36" />
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1107796713796" name="jetbrains.mps.baseLanguage.structure.Interface" flags="ig" index="3HP615" />
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
      <concept id="1146644623116" name="jetbrains.mps.baseLanguage.structure.PrivateVisibility" flags="nn" index="3Tm6S6" />
      <concept id="1080120340718" name="jetbrains.mps.baseLanguage.structure.AndExpression" flags="nn" index="1Wc70l" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="312cEu" id="1mAGFBLsNvp">
    <property role="3GE5qa" value="data.values" />
    <property role="TrG5h" value="TestPreviousResultAsNumber" />
    <node concept="312cEg" id="1mAGFBLsNvq" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="test" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm6S6" id="1mAGFBLsNvr" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBLC7iN" role="1tU5fm" />
    </node>
    <node concept="312cEg" id="4B5aqq4g$r$" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="specimenType" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm6S6" id="4B5aqq4g$f8" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4g$gi" role="1tU5fm" />
    </node>
    <node concept="2tJIrI" id="1mAGFBLsNvt" role="jymVt" />
    <node concept="3clFbW" id="1mAGFBLsNvu" role="jymVt">
      <node concept="3cqZAl" id="1mAGFBLsNvv" role="3clF45" />
      <node concept="3Tm1VV" id="1mAGFBLsNvw" role="1B3o_S" />
      <node concept="3clFbS" id="1mAGFBLsNvx" role="3clF47">
        <node concept="3clFbF" id="1mAGFBLsNvy" role="3cqZAp">
          <node concept="37vLTI" id="1mAGFBLsNvz" role="3clFbG">
            <node concept="37vLTw" id="1mAGFBLsNv$" role="37vLTx">
              <ref role="3cqZAo" node="1mAGFBLsNvC" resolve="test" />
            </node>
            <node concept="2OqwBi" id="1mAGFBLsNv_" role="37vLTJ">
              <node concept="Xjq3P" id="1mAGFBLsNvA" role="2Oq$k0" />
              <node concept="2OwXpG" id="1mAGFBLsNvB" role="2OqNvi">
                <ref role="2Oxat5" node="1mAGFBLsNvq" resolve="test" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4B5aqq4g$L$" role="3cqZAp">
          <node concept="37vLTI" id="4B5aqq4g_jl" role="3clFbG">
            <node concept="37vLTw" id="4B5aqq4g_vd" role="37vLTx">
              <ref role="3cqZAo" node="4B5aqq4g$C0" resolve="specimenType" />
            </node>
            <node concept="2OqwBi" id="4B5aqq4g$Q1" role="37vLTJ">
              <node concept="Xjq3P" id="4B5aqq4g$Ly" role="2Oq$k0" />
              <node concept="2OwXpG" id="4B5aqq4g$Y6" role="2OqNvi">
                <ref role="2Oxat5" node="4B5aqq4g$r$" resolve="specimenType" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="1mAGFBLsNvC" role="3clF46">
        <property role="TrG5h" value="test" />
        <node concept="17QB3L" id="1mAGFBLC7nv" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="4B5aqq4g$C0" role="3clF46">
        <property role="TrG5h" value="specimenType" />
        <node concept="17QB3L" id="4B5aqq4g$Jv" role="1tU5fm" />
      </node>
    </node>
    <node concept="2tJIrI" id="1mAGFBLsNvE" role="jymVt" />
    <node concept="3clFb_" id="1mAGFBLEGUK" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestName" />
      <node concept="3Tm1VV" id="1mAGFBLEGUM" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBLEGUN" role="3clF45" />
      <node concept="3clFbS" id="1mAGFBLEGUP" role="3clF47">
        <node concept="3cpWs6" id="1mAGFBLsNvJ" role="3cqZAp">
          <node concept="2OqwBi" id="1mAGFBLsNvK" role="3cqZAk">
            <node concept="Xjq3P" id="1mAGFBLsNvL" role="2Oq$k0" />
            <node concept="2OwXpG" id="1mAGFBLsNvM" role="2OqNvi">
              <ref role="2Oxat5" node="1mAGFBLsNvq" resolve="test" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="4B5aqq4g_FZ" role="jymVt" />
    <node concept="3clFb_" id="4B5aqq4gAiy" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="getSpecimenType" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="4B5aqq4gAi_" role="3clF47">
        <node concept="3cpWs6" id="4B5aqq4gAvX" role="3cqZAp">
          <node concept="2OqwBi" id="4B5aqq4gAB4" role="3cqZAk">
            <node concept="Xjq3P" id="4B5aqq4gAwc" role="2Oq$k0" />
            <node concept="2OwXpG" id="4B5aqq4gAQQ" role="2OqNvi">
              <ref role="2Oxat5" node="4B5aqq4g$r$" resolve="specimenType" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="4B5aqq4gA5C" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4gA6X" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="5jVYnMGNAh4" role="jymVt" />
    <node concept="3clFb_" id="5jVYnMGNAh5" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="accept" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3Tm1VV" id="5jVYnMGNAh6" role="1B3o_S" />
      <node concept="37vLTG" id="5jVYnMGNAh8" role="3clF46">
        <property role="TrG5h" value="visitor" />
        <node concept="3uibUv" id="5jVYnMGNAh9" role="1tU5fm">
          <ref role="3uigEE" to="8r9s:5jVYnMGKCt5" resolve="DataValueVisitor" />
        </node>
      </node>
      <node concept="3clFbS" id="5jVYnMGNAha" role="3clF47">
        <node concept="3clFbJ" id="5jVYnMGNAhb" role="3cqZAp">
          <node concept="2ZW3vV" id="5jVYnMGNAhc" role="3clFbw">
            <node concept="3uibUv" id="5jVYnMGNAhd" role="2ZW6by">
              <ref role="3uigEE" node="5jVYnMGJD4D" resolve="LaboratoryTestWithNumberResultDataValueVisitor" />
            </node>
            <node concept="37vLTw" id="5jVYnMGNAhe" role="2ZW6bz">
              <ref role="3cqZAo" node="5jVYnMGNAh8" resolve="visitor" />
            </node>
          </node>
          <node concept="3clFbS" id="5jVYnMGNAhf" role="3clFbx">
            <node concept="3cpWs6" id="5jVYnMGNAhg" role="3cqZAp">
              <node concept="2OqwBi" id="5jVYnMGNAhh" role="3cqZAk">
                <node concept="1eOMI4" id="5jVYnMGNAhi" role="2Oq$k0">
                  <node concept="10QFUN" id="5jVYnMGNAhj" role="1eOMHV">
                    <node concept="3uibUv" id="5jVYnMGNAhk" role="10QFUM">
                      <ref role="3uigEE" node="5jVYnMGJD4D" resolve="LaboratoryTestWithNumberResultDataValueVisitor" />
                    </node>
                    <node concept="37vLTw" id="5jVYnMGNAhl" role="10QFUP">
                      <ref role="3cqZAo" node="5jVYnMGNAh8" resolve="visitor" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="5jVYnMGNAhm" role="2OqNvi">
                  <ref role="37wK5l" node="5jVYnMGJD7G" resolve="visit" />
                  <node concept="Xjq3P" id="5jVYnMGNAhn" role="37wK5m" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="5jVYnMGNAho" role="3cqZAp">
          <node concept="10Nm6u" id="5jVYnMGNAhp" role="3cqZAk" />
        </node>
      </node>
      <node concept="2AHcQZ" id="5jVYnMGNAhq" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
      <node concept="3uibUv" id="5jVYnMGV7ZA" role="3clF45">
        <ref role="3uigEE" to="8r9s:2XLt5KVsHET" resolve="LisResponse" />
        <node concept="3uibUv" id="5jVYnMGV8pi" role="11_B2D">
          <ref role="3uigEE" to="wyt6:~Double" resolve="Double" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="7lYCqhv4yr8" role="jymVt" />
    <node concept="3clFb_" id="7lYCqhv3U1k" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="hashCode" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3Tm1VV" id="7lYCqhv3U1l" role="1B3o_S" />
      <node concept="10Oyi0" id="7lYCqhv3U1n" role="3clF45" />
      <node concept="3clFbS" id="7lYCqhv3U1q" role="3clF47">
        <node concept="3clFbF" id="7lYCqhv3U1t" role="3cqZAp">
          <node concept="2YIFZM" id="7lYCqhv4t_e" role="3clFbG">
            <ref role="37wK5l" to="33ny:~Objects.hash(java.lang.Object...):int" resolve="hash" />
            <ref role="1Pybhc" to="33ny:~Objects" resolve="Objects" />
            <node concept="2OqwBi" id="7lYCqhv4Eii" role="37wK5m">
              <node concept="Xjq3P" id="7lYCqhv4t_X" role="2Oq$k0" />
              <node concept="liA8E" id="7lYCqhv4Exc" role="2OqNvi">
                <ref role="37wK5l" to="wyt6:~Object.getClass():java.lang.Class" resolve="getClass" />
              </node>
            </node>
            <node concept="2OqwBi" id="4B5aqq4gByH" role="37wK5m">
              <node concept="Xjq3P" id="4B5aqq4gBqR" role="2Oq$k0" />
              <node concept="2OwXpG" id="4B5aqq4gBCH" role="2OqNvi">
                <ref role="2Oxat5" node="1mAGFBLsNvq" resolve="test" />
              </node>
            </node>
            <node concept="2OqwBi" id="4B5aqq4gBVh" role="37wK5m">
              <node concept="Xjq3P" id="4B5aqq4gBMo" role="2Oq$k0" />
              <node concept="2OwXpG" id="4B5aqq4gC2k" role="2OqNvi">
                <ref role="2Oxat5" node="4B5aqq4g$r$" resolve="specimenType" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="7lYCqhv3U1r" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="7lYCqhv3UxP" role="jymVt" />
    <node concept="3clFb_" id="7lYCqhv3U1u" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="equals" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3Tm1VV" id="7lYCqhv3U1v" role="1B3o_S" />
      <node concept="10P_77" id="7lYCqhv3U1x" role="3clF45" />
      <node concept="37vLTG" id="7lYCqhv3U1y" role="3clF46">
        <property role="TrG5h" value="object" />
        <node concept="3uibUv" id="7lYCqhv3U1z" role="1tU5fm">
          <ref role="3uigEE" to="wyt6:~Object" resolve="Object" />
        </node>
      </node>
      <node concept="3clFbS" id="7lYCqhv3U1A" role="3clF47">
        <node concept="3clFbJ" id="4B5aqq5OJ30" role="3cqZAp">
          <node concept="3clFbS" id="4B5aqq5OJ31" role="3clFbx">
            <node concept="3cpWs6" id="4B5aqq5OJ32" role="3cqZAp">
              <node concept="3clFbT" id="4B5aqq5OJ33" role="3cqZAk">
                <property role="3clFbU" value="true" />
              </node>
            </node>
          </node>
          <node concept="3clFbC" id="4B5aqq5OJ34" role="3clFbw">
            <node concept="Xjq3P" id="4B5aqq5OJ35" role="3uHU7B" />
            <node concept="37vLTw" id="4B5aqq5OJ36" role="3uHU7w">
              <ref role="3cqZAo" node="7lYCqhv3U1y" resolve="object" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="4B5aqq5OJ37" role="3cqZAp">
          <node concept="3clFbS" id="4B5aqq5OJ38" role="3clFbx">
            <node concept="3cpWs6" id="4B5aqq5OJ39" role="3cqZAp">
              <node concept="3clFbT" id="4B5aqq5OJ3a" role="3cqZAk">
                <property role="3clFbU" value="false" />
              </node>
            </node>
          </node>
          <node concept="22lmx$" id="4B5aqq5OJW5" role="3clFbw">
            <node concept="3clFbC" id="4B5aqq5OKlz" role="3uHU7B">
              <node concept="10Nm6u" id="4B5aqq5OKtL" role="3uHU7w" />
              <node concept="37vLTw" id="4B5aqq5OKbr" role="3uHU7B">
                <ref role="3cqZAo" node="7lYCqhv3U1y" resolve="object" />
              </node>
            </node>
            <node concept="3y3z36" id="4B5aqq5OJ3b" role="3uHU7w">
              <node concept="2OqwBi" id="4B5aqq5OJ3c" role="3uHU7B">
                <node concept="Xjq3P" id="4B5aqq5OJ3d" role="2Oq$k0" />
                <node concept="liA8E" id="4B5aqq5OJ3e" role="2OqNvi">
                  <ref role="37wK5l" to="wyt6:~Object.getClass():java.lang.Class" resolve="getClass" />
                </node>
              </node>
              <node concept="2OqwBi" id="4B5aqq5OJ3f" role="3uHU7w">
                <node concept="37vLTw" id="4B5aqq5OJ3g" role="2Oq$k0">
                  <ref role="3cqZAo" node="7lYCqhv3U1y" resolve="object" />
                </node>
                <node concept="liA8E" id="4B5aqq5OJ3h" role="2OqNvi">
                  <ref role="37wK5l" to="wyt6:~Object.getClass():java.lang.Class" resolve="getClass" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="7lYCqhv3Ubu" role="3cqZAp">
          <node concept="3cpWsn" id="7lYCqhv3Ubv" role="3cpWs9">
            <property role="TrG5h" value="other" />
            <node concept="3uibUv" id="7lYCqhv4yVf" role="1tU5fm">
              <ref role="3uigEE" node="1mAGFBLsNvp" resolve="TestPreviousResultAsNumber" />
            </node>
            <node concept="10QFUN" id="7lYCqhv3Ubx" role="33vP2m">
              <node concept="3uibUv" id="7lYCqhv4yIR" role="10QFUM">
                <ref role="3uigEE" node="1mAGFBLsNvp" resolve="TestPreviousResultAsNumber" />
              </node>
              <node concept="37vLTw" id="7lYCqhv3Ubz" role="10QFUP">
                <ref role="3cqZAo" node="7lYCqhv3U1y" resolve="object" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="7lYCqhv3Ub$" role="3cqZAp">
          <node concept="1Wc70l" id="4B5aqq5CtfT" role="3cqZAk">
            <node concept="2YIFZM" id="4B5aqq5CtDR" role="3uHU7w">
              <ref role="1Pybhc" to="33ny:~Objects" resolve="Objects" />
              <ref role="37wK5l" to="33ny:~Objects.equals(java.lang.Object,java.lang.Object):boolean" resolve="equals" />
              <node concept="2OqwBi" id="4B5aqq5CtZt" role="37wK5m">
                <node concept="Xjq3P" id="4B5aqq5CtQx" role="2Oq$k0" />
                <node concept="2OwXpG" id="4B5aqq5CufZ" role="2OqNvi">
                  <ref role="2Oxat5" node="4B5aqq4g$r$" resolve="specimenType" />
                </node>
              </node>
              <node concept="2OqwBi" id="4B5aqq5CuAm" role="37wK5m">
                <node concept="37vLTw" id="4B5aqq5Cut9" role="2Oq$k0">
                  <ref role="3cqZAo" node="7lYCqhv3Ubv" resolve="other" />
                </node>
                <node concept="2OwXpG" id="4B5aqq5CuR9" role="2OqNvi">
                  <ref role="2Oxat5" node="4B5aqq4g$r$" resolve="specimenType" />
                </node>
              </node>
            </node>
            <node concept="2YIFZM" id="4B5aqq5CrKt" role="3uHU7B">
              <ref role="37wK5l" to="33ny:~Objects.equals(java.lang.Object,java.lang.Object):boolean" resolve="equals" />
              <ref role="1Pybhc" to="33ny:~Objects" resolve="Objects" />
              <node concept="2OqwBi" id="4B5aqq5Cs7l" role="37wK5m">
                <node concept="Xjq3P" id="4B5aqq5CrWu" role="2Oq$k0" />
                <node concept="2OwXpG" id="4B5aqq5Csne" role="2OqNvi">
                  <ref role="2Oxat5" node="1mAGFBLsNvq" resolve="test" />
                </node>
              </node>
              <node concept="2OqwBi" id="4B5aqq5CsGj" role="37wK5m">
                <node concept="37vLTw" id="4B5aqq5CszJ" role="2Oq$k0">
                  <ref role="3cqZAo" node="7lYCqhv3Ubv" resolve="other" />
                </node>
                <node concept="2OwXpG" id="4B5aqq6h1vb" role="2OqNvi">
                  <ref role="2Oxat5" node="1mAGFBLsNvq" resolve="test" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="7lYCqhv3U1B" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3Tm1VV" id="1mAGFBLsNvO" role="1B3o_S" />
    <node concept="3uibUv" id="1mAGFBLsNvP" role="1zkMxy">
      <ref role="3uigEE" to="8r9s:1mAGFBLk578" resolve="NumberDataValue" />
    </node>
  </node>
  <node concept="312cEu" id="1mAGFBLlG$Q">
    <property role="3GE5qa" value="data.values" />
    <property role="TrG5h" value="TestResultAsNumber" />
    <node concept="312cEg" id="1mAGFBLnVaQ" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="test" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm6S6" id="1mAGFBLnV90" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBLC7DD" role="1tU5fm" />
    </node>
    <node concept="312cEg" id="4B5aqq4gF4M" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="specimenType" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm6S6" id="4B5aqq4gEMJ" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4gEZ1" role="1tU5fm" />
    </node>
    <node concept="2tJIrI" id="1mAGFBLnVb3" role="jymVt" />
    <node concept="3clFbW" id="1mAGFBLnVbo" role="jymVt">
      <node concept="3cqZAl" id="1mAGFBLnVbq" role="3clF45" />
      <node concept="3Tm1VV" id="1mAGFBLnVbr" role="1B3o_S" />
      <node concept="3clFbS" id="1mAGFBLnVbs" role="3clF47">
        <node concept="3clFbF" id="1mAGFBLnVcq" role="3cqZAp">
          <node concept="37vLTI" id="1mAGFBLnVpW" role="3clFbG">
            <node concept="37vLTw" id="1mAGFBLnVrd" role="37vLTx">
              <ref role="3cqZAo" node="1mAGFBLnVbM" resolve="test" />
            </node>
            <node concept="2OqwBi" id="1mAGFBLnVfN" role="37vLTJ">
              <node concept="Xjq3P" id="1mAGFBLnVcp" role="2Oq$k0" />
              <node concept="2OwXpG" id="1mAGFBLnVjU" role="2OqNvi">
                <ref role="2Oxat5" node="1mAGFBLnVaQ" resolve="test" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4B5aqq4gF_k" role="3cqZAp">
          <node concept="37vLTI" id="4B5aqq4gFXs" role="3clFbG">
            <node concept="37vLTw" id="4B5aqq4gG9k" role="37vLTx">
              <ref role="3cqZAo" node="4B5aqq4gFsM" resolve="specimenType" />
            </node>
            <node concept="2OqwBi" id="4B5aqq4gFDL" role="37vLTJ">
              <node concept="Xjq3P" id="4B5aqq4gF_i" role="2Oq$k0" />
              <node concept="2OwXpG" id="4B5aqq4gFI8" role="2OqNvi">
                <ref role="2Oxat5" node="4B5aqq4gF4M" resolve="specimenType" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="1mAGFBLnVbM" role="3clF46">
        <property role="TrG5h" value="test" />
        <node concept="17QB3L" id="1mAGFBLC7zF" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="4B5aqq4gFsM" role="3clF46">
        <property role="TrG5h" value="specimenType" />
        <node concept="17QB3L" id="4B5aqq4gF$h" role="1tU5fm" />
      </node>
    </node>
    <node concept="2tJIrI" id="1mAGFBLnVsw" role="jymVt" />
    <node concept="3clFb_" id="1mAGFBLEHbp" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestName" />
      <node concept="3Tm1VV" id="1mAGFBLEHbr" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBLEHbs" role="3clF45" />
      <node concept="3clFbS" id="1mAGFBLEHbu" role="3clF47">
        <node concept="3cpWs6" id="1mAGFBLsM2F" role="3cqZAp">
          <node concept="2OqwBi" id="1mAGFBLsM9K" role="3cqZAk">
            <node concept="Xjq3P" id="1mAGFBLsM3u" role="2Oq$k0" />
            <node concept="2OwXpG" id="1mAGFBLsMia" role="2OqNvi">
              <ref role="2Oxat5" node="1mAGFBLnVaQ" resolve="test" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="4B5aqq4gGm6" role="jymVt" />
    <node concept="3clFb_" id="4B5aqq4gGWD" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="getSpecimenType" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="4B5aqq4gGWG" role="3clF47">
        <node concept="3cpWs6" id="4B5aqq4gHa4" role="3cqZAp">
          <node concept="2OqwBi" id="4B5aqq4gHhb" role="3cqZAk">
            <node concept="Xjq3P" id="4B5aqq4gHaj" role="2Oq$k0" />
            <node concept="2OwXpG" id="4B5aqq4gHwX" role="2OqNvi">
              <ref role="2Oxat5" node="4B5aqq4gF4M" resolve="specimenType" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="4B5aqq4gGJJ" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4gGL4" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="5jVYnMGNBtf" role="jymVt" />
    <node concept="3clFb_" id="5jVYnMGNBtg" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="accept" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3Tm1VV" id="5jVYnMGNBth" role="1B3o_S" />
      <node concept="37vLTG" id="5jVYnMGNBtj" role="3clF46">
        <property role="TrG5h" value="visitor" />
        <node concept="3uibUv" id="5jVYnMGNBtk" role="1tU5fm">
          <ref role="3uigEE" to="8r9s:5jVYnMGKCt5" resolve="DataValueVisitor" />
        </node>
      </node>
      <node concept="3clFbS" id="5jVYnMGNBtl" role="3clF47">
        <node concept="3clFbJ" id="5jVYnMGNBtm" role="3cqZAp">
          <node concept="2ZW3vV" id="5jVYnMGNBtn" role="3clFbw">
            <node concept="3uibUv" id="5jVYnMGNBto" role="2ZW6by">
              <ref role="3uigEE" node="5jVYnMGJD4D" resolve="LaboratoryTestWithNumberResultDataValueVisitor" />
            </node>
            <node concept="37vLTw" id="5jVYnMGNBtp" role="2ZW6bz">
              <ref role="3cqZAo" node="5jVYnMGNBtj" resolve="visitor" />
            </node>
          </node>
          <node concept="3clFbS" id="5jVYnMGNBtq" role="3clFbx">
            <node concept="3cpWs6" id="5jVYnMGNBtr" role="3cqZAp">
              <node concept="2OqwBi" id="5jVYnMGNBts" role="3cqZAk">
                <node concept="1eOMI4" id="5jVYnMGNBtt" role="2Oq$k0">
                  <node concept="10QFUN" id="5jVYnMGNBtu" role="1eOMHV">
                    <node concept="3uibUv" id="5jVYnMGNBtv" role="10QFUM">
                      <ref role="3uigEE" node="5jVYnMGJD4D" resolve="LaboratoryTestWithNumberResultDataValueVisitor" />
                    </node>
                    <node concept="37vLTw" id="5jVYnMGNBtw" role="10QFUP">
                      <ref role="3cqZAo" node="5jVYnMGNBtj" resolve="visitor" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="5jVYnMGNBtx" role="2OqNvi">
                  <ref role="37wK5l" node="5jVYnMGJDb7" resolve="visit" />
                  <node concept="Xjq3P" id="5jVYnMGNBty" role="37wK5m" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="5jVYnMGNBtz" role="3cqZAp">
          <node concept="10Nm6u" id="5jVYnMGNBt$" role="3cqZAk" />
        </node>
      </node>
      <node concept="2AHcQZ" id="5jVYnMGNBt_" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
      <node concept="3uibUv" id="5jVYnMGV9f3" role="3clF45">
        <ref role="3uigEE" to="8r9s:2XLt5KVsHET" resolve="LisResponse" />
        <node concept="3uibUv" id="5jVYnMGV9CJ" role="11_B2D">
          <ref role="3uigEE" to="wyt6:~Double" resolve="Double" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="7lYCqhv4zHJ" role="jymVt" />
    <node concept="3clFb_" id="7lYCqhv4zsb" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="hashCode" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3Tm1VV" id="7lYCqhv4zsc" role="1B3o_S" />
      <node concept="10Oyi0" id="7lYCqhv4zsd" role="3clF45" />
      <node concept="3clFbS" id="7lYCqhv4zse" role="3clF47">
        <node concept="3clFbF" id="7lYCqhv4zsf" role="3cqZAp">
          <node concept="2YIFZM" id="7lYCqhv4zsg" role="3clFbG">
            <ref role="37wK5l" to="33ny:~Objects.hash(java.lang.Object...):int" resolve="hash" />
            <ref role="1Pybhc" to="33ny:~Objects" resolve="Objects" />
            <node concept="2OqwBi" id="7lYCqhv4_ru" role="37wK5m">
              <node concept="Xjq3P" id="7lYCqhv4zsh" role="2Oq$k0" />
              <node concept="liA8E" id="7lYCqhv4_J4" role="2OqNvi">
                <ref role="37wK5l" to="wyt6:~Object.getClass():java.lang.Class" resolve="getClass" />
              </node>
            </node>
            <node concept="2OqwBi" id="4B5aqq4gI4v" role="37wK5m">
              <node concept="Xjq3P" id="4B5aqq4gHWD" role="2Oq$k0" />
              <node concept="2OwXpG" id="4B5aqq4gIav" role="2OqNvi">
                <ref role="2Oxat5" node="1mAGFBLnVaQ" resolve="test" />
              </node>
            </node>
            <node concept="2OqwBi" id="4B5aqq4gIt3" role="37wK5m">
              <node concept="Xjq3P" id="4B5aqq4gIka" role="2Oq$k0" />
              <node concept="2OwXpG" id="4B5aqq4gI$6" role="2OqNvi">
                <ref role="2Oxat5" node="4B5aqq4gF4M" resolve="specimenType" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="7lYCqhv4zsj" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="7lYCqhv4zsk" role="jymVt" />
    <node concept="3clFb_" id="7lYCqhv4zsl" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="equals" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3Tm1VV" id="7lYCqhv4zsm" role="1B3o_S" />
      <node concept="10P_77" id="7lYCqhv4zsn" role="3clF45" />
      <node concept="37vLTG" id="7lYCqhv4zso" role="3clF46">
        <property role="TrG5h" value="object" />
        <node concept="3uibUv" id="7lYCqhv4zsp" role="1tU5fm">
          <ref role="3uigEE" to="wyt6:~Object" resolve="Object" />
        </node>
      </node>
      <node concept="3clFbS" id="7lYCqhv4zsq" role="3clF47">
        <node concept="3clFbJ" id="4B5aqq5OO9D" role="3cqZAp">
          <node concept="3clFbS" id="4B5aqq5OO9E" role="3clFbx">
            <node concept="3cpWs6" id="4B5aqq5OO9F" role="3cqZAp">
              <node concept="3clFbT" id="4B5aqq5OO9G" role="3cqZAk">
                <property role="3clFbU" value="true" />
              </node>
            </node>
          </node>
          <node concept="3clFbC" id="4B5aqq5OO9H" role="3clFbw">
            <node concept="Xjq3P" id="4B5aqq5OO9I" role="3uHU7B" />
            <node concept="37vLTw" id="4B5aqq5OO9J" role="3uHU7w">
              <ref role="3cqZAo" node="7lYCqhv4zso" resolve="object" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="4B5aqq5OO9K" role="3cqZAp">
          <node concept="3clFbS" id="4B5aqq5OO9L" role="3clFbx">
            <node concept="3cpWs6" id="4B5aqq5OO9M" role="3cqZAp">
              <node concept="3clFbT" id="4B5aqq5OO9N" role="3cqZAk">
                <property role="3clFbU" value="false" />
              </node>
            </node>
          </node>
          <node concept="22lmx$" id="4B5aqq5OO9O" role="3clFbw">
            <node concept="3clFbC" id="4B5aqq5OO9P" role="3uHU7B">
              <node concept="10Nm6u" id="4B5aqq5OO9Q" role="3uHU7w" />
              <node concept="37vLTw" id="4B5aqq5OO9R" role="3uHU7B">
                <ref role="3cqZAo" node="7lYCqhv4zso" resolve="object" />
              </node>
            </node>
            <node concept="3y3z36" id="4B5aqq5OO9S" role="3uHU7w">
              <node concept="2OqwBi" id="4B5aqq5OO9T" role="3uHU7B">
                <node concept="Xjq3P" id="4B5aqq5OO9U" role="2Oq$k0" />
                <node concept="liA8E" id="4B5aqq5OO9V" role="2OqNvi">
                  <ref role="37wK5l" to="wyt6:~Object.getClass():java.lang.Class" resolve="getClass" />
                </node>
              </node>
              <node concept="2OqwBi" id="4B5aqq5OO9W" role="3uHU7w">
                <node concept="37vLTw" id="4B5aqq5OO9X" role="2Oq$k0">
                  <ref role="3cqZAo" node="7lYCqhv4zso" resolve="object" />
                </node>
                <node concept="liA8E" id="4B5aqq5OO9Y" role="2OqNvi">
                  <ref role="37wK5l" to="wyt6:~Object.getClass():java.lang.Class" resolve="getClass" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="7lYCqhv4zsy" role="3cqZAp">
          <node concept="3cpWsn" id="7lYCqhv4zsz" role="3cpWs9">
            <property role="TrG5h" value="other" />
            <node concept="3uibUv" id="7lYCqhv4$dQ" role="1tU5fm">
              <ref role="3uigEE" node="1mAGFBLlG$Q" resolve="TestResultAsNumber" />
            </node>
            <node concept="10QFUN" id="7lYCqhv4zs$" role="33vP2m">
              <node concept="3uibUv" id="7lYCqhv4$1u" role="10QFUM">
                <ref role="3uigEE" node="1mAGFBLlG$Q" resolve="TestResultAsNumber" />
              </node>
              <node concept="37vLTw" id="7lYCqhv4zs_" role="10QFUP">
                <ref role="3cqZAo" node="7lYCqhv4zso" resolve="object" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="7lYCqhv4zsA" role="3cqZAp">
          <node concept="1Wc70l" id="4B5aqq5CxsR" role="3cqZAk">
            <node concept="2YIFZM" id="4B5aqq5CxsS" role="3uHU7w">
              <ref role="1Pybhc" to="33ny:~Objects" resolve="Objects" />
              <ref role="37wK5l" to="33ny:~Objects.equals(java.lang.Object,java.lang.Object):boolean" resolve="equals" />
              <node concept="2OqwBi" id="4B5aqq5CxsT" role="37wK5m">
                <node concept="Xjq3P" id="4B5aqq5CxsU" role="2Oq$k0" />
                <node concept="2OwXpG" id="4B5aqq5CxsV" role="2OqNvi">
                  <ref role="2Oxat5" node="4B5aqq4gF4M" resolve="specimenType" />
                </node>
              </node>
              <node concept="2OqwBi" id="4B5aqq5CxsW" role="37wK5m">
                <node concept="37vLTw" id="4B5aqq5CxsX" role="2Oq$k0">
                  <ref role="3cqZAo" node="7lYCqhv4zsz" resolve="other" />
                </node>
                <node concept="2OwXpG" id="4B5aqq5CxsY" role="2OqNvi">
                  <ref role="2Oxat5" node="4B5aqq4gF4M" resolve="specimenType" />
                </node>
              </node>
            </node>
            <node concept="2YIFZM" id="4B5aqq5CxsZ" role="3uHU7B">
              <ref role="37wK5l" to="33ny:~Objects.equals(java.lang.Object,java.lang.Object):boolean" resolve="equals" />
              <ref role="1Pybhc" to="33ny:~Objects" resolve="Objects" />
              <node concept="2OqwBi" id="4B5aqq5Cxt0" role="37wK5m">
                <node concept="Xjq3P" id="4B5aqq5Cxt1" role="2Oq$k0" />
                <node concept="2OwXpG" id="4B5aqq5Cxt2" role="2OqNvi">
                  <ref role="2Oxat5" node="1mAGFBLnVaQ" resolve="test" />
                </node>
              </node>
              <node concept="2OqwBi" id="4B5aqq5Cxt3" role="37wK5m">
                <node concept="37vLTw" id="4B5aqq5Cxt4" role="2Oq$k0">
                  <ref role="3cqZAo" node="7lYCqhv4zsz" resolve="other" />
                </node>
                <node concept="2OwXpG" id="4B5aqq6h223" role="2OqNvi">
                  <ref role="2Oxat5" node="1mAGFBLnVaQ" resolve="test" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="7lYCqhv4zsI" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3Tm1VV" id="1mAGFBLlG$R" role="1B3o_S" />
    <node concept="3uibUv" id="1mAGFBLlG_K" role="1zkMxy">
      <ref role="3uigEE" to="8r9s:1mAGFBLk578" resolve="NumberDataValue" />
    </node>
  </node>
  <node concept="312cEu" id="PDjyzkyPDJ">
    <property role="3GE5qa" value="data.booleans" />
    <property role="TrG5h" value="TestResultHasNumberValue" />
    <node concept="312cEg" id="PDjyzkyPDK" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="test" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm6S6" id="PDjyzkyPDL" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkyPDM" role="1tU5fm" />
    </node>
    <node concept="312cEg" id="PDjyzkyPDN" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="specimenType" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm6S6" id="PDjyzkyPDO" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkyPDP" role="1tU5fm" />
    </node>
    <node concept="2tJIrI" id="PDjyzkyPDQ" role="jymVt" />
    <node concept="3clFbW" id="PDjyzkyPDR" role="jymVt">
      <node concept="3cqZAl" id="PDjyzkyPDS" role="3clF45" />
      <node concept="3Tm1VV" id="PDjyzkyPDT" role="1B3o_S" />
      <node concept="3clFbS" id="PDjyzkyPDU" role="3clF47">
        <node concept="3clFbF" id="PDjyzkyPDV" role="3cqZAp">
          <node concept="37vLTI" id="PDjyzkyPDW" role="3clFbG">
            <node concept="37vLTw" id="PDjyzkyPDX" role="37vLTx">
              <ref role="3cqZAo" node="PDjyzkyPE7" resolve="test" />
            </node>
            <node concept="2OqwBi" id="PDjyzkyPDY" role="37vLTJ">
              <node concept="Xjq3P" id="PDjyzkyPDZ" role="2Oq$k0" />
              <node concept="2OwXpG" id="PDjyzkyPE0" role="2OqNvi">
                <ref role="2Oxat5" node="PDjyzkyPDK" resolve="test" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="PDjyzkyPE1" role="3cqZAp">
          <node concept="37vLTI" id="PDjyzkyPE2" role="3clFbG">
            <node concept="37vLTw" id="PDjyzkyPE3" role="37vLTx">
              <ref role="3cqZAo" node="PDjyzkyPE9" resolve="specimenType" />
            </node>
            <node concept="2OqwBi" id="PDjyzkyPE4" role="37vLTJ">
              <node concept="Xjq3P" id="PDjyzkyPE5" role="2Oq$k0" />
              <node concept="2OwXpG" id="PDjyzkyPE6" role="2OqNvi">
                <ref role="2Oxat5" node="PDjyzkyPDN" resolve="specimenType" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="PDjyzkyPE7" role="3clF46">
        <property role="TrG5h" value="test" />
        <node concept="17QB3L" id="PDjyzkyPE8" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="PDjyzkyPE9" role="3clF46">
        <property role="TrG5h" value="specimenType" />
        <node concept="17QB3L" id="PDjyzkyPEa" role="1tU5fm" />
      </node>
    </node>
    <node concept="2tJIrI" id="PDjyzkyPEb" role="jymVt" />
    <node concept="3clFb_" id="PDjyzkyPEc" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestName" />
      <node concept="3Tm1VV" id="PDjyzkyPEd" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkyPEe" role="3clF45" />
      <node concept="3clFbS" id="PDjyzkyPEf" role="3clF47">
        <node concept="3cpWs6" id="PDjyzkyPEg" role="3cqZAp">
          <node concept="2OqwBi" id="PDjyzkyPEh" role="3cqZAk">
            <node concept="Xjq3P" id="PDjyzkyPEi" role="2Oq$k0" />
            <node concept="2OwXpG" id="PDjyzkyPEj" role="2OqNvi">
              <ref role="2Oxat5" node="PDjyzkyPDK" resolve="test" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="PDjyzkyPEk" role="jymVt" />
    <node concept="3clFb_" id="PDjyzkyPEl" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="getSpecimenType" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="PDjyzkyPEm" role="3clF47">
        <node concept="3cpWs6" id="PDjyzkyPEn" role="3cqZAp">
          <node concept="2OqwBi" id="PDjyzkyPEo" role="3cqZAk">
            <node concept="Xjq3P" id="PDjyzkyPEp" role="2Oq$k0" />
            <node concept="2OwXpG" id="PDjyzkyPEq" role="2OqNvi">
              <ref role="2Oxat5" node="PDjyzkyPDN" resolve="specimenType" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="PDjyzkyPEr" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkyPEs" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="5jVYnMGMIvS" role="jymVt" />
    <node concept="3clFb_" id="5jVYnMGMI1T" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="accept" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3Tm1VV" id="5jVYnMGMI1U" role="1B3o_S" />
      <node concept="37vLTG" id="5jVYnMGMI1W" role="3clF46">
        <property role="TrG5h" value="visitor" />
        <node concept="3uibUv" id="5jVYnMGMI1X" role="1tU5fm">
          <ref role="3uigEE" to="8r9s:5jVYnMGKCt5" resolve="DataValueVisitor" />
        </node>
      </node>
      <node concept="3clFbS" id="5jVYnMGMI1Y" role="3clF47">
        <node concept="3clFbJ" id="5jVYnMGMI1Z" role="3cqZAp">
          <node concept="2ZW3vV" id="5jVYnMGMI20" role="3clFbw">
            <node concept="3uibUv" id="5jVYnMGNxIx" role="2ZW6by">
              <ref role="3uigEE" node="5jVYnMGJD4D" resolve="LaboratoryTestWithNumberResultDataValueVisitor" />
            </node>
            <node concept="37vLTw" id="5jVYnMGMI22" role="2ZW6bz">
              <ref role="3cqZAo" node="5jVYnMGMI1W" resolve="visitor" />
            </node>
          </node>
          <node concept="3clFbS" id="5jVYnMGMI23" role="3clFbx">
            <node concept="3cpWs6" id="5jVYnMGMI24" role="3cqZAp">
              <node concept="2OqwBi" id="5jVYnMGMI25" role="3cqZAk">
                <node concept="1eOMI4" id="5jVYnMGMI26" role="2Oq$k0">
                  <node concept="10QFUN" id="5jVYnMGMI27" role="1eOMHV">
                    <node concept="3uibUv" id="5jVYnMGNxN2" role="10QFUM">
                      <ref role="3uigEE" node="5jVYnMGJD4D" resolve="LaboratoryTestWithNumberResultDataValueVisitor" />
                    </node>
                    <node concept="37vLTw" id="5jVYnMGMI29" role="10QFUP">
                      <ref role="3cqZAo" node="5jVYnMGMI1W" resolve="visitor" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="5jVYnMGMI2a" role="2OqNvi">
                  <ref role="37wK5l" node="5jVYnMGJD5t" resolve="visit" />
                  <node concept="Xjq3P" id="5jVYnMGMI2b" role="37wK5m" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="5jVYnMGMI2c" role="3cqZAp">
          <node concept="10Nm6u" id="5jVYnMGMI2d" role="3cqZAk" />
        </node>
      </node>
      <node concept="2AHcQZ" id="5jVYnMGMI2e" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
      <node concept="3uibUv" id="5jVYnMGV7f3" role="3clF45">
        <ref role="3uigEE" to="8r9s:2XLt5KVsHET" resolve="LisResponse" />
        <node concept="3uibUv" id="5jVYnMGV7f4" role="11_B2D">
          <ref role="3uigEE" to="wyt6:~Boolean" resolve="Boolean" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="PDjyzkyPEt" role="jymVt" />
    <node concept="3clFb_" id="PDjyzkyPEu" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="hashCode" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3Tm1VV" id="PDjyzkyPEv" role="1B3o_S" />
      <node concept="10Oyi0" id="PDjyzkyPEw" role="3clF45" />
      <node concept="3clFbS" id="PDjyzkyPEx" role="3clF47">
        <node concept="3clFbF" id="PDjyzkyPEy" role="3cqZAp">
          <node concept="2YIFZM" id="PDjyzkyPEz" role="3clFbG">
            <ref role="1Pybhc" to="33ny:~Objects" resolve="Objects" />
            <ref role="37wK5l" to="33ny:~Objects.hash(java.lang.Object...):int" resolve="hash" />
            <node concept="2OqwBi" id="PDjyzkyPE$" role="37wK5m">
              <node concept="Xjq3P" id="PDjyzkyPE_" role="2Oq$k0" />
              <node concept="liA8E" id="PDjyzkyPEA" role="2OqNvi">
                <ref role="37wK5l" to="wyt6:~Object.getClass():java.lang.Class" resolve="getClass" />
              </node>
            </node>
            <node concept="2OqwBi" id="PDjyzkyPEB" role="37wK5m">
              <node concept="Xjq3P" id="PDjyzkyPEC" role="2Oq$k0" />
              <node concept="2OwXpG" id="PDjyzkyPED" role="2OqNvi">
                <ref role="2Oxat5" node="PDjyzkyPDK" resolve="test" />
              </node>
            </node>
            <node concept="2OqwBi" id="PDjyzkyPEE" role="37wK5m">
              <node concept="Xjq3P" id="PDjyzkyPEF" role="2Oq$k0" />
              <node concept="2OwXpG" id="PDjyzkyPEG" role="2OqNvi">
                <ref role="2Oxat5" node="PDjyzkyPDN" resolve="specimenType" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkyPEH" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="PDjyzkyPEI" role="jymVt" />
    <node concept="3clFb_" id="PDjyzkyPEJ" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="equals" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3Tm1VV" id="PDjyzkyPEK" role="1B3o_S" />
      <node concept="10P_77" id="PDjyzkyPEL" role="3clF45" />
      <node concept="37vLTG" id="PDjyzkyPEM" role="3clF46">
        <property role="TrG5h" value="object" />
        <node concept="3uibUv" id="PDjyzkyPEN" role="1tU5fm">
          <ref role="3uigEE" to="wyt6:~Object" resolve="Object" />
        </node>
      </node>
      <node concept="3clFbS" id="PDjyzkyPEO" role="3clF47">
        <node concept="3clFbJ" id="PDjyzkyPEP" role="3cqZAp">
          <node concept="3clFbS" id="PDjyzkyPEQ" role="3clFbx">
            <node concept="3cpWs6" id="PDjyzkyPER" role="3cqZAp">
              <node concept="3clFbT" id="PDjyzkyPES" role="3cqZAk">
                <property role="3clFbU" value="true" />
              </node>
            </node>
          </node>
          <node concept="3clFbC" id="PDjyzkyPET" role="3clFbw">
            <node concept="Xjq3P" id="PDjyzkyPEU" role="3uHU7B" />
            <node concept="37vLTw" id="PDjyzkyPEV" role="3uHU7w">
              <ref role="3cqZAo" node="PDjyzkyPEM" resolve="object" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="PDjyzkyPEW" role="3cqZAp">
          <node concept="3clFbS" id="PDjyzkyPEX" role="3clFbx">
            <node concept="3cpWs6" id="PDjyzkyPEY" role="3cqZAp">
              <node concept="3clFbT" id="PDjyzkyPEZ" role="3cqZAk">
                <property role="3clFbU" value="false" />
              </node>
            </node>
          </node>
          <node concept="22lmx$" id="PDjyzkyPF0" role="3clFbw">
            <node concept="3clFbC" id="PDjyzkyPF1" role="3uHU7B">
              <node concept="10Nm6u" id="PDjyzkyPF2" role="3uHU7w" />
              <node concept="37vLTw" id="PDjyzkyPF3" role="3uHU7B">
                <ref role="3cqZAo" node="PDjyzkyPEM" resolve="object" />
              </node>
            </node>
            <node concept="3y3z36" id="PDjyzkyPF4" role="3uHU7w">
              <node concept="2OqwBi" id="PDjyzkyPF5" role="3uHU7B">
                <node concept="Xjq3P" id="PDjyzkyPF6" role="2Oq$k0" />
                <node concept="liA8E" id="PDjyzkyPF7" role="2OqNvi">
                  <ref role="37wK5l" to="wyt6:~Object.getClass():java.lang.Class" resolve="getClass" />
                </node>
              </node>
              <node concept="2OqwBi" id="PDjyzkyPF8" role="3uHU7w">
                <node concept="37vLTw" id="PDjyzkyPF9" role="2Oq$k0">
                  <ref role="3cqZAo" node="PDjyzkyPEM" resolve="object" />
                </node>
                <node concept="liA8E" id="PDjyzkyPFa" role="2OqNvi">
                  <ref role="37wK5l" to="wyt6:~Object.getClass():java.lang.Class" resolve="getClass" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="PDjyzkyPFb" role="3cqZAp">
          <node concept="3cpWsn" id="PDjyzkyPFc" role="3cpWs9">
            <property role="TrG5h" value="other" />
            <node concept="3uibUv" id="PDjyzkyPFd" role="1tU5fm">
              <ref role="3uigEE" node="PDjyzkyPDJ" resolve="TestResultHasNumberValue" />
            </node>
            <node concept="10QFUN" id="PDjyzkyPFe" role="33vP2m">
              <node concept="3uibUv" id="PDjyzkyPFf" role="10QFUM">
                <ref role="3uigEE" node="PDjyzkyPDJ" resolve="TestResultHasNumberValue" />
              </node>
              <node concept="37vLTw" id="PDjyzkyPFg" role="10QFUP">
                <ref role="3cqZAo" node="PDjyzkyPEM" resolve="object" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="PDjyzkyPFh" role="3cqZAp">
          <node concept="1Wc70l" id="PDjyzkyPFi" role="3cqZAk">
            <node concept="2YIFZM" id="PDjyzkyPFj" role="3uHU7w">
              <ref role="1Pybhc" to="33ny:~Objects" resolve="Objects" />
              <ref role="37wK5l" to="33ny:~Objects.equals(java.lang.Object,java.lang.Object):boolean" resolve="equals" />
              <node concept="2OqwBi" id="PDjyzkyPFk" role="37wK5m">
                <node concept="Xjq3P" id="PDjyzkyPFl" role="2Oq$k0" />
                <node concept="2OwXpG" id="PDjyzkyPFm" role="2OqNvi">
                  <ref role="2Oxat5" node="PDjyzkyPDN" resolve="specimenType" />
                </node>
              </node>
              <node concept="2OqwBi" id="PDjyzkyPFn" role="37wK5m">
                <node concept="37vLTw" id="PDjyzkyPFo" role="2Oq$k0">
                  <ref role="3cqZAo" node="PDjyzkyPFc" resolve="other" />
                </node>
                <node concept="2OwXpG" id="PDjyzkyPFp" role="2OqNvi">
                  <ref role="2Oxat5" node="PDjyzkyPDN" resolve="specimenType" />
                </node>
              </node>
            </node>
            <node concept="2YIFZM" id="PDjyzkyPFq" role="3uHU7B">
              <ref role="37wK5l" to="33ny:~Objects.equals(java.lang.Object,java.lang.Object):boolean" resolve="equals" />
              <ref role="1Pybhc" to="33ny:~Objects" resolve="Objects" />
              <node concept="2OqwBi" id="PDjyzkyPFr" role="37wK5m">
                <node concept="Xjq3P" id="PDjyzkyPFs" role="2Oq$k0" />
                <node concept="2OwXpG" id="PDjyzkyPFt" role="2OqNvi">
                  <ref role="2Oxat5" node="PDjyzkyPDK" resolve="test" />
                </node>
              </node>
              <node concept="2OqwBi" id="PDjyzkyPFu" role="37wK5m">
                <node concept="37vLTw" id="PDjyzkyPFv" role="2Oq$k0">
                  <ref role="3cqZAo" node="PDjyzkyPFc" resolve="other" />
                </node>
                <node concept="2OwXpG" id="PDjyzkyPFw" role="2OqNvi">
                  <ref role="2Oxat5" node="PDjyzkyPDK" resolve="test" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkyPFx" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3Tm1VV" id="PDjyzkyPFy" role="1B3o_S" />
    <node concept="3uibUv" id="5jVYnMGN_lx" role="1zkMxy">
      <ref role="3uigEE" to="8r9s:1mAGFBLk56n" resolve="BooleanDataValue" />
    </node>
  </node>
  <node concept="3HP615" id="5jVYnMGJD4D">
    <property role="3GE5qa" value="data" />
    <property role="TrG5h" value="LaboratoryTestWithNumberResultDataValueVisitor" />
    <node concept="3clFb_" id="5jVYnMGJD5t" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="visit" />
      <node concept="3clFbS" id="5jVYnMGJD5w" role="3clF47" />
      <node concept="3Tm1VV" id="5jVYnMGJD5x" role="1B3o_S" />
      <node concept="37vLTG" id="5jVYnMGJD5V" role="3clF46">
        <property role="TrG5h" value="dataValue" />
        <node concept="3uibUv" id="5jVYnMGJE5_" role="1tU5fm">
          <ref role="3uigEE" node="PDjyzkyPDJ" resolve="TestResultHasNumberValue" />
        </node>
      </node>
      <node concept="3uibUv" id="5jVYnMGUYaN" role="3clF45">
        <ref role="3uigEE" to="8r9s:2XLt5KVsHET" resolve="LisResponse" />
        <node concept="3uibUv" id="5jVYnMGUYpP" role="11_B2D">
          <ref role="3uigEE" to="wyt6:~Boolean" resolve="Boolean" />
        </node>
      </node>
    </node>
    <node concept="3clFb_" id="5jVYnMGJD7G" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="visit" />
      <node concept="3clFbS" id="5jVYnMGJD7J" role="3clF47" />
      <node concept="3Tm1VV" id="5jVYnMGJD7K" role="1B3o_S" />
      <node concept="37vLTG" id="5jVYnMGJD8s" role="3clF46">
        <property role="TrG5h" value="dataValue" />
        <node concept="3uibUv" id="5jVYnMGJE7y" role="1tU5fm">
          <ref role="3uigEE" node="1mAGFBLsNvp" resolve="TestPreviousResultAsNumber" />
        </node>
      </node>
      <node concept="3uibUv" id="5jVYnMGV6FJ" role="3clF45">
        <ref role="3uigEE" to="8r9s:2XLt5KVsHET" resolve="LisResponse" />
        <node concept="3uibUv" id="5jVYnMGV6Kn" role="11_B2D">
          <ref role="3uigEE" to="wyt6:~Double" resolve="Double" />
        </node>
      </node>
    </node>
    <node concept="3clFb_" id="5jVYnMGJDb7" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="visit" />
      <node concept="3clFbS" id="5jVYnMGJDb8" role="3clF47" />
      <node concept="3Tm1VV" id="5jVYnMGJDb9" role="1B3o_S" />
      <node concept="37vLTG" id="5jVYnMGJDbb" role="3clF46">
        <property role="TrG5h" value="dataValue" />
        <node concept="3uibUv" id="5jVYnMGJE9v" role="1tU5fm">
          <ref role="3uigEE" node="1mAGFBLlG$Q" resolve="TestResultAsNumber" />
        </node>
      </node>
      <node concept="3uibUv" id="5jVYnMGV6MM" role="3clF45">
        <ref role="3uigEE" to="8r9s:2XLt5KVsHET" resolve="LisResponse" />
        <node concept="3uibUv" id="5jVYnMGV6PF" role="11_B2D">
          <ref role="3uigEE" to="wyt6:~Double" resolve="Double" />
        </node>
      </node>
    </node>
    <node concept="3Tm1VV" id="5jVYnMGJD4E" role="1B3o_S" />
  </node>
</model>

