package no.uio.mLab.decisions.data.laboratoryTest.numberResult.runtime;

/*Generated by MPS */

import no.uio.mLab.decisions.core.runtime.LisResponse;

public interface LaboratoryTestWithNumberResultDataValueVisitor {
  LisResponse<Boolean> visit(TestResultHasNumberValue dataValue);
  LisResponse<Double> visit(TestPreviousResultAsNumber dataValue);
  LisResponse<Double> visit(TestResultAsNumber dataValue);
}
