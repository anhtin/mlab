<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:696ac25d-2b68-4160-827c-e7001fe7184d(main@generator)">
  <persistence version="9" />
  <languages>
    <devkit ref="a2eb3a43-fcc2-4200-80dc-c60110c4862d(jetbrains.mps.devkit.templates)" />
  </languages>
  <imports>
    <import index="veyb" ref="r:f97bcc9c-211d-441a-840a-958b18315a7e(no.uio.mLab.decisions.data.laboratoryTest.textualResult.structure)" />
    <import index="86dc" ref="r:cb4e7a32-6962-4d4e-b2da-20a2eb1d1e9e(no.uio.mLab.decisions.data.laboratoryTest.textualResult.runtime)" />
    <import index="aia3" ref="r:856e974b-6e42-49cc-99cd-c3adb4161c21(no.uio.mLab.decisions.data.laboratoryTest.structure)" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1070534058343" name="jetbrains.mps.baseLanguage.structure.NullLiteral" flags="nn" index="10Nm6u" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1212685548494" name="jetbrains.mps.baseLanguage.structure.ClassCreator" flags="nn" index="1pGfFk" />
    </language>
    <language id="b401a680-8325-4110-8fd3-84331ff25bef" name="jetbrains.mps.lang.generator">
      <concept id="1114706874351" name="jetbrains.mps.lang.generator.structure.CopySrcNodeMacro" flags="ln" index="29HgVG">
        <child id="1168024447342" name="sourceNodeQuery" index="3NFExx" />
      </concept>
      <concept id="1095416546421" name="jetbrains.mps.lang.generator.structure.MappingConfiguration" flags="ig" index="bUwia">
        <child id="1167328349397" name="reductionMappingRule" index="3acgRq" />
        <child id="1195502100749" name="preMappingScript" index="1puA0r" />
      </concept>
      <concept id="1177093525992" name="jetbrains.mps.lang.generator.structure.InlineTemplate_RuleConsequence" flags="lg" index="gft3U">
        <child id="1177093586806" name="templateNode" index="gfFT$" />
      </concept>
      <concept id="1167169188348" name="jetbrains.mps.lang.generator.structure.TemplateFunctionParameter_sourceNode" flags="nn" index="30H73N" />
      <concept id="1167169308231" name="jetbrains.mps.lang.generator.structure.BaseMappingRule" flags="ng" index="30H$t8">
        <reference id="1167169349424" name="applicableConcept" index="30HIoZ" />
      </concept>
      <concept id="1167327847730" name="jetbrains.mps.lang.generator.structure.Reduction_MappingRule" flags="lg" index="3aamgX">
        <child id="1169672767469" name="ruleConsequence" index="1lVwrX" />
      </concept>
      <concept id="1195499912406" name="jetbrains.mps.lang.generator.structure.MappingScript" flags="lg" index="1pmfR0">
        <property id="1195595592106" name="scriptKind" index="1v3f2W" />
        <child id="1195501105008" name="codeBlock" index="1pqMTA" />
      </concept>
      <concept id="1195500722856" name="jetbrains.mps.lang.generator.structure.MappingScript_CodeBlock" flags="in" index="1pplIY" />
      <concept id="1195502151594" name="jetbrains.mps.lang.generator.structure.MappingScriptReference" flags="lg" index="1puMqW">
        <reference id="1195502167610" name="mappingScript" index="1puQsG" />
      </concept>
      <concept id="1167945743726" name="jetbrains.mps.lang.generator.structure.IfMacro_Condition" flags="in" index="3IZrLx" />
      <concept id="1168024337012" name="jetbrains.mps.lang.generator.structure.SourceSubstituteMacro_SourceNodeQuery" flags="in" index="3NFfHV" />
      <concept id="1118773211870" name="jetbrains.mps.lang.generator.structure.IfMacro" flags="ln" index="1W57fq">
        <child id="1194989344771" name="alternativeConsequence" index="UU_$l" />
        <child id="1167945861827" name="conditionFunction" index="3IZSJc" />
      </concept>
    </language>
    <language id="760a0a8c-eabb-4521-8bfd-65db761a9ba3" name="jetbrains.mps.baseLanguage.logging">
      <concept id="6332851714983831325" name="jetbrains.mps.baseLanguage.logging.structure.MsgStatement" flags="ng" index="2xdQw9">
        <property id="6332851714983843871" name="severity" index="2xdLsb" />
        <child id="5721587534047265374" name="message" index="9lYJi" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1172008320231" name="jetbrains.mps.lang.smodel.structure.Node_IsNotNullOperation" flags="nn" index="3x8VRR" />
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="bUwia" id="4V3GMfXrF4D">
    <property role="TrG5h" value="dataLaboratoryTestTextResultMain" />
    <node concept="1puMqW" id="4V3GMfXSDGY" role="1puA0r">
      <ref role="1puQsG" node="4V3GMfXSDH1" resolve="dataLaboratoryTestTextResultScript" />
    </node>
    <node concept="3aamgX" id="4B5aqq4e1dZ" role="3acgRq">
      <ref role="30HIoZ" to="veyb:4B5aqq4dEoJ" resolve="LaboratoryTestPreviousResultAsText" />
      <node concept="gft3U" id="4B5aqq4e1ex" role="1lVwrX">
        <node concept="2ShNRf" id="4B5aqq4e1eB" role="gfFT$">
          <node concept="1pGfFk" id="4B5aqq4e1hS" role="2ShVmc">
            <ref role="37wK5l" to="86dc:1mAGFBLsNvu" resolve="TestPreviousResultAsText" />
            <node concept="Xl_RD" id="4B5aqq4W0kI" role="37wK5m">
              <property role="Xl_RC" value="test" />
              <node concept="29HgVG" id="4B5aqq4W0kJ" role="lGtFl">
                <node concept="3NFfHV" id="4B5aqq4W0kK" role="3NFExx">
                  <node concept="3clFbS" id="4B5aqq4W0kL" role="2VODD2">
                    <node concept="3clFbF" id="4B5aqq4W0kM" role="3cqZAp">
                      <node concept="2OqwBi" id="4B5aqq4W0kN" role="3clFbG">
                        <node concept="3TrEf2" id="1I84Bf75Mo5" role="2OqNvi">
                          <ref role="3Tt5mk" to="veyb:6LTgXmMj7Lm" resolve="testReference" />
                        </node>
                        <node concept="30H73N" id="4B5aqq4W0kP" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="Xl_RD" id="4B5aqq4e1pt" role="37wK5m">
              <property role="Xl_RC" value="specimen" />
              <node concept="1W57fq" id="6LTgXmMpK0c" role="lGtFl">
                <node concept="3IZrLx" id="6LTgXmMpK0d" role="3IZSJc">
                  <node concept="3clFbS" id="6LTgXmMpK0e" role="2VODD2">
                    <node concept="3clFbF" id="6LTgXmMpKV6" role="3cqZAp">
                      <node concept="2OqwBi" id="6LTgXmMpMyA" role="3clFbG">
                        <node concept="2OqwBi" id="6LTgXmMpLgx" role="2Oq$k0">
                          <node concept="30H73N" id="6LTgXmMpKV5" role="2Oq$k0" />
                          <node concept="3TrEf2" id="1I84Bf75NxW" role="2OqNvi">
                            <ref role="3Tt5mk" to="veyb:4B5aqq3QF7a" resolve="specimenTypeReference" />
                          </node>
                        </node>
                        <node concept="3x8VRR" id="6LTgXmMpMZX" role="2OqNvi" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="gft3U" id="6LTgXmMpNvJ" role="UU_$l">
                  <node concept="10Nm6u" id="6LTgXmMpNMa" role="gfFT$" />
                </node>
              </node>
              <node concept="29HgVG" id="4B5aqq4e1X3" role="lGtFl">
                <node concept="3NFfHV" id="4B5aqq4e1X4" role="3NFExx">
                  <node concept="3clFbS" id="4B5aqq4e1X5" role="2VODD2">
                    <node concept="3clFbF" id="4B5aqq4e1Xb" role="3cqZAp">
                      <node concept="2OqwBi" id="4B5aqq4e1X6" role="3clFbG">
                        <node concept="3TrEf2" id="1I84Bf75MX2" role="2OqNvi">
                          <ref role="3Tt5mk" to="veyb:4B5aqq3QF7a" resolve="specimenTypeReference" />
                        </node>
                        <node concept="30H73N" id="4B5aqq4e1Xa" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="4B5aqq4e2j9" role="3acgRq">
      <ref role="30HIoZ" to="veyb:4B5aqq4dEoN" resolve="LaboratoryTestResultAsText" />
      <node concept="gft3U" id="4B5aqq4e2ja" role="1lVwrX">
        <node concept="2ShNRf" id="4B5aqq4e2jb" role="gfFT$">
          <node concept="1pGfFk" id="4B5aqq4e2jc" role="2ShVmc">
            <ref role="37wK5l" to="86dc:1mAGFBLnVbo" resolve="TestResultAsText" />
            <node concept="Xl_RD" id="1I84Bf75O1T" role="37wK5m">
              <property role="Xl_RC" value="test" />
              <node concept="29HgVG" id="1I84Bf75O1U" role="lGtFl">
                <node concept="3NFfHV" id="1I84Bf75O1V" role="3NFExx">
                  <node concept="3clFbS" id="1I84Bf75O1W" role="2VODD2">
                    <node concept="3clFbF" id="1I84Bf75O1X" role="3cqZAp">
                      <node concept="2OqwBi" id="1I84Bf75O1Y" role="3clFbG">
                        <node concept="3TrEf2" id="1I84Bf75O1Z" role="2OqNvi">
                          <ref role="3Tt5mk" to="veyb:6LTgXmMj7Lm" resolve="testReference" />
                        </node>
                        <node concept="30H73N" id="1I84Bf75O20" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="Xl_RD" id="1I84Bf75Qjf" role="37wK5m">
              <property role="Xl_RC" value="specimen" />
              <node concept="1W57fq" id="1I84Bf75Qjg" role="lGtFl">
                <node concept="3IZrLx" id="1I84Bf75Qjh" role="3IZSJc">
                  <node concept="3clFbS" id="1I84Bf75Qji" role="2VODD2">
                    <node concept="3clFbF" id="1I84Bf75Qjj" role="3cqZAp">
                      <node concept="2OqwBi" id="1I84Bf75Qjk" role="3clFbG">
                        <node concept="2OqwBi" id="1I84Bf75Qjl" role="2Oq$k0">
                          <node concept="30H73N" id="1I84Bf75Qjm" role="2Oq$k0" />
                          <node concept="3TrEf2" id="1I84Bf75Qjn" role="2OqNvi">
                            <ref role="3Tt5mk" to="veyb:4B5aqq3QF7a" resolve="specimenTypeReference" />
                          </node>
                        </node>
                        <node concept="3x8VRR" id="1I84Bf75Qjo" role="2OqNvi" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="gft3U" id="1I84Bf75Qjp" role="UU_$l">
                  <node concept="10Nm6u" id="1I84Bf75Qjq" role="gfFT$" />
                </node>
              </node>
              <node concept="29HgVG" id="1I84Bf75Qjr" role="lGtFl">
                <node concept="3NFfHV" id="1I84Bf75Qjs" role="3NFExx">
                  <node concept="3clFbS" id="1I84Bf75Qjt" role="2VODD2">
                    <node concept="3clFbF" id="1I84Bf75Qju" role="3cqZAp">
                      <node concept="2OqwBi" id="1I84Bf75Qjv" role="3clFbG">
                        <node concept="3TrEf2" id="1I84Bf75Qjw" role="2OqNvi">
                          <ref role="3Tt5mk" to="veyb:4B5aqq3QF7a" resolve="specimenTypeReference" />
                        </node>
                        <node concept="30H73N" id="1I84Bf75Qjx" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="PDjyzkyOkU" role="3acgRq">
      <ref role="30HIoZ" to="veyb:PDjyzky6xW" resolve="LaboratoryTestHasResultAsText" />
      <node concept="gft3U" id="PDjyzkyOms" role="1lVwrX">
        <node concept="2ShNRf" id="PDjyzkyOmy" role="gfFT$">
          <node concept="1pGfFk" id="PDjyzkyOpB" role="2ShVmc">
            <ref role="37wK5l" to="86dc:PDjyzkyH1o" resolve="TestResultHasTextValue" />
            <node concept="Xl_RD" id="1I84Bf75OB4" role="37wK5m">
              <property role="Xl_RC" value="test" />
              <node concept="29HgVG" id="1I84Bf75OB5" role="lGtFl">
                <node concept="3NFfHV" id="1I84Bf75OB6" role="3NFExx">
                  <node concept="3clFbS" id="1I84Bf75OB7" role="2VODD2">
                    <node concept="3clFbF" id="1I84Bf75OB8" role="3cqZAp">
                      <node concept="2OqwBi" id="1I84Bf75OB9" role="3clFbG">
                        <node concept="3TrEf2" id="1I84Bf75OBa" role="2OqNvi">
                          <ref role="3Tt5mk" to="veyb:6LTgXmMj7Lm" resolve="testReference" />
                        </node>
                        <node concept="30H73N" id="1I84Bf75OBb" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="Xl_RD" id="1I84Bf75QIm" role="37wK5m">
              <property role="Xl_RC" value="specimen" />
              <node concept="1W57fq" id="1I84Bf75QIn" role="lGtFl">
                <node concept="3IZrLx" id="1I84Bf75QIo" role="3IZSJc">
                  <node concept="3clFbS" id="1I84Bf75QIp" role="2VODD2">
                    <node concept="3clFbF" id="1I84Bf75QIq" role="3cqZAp">
                      <node concept="2OqwBi" id="1I84Bf75QIr" role="3clFbG">
                        <node concept="2OqwBi" id="1I84Bf75QIs" role="2Oq$k0">
                          <node concept="30H73N" id="1I84Bf75QIt" role="2Oq$k0" />
                          <node concept="3TrEf2" id="1I84Bf75QIu" role="2OqNvi">
                            <ref role="3Tt5mk" to="veyb:4B5aqq3QF7a" resolve="specimenTypeReference" />
                          </node>
                        </node>
                        <node concept="3x8VRR" id="1I84Bf75QIv" role="2OqNvi" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="gft3U" id="1I84Bf75QIw" role="UU_$l">
                  <node concept="10Nm6u" id="1I84Bf75QIx" role="gfFT$" />
                </node>
              </node>
              <node concept="29HgVG" id="1I84Bf75QIy" role="lGtFl">
                <node concept="3NFfHV" id="1I84Bf75QIz" role="3NFExx">
                  <node concept="3clFbS" id="1I84Bf75QI$" role="2VODD2">
                    <node concept="3clFbF" id="1I84Bf75QI_" role="3cqZAp">
                      <node concept="2OqwBi" id="1I84Bf75QIA" role="3clFbG">
                        <node concept="3TrEf2" id="1I84Bf75QIB" role="2OqNvi">
                          <ref role="3Tt5mk" to="veyb:4B5aqq3QF7a" resolve="specimenTypeReference" />
                        </node>
                        <node concept="30H73N" id="1I84Bf75QIC" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="1pmfR0" id="4V3GMfXSDH1">
    <property role="TrG5h" value="dataLaboratoryTestTextResultScript" />
    <property role="1v3f2W" value="pre_processing" />
    <node concept="1pplIY" id="4V3GMfXSDH2" role="1pqMTA">
      <node concept="3clFbS" id="4V3GMfXSDH3" role="2VODD2">
        <node concept="2xdQw9" id="4V3GMfXSDHe" role="3cqZAp">
          <property role="2xdLsb" value="debug" />
          <node concept="Xl_RD" id="4V3GMfXSDHg" role="9lYJi">
            <property role="Xl_RC" value="data.laboratoryTests.textualResult" />
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

