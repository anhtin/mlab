<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:13bcfbcf-6981-4a0f-b575-3d80ade7a177(no.uio.mLab.decisions.data.laboratoryTest.textualResult.behavior)">
  <persistence version="9" />
  <languages>
    <use id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior" version="1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="1yj" ref="r:0c887d67-81ab-420f-9a35-a9f6bf96c975(no.uio.mLab.shared.behavior)" />
    <import index="vbok" ref="r:6cc208d3-a370-49ca-a6ac-ded97805a924(no.uio.mLab.shared.structure)" />
    <import index="zf0i" ref="r:8040e133-faf5-468f-9abc-4d6ff2d83b11(no.uio.mLab.decisions.data.laboratoryTest.textualResult.translations)" />
    <import index="wb6c" ref="r:19d15037-83c1-4ae2-b66a-763828cb8279(no.uio.mLab.decisions.core.behavior)" />
    <import index="7f9y" ref="r:790ac37e-42d3-4dd3-977c-e8fe9cc56a45(no.uio.mLab.decisions.core.structure)" />
    <import index="veyb" ref="r:f97bcc9c-211d-441a-840a-958b18315a7e(no.uio.mLab.decisions.data.laboratoryTest.textualResult.structure)" />
    <import index="hyw5" ref="r:b77da972-4430-4cfb-8552-374583961329(no.uio.mLab.decisions.core.utils)" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" />
  </imports>
  <registry>
    <language id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior">
      <concept id="6496299201655527393" name="jetbrains.mps.lang.behavior.structure.LocalBehaviorMethodCall" flags="nn" index="BsUDl" />
      <concept id="1225194240794" name="jetbrains.mps.lang.behavior.structure.ConceptBehavior" flags="ng" index="13h7C7">
        <reference id="1225194240799" name="concept" index="13h7C2" />
        <child id="1225194240805" name="method" index="13h7CS" />
        <child id="1225194240801" name="constructor" index="13h7CW" />
      </concept>
      <concept id="1225194413805" name="jetbrains.mps.lang.behavior.structure.ConceptConstructorDeclaration" flags="in" index="13hLZK" />
      <concept id="1225194472830" name="jetbrains.mps.lang.behavior.structure.ConceptMethodDeclaration" flags="ng" index="13i0hz">
        <property id="5864038008284099149" name="isStatic" index="2Ki8OM" />
        <property id="1225194472832" name="isVirtual" index="13i0it" />
        <property id="1225194472834" name="isAbstract" index="13i0iv" />
        <reference id="1225194472831" name="overriddenMethod" index="13i0hy" />
      </concept>
      <concept id="1225194628440" name="jetbrains.mps.lang.behavior.structure.SuperNodeExpression" flags="nn" index="13iAh5">
        <reference id="5299096511375896640" name="superConcept" index="3eA5LN" />
      </concept>
      <concept id="1225194691553" name="jetbrains.mps.lang.behavior.structure.ThisNodeExpression" flags="nn" index="13iPFW" />
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="4836112446988635817" name="jetbrains.mps.baseLanguage.structure.UndefinedType" flags="in" index="2jxLKc" />
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1173175405605" name="jetbrains.mps.baseLanguage.structure.ArrayAccessExpression" flags="nn" index="AH0OO">
        <child id="1173175577737" name="index" index="AHEQo" />
        <child id="1173175590490" name="array" index="AHHXb" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1070534058343" name="jetbrains.mps.baseLanguage.structure.NullLiteral" flags="nn" index="10Nm6u" />
      <concept id="1070534760951" name="jetbrains.mps.baseLanguage.structure.ArrayType" flags="in" index="10Q1$e">
        <child id="1070534760952" name="componentType" index="10Q1$1" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123152" name="jetbrains.mps.baseLanguage.structure.EqualsExpression" flags="nn" index="3clFbC" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1154542696413" name="jetbrains.mps.baseLanguage.structure.ArrayCreatorWithInitializer" flags="nn" index="3g6Rrh">
        <child id="1154542793668" name="componentType" index="3g7fb8" />
        <child id="1154542803372" name="initValue" index="3g7hyw" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="5497648299878491908" name="jetbrains.mps.baseLanguage.structure.BaseVariableReference" flags="nn" index="1M0zk4">
        <reference id="5497648299878491909" name="baseVariableDeclaration" index="1M0zk5" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
      <concept id="1146644623116" name="jetbrains.mps.baseLanguage.structure.PrivateVisibility" flags="nn" index="3Tm6S6" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1204834851141" name="jetbrains.mps.lang.smodel.structure.PoundExpression" flags="ng" index="25Kdxt">
        <child id="1204834868751" name="expression" index="25KhWn" />
      </concept>
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="7453996997717780434" name="jetbrains.mps.lang.smodel.structure.Node_GetSConceptOperation" flags="nn" index="2yIwOk" />
      <concept id="1883223317721008708" name="jetbrains.mps.lang.smodel.structure.IfInstanceOfStatement" flags="nn" index="Jncv_">
        <reference id="1883223317721008712" name="nodeConcept" index="JncvD" />
        <child id="1883223317721008709" name="body" index="Jncv$" />
        <child id="1883223317721008711" name="variable" index="JncvA" />
        <child id="1883223317721008710" name="nodeExpression" index="JncvB" />
      </concept>
      <concept id="1883223317721008713" name="jetbrains.mps.lang.smodel.structure.IfInstanceOfVariable" flags="ng" index="JncvC" />
      <concept id="1883223317721107059" name="jetbrains.mps.lang.smodel.structure.IfInstanceOfVarReference" flags="nn" index="Jnkvi" />
      <concept id="1139621453865" name="jetbrains.mps.lang.smodel.structure.Node_IsInstanceOfOperation" flags="nn" index="1mIQ4w">
        <child id="1177027386292" name="conceptArgument" index="cj9EA" />
      </concept>
      <concept id="1172008320231" name="jetbrains.mps.lang.smodel.structure.Node_IsNotNullOperation" flags="nn" index="3x8VRR" />
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2" />
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="13h7C7" id="4V3GMfXwxjp">
    <property role="3GE5qa" value="shared" />
    <ref role="13h7C2" to="veyb:4V3GMfXwlp$" resolve="ITranslatableTextResultLaboratoryTestDataConcept" />
    <node concept="13i0hz" id="4V3GMfXwxj$" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getDisplayTranslations" />
      <node concept="3Tm1VV" id="4V3GMfXwxj_" role="1B3o_S" />
      <node concept="3uibUv" id="4V3GMfXwxkv" role="3clF45">
        <ref role="3uigEE" to="zf0i:4zMac8rUNtP" resolve="ITextResultLaboratoryTestDataTranslations" />
      </node>
      <node concept="3clFbS" id="4V3GMfXwxjB" role="3clF47">
        <node concept="3clFbF" id="4V3GMfXwxtp" role="3cqZAp">
          <node concept="10M0yZ" id="4V3GMfXwxtM" role="3clFbG">
            <ref role="3cqZAo" to="zf0i:4zMac8rVBeK" resolve="displayTranslations" />
            <ref role="1PxDUh" to="zf0i:4zMac8rUNsN" resolve="TextResultLaboratoryTestDataTranslationProvider" />
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="4V3GMfXwxuj" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getGenerationTranslations" />
      <node concept="3Tm1VV" id="4V3GMfXwxuk" role="1B3o_S" />
      <node concept="3uibUv" id="4V3GMfXwxul" role="3clF45">
        <ref role="3uigEE" to="zf0i:4zMac8rUNtP" resolve="ITextResultLaboratoryTestDataTranslations" />
      </node>
      <node concept="3clFbS" id="4V3GMfXwxum" role="3clF47">
        <node concept="3clFbF" id="4V3GMfXwxun" role="3cqZAp">
          <node concept="10M0yZ" id="4V3GMfXwxwl" role="3clFbG">
            <ref role="3cqZAo" to="zf0i:1Hxyv4DUmfq" resolve="generationTranslations" />
            <ref role="1PxDUh" to="zf0i:4zMac8rUNsN" resolve="TextResultLaboratoryTestDataTranslationProvider" />
          </node>
        </node>
      </node>
    </node>
    <node concept="13hLZK" id="4V3GMfXwxjq" role="13h7CW">
      <node concept="3clFbS" id="4V3GMfXwxjr" role="2VODD2" />
    </node>
  </node>
  <node concept="13h7C7" id="4B5aqq4ec9R">
    <property role="3GE5qa" value="base.data.values" />
    <ref role="13h7C2" to="veyb:4B5aqq4dEoJ" resolve="LaboratoryTestPreviousResultAsText" />
    <node concept="13hLZK" id="4B5aqq4ec9S" role="13h7CW">
      <node concept="3clFbS" id="4B5aqq4ec9T" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="4B5aqq4eca2" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="4B5aqq4eca3" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq4eca8" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4eclq" role="3cqZAp">
          <node concept="2OqwBi" id="4B5aqq4ecsm" role="3clFbG">
            <node concept="BsUDl" id="4B5aqq4eclp" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXwxj$" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="4B5aqq4ecyb" role="2OqNvi">
              <ref role="37wK5l" to="zf0i:4V3GMfXwRAF" resolve="getLaboratoryTestPreviousResultAsTextAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="4B5aqq4eca9" role="3clF45" />
    </node>
    <node concept="13i0hz" id="4B5aqq4ecae" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="4B5aqq4ecaf" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq4ecak" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4ecB7" role="3cqZAp">
          <node concept="2OqwBi" id="4B5aqq4ecB9" role="3clFbG">
            <node concept="BsUDl" id="4B5aqq4ecBa" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXwxj$" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="4B5aqq4ecBb" role="2OqNvi">
              <ref role="37wK5l" to="zf0i:4V3GMfXwRCA" resolve="getLaboratoryTestPreviousResultAsTextDescription" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="4B5aqq4ecal" role="3clF45" />
    </node>
    <node concept="13i0hz" id="4B5aqq4ecaq" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getGenerationAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:4QUW3efxqUv" resolve="getGenerationAlias" />
      <node concept="3Tm1VV" id="4B5aqq4ecar" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq4ecaw" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4eca_" role="3cqZAp">
          <node concept="2OqwBi" id="4B5aqq4ecDi" role="3clFbG">
            <node concept="BsUDl" id="4B5aqq4ecEi" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXwxuj" resolve="getGenerationTranslations" />
            </node>
            <node concept="liA8E" id="4B5aqq4ecDk" role="2OqNvi">
              <ref role="37wK5l" to="zf0i:4V3GMfXwRAF" resolve="getLaboratoryTestPreviousResultAsTextAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="4B5aqq4ecax" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmMpxVE" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getAliasWithSpecimenType" />
      <ref role="13i0hy" node="6LTgXmMlgN$" resolve="getAliasWithSpecimenType" />
      <node concept="3Tm1VV" id="6LTgXmMpxVF" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmMpxVN" role="3clF47">
        <node concept="3clFbF" id="6LTgXmMpxVS" role="3cqZAp">
          <node concept="2OqwBi" id="6LTgXmMpyih" role="3clFbG">
            <node concept="BsUDl" id="6LTgXmMpyii" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXwxj$" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6LTgXmMpyij" role="2OqNvi">
              <ref role="37wK5l" to="zf0i:4B5aqq4dEr0" resolve="getLaboratoryTestPreviousResultWithSpecimenAsTextAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmNei_Y" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmMpxVT" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getDescriptionWithSpecimenType" />
      <ref role="13i0hy" node="6LTgXmMlq1k" resolve="getDescriptionWithSpecimenType" />
      <node concept="3Tm1VV" id="6LTgXmMpxVU" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmMpxVZ" role="3clF47">
        <node concept="3clFbF" id="6LTgXmMpxW4" role="3cqZAp">
          <node concept="2OqwBi" id="6LTgXmMpyoV" role="3clFbG">
            <node concept="BsUDl" id="6LTgXmMpyoW" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXwxj$" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6LTgXmMpyoX" role="2OqNvi">
              <ref role="37wK5l" to="zf0i:4B5aqq4dEr4" resolve="getLaboratoryTestPreviousResultWithSpecimenAsTextDescription" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmNeiEz" role="3clF45" />
    </node>
    <node concept="13i0hz" id="4B5aqq4ecaA" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getDisplayKeywords" />
      <ref role="13i0hy" to="1yj:4B5aqq3ZFNV" resolve="getDisplayKeywords" />
      <node concept="3Tm1VV" id="4B5aqq4ecaB" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq4ecaK" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4efQ5" role="3cqZAp">
          <node concept="BsUDl" id="4B5aqq4efQ4" role="3clFbG">
            <ref role="37wK5l" node="4B5aqq4ecF0" resolve="getLocalizedKeywords" />
            <node concept="BsUDl" id="4B5aqq4efQW" role="37wK5m">
              <ref role="37wK5l" node="4V3GMfXwxj$" resolve="getDisplayTranslations" />
            </node>
          </node>
        </node>
      </node>
      <node concept="10Q1$e" id="4B5aqq4ecaL" role="3clF45">
        <node concept="17QB3L" id="4B5aqq4ecaM" role="10Q1$1" />
      </node>
    </node>
    <node concept="13i0hz" id="4B5aqq4ecaR" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getGenerationKeywords" />
      <ref role="13i0hy" to="1yj:4B5aqq3ZFQj" resolve="getGenerationKeywords" />
      <node concept="3Tm1VV" id="4B5aqq4ecaS" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq4ecb1" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4ecb7" role="3cqZAp">
          <node concept="BsUDl" id="4B5aqq4efOp" role="3clFbG">
            <ref role="37wK5l" node="4B5aqq4ecF0" resolve="getLocalizedKeywords" />
            <node concept="BsUDl" id="4B5aqq4efP7" role="37wK5m">
              <ref role="37wK5l" node="4V3GMfXwxuj" resolve="getGenerationTranslations" />
            </node>
          </node>
        </node>
      </node>
      <node concept="10Q1$e" id="4B5aqq4ecb2" role="3clF45">
        <node concept="17QB3L" id="4B5aqq4ecb3" role="10Q1$1" />
      </node>
    </node>
    <node concept="13i0hz" id="4B5aqq4ecF0" role="13h7CS">
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getLocalizedKeywords" />
      <node concept="3Tm6S6" id="4B5aqq4ecFU" role="1B3o_S" />
      <node concept="10Q1$e" id="4B5aqq4ecG9" role="3clF45">
        <node concept="17QB3L" id="4B5aqq4ecG5" role="10Q1$1" />
      </node>
      <node concept="3clFbS" id="4B5aqq4ecF3" role="3clF47">
        <node concept="3cpWs8" id="4B5aqq4ecI9" role="3cqZAp">
          <node concept="3cpWsn" id="4B5aqq4ecIc" role="3cpWs9">
            <property role="TrG5h" value="firstKeyword" />
            <node concept="17QB3L" id="4B5aqq4ecI8" role="1tU5fm" />
            <node concept="2OqwBi" id="4B5aqq4ecPU" role="33vP2m">
              <node concept="37vLTw" id="4B5aqq4ecIZ" role="2Oq$k0">
                <ref role="3cqZAo" node="4B5aqq4ecHn" resolve="translations" />
              </node>
              <node concept="liA8E" id="4B5aqq4ecVC" role="2OqNvi">
                <ref role="37wK5l" to="zf0i:4B5aqq4dF48" resolve="getLaboratoryTestPreviousResultWithSpecimenAsTextKeywordPreviousTextResult" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="4B5aqq4edva" role="3cqZAp">
          <node concept="3cpWsn" id="4B5aqq4edvd" role="3cpWs9">
            <property role="TrG5h" value="secondKeyword" />
            <node concept="17QB3L" id="4B5aqq4edve" role="1tU5fm" />
            <node concept="2OqwBi" id="4B5aqq4edvf" role="33vP2m">
              <node concept="37vLTw" id="4B5aqq4edvg" role="2Oq$k0">
                <ref role="3cqZAo" node="4B5aqq4ecHn" resolve="translations" />
              </node>
              <node concept="liA8E" id="4B5aqq4edvh" role="2OqNvi">
                <ref role="37wK5l" to="zf0i:4B5aqq4dF4c" resolve="getLaboratoryTestPreviousResultWithSpecimenAsTextKeywordWithSpecimen" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4B5aqq4ed_g" role="3cqZAp">
          <node concept="2ShNRf" id="4B5aqq4ed_c" role="3clFbG">
            <node concept="3g6Rrh" id="4B5aqq4edKB" role="2ShVmc">
              <node concept="17QB3L" id="4B5aqq4edBu" role="3g7fb8" />
              <node concept="37vLTw" id="4B5aqq4edRP" role="3g7hyw">
                <ref role="3cqZAo" node="4B5aqq4ecIc" resolve="firstKeyword" />
              </node>
              <node concept="37vLTw" id="4B5aqq4edSs" role="3g7hyw">
                <ref role="3cqZAo" node="4B5aqq4edvd" resolve="secondKeyword" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="4B5aqq4ecHn" role="3clF46">
        <property role="TrG5h" value="translations" />
        <node concept="3uibUv" id="4B5aqq4ecHm" role="1tU5fm">
          <ref role="3uigEE" to="zf0i:4zMac8rUNtP" resolve="ITextResultLaboratoryTestDataTranslations" />
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="4B5aqq4e$yB" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayUnit" />
      <ref role="13i0hy" to="wb6c:1mAGFBKFXUa" resolve="getDisplayUnit" />
      <node concept="3Tm1VV" id="4B5aqq4e$yC" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq4e$yD" role="3clF47">
        <node concept="Jncv_" id="6LTgXmMI1Yt" role="3cqZAp">
          <ref role="JncvD" to="7f9y:6LTgXmMvpq9" resolve="IEntityReference" />
          <node concept="2OqwBi" id="6LTgXmMI2qT" role="JncvB">
            <node concept="13iPFW" id="6LTgXmMI2a1" role="2Oq$k0" />
            <node concept="3TrEf2" id="6LTgXmNepGq" role="2OqNvi">
              <ref role="3Tt5mk" to="veyb:6LTgXmMj7Lm" resolve="testReference" />
            </node>
          </node>
          <node concept="3clFbS" id="6LTgXmMI1Yx" role="Jncv$">
            <node concept="Jncv_" id="4B5aqq4e$yE" role="3cqZAp">
              <ref role="JncvD" to="vbok:7lYCqhujn5u" resolve="IHasUnit" />
              <node concept="3clFbS" id="4B5aqq4e$yK" role="Jncv$">
                <node concept="3cpWs6" id="4B5aqq4e$yL" role="3cqZAp">
                  <node concept="2OqwBi" id="4B5aqq4e$yM" role="3cqZAk">
                    <node concept="Jnkvi" id="4B5aqq4e$yN" role="2Oq$k0">
                      <ref role="1M0zk5" node="4B5aqq4e$yP" resolve="target" />
                    </node>
                    <node concept="2qgKlT" id="4B5aqq4e$yO" role="2OqNvi">
                      <ref role="37wK5l" to="1yj:7lYCqhujn6u" resolve="getDisplayUnit" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="JncvC" id="4B5aqq4e$yP" role="JncvA">
                <property role="TrG5h" value="target" />
                <node concept="2jxLKc" id="4B5aqq4e$yQ" role="1tU5fm" />
              </node>
              <node concept="2OqwBi" id="6LTgXmMI4bK" role="JncvB">
                <node concept="Jnkvi" id="6LTgXmMI3Ut" role="2Oq$k0">
                  <ref role="1M0zk5" node="6LTgXmMI1Yz" resolve="entityRef" />
                </node>
                <node concept="3TrEf2" id="6LTgXmMI4vb" role="2OqNvi">
                  <ref role="3Tt5mk" to="7f9y:4QUW3efv2jv" resolve="target" />
                </node>
              </node>
            </node>
          </node>
          <node concept="JncvC" id="6LTgXmMI1Yz" role="JncvA">
            <property role="TrG5h" value="entityRef" />
            <node concept="2jxLKc" id="6LTgXmMI1Y$" role="1tU5fm" />
          </node>
        </node>
        <node concept="3clFbF" id="4B5aqq4e$yR" role="3cqZAp">
          <node concept="10Nm6u" id="4B5aqq4e$yS" role="3clFbG" />
        </node>
      </node>
      <node concept="17QB3L" id="4B5aqq4e$yT" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="4B5aqq4en5S">
    <property role="3GE5qa" value="base.data.values" />
    <ref role="13h7C2" to="veyb:4B5aqq4dEoN" resolve="LaboratoryTestResultAsText" />
    <node concept="13hLZK" id="4B5aqq4en5T" role="13h7CW">
      <node concept="3clFbS" id="4B5aqq4en5U" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="4B5aqq4en63" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="4B5aqq4en64" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq4en69" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4enhr" role="3cqZAp">
          <node concept="2OqwBi" id="4B5aqq4enon" role="3clFbG">
            <node concept="BsUDl" id="4B5aqq4enhq" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXwxj$" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="4B5aqq4enuc" role="2OqNvi">
              <ref role="37wK5l" to="zf0i:4V3GMfXwkrq" resolve="getLaboratoryTestResultAsTextAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="4B5aqq4en6a" role="3clF45" />
    </node>
    <node concept="13i0hz" id="4B5aqq4en6f" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="4B5aqq4en6g" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq4en6l" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4en6q" role="3cqZAp">
          <node concept="2OqwBi" id="4B5aqq4enyY" role="3clFbG">
            <node concept="BsUDl" id="4B5aqq4enyZ" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXwxj$" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="4B5aqq4enz0" role="2OqNvi">
              <ref role="37wK5l" to="zf0i:4V3GMfXwkse" resolve="getLaboratoryTestResultAsTextDescription" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="4B5aqq4en6m" role="3clF45" />
    </node>
    <node concept="13i0hz" id="4B5aqq4en6r" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getGenerationAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:4QUW3efxqUv" resolve="getGenerationAlias" />
      <node concept="3Tm1VV" id="4B5aqq4en6s" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq4en6x" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4en6A" role="3cqZAp">
          <node concept="2OqwBi" id="4B5aqq4enzN" role="3clFbG">
            <node concept="BsUDl" id="4B5aqq4en$N" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXwxuj" resolve="getGenerationTranslations" />
            </node>
            <node concept="liA8E" id="4B5aqq4enzP" role="2OqNvi">
              <ref role="37wK5l" to="zf0i:4V3GMfXwkrq" resolve="getLaboratoryTestResultAsTextAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="4B5aqq4en6y" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmMptJL" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getAliasWithSpecimenType" />
      <ref role="13i0hy" node="6LTgXmMlgN$" resolve="getAliasWithSpecimenType" />
      <node concept="3Tm1VV" id="6LTgXmMptJM" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmMptJU" role="3clF47">
        <node concept="3clFbF" id="6LTgXmMpsDQ" role="3cqZAp">
          <node concept="2OqwBi" id="6LTgXmMpt31" role="3clFbG">
            <node concept="BsUDl" id="6LTgXmMpt32" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXwxj$" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6LTgXmMpt33" role="2OqNvi">
              <ref role="37wK5l" to="zf0i:4B5aqq4dEqR" resolve="getLaboratoryTestResultWithSpecimenAsTextAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmNekIy" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmMptK0" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getDescriptionWithSpecimenType" />
      <ref role="13i0hy" node="6LTgXmMlq1k" resolve="getDescriptionWithSpecimenType" />
      <node concept="3Tm1VV" id="6LTgXmMptK1" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmMptK6" role="3clF47">
        <node concept="3clFbF" id="6LTgXmMpsE4" role="3cqZAp">
          <node concept="2OqwBi" id="6LTgXmMpt8z" role="3clFbG">
            <node concept="BsUDl" id="6LTgXmMpt8$" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXwxj$" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6LTgXmMpt8_" role="2OqNvi">
              <ref role="37wK5l" to="zf0i:4B5aqq4dEqR" resolve="getLaboratoryTestResultWithSpecimenAsTextAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmNekNu" role="3clF45" />
    </node>
    <node concept="13i0hz" id="4B5aqq4en6B" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getDisplayKeywords" />
      <ref role="13i0hy" to="1yj:4B5aqq3ZFNV" resolve="getDisplayKeywords" />
      <node concept="3Tm1VV" id="4B5aqq4en6C" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq4en6L" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4eqnP" role="3cqZAp">
          <node concept="BsUDl" id="4B5aqq4eqnO" role="3clFbG">
            <ref role="37wK5l" node="4B5aqq4en_x" resolve="getLocalizedKeywords" />
            <node concept="BsUDl" id="4B5aqq4eqoG" role="37wK5m">
              <ref role="37wK5l" node="4V3GMfXwxj$" resolve="getDisplayTranslations" />
            </node>
          </node>
        </node>
      </node>
      <node concept="10Q1$e" id="4B5aqq4en6M" role="3clF45">
        <node concept="17QB3L" id="4B5aqq4en6N" role="10Q1$1" />
      </node>
    </node>
    <node concept="13i0hz" id="4B5aqq4en6S" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getGenerationKeywords" />
      <ref role="13i0hy" to="1yj:4B5aqq3ZFQj" resolve="getGenerationKeywords" />
      <node concept="3Tm1VV" id="4B5aqq4en6T" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq4en72" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4en78" role="3cqZAp">
          <node concept="BsUDl" id="4B5aqq4eqm9" role="3clFbG">
            <ref role="37wK5l" node="4B5aqq4en_x" resolve="getLocalizedKeywords" />
            <node concept="BsUDl" id="4B5aqq4eqmR" role="37wK5m">
              <ref role="37wK5l" node="4V3GMfXwxuj" resolve="getGenerationTranslations" />
            </node>
          </node>
        </node>
      </node>
      <node concept="10Q1$e" id="4B5aqq4en73" role="3clF45">
        <node concept="17QB3L" id="4B5aqq4en74" role="10Q1$1" />
      </node>
    </node>
    <node concept="13i0hz" id="4B5aqq4en_x" role="13h7CS">
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getLocalizedKeywords" />
      <node concept="3Tm6S6" id="4B5aqq4enCU" role="1B3o_S" />
      <node concept="10Q1$e" id="4B5aqq4enAB" role="3clF45">
        <node concept="17QB3L" id="4B5aqq4enAz" role="10Q1$1" />
      </node>
      <node concept="3clFbS" id="4B5aqq4en_$" role="3clF47">
        <node concept="3cpWs8" id="4B5aqq4enD6" role="3cqZAp">
          <node concept="3cpWsn" id="4B5aqq4enD9" role="3cpWs9">
            <property role="TrG5h" value="firstKeyword" />
            <node concept="17QB3L" id="4B5aqq4enD5" role="1tU5fm" />
            <node concept="2OqwBi" id="4B5aqq4enK_" role="33vP2m">
              <node concept="37vLTw" id="4B5aqq4enDM" role="2Oq$k0">
                <ref role="3cqZAo" node="4B5aqq4enBP" resolve="translations" />
              </node>
              <node concept="liA8E" id="4B5aqq4enQj" role="2OqNvi">
                <ref role="37wK5l" to="zf0i:4B5aqq4dECb" resolve="getLaboratoryTestResultWithSpecimenAsTextKeywordTextResult" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="4B5aqq4enR0" role="3cqZAp">
          <node concept="3cpWsn" id="4B5aqq4enR3" role="3cpWs9">
            <property role="TrG5h" value="secondKeyword" />
            <node concept="17QB3L" id="4B5aqq4enR4" role="1tU5fm" />
            <node concept="2OqwBi" id="4B5aqq4enR5" role="33vP2m">
              <node concept="37vLTw" id="4B5aqq4enR6" role="2Oq$k0">
                <ref role="3cqZAo" node="4B5aqq4enBP" resolve="translations" />
              </node>
              <node concept="liA8E" id="4B5aqq4enR7" role="2OqNvi">
                <ref role="37wK5l" to="zf0i:4B5aqq4dF4c" resolve="getLaboratoryTestPreviousResultWithSpecimenAsTextKeywordWithSpecimen" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4B5aqq4enUj" role="3cqZAp">
          <node concept="2ShNRf" id="4B5aqq4enUf" role="3clFbG">
            <node concept="3g6Rrh" id="4B5aqq4eo5E" role="2ShVmc">
              <node concept="17QB3L" id="4B5aqq4enXd" role="3g7fb8" />
              <node concept="37vLTw" id="4B5aqq4eo6h" role="3g7hyw">
                <ref role="3cqZAo" node="4B5aqq4enD9" resolve="firstKeyword" />
              </node>
              <node concept="37vLTw" id="4B5aqq4eo6S" role="3g7hyw">
                <ref role="3cqZAo" node="4B5aqq4enR3" resolve="secondKeyword" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="4B5aqq4enBP" role="3clF46">
        <property role="TrG5h" value="translations" />
        <node concept="3uibUv" id="4B5aqq4enBO" role="1tU5fm">
          <ref role="3uigEE" to="zf0i:4zMac8rUNtP" resolve="ITextResultLaboratoryTestDataTranslations" />
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="6LTgXmMI66d" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayUnit" />
      <ref role="13i0hy" to="wb6c:1mAGFBKFXUa" resolve="getDisplayUnit" />
      <node concept="3Tm1VV" id="6LTgXmMI66e" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmMI66f" role="3clF47">
        <node concept="Jncv_" id="6LTgXmMI66g" role="3cqZAp">
          <ref role="JncvD" to="7f9y:6LTgXmMvpq9" resolve="IEntityReference" />
          <node concept="2OqwBi" id="6LTgXmMI66h" role="JncvB">
            <node concept="13iPFW" id="6LTgXmMI66i" role="2Oq$k0" />
            <node concept="3TrEf2" id="6LTgXmNeu$1" role="2OqNvi">
              <ref role="3Tt5mk" to="veyb:6LTgXmMj7Lm" resolve="testReference" />
            </node>
          </node>
          <node concept="3clFbS" id="6LTgXmMI66k" role="Jncv$">
            <node concept="Jncv_" id="6LTgXmMI66l" role="3cqZAp">
              <ref role="JncvD" to="vbok:7lYCqhujn5u" resolve="IHasUnit" />
              <node concept="3clFbS" id="6LTgXmMI66m" role="Jncv$">
                <node concept="3cpWs6" id="6LTgXmMI66n" role="3cqZAp">
                  <node concept="2OqwBi" id="6LTgXmMI66o" role="3cqZAk">
                    <node concept="Jnkvi" id="6LTgXmMI66p" role="2Oq$k0">
                      <ref role="1M0zk5" node="6LTgXmMI66r" resolve="target" />
                    </node>
                    <node concept="2qgKlT" id="6LTgXmMI66q" role="2OqNvi">
                      <ref role="37wK5l" to="1yj:7lYCqhujn6u" resolve="getDisplayUnit" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="JncvC" id="6LTgXmMI66r" role="JncvA">
                <property role="TrG5h" value="target" />
                <node concept="2jxLKc" id="6LTgXmMI66s" role="1tU5fm" />
              </node>
              <node concept="2OqwBi" id="6LTgXmMI66t" role="JncvB">
                <node concept="Jnkvi" id="6LTgXmMI66u" role="2Oq$k0">
                  <ref role="1M0zk5" node="6LTgXmMI66w" resolve="entityRef" />
                </node>
                <node concept="3TrEf2" id="6LTgXmMI66v" role="2OqNvi">
                  <ref role="3Tt5mk" to="7f9y:4QUW3efv2jv" resolve="target" />
                </node>
              </node>
            </node>
          </node>
          <node concept="JncvC" id="6LTgXmMI66w" role="JncvA">
            <property role="TrG5h" value="entityRef" />
            <node concept="2jxLKc" id="6LTgXmMI66x" role="1tU5fm" />
          </node>
        </node>
        <node concept="3clFbF" id="6LTgXmMI66y" role="3cqZAp">
          <node concept="10Nm6u" id="6LTgXmMI66z" role="3clFbG" />
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmMI66$" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmMpvcf" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getGenerationUnit" />
      <ref role="13i0hy" to="wb6c:4B5aqq6oEpg" resolve="getGenerationUnit" />
      <node concept="3Tm1VV" id="6LTgXmMpvcg" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmMpvcP" role="3clF47">
        <node concept="3clFbF" id="6LTgXmMpvcU" role="3cqZAp">
          <node concept="2OqwBi" id="6LTgXmMpvcR" role="3clFbG">
            <node concept="13iAh5" id="6LTgXmMpvcS" role="2Oq$k0">
              <ref role="3eA5LN" to="7f9y:1mAGFBKFXT_" resolve="IDataValueWithUnit" />
            </node>
            <node concept="2qgKlT" id="6LTgXmMpvcT" role="2OqNvi">
              <ref role="37wK5l" to="wb6c:4B5aqq6oEpg" resolve="getGenerationUnit" />
            </node>
          </node>
        </node>
        <node concept="Jncv_" id="6LTgXmMI6B$" role="3cqZAp">
          <ref role="JncvD" to="7f9y:6LTgXmMvpq9" resolve="IEntityReference" />
          <node concept="2OqwBi" id="6LTgXmMI6B_" role="JncvB">
            <node concept="13iPFW" id="6LTgXmMI6BA" role="2Oq$k0" />
            <node concept="3TrEf2" id="6LTgXmNeuYm" role="2OqNvi">
              <ref role="3Tt5mk" to="veyb:6LTgXmMj7Lm" resolve="testReference" />
            </node>
          </node>
          <node concept="3clFbS" id="6LTgXmMI6BC" role="Jncv$">
            <node concept="Jncv_" id="6LTgXmMI6BD" role="3cqZAp">
              <ref role="JncvD" to="vbok:7lYCqhujn5u" resolve="IHasUnit" />
              <node concept="3clFbS" id="6LTgXmMI6BE" role="Jncv$">
                <node concept="3cpWs6" id="6LTgXmMI6BF" role="3cqZAp">
                  <node concept="2OqwBi" id="6LTgXmMI6BG" role="3cqZAk">
                    <node concept="Jnkvi" id="6LTgXmMI6BH" role="2Oq$k0">
                      <ref role="1M0zk5" node="6LTgXmMI6BJ" resolve="target" />
                    </node>
                    <node concept="2qgKlT" id="6LTgXmMI6ZT" role="2OqNvi">
                      <ref role="37wK5l" to="1yj:4B5aqq6oFwi" resolve="getGenerationUnit" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="JncvC" id="6LTgXmMI6BJ" role="JncvA">
                <property role="TrG5h" value="target" />
                <node concept="2jxLKc" id="6LTgXmMI6BK" role="1tU5fm" />
              </node>
              <node concept="2OqwBi" id="6LTgXmMI6BL" role="JncvB">
                <node concept="Jnkvi" id="6LTgXmMI6BM" role="2Oq$k0">
                  <ref role="1M0zk5" node="6LTgXmMI6BO" resolve="entityRef" />
                </node>
                <node concept="3TrEf2" id="6LTgXmMI6BN" role="2OqNvi">
                  <ref role="3Tt5mk" to="7f9y:4QUW3efv2jv" resolve="target" />
                </node>
              </node>
            </node>
          </node>
          <node concept="JncvC" id="6LTgXmMI6BO" role="JncvA">
            <property role="TrG5h" value="entityRef" />
            <node concept="2jxLKc" id="6LTgXmMI6BP" role="1tU5fm" />
          </node>
        </node>
        <node concept="3clFbF" id="6LTgXmMI6BQ" role="3cqZAp">
          <node concept="10Nm6u" id="6LTgXmMI6BR" role="3clFbG" />
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmMpvcQ" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="PDjyzkysr3">
    <property role="3GE5qa" value="base.data.boolean" />
    <ref role="13h7C2" to="veyb:PDjyzky6xW" resolve="LaboratoryTestHasResultAsText" />
    <node concept="13i0hz" id="4B5aqq4gk1V" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="4B5aqq4gk1W" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq4gk21" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4gkdj" role="3cqZAp">
          <node concept="2OqwBi" id="4B5aqq4gkk7" role="3clFbG">
            <node concept="BsUDl" id="PDjyzkysxw" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXwxj$" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="4B5aqq4gkpW" role="2OqNvi">
              <ref role="37wK5l" to="zf0i:PDjyzkycdF" resolve="getLaboratoryTestHasResultAsTextAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="4B5aqq4gk22" role="3clF45" />
    </node>
    <node concept="13i0hz" id="4B5aqq4gk27" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="4B5aqq4gk28" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq4gk2d" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4gkuI" role="3cqZAp">
          <node concept="2OqwBi" id="PDjyzkysDz" role="3clFbG">
            <node concept="BsUDl" id="PDjyzkysD$" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXwxj$" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="PDjyzkysD_" role="2OqNvi">
              <ref role="37wK5l" to="zf0i:PDjyzkycoB" resolve="getLaboratoryTestHasResultAsTextDescription" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="4B5aqq4gk2e" role="3clF45" />
    </node>
    <node concept="13i0hz" id="4B5aqq4gk2j" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getGenerationAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:4QUW3efxqUv" resolve="getGenerationAlias" />
      <node concept="3Tm1VV" id="4B5aqq4gk2k" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq4gk2p" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4gk2u" role="3cqZAp">
          <node concept="2OqwBi" id="PDjyzkysJN" role="3clFbG">
            <node concept="BsUDl" id="PDjyzkysKV" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXwxuj" resolve="getGenerationTranslations" />
            </node>
            <node concept="liA8E" id="PDjyzkysJP" role="2OqNvi">
              <ref role="37wK5l" to="zf0i:PDjyzkycdF" resolve="getLaboratoryTestHasResultAsTextAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="4B5aqq4gk2q" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmMp7m$" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getAliasWithSpecimenType" />
      <ref role="13i0hy" node="6LTgXmMlgN$" resolve="getAliasWithSpecimenType" />
      <node concept="3Tm1VV" id="6LTgXmMp7m_" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmMp7mH" role="3clF47">
        <node concept="3clFbF" id="6LTgXmMp7mM" role="3cqZAp">
          <node concept="2OqwBi" id="6LTgXmMp9Zr" role="3clFbG">
            <node concept="BsUDl" id="6LTgXmMp9Zs" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXwxj$" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6LTgXmMp9Zt" role="2OqNvi">
              <ref role="37wK5l" to="zf0i:PDjyzkycte" resolve="getLaboratoryTestHasResultWithSpecimenAsTextAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmNeecw" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmMp7mN" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getDescriptionWithSpecimenType" />
      <ref role="13i0hy" node="6LTgXmMlq1k" resolve="getDescriptionWithSpecimenType" />
      <node concept="3Tm1VV" id="6LTgXmMp7mO" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmMp7mT" role="3clF47">
        <node concept="3clFbF" id="6LTgXmMp7mY" role="3cqZAp">
          <node concept="2OqwBi" id="6LTgXmMpa2a" role="3clFbG">
            <node concept="BsUDl" id="6LTgXmMpa2b" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXwxj$" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6LTgXmMpa2c" role="2OqNvi">
              <ref role="37wK5l" to="zf0i:PDjyzkycti" resolve="getLaboratoryTestHasResultWithSpecimenAsTextDescription" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmNeecY" role="3clF45" />
    </node>
    <node concept="13i0hz" id="4B5aqq4gk2v" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getDisplayKeywords" />
      <ref role="13i0hy" to="1yj:4B5aqq3ZFNV" resolve="getDisplayKeywords" />
      <node concept="3Tm1VV" id="4B5aqq4gk2w" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq4gk2D" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4gk2J" role="3cqZAp">
          <node concept="BsUDl" id="4B5aqq4gnbh" role="3clFbG">
            <ref role="37wK5l" node="4B5aqq4gkyB" resolve="getLocalizedKeywords" />
            <node concept="BsUDl" id="PDjyzkysP4" role="37wK5m">
              <ref role="37wK5l" node="4V3GMfXwxj$" resolve="getDisplayTranslations" />
            </node>
          </node>
        </node>
      </node>
      <node concept="10Q1$e" id="4B5aqq4gk2E" role="3clF45">
        <node concept="17QB3L" id="4B5aqq4gk2F" role="10Q1$1" />
      </node>
    </node>
    <node concept="13i0hz" id="4B5aqq4gk2K" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getGenerationKeywords" />
      <ref role="13i0hy" to="1yj:4B5aqq3ZFQj" resolve="getGenerationKeywords" />
      <node concept="3Tm1VV" id="4B5aqq4gk2L" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq4gk2U" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4gk30" role="3cqZAp">
          <node concept="BsUDl" id="4B5aqq4gn17" role="3clFbG">
            <ref role="37wK5l" node="4B5aqq4gkyB" resolve="getLocalizedKeywords" />
            <node concept="BsUDl" id="PDjyzkysQb" role="37wK5m">
              <ref role="37wK5l" node="4V3GMfXwxuj" resolve="getGenerationTranslations" />
            </node>
          </node>
        </node>
      </node>
      <node concept="10Q1$e" id="4B5aqq4gk2V" role="3clF45">
        <node concept="17QB3L" id="4B5aqq4gk2W" role="10Q1$1" />
      </node>
    </node>
    <node concept="13i0hz" id="4B5aqq4gkyB" role="13h7CS">
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getLocalizedKeywords" />
      <node concept="3Tm6S6" id="4B5aqq4gkzx" role="1B3o_S" />
      <node concept="10Q1$e" id="4B5aqq4gkzK" role="3clF45">
        <node concept="17QB3L" id="4B5aqq4gkzG" role="10Q1$1" />
      </node>
      <node concept="3clFbS" id="4B5aqq4gkyE" role="3clF47">
        <node concept="3cpWs8" id="4B5aqq4gk_g" role="3cqZAp">
          <node concept="3cpWsn" id="4B5aqq4gk_j" role="3cpWs9">
            <property role="TrG5h" value="firstKeyword" />
            <node concept="17QB3L" id="4B5aqq4gk_f" role="1tU5fm" />
            <node concept="2OqwBi" id="4B5aqq4gkGT" role="33vP2m">
              <node concept="37vLTw" id="4B5aqq4gkA6" role="2Oq$k0">
                <ref role="3cqZAo" node="4B5aqq4gk$u" resolve="translations" />
              </node>
              <node concept="liA8E" id="4B5aqq4gkMB" role="2OqNvi">
                <ref role="37wK5l" to="zf0i:PDjyzkycIh" resolve="getLaboratoryTestHasResultWithSpecimenAsTextKeywordHasResultAsText" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="4B5aqq4gkNk" role="3cqZAp">
          <node concept="3cpWsn" id="4B5aqq4gkNn" role="3cpWs9">
            <property role="TrG5h" value="secondKeyword" />
            <node concept="17QB3L" id="4B5aqq4gkNo" role="1tU5fm" />
            <node concept="2OqwBi" id="4B5aqq4gkNp" role="33vP2m">
              <node concept="37vLTw" id="4B5aqq4gkNq" role="2Oq$k0">
                <ref role="3cqZAo" node="4B5aqq4gk$u" resolve="translations" />
              </node>
              <node concept="liA8E" id="4B5aqq4gkNr" role="2OqNvi">
                <ref role="37wK5l" to="zf0i:PDjyzkycIl" resolve="getLaboratoryTestHasResultWithSpecimenAsTextKeywordWithSpecimen" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4B5aqq4gkRO" role="3cqZAp">
          <node concept="2ShNRf" id="4B5aqq4gkRK" role="3clFbG">
            <node concept="3g6Rrh" id="4B5aqq4gl3b" role="2ShVmc">
              <node concept="17QB3L" id="4B5aqq4gkUI" role="3g7fb8" />
              <node concept="37vLTw" id="4B5aqq4gl3M" role="3g7hyw">
                <ref role="3cqZAo" node="4B5aqq4gk_j" resolve="firstKeyword" />
              </node>
              <node concept="37vLTw" id="4B5aqq4gl4p" role="3g7hyw">
                <ref role="3cqZAo" node="4B5aqq4gkNn" resolve="secondKeyword" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="4B5aqq4gk$u" role="3clF46">
        <property role="TrG5h" value="translations" />
        <node concept="3uibUv" id="PDjyzkysLX" role="1tU5fm">
          <ref role="3uigEE" to="zf0i:4zMac8rUNtP" resolve="ITextResultLaboratoryTestDataTranslations" />
        </node>
      </node>
    </node>
    <node concept="13hLZK" id="PDjyzkysr4" role="13h7CW">
      <node concept="3clFbS" id="PDjyzkysr5" role="2VODD2" />
    </node>
  </node>
  <node concept="13h7C7" id="6LTgXmNdC1m">
    <property role="3GE5qa" value="base.data" />
    <ref role="13h7C2" to="veyb:4B5aqq3QF78" resolve="ILaboratoryTestWithTextResultDataValue" />
    <node concept="13i0hz" id="6LTgXmMlgN$" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="true" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getAliasWithSpecimenType" />
      <node concept="3Tm1VV" id="6LTgXmMlgN_" role="1B3o_S" />
      <node concept="17QB3L" id="6LTgXmMlh67" role="3clF45" />
      <node concept="3clFbS" id="6LTgXmMlgNB" role="3clF47">
        <node concept="3clFbF" id="6LTgXmMlhXr" role="3cqZAp">
          <node concept="2YIFZM" id="6LTgXmMlhXL" role="3clFbG">
            <ref role="1Pybhc" to="wyt6:~String" resolve="String" />
            <ref role="37wK5l" to="wyt6:~String.format(java.lang.String,java.lang.Object...):java.lang.String" resolve="format" />
            <node concept="Xl_RD" id="6LTgXmMli2j" role="37wK5m">
              <property role="Xl_RC" value="%s %s" />
            </node>
            <node concept="BsUDl" id="6LTgXmMlil1" role="37wK5m">
              <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
            </node>
            <node concept="BsUDl" id="6LTgXmMliDp" role="37wK5m">
              <ref role="37wK5l" node="4B5aqq3QFqK" resolve="getWithSpecimenTypeKeyword" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="6LTgXmMlq1k" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="true" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getDescriptionWithSpecimenType" />
      <node concept="3Tm1VV" id="6LTgXmMlq1l" role="1B3o_S" />
      <node concept="17QB3L" id="6LTgXmMlqk9" role="3clF45" />
      <node concept="3clFbS" id="6LTgXmMlq1n" role="3clF47">
        <node concept="3clFbF" id="6LTgXmMlql6" role="3cqZAp">
          <node concept="BsUDl" id="6LTgXmMlql5" role="3clFbG">
            <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="4B5aqq3QFqK" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="true" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getWithSpecimenTypeKeyword" />
      <node concept="3Tm1VV" id="4B5aqq3QFqL" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq3QFrg" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq3QFqN" role="3clF47">
        <node concept="3clFbF" id="4B5aqq3QFrW" role="3cqZAp">
          <node concept="AH0OO" id="4B5aqq49y37" role="3clFbG">
            <node concept="3cmrfG" id="4B5aqq49y48" role="AHEQo">
              <property role="3cmrfH" value="1" />
            </node>
            <node concept="BsUDl" id="4B5aqq49xAu" role="AHHXb">
              <ref role="37wK5l" to="1yj:4B5aqq3ZFNV" resolve="getDisplayKeywords" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="6LTgXmMiWh2" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="true" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getIncludeSpecimenTypeIntentionDescription" />
      <node concept="3Tm1VV" id="6LTgXmMiWh3" role="1B3o_S" />
      <node concept="17QB3L" id="6LTgXmMiWz1" role="3clF45" />
      <node concept="3clFbS" id="6LTgXmMiWh5" role="3clF47">
        <node concept="3clFbF" id="6LTgXmMiW$6" role="3cqZAp">
          <node concept="2OqwBi" id="6LTgXmMiWF4" role="3clFbG">
            <node concept="BsUDl" id="6LTgXmNdXfL" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXwxj$" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6LTgXmMiWKY" role="2OqNvi">
              <ref role="37wK5l" to="zf0i:6LTgXmNdXX8" resolve="getILaboratoryTestWithTextResultDataValueIncludeSpecimenTypeIntentionDescription" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="4B5aqq3ZCop" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="toDisplayString" />
      <ref role="13i0hy" to="1yj:499Gn2DGTs1" resolve="toDisplayString" />
      <node concept="3Tm1VV" id="4B5aqq3ZCoq" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq3ZCoA" role="3clF47">
        <node concept="3cpWs8" id="4B5aqq3ZH5N" role="3cqZAp">
          <node concept="3cpWsn" id="4B5aqq3ZH5Q" role="3cpWs9">
            <property role="TrG5h" value="keywords" />
            <node concept="10Q1$e" id="4B5aqq3ZHeY" role="1tU5fm">
              <node concept="17QB3L" id="4B5aqq3ZH5L" role="10Q1$1" />
            </node>
            <node concept="BsUDl" id="4B5aqq3ZHnO" role="33vP2m">
              <ref role="37wK5l" to="1yj:4B5aqq3ZFNV" resolve="getDisplayKeywords" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="6LTgXmMjn75" role="3cqZAp">
          <node concept="BsUDl" id="6LTgXmMjn76" role="3clFbG">
            <ref role="37wK5l" node="6LTgXmMj8IB" resolve="toLocalizedString" />
            <node concept="AH0OO" id="6LTgXmMjn77" role="37wK5m">
              <node concept="3cmrfG" id="6LTgXmMjn78" role="AHEQo">
                <property role="3cmrfH" value="0" />
              </node>
              <node concept="37vLTw" id="6LTgXmMjn79" role="AHHXb">
                <ref role="3cqZAo" node="4B5aqq3ZH5Q" resolve="keywords" />
              </node>
            </node>
            <node concept="2OqwBi" id="6LTgXmMjn7a" role="37wK5m">
              <node concept="2OqwBi" id="6LTgXmMjn7b" role="2Oq$k0">
                <node concept="13iPFW" id="6LTgXmMjn7c" role="2Oq$k0" />
                <node concept="3TrEf2" id="6LTgXmMjn7d" role="2OqNvi">
                  <ref role="3Tt5mk" to="veyb:6LTgXmMj7Lm" resolve="testReference" />
                </node>
              </node>
              <node concept="2qgKlT" id="6LTgXmMjnC4" role="2OqNvi">
                <ref role="37wK5l" to="1yj:499Gn2DGTs1" resolve="toDisplayString" />
              </node>
            </node>
            <node concept="AH0OO" id="6LTgXmMjn7f" role="37wK5m">
              <node concept="3cmrfG" id="6LTgXmMjn7g" role="AHEQo">
                <property role="3cmrfH" value="1" />
              </node>
              <node concept="37vLTw" id="6LTgXmMjn7h" role="AHHXb">
                <ref role="3cqZAo" node="4B5aqq3ZH5Q" resolve="keywords" />
              </node>
            </node>
            <node concept="2OqwBi" id="6LTgXmMjn7i" role="37wK5m">
              <node concept="2OqwBi" id="6LTgXmMjn7j" role="2Oq$k0">
                <node concept="13iPFW" id="6LTgXmMjn7k" role="2Oq$k0" />
                <node concept="3TrEf2" id="6LTgXmMjn7l" role="2OqNvi">
                  <ref role="3Tt5mk" to="veyb:4B5aqq3QF7a" resolve="specimenTypeReference" />
                </node>
              </node>
              <node concept="2qgKlT" id="6LTgXmMjo5h" role="2OqNvi">
                <ref role="37wK5l" to="1yj:499Gn2DGTs1" resolve="toDisplayString" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="4B5aqq3ZCoB" role="3clF45" />
    </node>
    <node concept="13i0hz" id="4B5aqq3ZCoG" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="toGenerationString" />
      <ref role="13i0hy" to="1yj:499Gn2DGU_t" resolve="toGenerationString" />
      <node concept="3Tm1VV" id="4B5aqq3ZCoH" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq3ZCoT" role="3clF47">
        <node concept="3cpWs8" id="4B5aqq3ZLjH" role="3cqZAp">
          <node concept="3cpWsn" id="4B5aqq3ZLjI" role="3cpWs9">
            <property role="TrG5h" value="keywords" />
            <node concept="10Q1$e" id="4B5aqq3ZLjJ" role="1tU5fm">
              <node concept="17QB3L" id="4B5aqq3ZLjK" role="10Q1$1" />
            </node>
            <node concept="BsUDl" id="4B5aqq3ZMc7" role="33vP2m">
              <ref role="37wK5l" to="1yj:4B5aqq3ZFQj" resolve="getGenerationKeywords" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="6LTgXmMjghk" role="3cqZAp">
          <node concept="BsUDl" id="6LTgXmMjghi" role="3clFbG">
            <ref role="37wK5l" node="6LTgXmMj8IB" resolve="toLocalizedString" />
            <node concept="AH0OO" id="6LTgXmMjhpp" role="37wK5m">
              <node concept="3cmrfG" id="6LTgXmMjhus" role="AHEQo">
                <property role="3cmrfH" value="0" />
              </node>
              <node concept="37vLTw" id="6LTgXmMjgwY" role="AHHXb">
                <ref role="3cqZAo" node="4B5aqq3ZLjI" resolve="keywords" />
              </node>
            </node>
            <node concept="2OqwBi" id="6LTgXmMjiK9" role="37wK5m">
              <node concept="2OqwBi" id="6LTgXmMjhWN" role="2Oq$k0">
                <node concept="13iPFW" id="6LTgXmMjhGj" role="2Oq$k0" />
                <node concept="3TrEf2" id="6LTgXmMjiiq" role="2OqNvi">
                  <ref role="3Tt5mk" to="veyb:6LTgXmMj7Lm" resolve="testReference" />
                </node>
              </node>
              <node concept="2qgKlT" id="6LTgXmMjjc5" role="2OqNvi">
                <ref role="37wK5l" to="1yj:499Gn2DGU_t" resolve="toGenerationString" />
              </node>
            </node>
            <node concept="AH0OO" id="6LTgXmMjk2W" role="37wK5m">
              <node concept="3cmrfG" id="6LTgXmMjkg2" role="AHEQo">
                <property role="3cmrfH" value="1" />
              </node>
              <node concept="37vLTw" id="6LTgXmMjjCX" role="AHHXb">
                <ref role="3cqZAo" node="4B5aqq3ZLjI" resolve="keywords" />
              </node>
            </node>
            <node concept="2OqwBi" id="6LTgXmMjlXu" role="37wK5m">
              <node concept="2OqwBi" id="6LTgXmMjkZ9" role="2Oq$k0">
                <node concept="13iPFW" id="6LTgXmMjkHJ" role="2Oq$k0" />
                <node concept="3TrEf2" id="6LTgXmMjloF" role="2OqNvi">
                  <ref role="3Tt5mk" to="veyb:4B5aqq3QF7a" resolve="specimenTypeReference" />
                </node>
              </node>
              <node concept="2qgKlT" id="6LTgXmMjmtn" role="2OqNvi">
                <ref role="37wK5l" to="1yj:499Gn2DGU_t" resolve="toGenerationString" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="4B5aqq3ZCoU" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmMj8IB" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="true" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="toLocalizedString" />
      <node concept="3Tm1VV" id="6LTgXmMj8IC" role="1B3o_S" />
      <node concept="17QB3L" id="6LTgXmMj90Q" role="3clF45" />
      <node concept="3clFbS" id="6LTgXmMj8IE" role="3clF47">
        <node concept="3clFbJ" id="6LTgXmMjdms" role="3cqZAp">
          <node concept="3clFbS" id="6LTgXmMjdmu" role="3clFbx">
            <node concept="3cpWs6" id="6LTgXmMjfc6" role="3cqZAp">
              <node concept="2YIFZM" id="6LTgXmMjfma" role="3cqZAk">
                <ref role="1Pybhc" to="wyt6:~String" resolve="String" />
                <ref role="37wK5l" to="wyt6:~String.format(java.lang.String,java.lang.Object...):java.lang.String" resolve="format" />
                <node concept="Xl_RD" id="6LTgXmMjfmb" role="37wK5m">
                  <property role="Xl_RC" value="%s %s" />
                </node>
                <node concept="37vLTw" id="6LTgXmMjfmc" role="37wK5m">
                  <ref role="3cqZAo" node="6LTgXmMj91F" resolve="firstKeyword" />
                </node>
                <node concept="37vLTw" id="6LTgXmMjfmd" role="37wK5m">
                  <ref role="3cqZAo" node="6LTgXmMj92R" resolve="test" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbC" id="6LTgXmMjeTu" role="3clFbw">
            <node concept="10Nm6u" id="6LTgXmMjf7A" role="3uHU7w" />
            <node concept="37vLTw" id="6LTgXmMjdsy" role="3uHU7B">
              <ref role="3cqZAo" node="6LTgXmMjbE1" resolve="specimen" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="6LTgXmMj99P" role="3cqZAp">
          <node concept="2YIFZM" id="6LTgXmMj99Q" role="3clFbG">
            <ref role="37wK5l" to="wyt6:~String.format(java.lang.String,java.lang.Object...):java.lang.String" resolve="format" />
            <ref role="1Pybhc" to="wyt6:~String" resolve="String" />
            <node concept="Xl_RD" id="6LTgXmMj99R" role="37wK5m">
              <property role="Xl_RC" value="%s %s %s %s" />
            </node>
            <node concept="37vLTw" id="6LTgXmMjbdT" role="37wK5m">
              <ref role="3cqZAo" node="6LTgXmMj91F" resolve="firstKeyword" />
            </node>
            <node concept="37vLTw" id="6LTgXmMjbQ2" role="37wK5m">
              <ref role="3cqZAo" node="6LTgXmMj92R" resolve="test" />
            </node>
            <node concept="37vLTw" id="6LTgXmMjctm" role="37wK5m">
              <ref role="3cqZAo" node="6LTgXmMj94D" resolve="secondKeyword" />
            </node>
            <node concept="37vLTw" id="6LTgXmMjd20" role="37wK5m">
              <ref role="3cqZAo" node="6LTgXmMjbE1" resolve="specimen" />
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="6LTgXmMj91F" role="3clF46">
        <property role="TrG5h" value="firstKeyword" />
        <node concept="17QB3L" id="6LTgXmMj91E" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="6LTgXmMj92R" role="3clF46">
        <property role="TrG5h" value="test" />
        <node concept="17QB3L" id="6LTgXmMj93b" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="6LTgXmMj94D" role="3clF46">
        <property role="TrG5h" value="secondKeyword" />
        <node concept="17QB3L" id="6LTgXmMj951" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="6LTgXmMjbE1" role="3clF46">
        <property role="TrG5h" value="specimen" />
        <node concept="17QB3L" id="6LTgXmMjbPm" role="1tU5fm" />
      </node>
    </node>
    <node concept="13i0hz" id="1I84Bf76eHv" role="13h7CS">
      <property role="TrG5h" value="matches" />
      <property role="13i0it" value="false" />
      <property role="13i0iv" value="false" />
      <ref role="13i0hy" to="wb6c:6LTgXmMs$_D" resolve="matches" />
      <node concept="3Tm1VV" id="1I84Bf76eHw" role="1B3o_S" />
      <node concept="3clFbS" id="1I84Bf76eHH" role="3clF47">
        <node concept="Jncv_" id="1I84Bf75U80" role="3cqZAp">
          <ref role="JncvD" to="veyb:4B5aqq3QF78" resolve="ILaboratoryTestWithTextResultDataValue" />
          <node concept="37vLTw" id="1I84Bf75U8t" role="JncvB">
            <ref role="3cqZAo" node="1I84Bf76eHI" resolve="node" />
          </node>
          <node concept="3clFbS" id="1I84Bf75U82" role="Jncv$">
            <node concept="3clFbJ" id="1I84Bf75U9m" role="3cqZAp">
              <node concept="2OqwBi" id="1I84Bf75Ulq" role="3clFbw">
                <node concept="Jnkvi" id="1I84Bf75U9E" role="2Oq$k0">
                  <ref role="1M0zk5" node="1I84Bf75U83" resolve="other" />
                </node>
                <node concept="1mIQ4w" id="1I84Bf75Uyr" role="2OqNvi">
                  <node concept="25Kdxt" id="1I84Bf75U_e" role="cj9EA">
                    <node concept="2OqwBi" id="1I84Bf75UQv" role="25KhWn">
                      <node concept="13iPFW" id="1I84Bf75UC4" role="2Oq$k0" />
                      <node concept="2yIwOk" id="1I84Bf75V5L" role="2OqNvi" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbS" id="1I84Bf75U9o" role="3clFbx">
                <node concept="3cpWs8" id="1I84Bf75VG0" role="3cqZAp">
                  <node concept="3cpWsn" id="1I84Bf75VG1" role="3cpWs9">
                    <property role="TrG5h" value="match" />
                    <node concept="3uibUv" id="1I84Bf75VG2" role="1tU5fm">
                      <ref role="3uigEE" to="hyw5:6LTgXmO6cZw" resolve="JoinPointMatch" />
                    </node>
                    <node concept="2OqwBi" id="1I84Bf75VG3" role="33vP2m">
                      <node concept="2OqwBi" id="1I84Bf75VG4" role="2Oq$k0">
                        <node concept="13iPFW" id="1I84Bf75VG5" role="2Oq$k0" />
                        <node concept="3TrEf2" id="1I84Bf76iIZ" role="2OqNvi">
                          <ref role="3Tt5mk" to="veyb:6LTgXmMj7Lm" resolve="testReference" />
                        </node>
                      </node>
                      <node concept="2qgKlT" id="1I84Bf75VG7" role="2OqNvi">
                        <ref role="37wK5l" to="wb6c:6LTgXmMs$_D" resolve="matches" />
                        <node concept="2OqwBi" id="1I84Bf75VG8" role="37wK5m">
                          <node concept="Jnkvi" id="1I84Bf764B$" role="2Oq$k0">
                            <ref role="1M0zk5" node="1I84Bf75U83" resolve="other" />
                          </node>
                          <node concept="3TrEf2" id="1I84Bf76igv" role="2OqNvi">
                            <ref role="3Tt5mk" to="veyb:6LTgXmMj7Lm" resolve="testReference" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbJ" id="1I84Bf75VGb" role="3cqZAp">
                  <node concept="3clFbS" id="1I84Bf75VGc" role="3clFbx">
                    <node concept="3clFbF" id="1I84Bf75VGd" role="3cqZAp">
                      <node concept="37vLTI" id="1I84Bf75VGe" role="3clFbG">
                        <node concept="2OqwBi" id="1I84Bf75VGf" role="37vLTx">
                          <node concept="37vLTw" id="1I84Bf75VGg" role="2Oq$k0">
                            <ref role="3cqZAo" node="1I84Bf75VG1" resolve="match" />
                          </node>
                          <node concept="liA8E" id="1I84Bf75VGh" role="2OqNvi">
                            <ref role="37wK5l" to="hyw5:1I84Bf7_lfa" resolve="merge" />
                            <node concept="2OqwBi" id="1I84Bf75VGi" role="37wK5m">
                              <node concept="2OqwBi" id="1I84Bf75VGj" role="2Oq$k0">
                                <node concept="13iPFW" id="1I84Bf75VGk" role="2Oq$k0" />
                                <node concept="3TrEf2" id="1I84Bf76jbf" role="2OqNvi">
                                  <ref role="3Tt5mk" to="veyb:4B5aqq3QF7a" resolve="specimenTypeReference" />
                                </node>
                              </node>
                              <node concept="2qgKlT" id="1I84Bf75VGm" role="2OqNvi">
                                <ref role="37wK5l" to="wb6c:6LTgXmMs$_D" resolve="matches" />
                                <node concept="2OqwBi" id="1I84Bf75VGn" role="37wK5m">
                                  <node concept="Jnkvi" id="1I84Bf765a9" role="2Oq$k0">
                                    <ref role="1M0zk5" node="1I84Bf75U83" resolve="other" />
                                  </node>
                                  <node concept="3TrEf2" id="1I84Bf76jwH" role="2OqNvi">
                                    <ref role="3Tt5mk" to="veyb:4B5aqq3QF7a" resolve="specimenTypeReference" />
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="37vLTw" id="1I84Bf75VGq" role="37vLTJ">
                          <ref role="3cqZAo" node="1I84Bf75VG1" resolve="match" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="2OqwBi" id="1I84Bf75VGr" role="3clFbw">
                    <node concept="2OqwBi" id="1I84Bf75VGs" role="2Oq$k0">
                      <node concept="Jnkvi" id="1I84Bf764TZ" role="2Oq$k0">
                        <ref role="1M0zk5" node="1I84Bf75U83" resolve="other" />
                      </node>
                      <node concept="3TrEf2" id="1I84Bf76lNk" role="2OqNvi">
                        <ref role="3Tt5mk" to="veyb:4B5aqq3QF7a" resolve="specimenTypeReference" />
                      </node>
                    </node>
                    <node concept="3x8VRR" id="1I84Bf75VGv" role="2OqNvi" />
                  </node>
                </node>
                <node concept="3cpWs6" id="1I84Bf75VGw" role="3cqZAp">
                  <node concept="37vLTw" id="1I84Bf75VGx" role="3cqZAk">
                    <ref role="3cqZAo" node="1I84Bf75VG1" resolve="match" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="JncvC" id="1I84Bf75U83" role="JncvA">
            <property role="TrG5h" value="other" />
            <node concept="2jxLKc" id="1I84Bf75U84" role="1tU5fm" />
          </node>
        </node>
        <node concept="3clFbF" id="1I84Bf75VG$" role="3cqZAp">
          <node concept="2YIFZM" id="1I84Bf80ett" role="3clFbG">
            <ref role="37wK5l" to="hyw5:1I84Bf7XEgz" resolve="createEmpty" />
            <ref role="1Pybhc" to="hyw5:6LTgXmO6cZw" resolve="JoinPointMatch" />
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="1I84Bf76eHI" role="3clF46">
        <property role="TrG5h" value="node" />
        <node concept="3Tqbb2" id="1I84Bf76eHJ" role="1tU5fm" />
      </node>
      <node concept="3uibUv" id="1I84Bf76eHK" role="3clF45">
        <ref role="3uigEE" to="hyw5:6LTgXmO6cZw" resolve="JoinPointMatch" />
      </node>
    </node>
    <node concept="13hLZK" id="6LTgXmNdC1n" role="13h7CW">
      <node concept="3clFbS" id="6LTgXmNdC1o" role="2VODD2" />
    </node>
  </node>
</model>

