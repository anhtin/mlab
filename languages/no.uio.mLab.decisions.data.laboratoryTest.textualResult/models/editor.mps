<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:e7a55a23-c2e7-4d66-b3ae-22e7615ac36f(no.uio.mLab.decisions.data.laboratoryTest.textualResult.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="11" />
    <use id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions" version="4" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="i7z6" ref="r:515ff38d-92f9-43f3-aa4c-c5bec29efe22(no.uio.mLab.decisions.core.editor)" />
    <import index="veyb" ref="r:f97bcc9c-211d-441a-840a-958b18315a7e(no.uio.mLab.decisions.data.laboratoryTest.textualResult.structure)" implicit="true" />
    <import index="1yj" ref="r:0c887d67-81ab-420f-9a35-a9f6bf96c975(no.uio.mLab.shared.behavior)" implicit="true" />
    <import index="q9tj" ref="r:13bcfbcf-6981-4a0f-b575-3d80ade7a177(no.uio.mLab.decisions.data.laboratoryTest.textualResult.behavior)" implicit="true" />
    <import index="uubs" ref="r:481ae0fe-9100-4bb8-9488-731d6cfeba53(no.uio.mLab.shared.editor)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="2000375450116454183" name="jetbrains.mps.lang.editor.structure.ISubstituteMenu" flags="ng" index="22mbnS">
        <child id="414384289274416996" name="parts" index="3ft7WO" />
      </concept>
      <concept id="540685334799947899" name="jetbrains.mps.lang.editor.structure.SubstituteMenuVariableDeclaration" flags="ig" index="23wRS9">
        <child id="540685334802085316" name="initializerBlock" index="23DdeQ" />
      </concept>
      <concept id="540685334799947902" name="jetbrains.mps.lang.editor.structure.SubstituteMenuVariableReference" flags="ng" index="23wRSc" />
      <concept id="540685334802085318" name="jetbrains.mps.lang.editor.structure.QueryFunction_SubstituteMenuVariable_Initializer" flags="ig" index="23DdeO" />
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi" />
      <concept id="1106270549637" name="jetbrains.mps.lang.editor.structure.CellLayout_Horizontal" flags="nn" index="2iRfu4" />
      <concept id="1142886221719" name="jetbrains.mps.lang.editor.structure.QueryFunction_NodeCondition" flags="in" index="pkWqt" />
      <concept id="1142886811589" name="jetbrains.mps.lang.editor.structure.ConceptFunctionParameter_node" flags="nn" index="pncrf" />
      <concept id="1177327570013" name="jetbrains.mps.lang.editor.structure.QueryFunction_SubstituteMenu_Substitute" flags="in" index="ucgPf" />
      <concept id="8478191136883534237" name="jetbrains.mps.lang.editor.structure.IExtensibleSubstituteMenuPart" flags="ng" index="upBLQ">
        <child id="8478191136883534238" name="features" index="upBLP" />
      </concept>
      <concept id="1177335944525" name="jetbrains.mps.lang.editor.structure.QueryFunction_SubstituteMenu_SubstituteString" flags="in" index="uGdhv" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="8383079901754291618" name="jetbrains.mps.lang.editor.structure.CellModel_NextEditor" flags="ng" index="B$lHz" />
      <concept id="1630016958697344083" name="jetbrains.mps.lang.editor.structure.IMenu_Concept" flags="ng" index="2ZABuq">
        <reference id="6591946374543067572" name="conceptDeclaration" index="aqKnT" />
      </concept>
      <concept id="8998492695583125082" name="jetbrains.mps.lang.editor.structure.SubstituteFeature_MatchingText" flags="ng" index="16NfWO">
        <child id="8998492695583129244" name="query" index="16NeZM" />
      </concept>
      <concept id="8998492695583129971" name="jetbrains.mps.lang.editor.structure.SubstituteFeature_DescriptionText" flags="ng" index="16NL0t">
        <child id="8998492695583129972" name="query" index="16NL0q" />
      </concept>
      <concept id="7342352913006985483" name="jetbrains.mps.lang.editor.structure.SubstituteMenuPart_Action" flags="ng" index="3eGOop">
        <child id="8612453216082699922" name="substituteHandler" index="3aKz83" />
      </concept>
      <concept id="414384289274418283" name="jetbrains.mps.lang.editor.structure.SubstituteMenuPart_Group" flags="ng" index="3ft6gV">
        <child id="540685334802084769" name="variables" index="23Ddnj" />
        <child id="414384289274424751" name="parts" index="3ft5RZ" />
      </concept>
      <concept id="1381004262292414836" name="jetbrains.mps.lang.editor.structure.ICellStyle" flags="ng" index="1k5N5V">
        <reference id="1381004262292426837" name="parentStyleClass" index="1k5W1q" />
      </concept>
      <concept id="3308396621974580100" name="jetbrains.mps.lang.editor.structure.SubstituteMenu_Default" flags="ng" index="3p36aQ" />
      <concept id="5425882385312046132" name="jetbrains.mps.lang.editor.structure.QueryFunctionParameter_SubstituteMenu_CurrentTargetNode" flags="nn" index="1yR$tW" />
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389214265" name="jetbrains.mps.lang.editor.structure.EditorCellModel" flags="ng" index="3EYTF0">
        <child id="1142887637401" name="renderingCondition" index="pqm2j" />
      </concept>
      <concept id="1073389446423" name="jetbrains.mps.lang.editor.structure.CellModel_Collection" flags="sn" stub="3013115976261988961" index="3EZMnI">
        <child id="1106270802874" name="cellLayout" index="2iSdaV" />
        <child id="1073389446424" name="childCellModel" index="3EZMnx" />
      </concept>
      <concept id="1073389882823" name="jetbrains.mps.lang.editor.structure.CellModel_RefNode" flags="sg" stub="730538219795960754" index="3F1sOY" />
      <concept id="1225898583838" name="jetbrains.mps.lang.editor.structure.ReadOnlyModelAccessor" flags="ng" index="1HfYo3">
        <child id="1225898971709" name="getter" index="1Hhtcw" />
      </concept>
      <concept id="1225900081164" name="jetbrains.mps.lang.editor.structure.CellModel_ReadOnlyModelAccessor" flags="sg" stub="3708815482283559694" index="1HlG4h">
        <child id="1225900141900" name="modelAccessor" index="1HlULh" />
      </concept>
      <concept id="1176717841777" name="jetbrains.mps.lang.editor.structure.QueryFunction_ModelAccess_Getter" flags="in" index="3TQlhw" />
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
      </concept>
    </language>
    <language id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions">
      <concept id="7776141288922801652" name="jetbrains.mps.lang.actions.structure.NF_Concept_NewInstance" flags="nn" index="q_SaT">
        <child id="3757480014665178932" name="prototype" index="1wAxWu" />
      </concept>
      <concept id="767145758118872830" name="jetbrains.mps.lang.actions.structure.NF_Link_SetNewChildOperation" flags="nn" index="2DeJnY" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="7453996997717780434" name="jetbrains.mps.lang.smodel.structure.Node_GetSConceptOperation" flags="nn" index="2yIwOk" />
      <concept id="2644386474300074836" name="jetbrains.mps.lang.smodel.structure.ConceptIdRefExpression" flags="nn" index="35c_gC">
        <reference id="2644386474300074837" name="conceptDeclaration" index="35c_gD" />
      </concept>
      <concept id="6677504323281689838" name="jetbrains.mps.lang.smodel.structure.SConceptType" flags="in" index="3bZ5Sz">
        <reference id="6677504323281689839" name="conceptDeclaraton" index="3bZ5Sy" />
      </concept>
      <concept id="1172008320231" name="jetbrains.mps.lang.smodel.structure.Node_IsNotNullOperation" flags="nn" index="3x8VRR" />
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="3p36aQ" id="4B5aqq4e86r">
    <property role="3GE5qa" value="base.data.values" />
    <ref role="aqKnT" to="veyb:4B5aqq4dEoJ" resolve="LaboratoryTestPreviousResultAsText" />
    <node concept="3ft6gV" id="6LTgXmMpSlF" role="3ft7WO">
      <node concept="23wRS9" id="6LTgXmMpSlG" role="23Ddnj">
        <property role="TrG5h" value="concept" />
        <node concept="23DdeO" id="6LTgXmMpSlH" role="23DdeQ">
          <node concept="3clFbS" id="6LTgXmMpSlI" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmMpSlJ" role="3cqZAp">
              <node concept="35c_gC" id="6LTgXmMpSlK" role="3clFbG">
                <ref role="35c_gD" to="veyb:4B5aqq4dEoJ" resolve="LaboratoryTestPreviousResultAsText" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3bZ5Sz" id="6LTgXmMpSlL" role="1tU5fm">
          <ref role="3bZ5Sy" to="veyb:4B5aqq4dEoJ" resolve="LaboratoryTestPreviousResultAsText" />
        </node>
      </node>
      <node concept="3eGOop" id="6LTgXmMpSlM" role="3ft5RZ">
        <node concept="ucgPf" id="6LTgXmMpSlN" role="3aKz83">
          <node concept="3clFbS" id="6LTgXmMpSlO" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmMpSlP" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmMpSlQ" role="3clFbG">
                <node concept="23wRSc" id="6LTgXmMpSlR" role="2Oq$k0">
                  <ref role="3cqZAo" node="6LTgXmMpSlG" resolve="concept" />
                </node>
                <node concept="q_SaT" id="6LTgXmMpSlS" role="2OqNvi">
                  <node concept="1yR$tW" id="6LTgXmMpSlT" role="1wAxWu" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="16NfWO" id="6LTgXmMpSlU" role="upBLP">
          <node concept="uGdhv" id="6LTgXmMpSlV" role="16NeZM">
            <node concept="3clFbS" id="6LTgXmMpSlW" role="2VODD2">
              <node concept="3clFbF" id="6LTgXmMpSlX" role="3cqZAp">
                <node concept="2OqwBi" id="6LTgXmMpSlY" role="3clFbG">
                  <node concept="2qgKlT" id="6LTgXmMpSlZ" role="2OqNvi">
                    <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                  </node>
                  <node concept="23wRSc" id="6LTgXmMpSm0" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMpSlG" resolve="concept" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="16NL0t" id="6LTgXmMpSm1" role="upBLP">
          <node concept="uGdhv" id="6LTgXmMpSm2" role="16NL0q">
            <node concept="3clFbS" id="6LTgXmMpSm3" role="2VODD2">
              <node concept="3clFbF" id="6LTgXmMpSm4" role="3cqZAp">
                <node concept="2OqwBi" id="6LTgXmMpSm5" role="3clFbG">
                  <node concept="2qgKlT" id="6LTgXmMpSm6" role="2OqNvi">
                    <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                  </node>
                  <node concept="23wRSc" id="6LTgXmMpSm7" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMpSlG" resolve="concept" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3eGOop" id="6LTgXmMpSm8" role="3ft5RZ">
        <node concept="ucgPf" id="6LTgXmMpSm9" role="3aKz83">
          <node concept="3clFbS" id="6LTgXmMpSma" role="2VODD2">
            <node concept="3cpWs8" id="6LTgXmMpSmb" role="3cqZAp">
              <node concept="3cpWsn" id="6LTgXmMpSmc" role="3cpWs9">
                <property role="TrG5h" value="node" />
                <node concept="3Tqbb2" id="6LTgXmMpSmd" role="1tU5fm">
                  <ref role="ehGHo" to="veyb:4B5aqq4dEoJ" resolve="LaboratoryTestPreviousResultAsText" />
                </node>
                <node concept="2OqwBi" id="6LTgXmMpSme" role="33vP2m">
                  <node concept="23wRSc" id="6LTgXmMpSmf" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMpSlG" resolve="concept" />
                  </node>
                  <node concept="q_SaT" id="6LTgXmMpSmg" role="2OqNvi">
                    <node concept="1yR$tW" id="6LTgXmMpSmh" role="1wAxWu" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="6LTgXmMpSmi" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmMpSmj" role="3clFbG">
                <node concept="2OqwBi" id="6LTgXmMpSmk" role="2Oq$k0">
                  <node concept="37vLTw" id="6LTgXmMpSml" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMpSmc" resolve="node" />
                  </node>
                  <node concept="3TrEf2" id="6LTgXmNdOUE" role="2OqNvi">
                    <ref role="3Tt5mk" to="veyb:4B5aqq3QF7a" resolve="specimenTypeReference" />
                  </node>
                </node>
                <node concept="2DeJnY" id="6LTgXmMpSmn" role="2OqNvi" />
              </node>
            </node>
            <node concept="3clFbF" id="6LTgXmMpSmo" role="3cqZAp">
              <node concept="37vLTw" id="6LTgXmMpSmp" role="3clFbG">
                <ref role="3cqZAo" node="6LTgXmMpSmc" resolve="node" />
              </node>
            </node>
          </node>
        </node>
        <node concept="16NfWO" id="6LTgXmMpSmq" role="upBLP">
          <node concept="uGdhv" id="6LTgXmMpSmr" role="16NeZM">
            <node concept="3clFbS" id="6LTgXmMpSms" role="2VODD2">
              <node concept="3clFbF" id="6LTgXmMpSmt" role="3cqZAp">
                <node concept="2OqwBi" id="6LTgXmMpSmu" role="3clFbG">
                  <node concept="2qgKlT" id="6LTgXmNdPum" role="2OqNvi">
                    <ref role="37wK5l" to="q9tj:6LTgXmMlgN$" resolve="getAliasWithSpecimenType" />
                  </node>
                  <node concept="23wRSc" id="6LTgXmMpSmw" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMpSlG" resolve="concept" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="16NL0t" id="6LTgXmMpSmx" role="upBLP">
          <node concept="uGdhv" id="6LTgXmMpSmy" role="16NL0q">
            <node concept="3clFbS" id="6LTgXmMpSmz" role="2VODD2">
              <node concept="3clFbF" id="6LTgXmMpSm$" role="3cqZAp">
                <node concept="2OqwBi" id="6LTgXmMpSm_" role="3clFbG">
                  <node concept="2qgKlT" id="6LTgXmNdQ4$" role="2OqNvi">
                    <ref role="37wK5l" to="q9tj:6LTgXmMlq1k" resolve="getDescriptionWithSpecimenType" />
                  </node>
                  <node concept="23wRSc" id="6LTgXmMpSmB" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMpSlG" resolve="concept" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="4B5aqq4eiRr">
    <property role="3GE5qa" value="base.data.values" />
    <ref role="aqKnT" to="veyb:4B5aqq4dEoN" resolve="LaboratoryTestResultAsText" />
    <node concept="3ft6gV" id="6LTgXmMpUFX" role="3ft7WO">
      <node concept="23wRS9" id="6LTgXmMpUFY" role="23Ddnj">
        <property role="TrG5h" value="concept" />
        <node concept="23DdeO" id="6LTgXmMpUFZ" role="23DdeQ">
          <node concept="3clFbS" id="6LTgXmMpUG0" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmMpUG1" role="3cqZAp">
              <node concept="35c_gC" id="6LTgXmMpUG2" role="3clFbG">
                <ref role="35c_gD" to="veyb:4B5aqq4dEoN" resolve="LaboratoryTestResultAsText" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3bZ5Sz" id="6LTgXmMpUG3" role="1tU5fm">
          <ref role="3bZ5Sy" to="veyb:4B5aqq4dEoN" resolve="LaboratoryTestResultAsText" />
        </node>
      </node>
      <node concept="3eGOop" id="6LTgXmMpUG4" role="3ft5RZ">
        <node concept="ucgPf" id="6LTgXmMpUG5" role="3aKz83">
          <node concept="3clFbS" id="6LTgXmMpUG6" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmMpUG7" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmMpUG8" role="3clFbG">
                <node concept="23wRSc" id="6LTgXmMpUG9" role="2Oq$k0">
                  <ref role="3cqZAo" node="6LTgXmMpUFY" resolve="concept" />
                </node>
                <node concept="q_SaT" id="6LTgXmMpUGa" role="2OqNvi">
                  <node concept="1yR$tW" id="6LTgXmMpUGb" role="1wAxWu" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="16NfWO" id="6LTgXmMpUGc" role="upBLP">
          <node concept="uGdhv" id="6LTgXmMpUGd" role="16NeZM">
            <node concept="3clFbS" id="6LTgXmMpUGe" role="2VODD2">
              <node concept="3clFbF" id="6LTgXmMpUGf" role="3cqZAp">
                <node concept="2OqwBi" id="6LTgXmMpUGg" role="3clFbG">
                  <node concept="2qgKlT" id="6LTgXmMpUGh" role="2OqNvi">
                    <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                  </node>
                  <node concept="23wRSc" id="6LTgXmMpUGi" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMpUFY" resolve="concept" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="16NL0t" id="6LTgXmMpUGj" role="upBLP">
          <node concept="uGdhv" id="6LTgXmMpUGk" role="16NL0q">
            <node concept="3clFbS" id="6LTgXmMpUGl" role="2VODD2">
              <node concept="3clFbF" id="6LTgXmMpUGm" role="3cqZAp">
                <node concept="2OqwBi" id="6LTgXmMpUGn" role="3clFbG">
                  <node concept="2qgKlT" id="6LTgXmMpUGo" role="2OqNvi">
                    <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                  </node>
                  <node concept="23wRSc" id="6LTgXmMpUGp" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMpUFY" resolve="concept" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3eGOop" id="6LTgXmMpUGq" role="3ft5RZ">
        <node concept="ucgPf" id="6LTgXmMpUGr" role="3aKz83">
          <node concept="3clFbS" id="6LTgXmMpUGs" role="2VODD2">
            <node concept="3cpWs8" id="6LTgXmMpUGt" role="3cqZAp">
              <node concept="3cpWsn" id="6LTgXmMpUGu" role="3cpWs9">
                <property role="TrG5h" value="node" />
                <node concept="3Tqbb2" id="6LTgXmMpUGv" role="1tU5fm">
                  <ref role="ehGHo" to="veyb:4B5aqq4dEoN" resolve="LaboratoryTestResultAsText" />
                </node>
                <node concept="2OqwBi" id="6LTgXmMpUGw" role="33vP2m">
                  <node concept="23wRSc" id="6LTgXmMpUGx" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMpUFY" resolve="concept" />
                  </node>
                  <node concept="q_SaT" id="6LTgXmMpUGy" role="2OqNvi">
                    <node concept="1yR$tW" id="6LTgXmMpUGz" role="1wAxWu" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="6LTgXmMpUG$" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmMpUG_" role="3clFbG">
                <node concept="2OqwBi" id="6LTgXmMpUGA" role="2Oq$k0">
                  <node concept="37vLTw" id="6LTgXmMpUGB" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMpUGu" resolve="node" />
                  </node>
                  <node concept="3TrEf2" id="6LTgXmNdRdA" role="2OqNvi">
                    <ref role="3Tt5mk" to="veyb:4B5aqq3QF7a" resolve="specimenTypeReference" />
                  </node>
                </node>
                <node concept="2DeJnY" id="6LTgXmMpUGD" role="2OqNvi" />
              </node>
            </node>
            <node concept="3clFbF" id="6LTgXmMpUGE" role="3cqZAp">
              <node concept="37vLTw" id="6LTgXmMpUGF" role="3clFbG">
                <ref role="3cqZAo" node="6LTgXmMpUGu" resolve="node" />
              </node>
            </node>
          </node>
        </node>
        <node concept="16NfWO" id="6LTgXmMpUGG" role="upBLP">
          <node concept="uGdhv" id="6LTgXmMpUGH" role="16NeZM">
            <node concept="3clFbS" id="6LTgXmMpUGI" role="2VODD2">
              <node concept="3clFbF" id="6LTgXmMpUGJ" role="3cqZAp">
                <node concept="2OqwBi" id="6LTgXmMpUGK" role="3clFbG">
                  <node concept="2qgKlT" id="6LTgXmNdRLi" role="2OqNvi">
                    <ref role="37wK5l" to="q9tj:6LTgXmMlgN$" resolve="getAliasWithSpecimenType" />
                  </node>
                  <node concept="23wRSc" id="6LTgXmMpUGM" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMpUFY" resolve="concept" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="16NL0t" id="6LTgXmMpUGN" role="upBLP">
          <node concept="uGdhv" id="6LTgXmMpUGO" role="16NL0q">
            <node concept="3clFbS" id="6LTgXmMpUGP" role="2VODD2">
              <node concept="3clFbF" id="6LTgXmMpUGQ" role="3cqZAp">
                <node concept="2OqwBi" id="6LTgXmMpUGR" role="3clFbG">
                  <node concept="2qgKlT" id="6LTgXmNdSnw" role="2OqNvi">
                    <ref role="37wK5l" to="q9tj:6LTgXmMlq1k" resolve="getDescriptionWithSpecimenType" />
                  </node>
                  <node concept="23wRSc" id="6LTgXmMpUGT" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMpUFY" resolve="concept" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="PDjyzky_yH">
    <property role="3GE5qa" value="base.data.boolean" />
    <ref role="aqKnT" to="veyb:PDjyzky6xW" resolve="LaboratoryTestHasResultAsText" />
    <node concept="3ft6gV" id="6LTgXmMmuPX" role="3ft7WO">
      <node concept="23wRS9" id="6LTgXmMmuPY" role="23Ddnj">
        <property role="TrG5h" value="concept" />
        <node concept="23DdeO" id="6LTgXmMmuPZ" role="23DdeQ">
          <node concept="3clFbS" id="6LTgXmMmuQ0" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmMmuQ1" role="3cqZAp">
              <node concept="35c_gC" id="6LTgXmMmuQ2" role="3clFbG">
                <ref role="35c_gD" to="veyb:PDjyzky6xW" resolve="LaboratoryTestHasResultAsText" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3bZ5Sz" id="6LTgXmMmuQ3" role="1tU5fm">
          <ref role="3bZ5Sy" to="veyb:PDjyzky6xW" resolve="LaboratoryTestHasResultAsText" />
        </node>
      </node>
      <node concept="3eGOop" id="6LTgXmMmuQ4" role="3ft5RZ">
        <node concept="ucgPf" id="6LTgXmMmuQ5" role="3aKz83">
          <node concept="3clFbS" id="6LTgXmMmuQ6" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmMmuQ7" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmMmuQ8" role="3clFbG">
                <node concept="23wRSc" id="6LTgXmMmuQ9" role="2Oq$k0">
                  <ref role="3cqZAo" node="6LTgXmMmuPY" resolve="concept" />
                </node>
                <node concept="q_SaT" id="6LTgXmMmuQa" role="2OqNvi">
                  <node concept="1yR$tW" id="6LTgXmMmuQb" role="1wAxWu" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="16NfWO" id="6LTgXmMmuQc" role="upBLP">
          <node concept="uGdhv" id="6LTgXmMmuQd" role="16NeZM">
            <node concept="3clFbS" id="6LTgXmMmuQe" role="2VODD2">
              <node concept="3clFbF" id="6LTgXmMmuQf" role="3cqZAp">
                <node concept="2OqwBi" id="6LTgXmMmuQg" role="3clFbG">
                  <node concept="2qgKlT" id="6LTgXmMmuQh" role="2OqNvi">
                    <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                  </node>
                  <node concept="23wRSc" id="6LTgXmMmuQi" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMmuPY" resolve="concept" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="16NL0t" id="6LTgXmMmuQj" role="upBLP">
          <node concept="uGdhv" id="6LTgXmMmuQk" role="16NL0q">
            <node concept="3clFbS" id="6LTgXmMmuQl" role="2VODD2">
              <node concept="3clFbF" id="6LTgXmMmuQm" role="3cqZAp">
                <node concept="2OqwBi" id="6LTgXmMmuQn" role="3clFbG">
                  <node concept="2qgKlT" id="6LTgXmMmuQo" role="2OqNvi">
                    <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                  </node>
                  <node concept="23wRSc" id="6LTgXmMmuQp" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMmuPY" resolve="concept" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3eGOop" id="6LTgXmMmuQq" role="3ft5RZ">
        <node concept="ucgPf" id="6LTgXmMmuQr" role="3aKz83">
          <node concept="3clFbS" id="6LTgXmMmuQs" role="2VODD2">
            <node concept="3cpWs8" id="6LTgXmMmuQt" role="3cqZAp">
              <node concept="3cpWsn" id="6LTgXmMmuQu" role="3cpWs9">
                <property role="TrG5h" value="node" />
                <node concept="3Tqbb2" id="6LTgXmMmuQv" role="1tU5fm">
                  <ref role="ehGHo" to="veyb:PDjyzky6xW" resolve="LaboratoryTestHasResultAsText" />
                </node>
                <node concept="2OqwBi" id="6LTgXmMmuQw" role="33vP2m">
                  <node concept="23wRSc" id="6LTgXmMmuQx" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMmuPY" resolve="concept" />
                  </node>
                  <node concept="q_SaT" id="6LTgXmMmuQy" role="2OqNvi">
                    <node concept="1yR$tW" id="6LTgXmMmuQz" role="1wAxWu" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="6LTgXmMmuQ$" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmMmuQ_" role="3clFbG">
                <node concept="2OqwBi" id="6LTgXmMmuQA" role="2Oq$k0">
                  <node concept="37vLTw" id="6LTgXmMmuQB" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMmuQu" resolve="node" />
                  </node>
                  <node concept="3TrEf2" id="6LTgXmNd$tA" role="2OqNvi">
                    <ref role="3Tt5mk" to="veyb:4B5aqq3QF7a" resolve="specimenTypeReference" />
                  </node>
                </node>
                <node concept="2DeJnY" id="6LTgXmMmuQD" role="2OqNvi" />
              </node>
            </node>
            <node concept="3clFbF" id="6LTgXmMmuQE" role="3cqZAp">
              <node concept="37vLTw" id="6LTgXmMmuQF" role="3clFbG">
                <ref role="3cqZAo" node="6LTgXmMmuQu" resolve="node" />
              </node>
            </node>
          </node>
        </node>
        <node concept="16NfWO" id="6LTgXmMmuQG" role="upBLP">
          <node concept="uGdhv" id="6LTgXmMmuQH" role="16NeZM">
            <node concept="3clFbS" id="6LTgXmMmuQI" role="2VODD2">
              <node concept="3clFbF" id="6LTgXmMmuQJ" role="3cqZAp">
                <node concept="2OqwBi" id="6LTgXmMmuQK" role="3clFbG">
                  <node concept="2qgKlT" id="6LTgXmNdNey" role="2OqNvi">
                    <ref role="37wK5l" to="q9tj:6LTgXmMlgN$" resolve="getAliasWithSpecimenType" />
                  </node>
                  <node concept="23wRSc" id="6LTgXmMmuQM" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMmuPY" resolve="concept" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="16NL0t" id="6LTgXmMmuQN" role="upBLP">
          <node concept="uGdhv" id="6LTgXmMmuQO" role="16NL0q">
            <node concept="3clFbS" id="6LTgXmMmuQP" role="2VODD2">
              <node concept="3clFbF" id="6LTgXmMmuQQ" role="3cqZAp">
                <node concept="2OqwBi" id="6LTgXmMmuQR" role="3clFbG">
                  <node concept="2qgKlT" id="6LTgXmNdNLX" role="2OqNvi">
                    <ref role="37wK5l" to="q9tj:6LTgXmMlq1k" resolve="getDescriptionWithSpecimenType" />
                  </node>
                  <node concept="23wRSc" id="6LTgXmMmuQT" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMmuPY" resolve="concept" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="6LTgXmNdAvm">
    <property role="3GE5qa" value="base.data" />
    <ref role="1XX52x" to="veyb:4B5aqq3QF78" resolve="ILaboratoryTestWithTextResultDataValue" />
    <node concept="3EZMnI" id="4B5aqq3QF9R" role="2wV5jI">
      <node concept="2iRfu4" id="4B5aqq3QF9S" role="2iSdaV" />
      <node concept="B$lHz" id="4B5aqq3QF9T" role="3EZMnx" />
      <node concept="3F1sOY" id="6LTgXmMnI9B" role="3EZMnx">
        <ref role="1NtTu8" to="veyb:6LTgXmMj7Lm" resolve="testReference" />
      </node>
      <node concept="3EZMnI" id="6LTgXmMdFLF" role="3EZMnx">
        <node concept="2iRfu4" id="6LTgXmMdFLG" role="2iSdaV" />
        <node concept="1HlG4h" id="4B5aqq3QF9V" role="3EZMnx">
          <ref role="1k5W1q" to="uubs:2XLt5KUltJN" resolve="DataValueKeyword" />
          <node concept="1HfYo3" id="4B5aqq3QF9W" role="1HlULh">
            <node concept="3TQlhw" id="4B5aqq3QF9X" role="1Hhtcw">
              <node concept="3clFbS" id="4B5aqq3QF9Y" role="2VODD2">
                <node concept="3clFbF" id="4B5aqq3QF9Z" role="3cqZAp">
                  <node concept="2OqwBi" id="4B5aqq3QFa0" role="3clFbG">
                    <node concept="2OqwBi" id="4B5aqq3QFa1" role="2Oq$k0">
                      <node concept="pncrf" id="4B5aqq3QFa2" role="2Oq$k0" />
                      <node concept="2yIwOk" id="4B5aqq3QFa3" role="2OqNvi" />
                    </node>
                    <node concept="2qgKlT" id="6LTgXmNdIpl" role="2OqNvi">
                      <ref role="37wK5l" to="q9tj:4B5aqq3QFqK" resolve="getWithSpecimenTypeKeyword" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3F1sOY" id="4B5aqq3QFa5" role="3EZMnx">
          <ref role="1NtTu8" to="veyb:4B5aqq3QF7a" resolve="specimenTypeReference" />
        </node>
        <node concept="pkWqt" id="6LTgXmMdGxH" role="pqm2j">
          <node concept="3clFbS" id="6LTgXmMdGxI" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmMdGOL" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmMdI1v" role="3clFbG">
                <node concept="2OqwBi" id="6LTgXmMdH4g" role="2Oq$k0">
                  <node concept="pncrf" id="6LTgXmMdGOK" role="2Oq$k0" />
                  <node concept="3TrEf2" id="6LTgXmMdHyY" role="2OqNvi">
                    <ref role="3Tt5mk" to="veyb:4B5aqq3QF7a" resolve="specimenTypeReference" />
                  </node>
                </node>
                <node concept="3x8VRR" id="6LTgXmMdIwa" role="2OqNvi" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

