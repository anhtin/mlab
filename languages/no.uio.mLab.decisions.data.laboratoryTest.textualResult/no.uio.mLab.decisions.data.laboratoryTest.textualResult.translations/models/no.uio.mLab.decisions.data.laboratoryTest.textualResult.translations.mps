<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:8040e133-faf5-468f-9abc-4d6ff2d83b11(no.uio.mLab.decisions.data.laboratoryTest.textualResult.translations)">
  <persistence version="9" />
  <languages>
    <use id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage" version="6" />
  </languages>
  <imports>
    <import index="9igz" ref="r:0ba33904-c124-449b-bde4-640f55a71953(no.uio.mLab.shared.translations)" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1465982738277781862" name="jetbrains.mps.baseLanguage.structure.PlaceholderMember" flags="ng" index="2tJIrI" />
      <concept id="1188207840427" name="jetbrains.mps.baseLanguage.structure.AnnotationInstance" flags="nn" index="2AHcQZ">
        <reference id="1188208074048" name="annotation" index="2AI5Lk" />
      </concept>
      <concept id="1188208481402" name="jetbrains.mps.baseLanguage.structure.HasAnnotation" flags="ng" index="2AJDlI">
        <child id="1188208488637" name="annotation" index="2AJF6D" />
      </concept>
      <concept id="2820489544401957797" name="jetbrains.mps.baseLanguage.structure.DefaultClassCreator" flags="nn" index="HV5vD">
        <reference id="2820489544401957798" name="classifier" index="HV5vE" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1070462154015" name="jetbrains.mps.baseLanguage.structure.StaticFieldDeclaration" flags="ig" index="Wx3nA">
        <property id="6468716278899126575" name="isVolatile" index="2dlcS1" />
        <property id="6468716278899125786" name="isTransient" index="2dld4O" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1081236700938" name="jetbrains.mps.baseLanguage.structure.StaticMethodDeclaration" flags="ig" index="2YIFZL" />
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu">
        <child id="1095933932569" name="implementedInterface" index="EKbjA" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <property id="1176718929932" name="isFinal" index="3TUv4t" />
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <property id="4276006055363816570" name="isSynchronized" index="od$2w" />
        <property id="1181808852946" name="isFinal" index="DiZV1" />
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123165" name="jetbrains.mps.baseLanguage.structure.InstanceMethodDeclaration" flags="ig" index="3clFb_">
        <property id="1178608670077" name="isAbstract" index="1EzhhJ" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242875" name="jetbrains.mps.baseLanguage.structure.PlusExpression" flags="nn" index="3cpWs3" />
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1107461130800" name="jetbrains.mps.baseLanguage.structure.Classifier" flags="ng" index="3pOWGL">
        <child id="5375687026011219971" name="member" index="jymVt" unordered="true" />
      </concept>
      <concept id="7812454656619025416" name="jetbrains.mps.baseLanguage.structure.MethodDeclaration" flags="ng" index="1rXfSm">
        <property id="8355037393041754995" name="isNative" index="2aFKle" />
      </concept>
      <concept id="7812454656619025412" name="jetbrains.mps.baseLanguage.structure.LocalMethodCall" flags="nn" index="1rXfSq" />
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1107796713796" name="jetbrains.mps.baseLanguage.structure.Interface" flags="ig" index="3HP615" />
      <concept id="1163670490218" name="jetbrains.mps.baseLanguage.structure.SwitchStatement" flags="nn" index="3KaCP$">
        <child id="1163670592366" name="defaultBlock" index="3Kb1Dw" />
        <child id="1163670766145" name="expression" index="3KbGdf" />
        <child id="1163670772911" name="case" index="3KbHQx" />
      </concept>
      <concept id="1163670641947" name="jetbrains.mps.baseLanguage.structure.SwitchCase" flags="ng" index="3KbdKl">
        <child id="1163670677455" name="expression" index="3Kbmr1" />
        <child id="1163670683720" name="body" index="3Kbo56" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
      <concept id="1146644623116" name="jetbrains.mps.baseLanguage.structure.PrivateVisibility" flags="nn" index="3Tm6S6" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="3HP615" id="4zMac8rUNtP">
    <property role="3GE5qa" value="internationalization" />
    <property role="TrG5h" value="ITextResultLaboratoryTestDataTranslations" />
    <node concept="3clFb_" id="6LTgXmNdXX8" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getILaboratoryTestWithTextResultDataValueIncludeSpecimenTypeIntentionDescription" />
      <node concept="3clFbS" id="6LTgXmNdXXb" role="3clF47" />
      <node concept="3Tm1VV" id="6LTgXmNdXXc" role="1B3o_S" />
      <node concept="17QB3L" id="6LTgXmNdXTx" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="6LTgXmNdY3h" role="jymVt" />
    <node concept="3clFb_" id="4V3GMfXwkrq" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultAsTextAlias" />
      <node concept="3clFbS" id="4V3GMfXwkrt" role="3clF47" />
      <node concept="3Tm1VV" id="4V3GMfXwkru" role="1B3o_S" />
      <node concept="17QB3L" id="4V3GMfXwkrm" role="3clF45" />
    </node>
    <node concept="3clFb_" id="4V3GMfXwkse" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultAsTextDescription" />
      <node concept="3clFbS" id="4V3GMfXwksh" role="3clF47" />
      <node concept="3Tm1VV" id="4V3GMfXwksi" role="1B3o_S" />
      <node concept="17QB3L" id="4V3GMfXwkrX" role="3clF45" />
    </node>
    <node concept="3clFb_" id="4B5aqq4dEqR" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultWithSpecimenAsTextAlias" />
      <node concept="3clFbS" id="4B5aqq4dEqS" role="3clF47" />
      <node concept="3Tm1VV" id="4B5aqq4dEqT" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4dEqU" role="3clF45" />
    </node>
    <node concept="3clFb_" id="4B5aqq4dEqV" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultWithSpecimenAsTextDescription" />
      <node concept="3clFbS" id="4B5aqq4dEqW" role="3clF47" />
      <node concept="3Tm1VV" id="4B5aqq4dEqX" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4dEqY" role="3clF45" />
    </node>
    <node concept="3clFb_" id="4B5aqq4dECb" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultWithSpecimenAsTextKeywordTextResult" />
      <node concept="3clFbS" id="4B5aqq4dECc" role="3clF47" />
      <node concept="3Tm1VV" id="4B5aqq4dECd" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4dECe" role="3clF45" />
    </node>
    <node concept="3clFb_" id="4B5aqq4dEY1" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultWithSpecimenAsTextKeywordWithSpecimen" />
      <node concept="3clFbS" id="4B5aqq4dEY2" role="3clF47" />
      <node concept="3Tm1VV" id="4B5aqq4dEY3" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4dEY4" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="4V3GMfXwR_K" role="jymVt" />
    <node concept="3clFb_" id="4V3GMfXwRAF" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultAsTextAlias" />
      <node concept="3clFbS" id="4V3GMfXwRAI" role="3clF47" />
      <node concept="3Tm1VV" id="4V3GMfXwRAJ" role="1B3o_S" />
      <node concept="17QB3L" id="4V3GMfXwRAh" role="3clF45" />
    </node>
    <node concept="3clFb_" id="4V3GMfXwRCA" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultAsTextDescription" />
      <node concept="3clFbS" id="4V3GMfXwRCD" role="3clF47" />
      <node concept="3Tm1VV" id="4V3GMfXwRCE" role="1B3o_S" />
      <node concept="17QB3L" id="4V3GMfXwRC4" role="3clF45" />
    </node>
    <node concept="3clFb_" id="4B5aqq4dEr0" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultWithSpecimenAsTextAlias" />
      <node concept="3clFbS" id="4B5aqq4dEr1" role="3clF47" />
      <node concept="3Tm1VV" id="4B5aqq4dEr2" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4dEr3" role="3clF45" />
    </node>
    <node concept="3clFb_" id="4B5aqq4dEr4" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultWithSpecimenAsTextDescription" />
      <node concept="3clFbS" id="4B5aqq4dEr5" role="3clF47" />
      <node concept="3Tm1VV" id="4B5aqq4dEr6" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4dEr7" role="3clF45" />
    </node>
    <node concept="3clFb_" id="4B5aqq4dF48" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultWithSpecimenAsTextKeywordPreviousTextResult" />
      <node concept="3clFbS" id="4B5aqq4dF49" role="3clF47" />
      <node concept="3Tm1VV" id="4B5aqq4dF4a" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4dF4b" role="3clF45" />
    </node>
    <node concept="3clFb_" id="4B5aqq4dF4c" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultWithSpecimenAsTextKeywordWithSpecimen" />
      <node concept="3clFbS" id="4B5aqq4dF4d" role="3clF47" />
      <node concept="3Tm1VV" id="4B5aqq4dF4e" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4dF4f" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="4B5aqq4dEqg" role="jymVt" />
    <node concept="3clFb_" id="PDjyzkycdF" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasResultAsTextAlias" />
      <node concept="3clFbS" id="PDjyzkycdI" role="3clF47" />
      <node concept="3Tm1VV" id="PDjyzkycdJ" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkycbW" role="3clF45" />
    </node>
    <node concept="3clFb_" id="PDjyzkycoB" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasResultAsTextDescription" />
      <node concept="3clFbS" id="PDjyzkycoE" role="3clF47" />
      <node concept="3Tm1VV" id="PDjyzkycoF" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkyciY" role="3clF45" />
    </node>
    <node concept="3clFb_" id="PDjyzkycte" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasResultWithSpecimenAsTextAlias" />
      <node concept="3clFbS" id="PDjyzkyctf" role="3clF47" />
      <node concept="3Tm1VV" id="PDjyzkyctg" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkycth" role="3clF45" />
    </node>
    <node concept="3clFb_" id="PDjyzkycti" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasResultWithSpecimenAsTextDescription" />
      <node concept="3clFbS" id="PDjyzkyctj" role="3clF47" />
      <node concept="3Tm1VV" id="PDjyzkyctk" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkyctl" role="3clF45" />
    </node>
    <node concept="3clFb_" id="PDjyzkycIh" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasResultWithSpecimenAsTextKeywordHasResultAsText" />
      <node concept="3clFbS" id="PDjyzkycIi" role="3clF47" />
      <node concept="3Tm1VV" id="PDjyzkycIj" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkycIk" role="3clF45" />
    </node>
    <node concept="3clFb_" id="PDjyzkycIl" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasResultWithSpecimenAsTextKeywordWithSpecimen" />
      <node concept="3clFbS" id="PDjyzkycIm" role="3clF47" />
      <node concept="3Tm1VV" id="PDjyzkycIn" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkycIo" role="3clF45" />
    </node>
    <node concept="3Tm1VV" id="4zMac8rUNtQ" role="1B3o_S" />
  </node>
  <node concept="312cEu" id="4zMac8rUNsN">
    <property role="TrG5h" value="TextResultLaboratoryTestDataTranslationProvider" />
    <property role="3GE5qa" value="internationalization" />
    <node concept="Wx3nA" id="4zMac8rVBeK" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="TrG5h" value="displayTranslations" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm1VV" id="4zMac8rVBei" role="1B3o_S" />
      <node concept="3uibUv" id="4zMac8rVBeC" role="1tU5fm">
        <ref role="3uigEE" node="4zMac8rUNtP" resolve="ITextResultLaboratoryTestDataTranslations" />
      </node>
      <node concept="1rXfSq" id="4zMac8rVBfh" role="33vP2m">
        <ref role="37wK5l" node="4zMac8rUNuE" resolve="init" />
        <node concept="10M0yZ" id="ofcHXfTMGc" role="37wK5m">
          <ref role="3cqZAo" to="9igz:5fXsrCVmzVm" resolve="displayLanguage" />
          <ref role="1PxDUh" to="9igz:5fXsrCVmxTN" resolve="TranslationConfiguration" />
        </node>
      </node>
    </node>
    <node concept="Wx3nA" id="1Hxyv4DUmfq" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="TrG5h" value="generationTranslations" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm1VV" id="1Hxyv4DUmfr" role="1B3o_S" />
      <node concept="3uibUv" id="1Hxyv4DUmfs" role="1tU5fm">
        <ref role="3uigEE" node="4zMac8rUNtP" resolve="ITextResultLaboratoryTestDataTranslations" />
      </node>
      <node concept="1rXfSq" id="1Hxyv4DUmft" role="33vP2m">
        <ref role="37wK5l" node="4zMac8rUNuE" resolve="init" />
        <node concept="10M0yZ" id="ofcHXfTMH1" role="37wK5m">
          <ref role="1PxDUh" to="9igz:5fXsrCVmxTN" resolve="TranslationConfiguration" />
          <ref role="3cqZAo" to="9igz:5fXsrCVmzVN" resolve="generationLanguage" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="4zMac8rUNtp" role="jymVt" />
    <node concept="2YIFZL" id="4zMac8rUNuE" role="jymVt">
      <property role="TrG5h" value="init" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="4zMac8rUNuH" role="3clF47">
        <node concept="3KaCP$" id="4zMac8rUNv7" role="3cqZAp">
          <node concept="3KbdKl" id="4zMac8rUNXu" role="3KbHQx">
            <node concept="Xl_RD" id="4zMac8rUNXU" role="3Kbmr1">
              <property role="Xl_RC" value="no" />
            </node>
            <node concept="3clFbS" id="4zMac8rUNXw" role="3Kbo56">
              <node concept="3cpWs6" id="4zMac8rUNYt" role="3cqZAp">
                <node concept="2ShNRf" id="4zMac8rUO3y" role="3cqZAk">
                  <node concept="HV5vD" id="4zMac8rUP5z" role="2ShVmc">
                    <ref role="HV5vE" node="4zMac8rUO1O" resolve="NoTextResultLaboratoryTestDataTranslations" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="4zMac8rUNYX" role="3Kb1Dw">
            <node concept="3cpWs6" id="4zMac8rUNZC" role="3cqZAp">
              <node concept="2ShNRf" id="4zMac8rUP6U" role="3cqZAk">
                <node concept="HV5vD" id="4zMac8rUPap" role="2ShVmc">
                  <ref role="HV5vE" node="4zMac8rUO0_" resolve="EnTextResultLaboratoryTestDataTranslations" />
                </node>
              </node>
            </node>
          </node>
          <node concept="37vLTw" id="1Hxyv4DUmde" role="3KbGdf">
            <ref role="3cqZAo" node="1Hxyv4DUm7P" resolve="language" />
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="4zMac8rW809" role="1B3o_S" />
      <node concept="3uibUv" id="4zMac8rUNuz" role="3clF45">
        <ref role="3uigEE" node="4zMac8rUNtP" resolve="ITextResultLaboratoryTestDataTranslations" />
      </node>
      <node concept="37vLTG" id="1Hxyv4DUm7P" role="3clF46">
        <property role="TrG5h" value="language" />
        <node concept="17QB3L" id="1Hxyv4DUm7O" role="1tU5fm" />
      </node>
    </node>
    <node concept="3Tm1VV" id="4zMac8rUNsO" role="1B3o_S" />
  </node>
  <node concept="312cEu" id="4zMac8rUO0_">
    <property role="TrG5h" value="EnTextResultLaboratoryTestDataTranslations" />
    <property role="3GE5qa" value="localization" />
    <node concept="3clFb_" id="6LTgXmNe0$L" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getILaboratoryTestWithTextResultDataValueIncludeSpecimenTypeIntentionDescription" />
      <node concept="3Tm1VV" id="6LTgXmNe0$N" role="1B3o_S" />
      <node concept="17QB3L" id="6LTgXmNe0$O" role="3clF45" />
      <node concept="3clFbS" id="6LTgXmNe0$P" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNe14_" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNe14$" role="3clFbG">
            <property role="Xl_RC" value="Include Specimen Type" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6LTgXmNe0$Q" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="6LTgXmNe099" role="jymVt" />
    <node concept="3clFb_" id="4V3GMfXwkIt" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultAsTextAlias" />
      <node concept="3Tm1VV" id="4V3GMfXwkIv" role="1B3o_S" />
      <node concept="17QB3L" id="4V3GMfXwkIw" role="3clF45" />
      <node concept="3clFbS" id="4V3GMfXwkIx" role="3clF47">
        <node concept="3clFbF" id="4V3GMfXwkNI" role="3cqZAp">
          <node concept="Xl_RD" id="4V3GMfXwkNH" role="3clFbG">
            <property role="Xl_RC" value="textual test result" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4V3GMfXwkIy" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4V3GMfXwkIz" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultAsTextDescription" />
      <node concept="3Tm1VV" id="4V3GMfXwkI_" role="1B3o_S" />
      <node concept="17QB3L" id="4V3GMfXwkIA" role="3clF45" />
      <node concept="3clFbS" id="4V3GMfXwkIB" role="3clF47">
        <node concept="3clFbF" id="4V3GMfXwkPe" role="3cqZAp">
          <node concept="Xl_RD" id="4V3GMfXwkPd" role="3clFbG">
            <property role="Xl_RC" value="check textual value of current result of test" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4V3GMfXwkIC" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq4dF$D" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultWithSpecimenAsTextAlias" />
      <node concept="3Tm1VV" id="4B5aqq4dF$F" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4dF$G" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq4dF$H" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4dFP2" role="3cqZAp">
          <node concept="3cpWs3" id="4B5aqq4dG4A" role="3clFbG">
            <node concept="Xl_RD" id="4B5aqq4dGgm" role="3uHU7w">
              <property role="Xl_RC" value=" with" />
            </node>
            <node concept="1rXfSq" id="4B5aqq4dFP1" role="3uHU7B">
              <ref role="37wK5l" node="4V3GMfXwkIt" resolve="getLaboratoryTestResultAsTextAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq4dF$I" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq4dF$J" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultWithSpecimenAsTextDescription" />
      <node concept="3Tm1VV" id="4B5aqq4dF$L" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4dF$M" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq4dF$N" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4dGtw" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq4dGty" role="3clFbG">
            <property role="Xl_RC" value="check textual value of current result of test on specimen type" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq4dF$O" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq4dF$P" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultWithSpecimenAsTextKeywordTextResult" />
      <node concept="3Tm1VV" id="4B5aqq4dF$R" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4dF$S" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq4dF$T" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4dGAD" role="3cqZAp">
          <node concept="1rXfSq" id="4B5aqq4dGAF" role="3clFbG">
            <ref role="37wK5l" node="4V3GMfXwkIt" resolve="getLaboratoryTestResultAsTextAlias" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq4dF$U" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq4dF$V" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultWithSpecimenAsTextKeywordWithSpecimen" />
      <node concept="3Tm1VV" id="4B5aqq4dF$X" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4dF$Y" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq4dF$Z" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4dGMm" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq4dGMl" role="3clFbG">
            <property role="Xl_RC" value="with" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq4dF_0" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="4V3GMfXwV06" role="jymVt" />
    <node concept="2tJIrI" id="6LTgXmMpeiS" role="jymVt" />
    <node concept="3clFb_" id="4V3GMfXwUPA" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultAsTextAlias" />
      <node concept="3Tm1VV" id="4V3GMfXwUPC" role="1B3o_S" />
      <node concept="17QB3L" id="4V3GMfXwUPD" role="3clF45" />
      <node concept="3clFbS" id="4V3GMfXwUPE" role="3clF47">
        <node concept="3clFbF" id="4V3GMfXwV4D" role="3cqZAp">
          <node concept="Xl_RD" id="4V3GMfXwV4C" role="3clFbG">
            <property role="Xl_RC" value="previous textual test result" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4V3GMfXwUPF" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4V3GMfXwUPG" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultAsTextDescription" />
      <node concept="3Tm1VV" id="4V3GMfXwUPI" role="1B3o_S" />
      <node concept="17QB3L" id="4V3GMfXwUPJ" role="3clF45" />
      <node concept="3clFbS" id="4V3GMfXwUPK" role="3clF47">
        <node concept="3clFbF" id="4V3GMfXwV5y" role="3cqZAp">
          <node concept="Xl_RD" id="4V3GMfXwV5x" role="3clFbG">
            <property role="Xl_RC" value="check textual value of previous result of test on patient" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4V3GMfXwUPL" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq4dF_1" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultWithSpecimenAsTextAlias" />
      <node concept="3Tm1VV" id="4B5aqq4dF_3" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4dF_4" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq4dF_5" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4dH4A" role="3cqZAp">
          <node concept="3cpWs3" id="4B5aqq4dHsU" role="3clFbG">
            <node concept="Xl_RD" id="4B5aqq4dHCC" role="3uHU7w">
              <property role="Xl_RC" value=" with" />
            </node>
            <node concept="1rXfSq" id="4B5aqq4dH4_" role="3uHU7B">
              <ref role="37wK5l" node="4V3GMfXwUPA" resolve="getLaboratoryTestPreviousResultAsTextAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq4dF_6" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq4dF_7" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultWithSpecimenAsTextDescription" />
      <node concept="3Tm1VV" id="4B5aqq4dF_9" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4dF_a" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq4dF_b" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4dHYM" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq4dHYO" role="3clFbG">
            <property role="Xl_RC" value="check textual value of previous result of test on specimen type from patient" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq4dF_c" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq4dF_d" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultWithSpecimenAsTextKeywordPreviousTextResult" />
      <node concept="3Tm1VV" id="4B5aqq4dF_f" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4dF_g" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq4dF_h" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4dI3N" role="3cqZAp">
          <node concept="1rXfSq" id="4B5aqq4dI3P" role="3clFbG">
            <ref role="37wK5l" node="4V3GMfXwUPA" resolve="getLaboratoryTestPreviousResultAsTextAlias" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq4dF_i" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq4dF_j" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultWithSpecimenAsTextKeywordWithSpecimen" />
      <node concept="3Tm1VV" id="4B5aqq4dF_l" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4dF_m" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq4dF_n" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4dIjC" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq4dIjB" role="3clFbG">
            <property role="Xl_RC" value="with" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq4dF_o" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="4B5aqq4dFrc" role="jymVt" />
    <node concept="2tJIrI" id="6LTgXmMpeIr" role="jymVt" />
    <node concept="3clFb_" id="PDjyzkydwn" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasResultAsTextAlias" />
      <node concept="3Tm1VV" id="PDjyzkydwp" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkydwq" role="3clF45" />
      <node concept="3clFbS" id="PDjyzkydwr" role="3clF47">
        <node concept="3clFbF" id="PDjyzkydRp" role="3cqZAp">
          <node concept="Xl_RD" id="PDjyzkydRo" role="3clFbG">
            <property role="Xl_RC" value="has textual test result" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkydws" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzkydwt" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasResultAsTextDescription" />
      <node concept="3Tm1VV" id="PDjyzkydwv" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkydww" role="3clF45" />
      <node concept="3clFbS" id="PDjyzkydwx" role="3clF47">
        <node concept="3clFbF" id="PDjyzkydTj" role="3cqZAp">
          <node concept="Xl_RD" id="PDjyzkydTi" role="3clFbG">
            <property role="Xl_RC" value="check if result of test has textual value" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkydwy" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzkydVK" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasResultWithSpecimenAsTextAlias" />
      <node concept="3Tm1VV" id="PDjyzkydVM" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkydVN" role="3clF45" />
      <node concept="3clFbS" id="PDjyzkydVO" role="3clF47">
        <node concept="3clFbF" id="PDjyzkyeKC" role="3cqZAp">
          <node concept="3cpWs3" id="PDjyzkyfiq" role="3clFbG">
            <node concept="Xl_RD" id="PDjyzkyfzw" role="3uHU7w">
              <property role="Xl_RC" value=" with" />
            </node>
            <node concept="1rXfSq" id="PDjyzkyeKB" role="3uHU7B">
              <ref role="37wK5l" node="PDjyzkydwn" resolve="getLaboratoryTestHasResultAsTextAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkydVP" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzkydVQ" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasResultWithSpecimenAsTextDescription" />
      <node concept="3Tm1VV" id="PDjyzkydVS" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkydVT" role="3clF45" />
      <node concept="3clFbS" id="PDjyzkydVU" role="3clF47">
        <node concept="3clFbF" id="PDjyzkylOf" role="3cqZAp">
          <node concept="Xl_RD" id="PDjyzkylOe" role="3clFbG">
            <property role="Xl_RC" value="check if result of test with specimen has textual value" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkydVV" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzkydVW" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasResultWithSpecimenAsTextKeywordHasResultAsText" />
      <node concept="3Tm1VV" id="PDjyzkydVY" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkydVZ" role="3clF45" />
      <node concept="3clFbS" id="PDjyzkydW0" role="3clF47">
        <node concept="3clFbF" id="PDjyzkylUE" role="3cqZAp">
          <node concept="1rXfSq" id="PDjyzkylUG" role="3clFbG">
            <ref role="37wK5l" node="PDjyzkydwn" resolve="getLaboratoryTestHasResultAsTextAlias" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkydW1" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzkydW2" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasResultWithSpecimenAsTextKeywordWithSpecimen" />
      <node concept="3Tm1VV" id="PDjyzkydW4" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkydW5" role="3clF45" />
      <node concept="3clFbS" id="PDjyzkydW6" role="3clF47">
        <node concept="3clFbF" id="PDjyzkymbH" role="3cqZAp">
          <node concept="Xl_RD" id="PDjyzkymbG" role="3clFbG">
            <property role="Xl_RC" value="with" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkydW7" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3Tm1VV" id="4zMac8rUO0A" role="1B3o_S" />
    <node concept="3uibUv" id="4zMac8rUO1F" role="EKbjA">
      <ref role="3uigEE" node="4zMac8rUNtP" resolve="ITextResultLaboratoryTestDataTranslations" />
    </node>
  </node>
  <node concept="312cEu" id="4zMac8rUO1O">
    <property role="TrG5h" value="NoTextResultLaboratoryTestDataTranslations" />
    <property role="3GE5qa" value="localization" />
    <node concept="3clFb_" id="6LTgXmNe3nK" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getILaboratoryTestWithTextResultDataValueIncludeSpecimenTypeIntentionDescription" />
      <node concept="3Tm1VV" id="6LTgXmNe3nM" role="1B3o_S" />
      <node concept="17QB3L" id="6LTgXmNe3nN" role="3clF45" />
      <node concept="3clFbS" id="6LTgXmNe3nO" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNe4KJ" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNe4KI" role="3clFbG">
            <property role="Xl_RC" value="Inkluder Materiale" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6LTgXmNe3nP" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="6LTgXmNe3Rk" role="jymVt" />
    <node concept="3clFb_" id="4V3GMfXwl5S" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultAsTextAlias" />
      <node concept="3Tm1VV" id="4V3GMfXwl5U" role="1B3o_S" />
      <node concept="17QB3L" id="4V3GMfXwl5V" role="3clF45" />
      <node concept="3clFbS" id="4V3GMfXwl5W" role="3clF47">
        <node concept="3clFbF" id="4V3GMfXwlb9" role="3cqZAp">
          <node concept="Xl_RD" id="4V3GMfXwlb8" role="3clFbG">
            <property role="Xl_RC" value="tekstlig analysesvar" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4V3GMfXwl5X" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4V3GMfXwl5Y" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultAsTextDescription" />
      <node concept="3Tm1VV" id="4V3GMfXwl60" role="1B3o_S" />
      <node concept="17QB3L" id="4V3GMfXwl61" role="3clF45" />
      <node concept="3clFbS" id="4V3GMfXwl62" role="3clF47">
        <node concept="3clFbF" id="4V3GMfXwlcs" role="3cqZAp">
          <node concept="Xl_RD" id="4V3GMfXwlcr" role="3clFbG">
            <property role="Xl_RC" value="sjekk tekstverdi gjeldende analysesvar" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4V3GMfXwl63" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq4dIuO" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultWithSpecimenAsTextAlias" />
      <node concept="3Tm1VV" id="4B5aqq4dIuQ" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4dIuR" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq4dIuS" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4dIJd" role="3cqZAp">
          <node concept="3cpWs3" id="4B5aqq4dIYL" role="3clFbG">
            <node concept="Xl_RD" id="4B5aqq4dJav" role="3uHU7w">
              <property role="Xl_RC" value=" for" />
            </node>
            <node concept="1rXfSq" id="4B5aqq4dIJc" role="3uHU7B">
              <ref role="37wK5l" node="4V3GMfXwl5S" resolve="getLaboratoryTestResultAsTextAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq4dIuT" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq4dIuU" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultWithSpecimenAsTextDescription" />
      <node concept="3Tm1VV" id="4B5aqq4dIuW" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4dIuX" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq4dIuY" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4dJo1" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq4dJo3" role="3clFbG">
            <property role="Xl_RC" value="sjekk tekstverdi på gjeldende svar for analyse på materiale" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq4dIuZ" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq4dIv0" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultWithSpecimenAsTextKeywordTextResult" />
      <node concept="3Tm1VV" id="4B5aqq4dIv2" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4dIv3" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq4dIv4" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4dJtf" role="3cqZAp">
          <node concept="1rXfSq" id="4B5aqq4dJxD" role="3clFbG">
            <ref role="37wK5l" node="4V3GMfXwl5S" resolve="getLaboratoryTestResultAsTextAlias" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq4dIv5" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq4dIv6" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultWithSpecimenAsTextKeywordWithSpecimen" />
      <node concept="3Tm1VV" id="4B5aqq4dIv8" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4dIv9" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq4dIva" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4dJHj" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq4dJHi" role="3clFbG">
            <property role="Xl_RC" value="for" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq4dIvb" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="6LTgXmMpkKL" role="jymVt" />
    <node concept="2tJIrI" id="4V3GMfXwVey" role="jymVt" />
    <node concept="3clFb_" id="4V3GMfXwV9b" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultAsTextAlias" />
      <node concept="3Tm1VV" id="4V3GMfXwV9d" role="1B3o_S" />
      <node concept="17QB3L" id="4V3GMfXwV9e" role="3clF45" />
      <node concept="3clFbS" id="4V3GMfXwV9f" role="3clF47">
        <node concept="3clFbF" id="4V3GMfXwVj5" role="3cqZAp">
          <node concept="Xl_RD" id="4V3GMfXwVj4" role="3clFbG">
            <property role="Xl_RC" value="forrige tekstlig analysesvar" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4V3GMfXwV9g" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4V3GMfXwV9h" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultAsTextDescription" />
      <node concept="3Tm1VV" id="4V3GMfXwV9j" role="1B3o_S" />
      <node concept="17QB3L" id="4V3GMfXwV9k" role="3clF45" />
      <node concept="3clFbS" id="4V3GMfXwV9l" role="3clF47">
        <node concept="3clFbF" id="4V3GMfXwVkM" role="3cqZAp">
          <node concept="Xl_RD" id="4V3GMfXwVkL" role="3clFbG">
            <property role="Xl_RC" value="sjekk tekstverdi på forrige svar for analyse på samme pasient" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4V3GMfXwV9m" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq4dIvc" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultWithSpecimenAsTextAlias" />
      <node concept="3Tm1VV" id="4B5aqq4dIve" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4dIvf" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq4dIvg" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4dJZz" role="3cqZAp">
          <node concept="3cpWs3" id="4B5aqq4dKfe" role="3clFbG">
            <node concept="Xl_RD" id="4B5aqq4dKqU" role="3uHU7w">
              <property role="Xl_RC" value=" for" />
            </node>
            <node concept="1rXfSq" id="4B5aqq4dJZy" role="3uHU7B">
              <ref role="37wK5l" node="4V3GMfXwV9b" resolve="getLaboratoryTestPreviousResultAsTextAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq4dIvh" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq4dIvi" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultWithSpecimenAsTextDescription" />
      <node concept="3Tm1VV" id="4B5aqq4dIvk" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4dIvl" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq4dIvm" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4dKCi" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq4dKCh" role="3clFbG">
            <property role="Xl_RC" value="sjekk tekstverdi på forrige svar for analyse på materiale fra pasient som tekst" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq4dIvn" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq4dIvo" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultWithSpecimenAsTextKeywordPreviousTextResult" />
      <node concept="3Tm1VV" id="4B5aqq4dIvq" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4dIvr" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq4dIvs" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4dKMe" role="3cqZAp">
          <node concept="1rXfSq" id="4B5aqq4dKMg" role="3clFbG">
            <ref role="37wK5l" node="4V3GMfXwV9b" resolve="getLaboratoryTestPreviousResultAsTextAlias" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq4dIvt" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq4dIvu" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultWithSpecimenAsTextKeywordWithSpecimen" />
      <node concept="3Tm1VV" id="4B5aqq4dIvw" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4dIvx" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq4dIvy" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4dL23" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq4dL22" role="3clFbG">
            <property role="Xl_RC" value="for" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq4dIvz" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="6LTgXmMpm3m" role="jymVt" />
    <node concept="2tJIrI" id="4B5aqq4dIpi" role="jymVt" />
    <node concept="3clFb_" id="PDjyzkynkO" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasResultAsTextAlias" />
      <node concept="3Tm1VV" id="PDjyzkynkQ" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkynkR" role="3clF45" />
      <node concept="3clFbS" id="PDjyzkynkS" role="3clF47">
        <node concept="3clFbF" id="PDjyzkynFQ" role="3cqZAp">
          <node concept="Xl_RD" id="PDjyzkynFP" role="3clFbG">
            <property role="Xl_RC" value="har tekstlig analysesvar" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkynkT" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzkynkU" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasResultAsTextDescription" />
      <node concept="3Tm1VV" id="PDjyzkynkW" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkynkX" role="3clF45" />
      <node concept="3clFbS" id="PDjyzkynkY" role="3clF47">
        <node concept="3clFbF" id="PDjyzkynHm" role="3cqZAp">
          <node concept="Xl_RD" id="PDjyzkynHl" role="3clFbG">
            <property role="Xl_RC" value="sjekk om svar for analyse på materiale har tekstlig verdi" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkynkZ" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzkyp0x" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasResultWithSpecimenAsTextAlias" />
      <node concept="3Tm1VV" id="PDjyzkyp0z" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkyp0$" role="3clF45" />
      <node concept="3clFbS" id="PDjyzkyp0_" role="3clF47">
        <node concept="3clFbF" id="PDjyzkypsO" role="3cqZAp">
          <node concept="3cpWs3" id="PDjyzkypU_" role="3clFbG">
            <node concept="Xl_RD" id="PDjyzkyqbD" role="3uHU7w">
              <property role="Xl_RC" value=" på" />
            </node>
            <node concept="1rXfSq" id="PDjyzkypsN" role="3uHU7B">
              <ref role="37wK5l" node="PDjyzkynkO" resolve="getLaboratoryTestHasResultAsTextAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkyp0A" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzkyp0B" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasResultWithSpecimenAsTextDescription" />
      <node concept="3Tm1VV" id="PDjyzkyp0D" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkyp0E" role="3clF45" />
      <node concept="3clFbS" id="PDjyzkyp0F" role="3clF47">
        <node concept="3clFbF" id="PDjyzkyqpp" role="3cqZAp">
          <node concept="Xl_RD" id="PDjyzkyqpo" role="3clFbG">
            <property role="Xl_RC" value="sjekk tekstverdi på svar fra analyse på materiale" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkyp0G" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzkyp0H" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasResultWithSpecimenAsTextKeywordHasResultAsText" />
      <node concept="3Tm1VV" id="PDjyzkyp0J" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkyp0K" role="3clF45" />
      <node concept="3clFbS" id="PDjyzkyp0L" role="3clF47">
        <node concept="3clFbF" id="PDjyzkyqrw" role="3cqZAp">
          <node concept="1rXfSq" id="PDjyzkyqvT" role="3clFbG">
            <ref role="37wK5l" node="PDjyzkynkO" resolve="getLaboratoryTestHasResultAsTextAlias" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkyp0M" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzkyp0N" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasResultWithSpecimenAsTextKeywordWithSpecimen" />
      <node concept="3Tm1VV" id="PDjyzkyp0P" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkyp0Q" role="3clF45" />
      <node concept="3clFbS" id="PDjyzkyp0R" role="3clF47">
        <node concept="3clFbF" id="PDjyzkyqKT" role="3cqZAp">
          <node concept="Xl_RD" id="PDjyzkyqKS" role="3clFbG">
            <property role="Xl_RC" value="på" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkyp0S" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3Tm1VV" id="4zMac8rUO1P" role="1B3o_S" />
    <node concept="3uibUv" id="4zMac8rUO2g" role="EKbjA">
      <ref role="3uigEE" node="4zMac8rUNtP" resolve="ITextResultLaboratoryTestDataTranslations" />
    </node>
  </node>
</model>

