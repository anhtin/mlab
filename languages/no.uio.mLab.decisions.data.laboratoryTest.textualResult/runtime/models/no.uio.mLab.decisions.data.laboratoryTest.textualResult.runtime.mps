<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:cb4e7a32-6962-4d4e-b2da-20a2eb1d1e9e(no.uio.mLab.decisions.data.laboratoryTest.textualResult.runtime)">
  <persistence version="9" />
  <languages>
    <use id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage" version="6" />
  </languages>
  <imports>
    <import index="8r9s" ref="r:0e143259-7e64-4fad-906c-02a97b75504d(no.uio.mLab.decisions.core.runtime)" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" />
    <import index="mygi" ref="r:d014d2e6-828c-4ff5-a027-d2fa848d3c1b(no.uio.mLab.decisions.references.laboratoryTest.runtime)" />
    <import index="33ny" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.util(JDK/)" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1080223426719" name="jetbrains.mps.baseLanguage.structure.OrExpression" flags="nn" index="22lmx$" />
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1465982738277781862" name="jetbrains.mps.baseLanguage.structure.PlaceholderMember" flags="ng" index="2tJIrI" />
      <concept id="1188207840427" name="jetbrains.mps.baseLanguage.structure.AnnotationInstance" flags="nn" index="2AHcQZ">
        <reference id="1188208074048" name="annotation" index="2AI5Lk" />
      </concept>
      <concept id="1188208481402" name="jetbrains.mps.baseLanguage.structure.HasAnnotation" flags="ng" index="2AJDlI">
        <child id="1188208488637" name="annotation" index="2AJF6D" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1197029447546" name="jetbrains.mps.baseLanguage.structure.FieldReferenceOperation" flags="nn" index="2OwXpG">
        <reference id="1197029500499" name="fieldDeclaration" index="2Oxat5" />
      </concept>
      <concept id="1070475354124" name="jetbrains.mps.baseLanguage.structure.ThisExpression" flags="nn" index="Xjq3P" />
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1081256982272" name="jetbrains.mps.baseLanguage.structure.InstanceOfExpression" flags="nn" index="2ZW3vV">
        <child id="1081256993305" name="classType" index="2ZW6by" />
        <child id="1081256993304" name="leftExpression" index="2ZW6bz" />
      </concept>
      <concept id="1070534058343" name="jetbrains.mps.baseLanguage.structure.NullLiteral" flags="nn" index="10Nm6u" />
      <concept id="1070534370425" name="jetbrains.mps.baseLanguage.structure.IntegerType" flags="in" index="10Oyi0" />
      <concept id="1070534644030" name="jetbrains.mps.baseLanguage.structure.BooleanType" flags="in" index="10P_77" />
      <concept id="1070534934090" name="jetbrains.mps.baseLanguage.structure.CastExpression" flags="nn" index="10QFUN">
        <child id="1070534934091" name="type" index="10QFUM" />
        <child id="1070534934092" name="expression" index="10QFUP" />
      </concept>
      <concept id="1068390468200" name="jetbrains.mps.baseLanguage.structure.FieldDeclaration" flags="ig" index="312cEg">
        <property id="8606350594693632173" name="isTransient" index="eg7rD" />
        <property id="1240249534625" name="isVolatile" index="34CwA1" />
      </concept>
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu">
        <child id="1165602531693" name="superclass" index="1zkMxy" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <property id="1176718929932" name="isFinal" index="3TUv4t" />
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <property id="4276006055363816570" name="isSynchronized" index="od$2w" />
        <property id="1181808852946" name="isFinal" index="DiZV1" />
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123165" name="jetbrains.mps.baseLanguage.structure.InstanceMethodDeclaration" flags="ig" index="3clFb_">
        <property id="1178608670077" name="isAbstract" index="1EzhhJ" />
      </concept>
      <concept id="1068580123152" name="jetbrains.mps.baseLanguage.structure.EqualsExpression" flags="nn" index="3clFbC" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123137" name="jetbrains.mps.baseLanguage.structure.BooleanConstant" flags="nn" index="3clFbT">
        <property id="1068580123138" name="value" index="3clFbU" />
      </concept>
      <concept id="1068580123140" name="jetbrains.mps.baseLanguage.structure.ConstructorDeclaration" flags="ig" index="3clFbW" />
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1068581517677" name="jetbrains.mps.baseLanguage.structure.VoidType" flags="in" index="3cqZAl" />
      <concept id="1079359253375" name="jetbrains.mps.baseLanguage.structure.ParenthesizedExpression" flags="nn" index="1eOMI4">
        <child id="1079359253376" name="expression" index="1eOMHV" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1107461130800" name="jetbrains.mps.baseLanguage.structure.Classifier" flags="ng" index="3pOWGL">
        <child id="5375687026011219971" name="member" index="jymVt" unordered="true" />
      </concept>
      <concept id="7812454656619025416" name="jetbrains.mps.baseLanguage.structure.MethodDeclaration" flags="ng" index="1rXfSm">
        <property id="8355037393041754995" name="isNative" index="2aFKle" />
      </concept>
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
        <child id="1109201940907" name="parameter" index="11_B2D" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1073239437375" name="jetbrains.mps.baseLanguage.structure.NotEqualsExpression" flags="nn" index="3y3z36" />
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1107796713796" name="jetbrains.mps.baseLanguage.structure.Interface" flags="ig" index="3HP615">
        <child id="1107797138135" name="extendedInterface" index="3HQHJm" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
      <concept id="1146644623116" name="jetbrains.mps.baseLanguage.structure.PrivateVisibility" flags="nn" index="3Tm6S6" />
      <concept id="1080120340718" name="jetbrains.mps.baseLanguage.structure.AndExpression" flags="nn" index="1Wc70l" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="312cEu" id="1mAGFBLsNvp">
    <property role="3GE5qa" value="data.value" />
    <property role="TrG5h" value="TestPreviousResultAsText" />
    <node concept="312cEg" id="1mAGFBLsNvq" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="test" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm6S6" id="1mAGFBLsNvr" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBLC7iN" role="1tU5fm" />
    </node>
    <node concept="312cEg" id="4B5aqq4dMQY" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="specimenType" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm6S6" id="4B5aqq4dMEw" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4dMFG" role="1tU5fm" />
    </node>
    <node concept="2tJIrI" id="1mAGFBLsNvt" role="jymVt" />
    <node concept="3clFbW" id="1mAGFBLsNvu" role="jymVt">
      <node concept="3cqZAl" id="1mAGFBLsNvv" role="3clF45" />
      <node concept="3Tm1VV" id="1mAGFBLsNvw" role="1B3o_S" />
      <node concept="3clFbS" id="1mAGFBLsNvx" role="3clF47">
        <node concept="3clFbF" id="1mAGFBLsNvy" role="3cqZAp">
          <node concept="37vLTI" id="1mAGFBLsNvz" role="3clFbG">
            <node concept="37vLTw" id="1mAGFBLsNv$" role="37vLTx">
              <ref role="3cqZAo" node="1mAGFBLsNvC" resolve="test" />
            </node>
            <node concept="2OqwBi" id="1mAGFBLsNv_" role="37vLTJ">
              <node concept="Xjq3P" id="1mAGFBLsNvA" role="2Oq$k0" />
              <node concept="2OwXpG" id="1mAGFBLsNvB" role="2OqNvi">
                <ref role="2Oxat5" node="1mAGFBLsNvq" resolve="test" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4B5aqq4dN3K" role="3cqZAp">
          <node concept="37vLTI" id="4B5aqq4dNw1" role="3clFbG">
            <node concept="37vLTw" id="4B5aqq4dNFT" role="37vLTx">
              <ref role="3cqZAo" node="4B5aqq4dMlT" resolve="specimenType" />
            </node>
            <node concept="2OqwBi" id="4B5aqq4dN8d" role="37vLTJ">
              <node concept="Xjq3P" id="4B5aqq4dN3I" role="2Oq$k0" />
              <node concept="2OwXpG" id="4B5aqq4dNc$" role="2OqNvi">
                <ref role="2Oxat5" node="4B5aqq4dMQY" resolve="specimenType" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="1mAGFBLsNvC" role="3clF46">
        <property role="TrG5h" value="test" />
        <node concept="17QB3L" id="1mAGFBLC7nv" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="4B5aqq4dMlT" role="3clF46">
        <property role="TrG5h" value="specimenType" />
        <node concept="17QB3L" id="4B5aqq4dMto" role="1tU5fm" />
      </node>
    </node>
    <node concept="2tJIrI" id="1mAGFBLsNvE" role="jymVt" />
    <node concept="3clFb_" id="1mAGFBLEGUK" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestName" />
      <node concept="3Tm1VV" id="1mAGFBLEGUM" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBLEGUN" role="3clF45" />
      <node concept="3clFbS" id="1mAGFBLEGUP" role="3clF47">
        <node concept="3cpWs6" id="1mAGFBLsNvJ" role="3cqZAp">
          <node concept="2OqwBi" id="1mAGFBLsNvK" role="3cqZAk">
            <node concept="Xjq3P" id="1mAGFBLsNvL" role="2Oq$k0" />
            <node concept="2OwXpG" id="1mAGFBLsNvM" role="2OqNvi">
              <ref role="2Oxat5" node="1mAGFBLsNvq" resolve="test" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="4B5aqq4dO6D" role="jymVt" />
    <node concept="3clFb_" id="4B5aqq4dNSF" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getSpecimenType" />
      <node concept="3Tm1VV" id="4B5aqq4dNSG" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4dNSH" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq4dNSI" role="3clF47">
        <node concept="3cpWs6" id="4B5aqq4dNSJ" role="3cqZAp">
          <node concept="2OqwBi" id="4B5aqq4dNSK" role="3cqZAk">
            <node concept="Xjq3P" id="4B5aqq4dNSL" role="2Oq$k0" />
            <node concept="2OwXpG" id="4B5aqq4dPkw" role="2OqNvi">
              <ref role="2Oxat5" node="4B5aqq4dMQY" resolve="specimenType" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="5jVYnMGMIvS" role="jymVt" />
    <node concept="3clFb_" id="5jVYnMGMI1T" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="accept" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3Tm1VV" id="5jVYnMGMI1U" role="1B3o_S" />
      <node concept="37vLTG" id="5jVYnMGMI1W" role="3clF46">
        <property role="TrG5h" value="visitor" />
        <node concept="3uibUv" id="5jVYnMGMI1X" role="1tU5fm">
          <ref role="3uigEE" to="8r9s:5jVYnMGKCt5" resolve="DataValueVisitor" />
        </node>
      </node>
      <node concept="3clFbS" id="5jVYnMGMI1Y" role="3clF47">
        <node concept="3clFbJ" id="5jVYnMGMI1Z" role="3cqZAp">
          <node concept="2ZW3vV" id="5jVYnMGMI20" role="3clFbw">
            <node concept="3uibUv" id="5jVYnMGMI21" role="2ZW6by">
              <ref role="3uigEE" node="5jVYnMGJD4D" resolve="LaboratoryTestWithTextResultDataValueVisitor" />
            </node>
            <node concept="37vLTw" id="5jVYnMGMI22" role="2ZW6bz">
              <ref role="3cqZAo" node="5jVYnMGMI1W" resolve="visitor" />
            </node>
          </node>
          <node concept="3clFbS" id="5jVYnMGMI23" role="3clFbx">
            <node concept="3cpWs6" id="5jVYnMGMI24" role="3cqZAp">
              <node concept="2OqwBi" id="5jVYnMGMI25" role="3cqZAk">
                <node concept="1eOMI4" id="5jVYnMGMI26" role="2Oq$k0">
                  <node concept="10QFUN" id="5jVYnMGMI27" role="1eOMHV">
                    <node concept="3uibUv" id="5jVYnMGMI28" role="10QFUM">
                      <ref role="3uigEE" node="5jVYnMGJD4D" resolve="LaboratoryTestWithTextResultDataValueVisitor" />
                    </node>
                    <node concept="37vLTw" id="5jVYnMGMI29" role="10QFUP">
                      <ref role="3cqZAo" node="5jVYnMGMI1W" resolve="visitor" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="5jVYnMGMI2a" role="2OqNvi">
                  <ref role="37wK5l" node="5jVYnMGJD7G" resolve="visit" />
                  <node concept="Xjq3P" id="5jVYnMGMI2b" role="37wK5m" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="5jVYnMGMI2c" role="3cqZAp">
          <node concept="10Nm6u" id="5jVYnMGMI2d" role="3cqZAk" />
        </node>
      </node>
      <node concept="2AHcQZ" id="5jVYnMGMI2e" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
      <node concept="3uibUv" id="5jVYnMGVdtV" role="3clF45">
        <ref role="3uigEE" to="8r9s:2XLt5KVsHET" resolve="LisResponse" />
        <node concept="17QB3L" id="5jVYnMGVe2z" role="11_B2D" />
      </node>
    </node>
    <node concept="2tJIrI" id="7lYCqhv4vjA" role="jymVt" />
    <node concept="3clFb_" id="7lYCqhv3U1k" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="hashCode" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3Tm1VV" id="7lYCqhv3U1l" role="1B3o_S" />
      <node concept="10Oyi0" id="7lYCqhv3U1n" role="3clF45" />
      <node concept="3clFbS" id="7lYCqhv3U1q" role="3clF47">
        <node concept="3clFbF" id="7lYCqhv3U1t" role="3cqZAp">
          <node concept="2YIFZM" id="7lYCqhv4t_e" role="3clFbG">
            <ref role="37wK5l" to="33ny:~Objects.hash(java.lang.Object...):int" resolve="hash" />
            <ref role="1Pybhc" to="33ny:~Objects" resolve="Objects" />
            <node concept="2OqwBi" id="7lYCqhv4C2j" role="37wK5m">
              <node concept="Xjq3P" id="7lYCqhv4t_X" role="2Oq$k0" />
              <node concept="liA8E" id="7lYCqhv4ClT" role="2OqNvi">
                <ref role="37wK5l" to="wyt6:~Object.getClass():java.lang.Class" resolve="getClass" />
              </node>
            </node>
            <node concept="2OqwBi" id="4B5aqq4dQrT" role="37wK5m">
              <node concept="Xjq3P" id="4B5aqq4dQj0" role="2Oq$k0" />
              <node concept="2OwXpG" id="4B5aqq4dQFv" role="2OqNvi">
                <ref role="2Oxat5" node="1mAGFBLsNvq" resolve="test" />
              </node>
            </node>
            <node concept="2OqwBi" id="4B5aqq4dPS4" role="37wK5m">
              <node concept="Xjq3P" id="4B5aqq4dPKe" role="2Oq$k0" />
              <node concept="2OwXpG" id="4B5aqq4dPY4" role="2OqNvi">
                <ref role="2Oxat5" node="4B5aqq4dMQY" resolve="specimenType" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="7lYCqhv3U1r" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="7lYCqhv3UxP" role="jymVt" />
    <node concept="3clFb_" id="7lYCqhv3U1u" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="equals" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3Tm1VV" id="7lYCqhv3U1v" role="1B3o_S" />
      <node concept="10P_77" id="7lYCqhv3U1x" role="3clF45" />
      <node concept="37vLTG" id="7lYCqhv3U1y" role="3clF46">
        <property role="TrG5h" value="object" />
        <node concept="3uibUv" id="7lYCqhv3U1z" role="1tU5fm">
          <ref role="3uigEE" to="wyt6:~Object" resolve="Object" />
        </node>
      </node>
      <node concept="3clFbS" id="7lYCqhv3U1A" role="3clF47">
        <node concept="3clFbJ" id="4B5aqq5OKCE" role="3cqZAp">
          <node concept="3clFbS" id="4B5aqq5OKCF" role="3clFbx">
            <node concept="3cpWs6" id="4B5aqq5OKCG" role="3cqZAp">
              <node concept="3clFbT" id="4B5aqq5OKCH" role="3cqZAk">
                <property role="3clFbU" value="true" />
              </node>
            </node>
          </node>
          <node concept="3clFbC" id="4B5aqq5OKCI" role="3clFbw">
            <node concept="Xjq3P" id="4B5aqq5OKCJ" role="3uHU7B" />
            <node concept="37vLTw" id="4B5aqq5OKCK" role="3uHU7w">
              <ref role="3cqZAo" node="7lYCqhv3U1y" resolve="object" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="4B5aqq5OKCL" role="3cqZAp">
          <node concept="3clFbS" id="4B5aqq5OKCM" role="3clFbx">
            <node concept="3cpWs6" id="4B5aqq5OKCN" role="3cqZAp">
              <node concept="3clFbT" id="4B5aqq5OKCO" role="3cqZAk">
                <property role="3clFbU" value="false" />
              </node>
            </node>
          </node>
          <node concept="22lmx$" id="4B5aqq5OKCP" role="3clFbw">
            <node concept="3clFbC" id="4B5aqq5OKCQ" role="3uHU7B">
              <node concept="10Nm6u" id="4B5aqq5OKCR" role="3uHU7w" />
              <node concept="37vLTw" id="4B5aqq5OKCS" role="3uHU7B">
                <ref role="3cqZAo" node="7lYCqhv3U1y" resolve="object" />
              </node>
            </node>
            <node concept="3y3z36" id="4B5aqq5OKCT" role="3uHU7w">
              <node concept="2OqwBi" id="4B5aqq5OKCU" role="3uHU7B">
                <node concept="Xjq3P" id="4B5aqq5OKCV" role="2Oq$k0" />
                <node concept="liA8E" id="4B5aqq5OKCW" role="2OqNvi">
                  <ref role="37wK5l" to="wyt6:~Object.getClass():java.lang.Class" resolve="getClass" />
                </node>
              </node>
              <node concept="2OqwBi" id="4B5aqq5OKCX" role="3uHU7w">
                <node concept="37vLTw" id="4B5aqq5OKCY" role="2Oq$k0">
                  <ref role="3cqZAo" node="7lYCqhv3U1y" resolve="object" />
                </node>
                <node concept="liA8E" id="4B5aqq5OKCZ" role="2OqNvi">
                  <ref role="37wK5l" to="wyt6:~Object.getClass():java.lang.Class" resolve="getClass" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="7lYCqhv3Ubu" role="3cqZAp">
          <node concept="3cpWsn" id="7lYCqhv3Ubv" role="3cpWs9">
            <property role="TrG5h" value="other" />
            <node concept="3uibUv" id="7lYCqhv4vNH" role="1tU5fm">
              <ref role="3uigEE" node="1mAGFBLsNvp" resolve="TestPreviousResultAsText" />
            </node>
            <node concept="10QFUN" id="7lYCqhv3Ubx" role="33vP2m">
              <node concept="3uibUv" id="7lYCqhv4vBl" role="10QFUM">
                <ref role="3uigEE" node="1mAGFBLsNvp" resolve="TestPreviousResultAsText" />
              </node>
              <node concept="37vLTw" id="7lYCqhv3Ubz" role="10QFUP">
                <ref role="3cqZAo" node="7lYCqhv3U1y" resolve="object" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="7lYCqhv3Ub$" role="3cqZAp">
          <node concept="1Wc70l" id="4B5aqq5CtfT" role="3cqZAk">
            <node concept="2YIFZM" id="4B5aqq5CtDR" role="3uHU7w">
              <ref role="37wK5l" to="33ny:~Objects.equals(java.lang.Object,java.lang.Object):boolean" resolve="equals" />
              <ref role="1Pybhc" to="33ny:~Objects" resolve="Objects" />
              <node concept="2OqwBi" id="4B5aqq5CtZt" role="37wK5m">
                <node concept="Xjq3P" id="4B5aqq5CtQx" role="2Oq$k0" />
                <node concept="2OwXpG" id="4B5aqq5CufZ" role="2OqNvi">
                  <ref role="2Oxat5" node="4B5aqq4dMQY" resolve="specimenType" />
                </node>
              </node>
              <node concept="2OqwBi" id="4B5aqq5CuAm" role="37wK5m">
                <node concept="37vLTw" id="4B5aqq5Cut9" role="2Oq$k0">
                  <ref role="3cqZAo" node="7lYCqhv3Ubv" resolve="other" />
                </node>
                <node concept="2OwXpG" id="4B5aqq5CuR9" role="2OqNvi">
                  <ref role="2Oxat5" node="4B5aqq4dMQY" resolve="specimenType" />
                </node>
              </node>
            </node>
            <node concept="2YIFZM" id="4B5aqq5CrKt" role="3uHU7B">
              <ref role="37wK5l" to="33ny:~Objects.equals(java.lang.Object,java.lang.Object):boolean" resolve="equals" />
              <ref role="1Pybhc" to="33ny:~Objects" resolve="Objects" />
              <node concept="2OqwBi" id="4B5aqq5Cs7l" role="37wK5m">
                <node concept="Xjq3P" id="4B5aqq5CrWu" role="2Oq$k0" />
                <node concept="2OwXpG" id="4B5aqq5Csne" role="2OqNvi">
                  <ref role="2Oxat5" node="1mAGFBLsNvq" resolve="test" />
                </node>
              </node>
              <node concept="2OqwBi" id="4B5aqq5CsGj" role="37wK5m">
                <node concept="37vLTw" id="4B5aqq5CszJ" role="2Oq$k0">
                  <ref role="3cqZAo" node="7lYCqhv3Ubv" resolve="other" />
                </node>
                <node concept="2OwXpG" id="4B5aqq6h09k" role="2OqNvi">
                  <ref role="2Oxat5" node="1mAGFBLsNvq" resolve="test" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="7lYCqhv3U1B" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3Tm1VV" id="1mAGFBLsNvO" role="1B3o_S" />
    <node concept="3uibUv" id="5jVYnMGN2Z8" role="1zkMxy">
      <ref role="3uigEE" to="8r9s:1mAGFBLlvAI" resolve="TextDataValue" />
    </node>
  </node>
  <node concept="312cEu" id="1mAGFBLlG$Q">
    <property role="3GE5qa" value="data.value" />
    <property role="TrG5h" value="TestResultAsText" />
    <node concept="312cEg" id="1mAGFBLnVaQ" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="test" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm6S6" id="1mAGFBLnV90" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBLC7DD" role="1tU5fm" />
    </node>
    <node concept="312cEg" id="4B5aqq4dU0F" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="specimenType" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm6S6" id="4B5aqq4dTIi" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4dTUG" role="1tU5fm" />
    </node>
    <node concept="2tJIrI" id="1mAGFBLnVb3" role="jymVt" />
    <node concept="3clFbW" id="1mAGFBLnVbo" role="jymVt">
      <node concept="3cqZAl" id="1mAGFBLnVbq" role="3clF45" />
      <node concept="3Tm1VV" id="1mAGFBLnVbr" role="1B3o_S" />
      <node concept="3clFbS" id="1mAGFBLnVbs" role="3clF47">
        <node concept="3clFbF" id="1mAGFBLnVcq" role="3cqZAp">
          <node concept="37vLTI" id="1mAGFBLnVpW" role="3clFbG">
            <node concept="37vLTw" id="1mAGFBLnVrd" role="37vLTx">
              <ref role="3cqZAo" node="1mAGFBLnVbM" resolve="test" />
            </node>
            <node concept="2OqwBi" id="1mAGFBLnVfN" role="37vLTJ">
              <node concept="Xjq3P" id="1mAGFBLnVcp" role="2Oq$k0" />
              <node concept="2OwXpG" id="1mAGFBLnVjU" role="2OqNvi">
                <ref role="2Oxat5" node="1mAGFBLnVaQ" resolve="test" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4B5aqq4dUmF" role="3cqZAp">
          <node concept="37vLTI" id="4B5aqq4dUIN" role="3clFbG">
            <node concept="37vLTw" id="4B5aqq4dUUF" role="37vLTx">
              <ref role="3cqZAo" node="4B5aqq4dUd7" resolve="specimenType" />
            </node>
            <node concept="2OqwBi" id="4B5aqq4dUr8" role="37vLTJ">
              <node concept="Xjq3P" id="4B5aqq4dUmD" role="2Oq$k0" />
              <node concept="2OwXpG" id="4B5aqq4dUvv" role="2OqNvi">
                <ref role="2Oxat5" node="4B5aqq4dU0F" resolve="specimenType" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="1mAGFBLnVbM" role="3clF46">
        <property role="TrG5h" value="test" />
        <node concept="17QB3L" id="1mAGFBLC7zF" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="4B5aqq4dUd7" role="3clF46">
        <property role="TrG5h" value="specimenType" />
        <node concept="17QB3L" id="4B5aqq4dUkA" role="1tU5fm" />
      </node>
    </node>
    <node concept="2tJIrI" id="1mAGFBLnVsw" role="jymVt" />
    <node concept="3clFb_" id="1mAGFBLEHbp" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestName" />
      <node concept="3Tm1VV" id="1mAGFBLEHbr" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBLEHbs" role="3clF45" />
      <node concept="3clFbS" id="1mAGFBLEHbu" role="3clF47">
        <node concept="3cpWs6" id="1mAGFBLsM2F" role="3cqZAp">
          <node concept="2OqwBi" id="1mAGFBLsM9K" role="3cqZAk">
            <node concept="Xjq3P" id="1mAGFBLsM3u" role="2Oq$k0" />
            <node concept="2OwXpG" id="1mAGFBLsMia" role="2OqNvi">
              <ref role="2Oxat5" node="1mAGFBLnVaQ" resolve="test" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="4B5aqq4dV7t" role="jymVt" />
    <node concept="3clFb_" id="4B5aqq4dVNX" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="getSpecimenType" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="4B5aqq4dVO0" role="3clF47">
        <node concept="3cpWs6" id="4B5aqq4dW1o" role="3cqZAp">
          <node concept="2OqwBi" id="4B5aqq4dW8v" role="3cqZAk">
            <node concept="Xjq3P" id="4B5aqq4dW1B" role="2Oq$k0" />
            <node concept="2OwXpG" id="4B5aqq4dWoh" role="2OqNvi">
              <ref role="2Oxat5" node="4B5aqq4dU0F" resolve="specimenType" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="4B5aqq4dVx6" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4dVHY" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="5jVYnMGNqQv" role="jymVt" />
    <node concept="3clFb_" id="5jVYnMGNqQw" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="accept" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3Tm1VV" id="5jVYnMGNqQx" role="1B3o_S" />
      <node concept="37vLTG" id="5jVYnMGNqQz" role="3clF46">
        <property role="TrG5h" value="visitor" />
        <node concept="3uibUv" id="5jVYnMGNqQ$" role="1tU5fm">
          <ref role="3uigEE" to="8r9s:5jVYnMGKCt5" resolve="DataValueVisitor" />
        </node>
      </node>
      <node concept="3clFbS" id="5jVYnMGNqQ_" role="3clF47">
        <node concept="3clFbJ" id="5jVYnMGNqQA" role="3cqZAp">
          <node concept="2ZW3vV" id="5jVYnMGNqQB" role="3clFbw">
            <node concept="3uibUv" id="5jVYnMGNqQC" role="2ZW6by">
              <ref role="3uigEE" node="5jVYnMGJD4D" resolve="LaboratoryTestWithTextResultDataValueVisitor" />
            </node>
            <node concept="37vLTw" id="5jVYnMGNqQD" role="2ZW6bz">
              <ref role="3cqZAo" node="5jVYnMGNqQz" resolve="visitor" />
            </node>
          </node>
          <node concept="3clFbS" id="5jVYnMGNqQE" role="3clFbx">
            <node concept="3cpWs6" id="5jVYnMGNqQF" role="3cqZAp">
              <node concept="2OqwBi" id="5jVYnMGNqQG" role="3cqZAk">
                <node concept="1eOMI4" id="5jVYnMGNqQH" role="2Oq$k0">
                  <node concept="10QFUN" id="5jVYnMGNqQI" role="1eOMHV">
                    <node concept="3uibUv" id="5jVYnMGNqQJ" role="10QFUM">
                      <ref role="3uigEE" node="5jVYnMGJD4D" resolve="LaboratoryTestWithTextResultDataValueVisitor" />
                    </node>
                    <node concept="37vLTw" id="5jVYnMGNqQK" role="10QFUP">
                      <ref role="3cqZAo" node="5jVYnMGNqQz" resolve="visitor" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="5jVYnMGNqQL" role="2OqNvi">
                  <ref role="37wK5l" node="5jVYnMGJDb7" resolve="visit" />
                  <node concept="Xjq3P" id="5jVYnMGNqQM" role="37wK5m" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="5jVYnMGNqQN" role="3cqZAp">
          <node concept="10Nm6u" id="5jVYnMGNqQO" role="3cqZAk" />
        </node>
      </node>
      <node concept="2AHcQZ" id="5jVYnMGNqQP" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
      <node concept="3uibUv" id="5jVYnMGVeYd" role="3clF45">
        <ref role="3uigEE" to="8r9s:2XLt5KVsHET" resolve="LisResponse" />
        <node concept="17QB3L" id="5jVYnMGVfyP" role="11_B2D" />
      </node>
    </node>
    <node concept="2tJIrI" id="7lYCqhv4wAd" role="jymVt" />
    <node concept="3clFb_" id="7lYCqhv4wkD" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="hashCode" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3Tm1VV" id="7lYCqhv4wkE" role="1B3o_S" />
      <node concept="10Oyi0" id="7lYCqhv4wkF" role="3clF45" />
      <node concept="3clFbS" id="7lYCqhv4wkG" role="3clF47">
        <node concept="3clFbF" id="7lYCqhv4wkH" role="3cqZAp">
          <node concept="2YIFZM" id="7lYCqhv4wkI" role="3clFbG">
            <ref role="37wK5l" to="33ny:~Objects.hash(java.lang.Object...):int" resolve="hash" />
            <ref role="1Pybhc" to="33ny:~Objects" resolve="Objects" />
            <node concept="2OqwBi" id="7lYCqhv4Ddb" role="37wK5m">
              <node concept="Xjq3P" id="7lYCqhv4wkJ" role="2Oq$k0" />
              <node concept="liA8E" id="7lYCqhv4Ds5" role="2OqNvi">
                <ref role="37wK5l" to="wyt6:~Object.getClass():java.lang.Class" resolve="getClass" />
              </node>
            </node>
            <node concept="2OqwBi" id="4B5aqq4dWTN" role="37wK5m">
              <node concept="Xjq3P" id="4B5aqq4dWLX" role="2Oq$k0" />
              <node concept="2OwXpG" id="4B5aqq4dWZO" role="2OqNvi">
                <ref role="2Oxat5" node="1mAGFBLnVaQ" resolve="test" />
              </node>
            </node>
            <node concept="2OqwBi" id="4B5aqq4dXqV" role="37wK5m">
              <node concept="Xjq3P" id="4B5aqq4dXi2" role="2Oq$k0" />
              <node concept="2OwXpG" id="4B5aqq4dXxY" role="2OqNvi">
                <ref role="2Oxat5" node="4B5aqq4dU0F" resolve="specimenType" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="7lYCqhv4wkL" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="7lYCqhv4wkM" role="jymVt" />
    <node concept="3clFb_" id="7lYCqhv4wkN" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="equals" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3Tm1VV" id="7lYCqhv4wkO" role="1B3o_S" />
      <node concept="10P_77" id="7lYCqhv4wkP" role="3clF45" />
      <node concept="37vLTG" id="7lYCqhv4wkQ" role="3clF46">
        <property role="TrG5h" value="object" />
        <node concept="3uibUv" id="7lYCqhv4wkR" role="1tU5fm">
          <ref role="3uigEE" to="wyt6:~Object" resolve="Object" />
        </node>
      </node>
      <node concept="3clFbS" id="7lYCqhv4wkS" role="3clF47">
        <node concept="3clFbJ" id="4B5aqq5OJ30" role="3cqZAp">
          <node concept="3clFbS" id="4B5aqq5OJ31" role="3clFbx">
            <node concept="3cpWs6" id="4B5aqq5OJ32" role="3cqZAp">
              <node concept="3clFbT" id="4B5aqq5OJ33" role="3cqZAk">
                <property role="3clFbU" value="true" />
              </node>
            </node>
          </node>
          <node concept="3clFbC" id="4B5aqq5OJ34" role="3clFbw">
            <node concept="Xjq3P" id="4B5aqq5OJ35" role="3uHU7B" />
            <node concept="37vLTw" id="4B5aqq5OJ36" role="3uHU7w">
              <ref role="3cqZAo" node="7lYCqhv4wkQ" resolve="object" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="4B5aqq5OJ37" role="3cqZAp">
          <node concept="3clFbS" id="4B5aqq5OJ38" role="3clFbx">
            <node concept="3cpWs6" id="4B5aqq5OJ39" role="3cqZAp">
              <node concept="3clFbT" id="4B5aqq5OJ3a" role="3cqZAk">
                <property role="3clFbU" value="false" />
              </node>
            </node>
          </node>
          <node concept="22lmx$" id="4B5aqq5OJW5" role="3clFbw">
            <node concept="3clFbC" id="4B5aqq5OKlz" role="3uHU7B">
              <node concept="10Nm6u" id="4B5aqq5OKtL" role="3uHU7w" />
              <node concept="37vLTw" id="4B5aqq5OKbr" role="3uHU7B">
                <ref role="3cqZAo" node="7lYCqhv4wkQ" resolve="object" />
              </node>
            </node>
            <node concept="3y3z36" id="4B5aqq5OJ3b" role="3uHU7w">
              <node concept="2OqwBi" id="4B5aqq5OJ3c" role="3uHU7B">
                <node concept="Xjq3P" id="4B5aqq5OJ3d" role="2Oq$k0" />
                <node concept="liA8E" id="4B5aqq5OJ3e" role="2OqNvi">
                  <ref role="37wK5l" to="wyt6:~Object.getClass():java.lang.Class" resolve="getClass" />
                </node>
              </node>
              <node concept="2OqwBi" id="4B5aqq5OJ3f" role="3uHU7w">
                <node concept="37vLTw" id="4B5aqq5OJ3g" role="2Oq$k0">
                  <ref role="3cqZAo" node="7lYCqhv4wkQ" resolve="object" />
                </node>
                <node concept="liA8E" id="4B5aqq5OJ3h" role="2OqNvi">
                  <ref role="37wK5l" to="wyt6:~Object.getClass():java.lang.Class" resolve="getClass" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="7lYCqhv4wl0" role="3cqZAp">
          <node concept="3cpWsn" id="7lYCqhv4wl1" role="3cpWs9">
            <property role="TrG5h" value="other" />
            <node concept="3uibUv" id="7lYCqhv4x6k" role="1tU5fm">
              <ref role="3uigEE" node="1mAGFBLlG$Q" resolve="TestResultAsText" />
            </node>
            <node concept="10QFUN" id="7lYCqhv4wl2" role="33vP2m">
              <node concept="3uibUv" id="7lYCqhv4wTW" role="10QFUM">
                <ref role="3uigEE" node="1mAGFBLlG$Q" resolve="TestResultAsText" />
              </node>
              <node concept="37vLTw" id="7lYCqhv4wl3" role="10QFUP">
                <ref role="3cqZAo" node="7lYCqhv4wkQ" resolve="object" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="7lYCqhv4wl4" role="3cqZAp">
          <node concept="1Wc70l" id="4B5aqq5CvUq" role="3cqZAk">
            <node concept="2YIFZM" id="4B5aqq5CvUr" role="3uHU7w">
              <ref role="1Pybhc" to="33ny:~Objects" resolve="Objects" />
              <ref role="37wK5l" to="33ny:~Objects.equals(java.lang.Object,java.lang.Object):boolean" resolve="equals" />
              <node concept="2OqwBi" id="4B5aqq5CvUs" role="37wK5m">
                <node concept="Xjq3P" id="4B5aqq5CvUt" role="2Oq$k0" />
                <node concept="2OwXpG" id="4B5aqq5CvUu" role="2OqNvi">
                  <ref role="2Oxat5" node="4B5aqq4dU0F" resolve="specimenType" />
                </node>
              </node>
              <node concept="2OqwBi" id="4B5aqq5CvUv" role="37wK5m">
                <node concept="37vLTw" id="4B5aqq5CvUw" role="2Oq$k0">
                  <ref role="3cqZAo" node="7lYCqhv4wl1" resolve="other" />
                </node>
                <node concept="2OwXpG" id="4B5aqq5CvUx" role="2OqNvi">
                  <ref role="2Oxat5" node="4B5aqq4dU0F" resolve="specimenType" />
                </node>
              </node>
            </node>
            <node concept="2YIFZM" id="4B5aqq5CvUy" role="3uHU7B">
              <ref role="37wK5l" to="33ny:~Objects.equals(java.lang.Object,java.lang.Object):boolean" resolve="equals" />
              <ref role="1Pybhc" to="33ny:~Objects" resolve="Objects" />
              <node concept="2OqwBi" id="4B5aqq5CvUz" role="37wK5m">
                <node concept="Xjq3P" id="4B5aqq5CvU$" role="2Oq$k0" />
                <node concept="2OwXpG" id="4B5aqq5CvU_" role="2OqNvi">
                  <ref role="2Oxat5" node="1mAGFBLnVaQ" resolve="test" />
                </node>
              </node>
              <node concept="2OqwBi" id="4B5aqq5CvUA" role="37wK5m">
                <node concept="37vLTw" id="4B5aqq5CvUB" role="2Oq$k0">
                  <ref role="3cqZAo" node="7lYCqhv4wl1" resolve="other" />
                </node>
                <node concept="2OwXpG" id="4B5aqq6gZAs" role="2OqNvi">
                  <ref role="2Oxat5" node="1mAGFBLnVaQ" resolve="test" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="7lYCqhv4wlc" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3Tm1VV" id="1mAGFBLlG$R" role="1B3o_S" />
    <node concept="3uibUv" id="5jVYnMGNtBd" role="1zkMxy">
      <ref role="3uigEE" to="8r9s:1mAGFBLlvAI" resolve="TextDataValue" />
    </node>
  </node>
  <node concept="312cEu" id="PDjyzkyGYy">
    <property role="3GE5qa" value="data.boolean" />
    <property role="TrG5h" value="TestResultHasTextValue" />
    <node concept="312cEg" id="PDjyzkyH0G" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="test" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm6S6" id="PDjyzkyH0r" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkyH0_" role="1tU5fm" />
    </node>
    <node concept="312cEg" id="PDjyzkyHE6" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="specimenType" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm6S6" id="PDjyzkyHBU" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkyHE1" role="1tU5fm" />
    </node>
    <node concept="2tJIrI" id="PDjyzkyH13" role="jymVt" />
    <node concept="3clFbW" id="PDjyzkyH1o" role="jymVt">
      <node concept="3cqZAl" id="PDjyzkyH1q" role="3clF45" />
      <node concept="3Tm1VV" id="PDjyzkyH1r" role="1B3o_S" />
      <node concept="3clFbS" id="PDjyzkyH1s" role="3clF47">
        <node concept="3clFbF" id="PDjyzkyH2o" role="3cqZAp">
          <node concept="37vLTI" id="PDjyzkyHhl" role="3clFbG">
            <node concept="37vLTw" id="PDjyzkyHj0" role="37vLTx">
              <ref role="3cqZAo" node="PDjyzkyH1M" resolve="test" />
            </node>
            <node concept="2OqwBi" id="PDjyzkyH58" role="37vLTJ">
              <node concept="Xjq3P" id="PDjyzkyH2n" role="2Oq$k0" />
              <node concept="2OwXpG" id="PDjyzkyH8I" role="2OqNvi">
                <ref role="2Oxat5" node="PDjyzkyH0G" resolve="test" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="PDjyzkyHJB" role="3cqZAp">
          <node concept="37vLTI" id="PDjyzkyI2Z" role="3clFbG">
            <node concept="37vLTw" id="PDjyzkyI8X" role="37vLTx">
              <ref role="3cqZAo" node="PDjyzkyHGf" resolve="specimenType" />
            </node>
            <node concept="2OqwBi" id="PDjyzkyHNr" role="37vLTJ">
              <node concept="Xjq3P" id="PDjyzkyHJ_" role="2Oq$k0" />
              <node concept="2OwXpG" id="PDjyzkyHUe" role="2OqNvi">
                <ref role="2Oxat5" node="PDjyzkyHE6" resolve="specimenType" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="PDjyzkyH1M" role="3clF46">
        <property role="TrG5h" value="test" />
        <node concept="17QB3L" id="PDjyzkyH1L" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="PDjyzkyHGf" role="3clF46">
        <property role="TrG5h" value="specimenType" />
        <node concept="17QB3L" id="PDjyzkyHHA" role="1tU5fm" />
      </node>
    </node>
    <node concept="2tJIrI" id="PDjyzkyHkf" role="jymVt" />
    <node concept="3clFb_" id="PDjyzkyHnH" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="getTest" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="PDjyzkyHnK" role="3clF47">
        <node concept="3cpWs6" id="PDjyzkyHpC" role="3cqZAp">
          <node concept="2OqwBi" id="PDjyzkyHvt" role="3cqZAk">
            <node concept="Xjq3P" id="PDjyzkyHpR" role="2Oq$k0" />
            <node concept="2OwXpG" id="PDjyzkyH$j" role="2OqNvi">
              <ref role="2Oxat5" node="PDjyzkyH0G" resolve="test" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="PDjyzkyHm3" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkyHnC" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="PDjyzkyIac" role="jymVt" />
    <node concept="3clFb_" id="PDjyzkyIfX" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="getSpecimenType" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="PDjyzkyIg0" role="3clF47">
        <node concept="3cpWs6" id="PDjyzkyIiS" role="3cqZAp">
          <node concept="2OqwBi" id="PDjyzkyIoH" role="3cqZAk">
            <node concept="Xjq3P" id="PDjyzkyIj7" role="2Oq$k0" />
            <node concept="2OwXpG" id="PDjyzkyIuf" role="2OqNvi">
              <ref role="2Oxat5" node="PDjyzkyHE6" resolve="specimenType" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="PDjyzkyIdl" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzkyIdZ" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="5jVYnMGMEyb" role="jymVt" />
    <node concept="3clFb_" id="5jVYnMGMAEm" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="accept" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3Tm1VV" id="5jVYnMGMAEn" role="1B3o_S" />
      <node concept="37vLTG" id="5jVYnMGMAEp" role="3clF46">
        <property role="TrG5h" value="visitor" />
        <node concept="3uibUv" id="5jVYnMGMAEq" role="1tU5fm">
          <ref role="3uigEE" to="8r9s:5jVYnMGKCt5" resolve="DataValueVisitor" />
        </node>
      </node>
      <node concept="3clFbS" id="5jVYnMGMAEr" role="3clF47">
        <node concept="3clFbJ" id="5jVYnMGMAEs" role="3cqZAp">
          <node concept="2ZW3vV" id="5jVYnMGMAEt" role="3clFbw">
            <node concept="3uibUv" id="5jVYnMGMET_" role="2ZW6by">
              <ref role="3uigEE" node="5jVYnMGJD4D" resolve="LaboratoryTestWithTextResultDataValueVisitor" />
            </node>
            <node concept="37vLTw" id="5jVYnMGMAEv" role="2ZW6bz">
              <ref role="3cqZAo" node="5jVYnMGMAEp" resolve="visitor" />
            </node>
          </node>
          <node concept="3clFbS" id="5jVYnMGMAEw" role="3clFbx">
            <node concept="3cpWs6" id="5jVYnMGMAEx" role="3cqZAp">
              <node concept="2OqwBi" id="5jVYnMGMAEy" role="3cqZAk">
                <node concept="1eOMI4" id="5jVYnMGMAEz" role="2Oq$k0">
                  <node concept="10QFUN" id="5jVYnMGMAE$" role="1eOMHV">
                    <node concept="3uibUv" id="5jVYnMGMGeS" role="10QFUM">
                      <ref role="3uigEE" node="5jVYnMGJD4D" resolve="LaboratoryTestWithTextResultDataValueVisitor" />
                    </node>
                    <node concept="37vLTw" id="5jVYnMGMAEA" role="10QFUP">
                      <ref role="3cqZAo" node="5jVYnMGMAEp" resolve="visitor" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="5jVYnMGMAEB" role="2OqNvi">
                  <ref role="37wK5l" node="5jVYnMGJD5t" resolve="visit" />
                  <node concept="Xjq3P" id="5jVYnMGMAEC" role="37wK5m" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="5jVYnMGMAED" role="3cqZAp">
          <node concept="10Nm6u" id="5jVYnMGMAEE" role="3cqZAk" />
        </node>
      </node>
      <node concept="2AHcQZ" id="5jVYnMGMAEF" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
      <node concept="3uibUv" id="5jVYnMGUYaN" role="3clF45">
        <ref role="3uigEE" to="8r9s:2XLt5KVsHET" resolve="LisResponse" />
        <node concept="3uibUv" id="5jVYnMGUYpP" role="11_B2D">
          <ref role="3uigEE" to="wyt6:~Boolean" resolve="Boolean" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="PDjyzkyIVw" role="jymVt" />
    <node concept="3clFb_" id="PDjyzkyILW" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="hashCode" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3Tm1VV" id="PDjyzkyILX" role="1B3o_S" />
      <node concept="10Oyi0" id="PDjyzkyILZ" role="3clF45" />
      <node concept="3clFbS" id="PDjyzkyIM1" role="3clF47">
        <node concept="3clFbF" id="PDjyzkyJ9y" role="3cqZAp">
          <node concept="2YIFZM" id="PDjyzkyJag" role="3clFbG">
            <ref role="37wK5l" to="33ny:~Objects.hash(java.lang.Object...):int" resolve="hash" />
            <ref role="1Pybhc" to="33ny:~Objects" resolve="Objects" />
            <node concept="2OqwBi" id="PDjyzkyJZB" role="37wK5m">
              <node concept="Xjq3P" id="PDjyzkyJTf" role="2Oq$k0" />
              <node concept="liA8E" id="PDjyzkyKdj" role="2OqNvi">
                <ref role="37wK5l" to="wyt6:~Object.getClass():java.lang.Class" resolve="getClass" />
              </node>
            </node>
            <node concept="2OqwBi" id="PDjyzkyJiM" role="37wK5m">
              <node concept="Xjq3P" id="PDjyzkyJaO" role="2Oq$k0" />
              <node concept="2OwXpG" id="PDjyzkyJmC" role="2OqNvi">
                <ref role="2Oxat5" node="PDjyzkyH0G" resolve="test" />
              </node>
            </node>
            <node concept="2OqwBi" id="PDjyzkyJFR" role="37wK5m">
              <node concept="Xjq3P" id="PDjyzkyJ_N" role="2Oq$k0" />
              <node concept="2OwXpG" id="PDjyzkyJKI" role="2OqNvi">
                <ref role="2Oxat5" node="PDjyzkyHE6" resolve="specimenType" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkyIM2" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzkyIM5" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="equals" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3Tm1VV" id="PDjyzkyIM6" role="1B3o_S" />
      <node concept="10P_77" id="PDjyzkyIM8" role="3clF45" />
      <node concept="37vLTG" id="PDjyzkyIM9" role="3clF46">
        <property role="TrG5h" value="object" />
        <node concept="3uibUv" id="PDjyzkyIMa" role="1tU5fm">
          <ref role="3uigEE" to="wyt6:~Object" resolve="Object" />
        </node>
      </node>
      <node concept="3clFbS" id="PDjyzkyIMc" role="3clF47">
        <node concept="3clFbJ" id="PDjyzkyKGp" role="3cqZAp">
          <node concept="3clFbS" id="PDjyzkyKGq" role="3clFbx">
            <node concept="3cpWs6" id="PDjyzkyKGr" role="3cqZAp">
              <node concept="3clFbT" id="PDjyzkyKGs" role="3cqZAk">
                <property role="3clFbU" value="true" />
              </node>
            </node>
          </node>
          <node concept="3clFbC" id="PDjyzkyKGt" role="3clFbw">
            <node concept="Xjq3P" id="PDjyzkyKGu" role="3uHU7B" />
            <node concept="37vLTw" id="PDjyzkyKGv" role="3uHU7w">
              <ref role="3cqZAo" node="PDjyzkyIM9" resolve="object" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="PDjyzkyKGw" role="3cqZAp">
          <node concept="3clFbS" id="PDjyzkyKGx" role="3clFbx">
            <node concept="3cpWs6" id="PDjyzkyKGy" role="3cqZAp">
              <node concept="3clFbT" id="PDjyzkyKGz" role="3cqZAk">
                <property role="3clFbU" value="false" />
              </node>
            </node>
          </node>
          <node concept="22lmx$" id="PDjyzkyKG$" role="3clFbw">
            <node concept="3clFbC" id="PDjyzkyKG_" role="3uHU7B">
              <node concept="10Nm6u" id="PDjyzkyKGA" role="3uHU7w" />
              <node concept="37vLTw" id="PDjyzkyKGB" role="3uHU7B">
                <ref role="3cqZAo" node="PDjyzkyIM9" resolve="object" />
              </node>
            </node>
            <node concept="3y3z36" id="PDjyzkyKGC" role="3uHU7w">
              <node concept="2OqwBi" id="PDjyzkyKGD" role="3uHU7B">
                <node concept="Xjq3P" id="PDjyzkyKGE" role="2Oq$k0" />
                <node concept="liA8E" id="PDjyzkyKGF" role="2OqNvi">
                  <ref role="37wK5l" to="wyt6:~Object.getClass():java.lang.Class" resolve="getClass" />
                </node>
              </node>
              <node concept="2OqwBi" id="PDjyzkyKGG" role="3uHU7w">
                <node concept="37vLTw" id="PDjyzkyKGH" role="2Oq$k0">
                  <ref role="3cqZAo" node="PDjyzkyIM9" resolve="object" />
                </node>
                <node concept="liA8E" id="PDjyzkyKGI" role="2OqNvi">
                  <ref role="37wK5l" to="wyt6:~Object.getClass():java.lang.Class" resolve="getClass" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="PDjyzkyKGJ" role="3cqZAp">
          <node concept="3cpWsn" id="PDjyzkyKGK" role="3cpWs9">
            <property role="TrG5h" value="other" />
            <node concept="3uibUv" id="PDjyzkyL6B" role="1tU5fm">
              <ref role="3uigEE" node="PDjyzkyGYy" resolve="TestResultHasTextValue" />
            </node>
            <node concept="10QFUN" id="PDjyzkyKGM" role="33vP2m">
              <node concept="3uibUv" id="PDjyzkyKTK" role="10QFUM">
                <ref role="3uigEE" node="PDjyzkyGYy" resolve="TestResultHasTextValue" />
              </node>
              <node concept="37vLTw" id="PDjyzkyKGO" role="10QFUP">
                <ref role="3cqZAo" node="PDjyzkyIM9" resolve="object" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="PDjyzkyKGP" role="3cqZAp">
          <node concept="1Wc70l" id="PDjyzkyKGQ" role="3cqZAk">
            <node concept="2YIFZM" id="PDjyzkyKGR" role="3uHU7w">
              <ref role="1Pybhc" to="33ny:~Objects" resolve="Objects" />
              <ref role="37wK5l" to="33ny:~Objects.equals(java.lang.Object,java.lang.Object):boolean" resolve="equals" />
              <node concept="2OqwBi" id="PDjyzkyKGS" role="37wK5m">
                <node concept="Xjq3P" id="PDjyzkyKGT" role="2Oq$k0" />
                <node concept="2OwXpG" id="PDjyzkyKGU" role="2OqNvi">
                  <ref role="2Oxat5" node="PDjyzkyHE6" resolve="specimenType" />
                </node>
              </node>
              <node concept="2OqwBi" id="PDjyzkyKGV" role="37wK5m">
                <node concept="37vLTw" id="PDjyzkyKGW" role="2Oq$k0">
                  <ref role="3cqZAo" node="PDjyzkyKGK" resolve="other" />
                </node>
                <node concept="2OwXpG" id="PDjyzkyLAL" role="2OqNvi">
                  <ref role="2Oxat5" node="PDjyzkyHE6" resolve="specimenType" />
                </node>
              </node>
            </node>
            <node concept="2YIFZM" id="PDjyzkyKGY" role="3uHU7B">
              <ref role="37wK5l" to="33ny:~Objects.equals(java.lang.Object,java.lang.Object):boolean" resolve="equals" />
              <ref role="1Pybhc" to="33ny:~Objects" resolve="Objects" />
              <node concept="2OqwBi" id="PDjyzkyKGZ" role="37wK5m">
                <node concept="Xjq3P" id="PDjyzkyKH0" role="2Oq$k0" />
                <node concept="2OwXpG" id="PDjyzkyKH1" role="2OqNvi">
                  <ref role="2Oxat5" node="PDjyzkyH0G" resolve="test" />
                </node>
              </node>
              <node concept="2OqwBi" id="PDjyzkyKH2" role="37wK5m">
                <node concept="37vLTw" id="PDjyzkyKH3" role="2Oq$k0">
                  <ref role="3cqZAo" node="PDjyzkyKGK" resolve="other" />
                </node>
                <node concept="2OwXpG" id="PDjyzkyLm_" role="2OqNvi">
                  <ref role="2Oxat5" node="PDjyzkyH0G" resolve="test" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzkyIMd" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3Tm1VV" id="PDjyzkyGYz" role="1B3o_S" />
    <node concept="3uibUv" id="PDjyzkyH0e" role="1zkMxy">
      <ref role="3uigEE" to="8r9s:1mAGFBLk56n" resolve="BooleanDataValue" />
    </node>
  </node>
  <node concept="3HP615" id="5jVYnMGJD4D">
    <property role="3GE5qa" value="data" />
    <property role="TrG5h" value="LaboratoryTestWithTextResultDataValueVisitor" />
    <node concept="3clFb_" id="5jVYnMGJD5t" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="visit" />
      <node concept="3clFbS" id="5jVYnMGJD5w" role="3clF47" />
      <node concept="3Tm1VV" id="5jVYnMGJD5x" role="1B3o_S" />
      <node concept="37vLTG" id="5jVYnMGJD5V" role="3clF46">
        <property role="TrG5h" value="dataValue" />
        <node concept="3uibUv" id="5jVYnMGJD5U" role="1tU5fm">
          <ref role="3uigEE" node="PDjyzkyGYy" resolve="TestResultHasTextValue" />
        </node>
      </node>
      <node concept="3uibUv" id="5jVYnMGVcog" role="3clF45">
        <ref role="3uigEE" to="8r9s:2XLt5KVsHET" resolve="LisResponse" />
        <node concept="3uibUv" id="5jVYnMGVcoh" role="11_B2D">
          <ref role="3uigEE" to="wyt6:~Boolean" resolve="Boolean" />
        </node>
      </node>
    </node>
    <node concept="3clFb_" id="5jVYnMGJD7G" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="visit" />
      <node concept="3clFbS" id="5jVYnMGJD7J" role="3clF47" />
      <node concept="3Tm1VV" id="5jVYnMGJD7K" role="1B3o_S" />
      <node concept="37vLTG" id="5jVYnMGJD8s" role="3clF46">
        <property role="TrG5h" value="dataValue" />
        <node concept="3uibUv" id="5jVYnMGJD8r" role="1tU5fm">
          <ref role="3uigEE" node="1mAGFBLsNvp" resolve="TestPreviousResultAsText" />
        </node>
      </node>
      <node concept="3uibUv" id="5jVYnMGVcqB" role="3clF45">
        <ref role="3uigEE" to="8r9s:2XLt5KVsHET" resolve="LisResponse" />
        <node concept="17QB3L" id="5jVYnMGVct6" role="11_B2D" />
      </node>
    </node>
    <node concept="3clFb_" id="5jVYnMGJDb7" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="visit" />
      <node concept="3clFbS" id="5jVYnMGJDb8" role="3clF47" />
      <node concept="3Tm1VV" id="5jVYnMGJDb9" role="1B3o_S" />
      <node concept="37vLTG" id="5jVYnMGJDbb" role="3clF46">
        <property role="TrG5h" value="dataValue" />
        <node concept="3uibUv" id="5jVYnMGJDdd" role="1tU5fm">
          <ref role="3uigEE" node="1mAGFBLlG$Q" resolve="TestResultAsText" />
        </node>
      </node>
      <node concept="3uibUv" id="5jVYnMGVcv$" role="3clF45">
        <ref role="3uigEE" to="8r9s:2XLt5KVsHET" resolve="LisResponse" />
        <node concept="17QB3L" id="5jVYnMGVcyw" role="11_B2D" />
      </node>
    </node>
    <node concept="3Tm1VV" id="5jVYnMGJD4E" role="1B3o_S" />
    <node concept="3uibUv" id="5jVYnMGMCG$" role="3HQHJm">
      <ref role="3uigEE" to="8r9s:5jVYnMGKCt5" resolve="DataValueVisitor" />
    </node>
  </node>
</model>

