package no.uio.mLab.decisions.data.laboratoryTest.textualResult.behavior;

/*Generated by MPS */

import jetbrains.mps.core.aspects.behaviour.BaseBehaviorAspectDescriptor;
import jetbrains.mps.core.aspects.behaviour.api.BHDescriptor;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.mps.openapi.language.SAbstractConcept;
import jetbrains.mps.lang.smodel.ConceptSwitchIndex;
import jetbrains.mps.lang.smodel.ConceptSwitchIndexBuilder;
import jetbrains.mps.smodel.adapter.ids.MetaIdFactory;

public final class BehaviorAspectDescriptor extends BaseBehaviorAspectDescriptor {
  private final BHDescriptor myLaboratoryTestHasResultAsText__BehaviorDescriptor = new LaboratoryTestHasResultAsText__BehaviorDescriptor();
  private final BHDescriptor myILaboratoryTestWithTextResultDataValue__BehaviorDescriptor = new ILaboratoryTestWithTextResultDataValue__BehaviorDescriptor();
  private final BHDescriptor myLaboratoryTestPreviousResultAsText__BehaviorDescriptor = new LaboratoryTestPreviousResultAsText__BehaviorDescriptor();
  private final BHDescriptor myLaboratoryTestResultAsText__BehaviorDescriptor = new LaboratoryTestResultAsText__BehaviorDescriptor();
  private final BHDescriptor myITranslatableTextResultLaboratoryTestDataConcept__BehaviorDescriptor = new ITranslatableTextResultLaboratoryTestDataConcept__BehaviorDescriptor();

  public BehaviorAspectDescriptor() {
  }

  @Nullable
  public BHDescriptor getDescriptor(@NotNull SAbstractConcept concept) {
    SAbstractConcept cncpt = concept;
    switch (conceptIndex.index(cncpt)) {
      case 0:
        return myILaboratoryTestWithTextResultDataValue__BehaviorDescriptor;
      case 1:
        return myITranslatableTextResultLaboratoryTestDataConcept__BehaviorDescriptor;
      case 2:
        return myLaboratoryTestHasResultAsText__BehaviorDescriptor;
      case 3:
        return myLaboratoryTestPreviousResultAsText__BehaviorDescriptor;
      case 4:
        return myLaboratoryTestResultAsText__BehaviorDescriptor;
      default:
    }
    return null;
  }
  private static final ConceptSwitchIndex conceptIndex = new ConceptSwitchIndexBuilder().put(MetaIdFactory.conceptId(0x60f87c040cd84174L, 0x8d1a4a254a9e2f58L, 0x49c529a683dab1c8L), MetaIdFactory.conceptId(0x60f87c040cd84174L, 0x8d1a4a254a9e2f58L, 0x4ec3b323fd815664L), MetaIdFactory.conceptId(0x60f87c040cd84174L, 0x8d1a4a254a9e2f58L, 0xd694e28d488687cL), MetaIdFactory.conceptId(0x60f87c040cd84174L, 0x8d1a4a254a9e2f58L, 0x49c529a68436a62fL), MetaIdFactory.conceptId(0x60f87c040cd84174L, 0x8d1a4a254a9e2f58L, 0x49c529a68436a633L)).seal();
}
