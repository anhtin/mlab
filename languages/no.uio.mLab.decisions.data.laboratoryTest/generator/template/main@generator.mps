<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:58ddf955-fa8b-4824-94fd-67516e063bea(main@generator)">
  <persistence version="9" />
  <languages>
    <devkit ref="a2eb3a43-fcc2-4200-80dc-c60110c4862d(jetbrains.mps.devkit.templates)" />
  </languages>
  <imports>
    <import index="aia3" ref="r:856e974b-6e42-49cc-99cd-c3adb4161c21(no.uio.mLab.decisions.data.laboratoryTest.structure)" />
    <import index="yhrd" ref="r:4677f140-6d3c-4aba-9fab-678c9a5f7c1f(no.uio.mLab.decisions.data.laboratoryTest.runtime)" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1070534058343" name="jetbrains.mps.baseLanguage.structure.NullLiteral" flags="nn" index="10Nm6u" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1212685548494" name="jetbrains.mps.baseLanguage.structure.ClassCreator" flags="nn" index="1pGfFk" />
    </language>
    <language id="b401a680-8325-4110-8fd3-84331ff25bef" name="jetbrains.mps.lang.generator">
      <concept id="1114706874351" name="jetbrains.mps.lang.generator.structure.CopySrcNodeMacro" flags="ln" index="29HgVG">
        <child id="1168024447342" name="sourceNodeQuery" index="3NFExx" />
      </concept>
      <concept id="1095416546421" name="jetbrains.mps.lang.generator.structure.MappingConfiguration" flags="ig" index="bUwia">
        <child id="1167328349397" name="reductionMappingRule" index="3acgRq" />
        <child id="1195502100749" name="preMappingScript" index="1puA0r" />
      </concept>
      <concept id="1177093525992" name="jetbrains.mps.lang.generator.structure.InlineTemplate_RuleConsequence" flags="lg" index="gft3U">
        <child id="1177093586806" name="templateNode" index="gfFT$" />
      </concept>
      <concept id="1167169188348" name="jetbrains.mps.lang.generator.structure.TemplateFunctionParameter_sourceNode" flags="nn" index="30H73N" />
      <concept id="1167169308231" name="jetbrains.mps.lang.generator.structure.BaseMappingRule" flags="ng" index="30H$t8">
        <reference id="1167169349424" name="applicableConcept" index="30HIoZ" />
      </concept>
      <concept id="1167327847730" name="jetbrains.mps.lang.generator.structure.Reduction_MappingRule" flags="lg" index="3aamgX">
        <child id="1169672767469" name="ruleConsequence" index="1lVwrX" />
      </concept>
      <concept id="1195499912406" name="jetbrains.mps.lang.generator.structure.MappingScript" flags="lg" index="1pmfR0">
        <property id="1195595592106" name="scriptKind" index="1v3f2W" />
        <child id="1195501105008" name="codeBlock" index="1pqMTA" />
      </concept>
      <concept id="1195500722856" name="jetbrains.mps.lang.generator.structure.MappingScript_CodeBlock" flags="in" index="1pplIY" />
      <concept id="1195502151594" name="jetbrains.mps.lang.generator.structure.MappingScriptReference" flags="lg" index="1puMqW">
        <reference id="1195502167610" name="mappingScript" index="1puQsG" />
      </concept>
      <concept id="1167945743726" name="jetbrains.mps.lang.generator.structure.IfMacro_Condition" flags="in" index="3IZrLx" />
      <concept id="1168024337012" name="jetbrains.mps.lang.generator.structure.SourceSubstituteMacro_SourceNodeQuery" flags="in" index="3NFfHV" />
      <concept id="1118773211870" name="jetbrains.mps.lang.generator.structure.IfMacro" flags="ln" index="1W57fq">
        <child id="1194989344771" name="alternativeConsequence" index="UU_$l" />
        <child id="1167945861827" name="conditionFunction" index="3IZSJc" />
      </concept>
    </language>
    <language id="760a0a8c-eabb-4521-8bfd-65db761a9ba3" name="jetbrains.mps.baseLanguage.logging">
      <concept id="6332851714983831325" name="jetbrains.mps.baseLanguage.logging.structure.MsgStatement" flags="ng" index="2xdQw9">
        <property id="6332851714983843871" name="severity" index="2xdLsb" />
        <child id="5721587534047265374" name="message" index="9lYJi" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1172008320231" name="jetbrains.mps.lang.smodel.structure.Node_IsNotNullOperation" flags="nn" index="3x8VRR" />
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="bUwia" id="4V3GMfXvz1u">
    <property role="TrG5h" value="dataLaboratoryTestMain" />
    <node concept="1puMqW" id="4V3GMfXSERe" role="1puA0r">
      <ref role="1puQsG" node="4V3GMfXSERl" resolve="dataLaboratoryTestScript" />
    </node>
    <node concept="3aamgX" id="4B5aqq4deqX" role="3acgRq">
      <ref role="30HIoZ" to="aia3:4B5aqq3QoID" resolve="LaboratoryTestHasPreviouslyBeenPerformed" />
      <node concept="gft3U" id="4B5aqq4desy" role="1lVwrX">
        <node concept="2ShNRf" id="4B5aqq4desG" role="gfFT$">
          <node concept="1pGfFk" id="4B5aqq4desH" role="2ShVmc">
            <ref role="37wK5l" to="yhrd:1mAGFBLsTz0" resolve="TestHasPreviouslyBeenPerformed" />
            <node concept="Xl_RD" id="4B5aqq4desI" role="37wK5m">
              <property role="Xl_RC" value="test" />
              <node concept="29HgVG" id="4B5aqq4desJ" role="lGtFl">
                <node concept="3NFfHV" id="4B5aqq4desK" role="3NFExx">
                  <node concept="3clFbS" id="4B5aqq4desL" role="2VODD2">
                    <node concept="3clFbF" id="4B5aqq4desM" role="3cqZAp">
                      <node concept="2OqwBi" id="4B5aqq4desN" role="3clFbG">
                        <node concept="3TrEf2" id="6LTgXmMozxo" role="2OqNvi">
                          <ref role="3Tt5mk" to="aia3:6LTgXmMj7Lm" resolve="testReference" />
                        </node>
                        <node concept="30H73N" id="4B5aqq4desP" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="Xl_RD" id="4B5aqq4desQ" role="37wK5m">
              <property role="Xl_RC" value="specimen" />
              <node concept="1W57fq" id="6LTgXmMoA_S" role="lGtFl">
                <node concept="3IZrLx" id="6LTgXmMoA_T" role="3IZSJc">
                  <node concept="3clFbS" id="6LTgXmMoA_U" role="2VODD2">
                    <node concept="3clFbF" id="6LTgXmMoAPo" role="3cqZAp">
                      <node concept="2OqwBi" id="6LTgXmMoCk3" role="3clFbG">
                        <node concept="2OqwBi" id="6LTgXmMoB7$" role="2Oq$k0">
                          <node concept="30H73N" id="6LTgXmMoAPn" role="2Oq$k0" />
                          <node concept="3TrEf2" id="6LTgXmMoBGc" role="2OqNvi">
                            <ref role="3Tt5mk" to="aia3:4B5aqq3QF7a" resolve="specimenTypeReference" />
                          </node>
                        </node>
                        <node concept="3x8VRR" id="6LTgXmMoDs3" role="2OqNvi" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="gft3U" id="6LTgXmMoDT6" role="UU_$l">
                  <node concept="10Nm6u" id="6LTgXmMoEaS" role="gfFT$" />
                </node>
              </node>
              <node concept="29HgVG" id="4B5aqq4desR" role="lGtFl">
                <node concept="3NFfHV" id="4B5aqq4desS" role="3NFExx">
                  <node concept="3clFbS" id="4B5aqq4desT" role="2VODD2">
                    <node concept="3clFbF" id="4B5aqq4desU" role="3cqZAp">
                      <node concept="2OqwBi" id="4B5aqq4desV" role="3clFbG">
                        <node concept="3TrEf2" id="4B5aqq4desW" role="2OqNvi">
                          <ref role="3Tt5mk" to="aia3:4B5aqq3QF7a" resolve="specimenTypeReference" />
                        </node>
                        <node concept="30H73N" id="4B5aqq4desX" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="6khVixyi6eM" role="3acgRq">
      <ref role="30HIoZ" to="aia3:6khVixyeQtz" resolve="LaboratoryTestHasRequest" />
      <node concept="gft3U" id="6khVixyi6fW" role="1lVwrX">
        <node concept="2ShNRf" id="6khVixyi6g2" role="gfFT$">
          <node concept="1pGfFk" id="6khVixyi6jb" role="2ShVmc">
            <ref role="37wK5l" to="yhrd:1mAGFBLsLA4" resolve="TestHasRequest" />
            <node concept="Xl_RD" id="6LTgXmMozTV" role="37wK5m">
              <property role="Xl_RC" value="test" />
              <node concept="29HgVG" id="6LTgXmMozTW" role="lGtFl">
                <node concept="3NFfHV" id="6LTgXmMozTX" role="3NFExx">
                  <node concept="3clFbS" id="6LTgXmMozTY" role="2VODD2">
                    <node concept="3clFbF" id="6LTgXmMozTZ" role="3cqZAp">
                      <node concept="2OqwBi" id="6LTgXmMozU0" role="3clFbG">
                        <node concept="3TrEf2" id="6LTgXmMozU1" role="2OqNvi">
                          <ref role="3Tt5mk" to="aia3:6LTgXmMj7Lm" resolve="testReference" />
                        </node>
                        <node concept="30H73N" id="6LTgXmMozU2" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="Xl_RD" id="6LTgXmMoEij" role="37wK5m">
              <property role="Xl_RC" value="specimen" />
              <node concept="1W57fq" id="6LTgXmMoEik" role="lGtFl">
                <node concept="3IZrLx" id="6LTgXmMoEil" role="3IZSJc">
                  <node concept="3clFbS" id="6LTgXmMoEim" role="2VODD2">
                    <node concept="3clFbF" id="6LTgXmMoEin" role="3cqZAp">
                      <node concept="2OqwBi" id="6LTgXmMoEio" role="3clFbG">
                        <node concept="2OqwBi" id="6LTgXmMoEip" role="2Oq$k0">
                          <node concept="30H73N" id="6LTgXmMoEiq" role="2Oq$k0" />
                          <node concept="3TrEf2" id="6LTgXmMoEir" role="2OqNvi">
                            <ref role="3Tt5mk" to="aia3:4B5aqq3QF7a" resolve="specimenTypeReference" />
                          </node>
                        </node>
                        <node concept="3x8VRR" id="6LTgXmMoEis" role="2OqNvi" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="gft3U" id="6LTgXmMoEit" role="UU_$l">
                  <node concept="10Nm6u" id="6LTgXmMoEiu" role="gfFT$" />
                </node>
              </node>
              <node concept="29HgVG" id="6LTgXmMoEiv" role="lGtFl">
                <node concept="3NFfHV" id="6LTgXmMoEiw" role="3NFExx">
                  <node concept="3clFbS" id="6LTgXmMoEix" role="2VODD2">
                    <node concept="3clFbF" id="6LTgXmMoEiy" role="3cqZAp">
                      <node concept="2OqwBi" id="6LTgXmMoEiz" role="3clFbG">
                        <node concept="3TrEf2" id="6LTgXmMoEi$" role="2OqNvi">
                          <ref role="3Tt5mk" to="aia3:4B5aqq3QF7a" resolve="specimenTypeReference" />
                        </node>
                        <node concept="30H73N" id="6LTgXmMoEi_" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="4B5aqq4deGj" role="3acgRq">
      <ref role="30HIoZ" to="aia3:4B5aqq3QoIG" resolve="LaboratoryTestPreviousResultIsValid" />
      <node concept="gft3U" id="4B5aqq4deQz" role="1lVwrX">
        <node concept="2ShNRf" id="4B5aqq4deQ$" role="gfFT$">
          <node concept="1pGfFk" id="4B5aqq4deQ_" role="2ShVmc">
            <ref role="37wK5l" to="yhrd:4V3GMfXxDwc" resolve="TestPreviousResultIsValid" />
            <node concept="Xl_RD" id="6LTgXmMo$9K" role="37wK5m">
              <property role="Xl_RC" value="test" />
              <node concept="29HgVG" id="6LTgXmMo$9L" role="lGtFl">
                <node concept="3NFfHV" id="6LTgXmMo$9M" role="3NFExx">
                  <node concept="3clFbS" id="6LTgXmMo$9N" role="2VODD2">
                    <node concept="3clFbF" id="6LTgXmMo$9O" role="3cqZAp">
                      <node concept="2OqwBi" id="6LTgXmMo$9P" role="3clFbG">
                        <node concept="3TrEf2" id="6LTgXmMo$9Q" role="2OqNvi">
                          <ref role="3Tt5mk" to="aia3:6LTgXmMj7Lm" resolve="testReference" />
                        </node>
                        <node concept="30H73N" id="6LTgXmMo$9R" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="Xl_RD" id="6LTgXmMoEGK" role="37wK5m">
              <property role="Xl_RC" value="specimen" />
              <node concept="1W57fq" id="6LTgXmMoEGL" role="lGtFl">
                <node concept="3IZrLx" id="6LTgXmMoEGM" role="3IZSJc">
                  <node concept="3clFbS" id="6LTgXmMoEGN" role="2VODD2">
                    <node concept="3clFbF" id="6LTgXmMoEGO" role="3cqZAp">
                      <node concept="2OqwBi" id="6LTgXmMoEGP" role="3clFbG">
                        <node concept="2OqwBi" id="6LTgXmMoEGQ" role="2Oq$k0">
                          <node concept="30H73N" id="6LTgXmMoEGR" role="2Oq$k0" />
                          <node concept="3TrEf2" id="6LTgXmMoEGS" role="2OqNvi">
                            <ref role="3Tt5mk" to="aia3:4B5aqq3QF7a" resolve="specimenTypeReference" />
                          </node>
                        </node>
                        <node concept="3x8VRR" id="6LTgXmMoEGT" role="2OqNvi" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="gft3U" id="6LTgXmMoEGU" role="UU_$l">
                  <node concept="10Nm6u" id="6LTgXmMoEGV" role="gfFT$" />
                </node>
              </node>
              <node concept="29HgVG" id="6LTgXmMoEGW" role="lGtFl">
                <node concept="3NFfHV" id="6LTgXmMoEGX" role="3NFExx">
                  <node concept="3clFbS" id="6LTgXmMoEGY" role="2VODD2">
                    <node concept="3clFbF" id="6LTgXmMoEGZ" role="3cqZAp">
                      <node concept="2OqwBi" id="6LTgXmMoEH0" role="3clFbG">
                        <node concept="3TrEf2" id="6LTgXmMoEH1" role="2OqNvi">
                          <ref role="3Tt5mk" to="aia3:4B5aqq3QF7a" resolve="specimenTypeReference" />
                        </node>
                        <node concept="30H73N" id="6LTgXmMoEH2" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="4B5aqq4dfe8" role="3acgRq">
      <ref role="30HIoZ" to="aia3:4B5aqq3QoIJ" resolve="LaboratoryTestResultIsValid" />
      <node concept="gft3U" id="4B5aqq4dfgx" role="1lVwrX">
        <node concept="2ShNRf" id="4B5aqq4dfgB" role="gfFT$">
          <node concept="1pGfFk" id="4B5aqq4dfgC" role="2ShVmc">
            <ref role="37wK5l" to="yhrd:1mAGFBLsTwA" resolve="TestResultIsValid" />
            <node concept="Xl_RD" id="6LTgXmMo$p$" role="37wK5m">
              <property role="Xl_RC" value="test" />
              <node concept="29HgVG" id="6LTgXmMo$p_" role="lGtFl">
                <node concept="3NFfHV" id="6LTgXmMo$pA" role="3NFExx">
                  <node concept="3clFbS" id="6LTgXmMo$pB" role="2VODD2">
                    <node concept="3clFbF" id="6LTgXmMo$pC" role="3cqZAp">
                      <node concept="2OqwBi" id="6LTgXmMo$pD" role="3clFbG">
                        <node concept="3TrEf2" id="6LTgXmMo$pE" role="2OqNvi">
                          <ref role="3Tt5mk" to="aia3:6LTgXmMj7Lm" resolve="testReference" />
                        </node>
                        <node concept="30H73N" id="6LTgXmMo$pF" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="Xl_RD" id="6LTgXmMoF7d" role="37wK5m">
              <property role="Xl_RC" value="specimen" />
              <node concept="1W57fq" id="6LTgXmMoF7e" role="lGtFl">
                <node concept="3IZrLx" id="6LTgXmMoF7f" role="3IZSJc">
                  <node concept="3clFbS" id="6LTgXmMoF7g" role="2VODD2">
                    <node concept="3clFbF" id="6LTgXmMoF7h" role="3cqZAp">
                      <node concept="2OqwBi" id="6LTgXmMoF7i" role="3clFbG">
                        <node concept="2OqwBi" id="6LTgXmMoF7j" role="2Oq$k0">
                          <node concept="30H73N" id="6LTgXmMoF7k" role="2Oq$k0" />
                          <node concept="3TrEf2" id="6LTgXmMoF7l" role="2OqNvi">
                            <ref role="3Tt5mk" to="aia3:4B5aqq3QF7a" resolve="specimenTypeReference" />
                          </node>
                        </node>
                        <node concept="3x8VRR" id="6LTgXmMoF7m" role="2OqNvi" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="gft3U" id="6LTgXmMoF7n" role="UU_$l">
                  <node concept="10Nm6u" id="6LTgXmMoF7o" role="gfFT$" />
                </node>
              </node>
              <node concept="29HgVG" id="6LTgXmMoF7p" role="lGtFl">
                <node concept="3NFfHV" id="6LTgXmMoF7q" role="3NFExx">
                  <node concept="3clFbS" id="6LTgXmMoF7r" role="2VODD2">
                    <node concept="3clFbF" id="6LTgXmMoF7s" role="3cqZAp">
                      <node concept="2OqwBi" id="6LTgXmMoF7t" role="3clFbG">
                        <node concept="3TrEf2" id="6LTgXmMoF7u" role="2OqNvi">
                          <ref role="3Tt5mk" to="aia3:4B5aqq3QF7a" resolve="specimenTypeReference" />
                        </node>
                        <node concept="30H73N" id="6LTgXmMoF7v" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="4B5aqq4dfwe" role="3acgRq">
      <ref role="30HIoZ" to="aia3:4B5aqq4cwJ4" resolve="LaboratoryTestTimeSinceLastRequest" />
      <node concept="gft3U" id="4B5aqq4dfyP" role="1lVwrX">
        <node concept="2ShNRf" id="4B5aqq4dfyV" role="gfFT$">
          <node concept="1pGfFk" id="4B5aqq4dfyW" role="2ShVmc">
            <ref role="37wK5l" to="yhrd:1mAGFBLsOMX" resolve="TestTimeSinceLastRequested" />
            <node concept="Xl_RD" id="6LTgXmMo$Dx" role="37wK5m">
              <property role="Xl_RC" value="test" />
              <node concept="29HgVG" id="6LTgXmMo$Dy" role="lGtFl">
                <node concept="3NFfHV" id="6LTgXmMo$Dz" role="3NFExx">
                  <node concept="3clFbS" id="6LTgXmMo$D$" role="2VODD2">
                    <node concept="3clFbF" id="6LTgXmMo$D_" role="3cqZAp">
                      <node concept="2OqwBi" id="6LTgXmMo$DA" role="3clFbG">
                        <node concept="3TrEf2" id="6LTgXmMo$DB" role="2OqNvi">
                          <ref role="3Tt5mk" to="aia3:6LTgXmMj7Lm" resolve="testReference" />
                        </node>
                        <node concept="30H73N" id="6LTgXmMo$DC" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="Xl_RD" id="6LTgXmMoFDj" role="37wK5m">
              <property role="Xl_RC" value="specimen" />
              <node concept="1W57fq" id="6LTgXmMoFDk" role="lGtFl">
                <node concept="3IZrLx" id="6LTgXmMoFDl" role="3IZSJc">
                  <node concept="3clFbS" id="6LTgXmMoFDm" role="2VODD2">
                    <node concept="3clFbF" id="6LTgXmMoFDn" role="3cqZAp">
                      <node concept="2OqwBi" id="6LTgXmMoFDo" role="3clFbG">
                        <node concept="2OqwBi" id="6LTgXmMoFDp" role="2Oq$k0">
                          <node concept="30H73N" id="6LTgXmMoFDq" role="2Oq$k0" />
                          <node concept="3TrEf2" id="6LTgXmMoFDr" role="2OqNvi">
                            <ref role="3Tt5mk" to="aia3:4B5aqq3QF7a" resolve="specimenTypeReference" />
                          </node>
                        </node>
                        <node concept="3x8VRR" id="6LTgXmMoFDs" role="2OqNvi" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="gft3U" id="6LTgXmMoFDt" role="UU_$l">
                  <node concept="10Nm6u" id="6LTgXmMoFDu" role="gfFT$" />
                </node>
              </node>
              <node concept="29HgVG" id="6LTgXmMoFDv" role="lGtFl">
                <node concept="3NFfHV" id="6LTgXmMoFDw" role="3NFExx">
                  <node concept="3clFbS" id="6LTgXmMoFDx" role="2VODD2">
                    <node concept="3clFbF" id="6LTgXmMoFDy" role="3cqZAp">
                      <node concept="2OqwBi" id="6LTgXmMoFDz" role="3clFbG">
                        <node concept="3TrEf2" id="6LTgXmMoFD$" role="2OqNvi">
                          <ref role="3Tt5mk" to="aia3:4B5aqq3QF7a" resolve="specimenTypeReference" />
                        </node>
                        <node concept="30H73N" id="6LTgXmMoFD_" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="PDjyzjRGyO" role="3acgRq">
      <ref role="30HIoZ" to="aia3:PDjyzjQppD" resolve="LaboratoryTestHasBeenPerformed" />
      <node concept="gft3U" id="PDjyzjRG_L" role="1lVwrX">
        <node concept="2ShNRf" id="PDjyzjRG_R" role="gfFT$">
          <node concept="1pGfFk" id="PDjyzjRGD1" role="2ShVmc">
            <ref role="37wK5l" to="yhrd:PDjyzjRFif" resolve="TestHasBeenPerformed" />
            <node concept="Xl_RD" id="6LTgXmMo$M_" role="37wK5m">
              <property role="Xl_RC" value="test" />
              <node concept="29HgVG" id="6LTgXmMo$MA" role="lGtFl">
                <node concept="3NFfHV" id="6LTgXmMo$MB" role="3NFExx">
                  <node concept="3clFbS" id="6LTgXmMo$MC" role="2VODD2">
                    <node concept="3clFbF" id="6LTgXmMo$MD" role="3cqZAp">
                      <node concept="2OqwBi" id="6LTgXmMo$ME" role="3clFbG">
                        <node concept="3TrEf2" id="6LTgXmMo$MF" role="2OqNvi">
                          <ref role="3Tt5mk" to="aia3:6LTgXmMj7Lm" resolve="testReference" />
                        </node>
                        <node concept="30H73N" id="6LTgXmMo$MG" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="Xl_RD" id="6LTgXmMoGb3" role="37wK5m">
              <property role="Xl_RC" value="specimen" />
              <node concept="1W57fq" id="6LTgXmMoGb4" role="lGtFl">
                <node concept="3IZrLx" id="6LTgXmMoGb5" role="3IZSJc">
                  <node concept="3clFbS" id="6LTgXmMoGb6" role="2VODD2">
                    <node concept="3clFbF" id="6LTgXmMoGb7" role="3cqZAp">
                      <node concept="2OqwBi" id="6LTgXmMoGb8" role="3clFbG">
                        <node concept="2OqwBi" id="6LTgXmMoGb9" role="2Oq$k0">
                          <node concept="30H73N" id="6LTgXmMoGba" role="2Oq$k0" />
                          <node concept="3TrEf2" id="6LTgXmMoGbb" role="2OqNvi">
                            <ref role="3Tt5mk" to="aia3:4B5aqq3QF7a" resolve="specimenTypeReference" />
                          </node>
                        </node>
                        <node concept="3x8VRR" id="6LTgXmMoGbc" role="2OqNvi" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="gft3U" id="6LTgXmMoGbd" role="UU_$l">
                  <node concept="10Nm6u" id="6LTgXmMoGbe" role="gfFT$" />
                </node>
              </node>
              <node concept="29HgVG" id="6LTgXmMoGbf" role="lGtFl">
                <node concept="3NFfHV" id="6LTgXmMoGbg" role="3NFExx">
                  <node concept="3clFbS" id="6LTgXmMoGbh" role="2VODD2">
                    <node concept="3clFbF" id="6LTgXmMoGbi" role="3cqZAp">
                      <node concept="2OqwBi" id="6LTgXmMoGbj" role="3clFbG">
                        <node concept="3TrEf2" id="6LTgXmMoGbk" role="2OqNvi">
                          <ref role="3Tt5mk" to="aia3:4B5aqq3QF7a" resolve="specimenTypeReference" />
                        </node>
                        <node concept="30H73N" id="6LTgXmMoGbl" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="1pmfR0" id="4V3GMfXSERl">
    <property role="TrG5h" value="dataLaboratoryTestScript" />
    <property role="1v3f2W" value="pre_processing" />
    <node concept="1pplIY" id="4V3GMfXSERm" role="1pqMTA">
      <node concept="3clFbS" id="4V3GMfXSERn" role="2VODD2">
        <node concept="2xdQw9" id="4V3GMfXSER_" role="3cqZAp">
          <property role="2xdLsb" value="debug" />
          <node concept="Xl_RD" id="4V3GMfXSERB" role="9lYJi">
            <property role="Xl_RC" value="data.laboratoryTests" />
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

