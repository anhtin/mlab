<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:d6374b20-dfe3-4cab-8f8b-b4fb4ea31050(no.uio.mLab.decisions.data.laboratoryTest.behavior)">
  <persistence version="9" />
  <languages>
    <use id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior" version="1" />
    <use id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions" version="4" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="yobs" ref="r:ef86e72b-c040-4e34-93f2-8df1d703b946(no.uio.mLab.decisions.data.laboratoryTest.translations)" />
    <import index="1yj" ref="r:0c887d67-81ab-420f-9a35-a9f6bf96c975(no.uio.mLab.shared.behavior)" />
    <import index="wb6c" ref="r:19d15037-83c1-4ae2-b66a-763828cb8279(no.uio.mLab.decisions.core.behavior)" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" />
    <import index="aia3" ref="r:856e974b-6e42-49cc-99cd-c3adb4161c21(no.uio.mLab.decisions.data.laboratoryTest.structure)" />
    <import index="7f9y" ref="r:790ac37e-42d3-4dd3-977c-e8fe9cc56a45(no.uio.mLab.decisions.core.structure)" />
    <import index="hyw5" ref="r:b77da972-4430-4cfb-8552-374583961329(no.uio.mLab.decisions.core.utils)" />
  </imports>
  <registry>
    <language id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior">
      <concept id="6496299201655527393" name="jetbrains.mps.lang.behavior.structure.LocalBehaviorMethodCall" flags="nn" index="BsUDl" />
      <concept id="1225194240794" name="jetbrains.mps.lang.behavior.structure.ConceptBehavior" flags="ng" index="13h7C7">
        <reference id="1225194240799" name="concept" index="13h7C2" />
        <child id="1225194240805" name="method" index="13h7CS" />
        <child id="1225194240801" name="constructor" index="13h7CW" />
      </concept>
      <concept id="1225194413805" name="jetbrains.mps.lang.behavior.structure.ConceptConstructorDeclaration" flags="in" index="13hLZK" />
      <concept id="1225194472830" name="jetbrains.mps.lang.behavior.structure.ConceptMethodDeclaration" flags="ng" index="13i0hz">
        <property id="5864038008284099149" name="isStatic" index="2Ki8OM" />
        <property id="1225194472832" name="isVirtual" index="13i0it" />
        <property id="1225194472834" name="isAbstract" index="13i0iv" />
        <reference id="1225194472831" name="overriddenMethod" index="13i0hy" />
      </concept>
      <concept id="1225194691553" name="jetbrains.mps.lang.behavior.structure.ThisNodeExpression" flags="nn" index="13iPFW" />
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="4836112446988635817" name="jetbrains.mps.baseLanguage.structure.UndefinedType" flags="in" index="2jxLKc" />
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1173175405605" name="jetbrains.mps.baseLanguage.structure.ArrayAccessExpression" flags="nn" index="AH0OO">
        <child id="1173175577737" name="index" index="AHEQo" />
        <child id="1173175590490" name="array" index="AHHXb" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1070534058343" name="jetbrains.mps.baseLanguage.structure.NullLiteral" flags="nn" index="10Nm6u" />
      <concept id="1070534760951" name="jetbrains.mps.baseLanguage.structure.ArrayType" flags="in" index="10Q1$e">
        <child id="1070534760952" name="componentType" index="10Q1$1" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123152" name="jetbrains.mps.baseLanguage.structure.EqualsExpression" flags="nn" index="3clFbC" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1154542696413" name="jetbrains.mps.baseLanguage.structure.ArrayCreatorWithInitializer" flags="nn" index="3g6Rrh">
        <child id="1154542793668" name="componentType" index="3g7fb8" />
        <child id="1154542803372" name="initValue" index="3g7hyw" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="5497648299878491908" name="jetbrains.mps.baseLanguage.structure.BaseVariableReference" flags="nn" index="1M0zk4">
        <reference id="5497648299878491909" name="baseVariableDeclaration" index="1M0zk5" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
      <concept id="1146644623116" name="jetbrains.mps.baseLanguage.structure.PrivateVisibility" flags="nn" index="3Tm6S6" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1204834851141" name="jetbrains.mps.lang.smodel.structure.PoundExpression" flags="ng" index="25Kdxt">
        <child id="1204834868751" name="expression" index="25KhWn" />
      </concept>
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="7453996997717780434" name="jetbrains.mps.lang.smodel.structure.Node_GetSConceptOperation" flags="nn" index="2yIwOk" />
      <concept id="1883223317721008708" name="jetbrains.mps.lang.smodel.structure.IfInstanceOfStatement" flags="nn" index="Jncv_">
        <reference id="1883223317721008712" name="nodeConcept" index="JncvD" />
        <child id="1883223317721008709" name="body" index="Jncv$" />
        <child id="1883223317721008711" name="variable" index="JncvA" />
        <child id="1883223317721008710" name="nodeExpression" index="JncvB" />
      </concept>
      <concept id="1883223317721008713" name="jetbrains.mps.lang.smodel.structure.IfInstanceOfVariable" flags="ng" index="JncvC" />
      <concept id="1883223317721107059" name="jetbrains.mps.lang.smodel.structure.IfInstanceOfVarReference" flags="nn" index="Jnkvi" />
      <concept id="1139621453865" name="jetbrains.mps.lang.smodel.structure.Node_IsInstanceOfOperation" flags="nn" index="1mIQ4w">
        <child id="1177027386292" name="conceptArgument" index="cj9EA" />
      </concept>
      <concept id="1172008320231" name="jetbrains.mps.lang.smodel.structure.Node_IsNotNullOperation" flags="nn" index="3x8VRR" />
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2" />
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="13h7C7" id="4V3GMfXvCx9">
    <property role="3GE5qa" value="shared" />
    <ref role="13h7C2" to="aia3:4V3GMfXvClD" resolve="ITranslatableLaboratoryDataConcept" />
    <node concept="13i0hz" id="4V3GMfXvCxk" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getDisplayTranslations" />
      <node concept="3Tm1VV" id="4V3GMfXvCxl" role="1B3o_S" />
      <node concept="3uibUv" id="4V3GMfXvCxO" role="3clF45">
        <ref role="3uigEE" to="yobs:4zMac8rUNtP" resolve="ILaboratoryTestDataTranslations" />
      </node>
      <node concept="3clFbS" id="4V3GMfXvCxn" role="3clF47">
        <node concept="3clFbF" id="4V3GMfXvCzh" role="3cqZAp">
          <node concept="10M0yZ" id="4V3GMfXvCzR" role="3clFbG">
            <ref role="3cqZAo" to="yobs:4zMac8rVBeK" resolve="displayTranslations" />
            <ref role="1PxDUh" to="yobs:4zMac8rUNsN" resolve="LaboratoryTestDataTranslationProvider" />
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="4V3GMfXvC$s" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getGenerationTranslations" />
      <node concept="3Tm1VV" id="4V3GMfXvC$t" role="1B3o_S" />
      <node concept="3uibUv" id="4V3GMfXvC$u" role="3clF45">
        <ref role="3uigEE" to="yobs:4zMac8rUNtP" resolve="ILaboratoryTestDataTranslations" />
      </node>
      <node concept="3clFbS" id="4V3GMfXvC$v" role="3clF47">
        <node concept="3clFbF" id="4V3GMfXvC$w" role="3cqZAp">
          <node concept="10M0yZ" id="4V3GMfXvCAT" role="3clFbG">
            <ref role="3cqZAo" to="yobs:1Hxyv4DUmfq" resolve="generationTranslations" />
            <ref role="1PxDUh" to="yobs:4zMac8rUNsN" resolve="LaboratoryTestDataTranslationProvider" />
          </node>
        </node>
      </node>
    </node>
    <node concept="13hLZK" id="4V3GMfXvCxa" role="13h7CW">
      <node concept="3clFbS" id="4V3GMfXvCxb" role="2VODD2" />
    </node>
  </node>
  <node concept="13h7C7" id="6khVixyeUpy">
    <property role="3GE5qa" value="base.data.boolean" />
    <ref role="13h7C2" to="aia3:6khVixyeQtz" resolve="LaboratoryTestHasRequest" />
    <node concept="13hLZK" id="6khVixyeUpz" role="13h7CW">
      <node concept="3clFbS" id="6khVixyeUp$" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6khVixyeUpR" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="6khVixyeUpS" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixyeUpX" role="3clF47">
        <node concept="3clFbF" id="6khVixyeUEM" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixyeULI" role="3clFbG">
            <node concept="BsUDl" id="6khVixyeUEL" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXvCxk" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixyeURv" role="2OqNvi">
              <ref role="37wK5l" to="yobs:7AAKH6gc61N" resolve="getLaboratoryTestHasRequestAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixyeUpY" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixyeUq3" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="6khVixyeUq4" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixyeUq9" role="3clF47">
        <node concept="3clFbF" id="6khVixyeUS9" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixyeUSb" role="3clFbG">
            <node concept="BsUDl" id="6khVixyeUSc" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXvCxk" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6LTgXmMnU5Y" role="2OqNvi">
              <ref role="37wK5l" to="yobs:7AAKH6gc6Dw" resolve="getLaboratoryTestHasRequestDescription" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixyeUqa" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixyeUqf" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getGenerationAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:4QUW3efxqUv" resolve="getGenerationAlias" />
      <node concept="3Tm1VV" id="6khVixyeUqg" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixyeUql" role="3clF47">
        <node concept="3clFbF" id="6khVixyeUU4" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixyeUU6" role="3clFbG">
            <node concept="BsUDl" id="6khVixyeUVf" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXvC$s" resolve="getGenerationTranslations" />
            </node>
            <node concept="liA8E" id="6khVixyeUU8" role="2OqNvi">
              <ref role="37wK5l" to="yobs:7AAKH6gc61N" resolve="getLaboratoryTestHasRequestAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixyeUqm" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmMnTAW" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getAliasWithSpecimenType" />
      <ref role="13i0hy" node="6LTgXmMlgN$" resolve="getAliasWithSpecimenType" />
      <node concept="3Tm1VV" id="6LTgXmMnTAX" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmMnTB5" role="3clF47">
        <node concept="3clFbF" id="6LTgXmMnTBa" role="3cqZAp">
          <node concept="2OqwBi" id="6LTgXmMnTS5" role="3clFbG">
            <node concept="BsUDl" id="6LTgXmMnTS6" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXvCxk" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6LTgXmMnTS7" role="2OqNvi">
              <ref role="37wK5l" to="yobs:4B5aqq44_aU" resolve="getLaboratoryTestHasRequestWithSpecimenAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmMnTB6" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmMnTBb" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getDescriptionWithSpecimenType" />
      <ref role="13i0hy" node="6LTgXmMlq1k" resolve="getDescriptionWithSpecimenType" />
      <node concept="3Tm1VV" id="6LTgXmMnTBc" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmMnTBh" role="3clF47">
        <node concept="3clFbF" id="6LTgXmMnTBm" role="3cqZAp">
          <node concept="2OqwBi" id="6LTgXmMnTXB" role="3clFbG">
            <node concept="BsUDl" id="6LTgXmMnTXC" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXvCxk" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6LTgXmMnTXD" role="2OqNvi">
              <ref role="37wK5l" to="yobs:4B5aqq44_aY" resolve="getLaboratoryTestHasRequestWithSpecimenDescription" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmMnTBi" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmMnp28" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getDisplayKeywords" />
      <ref role="13i0hy" to="1yj:4B5aqq3ZFNV" resolve="getDisplayKeywords" />
      <node concept="3Tm1VV" id="6LTgXmMnp29" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmMnp2a" role="3clF47">
        <node concept="3clFbF" id="6LTgXmMnp2b" role="3cqZAp">
          <node concept="BsUDl" id="6LTgXmMnp2c" role="3clFbG">
            <ref role="37wK5l" node="4B5aqq45CVk" resolve="getLocalizedKeywords" />
            <node concept="BsUDl" id="6LTgXmMnp2d" role="37wK5m">
              <ref role="37wK5l" node="4V3GMfXvCxk" resolve="getDisplayTranslations" />
            </node>
          </node>
        </node>
      </node>
      <node concept="10Q1$e" id="6LTgXmMnp2e" role="3clF45">
        <node concept="17QB3L" id="6LTgXmMnp2f" role="10Q1$1" />
      </node>
    </node>
    <node concept="13i0hz" id="6LTgXmMnp2g" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getGenerationKeywords" />
      <ref role="13i0hy" to="1yj:4B5aqq3ZFQj" resolve="getGenerationKeywords" />
      <node concept="3Tm1VV" id="6LTgXmMnp2h" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmMnp2i" role="3clF47">
        <node concept="3clFbF" id="6LTgXmMnp2j" role="3cqZAp">
          <node concept="BsUDl" id="6LTgXmMnp2k" role="3clFbG">
            <ref role="37wK5l" node="4B5aqq45CVk" resolve="getLocalizedKeywords" />
            <node concept="BsUDl" id="6LTgXmMnp2l" role="37wK5m">
              <ref role="37wK5l" node="4V3GMfXvC$s" resolve="getGenerationTranslations" />
            </node>
          </node>
        </node>
      </node>
      <node concept="10Q1$e" id="6LTgXmMnp2m" role="3clF45">
        <node concept="17QB3L" id="6LTgXmMnp2n" role="10Q1$1" />
      </node>
    </node>
    <node concept="13i0hz" id="4B5aqq45CVk" role="13h7CS">
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getLocalizedKeywords" />
      <node concept="3Tm1VV" id="4B5aqq45CVl" role="1B3o_S" />
      <node concept="10Q1$e" id="4B5aqq45Dgr" role="3clF45">
        <node concept="17QB3L" id="4B5aqq45Dgf" role="10Q1$1" />
      </node>
      <node concept="3clFbS" id="4B5aqq45CVn" role="3clF47">
        <node concept="3cpWs8" id="4B5aqq45Dic" role="3cqZAp">
          <node concept="3cpWsn" id="4B5aqq45Did" role="3cpWs9">
            <property role="TrG5h" value="firstKeyword" />
            <node concept="17QB3L" id="4B5aqq45Die" role="1tU5fm" />
            <node concept="2OqwBi" id="4B5aqq45Dif" role="33vP2m">
              <node concept="37vLTw" id="4B5aqq45Dig" role="2Oq$k0">
                <ref role="3cqZAo" node="4B5aqq45Dh9" resolve="translations" />
              </node>
              <node concept="liA8E" id="4B5aqq45Dih" role="2OqNvi">
                <ref role="37wK5l" to="yobs:4B5aqq44_b2" resolve="getLaboratoryTestHasRequestWithSpecimenKeywordTestHasRequest" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="4B5aqq45Dii" role="3cqZAp">
          <node concept="3cpWsn" id="4B5aqq45Dij" role="3cpWs9">
            <property role="TrG5h" value="secondKeyword" />
            <node concept="17QB3L" id="4B5aqq45Dik" role="1tU5fm" />
            <node concept="2OqwBi" id="4B5aqq45Dil" role="33vP2m">
              <node concept="37vLTw" id="4B5aqq45Dim" role="2Oq$k0">
                <ref role="3cqZAo" node="4B5aqq45Dh9" resolve="translations" />
              </node>
              <node concept="liA8E" id="4B5aqq45Din" role="2OqNvi">
                <ref role="37wK5l" to="yobs:4B5aqq44_b6" resolve="getLaboratoryTestHasRequestWithSpecimenKeywordWithSpecimen" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4B5aqq45Dl4" role="3cqZAp">
          <node concept="2ShNRf" id="4B5aqq45Dl0" role="3clFbG">
            <node concept="3g6Rrh" id="4B5aqq45Dwr" role="2ShVmc">
              <node concept="17QB3L" id="4B5aqq45DnY" role="3g7fb8" />
              <node concept="37vLTw" id="4B5aqq45DBD" role="3g7hyw">
                <ref role="3cqZAo" node="4B5aqq45Did" resolve="firstKeyword" />
              </node>
              <node concept="37vLTw" id="4B5aqq45DJn" role="3g7hyw">
                <ref role="3cqZAo" node="4B5aqq45Dij" resolve="secondKeyword" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="4B5aqq45Dh9" role="3clF46">
        <property role="TrG5h" value="translations" />
        <node concept="3uibUv" id="4B5aqq45Dh8" role="1tU5fm">
          <ref role="3uigEE" to="yobs:4zMac8rUNtP" resolve="ILaboratoryTestDataTranslations" />
        </node>
      </node>
    </node>
  </node>
  <node concept="13h7C7" id="4B5aqq3QFq_">
    <property role="3GE5qa" value="base.data" />
    <ref role="13h7C2" to="aia3:4B5aqq3QF78" resolve="ILaboratoryTestDataValue" />
    <node concept="13i0hz" id="6LTgXmMlgN$" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="true" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getAliasWithSpecimenType" />
      <node concept="3Tm1VV" id="6LTgXmMlgN_" role="1B3o_S" />
      <node concept="17QB3L" id="6LTgXmMlh67" role="3clF45" />
      <node concept="3clFbS" id="6LTgXmMlgNB" role="3clF47">
        <node concept="3clFbF" id="6LTgXmMlhXr" role="3cqZAp">
          <node concept="2YIFZM" id="6LTgXmMlhXL" role="3clFbG">
            <ref role="37wK5l" to="wyt6:~String.format(java.lang.String,java.lang.Object...):java.lang.String" resolve="format" />
            <ref role="1Pybhc" to="wyt6:~String" resolve="String" />
            <node concept="Xl_RD" id="6LTgXmMli2j" role="37wK5m">
              <property role="Xl_RC" value="%s %s" />
            </node>
            <node concept="BsUDl" id="6LTgXmMlil1" role="37wK5m">
              <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
            </node>
            <node concept="BsUDl" id="6LTgXmMliDp" role="37wK5m">
              <ref role="37wK5l" node="4B5aqq3QFqK" resolve="getWithSpecimenTypeKeyword" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="6LTgXmMlq1k" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="true" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getDescriptionWithSpecimenType" />
      <node concept="3Tm1VV" id="6LTgXmMlq1l" role="1B3o_S" />
      <node concept="17QB3L" id="6LTgXmMlqk9" role="3clF45" />
      <node concept="3clFbS" id="6LTgXmMlq1n" role="3clF47">
        <node concept="3clFbF" id="6LTgXmMlql6" role="3cqZAp">
          <node concept="BsUDl" id="6LTgXmMlql5" role="3clFbG">
            <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="4B5aqq3QFqK" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="true" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getWithSpecimenTypeKeyword" />
      <node concept="3Tm1VV" id="4B5aqq3QFqL" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq3QFrg" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq3QFqN" role="3clF47">
        <node concept="3clFbF" id="4B5aqq3QFrW" role="3cqZAp">
          <node concept="AH0OO" id="4B5aqq49y37" role="3clFbG">
            <node concept="3cmrfG" id="4B5aqq49y48" role="AHEQo">
              <property role="3cmrfH" value="1" />
            </node>
            <node concept="BsUDl" id="4B5aqq49xAu" role="AHHXb">
              <ref role="37wK5l" to="1yj:4B5aqq3ZFNV" resolve="getDisplayKeywords" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="6LTgXmMiWh2" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="true" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getIncludeSpecimenTypeIntentionDescription" />
      <node concept="3Tm1VV" id="6LTgXmMiWh3" role="1B3o_S" />
      <node concept="17QB3L" id="6LTgXmMiWz1" role="3clF45" />
      <node concept="3clFbS" id="6LTgXmMiWh5" role="3clF47">
        <node concept="3clFbF" id="6LTgXmMiW$6" role="3cqZAp">
          <node concept="2OqwBi" id="6LTgXmMiWF4" role="3clFbG">
            <node concept="BsUDl" id="6LTgXmMiW$5" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXvCxk" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6LTgXmMiWKY" role="2OqNvi">
              <ref role="37wK5l" to="yobs:6LTgXmMhnrg" resolve="getILaboratoryTestDataValueIncludeSpecimenTypeIntentionDescription" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="4B5aqq3ZCop" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="toDisplayString" />
      <ref role="13i0hy" to="1yj:499Gn2DGTs1" resolve="toDisplayString" />
      <node concept="3Tm1VV" id="4B5aqq3ZCoq" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq3ZCoA" role="3clF47">
        <node concept="3cpWs8" id="4B5aqq3ZH5N" role="3cqZAp">
          <node concept="3cpWsn" id="4B5aqq3ZH5Q" role="3cpWs9">
            <property role="TrG5h" value="keywords" />
            <node concept="10Q1$e" id="4B5aqq3ZHeY" role="1tU5fm">
              <node concept="17QB3L" id="4B5aqq3ZH5L" role="10Q1$1" />
            </node>
            <node concept="BsUDl" id="4B5aqq3ZHnO" role="33vP2m">
              <ref role="37wK5l" to="1yj:4B5aqq3ZFNV" resolve="getDisplayKeywords" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="6LTgXmMjn75" role="3cqZAp">
          <node concept="BsUDl" id="6LTgXmMjn76" role="3clFbG">
            <ref role="37wK5l" node="6LTgXmMj8IB" resolve="toLocalizedString" />
            <node concept="AH0OO" id="6LTgXmMjn77" role="37wK5m">
              <node concept="3cmrfG" id="6LTgXmMjn78" role="AHEQo">
                <property role="3cmrfH" value="0" />
              </node>
              <node concept="37vLTw" id="6LTgXmMjn79" role="AHHXb">
                <ref role="3cqZAo" node="4B5aqq3ZH5Q" resolve="keywords" />
              </node>
            </node>
            <node concept="2OqwBi" id="6LTgXmMjn7a" role="37wK5m">
              <node concept="2OqwBi" id="6LTgXmMjn7b" role="2Oq$k0">
                <node concept="13iPFW" id="6LTgXmMjn7c" role="2Oq$k0" />
                <node concept="3TrEf2" id="6LTgXmMjn7d" role="2OqNvi">
                  <ref role="3Tt5mk" to="aia3:6LTgXmMj7Lm" resolve="testReference" />
                </node>
              </node>
              <node concept="2qgKlT" id="6LTgXmMjnC4" role="2OqNvi">
                <ref role="37wK5l" to="1yj:499Gn2DGTs1" resolve="toDisplayString" />
              </node>
            </node>
            <node concept="AH0OO" id="6LTgXmMjn7f" role="37wK5m">
              <node concept="3cmrfG" id="6LTgXmMjn7g" role="AHEQo">
                <property role="3cmrfH" value="1" />
              </node>
              <node concept="37vLTw" id="6LTgXmMjn7h" role="AHHXb">
                <ref role="3cqZAo" node="4B5aqq3ZH5Q" resolve="keywords" />
              </node>
            </node>
            <node concept="2OqwBi" id="6LTgXmMjn7i" role="37wK5m">
              <node concept="2OqwBi" id="6LTgXmMjn7j" role="2Oq$k0">
                <node concept="13iPFW" id="6LTgXmMjn7k" role="2Oq$k0" />
                <node concept="3TrEf2" id="6LTgXmMjn7l" role="2OqNvi">
                  <ref role="3Tt5mk" to="aia3:4B5aqq3QF7a" resolve="specimenTypeReference" />
                </node>
              </node>
              <node concept="2qgKlT" id="6LTgXmMjo5h" role="2OqNvi">
                <ref role="37wK5l" to="1yj:499Gn2DGTs1" resolve="toDisplayString" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="4B5aqq3ZCoB" role="3clF45" />
    </node>
    <node concept="13i0hz" id="4B5aqq3ZCoG" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="toGenerationString" />
      <ref role="13i0hy" to="1yj:499Gn2DGU_t" resolve="toGenerationString" />
      <node concept="3Tm1VV" id="4B5aqq3ZCoH" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq3ZCoT" role="3clF47">
        <node concept="3cpWs8" id="4B5aqq3ZLjH" role="3cqZAp">
          <node concept="3cpWsn" id="4B5aqq3ZLjI" role="3cpWs9">
            <property role="TrG5h" value="keywords" />
            <node concept="10Q1$e" id="4B5aqq3ZLjJ" role="1tU5fm">
              <node concept="17QB3L" id="4B5aqq3ZLjK" role="10Q1$1" />
            </node>
            <node concept="BsUDl" id="4B5aqq3ZMc7" role="33vP2m">
              <ref role="37wK5l" to="1yj:4B5aqq3ZFQj" resolve="getGenerationKeywords" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="6LTgXmMjghk" role="3cqZAp">
          <node concept="BsUDl" id="6LTgXmMjghi" role="3clFbG">
            <ref role="37wK5l" node="6LTgXmMj8IB" resolve="toLocalizedString" />
            <node concept="AH0OO" id="6LTgXmMjhpp" role="37wK5m">
              <node concept="3cmrfG" id="6LTgXmMjhus" role="AHEQo">
                <property role="3cmrfH" value="0" />
              </node>
              <node concept="37vLTw" id="6LTgXmMjgwY" role="AHHXb">
                <ref role="3cqZAo" node="4B5aqq3ZLjI" resolve="keywords" />
              </node>
            </node>
            <node concept="2OqwBi" id="6LTgXmMjiK9" role="37wK5m">
              <node concept="2OqwBi" id="6LTgXmMjhWN" role="2Oq$k0">
                <node concept="13iPFW" id="6LTgXmMjhGj" role="2Oq$k0" />
                <node concept="3TrEf2" id="6LTgXmMjiiq" role="2OqNvi">
                  <ref role="3Tt5mk" to="aia3:6LTgXmMj7Lm" resolve="testReference" />
                </node>
              </node>
              <node concept="2qgKlT" id="6LTgXmMjjc5" role="2OqNvi">
                <ref role="37wK5l" to="1yj:499Gn2DGU_t" resolve="toGenerationString" />
              </node>
            </node>
            <node concept="AH0OO" id="6LTgXmMjk2W" role="37wK5m">
              <node concept="3cmrfG" id="6LTgXmMjkg2" role="AHEQo">
                <property role="3cmrfH" value="1" />
              </node>
              <node concept="37vLTw" id="6LTgXmMjjCX" role="AHHXb">
                <ref role="3cqZAo" node="4B5aqq3ZLjI" resolve="keywords" />
              </node>
            </node>
            <node concept="2OqwBi" id="6LTgXmMjlXu" role="37wK5m">
              <node concept="2OqwBi" id="6LTgXmMjkZ9" role="2Oq$k0">
                <node concept="13iPFW" id="6LTgXmMjkHJ" role="2Oq$k0" />
                <node concept="3TrEf2" id="6LTgXmMjloF" role="2OqNvi">
                  <ref role="3Tt5mk" to="aia3:4B5aqq3QF7a" resolve="specimenTypeReference" />
                </node>
              </node>
              <node concept="2qgKlT" id="6LTgXmMjmtn" role="2OqNvi">
                <ref role="37wK5l" to="1yj:499Gn2DGU_t" resolve="toGenerationString" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="4B5aqq3ZCoU" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmMj8IB" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="true" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="toLocalizedString" />
      <node concept="3Tm1VV" id="6LTgXmMj8IC" role="1B3o_S" />
      <node concept="17QB3L" id="6LTgXmMj90Q" role="3clF45" />
      <node concept="3clFbS" id="6LTgXmMj8IE" role="3clF47">
        <node concept="3clFbJ" id="6LTgXmMjdms" role="3cqZAp">
          <node concept="3clFbS" id="6LTgXmMjdmu" role="3clFbx">
            <node concept="3cpWs6" id="6LTgXmMjfc6" role="3cqZAp">
              <node concept="2YIFZM" id="6LTgXmMjfma" role="3cqZAk">
                <ref role="37wK5l" to="wyt6:~String.format(java.lang.String,java.lang.Object...):java.lang.String" resolve="format" />
                <ref role="1Pybhc" to="wyt6:~String" resolve="String" />
                <node concept="Xl_RD" id="6LTgXmMjfmb" role="37wK5m">
                  <property role="Xl_RC" value="%s %s" />
                </node>
                <node concept="37vLTw" id="6LTgXmMjfmc" role="37wK5m">
                  <ref role="3cqZAo" node="6LTgXmMj91F" resolve="firstKeyword" />
                </node>
                <node concept="37vLTw" id="6LTgXmMjfmd" role="37wK5m">
                  <ref role="3cqZAo" node="6LTgXmMj92R" resolve="test" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbC" id="6LTgXmMjeTu" role="3clFbw">
            <node concept="10Nm6u" id="6LTgXmMjf7A" role="3uHU7w" />
            <node concept="37vLTw" id="6LTgXmMjdsy" role="3uHU7B">
              <ref role="3cqZAo" node="6LTgXmMjbE1" resolve="specimen" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="6LTgXmMj99P" role="3cqZAp">
          <node concept="2YIFZM" id="6LTgXmMj99Q" role="3clFbG">
            <ref role="37wK5l" to="wyt6:~String.format(java.lang.String,java.lang.Object...):java.lang.String" resolve="format" />
            <ref role="1Pybhc" to="wyt6:~String" resolve="String" />
            <node concept="Xl_RD" id="6LTgXmMj99R" role="37wK5m">
              <property role="Xl_RC" value="%s %s %s %s" />
            </node>
            <node concept="37vLTw" id="6LTgXmMjbdT" role="37wK5m">
              <ref role="3cqZAo" node="6LTgXmMj91F" resolve="firstKeyword" />
            </node>
            <node concept="37vLTw" id="6LTgXmMjbQ2" role="37wK5m">
              <ref role="3cqZAo" node="6LTgXmMj92R" resolve="test" />
            </node>
            <node concept="37vLTw" id="6LTgXmMjctm" role="37wK5m">
              <ref role="3cqZAo" node="6LTgXmMj94D" resolve="secondKeyword" />
            </node>
            <node concept="37vLTw" id="6LTgXmMjd20" role="37wK5m">
              <ref role="3cqZAo" node="6LTgXmMjbE1" resolve="specimen" />
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="6LTgXmMj91F" role="3clF46">
        <property role="TrG5h" value="firstKeyword" />
        <node concept="17QB3L" id="6LTgXmMj91E" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="6LTgXmMj92R" role="3clF46">
        <property role="TrG5h" value="test" />
        <node concept="17QB3L" id="6LTgXmMj93b" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="6LTgXmMj94D" role="3clF46">
        <property role="TrG5h" value="secondKeyword" />
        <node concept="17QB3L" id="6LTgXmMj951" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="6LTgXmMjbE1" role="3clF46">
        <property role="TrG5h" value="specimen" />
        <node concept="17QB3L" id="6LTgXmMjbPm" role="1tU5fm" />
      </node>
    </node>
    <node concept="13i0hz" id="1I84Bf75TJj" role="13h7CS">
      <property role="TrG5h" value="matches" />
      <property role="13i0it" value="false" />
      <property role="13i0iv" value="false" />
      <ref role="13i0hy" to="wb6c:6LTgXmMs$_D" resolve="matches" />
      <node concept="3Tm1VV" id="1I84Bf75TJk" role="1B3o_S" />
      <node concept="3clFbS" id="1I84Bf75TJx" role="3clF47">
        <node concept="Jncv_" id="1I84Bf75U80" role="3cqZAp">
          <ref role="JncvD" to="aia3:4B5aqq3QF78" resolve="ILaboratoryTestDataValue" />
          <node concept="37vLTw" id="1I84Bf75U8t" role="JncvB">
            <ref role="3cqZAo" node="1I84Bf75TJy" resolve="node" />
          </node>
          <node concept="3clFbS" id="1I84Bf75U82" role="Jncv$">
            <node concept="3clFbJ" id="1I84Bf75U9m" role="3cqZAp">
              <node concept="2OqwBi" id="1I84Bf75Ulq" role="3clFbw">
                <node concept="Jnkvi" id="1I84Bf75U9E" role="2Oq$k0">
                  <ref role="1M0zk5" node="1I84Bf75U83" resolve="other" />
                </node>
                <node concept="1mIQ4w" id="1I84Bf75Uyr" role="2OqNvi">
                  <node concept="25Kdxt" id="1I84Bf75U_e" role="cj9EA">
                    <node concept="2OqwBi" id="1I84Bf75UQv" role="25KhWn">
                      <node concept="13iPFW" id="1I84Bf75UC4" role="2Oq$k0" />
                      <node concept="2yIwOk" id="1I84Bf75V5L" role="2OqNvi" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbS" id="1I84Bf75U9o" role="3clFbx">
                <node concept="3cpWs8" id="1I84Bf75VG0" role="3cqZAp">
                  <node concept="3cpWsn" id="1I84Bf75VG1" role="3cpWs9">
                    <property role="TrG5h" value="match" />
                    <node concept="3uibUv" id="1I84Bf75VG2" role="1tU5fm">
                      <ref role="3uigEE" to="hyw5:6LTgXmO6cZw" resolve="JoinPointMatch" />
                    </node>
                    <node concept="2OqwBi" id="1I84Bf75VG3" role="33vP2m">
                      <node concept="2OqwBi" id="1I84Bf75VG4" role="2Oq$k0">
                        <node concept="13iPFW" id="1I84Bf75VG5" role="2Oq$k0" />
                        <node concept="3TrEf2" id="1I84Bf75VG6" role="2OqNvi">
                          <ref role="3Tt5mk" to="aia3:6LTgXmMj7Lm" resolve="testReference" />
                        </node>
                      </node>
                      <node concept="2qgKlT" id="1I84Bf75VG7" role="2OqNvi">
                        <ref role="37wK5l" to="wb6c:6LTgXmMs$_D" resolve="matches" />
                        <node concept="2OqwBi" id="1I84Bf75VG8" role="37wK5m">
                          <node concept="Jnkvi" id="1I84Bf764B$" role="2Oq$k0">
                            <ref role="1M0zk5" node="1I84Bf75U83" resolve="other" />
                          </node>
                          <node concept="3TrEf2" id="1I84Bf75VGa" role="2OqNvi">
                            <ref role="3Tt5mk" to="aia3:6LTgXmMj7Lm" resolve="testReference" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbJ" id="1I84Bf75VGb" role="3cqZAp">
                  <node concept="3clFbS" id="1I84Bf75VGc" role="3clFbx">
                    <node concept="3clFbF" id="1I84Bf75VGd" role="3cqZAp">
                      <node concept="37vLTI" id="1I84Bf75VGe" role="3clFbG">
                        <node concept="2OqwBi" id="1I84Bf75VGf" role="37vLTx">
                          <node concept="37vLTw" id="1I84Bf75VGg" role="2Oq$k0">
                            <ref role="3cqZAo" node="1I84Bf75VG1" resolve="match" />
                          </node>
                          <node concept="liA8E" id="1I84Bf75VGh" role="2OqNvi">
                            <ref role="37wK5l" to="hyw5:1I84Bf7_lfa" resolve="merge" />
                            <node concept="2OqwBi" id="1I84Bf75VGi" role="37wK5m">
                              <node concept="2OqwBi" id="1I84Bf75VGj" role="2Oq$k0">
                                <node concept="13iPFW" id="1I84Bf75VGk" role="2Oq$k0" />
                                <node concept="3TrEf2" id="1I84Bf75VGl" role="2OqNvi">
                                  <ref role="3Tt5mk" to="aia3:4B5aqq3QF7a" resolve="specimenTypeReference" />
                                </node>
                              </node>
                              <node concept="2qgKlT" id="1I84Bf75VGm" role="2OqNvi">
                                <ref role="37wK5l" to="wb6c:6LTgXmMs$_D" resolve="matches" />
                                <node concept="2OqwBi" id="1I84Bf75VGn" role="37wK5m">
                                  <node concept="Jnkvi" id="1I84Bf765a9" role="2Oq$k0">
                                    <ref role="1M0zk5" node="1I84Bf75U83" resolve="other" />
                                  </node>
                                  <node concept="3TrEf2" id="1I84Bf75VGp" role="2OqNvi">
                                    <ref role="3Tt5mk" to="aia3:4B5aqq3QF7a" resolve="specimenTypeReference" />
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="37vLTw" id="1I84Bf75VGq" role="37vLTJ">
                          <ref role="3cqZAo" node="1I84Bf75VG1" resolve="match" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="2OqwBi" id="1I84Bf75VGr" role="3clFbw">
                    <node concept="2OqwBi" id="1I84Bf75VGs" role="2Oq$k0">
                      <node concept="Jnkvi" id="1I84Bf764TZ" role="2Oq$k0">
                        <ref role="1M0zk5" node="1I84Bf75U83" resolve="other" />
                      </node>
                      <node concept="3TrEf2" id="1I84Bf75VGu" role="2OqNvi">
                        <ref role="3Tt5mk" to="aia3:4B5aqq3QF7a" resolve="specimenTypeReference" />
                      </node>
                    </node>
                    <node concept="3x8VRR" id="1I84Bf75VGv" role="2OqNvi" />
                  </node>
                </node>
                <node concept="3cpWs6" id="1I84Bf75VGw" role="3cqZAp">
                  <node concept="37vLTw" id="1I84Bf75VGx" role="3cqZAk">
                    <ref role="3cqZAo" node="1I84Bf75VG1" resolve="match" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="JncvC" id="1I84Bf75U83" role="JncvA">
            <property role="TrG5h" value="other" />
            <node concept="2jxLKc" id="1I84Bf75U84" role="1tU5fm" />
          </node>
        </node>
        <node concept="3clFbF" id="1I84Bf75VG$" role="3cqZAp">
          <node concept="2YIFZM" id="1I84Bf80a99" role="3clFbG">
            <ref role="37wK5l" to="hyw5:1I84Bf7XEgz" resolve="createEmpty" />
            <ref role="1Pybhc" to="hyw5:6LTgXmO6cZw" resolve="JoinPointMatch" />
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="1I84Bf75TJy" role="3clF46">
        <property role="TrG5h" value="node" />
        <node concept="3Tqbb2" id="1I84Bf75TJz" role="1tU5fm" />
      </node>
      <node concept="3uibUv" id="1I84Bf75TJ$" role="3clF45">
        <ref role="3uigEE" to="hyw5:6LTgXmO6cZw" resolve="JoinPointMatch" />
      </node>
    </node>
    <node concept="13hLZK" id="4B5aqq3QFqA" role="13h7CW">
      <node concept="3clFbS" id="4B5aqq3QFqB" role="2VODD2" />
    </node>
  </node>
  <node concept="13h7C7" id="4B5aqq44z$7">
    <property role="3GE5qa" value="base.data.boolean" />
    <ref role="13h7C2" to="aia3:4B5aqq3QoID" resolve="LaboratoryTestHasPreviouslyBeenPerformed" />
    <node concept="13hLZK" id="4B5aqq44z$8" role="13h7CW">
      <node concept="3clFbS" id="4B5aqq44z$9" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="4B5aqq44z$i" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="4B5aqq44z$j" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq44z$o" role="3clF47">
        <node concept="3clFbF" id="4B5aqq44z$t" role="3cqZAp">
          <node concept="2OqwBi" id="4B5aqq45wtp" role="3clFbG">
            <node concept="BsUDl" id="4B5aqq45wmu" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXvCxk" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="4B5aqq45wze" role="2OqNvi">
              <ref role="37wK5l" to="yobs:1mAGFBJloUl" resolve="getLaboratoryTestHasPreviouslyBeenPerformedAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="4B5aqq44z$p" role="3clF45" />
    </node>
    <node concept="13i0hz" id="4B5aqq44z$u" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="4B5aqq44z$v" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq44z$$" role="3clF47">
        <node concept="3clFbF" id="6LTgXmMo1QZ" role="3cqZAp">
          <node concept="2OqwBi" id="6LTgXmMo1R1" role="3clFbG">
            <node concept="BsUDl" id="6LTgXmMo1R2" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXvCxk" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6LTgXmMo1R3" role="2OqNvi">
              <ref role="37wK5l" to="yobs:1mAGFBJloYV" resolve="getLaboratoryTestHasPreviouslyBeenPerformedDescription" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="4B5aqq44z$_" role="3clF45" />
    </node>
    <node concept="13i0hz" id="4B5aqq44z$E" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getGenerationAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:4QUW3efxqUv" resolve="getGenerationAlias" />
      <node concept="3Tm1VV" id="4B5aqq44z$F" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq44z$K" role="3clF47">
        <node concept="3clFbF" id="4B5aqq44z$P" role="3cqZAp">
          <node concept="2OqwBi" id="4B5aqq45wIt" role="3clFbG">
            <node concept="BsUDl" id="4B5aqq45wJB" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXvC$s" resolve="getGenerationTranslations" />
            </node>
            <node concept="liA8E" id="4B5aqq45wIv" role="2OqNvi">
              <ref role="37wK5l" to="yobs:1mAGFBJloUl" resolve="getLaboratoryTestHasPreviouslyBeenPerformedAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="4B5aqq44z$L" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmMo1Ct" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getAliasWithSpecimenType" />
      <ref role="13i0hy" node="6LTgXmMlgN$" resolve="getAliasWithSpecimenType" />
      <node concept="3Tm1VV" id="6LTgXmMo1Cu" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmMo1CA" role="3clF47">
        <node concept="3clFbF" id="6LTgXmMo1CF" role="3cqZAp">
          <node concept="2OqwBi" id="6LTgXmMo1Nr" role="3clFbG">
            <node concept="BsUDl" id="6LTgXmMo1Ns" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXvCxk" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6LTgXmMo1Nt" role="2OqNvi">
              <ref role="37wK5l" to="yobs:4B5aqq44_aC" resolve="getLaboratoryTestHasPreviouslyBeenPerformedOnSpecimenAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmMo1CB" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmMo1CG" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getDescriptionWithSpecimenType" />
      <ref role="13i0hy" node="6LTgXmMlq1k" resolve="getDescriptionWithSpecimenType" />
      <node concept="3Tm1VV" id="6LTgXmMo1CH" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmMo1CM" role="3clF47">
        <node concept="3clFbF" id="6LTgXmMo1CR" role="3cqZAp">
          <node concept="2OqwBi" id="4B5aqq45wGl" role="3clFbG">
            <node concept="BsUDl" id="4B5aqq45wGm" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXvCxk" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="4B5aqq45wGn" role="2OqNvi">
              <ref role="37wK5l" to="yobs:4B5aqq44_aG" resolve="getLaboratoryTestHasPreviouslyBeenPerformedOnSpecimenDescription" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmMo1CN" role="3clF45" />
    </node>
    <node concept="13i0hz" id="4B5aqq4b352" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getDisplayKeywords" />
      <ref role="13i0hy" to="1yj:4B5aqq3ZFNV" resolve="getDisplayKeywords" />
      <node concept="3Tm1VV" id="4B5aqq4b353" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq4b35c" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4b3hw" role="3cqZAp">
          <node concept="BsUDl" id="4B5aqq4b3hv" role="3clFbG">
            <ref role="37wK5l" node="4B5aqq45yPw" resolve="getLocalizedKeywords" />
            <node concept="BsUDl" id="4B5aqq4b3if" role="37wK5m">
              <ref role="37wK5l" node="4V3GMfXvCxk" resolve="getDisplayTranslations" />
            </node>
          </node>
        </node>
      </node>
      <node concept="10Q1$e" id="4B5aqq4b35d" role="3clF45">
        <node concept="17QB3L" id="4B5aqq4b35e" role="10Q1$1" />
      </node>
    </node>
    <node concept="13i0hz" id="4B5aqq4b35j" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getGenerationKeywords" />
      <ref role="13i0hy" to="1yj:4B5aqq3ZFQj" resolve="getGenerationKeywords" />
      <node concept="3Tm1VV" id="4B5aqq4b35k" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq4b35t" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4b3jl" role="3cqZAp">
          <node concept="BsUDl" id="4B5aqq4b3jk" role="3clFbG">
            <ref role="37wK5l" node="4B5aqq45yPw" resolve="getLocalizedKeywords" />
            <node concept="BsUDl" id="4B5aqq4b3kc" role="37wK5m">
              <ref role="37wK5l" node="4V3GMfXvC$s" resolve="getGenerationTranslations" />
            </node>
          </node>
        </node>
      </node>
      <node concept="10Q1$e" id="4B5aqq4b35u" role="3clF45">
        <node concept="17QB3L" id="4B5aqq4b35v" role="10Q1$1" />
      </node>
    </node>
    <node concept="13i0hz" id="4B5aqq45yPw" role="13h7CS">
      <property role="TrG5h" value="getLocalizedKeywords" />
      <property role="2Ki8OM" value="true" />
      <node concept="3Tm6S6" id="4B5aqq45yRg" role="1B3o_S" />
      <node concept="10Q1$e" id="4B5aqq45zAW" role="3clF45">
        <node concept="17QB3L" id="4B5aqq45yRr" role="10Q1$1" />
      </node>
      <node concept="3clFbS" id="4B5aqq45yPz" role="3clF47">
        <node concept="3cpWs8" id="4B5aqq45y$v" role="3cqZAp">
          <node concept="3cpWsn" id="4B5aqq45y$w" role="3cpWs9">
            <property role="TrG5h" value="firstKeyword" />
            <node concept="17QB3L" id="4B5aqq45y$x" role="1tU5fm" />
            <node concept="2OqwBi" id="4B5aqq45y$y" role="33vP2m">
              <node concept="37vLTw" id="4B5aqq45z2Y" role="2Oq$k0">
                <ref role="3cqZAo" node="4B5aqq45ySn" resolve="translations" />
              </node>
              <node concept="liA8E" id="4B5aqq45y$$" role="2OqNvi">
                <ref role="37wK5l" to="yobs:4B5aqq44KKT" resolve="getLaboratoryTestHasPreviouslyBeenPerformedOnSpecimenKeywordHasPreviousRequest" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="4B5aqq45y$_" role="3cqZAp">
          <node concept="3cpWsn" id="4B5aqq45y$A" role="3cpWs9">
            <property role="TrG5h" value="secondKeyword" />
            <node concept="17QB3L" id="4B5aqq45y$B" role="1tU5fm" />
            <node concept="2OqwBi" id="4B5aqq45y$C" role="33vP2m">
              <node concept="37vLTw" id="4B5aqq45za$" role="2Oq$k0">
                <ref role="3cqZAo" node="4B5aqq45ySn" resolve="translations" />
              </node>
              <node concept="liA8E" id="4B5aqq45y$E" role="2OqNvi">
                <ref role="37wK5l" to="yobs:4B5aqq44KYi" resolve="getLaboratoryTestHasPreviouslyBeenPerformedOnSpecimenKeywordOnSpecimen" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4B5aqq45y$F" role="3cqZAp">
          <node concept="2ShNRf" id="4B5aqq45y$G" role="3clFbG">
            <node concept="3g6Rrh" id="4B5aqq45y$H" role="2ShVmc">
              <node concept="17QB3L" id="4B5aqq45y$I" role="3g7fb8" />
              <node concept="37vLTw" id="4B5aqq45y$J" role="3g7hyw">
                <ref role="3cqZAo" node="4B5aqq45y$w" resolve="firstKeyword" />
              </node>
              <node concept="37vLTw" id="4B5aqq45y$K" role="3g7hyw">
                <ref role="3cqZAo" node="4B5aqq45y$A" resolve="secondKeyword" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="4B5aqq45ySn" role="3clF46">
        <property role="TrG5h" value="translations" />
        <node concept="3uibUv" id="4B5aqq45ySm" role="1tU5fm">
          <ref role="3uigEE" to="yobs:4zMac8rUNtP" resolve="ILaboratoryTestDataTranslations" />
        </node>
      </node>
    </node>
  </node>
  <node concept="13h7C7" id="4B5aqq4buL_">
    <property role="3GE5qa" value="base.data.boolean" />
    <ref role="13h7C2" to="aia3:4B5aqq3QoIG" resolve="LaboratoryTestPreviousResultIsValid" />
    <node concept="13hLZK" id="4B5aqq4buLA" role="13h7CW">
      <node concept="3clFbS" id="4B5aqq4buLB" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="4B5aqq4buLK" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="4B5aqq4buLL" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq4buLQ" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4buX8" role="3cqZAp">
          <node concept="2OqwBi" id="4B5aqq4bv44" role="3clFbG">
            <node concept="BsUDl" id="4B5aqq4buX7" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXvCxk" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="4B5aqq4bv9T" role="2OqNvi">
              <ref role="37wK5l" to="yobs:1mAGFBJlrOq" resolve="getLaboratoryTestPreviousResultIsValidAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="4B5aqq4buLR" role="3clF45" />
    </node>
    <node concept="13i0hz" id="4B5aqq4buLW" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="4B5aqq4buLX" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq4buM2" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4buM7" role="3cqZAp">
          <node concept="2OqwBi" id="4B5aqq4bvvF" role="3clFbG">
            <node concept="BsUDl" id="4B5aqq4bvvG" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXvCxk" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="4B5aqq4bvvH" role="2OqNvi">
              <ref role="37wK5l" to="yobs:1mAGFBJlseF" resolve="getLaboratoryTestPreviousResultIsValidDescription" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="4B5aqq4buM3" role="3clF45" />
    </node>
    <node concept="13i0hz" id="4B5aqq4buM8" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getGenerationAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:4QUW3efxqUv" resolve="getGenerationAlias" />
      <node concept="3Tm1VV" id="4B5aqq4buM9" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq4buMe" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4buMj" role="3cqZAp">
          <node concept="2OqwBi" id="4B5aqq4bvxX" role="3clFbG">
            <node concept="BsUDl" id="4B5aqq4bvz7" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXvC$s" resolve="getGenerationTranslations" />
            </node>
            <node concept="liA8E" id="4B5aqq4bvxZ" role="2OqNvi">
              <ref role="37wK5l" to="yobs:1mAGFBJlrOq" resolve="getLaboratoryTestPreviousResultIsValidAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="4B5aqq4buMf" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmMo9vB" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getAliasWithSpecimenType" />
      <ref role="13i0hy" node="6LTgXmMlgN$" resolve="getAliasWithSpecimenType" />
      <node concept="3Tm1VV" id="6LTgXmMo9vC" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmMo9vK" role="3clF47">
        <node concept="3clFbF" id="6LTgXmMo9vP" role="3cqZAp">
          <node concept="2OqwBi" id="6LTgXmMo9IS" role="3clFbG">
            <node concept="BsUDl" id="6LTgXmMo9IT" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXvCxk" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6LTgXmMo9IU" role="2OqNvi">
              <ref role="37wK5l" to="yobs:4B5aqq44_aL" resolve="getLaboratoryTestPreviousResultOnSpecimenIsValidAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmMo9vL" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmMo9vQ" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getDescriptionWithSpecimenType" />
      <ref role="13i0hy" node="6LTgXmMlq1k" resolve="getDescriptionWithSpecimenType" />
      <node concept="3Tm1VV" id="6LTgXmMo9vR" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmMo9vW" role="3clF47">
        <node concept="3clFbF" id="6LTgXmMo9w1" role="3cqZAp">
          <node concept="2OqwBi" id="6LTgXmMo9Os" role="3clFbG">
            <node concept="BsUDl" id="6LTgXmMo9Ot" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXvCxk" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6LTgXmMo9Ou" role="2OqNvi">
              <ref role="37wK5l" to="yobs:4B5aqq44_aP" resolve="getLaboratoryTestPreviousResultOnSpecimenIsValidDescription" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmMo9vX" role="3clF45" />
    </node>
    <node concept="13i0hz" id="4B5aqq4buMk" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getDisplayKeywords" />
      <ref role="13i0hy" to="1yj:4B5aqq3ZFNV" resolve="getDisplayKeywords" />
      <node concept="3Tm1VV" id="4B5aqq4buMl" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq4buMu" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4bwiw" role="3cqZAp">
          <node concept="BsUDl" id="4B5aqq4bwiv" role="3clFbG">
            <ref role="37wK5l" node="4B5aqq4bv$A" resolve="getLocalizedKeywords" />
            <node concept="BsUDl" id="4B5aqq4bwjn" role="37wK5m">
              <ref role="37wK5l" node="4V3GMfXvCxk" resolve="getDisplayTranslations" />
            </node>
          </node>
        </node>
      </node>
      <node concept="10Q1$e" id="4B5aqq4buMv" role="3clF45">
        <node concept="17QB3L" id="4B5aqq4buMw" role="10Q1$1" />
      </node>
    </node>
    <node concept="13i0hz" id="4B5aqq4buM_" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getGenerationKeywords" />
      <ref role="13i0hy" to="1yj:4B5aqq3ZFQj" resolve="getGenerationKeywords" />
      <node concept="3Tm1VV" id="4B5aqq4buMA" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq4buMJ" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4bwgV" role="3cqZAp">
          <node concept="BsUDl" id="4B5aqq4bwgU" role="3clFbG">
            <ref role="37wK5l" node="4B5aqq4bv$A" resolve="getLocalizedKeywords" />
            <node concept="BsUDl" id="4B5aqq4bwhE" role="37wK5m">
              <ref role="37wK5l" node="4V3GMfXvC$s" resolve="getGenerationTranslations" />
            </node>
          </node>
        </node>
      </node>
      <node concept="10Q1$e" id="4B5aqq4buMK" role="3clF45">
        <node concept="17QB3L" id="4B5aqq4buML" role="10Q1$1" />
      </node>
    </node>
    <node concept="13i0hz" id="4B5aqq4bv$A" role="13h7CS">
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getLocalizedKeywords" />
      <node concept="3Tm6S6" id="4B5aqq4bv_P" role="1B3o_S" />
      <node concept="10Q1$e" id="4B5aqq4bv_w" role="3clF45">
        <node concept="17QB3L" id="4B5aqq4bv_s" role="10Q1$1" />
      </node>
      <node concept="3clFbS" id="4B5aqq4bv$D" role="3clF47">
        <node concept="3cpWs8" id="4B5aqq4bvAD" role="3cqZAp">
          <node concept="3cpWsn" id="4B5aqq4bvAG" role="3cpWs9">
            <property role="TrG5h" value="firstKeyword" />
            <node concept="17QB3L" id="4B5aqq4bvAC" role="1tU5fm" />
            <node concept="2OqwBi" id="4B5aqq4bvJe" role="33vP2m">
              <node concept="37vLTw" id="4B5aqq4bvCr" role="2Oq$k0">
                <ref role="3cqZAo" node="4B5aqq4bvBv" resolve="translations" />
              </node>
              <node concept="liA8E" id="4B5aqq4bvOW" role="2OqNvi">
                <ref role="37wK5l" to="yobs:4B5aqq44L4u" resolve="getLaboratoryTestPreviousResultOnSpecimenIsValidKeywordPreviousResultIsValid" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="4B5aqq4bvQb" role="3cqZAp">
          <node concept="3cpWsn" id="4B5aqq4bvQe" role="3cpWs9">
            <property role="TrG5h" value="secondKeyword" />
            <node concept="17QB3L" id="4B5aqq4bvQ9" role="1tU5fm" />
            <node concept="2OqwBi" id="4B5aqq4bvRc" role="33vP2m">
              <node concept="37vLTw" id="4B5aqq4bvRd" role="2Oq$k0">
                <ref role="3cqZAo" node="4B5aqq4bvBv" resolve="translations" />
              </node>
              <node concept="liA8E" id="4B5aqq4bvRe" role="2OqNvi">
                <ref role="37wK5l" to="yobs:4B5aqq44LrP" resolve="getLaboratoryTestPreviousResultOnSpecimenIsValidKeywordOnSpecimen" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4B5aqq4bvTQ" role="3cqZAp">
          <node concept="2ShNRf" id="4B5aqq4bvTM" role="3clFbG">
            <node concept="3g6Rrh" id="4B5aqq4bw8l" role="2ShVmc">
              <node concept="17QB3L" id="4B5aqq4bvZU" role="3g7fb8" />
              <node concept="37vLTw" id="4B5aqq4bwfz" role="3g7hyw">
                <ref role="3cqZAo" node="4B5aqq4bvAG" resolve="firstKeyword" />
              </node>
              <node concept="37vLTw" id="4B5aqq4bwga" role="3g7hyw">
                <ref role="3cqZAo" node="4B5aqq4bvQe" resolve="secondKeyword" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="4B5aqq4bvBv" role="3clF46">
        <property role="TrG5h" value="translations" />
        <node concept="3uibUv" id="4B5aqq4bvBu" role="1tU5fm">
          <ref role="3uigEE" to="yobs:4zMac8rUNtP" resolve="ILaboratoryTestDataTranslations" />
        </node>
      </node>
    </node>
  </node>
  <node concept="13h7C7" id="4B5aqq4bM6B">
    <property role="3GE5qa" value="base.data.boolean" />
    <ref role="13h7C2" to="aia3:4B5aqq3QoIJ" resolve="LaboratoryTestResultIsValid" />
    <node concept="13hLZK" id="4B5aqq4bM6C" role="13h7CW">
      <node concept="3clFbS" id="4B5aqq4bM6D" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="4B5aqq4bM6M" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="4B5aqq4bM6N" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq4bM6S" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4bMia" role="3cqZAp">
          <node concept="2OqwBi" id="4B5aqq4bMoY" role="3clFbG">
            <node concept="BsUDl" id="4B5aqq4bMi9" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXvCxk" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="4B5aqq4bMuN" role="2OqNvi">
              <ref role="37wK5l" to="yobs:17XAtu89FWp" resolve="getLaboratoryTestResultIsValidAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="4B5aqq4bM6T" role="3clF45" />
    </node>
    <node concept="13i0hz" id="4B5aqq4bM6Y" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="4B5aqq4bM6Z" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq4bM74" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4bMvt" role="3cqZAp">
          <node concept="2OqwBi" id="PDjyzkxYnB" role="3clFbG">
            <node concept="BsUDl" id="PDjyzkxYnC" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXvCxk" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="PDjyzkxYnD" role="2OqNvi">
              <ref role="37wK5l" to="yobs:17XAtu89GFx" resolve="getLaboratoryTestResultIsValidDescription" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="4B5aqq4bM75" role="3clF45" />
    </node>
    <node concept="13i0hz" id="4B5aqq4bM7a" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getGenerationAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:4QUW3efxqUv" resolve="getGenerationAlias" />
      <node concept="3Tm1VV" id="4B5aqq4bM7b" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq4bM7g" role="3clF47">
        <node concept="3clFbF" id="PDjyzkxYpR" role="3cqZAp">
          <node concept="2OqwBi" id="PDjyzkxYpT" role="3clFbG">
            <node concept="BsUDl" id="PDjyzkxYra" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXvC$s" resolve="getGenerationTranslations" />
            </node>
            <node concept="liA8E" id="PDjyzkxYpV" role="2OqNvi">
              <ref role="37wK5l" to="yobs:17XAtu89FWp" resolve="getLaboratoryTestResultIsValidAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="4B5aqq4bM7h" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmMohz3" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getAliasWithSpecimenType" />
      <ref role="13i0hy" node="6LTgXmMlgN$" resolve="getAliasWithSpecimenType" />
      <node concept="3Tm1VV" id="6LTgXmMohz4" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmMohzc" role="3clF47">
        <node concept="3clFbF" id="6LTgXmMohzh" role="3cqZAp">
          <node concept="2OqwBi" id="6LTgXmMohM9" role="3clFbG">
            <node concept="BsUDl" id="6LTgXmMohMa" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXvCxk" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6LTgXmMohMb" role="2OqNvi">
              <ref role="37wK5l" to="yobs:4B5aqq44_am" resolve="getLaboratoryTestResultOnSpecimenTypeIsValidAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmMohzd" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmMohzi" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getDescriptionWithSpecimenType" />
      <ref role="13i0hy" node="6LTgXmMlq1k" resolve="getDescriptionWithSpecimenType" />
      <node concept="3Tm1VV" id="6LTgXmMohzj" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmMohzo" role="3clF47">
        <node concept="3clFbF" id="6LTgXmMohzt" role="3cqZAp">
          <node concept="2OqwBi" id="6LTgXmMohRH" role="3clFbG">
            <node concept="BsUDl" id="6LTgXmMohRI" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXvCxk" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6LTgXmMohRJ" role="2OqNvi">
              <ref role="37wK5l" to="yobs:4B5aqq44_aq" resolve="getLaboratoryTestResultOnSpecimenTypeIsValidDescription" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmMohzp" role="3clF45" />
    </node>
    <node concept="13i0hz" id="4B5aqq4bM7m" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getDisplayKeywords" />
      <ref role="13i0hy" to="1yj:4B5aqq3ZFNV" resolve="getDisplayKeywords" />
      <node concept="3Tm1VV" id="4B5aqq4bM7n" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq4bM7w" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4bUW7" role="3cqZAp">
          <node concept="BsUDl" id="4B5aqq4bUW6" role="3clFbG">
            <ref role="37wK5l" node="4B5aqq4bMDX" resolve="getLocalizedKeywords" />
            <node concept="BsUDl" id="4B5aqq4bUWY" role="37wK5m">
              <ref role="37wK5l" node="4V3GMfXvCxk" resolve="getDisplayTranslations" />
            </node>
          </node>
        </node>
      </node>
      <node concept="10Q1$e" id="4B5aqq4bM7x" role="3clF45">
        <node concept="17QB3L" id="4B5aqq4bM7y" role="10Q1$1" />
      </node>
    </node>
    <node concept="13i0hz" id="4B5aqq4bM7B" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getGenerationKeywords" />
      <ref role="13i0hy" to="1yj:4B5aqq3ZFQj" resolve="getGenerationKeywords" />
      <node concept="3Tm1VV" id="4B5aqq4bM7C" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq4bM7L" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4bM7R" role="3cqZAp">
          <node concept="BsUDl" id="4B5aqq4bUUr" role="3clFbG">
            <ref role="37wK5l" node="4B5aqq4bMDX" resolve="getLocalizedKeywords" />
            <node concept="BsUDl" id="4B5aqq4bUV9" role="37wK5m">
              <ref role="37wK5l" node="4V3GMfXvC$s" resolve="getGenerationTranslations" />
            </node>
          </node>
        </node>
      </node>
      <node concept="10Q1$e" id="4B5aqq4bM7M" role="3clF45">
        <node concept="17QB3L" id="4B5aqq4bM7N" role="10Q1$1" />
      </node>
    </node>
    <node concept="13i0hz" id="4B5aqq4bMDX" role="13h7CS">
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getLocalizedKeywords" />
      <node concept="3Tm6S6" id="4B5aqq4bMER" role="1B3o_S" />
      <node concept="10Q1$e" id="4B5aqq4bMF6" role="3clF45">
        <node concept="17QB3L" id="4B5aqq4bMF2" role="10Q1$1" />
      </node>
      <node concept="3clFbS" id="4B5aqq4bME0" role="3clF47">
        <node concept="3cpWs8" id="4B5aqq4bMGm" role="3cqZAp">
          <node concept="3cpWsn" id="4B5aqq4bMGp" role="3cpWs9">
            <property role="TrG5h" value="firstKeyword" />
            <node concept="17QB3L" id="4B5aqq4bMGl" role="1tU5fm" />
            <node concept="2OqwBi" id="4B5aqq4bMNP" role="33vP2m">
              <node concept="37vLTw" id="4B5aqq4bMH2" role="2Oq$k0">
                <ref role="3cqZAo" node="4B5aqq4bMF$" resolve="translations" />
              </node>
              <node concept="liA8E" id="4B5aqq4bMTz" role="2OqNvi">
                <ref role="37wK5l" to="yobs:4B5aqq44IWm" resolve="getLaboratoryTestResultOnSpecimenTypeIsValidKeywordHasValidResult" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="4B5aqq4bMUE" role="3cqZAp">
          <node concept="3cpWsn" id="4B5aqq4bMUH" role="3cpWs9">
            <property role="TrG5h" value="secondKeyword" />
            <node concept="17QB3L" id="4B5aqq4bMUC" role="1tU5fm" />
            <node concept="2OqwBi" id="4B5aqq4bMVF" role="33vP2m">
              <node concept="37vLTw" id="4B5aqq4bMVG" role="2Oq$k0">
                <ref role="3cqZAo" node="4B5aqq4bMF$" resolve="translations" />
              </node>
              <node concept="liA8E" id="4B5aqq4bMVH" role="2OqNvi">
                <ref role="37wK5l" to="yobs:4B5aqq44IKt" resolve="getLaboratoryTestResultOnSpecimenTypeIsValidKeywordOnSpecimen" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4B5aqq4bMYd" role="3cqZAp">
          <node concept="2ShNRf" id="4B5aqq4bMY9" role="3clFbG">
            <node concept="3g6Rrh" id="4B5aqq4bNcG" role="2ShVmc">
              <node concept="17QB3L" id="4B5aqq4bN4h" role="3g7fb8" />
              <node concept="37vLTw" id="4B5aqq4bNjU" role="3g7hyw">
                <ref role="3cqZAo" node="4B5aqq4bMGp" resolve="firstKeyword" />
              </node>
              <node concept="37vLTw" id="4B5aqq4bNl1" role="3g7hyw">
                <ref role="3cqZAo" node="4B5aqq4bMUH" resolve="secondKeyword" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="4B5aqq4bMF$" role="3clF46">
        <property role="TrG5h" value="translations" />
        <node concept="3uibUv" id="4B5aqq4bMFz" role="1tU5fm">
          <ref role="3uigEE" to="yobs:4zMac8rUNtP" resolve="ILaboratoryTestDataTranslations" />
        </node>
      </node>
    </node>
  </node>
  <node concept="13h7C7" id="4B5aqq4c$S7">
    <property role="3GE5qa" value="base.data.value" />
    <ref role="13h7C2" to="aia3:4B5aqq4cwJ4" resolve="LaboratoryTestTimeSinceLastRequest" />
    <node concept="13hLZK" id="4B5aqq4c$S8" role="13h7CW">
      <node concept="3clFbS" id="4B5aqq4c$S9" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="4B5aqq4c$Si" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="4B5aqq4c$Sj" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq4c$So" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4c_3E" role="3cqZAp">
          <node concept="2OqwBi" id="4B5aqq4c_aA" role="3clFbG">
            <node concept="BsUDl" id="4B5aqq4c_3D" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXvCxk" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="4B5aqq4c_gr" role="2OqNvi">
              <ref role="37wK5l" to="yobs:17XAtu82mUY" resolve="getLaboratoryTestTimeSinceLastPerformedAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="4B5aqq4c$Sp" role="3clF45" />
    </node>
    <node concept="13i0hz" id="4B5aqq4c$Su" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="4B5aqq4c$Sv" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq4c$S$" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4c$SD" role="3cqZAp">
          <node concept="2OqwBi" id="4B5aqq4c_h5" role="3clFbG">
            <node concept="BsUDl" id="4B5aqq4c_h6" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXvCxk" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="4B5aqq4c_h7" role="2OqNvi">
              <ref role="37wK5l" to="yobs:17XAtu82nyP" resolve="getLaboratoryTestTimeSinceLastPerformedDescription" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="4B5aqq4c$S_" role="3clF45" />
    </node>
    <node concept="13i0hz" id="4B5aqq4c$SE" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getGenerationAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:4QUW3efxqUv" resolve="getGenerationAlias" />
      <node concept="3Tm1VV" id="4B5aqq4c$SF" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq4c$SK" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4c$SP" role="3cqZAp">
          <node concept="2OqwBi" id="4B5aqq4c_iW" role="3clFbG">
            <node concept="BsUDl" id="4B5aqq4c_k6" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXvC$s" resolve="getGenerationTranslations" />
            </node>
            <node concept="liA8E" id="4B5aqq4c_iY" role="2OqNvi">
              <ref role="37wK5l" to="yobs:17XAtu82mUY" resolve="getLaboratoryTestTimeSinceLastPerformedAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="4B5aqq4c$SL" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmMopA6" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getAliasWithSpecimenType" />
      <ref role="13i0hy" node="6LTgXmMlgN$" resolve="getAliasWithSpecimenType" />
      <node concept="3Tm1VV" id="6LTgXmMopA7" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmMopAf" role="3clF47">
        <node concept="3clFbF" id="6LTgXmMopAk" role="3cqZAp">
          <node concept="2OqwBi" id="6LTgXmMopL4" role="3clFbG">
            <node concept="BsUDl" id="6LTgXmMopL5" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXvCxk" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6LTgXmMopL6" role="2OqNvi">
              <ref role="37wK5l" to="yobs:4B5aqq44_av" resolve="getLaboratoryTestTimeSinceLastPerformedOnSpecimenAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmMopAg" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmMopAl" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getDescriptionWithSpecimenType" />
      <ref role="13i0hy" node="6LTgXmMlq1k" resolve="getDescriptionWithSpecimenType" />
      <node concept="3Tm1VV" id="6LTgXmMopAm" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmMopAr" role="3clF47">
        <node concept="3clFbF" id="6LTgXmMopAw" role="3cqZAp">
          <node concept="2OqwBi" id="6LTgXmMopQA" role="3clFbG">
            <node concept="BsUDl" id="6LTgXmMopQB" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXvCxk" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6LTgXmMopQC" role="2OqNvi">
              <ref role="37wK5l" to="yobs:4B5aqq44_az" resolve="getLaboratoryTestTimeSinceLastPerformedOnSpecimenDescription" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmMopAs" role="3clF45" />
    </node>
    <node concept="13i0hz" id="4B5aqq4c$SQ" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getDisplayKeywords" />
      <ref role="13i0hy" to="1yj:4B5aqq3ZFNV" resolve="getDisplayKeywords" />
      <node concept="3Tm1VV" id="4B5aqq4c$SR" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq4c$T0" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4c$T6" role="3cqZAp">
          <node concept="BsUDl" id="4B5aqq4cHjv" role="3clFbG">
            <ref role="37wK5l" node="4B5aqq4c_kO" resolve="getLocalizedKeywords" />
            <node concept="BsUDl" id="4B5aqq4cHkd" role="37wK5m">
              <ref role="37wK5l" node="4V3GMfXvCxk" resolve="getDisplayTranslations" />
            </node>
          </node>
        </node>
      </node>
      <node concept="10Q1$e" id="4B5aqq4c$T1" role="3clF45">
        <node concept="17QB3L" id="4B5aqq4c$T2" role="10Q1$1" />
      </node>
    </node>
    <node concept="13i0hz" id="4B5aqq4c$T7" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getGenerationKeywords" />
      <ref role="13i0hy" to="1yj:4B5aqq3ZFQj" resolve="getGenerationKeywords" />
      <node concept="3Tm1VV" id="4B5aqq4c$T8" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq4c$Th" role="3clF47">
        <node concept="3clFbF" id="4B5aqq4c$Tn" role="3cqZAp">
          <node concept="BsUDl" id="4B5aqq4cHlw" role="3clFbG">
            <ref role="37wK5l" node="4B5aqq4c_kO" resolve="getLocalizedKeywords" />
            <node concept="BsUDl" id="4B5aqq4cHmm" role="37wK5m">
              <ref role="37wK5l" node="4V3GMfXvC$s" resolve="getGenerationTranslations" />
            </node>
          </node>
        </node>
      </node>
      <node concept="10Q1$e" id="4B5aqq4c$Ti" role="3clF45">
        <node concept="17QB3L" id="4B5aqq4c$Tj" role="10Q1$1" />
      </node>
    </node>
    <node concept="13i0hz" id="4B5aqq4c_kO" role="13h7CS">
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getLocalizedKeywords" />
      <node concept="3Tm6S6" id="4B5aqq4c_m7" role="1B3o_S" />
      <node concept="10Q1$e" id="4B5aqq4c_lM" role="3clF45">
        <node concept="17QB3L" id="4B5aqq4c_lI" role="10Q1$1" />
      </node>
      <node concept="3clFbS" id="4B5aqq4c_kR" role="3clF47">
        <node concept="3cpWs8" id="4B5aqq4c_r0" role="3cqZAp">
          <node concept="3cpWsn" id="4B5aqq4c_r3" role="3cpWs9">
            <property role="TrG5h" value="firstKeywords" />
            <node concept="17QB3L" id="4B5aqq4c_qY" role="1tU5fm" />
            <node concept="2OqwBi" id="4B5aqq4c_yz" role="33vP2m">
              <node concept="37vLTw" id="4B5aqq4c_rK" role="2Oq$k0">
                <ref role="3cqZAo" node="4B5aqq4c_mz" resolve="translations" />
              </node>
              <node concept="liA8E" id="4B5aqq4c_Ch" role="2OqNvi">
                <ref role="37wK5l" to="yobs:4B5aqq44J8B" resolve="getLaboratoryTestTimeSinceLastPerformedOnSpecimenKeywordTimeSinceLastRequested" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="4B5aqq4c_CY" role="3cqZAp">
          <node concept="3cpWsn" id="4B5aqq4c_D1" role="3cpWs9">
            <property role="TrG5h" value="secondKeyword" />
            <node concept="17QB3L" id="4B5aqq4c_D2" role="1tU5fm" />
            <node concept="2OqwBi" id="4B5aqq4c_D3" role="33vP2m">
              <node concept="37vLTw" id="4B5aqq4c_D4" role="2Oq$k0">
                <ref role="3cqZAo" node="4B5aqq4c_mz" resolve="translations" />
              </node>
              <node concept="liA8E" id="4B5aqq4c_D5" role="2OqNvi">
                <ref role="37wK5l" to="yobs:4B5aqq44Jlg" resolve="getLaboratoryTestTimeSinceLastPerformedOnSpecimenKeywordOnSpecimen" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4B5aqq4c_nm" role="3cqZAp">
          <node concept="2ShNRf" id="4B5aqq4c_nk" role="3clFbG">
            <node concept="3g6Rrh" id="4B5aqq4c_QD" role="2ShVmc">
              <node concept="17QB3L" id="4B5aqq4c_Ie" role="3g7fb8" />
              <node concept="37vLTw" id="4B5aqq4c_Rg" role="3g7hyw">
                <ref role="3cqZAo" node="4B5aqq4c_r3" resolve="firstKeywords" />
              </node>
              <node concept="37vLTw" id="4B5aqq4c_RR" role="3g7hyw">
                <ref role="3cqZAo" node="4B5aqq4c_D1" resolve="secondKeyword" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="4B5aqq4c_mz" role="3clF46">
        <property role="TrG5h" value="translations" />
        <node concept="3uibUv" id="4B5aqq4c_my" role="1tU5fm">
          <ref role="3uigEE" to="yobs:4zMac8rUNtP" resolve="ILaboratoryTestDataTranslations" />
        </node>
      </node>
    </node>
  </node>
  <node concept="13h7C7" id="PDjyzjQpqU">
    <property role="3GE5qa" value="base.data.boolean" />
    <ref role="13h7C2" to="aia3:PDjyzjQppD" resolve="LaboratoryTestHasBeenPerformed" />
    <node concept="13i0hz" id="PDjyzjQqSC" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="PDjyzjQqSD" role="1B3o_S" />
      <node concept="3clFbS" id="PDjyzjQqSE" role="3clF47">
        <node concept="3clFbF" id="PDjyzjQqSF" role="3cqZAp">
          <node concept="2OqwBi" id="PDjyzjQqSG" role="3clFbG">
            <node concept="BsUDl" id="PDjyzjQqSH" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXvCxk" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="PDjyzjQqSI" role="2OqNvi">
              <ref role="37wK5l" to="yobs:PDjyzjQrlC" resolve="getLaboratoryTestHasBeenPerformedAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="PDjyzjQqSJ" role="3clF45" />
    </node>
    <node concept="13i0hz" id="PDjyzjQqSK" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="PDjyzjQqSL" role="1B3o_S" />
      <node concept="3clFbS" id="PDjyzjQqSM" role="3clF47">
        <node concept="3clFbF" id="PDjyzjQqSN" role="3cqZAp">
          <node concept="2OqwBi" id="PDjyzjQqSO" role="3clFbG">
            <node concept="BsUDl" id="PDjyzjQqSP" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXvCxk" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="PDjyzjQqSQ" role="2OqNvi">
              <ref role="37wK5l" to="yobs:PDjyzjQrDr" resolve="getLaboratoryTestHasBeenPerformedDescription" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="PDjyzjQqSR" role="3clF45" />
    </node>
    <node concept="13i0hz" id="PDjyzjQqSS" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getGenerationAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:4QUW3efxqUv" resolve="getGenerationAlias" />
      <node concept="3Tm1VV" id="PDjyzjQqST" role="1B3o_S" />
      <node concept="3clFbS" id="PDjyzjQqSU" role="3clF47">
        <node concept="3clFbF" id="PDjyzjQqSV" role="3cqZAp">
          <node concept="2OqwBi" id="PDjyzjQqSW" role="3clFbG">
            <node concept="BsUDl" id="PDjyzjQqSX" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXvC$s" resolve="getGenerationTranslations" />
            </node>
            <node concept="liA8E" id="PDjyzjQqSY" role="2OqNvi">
              <ref role="37wK5l" to="yobs:PDjyzjQrlC" resolve="getLaboratoryTestHasBeenPerformedAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="PDjyzjQqSZ" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmMlJc_" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getAliasWithSpecimenType" />
      <ref role="13i0hy" node="6LTgXmMlgN$" resolve="getAliasWithSpecimenType" />
      <node concept="3Tm1VV" id="6LTgXmMlJcA" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmMlJcI" role="3clF47">
        <node concept="3clFbF" id="6LTgXmMlJcN" role="3cqZAp">
          <node concept="2OqwBi" id="6LTgXmMlJrM" role="3clFbG">
            <node concept="BsUDl" id="6LTgXmMlJrN" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXvCxk" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6LTgXmMlJrO" role="2OqNvi">
              <ref role="37wK5l" to="yobs:PDjyzjQs0b" resolve="getLaboratoryTestHasBeenPerformedOnSpecimenAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmMlJcJ" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmMlJcO" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getDescriptionWithSpecimenType" />
      <ref role="13i0hy" node="6LTgXmMlq1k" resolve="getDescriptionWithSpecimenType" />
      <node concept="3Tm1VV" id="6LTgXmMlJcP" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmMlJcU" role="3clF47">
        <node concept="3clFbF" id="6LTgXmMlJcZ" role="3cqZAp">
          <node concept="2OqwBi" id="6LTgXmMlJxu" role="3clFbG">
            <node concept="BsUDl" id="6LTgXmMlJxv" role="2Oq$k0">
              <ref role="37wK5l" node="4V3GMfXvCxk" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6LTgXmMlJxw" role="2OqNvi">
              <ref role="37wK5l" to="yobs:PDjyzjQzjJ" resolve="getLaboratoryTestHasBeenPerformedOnSpecimenDescription" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmMlJcV" role="3clF45" />
    </node>
    <node concept="13i0hz" id="PDjyzjQqT0" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getDisplayKeywords" />
      <ref role="13i0hy" to="1yj:4B5aqq3ZFNV" resolve="getDisplayKeywords" />
      <node concept="3Tm1VV" id="PDjyzjQqT1" role="1B3o_S" />
      <node concept="3clFbS" id="PDjyzjQqT2" role="3clF47">
        <node concept="3clFbF" id="PDjyzjQqT3" role="3cqZAp">
          <node concept="BsUDl" id="PDjyzjQqT4" role="3clFbG">
            <ref role="37wK5l" node="PDjyzjQqTg" resolve="getLocalizedKeywords" />
            <node concept="BsUDl" id="PDjyzjQqT5" role="37wK5m">
              <ref role="37wK5l" node="4V3GMfXvCxk" resolve="getDisplayTranslations" />
            </node>
          </node>
        </node>
      </node>
      <node concept="10Q1$e" id="PDjyzjQqT6" role="3clF45">
        <node concept="17QB3L" id="PDjyzjQqT7" role="10Q1$1" />
      </node>
    </node>
    <node concept="13i0hz" id="PDjyzjQqT8" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getGenerationKeywords" />
      <ref role="13i0hy" to="1yj:4B5aqq3ZFQj" resolve="getGenerationKeywords" />
      <node concept="3Tm1VV" id="PDjyzjQqT9" role="1B3o_S" />
      <node concept="3clFbS" id="PDjyzjQqTa" role="3clF47">
        <node concept="3clFbF" id="PDjyzjQqTb" role="3cqZAp">
          <node concept="BsUDl" id="PDjyzjQqTc" role="3clFbG">
            <ref role="37wK5l" node="PDjyzjQqTg" resolve="getLocalizedKeywords" />
            <node concept="BsUDl" id="PDjyzjQqTd" role="37wK5m">
              <ref role="37wK5l" node="4V3GMfXvC$s" resolve="getGenerationTranslations" />
            </node>
          </node>
        </node>
      </node>
      <node concept="10Q1$e" id="PDjyzjQqTe" role="3clF45">
        <node concept="17QB3L" id="PDjyzjQqTf" role="10Q1$1" />
      </node>
    </node>
    <node concept="13i0hz" id="PDjyzjQqTg" role="13h7CS">
      <property role="TrG5h" value="getLocalizedKeywords" />
      <property role="2Ki8OM" value="true" />
      <node concept="3Tm6S6" id="PDjyzjQqTh" role="1B3o_S" />
      <node concept="10Q1$e" id="PDjyzjQqTi" role="3clF45">
        <node concept="17QB3L" id="PDjyzjQqTj" role="10Q1$1" />
      </node>
      <node concept="3clFbS" id="PDjyzjQqTk" role="3clF47">
        <node concept="3cpWs8" id="PDjyzjQqTl" role="3cqZAp">
          <node concept="3cpWsn" id="PDjyzjQqTm" role="3cpWs9">
            <property role="TrG5h" value="firstKeyword" />
            <node concept="17QB3L" id="PDjyzjQqTn" role="1tU5fm" />
            <node concept="2OqwBi" id="PDjyzjQqTo" role="33vP2m">
              <node concept="37vLTw" id="PDjyzjQqTp" role="2Oq$k0">
                <ref role="3cqZAo" node="PDjyzjQqTB" resolve="translations" />
              </node>
              <node concept="liA8E" id="PDjyzjQqTq" role="2OqNvi">
                <ref role="37wK5l" to="yobs:PDjyzjQs0f" resolve="getLaboratoryTestHasBeenPerformedOnSpecimenKeywordHasBeenPerformed" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="PDjyzjQqTr" role="3cqZAp">
          <node concept="3cpWsn" id="PDjyzjQqTs" role="3cpWs9">
            <property role="TrG5h" value="secondKeyword" />
            <node concept="17QB3L" id="PDjyzjQqTt" role="1tU5fm" />
            <node concept="2OqwBi" id="PDjyzjQqTu" role="33vP2m">
              <node concept="37vLTw" id="PDjyzjQqTv" role="2Oq$k0">
                <ref role="3cqZAo" node="PDjyzjQqTB" resolve="translations" />
              </node>
              <node concept="liA8E" id="PDjyzjQqTw" role="2OqNvi">
                <ref role="37wK5l" to="yobs:PDjyzjQsEj" resolve="getLaboratoryTestHasBeenPerformedOnSpecimenKeywordOnSpecimen" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="PDjyzjQqTx" role="3cqZAp">
          <node concept="2ShNRf" id="PDjyzjQqTy" role="3clFbG">
            <node concept="3g6Rrh" id="PDjyzjQqTz" role="2ShVmc">
              <node concept="17QB3L" id="PDjyzjQqT$" role="3g7fb8" />
              <node concept="37vLTw" id="PDjyzjQqT_" role="3g7hyw">
                <ref role="3cqZAo" node="PDjyzjQqTm" resolve="firstKeyword" />
              </node>
              <node concept="37vLTw" id="PDjyzjQqTA" role="3g7hyw">
                <ref role="3cqZAo" node="PDjyzjQqTs" resolve="secondKeyword" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="PDjyzjQqTB" role="3clF46">
        <property role="TrG5h" value="translations" />
        <node concept="3uibUv" id="PDjyzjQqTC" role="1tU5fm">
          <ref role="3uigEE" to="yobs:4zMac8rUNtP" resolve="ILaboratoryTestDataTranslations" />
        </node>
      </node>
    </node>
    <node concept="13hLZK" id="PDjyzjQpqV" role="13h7CW">
      <node concept="3clFbS" id="PDjyzjQpqW" role="2VODD2" />
    </node>
  </node>
</model>

