<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:f0b14da8-c905-46ee-a15f-c9b65635a71b(no.uio.mLab.decisions.data.laboratoryTest.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="11" />
    <use id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions" version="4" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="i7z6" ref="r:515ff38d-92f9-43f3-aa4c-c5bec29efe22(no.uio.mLab.decisions.core.editor)" />
    <import index="aia3" ref="r:856e974b-6e42-49cc-99cd-c3adb4161c21(no.uio.mLab.decisions.data.laboratoryTest.structure)" implicit="true" />
    <import index="uubs" ref="r:481ae0fe-9100-4bb8-9488-731d6cfeba53(no.uio.mLab.shared.editor)" implicit="true" />
    <import index="hlwg" ref="r:d6374b20-dfe3-4cab-8f8b-b4fb4ea31050(no.uio.mLab.decisions.data.laboratoryTest.behavior)" implicit="true" />
    <import index="1yj" ref="r:0c887d67-81ab-420f-9a35-a9f6bf96c975(no.uio.mLab.shared.behavior)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="2000375450116454183" name="jetbrains.mps.lang.editor.structure.ISubstituteMenu" flags="ng" index="22mbnS">
        <child id="414384289274416996" name="parts" index="3ft7WO" />
      </concept>
      <concept id="540685334799947899" name="jetbrains.mps.lang.editor.structure.SubstituteMenuVariableDeclaration" flags="ig" index="23wRS9">
        <child id="540685334802085316" name="initializerBlock" index="23DdeQ" />
      </concept>
      <concept id="540685334799947902" name="jetbrains.mps.lang.editor.structure.SubstituteMenuVariableReference" flags="ng" index="23wRSc" />
      <concept id="540685334802085318" name="jetbrains.mps.lang.editor.structure.QueryFunction_SubstituteMenuVariable_Initializer" flags="ig" index="23DdeO" />
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi" />
      <concept id="1106270549637" name="jetbrains.mps.lang.editor.structure.CellLayout_Horizontal" flags="nn" index="2iRfu4" />
      <concept id="1142886221719" name="jetbrains.mps.lang.editor.structure.QueryFunction_NodeCondition" flags="in" index="pkWqt" />
      <concept id="1142886811589" name="jetbrains.mps.lang.editor.structure.ConceptFunctionParameter_node" flags="nn" index="pncrf" />
      <concept id="1177327570013" name="jetbrains.mps.lang.editor.structure.QueryFunction_SubstituteMenu_Substitute" flags="in" index="ucgPf" />
      <concept id="8478191136883534237" name="jetbrains.mps.lang.editor.structure.IExtensibleSubstituteMenuPart" flags="ng" index="upBLQ">
        <child id="8478191136883534238" name="features" index="upBLP" />
      </concept>
      <concept id="1177335944525" name="jetbrains.mps.lang.editor.structure.QueryFunction_SubstituteMenu_SubstituteString" flags="in" index="uGdhv" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="8383079901754291618" name="jetbrains.mps.lang.editor.structure.CellModel_NextEditor" flags="ng" index="B$lHz" />
      <concept id="1630016958697344083" name="jetbrains.mps.lang.editor.structure.IMenu_Concept" flags="ng" index="2ZABuq">
        <reference id="6591946374543067572" name="conceptDeclaration" index="aqKnT" />
      </concept>
      <concept id="8998492695583125082" name="jetbrains.mps.lang.editor.structure.SubstituteFeature_MatchingText" flags="ng" index="16NfWO">
        <child id="8998492695583129244" name="query" index="16NeZM" />
      </concept>
      <concept id="8998492695583129971" name="jetbrains.mps.lang.editor.structure.SubstituteFeature_DescriptionText" flags="ng" index="16NL0t">
        <child id="8998492695583129972" name="query" index="16NL0q" />
      </concept>
      <concept id="7342352913006985483" name="jetbrains.mps.lang.editor.structure.SubstituteMenuPart_Action" flags="ng" index="3eGOop">
        <child id="8612453216082699922" name="substituteHandler" index="3aKz83" />
      </concept>
      <concept id="414384289274418283" name="jetbrains.mps.lang.editor.structure.SubstituteMenuPart_Group" flags="ng" index="3ft6gV">
        <child id="540685334802084769" name="variables" index="23Ddnj" />
        <child id="414384289274424751" name="parts" index="3ft5RZ" />
      </concept>
      <concept id="1381004262292414836" name="jetbrains.mps.lang.editor.structure.ICellStyle" flags="ng" index="1k5N5V">
        <reference id="1381004262292426837" name="parentStyleClass" index="1k5W1q" />
      </concept>
      <concept id="3308396621974580100" name="jetbrains.mps.lang.editor.structure.SubstituteMenu_Default" flags="ng" index="3p36aQ" />
      <concept id="5425882385312046132" name="jetbrains.mps.lang.editor.structure.QueryFunctionParameter_SubstituteMenu_CurrentTargetNode" flags="nn" index="1yR$tW" />
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389214265" name="jetbrains.mps.lang.editor.structure.EditorCellModel" flags="ng" index="3EYTF0">
        <child id="1142887637401" name="renderingCondition" index="pqm2j" />
      </concept>
      <concept id="1073389446423" name="jetbrains.mps.lang.editor.structure.CellModel_Collection" flags="sn" stub="3013115976261988961" index="3EZMnI">
        <child id="1106270802874" name="cellLayout" index="2iSdaV" />
        <child id="1073389446424" name="childCellModel" index="3EZMnx" />
      </concept>
      <concept id="1073389882823" name="jetbrains.mps.lang.editor.structure.CellModel_RefNode" flags="sg" stub="730538219795960754" index="3F1sOY" />
      <concept id="1225898583838" name="jetbrains.mps.lang.editor.structure.ReadOnlyModelAccessor" flags="ng" index="1HfYo3">
        <child id="1225898971709" name="getter" index="1Hhtcw" />
      </concept>
      <concept id="1225900081164" name="jetbrains.mps.lang.editor.structure.CellModel_ReadOnlyModelAccessor" flags="sg" stub="3708815482283559694" index="1HlG4h">
        <child id="1225900141900" name="modelAccessor" index="1HlULh" />
      </concept>
      <concept id="1176717841777" name="jetbrains.mps.lang.editor.structure.QueryFunction_ModelAccess_Getter" flags="in" index="3TQlhw" />
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
      </concept>
    </language>
    <language id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions">
      <concept id="7776141288922801652" name="jetbrains.mps.lang.actions.structure.NF_Concept_NewInstance" flags="nn" index="q_SaT">
        <child id="3757480014665178932" name="prototype" index="1wAxWu" />
      </concept>
      <concept id="767145758118872830" name="jetbrains.mps.lang.actions.structure.NF_Link_SetNewChildOperation" flags="nn" index="2DeJnY" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="7453996997717780434" name="jetbrains.mps.lang.smodel.structure.Node_GetSConceptOperation" flags="nn" index="2yIwOk" />
      <concept id="2644386474300074836" name="jetbrains.mps.lang.smodel.structure.ConceptIdRefExpression" flags="nn" index="35c_gC">
        <reference id="2644386474300074837" name="conceptDeclaration" index="35c_gD" />
      </concept>
      <concept id="6677504323281689838" name="jetbrains.mps.lang.smodel.structure.SConceptType" flags="in" index="3bZ5Sz">
        <reference id="6677504323281689839" name="conceptDeclaraton" index="3bZ5Sy" />
      </concept>
      <concept id="1172008320231" name="jetbrains.mps.lang.smodel.structure.Node_IsNotNullOperation" flags="nn" index="3x8VRR" />
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="24kQdi" id="4B5aqq3QF9M">
    <property role="3GE5qa" value="base.data" />
    <ref role="1XX52x" to="aia3:4B5aqq3QF78" resolve="ILaboratoryTestDataValue" />
    <node concept="3EZMnI" id="4B5aqq3QF9R" role="2wV5jI">
      <node concept="2iRfu4" id="4B5aqq3QF9S" role="2iSdaV" />
      <node concept="B$lHz" id="4B5aqq3QF9T" role="3EZMnx" />
      <node concept="3F1sOY" id="6LTgXmMnI9B" role="3EZMnx">
        <ref role="1NtTu8" to="aia3:6LTgXmMj7Lm" resolve="testReference" />
      </node>
      <node concept="3EZMnI" id="6LTgXmMdFLF" role="3EZMnx">
        <node concept="2iRfu4" id="6LTgXmMdFLG" role="2iSdaV" />
        <node concept="1HlG4h" id="4B5aqq3QF9V" role="3EZMnx">
          <ref role="1k5W1q" to="uubs:2XLt5KUltJN" resolve="DataValueKeyword" />
          <node concept="1HfYo3" id="4B5aqq3QF9W" role="1HlULh">
            <node concept="3TQlhw" id="4B5aqq3QF9X" role="1Hhtcw">
              <node concept="3clFbS" id="4B5aqq3QF9Y" role="2VODD2">
                <node concept="3clFbF" id="4B5aqq3QF9Z" role="3cqZAp">
                  <node concept="2OqwBi" id="4B5aqq3QFa0" role="3clFbG">
                    <node concept="2OqwBi" id="4B5aqq3QFa1" role="2Oq$k0">
                      <node concept="pncrf" id="4B5aqq3QFa2" role="2Oq$k0" />
                      <node concept="2yIwOk" id="4B5aqq3QFa3" role="2OqNvi" />
                    </node>
                    <node concept="2qgKlT" id="4B5aqq49E9b" role="2OqNvi">
                      <ref role="37wK5l" to="hlwg:4B5aqq3QFqK" resolve="getWithSpecimenTypeKeyword" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3F1sOY" id="4B5aqq3QFa5" role="3EZMnx">
          <ref role="1NtTu8" to="aia3:4B5aqq3QF7a" resolve="specimenTypeReference" />
        </node>
        <node concept="pkWqt" id="6LTgXmMdGxH" role="pqm2j">
          <node concept="3clFbS" id="6LTgXmMdGxI" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmMdGOL" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmMdI1v" role="3clFbG">
                <node concept="2OqwBi" id="6LTgXmMdH4g" role="2Oq$k0">
                  <node concept="pncrf" id="6LTgXmMdGOK" role="2Oq$k0" />
                  <node concept="3TrEf2" id="6LTgXmMdHyY" role="2OqNvi">
                    <ref role="3Tt5mk" to="aia3:4B5aqq3QF7a" resolve="specimenTypeReference" />
                  </node>
                </node>
                <node concept="3x8VRR" id="6LTgXmMdIwa" role="2OqNvi" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="6LTgXmMl8m2">
    <property role="3GE5qa" value="base.data.boolean" />
    <ref role="aqKnT" to="aia3:PDjyzjQppD" resolve="LaboratoryTestHasBeenPerformed" />
    <node concept="3ft6gV" id="6LTgXmMm4sS" role="3ft7WO">
      <node concept="23wRS9" id="6LTgXmMm4Ec" role="23Ddnj">
        <property role="TrG5h" value="concept" />
        <node concept="23DdeO" id="6LTgXmMm4Ed" role="23DdeQ">
          <node concept="3clFbS" id="6LTgXmMm4Ee" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmMm4Ut" role="3cqZAp">
              <node concept="35c_gC" id="6LTgXmMm4Us" role="3clFbG">
                <ref role="35c_gD" to="aia3:PDjyzjQppD" resolve="LaboratoryTestHasBeenPerformed" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3bZ5Sz" id="6LTgXmMm4EB" role="1tU5fm">
          <ref role="3bZ5Sy" to="aia3:PDjyzjQppD" resolve="LaboratoryTestHasBeenPerformed" />
        </node>
      </node>
      <node concept="3eGOop" id="6LTgXmMl8m3" role="3ft5RZ">
        <node concept="ucgPf" id="6LTgXmMl8m4" role="3aKz83">
          <node concept="3clFbS" id="6LTgXmMl8m5" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmMm8La" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmMm92H" role="3clFbG">
                <node concept="23wRSc" id="6LTgXmMm8L8" role="2Oq$k0">
                  <ref role="3cqZAo" node="6LTgXmMm4Ec" resolve="concept" />
                </node>
                <node concept="q_SaT" id="6LTgXmMm9oP" role="2OqNvi">
                  <node concept="1yR$tW" id="6LTgXmMm9yX" role="1wAxWu" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="16NfWO" id="6LTgXmMl8R_" role="upBLP">
          <node concept="uGdhv" id="6LTgXmMl8Xi" role="16NeZM">
            <node concept="3clFbS" id="6LTgXmMl8Xk" role="2VODD2">
              <node concept="3clFbF" id="6LTgXmMl95W" role="3cqZAp">
                <node concept="2OqwBi" id="6LTgXmMl9Kv" role="3clFbG">
                  <node concept="2qgKlT" id="6LTgXmMlait" role="2OqNvi">
                    <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                  </node>
                  <node concept="23wRSc" id="6LTgXmMmafa" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMm4Ec" resolve="concept" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="16NL0t" id="6LTgXmMlaAw" role="upBLP">
          <node concept="uGdhv" id="6LTgXmMlaGx" role="16NL0q">
            <node concept="3clFbS" id="6LTgXmMlaGz" role="2VODD2">
              <node concept="3clFbF" id="6LTgXmMlaPb" role="3cqZAp">
                <node concept="2OqwBi" id="6LTgXmMlbvI" role="3clFbG">
                  <node concept="2qgKlT" id="6LTgXmMlc1G" role="2OqNvi">
                    <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                  </node>
                  <node concept="23wRSc" id="6LTgXmMmavd" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMm4Ec" resolve="concept" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3eGOop" id="6LTgXmMlcmj" role="3ft5RZ">
        <node concept="ucgPf" id="6LTgXmMlcml" role="3aKz83">
          <node concept="3clFbS" id="6LTgXmMlcmn" role="2VODD2">
            <node concept="3cpWs8" id="6LTgXmMlcQm" role="3cqZAp">
              <node concept="3cpWsn" id="6LTgXmMlcQp" role="3cpWs9">
                <property role="TrG5h" value="node" />
                <node concept="3Tqbb2" id="6LTgXmMlcQl" role="1tU5fm">
                  <ref role="ehGHo" to="aia3:PDjyzjQppD" resolve="LaboratoryTestHasBeenPerformed" />
                </node>
                <node concept="2OqwBi" id="6LTgXmMmbF6" role="33vP2m">
                  <node concept="23wRSc" id="6LTgXmMmbmS" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMm4Ec" resolve="concept" />
                  </node>
                  <node concept="q_SaT" id="6LTgXmMmce5" role="2OqNvi">
                    <node concept="1yR$tW" id="6LTgXmMmcqC" role="1wAxWu" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="6LTgXmMldt_" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmMleJM" role="3clFbG">
                <node concept="2OqwBi" id="6LTgXmMldGt" role="2Oq$k0">
                  <node concept="37vLTw" id="6LTgXmMldtz" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMlcQp" resolve="node" />
                  </node>
                  <node concept="3TrEf2" id="6LTgXmMle6T" role="2OqNvi">
                    <ref role="3Tt5mk" to="aia3:4B5aqq3QF7a" resolve="specimenTypeReference" />
                  </node>
                </node>
                <node concept="2DeJnY" id="6LTgXmMlf9x" role="2OqNvi" />
              </node>
            </node>
            <node concept="3clFbF" id="6LTgXmMlfwq" role="3cqZAp">
              <node concept="37vLTw" id="6LTgXmMlfwo" role="3clFbG">
                <ref role="3cqZAo" node="6LTgXmMlcQp" resolve="node" />
              </node>
            </node>
          </node>
        </node>
        <node concept="16NfWO" id="6LTgXmMlfE0" role="upBLP">
          <node concept="uGdhv" id="6LTgXmMlfNc" role="16NeZM">
            <node concept="3clFbS" id="6LTgXmMlfNe" role="2VODD2">
              <node concept="3clFbF" id="6LTgXmMlfVQ" role="3cqZAp">
                <node concept="2OqwBi" id="6LTgXmMloTK" role="3clFbG">
                  <node concept="2qgKlT" id="6LTgXmMlprX" role="2OqNvi">
                    <ref role="37wK5l" to="hlwg:6LTgXmMlgN$" resolve="getAliasWithSpecimenType" />
                  </node>
                  <node concept="23wRSc" id="6LTgXmMmcBf" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMm4Ec" resolve="concept" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="16NL0t" id="6LTgXmMlpNx" role="upBLP">
          <node concept="uGdhv" id="6LTgXmMlwc7" role="16NL0q">
            <node concept="3clFbS" id="6LTgXmMlwc9" role="2VODD2">
              <node concept="3clFbF" id="6LTgXmMlwkL" role="3cqZAp">
                <node concept="2OqwBi" id="6LTgXmMlwZz" role="3clFbG">
                  <node concept="2qgKlT" id="6LTgXmMlxxK" role="2OqNvi">
                    <ref role="37wK5l" to="hlwg:6LTgXmMlq1k" resolve="getDescriptionWithSpecimenType" />
                  </node>
                  <node concept="23wRSc" id="6LTgXmMmcUr" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMm4Ec" resolve="concept" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="6LTgXmMlRtA">
    <property role="3GE5qa" value="base.data.boolean" />
    <ref role="aqKnT" to="aia3:4B5aqq3QoID" resolve="LaboratoryTestHasPreviouslyBeenPerformed" />
    <node concept="3ft6gV" id="6LTgXmMmlg$" role="3ft7WO">
      <node concept="23wRS9" id="6LTgXmMmlg_" role="23Ddnj">
        <property role="TrG5h" value="concept" />
        <node concept="23DdeO" id="6LTgXmMmlgA" role="23DdeQ">
          <node concept="3clFbS" id="6LTgXmMmlgB" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmMmlgC" role="3cqZAp">
              <node concept="35c_gC" id="6LTgXmMmlgD" role="3clFbG">
                <ref role="35c_gD" to="aia3:4B5aqq3QoID" resolve="LaboratoryTestHasPreviouslyBeenPerformed" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3bZ5Sz" id="6LTgXmMmlgE" role="1tU5fm">
          <ref role="3bZ5Sy" to="aia3:4B5aqq3QoID" resolve="LaboratoryTestHasPreviouslyBeenPerformed" />
        </node>
      </node>
      <node concept="3eGOop" id="6LTgXmMmlgF" role="3ft5RZ">
        <node concept="ucgPf" id="6LTgXmMmlgG" role="3aKz83">
          <node concept="3clFbS" id="6LTgXmMmlgH" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmMmlgI" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmMmlgJ" role="3clFbG">
                <node concept="23wRSc" id="6LTgXmMmlgK" role="2Oq$k0">
                  <ref role="3cqZAo" node="6LTgXmMmlg_" resolve="concept" />
                </node>
                <node concept="q_SaT" id="6LTgXmMmlgL" role="2OqNvi">
                  <node concept="1yR$tW" id="6LTgXmMmlgM" role="1wAxWu" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="16NfWO" id="6LTgXmMmlgN" role="upBLP">
          <node concept="uGdhv" id="6LTgXmMmlgO" role="16NeZM">
            <node concept="3clFbS" id="6LTgXmMmlgP" role="2VODD2">
              <node concept="3clFbF" id="6LTgXmMmlgQ" role="3cqZAp">
                <node concept="2OqwBi" id="6LTgXmMmlgR" role="3clFbG">
                  <node concept="2qgKlT" id="6LTgXmMmlgS" role="2OqNvi">
                    <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                  </node>
                  <node concept="23wRSc" id="6LTgXmMmlgT" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMmlg_" resolve="concept" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="16NL0t" id="6LTgXmMmlgU" role="upBLP">
          <node concept="uGdhv" id="6LTgXmMmlgV" role="16NL0q">
            <node concept="3clFbS" id="6LTgXmMmlgW" role="2VODD2">
              <node concept="3clFbF" id="6LTgXmMmlgX" role="3cqZAp">
                <node concept="2OqwBi" id="6LTgXmMmlgY" role="3clFbG">
                  <node concept="2qgKlT" id="6LTgXmMmlgZ" role="2OqNvi">
                    <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                  </node>
                  <node concept="23wRSc" id="6LTgXmMmlh0" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMmlg_" resolve="concept" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3eGOop" id="6LTgXmMmlh1" role="3ft5RZ">
        <node concept="ucgPf" id="6LTgXmMmlh2" role="3aKz83">
          <node concept="3clFbS" id="6LTgXmMmlh3" role="2VODD2">
            <node concept="3cpWs8" id="6LTgXmMmlh4" role="3cqZAp">
              <node concept="3cpWsn" id="6LTgXmMmlh5" role="3cpWs9">
                <property role="TrG5h" value="node" />
                <node concept="3Tqbb2" id="6LTgXmMmlh6" role="1tU5fm">
                  <ref role="ehGHo" to="aia3:4B5aqq3QoID" resolve="LaboratoryTestHasPreviouslyBeenPerformed" />
                </node>
                <node concept="2OqwBi" id="6LTgXmMmlh7" role="33vP2m">
                  <node concept="23wRSc" id="6LTgXmMmlh8" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMmlg_" resolve="concept" />
                  </node>
                  <node concept="q_SaT" id="6LTgXmMmlh9" role="2OqNvi">
                    <node concept="1yR$tW" id="6LTgXmMmlha" role="1wAxWu" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="6LTgXmMmlhb" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmMmlhc" role="3clFbG">
                <node concept="2OqwBi" id="6LTgXmMmlhd" role="2Oq$k0">
                  <node concept="37vLTw" id="6LTgXmMmlhe" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMmlh5" resolve="node" />
                  </node>
                  <node concept="3TrEf2" id="6LTgXmMmlhf" role="2OqNvi">
                    <ref role="3Tt5mk" to="aia3:4B5aqq3QF7a" resolve="specimenTypeReference" />
                  </node>
                </node>
                <node concept="2DeJnY" id="6LTgXmMmlhg" role="2OqNvi" />
              </node>
            </node>
            <node concept="3clFbF" id="6LTgXmMmlhh" role="3cqZAp">
              <node concept="37vLTw" id="6LTgXmMmlhi" role="3clFbG">
                <ref role="3cqZAo" node="6LTgXmMmlh5" resolve="node" />
              </node>
            </node>
          </node>
        </node>
        <node concept="16NfWO" id="6LTgXmMmlhj" role="upBLP">
          <node concept="uGdhv" id="6LTgXmMmlhk" role="16NeZM">
            <node concept="3clFbS" id="6LTgXmMmlhl" role="2VODD2">
              <node concept="3clFbF" id="6LTgXmMmlhm" role="3cqZAp">
                <node concept="2OqwBi" id="6LTgXmMmlhn" role="3clFbG">
                  <node concept="2qgKlT" id="6LTgXmMmlho" role="2OqNvi">
                    <ref role="37wK5l" to="hlwg:6LTgXmMlgN$" resolve="getAliasWithSpecimenType" />
                  </node>
                  <node concept="23wRSc" id="6LTgXmMmlhp" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMmlg_" resolve="concept" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="16NL0t" id="6LTgXmMmlhq" role="upBLP">
          <node concept="uGdhv" id="6LTgXmMmlhr" role="16NL0q">
            <node concept="3clFbS" id="6LTgXmMmlhs" role="2VODD2">
              <node concept="3clFbF" id="6LTgXmMmlht" role="3cqZAp">
                <node concept="2OqwBi" id="6LTgXmMmlhu" role="3clFbG">
                  <node concept="2qgKlT" id="6LTgXmMmlhv" role="2OqNvi">
                    <ref role="37wK5l" to="hlwg:6LTgXmMlq1k" resolve="getDescriptionWithSpecimenType" />
                  </node>
                  <node concept="23wRSc" id="6LTgXmMmlhw" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMmlg_" resolve="concept" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="6LTgXmMlTsU">
    <property role="3GE5qa" value="base.data.boolean" />
    <ref role="aqKnT" to="aia3:6khVixyeQtz" resolve="LaboratoryTestHasRequest" />
    <node concept="3ft6gV" id="6LTgXmMmocY" role="3ft7WO">
      <node concept="23wRS9" id="6LTgXmMmocZ" role="23Ddnj">
        <property role="TrG5h" value="concept" />
        <node concept="23DdeO" id="6LTgXmMmod0" role="23DdeQ">
          <node concept="3clFbS" id="6LTgXmMmod1" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmMmod2" role="3cqZAp">
              <node concept="35c_gC" id="6LTgXmMmod3" role="3clFbG">
                <ref role="35c_gD" to="aia3:6khVixyeQtz" resolve="LaboratoryTestHasRequest" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3bZ5Sz" id="6LTgXmMmod4" role="1tU5fm">
          <ref role="3bZ5Sy" to="aia3:6khVixyeQtz" resolve="LaboratoryTestHasRequest" />
        </node>
      </node>
      <node concept="3eGOop" id="6LTgXmMmod5" role="3ft5RZ">
        <node concept="ucgPf" id="6LTgXmMmod6" role="3aKz83">
          <node concept="3clFbS" id="6LTgXmMmod7" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmMmod8" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmMmod9" role="3clFbG">
                <node concept="23wRSc" id="6LTgXmMmoda" role="2Oq$k0">
                  <ref role="3cqZAo" node="6LTgXmMmocZ" resolve="concept" />
                </node>
                <node concept="q_SaT" id="6LTgXmMmodb" role="2OqNvi">
                  <node concept="1yR$tW" id="6LTgXmMmodc" role="1wAxWu" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="16NfWO" id="6LTgXmMmodd" role="upBLP">
          <node concept="uGdhv" id="6LTgXmMmode" role="16NeZM">
            <node concept="3clFbS" id="6LTgXmMmodf" role="2VODD2">
              <node concept="3clFbF" id="6LTgXmMmodg" role="3cqZAp">
                <node concept="2OqwBi" id="6LTgXmMmodh" role="3clFbG">
                  <node concept="2qgKlT" id="6LTgXmMmodi" role="2OqNvi">
                    <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                  </node>
                  <node concept="23wRSc" id="6LTgXmMmodj" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMmocZ" resolve="concept" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="16NL0t" id="6LTgXmMmodk" role="upBLP">
          <node concept="uGdhv" id="6LTgXmMmodl" role="16NL0q">
            <node concept="3clFbS" id="6LTgXmMmodm" role="2VODD2">
              <node concept="3clFbF" id="6LTgXmMmodn" role="3cqZAp">
                <node concept="2OqwBi" id="6LTgXmMmodo" role="3clFbG">
                  <node concept="2qgKlT" id="6LTgXmMmodp" role="2OqNvi">
                    <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                  </node>
                  <node concept="23wRSc" id="6LTgXmMmodq" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMmocZ" resolve="concept" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3eGOop" id="6LTgXmMmodr" role="3ft5RZ">
        <node concept="ucgPf" id="6LTgXmMmods" role="3aKz83">
          <node concept="3clFbS" id="6LTgXmMmodt" role="2VODD2">
            <node concept="3cpWs8" id="6LTgXmMmodu" role="3cqZAp">
              <node concept="3cpWsn" id="6LTgXmMmodv" role="3cpWs9">
                <property role="TrG5h" value="node" />
                <node concept="3Tqbb2" id="6LTgXmMmodw" role="1tU5fm">
                  <ref role="ehGHo" to="aia3:6khVixyeQtz" resolve="LaboratoryTestHasRequest" />
                </node>
                <node concept="2OqwBi" id="6LTgXmMmodx" role="33vP2m">
                  <node concept="23wRSc" id="6LTgXmMmody" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMmocZ" resolve="concept" />
                  </node>
                  <node concept="q_SaT" id="6LTgXmMmodz" role="2OqNvi">
                    <node concept="1yR$tW" id="6LTgXmMmod$" role="1wAxWu" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="6LTgXmMmod_" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmMmodA" role="3clFbG">
                <node concept="2OqwBi" id="6LTgXmMmodB" role="2Oq$k0">
                  <node concept="37vLTw" id="6LTgXmMmodC" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMmodv" resolve="node" />
                  </node>
                  <node concept="3TrEf2" id="6LTgXmMmodD" role="2OqNvi">
                    <ref role="3Tt5mk" to="aia3:4B5aqq3QF7a" resolve="specimenTypeReference" />
                  </node>
                </node>
                <node concept="2DeJnY" id="6LTgXmMmodE" role="2OqNvi" />
              </node>
            </node>
            <node concept="3clFbF" id="6LTgXmMmodF" role="3cqZAp">
              <node concept="37vLTw" id="6LTgXmMmodG" role="3clFbG">
                <ref role="3cqZAo" node="6LTgXmMmodv" resolve="node" />
              </node>
            </node>
          </node>
        </node>
        <node concept="16NfWO" id="6LTgXmMmodH" role="upBLP">
          <node concept="uGdhv" id="6LTgXmMmodI" role="16NeZM">
            <node concept="3clFbS" id="6LTgXmMmodJ" role="2VODD2">
              <node concept="3clFbF" id="6LTgXmMmodK" role="3cqZAp">
                <node concept="2OqwBi" id="6LTgXmMmodL" role="3clFbG">
                  <node concept="2qgKlT" id="6LTgXmMmodM" role="2OqNvi">
                    <ref role="37wK5l" to="hlwg:6LTgXmMlgN$" resolve="getAliasWithSpecimenType" />
                  </node>
                  <node concept="23wRSc" id="6LTgXmMmodN" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMmocZ" resolve="concept" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="16NL0t" id="6LTgXmMmodO" role="upBLP">
          <node concept="uGdhv" id="6LTgXmMmodP" role="16NL0q">
            <node concept="3clFbS" id="6LTgXmMmodQ" role="2VODD2">
              <node concept="3clFbF" id="6LTgXmMmodR" role="3cqZAp">
                <node concept="2OqwBi" id="6LTgXmMmodS" role="3clFbG">
                  <node concept="2qgKlT" id="6LTgXmMmodT" role="2OqNvi">
                    <ref role="37wK5l" to="hlwg:6LTgXmMlq1k" resolve="getDescriptionWithSpecimenType" />
                  </node>
                  <node concept="23wRSc" id="6LTgXmMmodU" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMmocZ" resolve="concept" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="6LTgXmMmqSa">
    <property role="3GE5qa" value="base.data.boolean" />
    <ref role="aqKnT" to="aia3:4B5aqq3QoIG" resolve="LaboratoryTestPreviousResultIsValid" />
    <node concept="3ft6gV" id="6LTgXmMmqSb" role="3ft7WO">
      <node concept="23wRS9" id="6LTgXmMmqSc" role="23Ddnj">
        <property role="TrG5h" value="concept" />
        <node concept="23DdeO" id="6LTgXmMmqSd" role="23DdeQ">
          <node concept="3clFbS" id="6LTgXmMmqSe" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmMmqSf" role="3cqZAp">
              <node concept="35c_gC" id="6LTgXmMmqSg" role="3clFbG">
                <ref role="35c_gD" to="aia3:4B5aqq3QoIG" resolve="LaboratoryTestPreviousResultIsValid" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3bZ5Sz" id="6LTgXmMmqSh" role="1tU5fm">
          <ref role="3bZ5Sy" to="aia3:4B5aqq3QoIG" resolve="LaboratoryTestPreviousResultIsValid" />
        </node>
      </node>
      <node concept="3eGOop" id="6LTgXmMmqSi" role="3ft5RZ">
        <node concept="ucgPf" id="6LTgXmMmqSj" role="3aKz83">
          <node concept="3clFbS" id="6LTgXmMmqSk" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmMmqSl" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmMmqSm" role="3clFbG">
                <node concept="23wRSc" id="6LTgXmMmqSn" role="2Oq$k0">
                  <ref role="3cqZAo" node="6LTgXmMmqSc" resolve="concept" />
                </node>
                <node concept="q_SaT" id="6LTgXmMmqSo" role="2OqNvi">
                  <node concept="1yR$tW" id="6LTgXmMmqSp" role="1wAxWu" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="16NfWO" id="6LTgXmMmqSq" role="upBLP">
          <node concept="uGdhv" id="6LTgXmMmqSr" role="16NeZM">
            <node concept="3clFbS" id="6LTgXmMmqSs" role="2VODD2">
              <node concept="3clFbF" id="6LTgXmMmqSt" role="3cqZAp">
                <node concept="2OqwBi" id="6LTgXmMmqSu" role="3clFbG">
                  <node concept="2qgKlT" id="6LTgXmMmqSv" role="2OqNvi">
                    <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                  </node>
                  <node concept="23wRSc" id="6LTgXmMmqSw" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMmqSc" resolve="concept" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="16NL0t" id="6LTgXmMmqSx" role="upBLP">
          <node concept="uGdhv" id="6LTgXmMmqSy" role="16NL0q">
            <node concept="3clFbS" id="6LTgXmMmqSz" role="2VODD2">
              <node concept="3clFbF" id="6LTgXmMmqS$" role="3cqZAp">
                <node concept="2OqwBi" id="6LTgXmMmqS_" role="3clFbG">
                  <node concept="2qgKlT" id="6LTgXmMmqSA" role="2OqNvi">
                    <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                  </node>
                  <node concept="23wRSc" id="6LTgXmMmqSB" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMmqSc" resolve="concept" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3eGOop" id="6LTgXmMmqSC" role="3ft5RZ">
        <node concept="ucgPf" id="6LTgXmMmqSD" role="3aKz83">
          <node concept="3clFbS" id="6LTgXmMmqSE" role="2VODD2">
            <node concept="3cpWs8" id="6LTgXmMmqSF" role="3cqZAp">
              <node concept="3cpWsn" id="6LTgXmMmqSG" role="3cpWs9">
                <property role="TrG5h" value="node" />
                <node concept="3Tqbb2" id="6LTgXmMmqSH" role="1tU5fm">
                  <ref role="ehGHo" to="aia3:4B5aqq3QoIG" resolve="LaboratoryTestPreviousResultIsValid" />
                </node>
                <node concept="2OqwBi" id="6LTgXmMmqSI" role="33vP2m">
                  <node concept="23wRSc" id="6LTgXmMmqSJ" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMmqSc" resolve="concept" />
                  </node>
                  <node concept="q_SaT" id="6LTgXmMmqSK" role="2OqNvi">
                    <node concept="1yR$tW" id="6LTgXmMmqSL" role="1wAxWu" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="6LTgXmMmqSM" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmMmqSN" role="3clFbG">
                <node concept="2OqwBi" id="6LTgXmMmqSO" role="2Oq$k0">
                  <node concept="37vLTw" id="6LTgXmMmqSP" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMmqSG" resolve="node" />
                  </node>
                  <node concept="3TrEf2" id="6LTgXmMmqSQ" role="2OqNvi">
                    <ref role="3Tt5mk" to="aia3:4B5aqq3QF7a" resolve="specimenTypeReference" />
                  </node>
                </node>
                <node concept="2DeJnY" id="6LTgXmMmqSR" role="2OqNvi" />
              </node>
            </node>
            <node concept="3clFbF" id="6LTgXmMmqSS" role="3cqZAp">
              <node concept="37vLTw" id="6LTgXmMmqST" role="3clFbG">
                <ref role="3cqZAo" node="6LTgXmMmqSG" resolve="node" />
              </node>
            </node>
          </node>
        </node>
        <node concept="16NfWO" id="6LTgXmMmqSU" role="upBLP">
          <node concept="uGdhv" id="6LTgXmMmqSV" role="16NeZM">
            <node concept="3clFbS" id="6LTgXmMmqSW" role="2VODD2">
              <node concept="3clFbF" id="6LTgXmMmqSX" role="3cqZAp">
                <node concept="2OqwBi" id="6LTgXmMmqSY" role="3clFbG">
                  <node concept="2qgKlT" id="6LTgXmMmqSZ" role="2OqNvi">
                    <ref role="37wK5l" to="hlwg:6LTgXmMlgN$" resolve="getAliasWithSpecimenType" />
                  </node>
                  <node concept="23wRSc" id="6LTgXmMmqT0" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMmqSc" resolve="concept" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="16NL0t" id="6LTgXmMmqT1" role="upBLP">
          <node concept="uGdhv" id="6LTgXmMmqT2" role="16NL0q">
            <node concept="3clFbS" id="6LTgXmMmqT3" role="2VODD2">
              <node concept="3clFbF" id="6LTgXmMmqT4" role="3cqZAp">
                <node concept="2OqwBi" id="6LTgXmMmqT5" role="3clFbG">
                  <node concept="2qgKlT" id="6LTgXmMmqT6" role="2OqNvi">
                    <ref role="37wK5l" to="hlwg:6LTgXmMlq1k" resolve="getDescriptionWithSpecimenType" />
                  </node>
                  <node concept="23wRSc" id="6LTgXmMmqT7" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMmqSc" resolve="concept" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="6LTgXmMmsvs">
    <property role="3GE5qa" value="base.data.boolean" />
    <ref role="aqKnT" to="aia3:4B5aqq3QoIJ" resolve="LaboratoryTestResultIsValid" />
    <node concept="3ft6gV" id="6LTgXmMmsvt" role="3ft7WO">
      <node concept="23wRS9" id="6LTgXmMmsvu" role="23Ddnj">
        <property role="TrG5h" value="concept" />
        <node concept="23DdeO" id="6LTgXmMmsvv" role="23DdeQ">
          <node concept="3clFbS" id="6LTgXmMmsvw" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmMmsvx" role="3cqZAp">
              <node concept="35c_gC" id="6LTgXmMmsvy" role="3clFbG">
                <ref role="35c_gD" to="aia3:4B5aqq3QoIJ" resolve="LaboratoryTestResultIsValid" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3bZ5Sz" id="6LTgXmMmsvz" role="1tU5fm">
          <ref role="3bZ5Sy" to="aia3:4B5aqq3QoIJ" resolve="LaboratoryTestResultIsValid" />
        </node>
      </node>
      <node concept="3eGOop" id="6LTgXmMmsv$" role="3ft5RZ">
        <node concept="ucgPf" id="6LTgXmMmsv_" role="3aKz83">
          <node concept="3clFbS" id="6LTgXmMmsvA" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmMmsvB" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmMmsvC" role="3clFbG">
                <node concept="23wRSc" id="6LTgXmMmsvD" role="2Oq$k0">
                  <ref role="3cqZAo" node="6LTgXmMmsvu" resolve="concept" />
                </node>
                <node concept="q_SaT" id="6LTgXmMmsvE" role="2OqNvi">
                  <node concept="1yR$tW" id="6LTgXmMmsvF" role="1wAxWu" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="16NfWO" id="6LTgXmMmsvG" role="upBLP">
          <node concept="uGdhv" id="6LTgXmMmsvH" role="16NeZM">
            <node concept="3clFbS" id="6LTgXmMmsvI" role="2VODD2">
              <node concept="3clFbF" id="6LTgXmMmsvJ" role="3cqZAp">
                <node concept="2OqwBi" id="6LTgXmMmsvK" role="3clFbG">
                  <node concept="2qgKlT" id="6LTgXmMmsvL" role="2OqNvi">
                    <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                  </node>
                  <node concept="23wRSc" id="6LTgXmMmsvM" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMmsvu" resolve="concept" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="16NL0t" id="6LTgXmMmsvN" role="upBLP">
          <node concept="uGdhv" id="6LTgXmMmsvO" role="16NL0q">
            <node concept="3clFbS" id="6LTgXmMmsvP" role="2VODD2">
              <node concept="3clFbF" id="6LTgXmMmsvQ" role="3cqZAp">
                <node concept="2OqwBi" id="6LTgXmMmsvR" role="3clFbG">
                  <node concept="2qgKlT" id="6LTgXmMmsvS" role="2OqNvi">
                    <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                  </node>
                  <node concept="23wRSc" id="6LTgXmMmsvT" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMmsvu" resolve="concept" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3eGOop" id="6LTgXmMmsvU" role="3ft5RZ">
        <node concept="ucgPf" id="6LTgXmMmsvV" role="3aKz83">
          <node concept="3clFbS" id="6LTgXmMmsvW" role="2VODD2">
            <node concept="3cpWs8" id="6LTgXmMmsvX" role="3cqZAp">
              <node concept="3cpWsn" id="6LTgXmMmsvY" role="3cpWs9">
                <property role="TrG5h" value="node" />
                <node concept="3Tqbb2" id="6LTgXmMmsvZ" role="1tU5fm">
                  <ref role="ehGHo" to="aia3:4B5aqq3QoIJ" resolve="LaboratoryTestResultIsValid" />
                </node>
                <node concept="2OqwBi" id="6LTgXmMmsw0" role="33vP2m">
                  <node concept="23wRSc" id="6LTgXmMmsw1" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMmsvu" resolve="concept" />
                  </node>
                  <node concept="q_SaT" id="6LTgXmMmsw2" role="2OqNvi">
                    <node concept="1yR$tW" id="6LTgXmMmsw3" role="1wAxWu" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="6LTgXmMmsw4" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmMmsw5" role="3clFbG">
                <node concept="2OqwBi" id="6LTgXmMmsw6" role="2Oq$k0">
                  <node concept="37vLTw" id="6LTgXmMmsw7" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMmsvY" resolve="node" />
                  </node>
                  <node concept="3TrEf2" id="6LTgXmMmsw8" role="2OqNvi">
                    <ref role="3Tt5mk" to="aia3:4B5aqq3QF7a" resolve="specimenTypeReference" />
                  </node>
                </node>
                <node concept="2DeJnY" id="6LTgXmMmsw9" role="2OqNvi" />
              </node>
            </node>
            <node concept="3clFbF" id="6LTgXmMmswa" role="3cqZAp">
              <node concept="37vLTw" id="6LTgXmMmswb" role="3clFbG">
                <ref role="3cqZAo" node="6LTgXmMmsvY" resolve="node" />
              </node>
            </node>
          </node>
        </node>
        <node concept="16NfWO" id="6LTgXmMmswc" role="upBLP">
          <node concept="uGdhv" id="6LTgXmMmswd" role="16NeZM">
            <node concept="3clFbS" id="6LTgXmMmswe" role="2VODD2">
              <node concept="3clFbF" id="6LTgXmMmswf" role="3cqZAp">
                <node concept="2OqwBi" id="6LTgXmMmswg" role="3clFbG">
                  <node concept="2qgKlT" id="6LTgXmMmswh" role="2OqNvi">
                    <ref role="37wK5l" to="hlwg:6LTgXmMlgN$" resolve="getAliasWithSpecimenType" />
                  </node>
                  <node concept="23wRSc" id="6LTgXmMmswi" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMmsvu" resolve="concept" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="16NL0t" id="6LTgXmMmswj" role="upBLP">
          <node concept="uGdhv" id="6LTgXmMmswk" role="16NL0q">
            <node concept="3clFbS" id="6LTgXmMmswl" role="2VODD2">
              <node concept="3clFbF" id="6LTgXmMmswm" role="3cqZAp">
                <node concept="2OqwBi" id="6LTgXmMmswn" role="3clFbG">
                  <node concept="2qgKlT" id="6LTgXmMmswo" role="2OqNvi">
                    <ref role="37wK5l" to="hlwg:6LTgXmMlq1k" resolve="getDescriptionWithSpecimenType" />
                  </node>
                  <node concept="23wRSc" id="6LTgXmMmswp" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMmsvu" resolve="concept" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="6LTgXmMmuly">
    <property role="3GE5qa" value="base.data.value" />
    <ref role="aqKnT" to="aia3:4B5aqq4cwJ4" resolve="LaboratoryTestTimeSinceLastRequest" />
    <node concept="3ft6gV" id="6LTgXmMmuPX" role="3ft7WO">
      <node concept="23wRS9" id="6LTgXmMmuPY" role="23Ddnj">
        <property role="TrG5h" value="concept" />
        <node concept="23DdeO" id="6LTgXmMmuPZ" role="23DdeQ">
          <node concept="3clFbS" id="6LTgXmMmuQ0" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmMmuQ1" role="3cqZAp">
              <node concept="35c_gC" id="6LTgXmMmuQ2" role="3clFbG">
                <ref role="35c_gD" to="aia3:4B5aqq4cwJ4" resolve="LaboratoryTestTimeSinceLastRequest" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3bZ5Sz" id="6LTgXmMmuQ3" role="1tU5fm">
          <ref role="3bZ5Sy" to="aia3:4B5aqq4cwJ4" resolve="LaboratoryTestTimeSinceLastRequest" />
        </node>
      </node>
      <node concept="3eGOop" id="6LTgXmMmuQ4" role="3ft5RZ">
        <node concept="ucgPf" id="6LTgXmMmuQ5" role="3aKz83">
          <node concept="3clFbS" id="6LTgXmMmuQ6" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmMmuQ7" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmMmuQ8" role="3clFbG">
                <node concept="23wRSc" id="6LTgXmMmuQ9" role="2Oq$k0">
                  <ref role="3cqZAo" node="6LTgXmMmuPY" resolve="concept" />
                </node>
                <node concept="q_SaT" id="6LTgXmMmuQa" role="2OqNvi">
                  <node concept="1yR$tW" id="6LTgXmMmuQb" role="1wAxWu" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="16NfWO" id="6LTgXmMmuQc" role="upBLP">
          <node concept="uGdhv" id="6LTgXmMmuQd" role="16NeZM">
            <node concept="3clFbS" id="6LTgXmMmuQe" role="2VODD2">
              <node concept="3clFbF" id="6LTgXmMmuQf" role="3cqZAp">
                <node concept="2OqwBi" id="6LTgXmMmuQg" role="3clFbG">
                  <node concept="2qgKlT" id="6LTgXmMmuQh" role="2OqNvi">
                    <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                  </node>
                  <node concept="23wRSc" id="6LTgXmMmuQi" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMmuPY" resolve="concept" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="16NL0t" id="6LTgXmMmuQj" role="upBLP">
          <node concept="uGdhv" id="6LTgXmMmuQk" role="16NL0q">
            <node concept="3clFbS" id="6LTgXmMmuQl" role="2VODD2">
              <node concept="3clFbF" id="6LTgXmMmuQm" role="3cqZAp">
                <node concept="2OqwBi" id="6LTgXmMmuQn" role="3clFbG">
                  <node concept="2qgKlT" id="6LTgXmMmuQo" role="2OqNvi">
                    <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                  </node>
                  <node concept="23wRSc" id="6LTgXmMmuQp" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMmuPY" resolve="concept" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3eGOop" id="6LTgXmMmuQq" role="3ft5RZ">
        <node concept="ucgPf" id="6LTgXmMmuQr" role="3aKz83">
          <node concept="3clFbS" id="6LTgXmMmuQs" role="2VODD2">
            <node concept="3cpWs8" id="6LTgXmMmuQt" role="3cqZAp">
              <node concept="3cpWsn" id="6LTgXmMmuQu" role="3cpWs9">
                <property role="TrG5h" value="node" />
                <node concept="3Tqbb2" id="6LTgXmMmuQv" role="1tU5fm">
                  <ref role="ehGHo" to="aia3:4B5aqq4cwJ4" resolve="LaboratoryTestTimeSinceLastRequest" />
                </node>
                <node concept="2OqwBi" id="6LTgXmMmuQw" role="33vP2m">
                  <node concept="23wRSc" id="6LTgXmMmuQx" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMmuPY" resolve="concept" />
                  </node>
                  <node concept="q_SaT" id="6LTgXmMmuQy" role="2OqNvi">
                    <node concept="1yR$tW" id="6LTgXmMmuQz" role="1wAxWu" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="6LTgXmMmuQ$" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmMmuQ_" role="3clFbG">
                <node concept="2OqwBi" id="6LTgXmMmuQA" role="2Oq$k0">
                  <node concept="37vLTw" id="6LTgXmMmuQB" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMmuQu" resolve="node" />
                  </node>
                  <node concept="3TrEf2" id="6LTgXmMmuQC" role="2OqNvi">
                    <ref role="3Tt5mk" to="aia3:4B5aqq3QF7a" resolve="specimenTypeReference" />
                  </node>
                </node>
                <node concept="2DeJnY" id="6LTgXmMmuQD" role="2OqNvi" />
              </node>
            </node>
            <node concept="3clFbF" id="6LTgXmMmuQE" role="3cqZAp">
              <node concept="37vLTw" id="6LTgXmMmuQF" role="3clFbG">
                <ref role="3cqZAo" node="6LTgXmMmuQu" resolve="node" />
              </node>
            </node>
          </node>
        </node>
        <node concept="16NfWO" id="6LTgXmMmuQG" role="upBLP">
          <node concept="uGdhv" id="6LTgXmMmuQH" role="16NeZM">
            <node concept="3clFbS" id="6LTgXmMmuQI" role="2VODD2">
              <node concept="3clFbF" id="6LTgXmMmuQJ" role="3cqZAp">
                <node concept="2OqwBi" id="6LTgXmMmuQK" role="3clFbG">
                  <node concept="2qgKlT" id="6LTgXmMmuQL" role="2OqNvi">
                    <ref role="37wK5l" to="hlwg:6LTgXmMlgN$" resolve="getAliasWithSpecimenType" />
                  </node>
                  <node concept="23wRSc" id="6LTgXmMmuQM" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMmuPY" resolve="concept" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="16NL0t" id="6LTgXmMmuQN" role="upBLP">
          <node concept="uGdhv" id="6LTgXmMmuQO" role="16NL0q">
            <node concept="3clFbS" id="6LTgXmMmuQP" role="2VODD2">
              <node concept="3clFbF" id="6LTgXmMmuQQ" role="3cqZAp">
                <node concept="2OqwBi" id="6LTgXmMmuQR" role="3clFbG">
                  <node concept="2qgKlT" id="6LTgXmMmuQS" role="2OqNvi">
                    <ref role="37wK5l" to="hlwg:6LTgXmMlq1k" resolve="getDescriptionWithSpecimenType" />
                  </node>
                  <node concept="23wRSc" id="6LTgXmMmuQT" role="2Oq$k0">
                    <ref role="3cqZAo" node="6LTgXmMmuPY" resolve="concept" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

