<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:ef86e72b-c040-4e34-93f2-8df1d703b946(no.uio.mLab.decisions.data.laboratoryTest.translations)">
  <persistence version="9" />
  <languages>
    <use id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core" version="1" />
    <use id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage" version="6" />
  </languages>
  <imports>
    <import index="9igz" ref="r:0ba33904-c124-449b-bde4-640f55a71953(no.uio.mLab.shared.translations)" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1465982738277781862" name="jetbrains.mps.baseLanguage.structure.PlaceholderMember" flags="ng" index="2tJIrI" />
      <concept id="1188207840427" name="jetbrains.mps.baseLanguage.structure.AnnotationInstance" flags="nn" index="2AHcQZ">
        <reference id="1188208074048" name="annotation" index="2AI5Lk" />
      </concept>
      <concept id="1188208481402" name="jetbrains.mps.baseLanguage.structure.HasAnnotation" flags="ng" index="2AJDlI">
        <child id="1188208488637" name="annotation" index="2AJF6D" />
      </concept>
      <concept id="2820489544401957797" name="jetbrains.mps.baseLanguage.structure.DefaultClassCreator" flags="nn" index="HV5vD">
        <reference id="2820489544401957798" name="classifier" index="HV5vE" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1070462154015" name="jetbrains.mps.baseLanguage.structure.StaticFieldDeclaration" flags="ig" index="Wx3nA">
        <property id="6468716278899126575" name="isVolatile" index="2dlcS1" />
        <property id="6468716278899125786" name="isTransient" index="2dld4O" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1081236700938" name="jetbrains.mps.baseLanguage.structure.StaticMethodDeclaration" flags="ig" index="2YIFZL" />
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu">
        <child id="1095933932569" name="implementedInterface" index="EKbjA" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <property id="1176718929932" name="isFinal" index="3TUv4t" />
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <property id="4276006055363816570" name="isSynchronized" index="od$2w" />
        <property id="1181808852946" name="isFinal" index="DiZV1" />
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123165" name="jetbrains.mps.baseLanguage.structure.InstanceMethodDeclaration" flags="ig" index="3clFb_">
        <property id="1178608670077" name="isAbstract" index="1EzhhJ" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242875" name="jetbrains.mps.baseLanguage.structure.PlusExpression" flags="nn" index="3cpWs3" />
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1107461130800" name="jetbrains.mps.baseLanguage.structure.Classifier" flags="ng" index="3pOWGL">
        <child id="5375687026011219971" name="member" index="jymVt" unordered="true" />
      </concept>
      <concept id="7812454656619025416" name="jetbrains.mps.baseLanguage.structure.MethodDeclaration" flags="ng" index="1rXfSm">
        <property id="8355037393041754995" name="isNative" index="2aFKle" />
      </concept>
      <concept id="7812454656619025412" name="jetbrains.mps.baseLanguage.structure.LocalMethodCall" flags="nn" index="1rXfSq" />
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1107796713796" name="jetbrains.mps.baseLanguage.structure.Interface" flags="ig" index="3HP615" />
      <concept id="1163670490218" name="jetbrains.mps.baseLanguage.structure.SwitchStatement" flags="nn" index="3KaCP$">
        <child id="1163670592366" name="defaultBlock" index="3Kb1Dw" />
        <child id="1163670766145" name="expression" index="3KbGdf" />
        <child id="1163670772911" name="case" index="3KbHQx" />
      </concept>
      <concept id="1163670641947" name="jetbrains.mps.baseLanguage.structure.SwitchCase" flags="ng" index="3KbdKl">
        <child id="1163670677455" name="expression" index="3Kbmr1" />
        <child id="1163670683720" name="body" index="3Kbo56" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
      <concept id="1146644623116" name="jetbrains.mps.baseLanguage.structure.PrivateVisibility" flags="nn" index="3Tm6S6" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="312cEu" id="4zMac8rUNsN">
    <property role="TrG5h" value="LaboratoryTestDataTranslationProvider" />
    <property role="3GE5qa" value="internationalization" />
    <node concept="Wx3nA" id="4zMac8rVBeK" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="TrG5h" value="displayTranslations" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm1VV" id="4zMac8rVBei" role="1B3o_S" />
      <node concept="3uibUv" id="4zMac8rVBeC" role="1tU5fm">
        <ref role="3uigEE" node="4zMac8rUNtP" resolve="ILaboratoryTestDataTranslations" />
      </node>
      <node concept="1rXfSq" id="4zMac8rVBfh" role="33vP2m">
        <ref role="37wK5l" node="4zMac8rUNuE" resolve="init" />
        <node concept="10M0yZ" id="ofcHXfTMGc" role="37wK5m">
          <ref role="1PxDUh" to="9igz:5fXsrCVmxTN" resolve="TranslationConfiguration" />
          <ref role="3cqZAo" to="9igz:5fXsrCVmzVm" resolve="displayLanguage" />
        </node>
      </node>
    </node>
    <node concept="Wx3nA" id="1Hxyv4DUmfq" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="TrG5h" value="generationTranslations" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm1VV" id="1Hxyv4DUmfr" role="1B3o_S" />
      <node concept="3uibUv" id="1Hxyv4DUmfs" role="1tU5fm">
        <ref role="3uigEE" node="4zMac8rUNtP" resolve="ILaboratoryTestDataTranslations" />
      </node>
      <node concept="1rXfSq" id="1Hxyv4DUmft" role="33vP2m">
        <ref role="37wK5l" node="4zMac8rUNuE" resolve="init" />
        <node concept="10M0yZ" id="ofcHXfTMH1" role="37wK5m">
          <ref role="1PxDUh" to="9igz:5fXsrCVmxTN" resolve="TranslationConfiguration" />
          <ref role="3cqZAo" to="9igz:5fXsrCVmzVN" resolve="generationLanguage" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="4zMac8rUNtp" role="jymVt" />
    <node concept="2YIFZL" id="4zMac8rUNuE" role="jymVt">
      <property role="TrG5h" value="init" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="4zMac8rUNuH" role="3clF47">
        <node concept="3KaCP$" id="4zMac8rUNv7" role="3cqZAp">
          <node concept="3KbdKl" id="4zMac8rUNXu" role="3KbHQx">
            <node concept="Xl_RD" id="4zMac8rUNXU" role="3Kbmr1">
              <property role="Xl_RC" value="no" />
            </node>
            <node concept="3clFbS" id="4zMac8rUNXw" role="3Kbo56">
              <node concept="3cpWs6" id="4zMac8rUNYt" role="3cqZAp">
                <node concept="2ShNRf" id="4zMac8rUO3y" role="3cqZAk">
                  <node concept="HV5vD" id="4zMac8rUP5z" role="2ShVmc">
                    <ref role="HV5vE" node="4zMac8rUO1O" resolve="NoLaboratoryTestDataTranslations" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="4zMac8rUNYX" role="3Kb1Dw">
            <node concept="3cpWs6" id="4zMac8rUNZC" role="3cqZAp">
              <node concept="2ShNRf" id="4zMac8rUP6U" role="3cqZAk">
                <node concept="HV5vD" id="4zMac8rUPap" role="2ShVmc">
                  <ref role="HV5vE" node="4zMac8rUO0_" resolve="EnLaboratoryTestDataTranslations" />
                </node>
              </node>
            </node>
          </node>
          <node concept="37vLTw" id="1Hxyv4DUmde" role="3KbGdf">
            <ref role="3cqZAo" node="1Hxyv4DUm7P" resolve="language" />
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="4zMac8rW809" role="1B3o_S" />
      <node concept="3uibUv" id="4zMac8rUNuz" role="3clF45">
        <ref role="3uigEE" node="4zMac8rUNtP" resolve="ILaboratoryTestDataTranslations" />
      </node>
      <node concept="37vLTG" id="1Hxyv4DUm7P" role="3clF46">
        <property role="TrG5h" value="language" />
        <node concept="17QB3L" id="1Hxyv4DUm7O" role="1tU5fm" />
      </node>
    </node>
    <node concept="3Tm1VV" id="4zMac8rUNsO" role="1B3o_S" />
  </node>
  <node concept="3HP615" id="4zMac8rUNtP">
    <property role="TrG5h" value="ILaboratoryTestDataTranslations" />
    <property role="3GE5qa" value="internationalization" />
    <node concept="3clFb_" id="6LTgXmMhnrg" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getILaboratoryTestDataValueIncludeSpecimenTypeIntentionDescription" />
      <node concept="3clFbS" id="6LTgXmMhnrj" role="3clF47" />
      <node concept="3Tm1VV" id="6LTgXmMhnrk" role="1B3o_S" />
      <node concept="17QB3L" id="6LTgXmMhnk9" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="6LTgXmMhnd6" role="jymVt" />
    <node concept="3clFb_" id="7AAKH6gc61N" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasRequestAlias" />
      <node concept="3clFbS" id="7AAKH6gc61Q" role="3clF47" />
      <node concept="3Tm1VV" id="7AAKH6gc61R" role="1B3o_S" />
      <node concept="17QB3L" id="7AAKH6gc5Qv" role="3clF45" />
    </node>
    <node concept="3clFb_" id="7AAKH6gc6Dw" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasRequestDescription" />
      <node concept="3clFbS" id="7AAKH6gc6Dz" role="3clF47" />
      <node concept="3Tm1VV" id="7AAKH6gc6D$" role="1B3o_S" />
      <node concept="17QB3L" id="7AAKH6gc6u4" role="3clF45" />
    </node>
    <node concept="3clFb_" id="4B5aqq44_aU" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasRequestWithSpecimenAlias" />
      <node concept="3clFbS" id="4B5aqq44_aV" role="3clF47" />
      <node concept="3Tm1VV" id="4B5aqq44_aW" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq44_aX" role="3clF45" />
    </node>
    <node concept="3clFb_" id="4B5aqq44_aY" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasRequestWithSpecimenDescription" />
      <node concept="3clFbS" id="4B5aqq44_aZ" role="3clF47" />
      <node concept="3Tm1VV" id="4B5aqq44_b0" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq44_b1" role="3clF45" />
    </node>
    <node concept="3clFb_" id="4B5aqq44_b2" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasRequestWithSpecimenKeywordTestHasRequest" />
      <node concept="3clFbS" id="4B5aqq44_b3" role="3clF47" />
      <node concept="3Tm1VV" id="4B5aqq44_b4" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq44_b5" role="3clF45" />
    </node>
    <node concept="3clFb_" id="4B5aqq44_b6" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasRequestWithSpecimenKeywordWithSpecimen" />
      <node concept="3clFbS" id="4B5aqq44_b7" role="3clF47" />
      <node concept="3Tm1VV" id="4B5aqq44_b8" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq44_b9" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="PDjyzjQr9w" role="jymVt" />
    <node concept="3clFb_" id="PDjyzjQrlC" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasBeenPerformedAlias" />
      <node concept="3clFbS" id="PDjyzjQrlF" role="3clF47" />
      <node concept="3Tm1VV" id="PDjyzjQrlG" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzjQrhz" role="3clF45" />
    </node>
    <node concept="3clFb_" id="PDjyzjQrDr" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasBeenPerformedDescription" />
      <node concept="3clFbS" id="PDjyzjQrDs" role="3clF47" />
      <node concept="3Tm1VV" id="PDjyzjQrDt" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzjQrDu" role="3clF45" />
    </node>
    <node concept="3clFb_" id="PDjyzjQs0b" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasBeenPerformedOnSpecimenAlias" />
      <node concept="3clFbS" id="PDjyzjQs0c" role="3clF47" />
      <node concept="3Tm1VV" id="PDjyzjQs0d" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzjQs0e" role="3clF45" />
    </node>
    <node concept="3clFb_" id="PDjyzjQzjJ" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasBeenPerformedOnSpecimenDescription" />
      <node concept="3clFbS" id="PDjyzjQzjK" role="3clF47" />
      <node concept="3Tm1VV" id="PDjyzjQzjL" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzjQzjM" role="3clF45" />
    </node>
    <node concept="3clFb_" id="PDjyzjQs0f" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasBeenPerformedOnSpecimenKeywordHasBeenPerformed" />
      <node concept="3clFbS" id="PDjyzjQs0g" role="3clF47" />
      <node concept="3Tm1VV" id="PDjyzjQs0h" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzjQs0i" role="3clF45" />
    </node>
    <node concept="3clFb_" id="PDjyzjQsEj" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasBeenPerformedOnSpecimenKeywordOnSpecimen" />
      <node concept="3clFbS" id="PDjyzjQsEk" role="3clF47" />
      <node concept="3Tm1VV" id="PDjyzjQsEl" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzjQsEm" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="1mAGFBJldHh" role="jymVt" />
    <node concept="3clFb_" id="17XAtu89FWp" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultIsValidAlias" />
      <node concept="3clFbS" id="17XAtu89FWs" role="3clF47" />
      <node concept="3Tm1VV" id="17XAtu89FWt" role="1B3o_S" />
      <node concept="17QB3L" id="17XAtu89FIn" role="3clF45" />
    </node>
    <node concept="3clFb_" id="17XAtu89GFx" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultIsValidDescription" />
      <node concept="3clFbS" id="17XAtu89GF$" role="3clF47" />
      <node concept="3Tm1VV" id="17XAtu89GF_" role="1B3o_S" />
      <node concept="17QB3L" id="17XAtu89Gtj" role="3clF45" />
    </node>
    <node concept="3clFb_" id="4B5aqq44_am" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultOnSpecimenTypeIsValidAlias" />
      <node concept="3clFbS" id="4B5aqq44_an" role="3clF47" />
      <node concept="3Tm1VV" id="4B5aqq44_ao" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq44_ap" role="3clF45" />
    </node>
    <node concept="3clFb_" id="4B5aqq44_aq" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultOnSpecimenTypeIsValidDescription" />
      <node concept="3clFbS" id="4B5aqq44_ar" role="3clF47" />
      <node concept="3Tm1VV" id="4B5aqq44_as" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq44_at" role="3clF45" />
    </node>
    <node concept="3clFb_" id="4B5aqq44IWm" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultOnSpecimenTypeIsValidKeywordHasValidResult" />
      <node concept="3clFbS" id="4B5aqq44IWp" role="3clF47" />
      <node concept="3Tm1VV" id="4B5aqq44IWq" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq44ITa" role="3clF45" />
    </node>
    <node concept="3clFb_" id="4B5aqq44IKt" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultOnSpecimenTypeIsValidKeywordOnSpecimen" />
      <node concept="3clFbS" id="4B5aqq44IKw" role="3clF47" />
      <node concept="3Tm1VV" id="4B5aqq44IKx" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq44IHp" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="301PNdoZxM$" role="jymVt" />
    <node concept="3clFb_" id="17XAtu82mUY" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestTimeSinceLastPerformedAlias" />
      <node concept="3clFbS" id="17XAtu82mV1" role="3clF47" />
      <node concept="3Tm1VV" id="17XAtu82mV2" role="1B3o_S" />
      <node concept="17QB3L" id="17XAtu82mJ7" role="3clF45" />
    </node>
    <node concept="3clFb_" id="17XAtu82nyP" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestTimeSinceLastPerformedDescription" />
      <node concept="3clFbS" id="17XAtu82nyS" role="3clF47" />
      <node concept="3Tm1VV" id="17XAtu82nyT" role="1B3o_S" />
      <node concept="17QB3L" id="17XAtu82nmM" role="3clF45" />
    </node>
    <node concept="3clFb_" id="4B5aqq44_av" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestTimeSinceLastPerformedOnSpecimenAlias" />
      <node concept="3clFbS" id="4B5aqq44_aw" role="3clF47" />
      <node concept="3Tm1VV" id="4B5aqq44_ax" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq44_ay" role="3clF45" />
    </node>
    <node concept="3clFb_" id="4B5aqq44_az" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestTimeSinceLastPerformedOnSpecimenDescription" />
      <node concept="3clFbS" id="4B5aqq44_a$" role="3clF47" />
      <node concept="3Tm1VV" id="4B5aqq44_a_" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq44_aA" role="3clF45" />
    </node>
    <node concept="3clFb_" id="4B5aqq44J8B" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestTimeSinceLastPerformedOnSpecimenKeywordTimeSinceLastRequested" />
      <node concept="3clFbS" id="4B5aqq44J8E" role="3clF47" />
      <node concept="3Tm1VV" id="4B5aqq44J8F" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq44J5j" role="3clF45" />
    </node>
    <node concept="3clFb_" id="4B5aqq44Jlg" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestTimeSinceLastPerformedOnSpecimenKeywordOnSpecimen" />
      <node concept="3clFbS" id="4B5aqq44Jlj" role="3clF47" />
      <node concept="3Tm1VV" id="4B5aqq44Jlk" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq44JhO" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="1mAGFBJloNq" role="jymVt" />
    <node concept="3clFb_" id="1mAGFBJloUl" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasPreviouslyBeenPerformedAlias" />
      <node concept="3clFbS" id="1mAGFBJloUo" role="3clF47" />
      <node concept="3Tm1VV" id="1mAGFBJloUp" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBJloRZ" role="3clF45" />
    </node>
    <node concept="3clFb_" id="1mAGFBJloYV" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasPreviouslyBeenPerformedDescription" />
      <node concept="3clFbS" id="1mAGFBJloYW" role="3clF47" />
      <node concept="3Tm1VV" id="1mAGFBJloYX" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBJloYY" role="3clF45" />
    </node>
    <node concept="3clFb_" id="4B5aqq44_aC" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasPreviouslyBeenPerformedOnSpecimenAlias" />
      <node concept="3clFbS" id="4B5aqq44_aD" role="3clF47" />
      <node concept="3Tm1VV" id="4B5aqq44_aE" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq44_aF" role="3clF45" />
    </node>
    <node concept="3clFb_" id="4B5aqq44_aG" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasPreviouslyBeenPerformedOnSpecimenDescription" />
      <node concept="3clFbS" id="4B5aqq44_aH" role="3clF47" />
      <node concept="3Tm1VV" id="4B5aqq44_aI" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq44_aJ" role="3clF45" />
    </node>
    <node concept="3clFb_" id="4B5aqq44KKT" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasPreviouslyBeenPerformedOnSpecimenKeywordHasPreviousRequest" />
      <node concept="3clFbS" id="4B5aqq44KKW" role="3clF47" />
      <node concept="3Tm1VV" id="4B5aqq44KKX" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq44KHl" role="3clF45" />
    </node>
    <node concept="3clFb_" id="4B5aqq44KYi" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasPreviouslyBeenPerformedOnSpecimenKeywordOnSpecimen" />
      <node concept="3clFbS" id="4B5aqq44KYl" role="3clF47" />
      <node concept="3Tm1VV" id="4B5aqq44KYm" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq44KUA" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="1mAGFBJlpkl" role="jymVt" />
    <node concept="3clFb_" id="1mAGFBJlrOq" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultIsValidAlias" />
      <node concept="3clFbS" id="1mAGFBJlrOt" role="3clF47" />
      <node concept="3Tm1VV" id="1mAGFBJlrOu" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBJlrLy" role="3clF45" />
    </node>
    <node concept="3clFb_" id="1mAGFBJlseF" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultIsValidDescription" />
      <node concept="3clFbS" id="1mAGFBJlseI" role="3clF47" />
      <node concept="3Tm1VV" id="1mAGFBJlseJ" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBJlrWJ" role="3clF45" />
    </node>
    <node concept="3clFb_" id="4B5aqq44_aL" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultOnSpecimenIsValidAlias" />
      <node concept="3clFbS" id="4B5aqq44_aM" role="3clF47" />
      <node concept="3Tm1VV" id="4B5aqq44_aN" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq44_aO" role="3clF45" />
    </node>
    <node concept="3clFb_" id="4B5aqq44_aP" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultOnSpecimenIsValidDescription" />
      <node concept="3clFbS" id="4B5aqq44_aQ" role="3clF47" />
      <node concept="3Tm1VV" id="4B5aqq44_aR" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq44_aS" role="3clF45" />
    </node>
    <node concept="3clFb_" id="4B5aqq44L4u" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultOnSpecimenIsValidKeywordPreviousResultIsValid" />
      <node concept="3clFbS" id="4B5aqq44L4v" role="3clF47" />
      <node concept="3Tm1VV" id="4B5aqq44L4w" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq44L4x" role="3clF45" />
    </node>
    <node concept="3clFb_" id="4B5aqq44LrP" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultOnSpecimenIsValidKeywordOnSpecimen" />
      <node concept="3clFbS" id="4B5aqq44LrQ" role="3clF47" />
      <node concept="3Tm1VV" id="4B5aqq44LrR" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq44LrS" role="3clF45" />
    </node>
    <node concept="3Tm1VV" id="4zMac8rUNtQ" role="1B3o_S" />
  </node>
  <node concept="312cEu" id="4zMac8rUO0_">
    <property role="TrG5h" value="EnLaboratoryTestDataTranslations" />
    <property role="3GE5qa" value="localization" />
    <node concept="3clFb_" id="6LTgXmMivLT" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getILaboratoryTestDataValueIncludeSpecimenTypeIntentionDescription" />
      <node concept="3Tm1VV" id="6LTgXmMivLV" role="1B3o_S" />
      <node concept="17QB3L" id="6LTgXmMivLW" role="3clF45" />
      <node concept="3clFbS" id="6LTgXmMivLX" role="3clF47">
        <node concept="3clFbF" id="6LTgXmMiyiV" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmMiyiU" role="3clFbG">
            <property role="Xl_RC" value="Include Specimen Type" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6LTgXmMivLY" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="6LTgXmMiwFe" role="jymVt" />
    <node concept="3clFb_" id="7AAKH6gc$$E" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasRequestAlias" />
      <node concept="3Tm1VV" id="7AAKH6gc$$G" role="1B3o_S" />
      <node concept="17QB3L" id="7AAKH6gc$$H" role="3clF45" />
      <node concept="3clFbS" id="7AAKH6gc$$I" role="3clF47">
        <node concept="3clFbF" id="7AAKH6gcBWi" role="3cqZAp">
          <node concept="Xl_RD" id="7AAKH6gcBWh" role="3clFbG">
            <property role="Xl_RC" value="has requested test" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="7AAKH6gc$$J" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="7AAKH6gc$$K" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasRequestDescription" />
      <node concept="3Tm1VV" id="7AAKH6gc$$M" role="1B3o_S" />
      <node concept="17QB3L" id="7AAKH6gc$$N" role="3clF45" />
      <node concept="3clFbS" id="7AAKH6gc$$O" role="3clF47">
        <node concept="3clFbF" id="7AAKH6gcOPu" role="3cqZAp">
          <node concept="Xl_RD" id="7AAKH6gcOPt" role="3clFbG">
            <property role="Xl_RC" value="check if test is requested" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="7AAKH6gc$$P" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="1mAGFBJlwWa" role="jymVt" />
    <node concept="3clFb_" id="PDjyzjQCbt" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasBeenPerformedAlias" />
      <node concept="3Tm1VV" id="PDjyzjQCbv" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzjQCbw" role="3clF45" />
      <node concept="3clFbS" id="PDjyzjQCbx" role="3clF47">
        <node concept="3clFbF" id="PDjyzjQCUg" role="3cqZAp">
          <node concept="Xl_RD" id="PDjyzjQCUf" role="3clFbG">
            <property role="Xl_RC" value="has performed test" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzjQCby" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzjQCbz" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasBeenPerformedDescription" />
      <node concept="3Tm1VV" id="PDjyzjQCb_" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzjQCbA" role="3clF45" />
      <node concept="3clFbS" id="PDjyzjQCbB" role="3clF47">
        <node concept="3clFbF" id="PDjyzjQCVz" role="3cqZAp">
          <node concept="Xl_RD" id="PDjyzjQCVy" role="3clFbG">
            <property role="Xl_RC" value="check if test has been performed" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzjQCbC" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzjQDAz" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasBeenPerformedOnSpecimenAlias" />
      <node concept="3Tm1VV" id="PDjyzjQDA_" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzjQDAA" role="3clF45" />
      <node concept="3clFbS" id="PDjyzjQDAB" role="3clF47">
        <node concept="3clFbF" id="PDjyzjQFZb" role="3cqZAp">
          <node concept="3cpWs3" id="PDjyzjQFZc" role="3clFbG">
            <node concept="1rXfSq" id="PDjyzjQFZd" role="3uHU7B">
              <ref role="37wK5l" node="PDjyzjQCbt" resolve="getLaboratoryTestHasBeenPerformedAlias" />
            </node>
            <node concept="Xl_RD" id="PDjyzjQFZe" role="3uHU7w">
              <property role="Xl_RC" value=" on" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzjQDAC" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzjQDAD" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasBeenPerformedOnSpecimenDescription" />
      <node concept="3Tm1VV" id="PDjyzjQDAF" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzjQDAG" role="3clF45" />
      <node concept="3clFbS" id="PDjyzjQDAH" role="3clF47">
        <node concept="3clFbF" id="PDjyzjQH0_" role="3cqZAp">
          <node concept="Xl_RD" id="PDjyzjQH0$" role="3clFbG">
            <property role="Xl_RC" value="check if test has been performed on specimen" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzjQDAI" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzjQDAJ" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasBeenPerformedOnSpecimenKeywordHasBeenPerformed" />
      <node concept="3Tm1VV" id="PDjyzjQDAL" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzjQDAM" role="3clF45" />
      <node concept="3clFbS" id="PDjyzjQDAN" role="3clF47">
        <node concept="3clFbF" id="PDjyzjQH5_" role="3cqZAp">
          <node concept="1rXfSq" id="PDjyzjQH5B" role="3clFbG">
            <ref role="37wK5l" node="PDjyzjQCbt" resolve="getLaboratoryTestHasBeenPerformedAlias" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzjQDAO" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzjQDAP" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasBeenPerformedOnSpecimenKeywordOnSpecimen" />
      <node concept="3Tm1VV" id="PDjyzjQDAR" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzjQDAS" role="3clF45" />
      <node concept="3clFbS" id="PDjyzjQDAT" role="3clF47">
        <node concept="3clFbF" id="PDjyzjQIuc" role="3cqZAp">
          <node concept="Xl_RD" id="PDjyzjQIub" role="3clFbG">
            <property role="Xl_RC" value="on" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzjQDAU" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="4B5aqq44Eib" role="jymVt" />
    <node concept="3clFb_" id="17XAtu8bhLh" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultIsValidAlias" />
      <node concept="3Tm1VV" id="17XAtu8bhLj" role="1B3o_S" />
      <node concept="17QB3L" id="17XAtu8bhLk" role="3clF45" />
      <node concept="3clFbS" id="17XAtu8bhLl" role="3clF47">
        <node concept="3clFbF" id="17XAtu8bzuX" role="3cqZAp">
          <node concept="Xl_RD" id="17XAtu8bzuW" role="3clFbG">
            <property role="Xl_RC" value="has valid test result" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="17XAtu8bhLm" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="17XAtu8bhLn" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultIsValidDescription" />
      <node concept="3Tm1VV" id="17XAtu8bhLp" role="1B3o_S" />
      <node concept="17QB3L" id="17XAtu8bhLq" role="3clF45" />
      <node concept="3clFbS" id="17XAtu8bhLr" role="3clF47">
        <node concept="3clFbF" id="17XAtu8bBfo" role="3cqZAp">
          <node concept="Xl_RD" id="17XAtu8bBfn" role="3clFbG">
            <property role="Xl_RC" value="check if current test result has valid value" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="17XAtu8bhLs" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq44F1j" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultOnSpecimenTypeIsValidAlias" />
      <node concept="3Tm1VV" id="4B5aqq44F1l" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq44F1m" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq44F1n" role="3clF47">
        <node concept="3clFbF" id="4B5aqq44O0U" role="3cqZAp">
          <node concept="3cpWs3" id="4B5aqq44OrP" role="3clFbG">
            <node concept="Xl_RD" id="4B5aqq44OID" role="3uHU7w">
              <property role="Xl_RC" value=" on" />
            </node>
            <node concept="1rXfSq" id="4B5aqq44O0T" role="3uHU7B">
              <ref role="37wK5l" node="17XAtu8bhLh" resolve="getLaboratoryTestResultIsValidAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq44F1o" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq44F1p" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultOnSpecimenTypeIsValidDescription" />
      <node concept="3Tm1VV" id="4B5aqq44F1r" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq44F1s" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq44F1t" role="3clF47">
        <node concept="3clFbF" id="4B5aqq44HHk" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq44HHj" role="3clFbG">
            <property role="Xl_RC" value="check if current test result with specimen type has valid value" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq44F1u" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq44PNi" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultOnSpecimenTypeIsValidKeywordHasValidResult" />
      <node concept="3Tm1VV" id="4B5aqq44PNk" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq44PNl" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq44PNm" role="3clF47">
        <node concept="3clFbF" id="4B5aqq44Qkt" role="3cqZAp">
          <node concept="1rXfSq" id="4B5aqq44Qkv" role="3clFbG">
            <ref role="37wK5l" node="17XAtu8bhLh" resolve="getLaboratoryTestResultIsValidAlias" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq44PNn" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq44PNo" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultOnSpecimenTypeIsValidKeywordOnSpecimen" />
      <node concept="3Tm1VV" id="4B5aqq44PNq" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq44PNr" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq44PNs" role="3clF47">
        <node concept="3clFbF" id="4B5aqq44QD4" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq44QD3" role="3clFbG">
            <property role="Xl_RC" value="on" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq44PNt" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="17XAtu8bBhj" role="jymVt" />
    <node concept="3clFb_" id="1mAGFBJluKr" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasPreviouslyBeenPerformedAlias" />
      <node concept="3Tm1VV" id="1mAGFBJluKt" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBJluKu" role="3clF45" />
      <node concept="3clFbS" id="1mAGFBJluKv" role="3clF47">
        <node concept="3clFbF" id="1mAGFBJlv$J" role="3cqZAp">
          <node concept="Xl_RD" id="1mAGFBJlv$I" role="3clFbG">
            <property role="Xl_RC" value="has previous test resut" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="1mAGFBJluKw" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="1mAGFBJluKx" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasPreviouslyBeenPerformedDescription" />
      <node concept="3Tm1VV" id="1mAGFBJluKz" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBJluK$" role="3clF45" />
      <node concept="3clFbS" id="1mAGFBJluK_" role="3clF47">
        <node concept="3clFbF" id="1mAGFBJlvAD" role="3cqZAp">
          <node concept="Xl_RD" id="1mAGFBJlvAC" role="3clFbG">
            <property role="Xl_RC" value="check if test has previously been performed on patient" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="1mAGFBJluKA" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq44F1F" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasPreviouslyBeenPerformedOnSpecimenAlias" />
      <node concept="3Tm1VV" id="4B5aqq44F1H" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq44F1I" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq44F1J" role="3clF47">
        <node concept="3clFbF" id="4B5aqq44XAL" role="3cqZAp">
          <node concept="3cpWs3" id="4B5aqq44Y72" role="3clFbG">
            <node concept="Xl_RD" id="4B5aqq44Yvc" role="3uHU7w">
              <property role="Xl_RC" value=" on" />
            </node>
            <node concept="1rXfSq" id="4B5aqq44XAK" role="3uHU7B">
              <ref role="37wK5l" node="1mAGFBJluKr" resolve="getLaboratoryTestHasPreviouslyBeenPerformedAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq44F1K" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq44F1L" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasPreviouslyBeenPerformedOnSpecimenDescription" />
      <node concept="3Tm1VV" id="4B5aqq44F1N" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq44F1O" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq44F1P" role="3clF47">
        <node concept="3clFbF" id="4B5aqq44Z0U" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq44Z0T" role="3clFbG">
            <property role="Xl_RC" value="check if test has previously been performed on specimen type from patient" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq44F1Q" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq450$b" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasPreviouslyBeenPerformedOnSpecimenKeywordHasPreviousRequest" />
      <node concept="3Tm1VV" id="4B5aqq450$d" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq450$e" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq450$f" role="3clF47">
        <node concept="3clFbF" id="4B5aqq455XR" role="3cqZAp">
          <node concept="1rXfSq" id="4B5aqq44YBS" role="3clFbG">
            <ref role="37wK5l" node="1mAGFBJluKr" resolve="getLaboratoryTestHasPreviouslyBeenPerformedAlias" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq450$g" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq450$h" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasPreviouslyBeenPerformedOnSpecimenKeywordOnSpecimen" />
      <node concept="3Tm1VV" id="4B5aqq450$j" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq450$k" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq450$l" role="3clF47">
        <node concept="3clFbF" id="4B5aqq454c4" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq454c3" role="3clFbG">
            <property role="Xl_RC" value="on" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq450$m" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="4B5aqq454VN" role="jymVt" />
    <node concept="3clFb_" id="1mAGFBJluKN" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultIsValidAlias" />
      <node concept="3Tm1VV" id="1mAGFBJluKP" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBJluKQ" role="3clF45" />
      <node concept="3clFbS" id="1mAGFBJluKR" role="3clF47">
        <node concept="3clFbF" id="1mAGFBJlxk4" role="3cqZAp">
          <node concept="Xl_RD" id="1mAGFBJlxk3" role="3clFbG">
            <property role="Xl_RC" value="has valid previous test result" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="1mAGFBJluKS" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="1mAGFBJluKT" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultIsValidDescription" />
      <node concept="3Tm1VV" id="1mAGFBJluKV" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBJluKW" role="3clF45" />
      <node concept="3clFbS" id="1mAGFBJluKX" role="3clF47">
        <node concept="3clFbF" id="1mAGFBJlxlY" role="3cqZAp">
          <node concept="Xl_RD" id="1mAGFBJlxlX" role="3clFbG">
            <property role="Xl_RC" value="check if previous result of test on patient has valid value" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="1mAGFBJluKY" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="6khVixyeRkT" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasRequestWithSpecimenAlias" />
      <node concept="3Tm1VV" id="6khVixyeRkV" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyeRkW" role="3clF45" />
      <node concept="3clFbS" id="6khVixyeRkX" role="3clF47">
        <node concept="3clFbF" id="6khVixyeR$z" role="3cqZAp">
          <node concept="3cpWs3" id="4B5aqq44PrK" role="3clFbG">
            <node concept="Xl_RD" id="4B5aqq44PIA" role="3uHU7w">
              <property role="Xl_RC" value=" with" />
            </node>
            <node concept="1rXfSq" id="4B5aqq44OWe" role="3uHU7B">
              <ref role="37wK5l" node="7AAKH6gc$$E" resolve="getLaboratoryTestHasRequestAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixyeRkY" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="6khVixyeRkZ" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasRequestWithSpecimenDescription" />
      <node concept="3Tm1VV" id="6khVixyeRl1" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyeRl2" role="3clF45" />
      <node concept="3clFbS" id="6khVixyeRl3" role="3clF47">
        <node concept="3clFbF" id="6khVixyeSbn" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixyeSbm" role="3clFbG">
            <property role="Xl_RC" value="check if test has been requested with specimen type" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixyeRl4" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq44T8d" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasRequestWithSpecimenKeywordTestHasRequest" />
      <node concept="3Tm1VV" id="4B5aqq44T8f" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq44T8g" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq44T8h" role="3clF47">
        <node concept="3clFbF" id="4B5aqq44TFl" role="3cqZAp">
          <node concept="1rXfSq" id="4B5aqq44TFn" role="3clFbG">
            <ref role="37wK5l" node="7AAKH6gc$$E" resolve="getLaboratoryTestHasRequestAlias" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq44T8i" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq44T8j" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasRequestWithSpecimenKeywordWithSpecimen" />
      <node concept="3Tm1VV" id="4B5aqq44T8l" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq44T8m" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq44T8n" role="3clF47">
        <node concept="3clFbF" id="4B5aqq44U5Q" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq44U5P" role="3clFbG">
            <property role="Xl_RC" value="with" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq44T8o" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq44F1R" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultOnSpecimenIsValidAlias" />
      <node concept="3Tm1VV" id="4B5aqq44F1T" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq44F1U" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq44F1V" role="3clF47">
        <node concept="3clFbF" id="4B5aqq44YBQ" role="3cqZAp">
          <node concept="3cpWs3" id="4B5aqq459bK" role="3clFbG">
            <node concept="Xl_RD" id="4B5aqq459Bu" role="3uHU7w">
              <property role="Xl_RC" value=" on" />
            </node>
            <node concept="1rXfSq" id="4B5aqq458vd" role="3uHU7B">
              <ref role="37wK5l" node="1mAGFBJluKN" resolve="getLaboratoryTestPreviousResultIsValidAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq44F1W" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq44F1X" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultOnSpecimenIsValidDescription" />
      <node concept="3Tm1VV" id="4B5aqq44F1Z" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq44F20" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq44F21" role="3clF47">
        <node concept="3clFbF" id="4B5aqq44Z01" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq44Z00" role="3clFbG">
            <property role="Xl_RC" value="check if previous result of test on specimen from patient has valid value" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq44F22" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq450$n" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultOnSpecimenIsValidKeywordPreviousResultIsValid" />
      <node concept="3Tm1VV" id="4B5aqq450$p" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq450$q" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq450$r" role="3clF47">
        <node concept="3clFbF" id="4B5aqq459Kw" role="3cqZAp">
          <node concept="1rXfSq" id="4B5aqq45hUg" role="3clFbG">
            <ref role="37wK5l" node="1mAGFBJluKN" resolve="getLaboratoryTestPreviousResultIsValidAlias" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq450$s" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq450$t" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultOnSpecimenIsValidKeywordOnSpecimen" />
      <node concept="3Tm1VV" id="4B5aqq450$v" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq450$w" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq450$x" role="3clF47">
        <node concept="3clFbF" id="4B5aqq45aG2" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq45aG1" role="3clFbG">
            <property role="Xl_RC" value="on" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq450$y" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="6khVixyeR9X" role="jymVt" />
    <node concept="3clFb_" id="17XAtu8bhLt" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestTimeSinceLastPerformedAlias" />
      <node concept="3Tm1VV" id="17XAtu8bhLv" role="1B3o_S" />
      <node concept="17QB3L" id="17XAtu8bhLw" role="3clF45" />
      <node concept="3clFbS" id="17XAtu8bhLx" role="3clF47">
        <node concept="3clFbF" id="17XAtu8bCxK" role="3cqZAp">
          <node concept="Xl_RD" id="17XAtu8bCxJ" role="3clFbG">
            <property role="Xl_RC" value="time since previous test" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="17XAtu8bhLy" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="17XAtu8bhLz" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestTimeSinceLastPerformedDescription" />
      <node concept="3Tm1VV" id="17XAtu8bhL_" role="1B3o_S" />
      <node concept="17QB3L" id="17XAtu8bhLA" role="3clF45" />
      <node concept="3clFbS" id="17XAtu8bhLB" role="3clF47">
        <node concept="3clFbF" id="17XAtu8bCzm" role="3cqZAp">
          <node concept="Xl_RD" id="17XAtu8bCzl" role="3clFbG">
            <property role="Xl_RC" value="check time since test was last performed on patient" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="17XAtu8bhLC" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq44F1v" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestTimeSinceLastPerformedOnSpecimenAlias" />
      <node concept="3Tm1VV" id="4B5aqq44F1x" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq44F1y" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq44F1z" role="3clF47">
        <node concept="3clFbF" id="4B5aqq44I_K" role="3cqZAp">
          <node concept="3cpWs3" id="4B5aqq44VOn" role="3clFbG">
            <node concept="Xl_RD" id="4B5aqq44Wcx" role="3uHU7w">
              <property role="Xl_RC" value=" on" />
            </node>
            <node concept="1rXfSq" id="4B5aqq44VfY" role="3uHU7B">
              <ref role="37wK5l" node="17XAtu8bhLt" resolve="getLaboratoryTestTimeSinceLastPerformedAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq44F1$" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq44F1_" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestTimeSinceLastPerformedOnSpecimenDescription" />
      <node concept="3Tm1VV" id="4B5aqq44F1B" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq44F1C" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq44F1D" role="3clF47">
        <node concept="3clFbF" id="4B5aqq44IBt" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq44IBs" role="3clFbG">
            <property role="Xl_RC" value="check time since test was last performed on specimen type from patient" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq44F1E" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq44U_E" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestTimeSinceLastPerformedOnSpecimenKeywordTimeSinceLastRequested" />
      <node concept="3Tm1VV" id="4B5aqq44U_G" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq44U_H" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq44U_I" role="3clF47">
        <node concept="3clFbF" id="4B5aqq44WpF" role="3cqZAp">
          <node concept="1rXfSq" id="4B5aqq44WpH" role="3clFbG">
            <ref role="37wK5l" node="17XAtu8bhLt" resolve="getLaboratoryTestTimeSinceLastPerformedAlias" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq44U_J" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq44U_K" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestTimeSinceLastPerformedOnSpecimenKeywordOnSpecimen" />
      <node concept="3Tm1VV" id="4B5aqq44U_M" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq44U_N" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq44U_O" role="3clF47">
        <node concept="3clFbF" id="4B5aqq44WPK" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq44X_U" role="3clFbG">
            <property role="Xl_RC" value="on" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq44U_P" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3Tm1VV" id="4zMac8rUO0A" role="1B3o_S" />
    <node concept="3uibUv" id="4zMac8rUO1F" role="EKbjA">
      <ref role="3uigEE" node="4zMac8rUNtP" resolve="ILaboratoryTestDataTranslations" />
    </node>
  </node>
  <node concept="312cEu" id="4zMac8rUO1O">
    <property role="TrG5h" value="NoLaboratoryTestDataTranslations" />
    <property role="3GE5qa" value="localization" />
    <node concept="3clFb_" id="6LTgXmMiRco" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getILaboratoryTestDataValueIncludeSpecimenTypeIntentionDescription" />
      <node concept="3Tm1VV" id="6LTgXmMiRcq" role="1B3o_S" />
      <node concept="17QB3L" id="6LTgXmMiRcr" role="3clF45" />
      <node concept="3clFbS" id="6LTgXmMiRcs" role="3clF47">
        <node concept="3clFbF" id="6LTgXmMiSTZ" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmMiSTY" role="3clFbG">
            <property role="Xl_RC" value="Inkluder Materiale" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6LTgXmMiRct" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="6LTgXmMiQpt" role="jymVt" />
    <node concept="3clFb_" id="7AAKH6gcakN" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasRequestAlias" />
      <node concept="3Tm1VV" id="7AAKH6gcakP" role="1B3o_S" />
      <node concept="17QB3L" id="7AAKH6gcakQ" role="3clF45" />
      <node concept="3clFbS" id="7AAKH6gcakR" role="3clF47">
        <node concept="3clFbF" id="7AAKH6gcc91" role="3cqZAp">
          <node concept="Xl_RD" id="7AAKH6gcc90" role="3clFbG">
            <property role="Xl_RC" value="har rekvirert analyse" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="7AAKH6gcakS" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="7AAKH6gcakT" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasRequestDescription" />
      <node concept="3Tm1VV" id="7AAKH6gcakV" role="1B3o_S" />
      <node concept="17QB3L" id="7AAKH6gcakW" role="3clF45" />
      <node concept="3clFbS" id="7AAKH6gcakX" role="3clF47">
        <node concept="3clFbF" id="7AAKH6gccac" role="3cqZAp">
          <node concept="Xl_RD" id="7AAKH6gccab" role="3clFbG">
            <property role="Xl_RC" value="sjekk om analyse er rekvirert for gjeldende prøve" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="7AAKH6gcakY" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq45foi" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasRequestWithSpecimenAlias" />
      <node concept="3Tm1VV" id="4B5aqq45foj" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq45fok" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq45fol" role="3clF47">
        <node concept="3clFbF" id="4B5aqq45fom" role="3cqZAp">
          <node concept="3cpWs3" id="4B5aqq45fon" role="3clFbG">
            <node concept="Xl_RD" id="4B5aqq45foo" role="3uHU7w">
              <property role="Xl_RC" value=" med" />
            </node>
            <node concept="1rXfSq" id="4B5aqq45fop" role="3uHU7B">
              <ref role="37wK5l" node="7AAKH6gcakN" resolve="getLaboratoryTestHasRequestAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq45foq" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq45for" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasRequestWithSpecimenDescription" />
      <node concept="3Tm1VV" id="4B5aqq45fos" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq45fot" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq45fou" role="3clF47">
        <node concept="3clFbF" id="4B5aqq45fov" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq45fow" role="3clFbG">
            <property role="Xl_RC" value="sjekk om analyse med materiale er rekvirert for gjeldende prøve" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq45fox" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq45foy" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasRequestWithSpecimenKeywordTestHasRequest" />
      <node concept="3Tm1VV" id="4B5aqq45foz" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq45fo$" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq45fo_" role="3clF47">
        <node concept="3clFbF" id="4B5aqq45foA" role="3cqZAp">
          <node concept="1rXfSq" id="4B5aqq45foB" role="3clFbG">
            <ref role="37wK5l" node="7AAKH6gcakN" resolve="getLaboratoryTestHasRequestAlias" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq45foC" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq45foD" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasRequestWithSpecimenKeywordWithSpecimen" />
      <node concept="3Tm1VV" id="4B5aqq45foE" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq45foF" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq45foG" role="3clF47">
        <node concept="3clFbF" id="4B5aqq45foH" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq45foI" role="3clFbG">
            <property role="Xl_RC" value="med" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq45foJ" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="17XAtu89Iwr" role="jymVt" />
    <node concept="2tJIrI" id="6LTgXmMiPu6" role="jymVt" />
    <node concept="3clFb_" id="17XAtu89JX8" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultIsValidAlias" />
      <node concept="3Tm1VV" id="17XAtu89JXa" role="1B3o_S" />
      <node concept="17QB3L" id="17XAtu89JXb" role="3clF45" />
      <node concept="3clFbS" id="17XAtu89JXc" role="3clF47">
        <node concept="3clFbF" id="17XAtu89N7Y" role="3cqZAp">
          <node concept="Xl_RD" id="17XAtu89N7X" role="3clFbG">
            <property role="Xl_RC" value="har gyldig analysesvar" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="17XAtu89JXd" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="17XAtu89JXe" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultIsValidDescription" />
      <node concept="3Tm1VV" id="17XAtu89JXg" role="1B3o_S" />
      <node concept="17QB3L" id="17XAtu89JXh" role="3clF45" />
      <node concept="3clFbS" id="17XAtu89JXi" role="3clF47">
        <node concept="3clFbF" id="17XAtu89Na_" role="3cqZAp">
          <node concept="Xl_RD" id="17XAtu89Na$" role="3clFbG">
            <property role="Xl_RC" value="sjekk om gjeldende svar for analyse har en gyldig verdi" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="17XAtu89JXj" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq45fnN" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultOnSpecimenTypeIsValidAlias" />
      <node concept="3Tm1VV" id="4B5aqq45fnO" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq45fnP" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq45fnQ" role="3clF47">
        <node concept="3clFbF" id="4B5aqq45fnR" role="3cqZAp">
          <node concept="3cpWs3" id="4B5aqq45fnS" role="3clFbG">
            <node concept="1rXfSq" id="4B5aqq45fnU" role="3uHU7B">
              <ref role="37wK5l" node="17XAtu89JX8" resolve="getLaboratoryTestResultIsValidAlias" />
            </node>
            <node concept="Xl_RD" id="4B5aqq4hSMo" role="3uHU7w">
              <property role="Xl_RC" value=" på" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq45fnV" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq45fnW" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultOnSpecimenTypeIsValidDescription" />
      <node concept="3Tm1VV" id="4B5aqq45fnX" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq45fnY" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq45fnZ" role="3clF47">
        <node concept="3clFbF" id="4B5aqq45fo0" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq45fo1" role="3clFbG">
            <property role="Xl_RC" value="sjekk om gjeldende svar for analyse med materiale har en gyldig verdi" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq45fo2" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq45fo3" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultOnSpecimenTypeIsValidKeywordHasValidResult" />
      <node concept="3Tm1VV" id="4B5aqq45fo4" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq45fo5" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq45fo6" role="3clF47">
        <node concept="3clFbF" id="4B5aqq45fo7" role="3cqZAp">
          <node concept="1rXfSq" id="4B5aqq45fo8" role="3clFbG">
            <ref role="37wK5l" node="17XAtu89JX8" resolve="getLaboratoryTestResultIsValidAlias" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq45fo9" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq45foa" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestResultOnSpecimenTypeIsValidKeywordOnSpecimen" />
      <node concept="3Tm1VV" id="4B5aqq45fob" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq45foc" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq45fod" role="3clF47">
        <node concept="3clFbF" id="4B5aqq45foe" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq45fof" role="3clFbG">
            <property role="Xl_RC" value="på" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq45fog" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="17XAtu82q9J" role="jymVt" />
    <node concept="2tJIrI" id="6LTgXmMiOFo" role="jymVt" />
    <node concept="3clFb_" id="17XAtu82oMQ" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestTimeSinceLastPerformedAlias" />
      <node concept="3Tm1VV" id="17XAtu82oMS" role="1B3o_S" />
      <node concept="17QB3L" id="17XAtu82oMT" role="3clF45" />
      <node concept="3clFbS" id="17XAtu82oMU" role="3clF47">
        <node concept="3clFbF" id="17XAtu82rlF" role="3cqZAp">
          <node concept="Xl_RD" id="17XAtu82rlE" role="3clFbG">
            <property role="Xl_RC" value="tid siden forrige gang analyse ble utført" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="17XAtu82oMV" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="17XAtu82oMW" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestTimeSinceLastPerformedDescription" />
      <node concept="3Tm1VV" id="17XAtu82oMY" role="1B3o_S" />
      <node concept="17QB3L" id="17XAtu82oMZ" role="3clF45" />
      <node concept="3clFbS" id="17XAtu82oN0" role="3clF47">
        <node concept="3clFbF" id="17XAtu82rmZ" role="3cqZAp">
          <node concept="Xl_RD" id="17XAtu82rmY" role="3clFbG">
            <property role="Xl_RC" value="sjekk tid siden analyse sist ble utført på pasient" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="17XAtu82oN1" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq45foL" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestTimeSinceLastPerformedOnSpecimenAlias" />
      <node concept="3Tm1VV" id="4B5aqq45foM" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq45foN" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq45foO" role="3clF47">
        <node concept="3clFbF" id="4B5aqq45foP" role="3cqZAp">
          <node concept="3cpWs3" id="4B5aqq45foQ" role="3clFbG">
            <node concept="Xl_RD" id="4B5aqq45foR" role="3uHU7w">
              <property role="Xl_RC" value=" på" />
            </node>
            <node concept="1rXfSq" id="4B5aqq45foS" role="3uHU7B">
              <ref role="37wK5l" node="17XAtu82oMQ" resolve="getLaboratoryTestTimeSinceLastPerformedAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq45foT" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq45foU" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestTimeSinceLastPerformedOnSpecimenDescription" />
      <node concept="3Tm1VV" id="4B5aqq45foV" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq45foW" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq45foX" role="3clF47">
        <node concept="3clFbF" id="4B5aqq45foY" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq45foZ" role="3clFbG">
            <property role="Xl_RC" value="sjekk tid siden analyse på materiale fra pasient sist ble utført" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq45fp0" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq45fp1" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestTimeSinceLastPerformedOnSpecimenKeywordTimeSinceLastRequested" />
      <node concept="3Tm1VV" id="4B5aqq45fp2" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq45fp3" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq45fp4" role="3clF47">
        <node concept="3clFbF" id="4B5aqq45fp5" role="3cqZAp">
          <node concept="1rXfSq" id="4B5aqq45fp6" role="3clFbG">
            <ref role="37wK5l" node="17XAtu82oMQ" resolve="getLaboratoryTestTimeSinceLastPerformedAlias" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq45fp7" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq45fp8" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestTimeSinceLastPerformedOnSpecimenKeywordOnSpecimen" />
      <node concept="3Tm1VV" id="4B5aqq45fp9" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq45fpa" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq45fpb" role="3clF47">
        <node concept="3clFbF" id="4B5aqq45fpc" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq45fpd" role="3clFbG">
            <property role="Xl_RC" value="på" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq45fpe" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="1mAGFBJlpMe" role="jymVt" />
    <node concept="2tJIrI" id="6LTgXmMiNSG" role="jymVt" />
    <node concept="3clFb_" id="1mAGFBJlqkR" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasPreviouslyBeenPerformedAlias" />
      <node concept="3Tm1VV" id="1mAGFBJlqkT" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBJlqkU" role="3clF45" />
      <node concept="3clFbS" id="1mAGFBJlqkV" role="3clF47">
        <node concept="3clFbF" id="1mAGFBJlrpO" role="3cqZAp">
          <node concept="Xl_RD" id="1mAGFBJlrpN" role="3clFbG">
            <property role="Xl_RC" value="har tidligere analysesvar" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="1mAGFBJlqkW" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="1mAGFBJlqkX" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasPreviouslyBeenPerformedDescription" />
      <node concept="3Tm1VV" id="1mAGFBJlqkZ" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBJlql0" role="3clF45" />
      <node concept="3clFbS" id="1mAGFBJlql1" role="3clF47">
        <node concept="3clFbF" id="1mAGFBJlrqU" role="3cqZAp">
          <node concept="Xl_RD" id="1mAGFBJlrqT" role="3clFbG">
            <property role="Xl_RC" value="sjekk om analyse har tidligere blitt rekvirert på pasient" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="1mAGFBJlql2" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq45fpg" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasPreviouslyBeenPerformedOnSpecimenAlias" />
      <node concept="3Tm1VV" id="4B5aqq45fph" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq45fpi" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq45fpj" role="3clF47">
        <node concept="3clFbF" id="4B5aqq45fpk" role="3cqZAp">
          <node concept="3cpWs3" id="4B5aqq45fpl" role="3clFbG">
            <node concept="Xl_RD" id="4B5aqq45fpm" role="3uHU7w">
              <property role="Xl_RC" value=" på" />
            </node>
            <node concept="1rXfSq" id="4B5aqq45fpn" role="3uHU7B">
              <ref role="37wK5l" node="1mAGFBJlqkR" resolve="getLaboratoryTestHasPreviouslyBeenPerformedAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq45fpo" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq45fpp" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasPreviouslyBeenPerformedOnSpecimenDescription" />
      <node concept="3Tm1VV" id="4B5aqq45fpq" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq45fpr" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq45fps" role="3clF47">
        <node concept="3clFbF" id="4B5aqq45fpt" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq45fpu" role="3clFbG">
            <property role="Xl_RC" value="sjekk om analyse med materiale fra pasient har tidligere blitt utført" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq45fpv" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq45fpw" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasPreviouslyBeenPerformedOnSpecimenKeywordHasPreviousRequest" />
      <node concept="3Tm1VV" id="4B5aqq45fpx" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq45fpy" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq45fpz" role="3clF47">
        <node concept="3clFbF" id="4B5aqq45fp$" role="3cqZAp">
          <node concept="1rXfSq" id="4B5aqq45fp_" role="3clFbG">
            <ref role="37wK5l" node="1mAGFBJlqkR" resolve="getLaboratoryTestHasPreviouslyBeenPerformedAlias" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq45fpA" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq45fpB" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasPreviouslyBeenPerformedOnSpecimenKeywordOnSpecimen" />
      <node concept="3Tm1VV" id="4B5aqq45fpC" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq45fpD" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq45fpE" role="3clF47">
        <node concept="3clFbF" id="4B5aqq45fpF" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq45fpG" role="3clFbG">
            <property role="Xl_RC" value="på" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq45fpH" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="1mAGFBJlr48" role="jymVt" />
    <node concept="2tJIrI" id="6LTgXmMiN62" role="jymVt" />
    <node concept="3clFb_" id="1mAGFBJlsYR" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultIsValidAlias" />
      <node concept="3Tm1VV" id="1mAGFBJlsYT" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBJlsYU" role="3clF45" />
      <node concept="3clFbS" id="1mAGFBJlsYV" role="3clF47">
        <node concept="3clFbF" id="1mAGFBJltrb" role="3cqZAp">
          <node concept="Xl_RD" id="1mAGFBJltra" role="3clFbG">
            <property role="Xl_RC" value="har gyldig forrige analysesvar" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="1mAGFBJlsYW" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="1mAGFBJlsYX" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultIsValidDescription" />
      <node concept="3Tm1VV" id="1mAGFBJlsYZ" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBJlsZ0" role="3clF45" />
      <node concept="3clFbS" id="1mAGFBJlsZ1" role="3clF47">
        <node concept="3clFbF" id="1mAGFBJltv7" role="3cqZAp">
          <node concept="Xl_RD" id="1mAGFBJltv6" role="3clFbG">
            <property role="Xl_RC" value="sjekk om forrige svar fra analyse på pasient har gyldig verdi" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="1mAGFBJlsZ2" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq45fpJ" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultOnSpecimenIsValidAlias" />
      <node concept="3Tm1VV" id="4B5aqq45fpK" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq45fpL" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq45fpM" role="3clF47">
        <node concept="3clFbF" id="4B5aqq45fpN" role="3cqZAp">
          <node concept="3cpWs3" id="4B5aqq45fpO" role="3clFbG">
            <node concept="Xl_RD" id="4B5aqq45fpP" role="3uHU7w">
              <property role="Xl_RC" value=" på" />
            </node>
            <node concept="1rXfSq" id="4B5aqq45fpQ" role="3uHU7B">
              <ref role="37wK5l" node="1mAGFBJlsYR" resolve="getLaboratoryTestPreviousResultIsValidAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq45fpR" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq45fpS" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultOnSpecimenIsValidDescription" />
      <node concept="3Tm1VV" id="4B5aqq45fpT" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq45fpU" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq45fpV" role="3clF47">
        <node concept="3clFbF" id="4B5aqq45fpW" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq45fpX" role="3clFbG">
            <property role="Xl_RC" value="sjekk om forrige svar fra analyse med materiale fra pasient har gyldig verdi" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq45fpY" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq45fpZ" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultOnSpecimenIsValidKeywordPreviousResultIsValid" />
      <node concept="3Tm1VV" id="4B5aqq45fq0" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq45fq1" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq45fq2" role="3clF47">
        <node concept="3clFbF" id="4B5aqq45fq3" role="3cqZAp">
          <node concept="1rXfSq" id="4B5aqq45hjQ" role="3clFbG">
            <ref role="37wK5l" node="1mAGFBJlsYR" resolve="getLaboratoryTestPreviousResultIsValidAlias" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq45fq5" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="4B5aqq45fq6" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestPreviousResultOnSpecimenIsValidKeywordOnSpecimen" />
      <node concept="3Tm1VV" id="4B5aqq45fq7" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq45fq8" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq45fq9" role="3clF47">
        <node concept="3clFbF" id="4B5aqq45fqa" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq45fqb" role="3clFbG">
            <property role="Xl_RC" value="på" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4B5aqq45fqc" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="4B5aqq45esJ" role="jymVt" />
    <node concept="2tJIrI" id="6LTgXmMiMjq" role="jymVt" />
    <node concept="3clFb_" id="PDjyzjQuQa" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasBeenPerformedAlias" />
      <node concept="3Tm1VV" id="PDjyzjQuQc" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzjQuQd" role="3clF45" />
      <node concept="3clFbS" id="PDjyzjQuQe" role="3clF47">
        <node concept="3clFbF" id="PDjyzjQv$Y" role="3cqZAp">
          <node concept="Xl_RD" id="PDjyzjQv$X" role="3clFbG">
            <property role="Xl_RC" value="har utført" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzjQuQf" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzjQuQg" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasBeenPerformedDescription" />
      <node concept="3Tm1VV" id="PDjyzjQuQi" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzjQuQj" role="3clF45" />
      <node concept="3clFbS" id="PDjyzjQuQk" role="3clF47">
        <node concept="3clFbF" id="PDjyzjQvAS" role="3cqZAp">
          <node concept="Xl_RD" id="PDjyzjQvAR" role="3clFbG">
            <property role="Xl_RC" value="sjekk om analyse har blitt utført" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzjQuQl" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzjQwj7" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasBeenPerformedOnSpecimenAlias" />
      <node concept="3Tm1VV" id="PDjyzjQwj9" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzjQwja" role="3clF45" />
      <node concept="3clFbS" id="PDjyzjQwjb" role="3clF47">
        <node concept="3clFbF" id="PDjyzjQxMw" role="3cqZAp">
          <node concept="3cpWs3" id="PDjyzjQymz" role="3clFbG">
            <node concept="Xl_RD" id="PDjyzjQymR" role="3uHU7w">
              <property role="Xl_RC" value=" på" />
            </node>
            <node concept="1rXfSq" id="PDjyzjQxMv" role="3uHU7B">
              <ref role="37wK5l" node="PDjyzjQuQa" resolve="getLaboratoryTestHasBeenPerformedAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzjQwjc" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzjQ$mk" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasBeenPerformedOnSpecimenDescription" />
      <node concept="3Tm1VV" id="PDjyzjQ$mm" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzjQ$mn" role="3clF45" />
      <node concept="3clFbS" id="PDjyzjQ$mo" role="3clF47">
        <node concept="3clFbF" id="PDjyzjQz4e" role="3cqZAp">
          <node concept="Xl_RD" id="PDjyzjQz4d" role="3clFbG">
            <property role="Xl_RC" value="sjekk om analyse har blitt utført på materiale" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzjQ$mp" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzjQwjd" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasBeenPerformedOnSpecimenKeywordHasBeenPerformed" />
      <node concept="3Tm1VV" id="PDjyzjQwjf" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzjQwjg" role="3clF45" />
      <node concept="3clFbS" id="PDjyzjQwjh" role="3clF47">
        <node concept="3clFbF" id="PDjyzjQ_eJ" role="3cqZAp">
          <node concept="1rXfSq" id="PDjyzjQ_eL" role="3clFbG">
            <ref role="37wK5l" node="PDjyzjQuQa" resolve="getLaboratoryTestHasBeenPerformedAlias" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzjQwji" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzjQwjj" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestHasBeenPerformedOnSpecimenKeywordOnSpecimen" />
      <node concept="3Tm1VV" id="PDjyzjQwjl" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzjQwjm" role="3clF45" />
      <node concept="3clFbS" id="PDjyzjQwjn" role="3clF47">
        <node concept="3clFbF" id="PDjyzjQz5V" role="3cqZAp">
          <node concept="Xl_RD" id="PDjyzjQz5U" role="3clFbG">
            <property role="Xl_RC" value="på" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzjQwjo" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3Tm1VV" id="4zMac8rUO1P" role="1B3o_S" />
    <node concept="3uibUv" id="4zMac8rUO2g" role="EKbjA">
      <ref role="3uigEE" node="4zMac8rUNtP" resolve="ILaboratoryTestDataTranslations" />
    </node>
  </node>
</model>

