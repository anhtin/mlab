package no.uio.mLab.decisions.data.laboratoryTest.translations;

/*Generated by MPS */


public interface ILaboratoryTestDataTranslations {
  String getILaboratoryTestDataValueIncludeSpecimenTypeIntentionDescription();

  String getLaboratoryTestHasRequestAlias();
  String getLaboratoryTestHasRequestDescription();
  String getLaboratoryTestHasRequestWithSpecimenAlias();
  String getLaboratoryTestHasRequestWithSpecimenDescription();
  String getLaboratoryTestHasRequestWithSpecimenKeywordTestHasRequest();
  String getLaboratoryTestHasRequestWithSpecimenKeywordWithSpecimen();

  String getLaboratoryTestHasBeenPerformedAlias();
  String getLaboratoryTestHasBeenPerformedDescription();
  String getLaboratoryTestHasBeenPerformedOnSpecimenAlias();
  String getLaboratoryTestHasBeenPerformedOnSpecimenDescription();
  String getLaboratoryTestHasBeenPerformedOnSpecimenKeywordHasBeenPerformed();
  String getLaboratoryTestHasBeenPerformedOnSpecimenKeywordOnSpecimen();

  String getLaboratoryTestResultIsValidAlias();
  String getLaboratoryTestResultIsValidDescription();
  String getLaboratoryTestResultOnSpecimenTypeIsValidAlias();
  String getLaboratoryTestResultOnSpecimenTypeIsValidDescription();
  String getLaboratoryTestResultOnSpecimenTypeIsValidKeywordHasValidResult();
  String getLaboratoryTestResultOnSpecimenTypeIsValidKeywordOnSpecimen();

  String getLaboratoryTestTimeSinceLastPerformedAlias();
  String getLaboratoryTestTimeSinceLastPerformedDescription();
  String getLaboratoryTestTimeSinceLastPerformedOnSpecimenAlias();
  String getLaboratoryTestTimeSinceLastPerformedOnSpecimenDescription();
  String getLaboratoryTestTimeSinceLastPerformedOnSpecimenKeywordTimeSinceLastRequested();
  String getLaboratoryTestTimeSinceLastPerformedOnSpecimenKeywordOnSpecimen();

  String getLaboratoryTestHasPreviouslyBeenPerformedAlias();
  String getLaboratoryTestHasPreviouslyBeenPerformedDescription();
  String getLaboratoryTestHasPreviouslyBeenPerformedOnSpecimenAlias();
  String getLaboratoryTestHasPreviouslyBeenPerformedOnSpecimenDescription();
  String getLaboratoryTestHasPreviouslyBeenPerformedOnSpecimenKeywordHasPreviousRequest();
  String getLaboratoryTestHasPreviouslyBeenPerformedOnSpecimenKeywordOnSpecimen();

  String getLaboratoryTestPreviousResultIsValidAlias();
  String getLaboratoryTestPreviousResultIsValidDescription();
  String getLaboratoryTestPreviousResultOnSpecimenIsValidAlias();
  String getLaboratoryTestPreviousResultOnSpecimenIsValidDescription();
  String getLaboratoryTestPreviousResultOnSpecimenIsValidKeywordPreviousResultIsValid();
  String getLaboratoryTestPreviousResultOnSpecimenIsValidKeywordOnSpecimen();
}
