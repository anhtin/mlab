<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:4677f140-6d3c-4aba-9fab-678c9a5f7c1f(no.uio.mLab.decisions.data.laboratoryTest.runtime)">
  <persistence version="9" />
  <languages>
    <use id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage" version="6" />
  </languages>
  <imports>
    <import index="8r9s" ref="r:0e143259-7e64-4fad-906c-02a97b75504d(no.uio.mLab.decisions.core.runtime)" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" />
    <import index="mygi" ref="r:d014d2e6-828c-4ff5-a027-d2fa848d3c1b(no.uio.mLab.decisions.references.laboratoryTest.runtime)" />
    <import index="33ny" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.util(JDK/)" />
    <import index="28m1" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.time(JDK/)" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1080223426719" name="jetbrains.mps.baseLanguage.structure.OrExpression" flags="nn" index="22lmx$" />
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1465982738277781862" name="jetbrains.mps.baseLanguage.structure.PlaceholderMember" flags="ng" index="2tJIrI" />
      <concept id="1188207840427" name="jetbrains.mps.baseLanguage.structure.AnnotationInstance" flags="nn" index="2AHcQZ">
        <reference id="1188208074048" name="annotation" index="2AI5Lk" />
      </concept>
      <concept id="1188208481402" name="jetbrains.mps.baseLanguage.structure.HasAnnotation" flags="ng" index="2AJDlI">
        <child id="1188208488637" name="annotation" index="2AJF6D" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1197029447546" name="jetbrains.mps.baseLanguage.structure.FieldReferenceOperation" flags="nn" index="2OwXpG">
        <reference id="1197029500499" name="fieldDeclaration" index="2Oxat5" />
      </concept>
      <concept id="1070475354124" name="jetbrains.mps.baseLanguage.structure.ThisExpression" flags="nn" index="Xjq3P" />
      <concept id="1070475587102" name="jetbrains.mps.baseLanguage.structure.SuperConstructorInvocation" flags="nn" index="XkiVB" />
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1081256982272" name="jetbrains.mps.baseLanguage.structure.InstanceOfExpression" flags="nn" index="2ZW3vV">
        <child id="1081256993305" name="classType" index="2ZW6by" />
        <child id="1081256993304" name="leftExpression" index="2ZW6bz" />
      </concept>
      <concept id="1070534058343" name="jetbrains.mps.baseLanguage.structure.NullLiteral" flags="nn" index="10Nm6u" />
      <concept id="1070534370425" name="jetbrains.mps.baseLanguage.structure.IntegerType" flags="in" index="10Oyi0" />
      <concept id="1070534644030" name="jetbrains.mps.baseLanguage.structure.BooleanType" flags="in" index="10P_77" />
      <concept id="1070534934090" name="jetbrains.mps.baseLanguage.structure.CastExpression" flags="nn" index="10QFUN">
        <child id="1070534934091" name="type" index="10QFUM" />
        <child id="1070534934092" name="expression" index="10QFUP" />
      </concept>
      <concept id="1068390468200" name="jetbrains.mps.baseLanguage.structure.FieldDeclaration" flags="ig" index="312cEg">
        <property id="8606350594693632173" name="isTransient" index="eg7rD" />
        <property id="1240249534625" name="isVolatile" index="34CwA1" />
      </concept>
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu">
        <property id="1075300953594" name="abstractClass" index="1sVAO0" />
        <child id="1165602531693" name="superclass" index="1zkMxy" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <property id="1176718929932" name="isFinal" index="3TUv4t" />
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <property id="4276006055363816570" name="isSynchronized" index="od$2w" />
        <property id="1181808852946" name="isFinal" index="DiZV1" />
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123165" name="jetbrains.mps.baseLanguage.structure.InstanceMethodDeclaration" flags="ig" index="3clFb_">
        <property id="1178608670077" name="isAbstract" index="1EzhhJ" />
      </concept>
      <concept id="1068580123152" name="jetbrains.mps.baseLanguage.structure.EqualsExpression" flags="nn" index="3clFbC" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123137" name="jetbrains.mps.baseLanguage.structure.BooleanConstant" flags="nn" index="3clFbT">
        <property id="1068580123138" name="value" index="3clFbU" />
      </concept>
      <concept id="1068580123140" name="jetbrains.mps.baseLanguage.structure.ConstructorDeclaration" flags="ig" index="3clFbW" />
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1068581517677" name="jetbrains.mps.baseLanguage.structure.VoidType" flags="in" index="3cqZAl" />
      <concept id="1079359253375" name="jetbrains.mps.baseLanguage.structure.ParenthesizedExpression" flags="nn" index="1eOMI4">
        <child id="1079359253376" name="expression" index="1eOMHV" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1107461130800" name="jetbrains.mps.baseLanguage.structure.Classifier" flags="ng" index="3pOWGL">
        <child id="5375687026011219971" name="member" index="jymVt" unordered="true" />
      </concept>
      <concept id="7812454656619025416" name="jetbrains.mps.baseLanguage.structure.MethodDeclaration" flags="ng" index="1rXfSm">
        <property id="8355037393041754995" name="isNative" index="2aFKle" />
      </concept>
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
        <child id="1109201940907" name="parameter" index="11_B2D" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1073239437375" name="jetbrains.mps.baseLanguage.structure.NotEqualsExpression" flags="nn" index="3y3z36" />
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1107796713796" name="jetbrains.mps.baseLanguage.structure.Interface" flags="ig" index="3HP615">
        <child id="1107797138135" name="extendedInterface" index="3HQHJm" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
      <concept id="1146644623116" name="jetbrains.mps.baseLanguage.structure.PrivateVisibility" flags="nn" index="3Tm6S6" />
      <concept id="1080120340718" name="jetbrains.mps.baseLanguage.structure.AndExpression" flags="nn" index="1Wc70l" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="312cEu" id="1mAGFBLsTyX">
    <property role="3GE5qa" value="data.booleans" />
    <property role="TrG5h" value="TestHasPreviouslyBeenPerformed" />
    <node concept="3Tm1VV" id="1mAGFBLsTyY" role="1B3o_S" />
    <node concept="3uibUv" id="7lYCqhuUSkH" role="1zkMxy">
      <ref role="3uigEE" node="1mAGFBLsL0H" resolve="BooleanTestData" />
    </node>
    <node concept="3clFbW" id="1mAGFBLsTz0" role="jymVt">
      <node concept="3cqZAl" id="1mAGFBLsTz1" role="3clF45" />
      <node concept="3Tm1VV" id="1mAGFBLsTz2" role="1B3o_S" />
      <node concept="37vLTG" id="1mAGFBLsTz3" role="3clF46">
        <property role="TrG5h" value="test" />
        <node concept="17QB3L" id="1mAGFBLCaI$" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="4B5aqq4d2l2" role="3clF46">
        <property role="TrG5h" value="specimenType" />
        <node concept="17QB3L" id="4B5aqq4d2l8" role="1tU5fm" />
      </node>
      <node concept="3clFbS" id="1mAGFBLsTz5" role="3clF47">
        <node concept="XkiVB" id="1mAGFBLsTz6" role="3cqZAp">
          <ref role="37wK5l" node="1mAGFBLsL0M" resolve="BooleanTestData" />
          <node concept="37vLTw" id="1mAGFBLsTz7" role="37wK5m">
            <ref role="3cqZAo" node="1mAGFBLsTz3" resolve="test" />
          </node>
          <node concept="37vLTw" id="4B5aqq4d2n2" role="37wK5m">
            <ref role="3cqZAo" node="4B5aqq4d2l2" resolve="specimenType" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="5jVYnMGMbIL" role="jymVt" />
    <node concept="3clFb_" id="5jVYnMGMbCV" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="accept" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3Tm1VV" id="5jVYnMGMbCX" role="1B3o_S" />
      <node concept="37vLTG" id="5jVYnMGMbCZ" role="3clF46">
        <property role="TrG5h" value="visitor" />
        <node concept="3uibUv" id="5jVYnMGMbD0" role="1tU5fm">
          <ref role="3uigEE" to="8r9s:5jVYnMGKCt5" resolve="DataValueVisitor" />
        </node>
      </node>
      <node concept="3clFbS" id="5jVYnMGMbD5" role="3clF47">
        <node concept="3clFbJ" id="5jVYnMGMbOk" role="3cqZAp">
          <node concept="2ZW3vV" id="5jVYnMGMbZ5" role="3clFbw">
            <node concept="3uibUv" id="5jVYnMGMc6o" role="2ZW6by">
              <ref role="3uigEE" node="5jVYnMGI$WP" resolve="LaboratoryTestDataValueVisitor" />
            </node>
            <node concept="37vLTw" id="5jVYnMGMbOV" role="2ZW6bz">
              <ref role="3cqZAo" node="5jVYnMGMbCZ" resolve="visitor" />
            </node>
          </node>
          <node concept="3clFbS" id="5jVYnMGMbOm" role="3clFbx">
            <node concept="3cpWs6" id="5jVYnMGMdeg" role="3cqZAp">
              <node concept="2OqwBi" id="5jVYnMGMcV0" role="3cqZAk">
                <node concept="1eOMI4" id="5jVYnMGMcOb" role="2Oq$k0">
                  <node concept="10QFUN" id="5jVYnMGMcpR" role="1eOMHV">
                    <node concept="3uibUv" id="5jVYnMGMcvS" role="10QFUM">
                      <ref role="3uigEE" node="5jVYnMGI$WP" resolve="LaboratoryTestDataValueVisitor" />
                    </node>
                    <node concept="37vLTw" id="5jVYnMGMc6Y" role="10QFUP">
                      <ref role="3cqZAo" node="5jVYnMGMbCZ" resolve="visitor" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="5jVYnMGMd2I" role="2OqNvi">
                  <ref role="37wK5l" node="5jVYnMGIDnM" resolve="visit" />
                  <node concept="Xjq3P" id="5jVYnMGMd4k" role="37wK5m" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="5jVYnMGMdro" role="3cqZAp">
          <node concept="10Nm6u" id="5jVYnMGMdsb" role="3cqZAk" />
        </node>
      </node>
      <node concept="2AHcQZ" id="5jVYnMGMbD6" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
      <node concept="3uibUv" id="5jVYnMGV1df" role="3clF45">
        <ref role="3uigEE" to="8r9s:2XLt5KVsHET" resolve="LisResponse" />
        <node concept="3uibUv" id="5jVYnMGV1dg" role="11_B2D">
          <ref role="3uigEE" to="wyt6:~Boolean" resolve="Boolean" />
        </node>
      </node>
    </node>
  </node>
  <node concept="312cEu" id="1mAGFBLlFuh">
    <property role="3GE5qa" value="data.booleans" />
    <property role="TrG5h" value="TestHasRequest" />
    <node concept="3Tm1VV" id="1mAGFBLlFui" role="1B3o_S" />
    <node concept="3uibUv" id="1mAGFBLsLwp" role="1zkMxy">
      <ref role="3uigEE" node="1mAGFBLsL0H" resolve="BooleanTestData" />
    </node>
    <node concept="3clFbW" id="1mAGFBLsLA4" role="jymVt">
      <node concept="3cqZAl" id="1mAGFBLsLA5" role="3clF45" />
      <node concept="3Tm1VV" id="1mAGFBLsLA6" role="1B3o_S" />
      <node concept="37vLTG" id="4B5aqq4d2r3" role="3clF46">
        <property role="TrG5h" value="test" />
        <node concept="17QB3L" id="4B5aqq4d2r4" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="4B5aqq4d2r5" role="3clF46">
        <property role="TrG5h" value="specimenType" />
        <node concept="17QB3L" id="4B5aqq4d2r6" role="1tU5fm" />
      </node>
      <node concept="3clFbS" id="1mAGFBLsLAg" role="3clF47">
        <node concept="XkiVB" id="1mAGFBLsLAi" role="3cqZAp">
          <ref role="37wK5l" node="1mAGFBLsL0M" resolve="BooleanTestData" />
          <node concept="37vLTw" id="4B5aqq4d2sn" role="37wK5m">
            <ref role="3cqZAo" node="4B5aqq4d2r3" resolve="test" />
          </node>
          <node concept="37vLTw" id="4B5aqq4d2tV" role="37wK5m">
            <ref role="3cqZAo" node="4B5aqq4d2r5" resolve="specimenType" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="5jVYnMGMek2" role="jymVt" />
    <node concept="3clFb_" id="5jVYnMGMek3" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="accept" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3Tm1VV" id="5jVYnMGMek4" role="1B3o_S" />
      <node concept="37vLTG" id="5jVYnMGMek6" role="3clF46">
        <property role="TrG5h" value="visitor" />
        <node concept="3uibUv" id="5jVYnMGMek7" role="1tU5fm">
          <ref role="3uigEE" to="8r9s:5jVYnMGKCt5" resolve="DataValueVisitor" />
        </node>
      </node>
      <node concept="3clFbS" id="5jVYnMGMek8" role="3clF47">
        <node concept="3clFbJ" id="5jVYnMGMek9" role="3cqZAp">
          <node concept="2ZW3vV" id="5jVYnMGMeka" role="3clFbw">
            <node concept="3uibUv" id="5jVYnMGV1qu" role="2ZW6by">
              <ref role="3uigEE" node="5jVYnMGI$WP" resolve="LaboratoryTestDataValueVisitor" />
            </node>
            <node concept="37vLTw" id="5jVYnMGMekc" role="2ZW6bz">
              <ref role="3cqZAo" node="5jVYnMGMek6" resolve="visitor" />
            </node>
          </node>
          <node concept="3clFbS" id="5jVYnMGMekd" role="3clFbx">
            <node concept="3cpWs6" id="5jVYnMGMeke" role="3cqZAp">
              <node concept="2OqwBi" id="5jVYnMGMekf" role="3cqZAk">
                <node concept="1eOMI4" id="5jVYnMGMekg" role="2Oq$k0">
                  <node concept="10QFUN" id="5jVYnMGMekh" role="1eOMHV">
                    <node concept="3uibUv" id="5jVYnMGMeki" role="10QFUM">
                      <ref role="3uigEE" node="5jVYnMGI$WP" resolve="LaboratoryTestDataValueVisitor" />
                    </node>
                    <node concept="37vLTw" id="5jVYnMGMekj" role="10QFUP">
                      <ref role="3cqZAo" node="5jVYnMGMek6" resolve="visitor" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="5jVYnMGMekk" role="2OqNvi">
                  <ref role="37wK5l" node="5jVYnMGIDoS" resolve="visit" />
                  <node concept="Xjq3P" id="5jVYnMGMekl" role="37wK5m" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="5jVYnMGMekm" role="3cqZAp">
          <node concept="10Nm6u" id="5jVYnMGMekn" role="3cqZAk" />
        </node>
      </node>
      <node concept="2AHcQZ" id="5jVYnMGMeko" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
      <node concept="3uibUv" id="5jVYnMGV1vr" role="3clF45">
        <ref role="3uigEE" to="8r9s:2XLt5KVsHET" resolve="LisResponse" />
        <node concept="3uibUv" id="5jVYnMGV1vs" role="11_B2D">
          <ref role="3uigEE" to="wyt6:~Boolean" resolve="Boolean" />
        </node>
      </node>
    </node>
  </node>
  <node concept="312cEu" id="1mAGFBLsTwz">
    <property role="3GE5qa" value="data.booleans" />
    <property role="TrG5h" value="TestResultIsValid" />
    <node concept="3Tm1VV" id="1mAGFBLsTw$" role="1B3o_S" />
    <node concept="3uibUv" id="1mAGFBLsTw_" role="1zkMxy">
      <ref role="3uigEE" node="1mAGFBLsL0H" resolve="BooleanTestData" />
    </node>
    <node concept="3clFbW" id="1mAGFBLsTwA" role="jymVt">
      <node concept="3cqZAl" id="1mAGFBLsTwB" role="3clF45" />
      <node concept="3Tm1VV" id="1mAGFBLsTwC" role="1B3o_S" />
      <node concept="37vLTG" id="4B5aqq4d3SW" role="3clF46">
        <property role="TrG5h" value="test" />
        <node concept="17QB3L" id="4B5aqq4d3SX" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="4B5aqq4d3SY" role="3clF46">
        <property role="TrG5h" value="specimenType" />
        <node concept="17QB3L" id="4B5aqq4d3SZ" role="1tU5fm" />
      </node>
      <node concept="3clFbS" id="1mAGFBLsTwF" role="3clF47">
        <node concept="XkiVB" id="1mAGFBLsTwG" role="3cqZAp">
          <ref role="37wK5l" node="1mAGFBLsL0M" resolve="BooleanTestData" />
          <node concept="37vLTw" id="4B5aqq4d3Ug" role="37wK5m">
            <ref role="3cqZAo" node="4B5aqq4d3SW" resolve="test" />
          </node>
          <node concept="37vLTw" id="4B5aqq4d3VO" role="37wK5m">
            <ref role="3cqZAo" node="4B5aqq4d3SY" resolve="specimenType" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="5jVYnMGMf0T" role="jymVt" />
    <node concept="3clFb_" id="5jVYnMGMf0U" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="accept" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3Tm1VV" id="5jVYnMGMf0V" role="1B3o_S" />
      <node concept="37vLTG" id="5jVYnMGMf0X" role="3clF46">
        <property role="TrG5h" value="visitor" />
        <node concept="3uibUv" id="5jVYnMGMf0Y" role="1tU5fm">
          <ref role="3uigEE" to="8r9s:5jVYnMGKCt5" resolve="DataValueVisitor" />
        </node>
      </node>
      <node concept="3clFbS" id="5jVYnMGMf0Z" role="3clF47">
        <node concept="3clFbJ" id="5jVYnMGMf10" role="3cqZAp">
          <node concept="2ZW3vV" id="5jVYnMGMf11" role="3clFbw">
            <node concept="3uibUv" id="5jVYnMGMfji" role="2ZW6by">
              <ref role="3uigEE" node="5jVYnMGI$WP" resolve="LaboratoryTestDataValueVisitor" />
            </node>
            <node concept="37vLTw" id="5jVYnMGMf13" role="2ZW6bz">
              <ref role="3cqZAo" node="5jVYnMGMf0X" resolve="visitor" />
            </node>
          </node>
          <node concept="3clFbS" id="5jVYnMGMf14" role="3clFbx">
            <node concept="3cpWs6" id="5jVYnMGMf15" role="3cqZAp">
              <node concept="2OqwBi" id="5jVYnMGMf16" role="3cqZAk">
                <node concept="1eOMI4" id="5jVYnMGMf17" role="2Oq$k0">
                  <node concept="10QFUN" id="5jVYnMGMf18" role="1eOMHV">
                    <node concept="3uibUv" id="5jVYnMGMf19" role="10QFUM">
                      <ref role="3uigEE" node="5jVYnMGI$WP" resolve="LaboratoryTestDataValueVisitor" />
                    </node>
                    <node concept="37vLTw" id="5jVYnMGMf1a" role="10QFUP">
                      <ref role="3cqZAo" node="5jVYnMGMf0X" resolve="visitor" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="5jVYnMGMf1b" role="2OqNvi">
                  <ref role="37wK5l" node="5jVYnMGJw8g" resolve="visit" />
                  <node concept="Xjq3P" id="5jVYnMGMf1c" role="37wK5m" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="5jVYnMGMf1d" role="3cqZAp">
          <node concept="10Nm6u" id="5jVYnMGMf1e" role="3cqZAk" />
        </node>
      </node>
      <node concept="2AHcQZ" id="5jVYnMGMf1f" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
      <node concept="3uibUv" id="5jVYnMGV1U3" role="3clF45">
        <ref role="3uigEE" to="8r9s:2XLt5KVsHET" resolve="LisResponse" />
        <node concept="3uibUv" id="5jVYnMGV1U4" role="11_B2D">
          <ref role="3uigEE" to="wyt6:~Boolean" resolve="Boolean" />
        </node>
      </node>
    </node>
  </node>
  <node concept="312cEu" id="1mAGFBLsL0H">
    <property role="3GE5qa" value="data.booleans" />
    <property role="TrG5h" value="BooleanTestData" />
    <property role="1sVAO0" value="true" />
    <node concept="312cEg" id="1mAGFBLsL0I" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="test" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm6S6" id="1mAGFBLsL0J" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBLC9vG" role="1tU5fm" />
    </node>
    <node concept="312cEg" id="4B5aqq4cW7P" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="specimenType" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm6S6" id="4B5aqq4cVVO" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4cW5c" role="1tU5fm" />
    </node>
    <node concept="2tJIrI" id="1mAGFBLsL0L" role="jymVt" />
    <node concept="3clFbW" id="1mAGFBLsL0M" role="jymVt">
      <node concept="3cqZAl" id="1mAGFBLsL0N" role="3clF45" />
      <node concept="3Tm1VV" id="1mAGFBLsL0O" role="1B3o_S" />
      <node concept="3clFbS" id="1mAGFBLsL0P" role="3clF47">
        <node concept="3clFbF" id="1mAGFBLsL0Q" role="3cqZAp">
          <node concept="37vLTI" id="1mAGFBLsL0R" role="3clFbG">
            <node concept="37vLTw" id="1mAGFBLsL0S" role="37vLTx">
              <ref role="3cqZAo" node="1mAGFBLsL0W" resolve="test" />
            </node>
            <node concept="2OqwBi" id="1mAGFBLsL0T" role="37vLTJ">
              <node concept="Xjq3P" id="1mAGFBLsL0U" role="2Oq$k0" />
              <node concept="2OwXpG" id="1mAGFBLsL0V" role="2OqNvi">
                <ref role="2Oxat5" node="1mAGFBLsL0I" resolve="test" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4B5aqq4cWo3" role="3cqZAp">
          <node concept="37vLTI" id="4B5aqq4cWEV" role="3clFbG">
            <node concept="37vLTw" id="4B5aqq4cWNu" role="37vLTx">
              <ref role="3cqZAo" node="4B5aqq4cWhf" resolve="specimenType" />
            </node>
            <node concept="2OqwBi" id="4B5aqq4cWrR" role="37vLTJ">
              <node concept="Xjq3P" id="4B5aqq4cWo1" role="2Oq$k0" />
              <node concept="2OwXpG" id="4B5aqq4cWv_" role="2OqNvi">
                <ref role="2Oxat5" node="4B5aqq4cW7P" resolve="specimenType" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="1mAGFBLsL0W" role="3clF46">
        <property role="TrG5h" value="test" />
        <node concept="17QB3L" id="1mAGFBLC9pI" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="4B5aqq4cWhf" role="3clF46">
        <property role="TrG5h" value="specimenType" />
        <node concept="17QB3L" id="4B5aqq4cWhl" role="1tU5fm" />
      </node>
    </node>
    <node concept="2tJIrI" id="1mAGFBLsL0Y" role="jymVt" />
    <node concept="3Tm1VV" id="1mAGFBLsL0Z" role="1B3o_S" />
    <node concept="3uibUv" id="1mAGFBLsL10" role="1zkMxy">
      <ref role="3uigEE" to="8r9s:1mAGFBLk56n" resolve="BooleanDataValue" />
    </node>
    <node concept="3clFb_" id="1mAGFBLEGE4" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestName" />
      <node concept="3Tm1VV" id="1mAGFBLEGE6" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBLEGE7" role="3clF45" />
      <node concept="3clFbS" id="1mAGFBLEGE9" role="3clF47">
        <node concept="3cpWs6" id="1mAGFBLsL16" role="3cqZAp">
          <node concept="2OqwBi" id="1mAGFBLsL17" role="3cqZAk">
            <node concept="Xjq3P" id="1mAGFBLsL18" role="2Oq$k0" />
            <node concept="2OwXpG" id="1mAGFBLsL19" role="2OqNvi">
              <ref role="2Oxat5" node="1mAGFBLsL0I" resolve="test" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="4B5aqq4cWXB" role="jymVt" />
    <node concept="3clFb_" id="4B5aqq4cXr7" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="getSpecimentType" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="4B5aqq4cXra" role="3clF47">
        <node concept="3cpWs6" id="4B5aqq4cXAl" role="3cqZAp">
          <node concept="2OqwBi" id="4B5aqq4cXKY" role="3cqZAk">
            <node concept="Xjq3P" id="4B5aqq4cXAM" role="2Oq$k0" />
            <node concept="2OwXpG" id="4B5aqq4cXX6" role="2OqNvi">
              <ref role="2Oxat5" node="4B5aqq4cW7P" resolve="specimenType" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="4B5aqq4cXhe" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4cXiz" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="7lYCqhv3OhF" role="jymVt" />
    <node concept="3clFb_" id="7lYCqhv3O98" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="hashCode" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3Tm1VV" id="7lYCqhv3O99" role="1B3o_S" />
      <node concept="10Oyi0" id="7lYCqhv3O9a" role="3clF45" />
      <node concept="2AHcQZ" id="7lYCqhv3O9h" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
      <node concept="3clFbS" id="7lYCqhv3O9i" role="3clF47">
        <node concept="3clFbF" id="7lYCqhv3OT2" role="3cqZAp">
          <node concept="2YIFZM" id="7lYCqhv3Pcc" role="3clFbG">
            <ref role="37wK5l" to="33ny:~Objects.hash(java.lang.Object...):int" resolve="hash" />
            <ref role="1Pybhc" to="33ny:~Objects" resolve="Objects" />
            <node concept="2OqwBi" id="7lYCqhv4FGG" role="37wK5m">
              <node concept="Xjq3P" id="7lYCqhv3PgO" role="2Oq$k0" />
              <node concept="liA8E" id="7lYCqhv4FUX" role="2OqNvi">
                <ref role="37wK5l" to="wyt6:~Object.getClass():java.lang.Class" resolve="getClass" />
              </node>
            </node>
            <node concept="2OqwBi" id="4B5aqq4cYSe" role="37wK5m">
              <node concept="Xjq3P" id="4B5aqq4cYLG" role="2Oq$k0" />
              <node concept="2OwXpG" id="4B5aqq4cZ66" role="2OqNvi">
                <ref role="2Oxat5" node="1mAGFBLsL0I" resolve="test" />
              </node>
            </node>
            <node concept="2OqwBi" id="4B5aqq4cZjZ" role="37wK5m">
              <node concept="Xjq3P" id="4B5aqq4cZcq" role="2Oq$k0" />
              <node concept="2OwXpG" id="4B5aqq4cZqn" role="2OqNvi">
                <ref role="2Oxat5" node="4B5aqq4cW7P" resolve="specimenType" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="6khVixyhGf$" role="jymVt" />
    <node concept="3clFb_" id="7lYCqhv3O9l" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="equals" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3Tm1VV" id="7lYCqhv3O9m" role="1B3o_S" />
      <node concept="10P_77" id="7lYCqhv3O9n" role="3clF45" />
      <node concept="37vLTG" id="7lYCqhv3O9o" role="3clF46">
        <property role="TrG5h" value="object" />
        <node concept="3uibUv" id="7lYCqhv3O9p" role="1tU5fm">
          <ref role="3uigEE" to="wyt6:~Object" resolve="Object" />
        </node>
      </node>
      <node concept="2AHcQZ" id="7lYCqhv3O9z" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
      <node concept="3clFbS" id="7lYCqhv3O9$" role="3clF47">
        <node concept="3clFbJ" id="4B5aqq5OJ30" role="3cqZAp">
          <node concept="3clFbS" id="4B5aqq5OJ31" role="3clFbx">
            <node concept="3cpWs6" id="4B5aqq5OJ32" role="3cqZAp">
              <node concept="3clFbT" id="4B5aqq5OJ33" role="3cqZAk">
                <property role="3clFbU" value="true" />
              </node>
            </node>
          </node>
          <node concept="3clFbC" id="4B5aqq5OJ34" role="3clFbw">
            <node concept="Xjq3P" id="4B5aqq5OJ35" role="3uHU7B" />
            <node concept="37vLTw" id="4B5aqq5OJ36" role="3uHU7w">
              <ref role="3cqZAo" node="7lYCqhv3O9o" resolve="object" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="4B5aqq5OJ37" role="3cqZAp">
          <node concept="3clFbS" id="4B5aqq5OJ38" role="3clFbx">
            <node concept="3cpWs6" id="4B5aqq5OJ39" role="3cqZAp">
              <node concept="3clFbT" id="4B5aqq5OJ3a" role="3cqZAk">
                <property role="3clFbU" value="false" />
              </node>
            </node>
          </node>
          <node concept="22lmx$" id="4B5aqq5OJW5" role="3clFbw">
            <node concept="3clFbC" id="4B5aqq5OKlz" role="3uHU7B">
              <node concept="10Nm6u" id="4B5aqq5OKtL" role="3uHU7w" />
              <node concept="37vLTw" id="4B5aqq5OKbr" role="3uHU7B">
                <ref role="3cqZAo" node="7lYCqhv3O9o" resolve="object" />
              </node>
            </node>
            <node concept="3y3z36" id="4B5aqq5OJ3b" role="3uHU7w">
              <node concept="2OqwBi" id="4B5aqq5OJ3c" role="3uHU7B">
                <node concept="Xjq3P" id="4B5aqq5OJ3d" role="2Oq$k0" />
                <node concept="liA8E" id="4B5aqq5OJ3e" role="2OqNvi">
                  <ref role="37wK5l" to="wyt6:~Object.getClass():java.lang.Class" resolve="getClass" />
                </node>
              </node>
              <node concept="2OqwBi" id="4B5aqq5OJ3f" role="3uHU7w">
                <node concept="37vLTw" id="4B5aqq5OJ3g" role="2Oq$k0">
                  <ref role="3cqZAo" node="7lYCqhv3O9o" resolve="object" />
                </node>
                <node concept="liA8E" id="4B5aqq5OJ3h" role="2OqNvi">
                  <ref role="37wK5l" to="wyt6:~Object.getClass():java.lang.Class" resolve="getClass" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="7lYCqhv3Qzi" role="3cqZAp">
          <node concept="3cpWsn" id="7lYCqhv3Qzj" role="3cpWs9">
            <property role="TrG5h" value="other" />
            <node concept="3uibUv" id="7lYCqhv3Qzk" role="1tU5fm">
              <ref role="3uigEE" node="1mAGFBLsL0H" resolve="BooleanTestData" />
            </node>
            <node concept="10QFUN" id="7lYCqhv3R6V" role="33vP2m">
              <node concept="3uibUv" id="7lYCqhv3Rc6" role="10QFUM">
                <ref role="3uigEE" node="1mAGFBLsL0H" resolve="BooleanTestData" />
              </node>
              <node concept="37vLTw" id="7lYCqhv3R2e" role="10QFUP">
                <ref role="3cqZAo" node="7lYCqhv3O9o" resolve="object" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="7lYCqhv3Rrl" role="3cqZAp">
          <node concept="1Wc70l" id="4B5aqq5Ch9p" role="3cqZAk">
            <node concept="2YIFZM" id="4B5aqq5ChuR" role="3uHU7w">
              <ref role="37wK5l" to="33ny:~Objects.equals(java.lang.Object,java.lang.Object):boolean" resolve="equals" />
              <ref role="1Pybhc" to="33ny:~Objects" resolve="Objects" />
              <node concept="2OqwBi" id="4B5aqq5CiuC" role="37wK5m">
                <node concept="Xjq3P" id="4B5aqq5Cill" role="2Oq$k0" />
                <node concept="2OwXpG" id="4B5aqq5OFRF" role="2OqNvi">
                  <ref role="2Oxat5" node="4B5aqq4cW7P" resolve="specimenType" />
                </node>
              </node>
              <node concept="2OqwBi" id="4B5aqq5ChMj" role="37wK5m">
                <node concept="37vLTw" id="4B5aqq5ChDi" role="2Oq$k0">
                  <ref role="3cqZAo" node="7lYCqhv3Qzj" resolve="other" />
                </node>
                <node concept="2OwXpG" id="4B5aqq5ChZV" role="2OqNvi">
                  <ref role="2Oxat5" node="4B5aqq4cW7P" resolve="specimenType" />
                </node>
              </node>
            </node>
            <node concept="2YIFZM" id="4B5aqq5CfH3" role="3uHU7B">
              <ref role="37wK5l" to="33ny:~Objects.equals(java.lang.Object,java.lang.Object):boolean" resolve="equals" />
              <ref role="1Pybhc" to="33ny:~Objects" resolve="Objects" />
              <node concept="2OqwBi" id="4B5aqq5Cg1f" role="37wK5m">
                <node concept="Xjq3P" id="4B5aqq5CfQV" role="2Oq$k0" />
                <node concept="2OwXpG" id="4B5aqq5Cgef" role="2OqNvi">
                  <ref role="2Oxat5" node="1mAGFBLsL0I" resolve="test" />
                </node>
              </node>
              <node concept="2OqwBi" id="4B5aqq5CgEW" role="37wK5m">
                <node concept="37vLTw" id="4B5aqq5Cgyi" role="2Oq$k0">
                  <ref role="3cqZAo" node="7lYCqhv3Qzj" resolve="other" />
                </node>
                <node concept="2OwXpG" id="4B5aqq5CgSd" role="2OqNvi">
                  <ref role="2Oxat5" node="1mAGFBLsL0I" resolve="test" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="312cEu" id="4V3GMfXxDw9">
    <property role="3GE5qa" value="data.booleans" />
    <property role="TrG5h" value="TestPreviousResultIsValid" />
    <node concept="3Tm1VV" id="4V3GMfXxDwa" role="1B3o_S" />
    <node concept="3uibUv" id="4V3GMfXxDwb" role="1zkMxy">
      <ref role="3uigEE" node="1mAGFBLsL0H" resolve="BooleanTestData" />
    </node>
    <node concept="3clFbW" id="4V3GMfXxDwc" role="jymVt">
      <node concept="3cqZAl" id="4V3GMfXxDwd" role="3clF45" />
      <node concept="3Tm1VV" id="4V3GMfXxDwe" role="1B3o_S" />
      <node concept="37vLTG" id="4B5aqq4d3Pj" role="3clF46">
        <property role="TrG5h" value="test" />
        <node concept="17QB3L" id="4B5aqq4d3Pk" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="4B5aqq4d3Pl" role="3clF46">
        <property role="TrG5h" value="specimenType" />
        <node concept="17QB3L" id="4B5aqq4d3Pm" role="1tU5fm" />
      </node>
      <node concept="3clFbS" id="4V3GMfXxDwh" role="3clF47">
        <node concept="XkiVB" id="4V3GMfXxDwi" role="3cqZAp">
          <ref role="37wK5l" node="1mAGFBLsL0M" resolve="BooleanTestData" />
          <node concept="37vLTw" id="4B5aqq4d3QB" role="37wK5m">
            <ref role="3cqZAo" node="4B5aqq4d3Pj" resolve="test" />
          </node>
          <node concept="37vLTw" id="4B5aqq4d3Sb" role="37wK5m">
            <ref role="3cqZAo" node="4B5aqq4d3Pl" resolve="specimenType" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="5jVYnMGMgh3" role="jymVt" />
    <node concept="3clFb_" id="5jVYnMGMgh4" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="accept" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3Tm1VV" id="5jVYnMGMgh5" role="1B3o_S" />
      <node concept="37vLTG" id="5jVYnMGMgh7" role="3clF46">
        <property role="TrG5h" value="visitor" />
        <node concept="3uibUv" id="5jVYnMGMgh8" role="1tU5fm">
          <ref role="3uigEE" to="8r9s:5jVYnMGKCt5" resolve="DataValueVisitor" />
        </node>
      </node>
      <node concept="3clFbS" id="5jVYnMGMgh9" role="3clF47">
        <node concept="3clFbJ" id="5jVYnMGMgha" role="3cqZAp">
          <node concept="2ZW3vV" id="5jVYnMGMghb" role="3clFbw">
            <node concept="3uibUv" id="5jVYnMGMghc" role="2ZW6by">
              <ref role="3uigEE" node="5jVYnMGI$WP" resolve="LaboratoryTestDataValueVisitor" />
            </node>
            <node concept="37vLTw" id="5jVYnMGMghd" role="2ZW6bz">
              <ref role="3cqZAo" node="5jVYnMGMgh7" resolve="visitor" />
            </node>
          </node>
          <node concept="3clFbS" id="5jVYnMGMghe" role="3clFbx">
            <node concept="3cpWs6" id="5jVYnMGMghf" role="3cqZAp">
              <node concept="2OqwBi" id="5jVYnMGMghg" role="3cqZAk">
                <node concept="1eOMI4" id="5jVYnMGMghh" role="2Oq$k0">
                  <node concept="10QFUN" id="5jVYnMGMghi" role="1eOMHV">
                    <node concept="3uibUv" id="5jVYnMGMghj" role="10QFUM">
                      <ref role="3uigEE" node="5jVYnMGI$WP" resolve="LaboratoryTestDataValueVisitor" />
                    </node>
                    <node concept="37vLTw" id="5jVYnMGMghk" role="10QFUP">
                      <ref role="3cqZAo" node="5jVYnMGMgh7" resolve="visitor" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="5jVYnMGMghl" role="2OqNvi">
                  <ref role="37wK5l" node="5jVYnMGIDqm" resolve="visit" />
                  <node concept="Xjq3P" id="5jVYnMGMghm" role="37wK5m" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="5jVYnMGMghn" role="3cqZAp">
          <node concept="10Nm6u" id="5jVYnMGMgho" role="3cqZAk" />
        </node>
      </node>
      <node concept="2AHcQZ" id="5jVYnMGMghp" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
      <node concept="3uibUv" id="5jVYnMGV1GO" role="3clF45">
        <ref role="3uigEE" to="8r9s:2XLt5KVsHET" resolve="LisResponse" />
        <node concept="3uibUv" id="5jVYnMGV1GP" role="11_B2D">
          <ref role="3uigEE" to="wyt6:~Boolean" resolve="Boolean" />
        </node>
      </node>
    </node>
  </node>
  <node concept="312cEu" id="1mAGFBLsOMS">
    <property role="3GE5qa" value="data.values" />
    <property role="TrG5h" value="TestTimeSinceLastRequested" />
    <node concept="312cEg" id="1mAGFBLsOMT" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="test" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm6S6" id="1mAGFBLsOMU" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBLC7Z7" role="1tU5fm" />
    </node>
    <node concept="312cEg" id="4B5aqq4d6l3" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="specimenType" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm6S6" id="4B5aqq4d62e" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4d6em" role="1tU5fm" />
    </node>
    <node concept="2tJIrI" id="1mAGFBLsOMW" role="jymVt" />
    <node concept="3clFbW" id="1mAGFBLsOMX" role="jymVt">
      <node concept="3cqZAl" id="1mAGFBLsOMY" role="3clF45" />
      <node concept="3Tm1VV" id="1mAGFBLsOMZ" role="1B3o_S" />
      <node concept="3clFbS" id="1mAGFBLsON0" role="3clF47">
        <node concept="3clFbF" id="1mAGFBLsON1" role="3cqZAp">
          <node concept="37vLTI" id="1mAGFBLsON2" role="3clFbG">
            <node concept="37vLTw" id="1mAGFBLsON3" role="37vLTx">
              <ref role="3cqZAo" node="1mAGFBLsON7" resolve="test" />
            </node>
            <node concept="2OqwBi" id="1mAGFBLsON4" role="37vLTJ">
              <node concept="Xjq3P" id="1mAGFBLsON5" role="2Oq$k0" />
              <node concept="2OwXpG" id="1mAGFBLsON6" role="2OqNvi">
                <ref role="2Oxat5" node="1mAGFBLsOMT" resolve="test" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4B5aqq4d6ye" role="3cqZAp">
          <node concept="37vLTI" id="4B5aqq4d6Um" role="3clFbG">
            <node concept="37vLTw" id="4B5aqq4d76e" role="37vLTx">
              <ref role="3cqZAo" node="4B5aqq4d4kB" resolve="specimenType" />
            </node>
            <node concept="2OqwBi" id="4B5aqq4d6AF" role="37vLTJ">
              <node concept="Xjq3P" id="4B5aqq4d6yc" role="2Oq$k0" />
              <node concept="2OwXpG" id="4B5aqq4d6F2" role="2OqNvi">
                <ref role="2Oxat5" node="4B5aqq4d6l3" resolve="specimenType" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="1mAGFBLsON7" role="3clF46">
        <property role="TrG5h" value="test" />
        <node concept="17QB3L" id="1mAGFBLC83N" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="4B5aqq4d4kB" role="3clF46">
        <property role="TrG5h" value="specimenType" />
        <node concept="17QB3L" id="4B5aqq4d4s6" role="1tU5fm" />
      </node>
    </node>
    <node concept="2tJIrI" id="1mAGFBLsON9" role="jymVt" />
    <node concept="3clFb_" id="1mAGFBLEHvM" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestName" />
      <node concept="3Tm1VV" id="1mAGFBLEHvO" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBLEHvP" role="3clF45" />
      <node concept="3clFbS" id="1mAGFBLEHvR" role="3clF47">
        <node concept="3cpWs6" id="1mAGFBLsONe" role="3cqZAp">
          <node concept="2OqwBi" id="1mAGFBLsONf" role="3cqZAk">
            <node concept="Xjq3P" id="1mAGFBLsONg" role="2Oq$k0" />
            <node concept="2OwXpG" id="1mAGFBLsONh" role="2OqNvi">
              <ref role="2Oxat5" node="1mAGFBLsOMT" resolve="test" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="4B5aqq4d4C5" role="jymVt" />
    <node concept="3clFb_" id="4B5aqq4d5dk" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="getSpecimenType" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="4B5aqq4d5dn" role="3clF47">
        <node concept="3cpWs6" id="4B5aqq4d5qj" role="3cqZAp">
          <node concept="2OqwBi" id="4B5aqq4d5_X" role="3cqZAk">
            <node concept="Xjq3P" id="4B5aqq4d5qK" role="2Oq$k0" />
            <node concept="2OwXpG" id="4B5aqq4d7bd" role="2OqNvi">
              <ref role="2Oxat5" node="4B5aqq4d6l3" resolve="specimenType" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="4B5aqq4d50Q" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq4d522" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="5jVYnMGMgEe" role="jymVt" />
    <node concept="3clFb_" id="5jVYnMGMgEf" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="accept" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3Tm1VV" id="5jVYnMGMgEg" role="1B3o_S" />
      <node concept="37vLTG" id="5jVYnMGMgEi" role="3clF46">
        <property role="TrG5h" value="visitor" />
        <node concept="3uibUv" id="5jVYnMGMgEj" role="1tU5fm">
          <ref role="3uigEE" to="8r9s:5jVYnMGKCt5" resolve="DataValueVisitor" />
        </node>
      </node>
      <node concept="3clFbS" id="5jVYnMGMgEk" role="3clF47">
        <node concept="3clFbJ" id="5jVYnMGMgEl" role="3cqZAp">
          <node concept="2ZW3vV" id="5jVYnMGMgEm" role="3clFbw">
            <node concept="3uibUv" id="5jVYnMGMgEn" role="2ZW6by">
              <ref role="3uigEE" node="5jVYnMGI$WP" resolve="LaboratoryTestDataValueVisitor" />
            </node>
            <node concept="37vLTw" id="5jVYnMGMgEo" role="2ZW6bz">
              <ref role="3cqZAo" node="5jVYnMGMgEi" resolve="visitor" />
            </node>
          </node>
          <node concept="3clFbS" id="5jVYnMGMgEp" role="3clFbx">
            <node concept="3cpWs6" id="5jVYnMGMgEq" role="3cqZAp">
              <node concept="2OqwBi" id="5jVYnMGMgEr" role="3cqZAk">
                <node concept="1eOMI4" id="5jVYnMGMgEs" role="2Oq$k0">
                  <node concept="10QFUN" id="5jVYnMGMgEt" role="1eOMHV">
                    <node concept="3uibUv" id="5jVYnMGMgEu" role="10QFUM">
                      <ref role="3uigEE" node="5jVYnMGI$WP" resolve="LaboratoryTestDataValueVisitor" />
                    </node>
                    <node concept="37vLTw" id="5jVYnMGMgEv" role="10QFUP">
                      <ref role="3cqZAo" node="5jVYnMGMgEi" resolve="visitor" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="5jVYnMGMgEw" role="2OqNvi">
                  <ref role="37wK5l" node="5jVYnMGJBVs" resolve="visit" />
                  <node concept="Xjq3P" id="5jVYnMGMgEx" role="37wK5m" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="5jVYnMGMgEy" role="3cqZAp">
          <node concept="10Nm6u" id="5jVYnMGMgEz" role="3cqZAk" />
        </node>
      </node>
      <node concept="2AHcQZ" id="5jVYnMGMgE$" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
      <node concept="3uibUv" id="5jVYnMGV45y" role="3clF45">
        <ref role="3uigEE" to="8r9s:2XLt5KVsHET" resolve="LisResponse" />
        <node concept="3uibUv" id="5jVYnMGV4ve" role="11_B2D">
          <ref role="3uigEE" to="28m1:~Duration" resolve="Duration" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="7lYCqhv3UnL" role="jymVt" />
    <node concept="3clFb_" id="7lYCqhv3U1k" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="hashCode" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3Tm1VV" id="7lYCqhv3U1l" role="1B3o_S" />
      <node concept="10Oyi0" id="7lYCqhv3U1n" role="3clF45" />
      <node concept="3clFbS" id="7lYCqhv3U1q" role="3clF47">
        <node concept="3clFbF" id="7lYCqhv3U1t" role="3cqZAp">
          <node concept="2YIFZM" id="7lYCqhv4t_e" role="3clFbG">
            <ref role="37wK5l" to="33ny:~Objects.hash(java.lang.Object...):int" resolve="hash" />
            <ref role="1Pybhc" to="33ny:~Objects" resolve="Objects" />
            <node concept="2OqwBi" id="7lYCqhv4AJ3" role="37wK5m">
              <node concept="Xjq3P" id="7lYCqhv4t_X" role="2Oq$k0" />
              <node concept="liA8E" id="7lYCqhv4B2D" role="2OqNvi">
                <ref role="37wK5l" to="wyt6:~Object.getClass():java.lang.Class" resolve="getClass" />
              </node>
            </node>
            <node concept="2OqwBi" id="4B5aqq4d81D" role="37wK5m">
              <node concept="Xjq3P" id="4B5aqq4d7TN" role="2Oq$k0" />
              <node concept="2OwXpG" id="4B5aqq4d87D" role="2OqNvi">
                <ref role="2Oxat5" node="1mAGFBLsOMT" resolve="test" />
              </node>
            </node>
            <node concept="2OqwBi" id="4B5aqq4d8_A" role="37wK5m">
              <node concept="Xjq3P" id="4B5aqq4d8sH" role="2Oq$k0" />
              <node concept="2OwXpG" id="4B5aqq4d8GD" role="2OqNvi">
                <ref role="2Oxat5" node="4B5aqq4d6l3" resolve="specimenType" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="7lYCqhv3U1r" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="7lYCqhv3UxP" role="jymVt" />
    <node concept="3clFb_" id="7lYCqhv3U1u" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="equals" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3Tm1VV" id="7lYCqhv3U1v" role="1B3o_S" />
      <node concept="10P_77" id="7lYCqhv3U1x" role="3clF45" />
      <node concept="37vLTG" id="7lYCqhv3U1y" role="3clF46">
        <property role="TrG5h" value="object" />
        <node concept="3uibUv" id="7lYCqhv3U1z" role="1tU5fm">
          <ref role="3uigEE" to="wyt6:~Object" resolve="Object" />
        </node>
      </node>
      <node concept="3clFbS" id="7lYCqhv3U1A" role="3clF47">
        <node concept="3clFbJ" id="4B5aqq5OMcE" role="3cqZAp">
          <node concept="3clFbS" id="4B5aqq5OMcF" role="3clFbx">
            <node concept="3cpWs6" id="4B5aqq5OMcG" role="3cqZAp">
              <node concept="3clFbT" id="4B5aqq5OMcH" role="3cqZAk">
                <property role="3clFbU" value="true" />
              </node>
            </node>
          </node>
          <node concept="3clFbC" id="4B5aqq5OMcI" role="3clFbw">
            <node concept="Xjq3P" id="4B5aqq5OMcJ" role="3uHU7B" />
            <node concept="37vLTw" id="4B5aqq5OMcK" role="3uHU7w">
              <ref role="3cqZAo" node="7lYCqhv3U1y" resolve="object" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="4B5aqq5OMcL" role="3cqZAp">
          <node concept="3clFbS" id="4B5aqq5OMcM" role="3clFbx">
            <node concept="3cpWs6" id="4B5aqq5OMcN" role="3cqZAp">
              <node concept="3clFbT" id="4B5aqq5OMcO" role="3cqZAk">
                <property role="3clFbU" value="false" />
              </node>
            </node>
          </node>
          <node concept="22lmx$" id="4B5aqq5OMcP" role="3clFbw">
            <node concept="3clFbC" id="4B5aqq5OMcQ" role="3uHU7B">
              <node concept="10Nm6u" id="4B5aqq5OMcR" role="3uHU7w" />
              <node concept="37vLTw" id="4B5aqq5OMcS" role="3uHU7B">
                <ref role="3cqZAo" node="7lYCqhv3U1y" resolve="object" />
              </node>
            </node>
            <node concept="3y3z36" id="4B5aqq5OMcT" role="3uHU7w">
              <node concept="2OqwBi" id="4B5aqq5OMcU" role="3uHU7B">
                <node concept="Xjq3P" id="4B5aqq5OMcV" role="2Oq$k0" />
                <node concept="liA8E" id="4B5aqq5OMcW" role="2OqNvi">
                  <ref role="37wK5l" to="wyt6:~Object.getClass():java.lang.Class" resolve="getClass" />
                </node>
              </node>
              <node concept="2OqwBi" id="4B5aqq5OMcX" role="3uHU7w">
                <node concept="37vLTw" id="4B5aqq5OMcY" role="2Oq$k0">
                  <ref role="3cqZAo" node="7lYCqhv3U1y" resolve="object" />
                </node>
                <node concept="liA8E" id="4B5aqq5OMcZ" role="2OqNvi">
                  <ref role="37wK5l" to="wyt6:~Object.getClass():java.lang.Class" resolve="getClass" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="7lYCqhv3Ubu" role="3cqZAp">
          <node concept="3cpWsn" id="7lYCqhv3Ubv" role="3cpWs9">
            <property role="TrG5h" value="other" />
            <node concept="3uibUv" id="7lYCqhv3UWc" role="1tU5fm">
              <ref role="3uigEE" node="1mAGFBLsOMS" resolve="TestTimeSinceLastRequested" />
            </node>
            <node concept="10QFUN" id="7lYCqhv3Ubx" role="33vP2m">
              <node concept="3uibUv" id="7lYCqhv3UMF" role="10QFUM">
                <ref role="3uigEE" node="1mAGFBLsOMS" resolve="TestTimeSinceLastRequested" />
              </node>
              <node concept="37vLTw" id="7lYCqhv3Ubz" role="10QFUP">
                <ref role="3cqZAo" node="7lYCqhv3U1y" resolve="object" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="7lYCqhv3Ub$" role="3cqZAp">
          <node concept="1Wc70l" id="4B5aqq5CnzJ" role="3cqZAk">
            <node concept="2YIFZM" id="4B5aqq5Cm4j" role="3uHU7B">
              <ref role="37wK5l" to="33ny:~Objects.equals(java.lang.Object,java.lang.Object):boolean" resolve="equals" />
              <ref role="1Pybhc" to="33ny:~Objects" resolve="Objects" />
              <node concept="2OqwBi" id="4B5aqq5Cmrb" role="37wK5m">
                <node concept="Xjq3P" id="4B5aqq5Cmgk" role="2Oq$k0" />
                <node concept="2OwXpG" id="4B5aqq5CmF4" role="2OqNvi">
                  <ref role="2Oxat5" node="1mAGFBLsOMT" resolve="test" />
                </node>
              </node>
              <node concept="2OqwBi" id="4B5aqq5Cn09" role="37wK5m">
                <node concept="37vLTw" id="4B5aqq5CmR_" role="2Oq$k0">
                  <ref role="3cqZAo" node="7lYCqhv3Ubv" resolve="other" />
                </node>
                <node concept="2OwXpG" id="4B5aqq5Cngj" role="2OqNvi">
                  <ref role="2Oxat5" node="1mAGFBLsOMT" resolve="test" />
                </node>
              </node>
            </node>
            <node concept="2YIFZM" id="4B5aqq5Cone" role="3uHU7w">
              <ref role="37wK5l" to="33ny:~Objects.equals(java.lang.Object,java.lang.Object):boolean" resolve="equals" />
              <ref role="1Pybhc" to="33ny:~Objects" resolve="Objects" />
              <node concept="2OqwBi" id="4B5aqq5CoO5" role="37wK5m">
                <node concept="Xjq3P" id="4B5aqq5CozZ" role="2Oq$k0" />
                <node concept="2OwXpG" id="4B5aqq5Cp4B" role="2OqNvi">
                  <ref role="2Oxat5" node="4B5aqq4d6l3" resolve="specimenType" />
                </node>
              </node>
              <node concept="2OqwBi" id="4B5aqq5CpqY" role="37wK5m">
                <node concept="37vLTw" id="4B5aqq5CphL" role="2Oq$k0">
                  <ref role="3cqZAo" node="7lYCqhv3Ubv" resolve="other" />
                </node>
                <node concept="2OwXpG" id="4B5aqq5CpFL" role="2OqNvi">
                  <ref role="2Oxat5" node="4B5aqq4d6l3" resolve="specimenType" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="7lYCqhv3U1B" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3Tm1VV" id="1mAGFBLsONj" role="1B3o_S" />
    <node concept="3uibUv" id="1mAGFBLsP7m" role="1zkMxy">
      <ref role="3uigEE" to="8r9s:1mAGFBLlvBo" resolve="TimeSpanDataValue" />
    </node>
  </node>
  <node concept="312cEu" id="PDjyzjRFic">
    <property role="3GE5qa" value="data.booleans" />
    <property role="TrG5h" value="TestHasBeenPerformed" />
    <node concept="3Tm1VV" id="PDjyzjRFid" role="1B3o_S" />
    <node concept="3uibUv" id="PDjyzjRFie" role="1zkMxy">
      <ref role="3uigEE" node="1mAGFBLsL0H" resolve="BooleanTestData" />
    </node>
    <node concept="3clFbW" id="PDjyzjRFif" role="jymVt">
      <node concept="3cqZAl" id="PDjyzjRFig" role="3clF45" />
      <node concept="3Tm1VV" id="PDjyzjRFih" role="1B3o_S" />
      <node concept="37vLTG" id="PDjyzjRFii" role="3clF46">
        <property role="TrG5h" value="test" />
        <node concept="17QB3L" id="PDjyzjRFij" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="PDjyzjRFik" role="3clF46">
        <property role="TrG5h" value="specimenType" />
        <node concept="17QB3L" id="PDjyzjRFil" role="1tU5fm" />
      </node>
      <node concept="3clFbS" id="PDjyzjRFim" role="3clF47">
        <node concept="XkiVB" id="PDjyzjRFin" role="3cqZAp">
          <ref role="37wK5l" node="1mAGFBLsL0M" resolve="BooleanTestData" />
          <node concept="37vLTw" id="PDjyzjRFio" role="37wK5m">
            <ref role="3cqZAo" node="PDjyzjRFii" resolve="test" />
          </node>
          <node concept="37vLTw" id="PDjyzjRFip" role="37wK5m">
            <ref role="3cqZAo" node="PDjyzjRFik" resolve="specimenType" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="5jVYnMGMAML" role="jymVt" />
    <node concept="3clFb_" id="5jVYnMGMAEm" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="accept" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3Tm1VV" id="5jVYnMGMAEn" role="1B3o_S" />
      <node concept="3uibUv" id="5jVYnMGUYaN" role="3clF45">
        <ref role="3uigEE" to="8r9s:2XLt5KVsHET" resolve="LisResponse" />
        <node concept="3uibUv" id="5jVYnMGUYpP" role="11_B2D">
          <ref role="3uigEE" to="wyt6:~Boolean" resolve="Boolean" />
        </node>
      </node>
      <node concept="37vLTG" id="5jVYnMGMAEp" role="3clF46">
        <property role="TrG5h" value="visitor" />
        <node concept="3uibUv" id="5jVYnMGMAEq" role="1tU5fm">
          <ref role="3uigEE" to="8r9s:5jVYnMGKCt5" resolve="DataValueVisitor" />
        </node>
      </node>
      <node concept="3clFbS" id="5jVYnMGMAEr" role="3clF47">
        <node concept="3clFbJ" id="5jVYnMGMAEs" role="3cqZAp">
          <node concept="2ZW3vV" id="5jVYnMGMAEt" role="3clFbw">
            <node concept="3uibUv" id="5jVYnMGMAEu" role="2ZW6by">
              <ref role="3uigEE" node="5jVYnMGI$WP" resolve="LaboratoryTestDataValueVisitor" />
            </node>
            <node concept="37vLTw" id="5jVYnMGMAEv" role="2ZW6bz">
              <ref role="3cqZAo" node="5jVYnMGMAEp" resolve="visitor" />
            </node>
          </node>
          <node concept="3clFbS" id="5jVYnMGMAEw" role="3clFbx">
            <node concept="3cpWs6" id="5jVYnMGMAEx" role="3cqZAp">
              <node concept="2OqwBi" id="5jVYnMGMAEy" role="3cqZAk">
                <node concept="1eOMI4" id="5jVYnMGMAEz" role="2Oq$k0">
                  <node concept="10QFUN" id="5jVYnMGMAE$" role="1eOMHV">
                    <node concept="3uibUv" id="5jVYnMGMAE_" role="10QFUM">
                      <ref role="3uigEE" node="5jVYnMGI$WP" resolve="LaboratoryTestDataValueVisitor" />
                    </node>
                    <node concept="37vLTw" id="5jVYnMGMAEA" role="10QFUP">
                      <ref role="3cqZAo" node="5jVYnMGMAEp" resolve="visitor" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="5jVYnMGMAEB" role="2OqNvi">
                  <ref role="37wK5l" node="5jVYnMGIDlH" resolve="visit" />
                  <node concept="Xjq3P" id="5jVYnMGMAEC" role="37wK5m" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="5jVYnMGMAED" role="3cqZAp">
          <node concept="10Nm6u" id="5jVYnMGMAEE" role="3cqZAk" />
        </node>
      </node>
      <node concept="2AHcQZ" id="5jVYnMGMAEF" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
  </node>
  <node concept="3HP615" id="5jVYnMGI$WP">
    <property role="TrG5h" value="LaboratoryTestDataValueVisitor" />
    <node concept="3clFb_" id="5jVYnMGIDlH" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="visit" />
      <node concept="3clFbS" id="5jVYnMGIDlK" role="3clF47" />
      <node concept="3Tm1VV" id="5jVYnMGIDlL" role="1B3o_S" />
      <node concept="37vLTG" id="5jVYnMGIDmu" role="3clF46">
        <property role="TrG5h" value="dataValue" />
        <node concept="3uibUv" id="5jVYnMGIDmt" role="1tU5fm">
          <ref role="3uigEE" node="PDjyzjRFic" resolve="TestHasBeenPerformed" />
        </node>
      </node>
      <node concept="3uibUv" id="5jVYnMGUYCw" role="3clF45">
        <ref role="3uigEE" to="8r9s:2XLt5KVsHET" resolve="LisResponse" />
        <node concept="3uibUv" id="5jVYnMGUYCx" role="11_B2D">
          <ref role="3uigEE" to="wyt6:~Boolean" resolve="Boolean" />
        </node>
      </node>
    </node>
    <node concept="3clFb_" id="5jVYnMGIDnM" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="visit" />
      <node concept="3clFbS" id="5jVYnMGIDnN" role="3clF47" />
      <node concept="3Tm1VV" id="5jVYnMGIDnO" role="1B3o_S" />
      <node concept="37vLTG" id="5jVYnMGIDnQ" role="3clF46">
        <property role="TrG5h" value="dataValue" />
        <node concept="3uibUv" id="5jVYnMGIDtK" role="1tU5fm">
          <ref role="3uigEE" node="1mAGFBLsTyX" resolve="TestHasPreviouslyBeenPerformed" />
        </node>
      </node>
      <node concept="3uibUv" id="5jVYnMGUYKU" role="3clF45">
        <ref role="3uigEE" to="8r9s:2XLt5KVsHET" resolve="LisResponse" />
        <node concept="3uibUv" id="5jVYnMGUYKV" role="11_B2D">
          <ref role="3uigEE" to="wyt6:~Boolean" resolve="Boolean" />
        </node>
      </node>
    </node>
    <node concept="3clFb_" id="5jVYnMGIDoS" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="visit" />
      <node concept="3clFbS" id="5jVYnMGIDoT" role="3clF47" />
      <node concept="3Tm1VV" id="5jVYnMGIDoU" role="1B3o_S" />
      <node concept="37vLTG" id="5jVYnMGIDoW" role="3clF46">
        <property role="TrG5h" value="dataValue" />
        <node concept="3uibUv" id="5jVYnMGIVH3" role="1tU5fm">
          <ref role="3uigEE" node="1mAGFBLlFuh" resolve="TestHasRequest" />
        </node>
      </node>
      <node concept="3uibUv" id="5jVYnMGUYT_" role="3clF45">
        <ref role="3uigEE" to="8r9s:2XLt5KVsHET" resolve="LisResponse" />
        <node concept="3uibUv" id="5jVYnMGUYTA" role="11_B2D">
          <ref role="3uigEE" to="wyt6:~Boolean" resolve="Boolean" />
        </node>
      </node>
    </node>
    <node concept="3clFb_" id="5jVYnMGIDqm" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="visit" />
      <node concept="3clFbS" id="5jVYnMGIDqn" role="3clF47" />
      <node concept="3Tm1VV" id="5jVYnMGIDqo" role="1B3o_S" />
      <node concept="37vLTG" id="5jVYnMGIDqq" role="3clF46">
        <property role="TrG5h" value="dataValue" />
        <node concept="3uibUv" id="5jVYnMGJvWD" role="1tU5fm">
          <ref role="3uigEE" node="4V3GMfXxDw9" resolve="TestPreviousResultIsValid" />
        </node>
      </node>
      <node concept="3uibUv" id="5jVYnMGUZ2w" role="3clF45">
        <ref role="3uigEE" to="8r9s:2XLt5KVsHET" resolve="LisResponse" />
        <node concept="3uibUv" id="5jVYnMGUZ2x" role="11_B2D">
          <ref role="3uigEE" to="wyt6:~Boolean" resolve="Boolean" />
        </node>
      </node>
    </node>
    <node concept="3clFb_" id="5jVYnMGJw8g" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="visit" />
      <node concept="3clFbS" id="5jVYnMGJw8j" role="3clF47" />
      <node concept="3Tm1VV" id="5jVYnMGJw8k" role="1B3o_S" />
      <node concept="37vLTG" id="5jVYnMGJwcE" role="3clF46">
        <property role="TrG5h" value="dataValue" />
        <node concept="3uibUv" id="5jVYnMGJwcD" role="1tU5fm">
          <ref role="3uigEE" node="1mAGFBLsTwz" resolve="TestResultIsValid" />
        </node>
      </node>
      <node concept="3uibUv" id="5jVYnMGUZbE" role="3clF45">
        <ref role="3uigEE" to="8r9s:2XLt5KVsHET" resolve="LisResponse" />
        <node concept="3uibUv" id="5jVYnMGUZbF" role="11_B2D">
          <ref role="3uigEE" to="wyt6:~Boolean" resolve="Boolean" />
        </node>
      </node>
    </node>
    <node concept="3clFb_" id="5jVYnMGJBVs" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="visit" />
      <node concept="3clFbS" id="5jVYnMGJBVv" role="3clF47" />
      <node concept="3Tm1VV" id="5jVYnMGJBVw" role="1B3o_S" />
      <node concept="37vLTG" id="5jVYnMGJC20" role="3clF46">
        <property role="TrG5h" value="dataValue" />
        <node concept="3uibUv" id="5jVYnMGJC1Z" role="1tU5fm">
          <ref role="3uigEE" node="1mAGFBLsOMS" resolve="TestTimeSinceLastRequested" />
        </node>
      </node>
      <node concept="3uibUv" id="5jVYnMGUZl2" role="3clF45">
        <ref role="3uigEE" to="8r9s:2XLt5KVsHET" resolve="LisResponse" />
        <node concept="3uibUv" id="5jVYnMGUZuB" role="11_B2D">
          <ref role="3uigEE" to="28m1:~Duration" resolve="Duration" />
        </node>
      </node>
    </node>
    <node concept="3Tm1VV" id="5jVYnMGI$WQ" role="1B3o_S" />
    <node concept="3uibUv" id="5jVYnMGM8ft" role="3HQHJm">
      <ref role="3uigEE" to="8r9s:5jVYnMGKCt5" resolve="DataValueVisitor" />
    </node>
  </node>
</model>

