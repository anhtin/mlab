package no.uio.mLab.decisions.data.laboratoryTest.structure;

/*Generated by MPS */

import jetbrains.mps.smodel.runtime.BaseStructureAspectDescriptor;
import jetbrains.mps.smodel.runtime.ConceptDescriptor;
import java.util.Collection;
import java.util.Arrays;
import org.jetbrains.annotations.Nullable;
import jetbrains.mps.smodel.adapter.ids.SConceptId;
import org.jetbrains.mps.openapi.language.SAbstractConcept;
import jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2;

public class StructureAspectDescriptor extends BaseStructureAspectDescriptor {
  /*package*/ final ConceptDescriptor myConceptILaboratoryTestDataValue = createDescriptorForILaboratoryTestDataValue();
  /*package*/ final ConceptDescriptor myConceptITranslatableLaboratoryDataConcept = createDescriptorForITranslatableLaboratoryDataConcept();
  /*package*/ final ConceptDescriptor myConceptLaboratoryTestHasBeenPerformed = createDescriptorForLaboratoryTestHasBeenPerformed();
  /*package*/ final ConceptDescriptor myConceptLaboratoryTestHasPreviouslyBeenPerformed = createDescriptorForLaboratoryTestHasPreviouslyBeenPerformed();
  /*package*/ final ConceptDescriptor myConceptLaboratoryTestHasRequest = createDescriptorForLaboratoryTestHasRequest();
  /*package*/ final ConceptDescriptor myConceptLaboratoryTestPreviousResultIsValid = createDescriptorForLaboratoryTestPreviousResultIsValid();
  /*package*/ final ConceptDescriptor myConceptLaboratoryTestResultIsValid = createDescriptorForLaboratoryTestResultIsValid();
  /*package*/ final ConceptDescriptor myConceptLaboratoryTestTimeSinceLastRequest = createDescriptorForLaboratoryTestTimeSinceLastRequest();
  private final LanguageConceptSwitch myIndexSwitch;

  public StructureAspectDescriptor() {
    myIndexSwitch = new LanguageConceptSwitch();
  }

  @Override
  public Collection<ConceptDescriptor> getDescriptors() {
    return Arrays.asList(myConceptILaboratoryTestDataValue, myConceptITranslatableLaboratoryDataConcept, myConceptLaboratoryTestHasBeenPerformed, myConceptLaboratoryTestHasPreviouslyBeenPerformed, myConceptLaboratoryTestHasRequest, myConceptLaboratoryTestPreviousResultIsValid, myConceptLaboratoryTestResultIsValid, myConceptLaboratoryTestTimeSinceLastRequest);
  }

  @Override
  @Nullable
  public ConceptDescriptor getDescriptor(SConceptId id) {
    switch (myIndexSwitch.index(id)) {
      case LanguageConceptSwitch.ILaboratoryTestDataValue:
        return myConceptILaboratoryTestDataValue;
      case LanguageConceptSwitch.ITranslatableLaboratoryDataConcept:
        return myConceptITranslatableLaboratoryDataConcept;
      case LanguageConceptSwitch.LaboratoryTestHasBeenPerformed:
        return myConceptLaboratoryTestHasBeenPerformed;
      case LanguageConceptSwitch.LaboratoryTestHasPreviouslyBeenPerformed:
        return myConceptLaboratoryTestHasPreviouslyBeenPerformed;
      case LanguageConceptSwitch.LaboratoryTestHasRequest:
        return myConceptLaboratoryTestHasRequest;
      case LanguageConceptSwitch.LaboratoryTestPreviousResultIsValid:
        return myConceptLaboratoryTestPreviousResultIsValid;
      case LanguageConceptSwitch.LaboratoryTestResultIsValid:
        return myConceptLaboratoryTestResultIsValid;
      case LanguageConceptSwitch.LaboratoryTestTimeSinceLastRequest:
        return myConceptLaboratoryTestTimeSinceLastRequest;
      default:
        return null;
    }
  }

  /*package*/ int internalIndex(SAbstractConcept c) {
    return myIndexSwitch.index(c);
  }

  private static ConceptDescriptor createDescriptorForILaboratoryTestDataValue() {
    ConceptDescriptorBuilder2 b = new ConceptDescriptorBuilder2("no.uio.mLab.decisions.data.laboratoryTest", "ILaboratoryTestDataValue", 0x93a279523baa41caL, 0x9e72e2a9cf1b3f77L, 0x49c529a683dab1c8L);
    b.interface_();
    b.parent(0x93a279523baa41caL, 0x9e72e2a9cf1b3f77L, 0x4ec3b323fd7e8569L);
    b.parent(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x6c7943d5b2724944L);
    b.origin("r:856e974b-6e42-49cc-99cd-c3adb4161c21(no.uio.mLab.decisions.data.laboratoryTest.structure)/5315700730334196168");
    b.version(2);
    b.aggregate("testReference", 0x6c7943d5b24c7c56L).target(0x4a652d5536844d2dL, 0x98c92ef46f124c44L, 0x4dbaf0338f8270c0L).optional(false).ordered(true).multiple(false).origin("7816353213376658518").done();
    b.aggregate("specimenTypeReference", 0x49c529a683dab1caL).target(0x4a652d5536844d2dL, 0x98c92ef46f124c44L, 0x6511ed286229eb8dL).optional(true).ordered(true).multiple(false).origin("5315700730334196170").done();
    return b.create();
  }
  private static ConceptDescriptor createDescriptorForITranslatableLaboratoryDataConcept() {
    ConceptDescriptorBuilder2 b = new ConceptDescriptorBuilder2("no.uio.mLab.decisions.data.laboratoryTest", "ITranslatableLaboratoryDataConcept", 0x93a279523baa41caL, 0x9e72e2a9cf1b3f77L, 0x4ec3b323fd7e8569L);
    b.interface_();
    b.parent(0x6adcef385cdc48dcL, 0xb225be76276615fdL, 0x5f0f3639007cc07fL);
    b.origin("r:856e974b-6e42-49cc-99cd-c3adb4161c21(no.uio.mLab.decisions.data.laboratoryTest.structure)/5675576922575111529");
    b.version(2);
    return b.create();
  }
  private static ConceptDescriptor createDescriptorForLaboratoryTestHasBeenPerformed() {
    ConceptDescriptorBuilder2 b = new ConceptDescriptorBuilder2("no.uio.mLab.decisions.data.laboratoryTest", "LaboratoryTestHasBeenPerformed", 0x93a279523baa41caL, 0x9e72e2a9cf1b3f77L, 0xd694e28d3d99669L);
    b.class_(false, false, false);
    b.super_("no.uio.mLab.decisions.core.structure.BooleanDataValue", 0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x15a6b2b9f079a4e6L);
    b.parent(0x93a279523baa41caL, 0x9e72e2a9cf1b3f77L, 0x49c529a683dab1c8L);
    b.origin("r:856e974b-6e42-49cc-99cd-c3adb4161c21(no.uio.mLab.decisions.data.laboratoryTest.structure)/966389532307592809");
    b.version(2);
    return b.create();
  }
  private static ConceptDescriptor createDescriptorForLaboratoryTestHasPreviouslyBeenPerformed() {
    ConceptDescriptorBuilder2 b = new ConceptDescriptorBuilder2("no.uio.mLab.decisions.data.laboratoryTest", "LaboratoryTestHasPreviouslyBeenPerformed", 0x93a279523baa41caL, 0x9e72e2a9cf1b3f77L, 0x49c529a683d98ba9L);
    b.class_(false, false, false);
    b.super_("no.uio.mLab.decisions.core.structure.BooleanDataValue", 0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x15a6b2b9f079a4e6L);
    b.parent(0x93a279523baa41caL, 0x9e72e2a9cf1b3f77L, 0x4ec3b323fd7e8569L);
    b.parent(0x93a279523baa41caL, 0x9e72e2a9cf1b3f77L, 0x49c529a683dab1c8L);
    b.origin("r:856e974b-6e42-49cc-99cd-c3adb4161c21(no.uio.mLab.decisions.data.laboratoryTest.structure)/5315700730334120873");
    b.version(2);
    return b.create();
  }
  private static ConceptDescriptor createDescriptorForLaboratoryTestHasRequest() {
    ConceptDescriptorBuilder2 b = new ConceptDescriptorBuilder2("no.uio.mLab.decisions.data.laboratoryTest", "LaboratoryTestHasRequest", 0x93a279523baa41caL, 0x9e72e2a9cf1b3f77L, 0x6511ed28623b6763L);
    b.class_(false, false, false);
    b.super_("no.uio.mLab.decisions.core.structure.BooleanDataValue", 0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x15a6b2b9f079a4e6L);
    b.parent(0x93a279523baa41caL, 0x9e72e2a9cf1b3f77L, 0x49c529a683dab1c8L);
    b.origin("r:856e974b-6e42-49cc-99cd-c3adb4161c21(no.uio.mLab.decisions.data.laboratoryTest.structure)/7282862830137337699");
    b.version(2);
    return b.create();
  }
  private static ConceptDescriptor createDescriptorForLaboratoryTestPreviousResultIsValid() {
    ConceptDescriptorBuilder2 b = new ConceptDescriptorBuilder2("no.uio.mLab.decisions.data.laboratoryTest", "LaboratoryTestPreviousResultIsValid", 0x93a279523baa41caL, 0x9e72e2a9cf1b3f77L, 0x49c529a683d98bacL);
    b.class_(false, false, false);
    b.super_("no.uio.mLab.decisions.core.structure.BooleanDataValue", 0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x15a6b2b9f079a4e6L);
    b.parent(0x93a279523baa41caL, 0x9e72e2a9cf1b3f77L, 0x4ec3b323fd7e8569L);
    b.parent(0x93a279523baa41caL, 0x9e72e2a9cf1b3f77L, 0x49c529a683dab1c8L);
    b.origin("r:856e974b-6e42-49cc-99cd-c3adb4161c21(no.uio.mLab.decisions.data.laboratoryTest.structure)/5315700730334120876");
    b.version(2);
    return b.create();
  }
  private static ConceptDescriptor createDescriptorForLaboratoryTestResultIsValid() {
    ConceptDescriptorBuilder2 b = new ConceptDescriptorBuilder2("no.uio.mLab.decisions.data.laboratoryTest", "LaboratoryTestResultIsValid", 0x93a279523baa41caL, 0x9e72e2a9cf1b3f77L, 0x49c529a683d98bafL);
    b.class_(false, false, false);
    b.super_("no.uio.mLab.decisions.core.structure.BooleanDataValue", 0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x15a6b2b9f079a4e6L);
    b.parent(0x93a279523baa41caL, 0x9e72e2a9cf1b3f77L, 0x4ec3b323fd7e8569L);
    b.parent(0x93a279523baa41caL, 0x9e72e2a9cf1b3f77L, 0x49c529a683dab1c8L);
    b.origin("r:856e974b-6e42-49cc-99cd-c3adb4161c21(no.uio.mLab.decisions.data.laboratoryTest.structure)/5315700730334120879");
    b.version(2);
    return b.create();
  }
  private static ConceptDescriptor createDescriptorForLaboratoryTestTimeSinceLastRequest() {
    ConceptDescriptorBuilder2 b = new ConceptDescriptorBuilder2("no.uio.mLab.decisions.data.laboratoryTest", "LaboratoryTestTimeSinceLastRequest", 0x93a279523baa41caL, 0x9e72e2a9cf1b3f77L, 0x49c529a684320bc4L);
    b.class_(false, false, false);
    b.super_("no.uio.mLab.decisions.core.structure.TimeSpanData", 0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x15a6b2b9f07bcb6bL);
    b.parent(0x93a279523baa41caL, 0x9e72e2a9cf1b3f77L, 0x4ec3b323fd7e8569L);
    b.parent(0x93a279523baa41caL, 0x9e72e2a9cf1b3f77L, 0x49c529a683dab1c8L);
    b.origin("r:856e974b-6e42-49cc-99cd-c3adb4161c21(no.uio.mLab.decisions.data.laboratoryTest.structure)/5315700730339920836");
    b.version(2);
    return b.create();
  }
}
