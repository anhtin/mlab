<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:2c3800ab-cd1f-48ee-8876-69529185b5c0(no.uio.mLab.decisions.data.microbiology.behavior)">
  <persistence version="9" />
  <languages>
    <use id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior" version="1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="j0tk" ref="r:2895f9f0-ca08-4015-b25a-0b35f51e5738(no.uio.mLab.decisions.data.microbiology.structure)" />
    <import index="gtaw" ref="r:8ca2c605-045f-4975-a8b1-e0310faaf919(no.uio.mLab.decisions.data.microbiology.translations)" />
    <import index="1yj" ref="r:0c887d67-81ab-420f-9a35-a9f6bf96c975(no.uio.mLab.shared.behavior)" />
    <import index="vbok" ref="r:6cc208d3-a370-49ca-a6ac-ded97805a924(no.uio.mLab.shared.structure)" />
    <import index="wb6c" ref="r:19d15037-83c1-4ae2-b66a-763828cb8279(no.uio.mLab.decisions.core.behavior)" />
    <import index="7f9y" ref="r:790ac37e-42d3-4dd3-977c-e8fe9cc56a45(no.uio.mLab.decisions.core.structure)" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" />
  </imports>
  <registry>
    <language id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior">
      <concept id="6496299201655527393" name="jetbrains.mps.lang.behavior.structure.LocalBehaviorMethodCall" flags="nn" index="BsUDl" />
      <concept id="1225194240794" name="jetbrains.mps.lang.behavior.structure.ConceptBehavior" flags="ng" index="13h7C7">
        <reference id="1225194240799" name="concept" index="13h7C2" />
        <child id="1225194240805" name="method" index="13h7CS" />
        <child id="1225194240801" name="constructor" index="13h7CW" />
      </concept>
      <concept id="1225194413805" name="jetbrains.mps.lang.behavior.structure.ConceptConstructorDeclaration" flags="in" index="13hLZK" />
      <concept id="1225194472830" name="jetbrains.mps.lang.behavior.structure.ConceptMethodDeclaration" flags="ng" index="13i0hz">
        <property id="5864038008284099149" name="isStatic" index="2Ki8OM" />
        <property id="1225194472832" name="isVirtual" index="13i0it" />
        <property id="1225194472834" name="isAbstract" index="13i0iv" />
        <reference id="1225194472831" name="overriddenMethod" index="13i0hy" />
      </concept>
      <concept id="1225194691553" name="jetbrains.mps.lang.behavior.structure.ThisNodeExpression" flags="nn" index="13iPFW" />
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="4836112446988635817" name="jetbrains.mps.baseLanguage.structure.UndefinedType" flags="in" index="2jxLKc" />
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1173175405605" name="jetbrains.mps.baseLanguage.structure.ArrayAccessExpression" flags="nn" index="AH0OO">
        <child id="1173175577737" name="index" index="AHEQo" />
        <child id="1173175590490" name="array" index="AHHXb" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1070534644030" name="jetbrains.mps.baseLanguage.structure.BooleanType" flags="in" index="10P_77" />
      <concept id="1070534760951" name="jetbrains.mps.baseLanguage.structure.ArrayType" flags="in" index="10Q1$e">
        <child id="1070534760952" name="componentType" index="10Q1$1" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1154542696413" name="jetbrains.mps.baseLanguage.structure.ArrayCreatorWithInitializer" flags="nn" index="3g6Rrh">
        <child id="1154542793668" name="componentType" index="3g7fb8" />
        <child id="1154542803372" name="initValue" index="3g7hyw" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
      <concept id="1146644623116" name="jetbrains.mps.baseLanguage.structure.PrivateVisibility" flags="nn" index="3Tm6S6" />
    </language>
    <language id="fd392034-7849-419d-9071-12563d152375" name="jetbrains.mps.baseLanguage.closures">
      <concept id="1199569711397" name="jetbrains.mps.baseLanguage.closures.structure.ClosureLiteral" flags="nn" index="1bVj0M">
        <child id="1199569906740" name="parameter" index="1bW2Oz" />
        <child id="1199569916463" name="body" index="1bW5cS" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="1180031783296" name="jetbrains.mps.lang.smodel.structure.Concept_IsSubConceptOfOperation" flags="nn" index="2Zo12i">
        <child id="1180031783297" name="conceptArgument" index="2Zo12j" />
      </concept>
      <concept id="6677504323281689838" name="jetbrains.mps.lang.smodel.structure.SConceptType" flags="in" index="3bZ5Sz">
        <reference id="6677504323281689839" name="conceptDeclaraton" index="3bZ5Sy" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1204796164442" name="jetbrains.mps.baseLanguage.collections.structure.InternalSequenceOperation" flags="nn" index="23sCx2">
        <child id="1204796294226" name="closure" index="23t8la" />
      </concept>
      <concept id="1203518072036" name="jetbrains.mps.baseLanguage.collections.structure.SmartClosureParameterDeclaration" flags="ig" index="Rh6nW" />
      <concept id="1240687580870" name="jetbrains.mps.baseLanguage.collections.structure.JoinOperation" flags="nn" index="3uJxvA">
        <child id="1240687658305" name="delimiter" index="3uJOhx" />
      </concept>
      <concept id="1202128969694" name="jetbrains.mps.baseLanguage.collections.structure.SelectOperation" flags="nn" index="3$u5V9" />
    </language>
  </registry>
  <node concept="13h7C7" id="1Hxyv4EUPRh">
    <property role="3GE5qa" value="shared" />
    <ref role="13h7C2" to="j0tk:1Hxyv4EUOl1" resolve="ITranslatableBiochemistryConcept" />
    <node concept="13i0hz" id="1Hxyv4EQSwS" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayTranslations" />
      <property role="2Ki8OM" value="true" />
      <node concept="3Tm1VV" id="1Hxyv4EQSwT" role="1B3o_S" />
      <node concept="3uibUv" id="2$xY$aF7f2E" role="3clF45">
        <ref role="3uigEE" to="gtaw:4zMac8rUNtP" resolve="IMicrobiologyDataTranslations" />
      </node>
      <node concept="3clFbS" id="1Hxyv4EQSwV" role="3clF47">
        <node concept="3clFbF" id="1Hxyv4EQSzj" role="3cqZAp">
          <node concept="10M0yZ" id="2$xY$aF7f3u" role="3clFbG">
            <ref role="3cqZAo" to="gtaw:4zMac8rVBeK" resolve="displayTranslations" />
            <ref role="1PxDUh" to="gtaw:4zMac8rUNsN" resolve="MicrobiologyDataTranslationProvider" />
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="1Hxyv4EQSBV" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getGenerationTranslations" />
      <property role="2Ki8OM" value="true" />
      <node concept="3Tm1VV" id="1Hxyv4EQSBW" role="1B3o_S" />
      <node concept="3uibUv" id="2$xY$aF7f43" role="3clF45">
        <ref role="3uigEE" to="gtaw:4zMac8rUNtP" resolve="IMicrobiologyDataTranslations" />
      </node>
      <node concept="3clFbS" id="1Hxyv4EQSBY" role="3clF47">
        <node concept="3clFbF" id="1Hxyv4EQSEX" role="3cqZAp">
          <node concept="10M0yZ" id="2$xY$aF7f4C" role="3clFbG">
            <ref role="3cqZAo" to="gtaw:1Hxyv4DUmfq" resolve="generationTranslations" />
            <ref role="1PxDUh" to="gtaw:4zMac8rUNsN" resolve="MicrobiologyDataTranslationProvider" />
          </node>
        </node>
      </node>
    </node>
    <node concept="13hLZK" id="1Hxyv4EUPRi" role="13h7CW">
      <node concept="3clFbS" id="1Hxyv4EUPRj" role="2VODD2" />
    </node>
  </node>
  <node concept="13h7C7" id="6khVixyaOgv">
    <property role="3GE5qa" value="base.data.cultureFindings" />
    <ref role="13h7C2" to="j0tk:6khVixyaF5m" resolve="FindingsFromAnyCultureTest" />
    <node concept="13hLZK" id="6khVixyaOgw" role="13h7CW">
      <node concept="3clFbS" id="6khVixyaOgx" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6khVixyaOgE" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="6khVixyaOgF" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixyaOgK" role="3clF47">
        <node concept="3clFbF" id="6khVixyaOqu" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixyaOxq" role="3clFbG">
            <node concept="BsUDl" id="6khVixyaOqt" role="2Oq$k0">
              <ref role="37wK5l" node="1Hxyv4EQSwS" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixyaOSv" role="2OqNvi">
              <ref role="37wK5l" to="gtaw:6khVixyaM_f" resolve="getFindingsFromAnyCultureTestAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixyaOgL" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixyaOgQ" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="6khVixyaOgR" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixyaOgW" role="3clF47">
        <node concept="3clFbF" id="6khVixyaOXp" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixyaOXq" role="3clFbG">
            <node concept="BsUDl" id="6khVixyaOXr" role="2Oq$k0">
              <ref role="37wK5l" node="1Hxyv4EQSwS" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixyaOXs" role="2OqNvi">
              <ref role="37wK5l" to="gtaw:6khVixyaMAm" resolve="getFindingsFromAnyCultureTestDescription" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixyaOgX" role="3clF45" />
    </node>
    <node concept="13i0hz" id="2FjKBCR2T0q" role="13h7CS">
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getDisplayAliasWithSpecimenType" />
      <node concept="3Tm1VV" id="2FjKBCR2T0r" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCR2Td8" role="3clF45" />
      <node concept="3clFbS" id="2FjKBCR2T0t" role="3clF47">
        <node concept="3clFbF" id="2FjKBCR2TeW" role="3cqZAp">
          <node concept="2OqwBi" id="2FjKBCR2TnU" role="3clFbG">
            <node concept="BsUDl" id="2FjKBCR2Tft" role="2Oq$k0">
              <ref role="37wK5l" node="1Hxyv4EQSwS" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="2FjKBCR2TtV" role="2OqNvi">
              <ref role="37wK5l" to="gtaw:2FjKBCR2CG5" resolve="getFindingsFromAnyCultureTestFromSpecimenTypeAlias" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="6khVixyaOh2" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getGenerationAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:4QUW3efxqUv" resolve="getGenerationAlias" />
      <node concept="3Tm1VV" id="6khVixyaOh3" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixyaOh8" role="3clF47">
        <node concept="3clFbF" id="6khVixyaOZh" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixyaOZi" role="3clFbG">
            <node concept="BsUDl" id="6khVixyaP0_" role="2Oq$k0">
              <ref role="37wK5l" node="1Hxyv4EQSBV" resolve="getGenerationTranslations" />
            </node>
            <node concept="liA8E" id="6khVixyaOZk" role="2OqNvi">
              <ref role="37wK5l" to="gtaw:6khVixyaM_f" resolve="getFindingsFromAnyCultureTestAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixyaOh9" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixyaOhe" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="toDisplayString" />
      <ref role="13i0hy" to="1yj:499Gn2DGTs1" resolve="toDisplayString" />
      <node concept="3Tm1VV" id="6khVixyaOhf" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixyaOhq" role="3clF47">
        <node concept="3cpWs8" id="6khVixybmRy" role="3cqZAp">
          <node concept="3cpWsn" id="6khVixybmR_" role="3cpWs9">
            <property role="TrG5h" value="alias" />
            <node concept="17QB3L" id="6khVixybmRw" role="1tU5fm" />
            <node concept="BsUDl" id="6khVixyc0wk" role="33vP2m">
              <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="6khVixyaP1s" role="3cqZAp">
          <node concept="2YIFZM" id="6khVixyaP1K" role="3clFbG">
            <ref role="37wK5l" to="wyt6:~String.format(java.lang.String,java.lang.Object...):java.lang.String" resolve="format" />
            <ref role="1Pybhc" to="wyt6:~String" resolve="String" />
            <node concept="Xl_RD" id="6khVixyaP2O" role="37wK5m">
              <property role="Xl_RC" value="%s %s" />
            </node>
            <node concept="37vLTw" id="6khVixybovv" role="37wK5m">
              <ref role="3cqZAo" node="6khVixybmR_" resolve="alias" />
            </node>
            <node concept="2OqwBi" id="6khVixybrY2" role="37wK5m">
              <node concept="2OqwBi" id="6khVixybqZ0" role="2Oq$k0">
                <node concept="13iPFW" id="6khVixybqJB" role="2Oq$k0" />
                <node concept="3TrEf2" id="6khVixybrvz" role="2OqNvi">
                  <ref role="3Tt5mk" to="j0tk:6khVixybjNf" resolve="specimenTypeReference" />
                </node>
              </node>
              <node concept="2qgKlT" id="6khVixybsn4" role="2OqNvi">
                <ref role="37wK5l" to="1yj:499Gn2DGTs1" resolve="toDisplayString" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixyaOhr" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixyaOhw" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="toGenerationString" />
      <ref role="13i0hy" to="1yj:499Gn2DGU_t" resolve="toGenerationString" />
      <node concept="3Tm1VV" id="6khVixyaOhx" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixyaOhG" role="3clF47">
        <node concept="3cpWs8" id="6khVixybsBd" role="3cqZAp">
          <node concept="3cpWsn" id="6khVixybsBe" role="3cpWs9">
            <property role="TrG5h" value="alias" />
            <node concept="17QB3L" id="6khVixybsBf" role="1tU5fm" />
            <node concept="BsUDl" id="6khVixyc1co" role="33vP2m">
              <ref role="37wK5l" to="1yj:4QUW3efxqUv" resolve="getGenerationAlias" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="6khVixybsBp" role="3cqZAp">
          <node concept="2YIFZM" id="6khVixybsBq" role="3clFbG">
            <ref role="37wK5l" to="wyt6:~String.format(java.lang.String,java.lang.Object...):java.lang.String" resolve="format" />
            <ref role="1Pybhc" to="wyt6:~String" resolve="String" />
            <node concept="Xl_RD" id="6khVixybsBr" role="37wK5m">
              <property role="Xl_RC" value="%s %s" />
            </node>
            <node concept="37vLTw" id="6khVixybsBs" role="37wK5m">
              <ref role="3cqZAo" node="6khVixybsBe" resolve="alias" />
            </node>
            <node concept="2OqwBi" id="6khVixybsBz" role="37wK5m">
              <node concept="2OqwBi" id="6khVixybsB$" role="2Oq$k0">
                <node concept="13iPFW" id="6khVixybsB_" role="2Oq$k0" />
                <node concept="3TrEf2" id="6khVixybsBA" role="2OqNvi">
                  <ref role="3Tt5mk" to="j0tk:6khVixybjNf" resolve="specimenTypeReference" />
                </node>
              </node>
              <node concept="2qgKlT" id="6khVixybsBB" role="2OqNvi">
                <ref role="37wK5l" to="1yj:499Gn2DGTs1" resolve="toDisplayString" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixyaOhH" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="6khVixyb84f">
    <property role="3GE5qa" value="base.constraints.cultureFindings.complex" />
    <ref role="13h7C2" to="j0tk:6khVixyb4G4" resolve="CultureFindingsIncludeAnyOf" />
    <node concept="13hLZK" id="6khVixyb84g" role="13h7CW">
      <node concept="3clFbS" id="6khVixyb84h" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6khVixyb84$" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="6khVixyb84_" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixyb84E" role="3clF47">
        <node concept="3clFbF" id="6khVixyb8eo" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixyb8lc" role="3clFbG">
            <node concept="BsUDl" id="6khVixyb8en" role="2Oq$k0">
              <ref role="37wK5l" node="1Hxyv4EQSwS" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixyb8qX" role="2OqNvi">
              <ref role="37wK5l" to="gtaw:6khVixyb4Uu" resolve="getCultureFindingsIncludeAnyOfAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixyb84F" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixyb84K" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="6khVixyb84L" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixyb84Q" role="3clF47">
        <node concept="3clFbF" id="6khVixyb8rB" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixyb8rD" role="3clFbG">
            <node concept="BsUDl" id="6khVixyb8rE" role="2Oq$k0">
              <ref role="37wK5l" node="1Hxyv4EQSwS" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixyb8rF" role="2OqNvi">
              <ref role="37wK5l" to="gtaw:6khVixyb4Wd" resolve="getCultureFindingsIncludeAnyOfDescription" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixyb84R" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixyb84W" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getGenerationAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:4QUW3efxqUv" resolve="getGenerationAlias" />
      <node concept="3Tm1VV" id="6khVixyb84X" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixyb852" role="3clF47">
        <node concept="3clFbF" id="6khVixyb857" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixyb8ty" role="3clFbG">
            <node concept="BsUDl" id="6khVixyb8uE" role="2Oq$k0">
              <ref role="37wK5l" node="1Hxyv4EQSBV" resolve="getGenerationTranslations" />
            </node>
            <node concept="liA8E" id="6khVixyb8t$" role="2OqNvi">
              <ref role="37wK5l" to="gtaw:6khVixyb4Uu" resolve="getCultureFindingsIncludeAnyOfAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixyb853" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="6khVixybgYN">
    <property role="3GE5qa" value="base.constraints.cultureFindings" />
    <ref role="13h7C2" to="j0tk:6khVixybgYp" resolve="CultureFindingsConstraint" />
    <node concept="13hLZK" id="6khVixybgYO" role="13h7CW">
      <node concept="3clFbS" id="6khVixybgYP" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6khVixybgYY" role="13h7CS">
      <property role="TrG5h" value="isAllowedToConstrain" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="13i0iv" value="false" />
      <ref role="13i0hy" to="wb6c:1mAGFBKNuSW" resolve="isAllowedToConstrain" />
      <node concept="3Tm1VV" id="6khVixybgYZ" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixybgZ6" role="3clF47">
        <node concept="3clFbF" id="6khVixybh6B" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixybhlM" role="3clFbG">
            <node concept="37vLTw" id="6khVixybh6A" role="2Oq$k0">
              <ref role="3cqZAo" node="6khVixybgZ7" resolve="concept" />
            </node>
            <node concept="2Zo12i" id="6khVixybi66" role="2OqNvi">
              <node concept="chp4Y" id="6khVixybid0" role="2Zo12j">
                <ref role="cht4Q" to="j0tk:6khVixyaF5n" resolve="CultureFindingsData" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="6khVixybgZ7" role="3clF46">
        <property role="TrG5h" value="concept" />
        <node concept="3bZ5Sz" id="6khVixybgZ8" role="1tU5fm">
          <ref role="3bZ5Sy" to="7f9y:1mAGFBKqQrZ" resolve="DataValue" />
        </node>
      </node>
      <node concept="10P_77" id="6khVixybgZ9" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="6khVixycq1q">
    <property role="3GE5qa" value="base.constraints.cultureFindings.complex" />
    <ref role="13h7C2" to="j0tk:6khVixycq0_" resolve="CultureFindingsIncludeAllOf" />
    <node concept="13hLZK" id="6khVixycq1r" role="13h7CW">
      <node concept="3clFbS" id="6khVixycq1s" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6khVixycq1H" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="6khVixycq1I" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixycq1N" role="3clF47">
        <node concept="3clFbF" id="6khVixycq1S" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixycBv1" role="3clFbG">
            <node concept="BsUDl" id="6khVixycBoe" role="2Oq$k0">
              <ref role="37wK5l" node="1Hxyv4EQSwS" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixycB$M" role="2OqNvi">
              <ref role="37wK5l" to="gtaw:6khVixybDN7" resolve="getCultureFindingsIncludeAllOfAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixycq1O" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixycq1T" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="6khVixycq1U" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixycq1Z" role="3clF47">
        <node concept="3clFbF" id="6khVixycB_H" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixycBGL" role="3clFbG">
            <node concept="BsUDl" id="6khVixycB_G" role="2Oq$k0">
              <ref role="37wK5l" node="1Hxyv4EQSwS" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixycBME" role="2OqNvi">
              <ref role="37wK5l" to="gtaw:6khVixybDNb" resolve="getCultureFindingsIncludeAllOfDescription" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixycq20" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixycq25" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getGenerationAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:4QUW3efxqUv" resolve="getGenerationAlias" />
      <node concept="3Tm1VV" id="6khVixycq26" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixycq2b" role="3clF47">
        <node concept="3clFbF" id="6khVixycBN_" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixycBUp" role="3clFbG">
            <node concept="BsUDl" id="6khVixycBN$" role="2Oq$k0">
              <ref role="37wK5l" node="1Hxyv4EQSBV" resolve="getGenerationTranslations" />
            </node>
            <node concept="liA8E" id="6khVixycC0a" role="2OqNvi">
              <ref role="37wK5l" to="gtaw:6khVixybDN7" resolve="getCultureFindingsIncludeAllOfAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixycq2c" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="6khVixycAVW">
    <property role="3GE5qa" value="base.constraints.cultureFindings.complex" />
    <ref role="13h7C2" to="j0tk:6khVixycqjC" resolve="ComplexCultureFindingsConstraint" />
    <node concept="13i0hz" id="6khVixycAYG" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="toDisplayString" />
      <ref role="13i0hy" to="1yj:499Gn2DGTs1" resolve="toDisplayString" />
      <node concept="3Tm1VV" id="6khVixycAYH" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixycAYI" role="3clF47">
        <node concept="3clFbF" id="6khVixycAYJ" role="3cqZAp">
          <node concept="2YIFZM" id="6khVixycAYK" role="3clFbG">
            <ref role="37wK5l" to="wyt6:~String.format(java.lang.String,java.lang.Object...):java.lang.String" resolve="format" />
            <ref role="1Pybhc" to="wyt6:~String" resolve="String" />
            <node concept="Xl_RD" id="6khVixycAYL" role="37wK5m">
              <property role="Xl_RC" value="%s [%s]" />
            </node>
            <node concept="BsUDl" id="6khVixycAYM" role="37wK5m">
              <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
            </node>
            <node concept="2OqwBi" id="6khVixycAYN" role="37wK5m">
              <node concept="2OqwBi" id="6khVixycAYO" role="2Oq$k0">
                <node concept="2OqwBi" id="6khVixycAYP" role="2Oq$k0">
                  <node concept="13iPFW" id="6khVixycAYQ" role="2Oq$k0" />
                  <node concept="3Tsc0h" id="6khVixycAYR" role="2OqNvi">
                    <ref role="3TtcxE" to="j0tk:6khVixycqjF" resolve="pathogensReferences" />
                  </node>
                </node>
                <node concept="3$u5V9" id="6khVixycAYS" role="2OqNvi">
                  <node concept="1bVj0M" id="6khVixycAYT" role="23t8la">
                    <node concept="3clFbS" id="6khVixycAYU" role="1bW5cS">
                      <node concept="3clFbF" id="6khVixycAYV" role="3cqZAp">
                        <node concept="2OqwBi" id="6khVixycAYW" role="3clFbG">
                          <node concept="37vLTw" id="6khVixycAYX" role="2Oq$k0">
                            <ref role="3cqZAo" node="6khVixycAYZ" resolve="it" />
                          </node>
                          <node concept="2qgKlT" id="6khVixycAYY" role="2OqNvi">
                            <ref role="37wK5l" to="1yj:499Gn2DGTs1" resolve="toDisplayString" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="Rh6nW" id="6khVixycAYZ" role="1bW2Oz">
                      <property role="TrG5h" value="it" />
                      <node concept="2jxLKc" id="6khVixycAZ0" role="1tU5fm" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3uJxvA" id="6khVixycAZ1" role="2OqNvi">
                <node concept="Xl_RD" id="6khVixycAZ2" role="3uJOhx">
                  <property role="Xl_RC" value=", " />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixycAZ3" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixycAZ4" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="toGenerationString" />
      <ref role="13i0hy" to="1yj:499Gn2DGU_t" resolve="toGenerationString" />
      <node concept="3Tm1VV" id="6khVixycAZ5" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixycAZ6" role="3clF47">
        <node concept="3clFbF" id="6khVixycAZ7" role="3cqZAp">
          <node concept="2YIFZM" id="6khVixycAZ8" role="3clFbG">
            <ref role="1Pybhc" to="wyt6:~String" resolve="String" />
            <ref role="37wK5l" to="wyt6:~String.format(java.lang.String,java.lang.Object...):java.lang.String" resolve="format" />
            <node concept="Xl_RD" id="6khVixycAZ9" role="37wK5m">
              <property role="Xl_RC" value="%s %s" />
            </node>
            <node concept="BsUDl" id="6khVixycAZa" role="37wK5m">
              <ref role="37wK5l" to="1yj:4QUW3efxqUv" resolve="getGenerationAlias" />
            </node>
            <node concept="2OqwBi" id="6khVixycAZb" role="37wK5m">
              <node concept="2OqwBi" id="6khVixycAZc" role="2Oq$k0">
                <node concept="2OqwBi" id="6khVixycAZd" role="2Oq$k0">
                  <node concept="13iPFW" id="6khVixycAZe" role="2Oq$k0" />
                  <node concept="3Tsc0h" id="6khVixycAZf" role="2OqNvi">
                    <ref role="3TtcxE" to="j0tk:6khVixycqjF" resolve="pathogensReferences" />
                  </node>
                </node>
                <node concept="3$u5V9" id="6khVixycAZg" role="2OqNvi">
                  <node concept="1bVj0M" id="6khVixycAZh" role="23t8la">
                    <node concept="3clFbS" id="6khVixycAZi" role="1bW5cS">
                      <node concept="3clFbF" id="6khVixycAZj" role="3cqZAp">
                        <node concept="2OqwBi" id="6khVixycAZk" role="3clFbG">
                          <node concept="37vLTw" id="6khVixycAZl" role="2Oq$k0">
                            <ref role="3cqZAo" node="6khVixycAZn" resolve="it" />
                          </node>
                          <node concept="2qgKlT" id="6khVixycAZm" role="2OqNvi">
                            <ref role="37wK5l" to="1yj:499Gn2DGU_t" resolve="toGenerationString" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="Rh6nW" id="6khVixycAZn" role="1bW2Oz">
                      <property role="TrG5h" value="it" />
                      <node concept="2jxLKc" id="6khVixycAZo" role="1tU5fm" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3uJxvA" id="6khVixycAZp" role="2OqNvi">
                <node concept="Xl_RD" id="6khVixycAZq" role="3uJOhx">
                  <property role="Xl_RC" value=", " />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixycAZr" role="3clF45" />
    </node>
    <node concept="13hLZK" id="6khVixycAVX" role="13h7CW">
      <node concept="3clFbS" id="6khVixycAVY" role="2VODD2" />
    </node>
  </node>
  <node concept="13h7C7" id="6khVixycP9l">
    <property role="3GE5qa" value="base.constraints.cultureFindings" />
    <ref role="13h7C2" to="j0tk:6khVixycP8V" resolve="CultureFindingsIncludePathogen" />
    <node concept="13hLZK" id="6khVixycP9m" role="13h7CW">
      <node concept="3clFbS" id="6khVixycP9n" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6khVixycP9w" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="6khVixycP9x" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixycP9A" role="3clF47">
        <node concept="3clFbF" id="6khVixycPBH" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixycPIx" role="3clFbG">
            <node concept="BsUDl" id="6khVixycPBG" role="2Oq$k0">
              <ref role="37wK5l" node="1Hxyv4EQSwS" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixycPOi" role="2OqNvi">
              <ref role="37wK5l" to="gtaw:6khVixybDEr" resolve="getCultureFindingsIncludePathogenAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixycP9B" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixycP9G" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="6khVixycP9H" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixycP9M" role="3clF47">
        <node concept="3clFbF" id="6khVixycPP5" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixycPW9" role="3clFbG">
            <node concept="BsUDl" id="6khVixycPP4" role="2Oq$k0">
              <ref role="37wK5l" node="1Hxyv4EQSwS" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixycQ22" role="2OqNvi">
              <ref role="37wK5l" to="gtaw:6khVixybDJn" resolve="getCultureFindingsIncludePathogenDescription" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixycP9N" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixycP9S" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getGenerationAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:4QUW3efxqUv" resolve="getGenerationAlias" />
      <node concept="3Tm1VV" id="6khVixycP9T" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixycP9Y" role="3clF47">
        <node concept="3clFbF" id="6khVixycQ2P" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixycQ9D" role="3clFbG">
            <node concept="BsUDl" id="6khVixycQ2O" role="2Oq$k0">
              <ref role="37wK5l" node="1Hxyv4EQSBV" resolve="getGenerationTranslations" />
            </node>
            <node concept="liA8E" id="6khVixycQfq" role="2OqNvi">
              <ref role="37wK5l" to="gtaw:6khVixybDEr" resolve="getCultureFindingsIncludePathogenAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixycP9Z" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixycPa4" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="toDisplayString" />
      <ref role="13i0hy" to="1yj:499Gn2DGTs1" resolve="toDisplayString" />
      <node concept="3Tm1VV" id="6khVixycPa5" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixycPag" role="3clF47">
        <node concept="3clFbF" id="6khVixycQgl" role="3cqZAp">
          <node concept="2YIFZM" id="6khVixycQgD" role="3clFbG">
            <ref role="37wK5l" to="wyt6:~String.format(java.lang.String,java.lang.Object...):java.lang.String" resolve="format" />
            <ref role="1Pybhc" to="wyt6:~String" resolve="String" />
            <node concept="Xl_RD" id="6khVixycQhG" role="37wK5m">
              <property role="Xl_RC" value="%s %s" />
            </node>
            <node concept="BsUDl" id="6khVixycQs5" role="37wK5m">
              <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
            </node>
            <node concept="2OqwBi" id="6khVixyIaBO" role="37wK5m">
              <node concept="2OqwBi" id="6khVixycQRD" role="2Oq$k0">
                <node concept="13iPFW" id="6khVixycQDj" role="2Oq$k0" />
                <node concept="3TrEf2" id="6khVixycRDT" role="2OqNvi">
                  <ref role="3Tt5mk" to="j0tk:6khVixycReJ" resolve="pathogenReference" />
                </node>
              </node>
              <node concept="2qgKlT" id="6khVixyIaV6" role="2OqNvi">
                <ref role="37wK5l" to="1yj:499Gn2DGTs1" resolve="toDisplayString" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixycPah" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixycPam" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="toGenerationString" />
      <ref role="13i0hy" to="1yj:499Gn2DGU_t" resolve="toGenerationString" />
      <node concept="3Tm1VV" id="6khVixycPan" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixycPay" role="3clF47">
        <node concept="3clFbF" id="6khVixycRNU" role="3cqZAp">
          <node concept="2YIFZM" id="6khVixycRNV" role="3clFbG">
            <ref role="1Pybhc" to="wyt6:~String" resolve="String" />
            <ref role="37wK5l" to="wyt6:~String.format(java.lang.String,java.lang.Object...):java.lang.String" resolve="format" />
            <node concept="Xl_RD" id="6khVixycRNW" role="37wK5m">
              <property role="Xl_RC" value="%s %s" />
            </node>
            <node concept="BsUDl" id="6khVixycYlI" role="37wK5m">
              <ref role="37wK5l" to="1yj:4QUW3efxqUv" resolve="getGenerationAlias" />
            </node>
            <node concept="2OqwBi" id="6khVixyI9kR" role="37wK5m">
              <node concept="2OqwBi" id="6khVixycRNY" role="2Oq$k0">
                <node concept="13iPFW" id="6khVixycRNZ" role="2Oq$k0" />
                <node concept="3TrEf2" id="6khVixycRO0" role="2OqNvi">
                  <ref role="3Tt5mk" to="j0tk:6khVixycReJ" resolve="pathogenReference" />
                </node>
              </node>
              <node concept="2qgKlT" id="6khVixyIa5b" role="2OqNvi">
                <ref role="37wK5l" to="1yj:499Gn2DGU_t" resolve="toGenerationString" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixycPaz" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="6khVixyiOTQ">
    <property role="3GE5qa" value="base.data.antimicrobialSusceptibility" />
    <ref role="13h7C2" to="j0tk:6khVixyiJy9" resolve="AntimicrobialSusceptibilityOfPathogenFromAnyCultureTestAndSpecimenType" />
    <node concept="13hLZK" id="6khVixyiOTR" role="13h7CW">
      <node concept="3clFbS" id="6khVixyiOTS" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6khVixyiOUb" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="6khVixyiOUc" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixyiOUh" role="3clF47">
        <node concept="3clFbF" id="6khVixyiOUm" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixyiPaR" role="3clFbG">
            <node concept="BsUDl" id="6khVixyiP44" role="2Oq$k0">
              <ref role="37wK5l" node="1Hxyv4EQSwS" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixyiPgC" role="2OqNvi">
              <ref role="37wK5l" to="gtaw:6khVixyiKpk" resolve="getAntimicrobialSusceptibilityOfPathogenFromAnyCultureTestAndSpecimenTypeAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixyiOUi" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixyiOUn" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="6khVixyiOUo" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixyiOUt" role="3clF47">
        <node concept="3clFbF" id="6khVixyiPhr" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixyiPov" role="3clFbG">
            <node concept="BsUDl" id="6khVixyiPhq" role="2Oq$k0">
              <ref role="37wK5l" node="1Hxyv4EQSwS" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixyiPuo" role="2OqNvi">
              <ref role="37wK5l" to="gtaw:6khVixyiKs1" resolve="getAntimicrobialSusceptibilityOfPathogenFromAnyCultureTestAndSpecimenTypeDescription" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixyiOUu" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixyiOUz" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getGenerationAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:4QUW3efxqUv" resolve="getGenerationAlias" />
      <node concept="3Tm1VV" id="6khVixyiOU$" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixyiOUD" role="3clF47">
        <node concept="3clFbF" id="6khVixyiPvb" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixyiPA7" role="3clFbG">
            <node concept="BsUDl" id="6khVixyiPva" role="2Oq$k0">
              <ref role="37wK5l" node="1Hxyv4EQSBV" resolve="getGenerationTranslations" />
            </node>
            <node concept="liA8E" id="6khVixyiPFS" role="2OqNvi">
              <ref role="37wK5l" to="gtaw:6khVixyiKpk" resolve="getAntimicrobialSusceptibilityOfPathogenFromAnyCultureTestAndSpecimenTypeAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixyiOUE" role="3clF45" />
    </node>
    <node concept="13i0hz" id="4B5aqq68$hv" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getDisplayKeywords" />
      <ref role="13i0hy" to="1yj:4B5aqq3ZFNV" resolve="getDisplayKeywords" />
      <node concept="3Tm1VV" id="4B5aqq68$hw" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq68$hD" role="3clF47">
        <node concept="3clFbF" id="4B5aqq68AKv" role="3cqZAp">
          <node concept="BsUDl" id="4B5aqq68AKu" role="3clFbG">
            <ref role="37wK5l" node="4B5aqq68_1k" resolve="getLocalizedKeywords" />
            <node concept="BsUDl" id="4B5aqq68ALm" role="37wK5m">
              <ref role="37wK5l" node="1Hxyv4EQSwS" resolve="getDisplayTranslations" />
            </node>
          </node>
        </node>
      </node>
      <node concept="10Q1$e" id="4B5aqq68$hE" role="3clF45">
        <node concept="17QB3L" id="4B5aqq68$hF" role="10Q1$1" />
      </node>
    </node>
    <node concept="13i0hz" id="4B5aqq68$hK" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getGenerationKeywords" />
      <ref role="13i0hy" to="1yj:4B5aqq3ZFQj" resolve="getGenerationKeywords" />
      <node concept="3Tm1VV" id="4B5aqq68$hL" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq68$hU" role="3clF47">
        <node concept="3clFbF" id="4B5aqq68$i0" role="3cqZAp">
          <node concept="BsUDl" id="4B5aqq68AIF" role="3clFbG">
            <ref role="37wK5l" node="4B5aqq68_1k" resolve="getLocalizedKeywords" />
            <node concept="BsUDl" id="4B5aqq68AJp" role="37wK5m">
              <ref role="37wK5l" node="1Hxyv4EQSBV" resolve="getGenerationTranslations" />
            </node>
          </node>
        </node>
      </node>
      <node concept="10Q1$e" id="4B5aqq68$hV" role="3clF45">
        <node concept="17QB3L" id="4B5aqq68$hW" role="10Q1$1" />
      </node>
    </node>
    <node concept="13i0hz" id="4B5aqq68_1k" role="13h7CS">
      <property role="TrG5h" value="getLocalizedKeywords" />
      <property role="2Ki8OM" value="true" />
      <node concept="3Tm6S6" id="4B5aqq69o8Z" role="1B3o_S" />
      <node concept="10Q1$e" id="4B5aqq68_y8" role="3clF45">
        <node concept="17QB3L" id="4B5aqq68_xW" role="10Q1$1" />
      </node>
      <node concept="3clFbS" id="4B5aqq68_1n" role="3clF47">
        <node concept="3clFbF" id="4B5aqq68A7g" role="3cqZAp">
          <node concept="2ShNRf" id="4B5aqq68A7c" role="3clFbG">
            <node concept="3g6Rrh" id="4B5aqq68AmA" role="2ShVmc">
              <node concept="17QB3L" id="4B5aqq68Aeb" role="3g7fb8" />
              <node concept="BsUDl" id="2FjKBCQSc6J" role="3g7hyw">
                <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
              </node>
              <node concept="2OqwBi" id="2FjKBCQSclX" role="3g7hyw">
                <node concept="37vLTw" id="2FjKBCQSceL" role="2Oq$k0">
                  <ref role="3cqZAo" node="4B5aqq68_yu" resolve="translations" />
                </node>
                <node concept="liA8E" id="2FjKBCQScyG" role="2OqNvi">
                  <ref role="37wK5l" to="gtaw:6khVixyGIrk" resolve="getAntimicrobialSusceptibilityOfPathogenFromAnyCultureTestAndSpecimenTypeForAntimicrobialKeyword" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="4B5aqq68_yu" role="3clF46">
        <property role="TrG5h" value="translations" />
        <node concept="3uibUv" id="4B5aqq68_yt" role="1tU5fm">
          <ref role="3uigEE" to="gtaw:4zMac8rUNtP" resolve="IMicrobiologyDataTranslations" />
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="6khVixyiOUJ" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="toDisplayString" />
      <ref role="13i0hy" to="1yj:499Gn2DGTs1" resolve="toDisplayString" />
      <node concept="3Tm1VV" id="6khVixyiOUK" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixyiOUV" role="3clF47">
        <node concept="3clFbF" id="4B5aqq697gP" role="3cqZAp">
          <node concept="BsUDl" id="4B5aqq697gQ" role="3clFbG">
            <ref role="37wK5l" node="4B5aqq68LCW" resolve="toLocalizedString" />
            <node concept="BsUDl" id="4B5aqq697gR" role="37wK5m">
              <ref role="37wK5l" to="1yj:4B5aqq3ZFQj" resolve="getGenerationKeywords" />
            </node>
            <node concept="2OqwBi" id="4B5aqq697gX" role="37wK5m">
              <node concept="2OqwBi" id="4B5aqq697gY" role="2Oq$k0">
                <node concept="13iPFW" id="4B5aqq697gZ" role="2Oq$k0" />
                <node concept="3TrEf2" id="4B5aqq697h0" role="2OqNvi">
                  <ref role="3Tt5mk" to="j0tk:6khVixyiJya" resolve="pathogenReference" />
                </node>
              </node>
              <node concept="2qgKlT" id="4B5aqq699no" role="2OqNvi">
                <ref role="37wK5l" to="1yj:499Gn2DGTs1" resolve="toDisplayString" />
              </node>
            </node>
            <node concept="2OqwBi" id="4B5aqq697gS" role="37wK5m">
              <node concept="2OqwBi" id="4B5aqq697gT" role="2Oq$k0">
                <node concept="13iPFW" id="4B5aqq697gU" role="2Oq$k0" />
                <node concept="3TrEf2" id="4B5aqq697gV" role="2OqNvi">
                  <ref role="3Tt5mk" to="j0tk:6khVixyGI0c" resolve="antimicrobialReference" />
                </node>
              </node>
              <node concept="2qgKlT" id="4B5aqq6983c" role="2OqNvi">
                <ref role="37wK5l" to="1yj:499Gn2DGTs1" resolve="toDisplayString" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixyiOUW" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixyiOV1" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="toGenerationString" />
      <ref role="13i0hy" to="1yj:499Gn2DGU_t" resolve="toGenerationString" />
      <node concept="3Tm1VV" id="6khVixyiOV2" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixyiOVd" role="3clF47">
        <node concept="3clFbF" id="4B5aqq68X0h" role="3cqZAp">
          <node concept="BsUDl" id="4B5aqq68X0f" role="3clFbG">
            <ref role="37wK5l" node="4B5aqq68LCW" resolve="toLocalizedString" />
            <node concept="BsUDl" id="4B5aqq691hR" role="37wK5m">
              <ref role="37wK5l" to="1yj:4B5aqq3ZFQj" resolve="getGenerationKeywords" />
            </node>
            <node concept="2OqwBi" id="4B5aqq68ZWg" role="37wK5m">
              <node concept="2OqwBi" id="4B5aqq68Z4s" role="2Oq$k0">
                <node concept="13iPFW" id="4B5aqq68YPb" role="2Oq$k0" />
                <node concept="3TrEf2" id="4B5aqq68Zu9" role="2OqNvi">
                  <ref role="3Tt5mk" to="j0tk:6khVixyiJya" resolve="pathogenReference" />
                </node>
              </node>
              <node concept="2qgKlT" id="4B5aqq690nc" role="2OqNvi">
                <ref role="37wK5l" to="1yj:499Gn2DGU_t" resolve="toGenerationString" />
              </node>
            </node>
            <node concept="2OqwBi" id="4B5aqq68Yie" role="37wK5m">
              <node concept="2OqwBi" id="4B5aqq68XxW" role="2Oq$k0">
                <node concept="13iPFW" id="4B5aqq68XiY" role="2Oq$k0" />
                <node concept="3TrEf2" id="4B5aqq68XOl" role="2OqNvi">
                  <ref role="3Tt5mk" to="j0tk:6khVixyGI0c" resolve="antimicrobialReference" />
                </node>
              </node>
              <node concept="2qgKlT" id="4B5aqq690P3" role="2OqNvi">
                <ref role="37wK5l" to="1yj:499Gn2DGU_t" resolve="toGenerationString" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixyiOVe" role="3clF45" />
    </node>
    <node concept="13i0hz" id="4B5aqq68LCW" role="13h7CS">
      <property role="TrG5h" value="toLocalizedString" />
      <node concept="3Tm1VV" id="4B5aqq68LCX" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq68McF" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq68LCZ" role="3clF47">
        <node concept="3clFbF" id="4B5aqq68MiI" role="3cqZAp">
          <node concept="2YIFZM" id="4B5aqq68MiJ" role="3clFbG">
            <ref role="37wK5l" to="wyt6:~String.format(java.lang.String,java.lang.Object...):java.lang.String" resolve="format" />
            <ref role="1Pybhc" to="wyt6:~String" resolve="String" />
            <node concept="Xl_RD" id="4B5aqq68MiK" role="37wK5m">
              <property role="Xl_RC" value="%s %s %s %s" />
            </node>
            <node concept="AH0OO" id="4B5aqq68MiL" role="37wK5m">
              <node concept="3cmrfG" id="4B5aqq68MiM" role="AHEQo">
                <property role="3cmrfH" value="0" />
              </node>
              <node concept="37vLTw" id="4B5aqq68QDm" role="AHHXb">
                <ref role="3cqZAo" node="4B5aqq68NRS" resolve="keywords" />
              </node>
            </node>
            <node concept="37vLTw" id="2FjKBCQROrZ" role="37wK5m">
              <ref role="3cqZAo" node="4B5aqq68ONF" resolve="pathogenReference" />
            </node>
            <node concept="AH0OO" id="4B5aqq68MiT" role="37wK5m">
              <node concept="3cmrfG" id="4B5aqq68MiU" role="AHEQo">
                <property role="3cmrfH" value="1" />
              </node>
              <node concept="37vLTw" id="4B5aqq68Raw" role="AHHXb">
                <ref role="3cqZAo" node="4B5aqq68NRS" resolve="keywords" />
              </node>
            </node>
            <node concept="37vLTw" id="2FjKBCQROEU" role="37wK5m">
              <ref role="3cqZAo" node="4B5aqq68OlH" resolve="antimicrobialReference" />
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="4B5aqq68NRS" role="3clF46">
        <property role="TrG5h" value="keywords" />
        <node concept="10Q1$e" id="4B5aqq68Ole" role="1tU5fm">
          <node concept="17QB3L" id="4B5aqq68Ol0" role="10Q1$1" />
        </node>
      </node>
      <node concept="37vLTG" id="4B5aqq68ONF" role="3clF46">
        <property role="TrG5h" value="pathogenReference" />
        <node concept="17QB3L" id="4B5aqq68PgI" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="4B5aqq68OlH" role="3clF46">
        <property role="TrG5h" value="antimicrobialReference" />
        <node concept="17QB3L" id="4B5aqq68OMG" role="1tU5fm" />
      </node>
    </node>
  </node>
  <node concept="13h7C7" id="6khVixyj$xl">
    <property role="3GE5qa" value="base.constraints.antimicrobialSensitivity" />
    <ref role="13h7C2" to="j0tk:6khVixyj$wV" resolve="AntimicrobialSensitivityConstraint" />
    <node concept="13hLZK" id="6khVixyj$xm" role="13h7CW">
      <node concept="3clFbS" id="6khVixyj$xn" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6khVixyj$xw" role="13h7CS">
      <property role="TrG5h" value="isAllowedToConstrain" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="13i0iv" value="false" />
      <ref role="13i0hy" to="wb6c:1mAGFBKNuSW" resolve="isAllowedToConstrain" />
      <node concept="3Tm1VV" id="6khVixyj$xx" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixyj$xC" role="3clF47">
        <node concept="3clFbF" id="6khVixyj$_E" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixyj$OP" role="3clFbG">
            <node concept="37vLTw" id="6khVixyj$_D" role="2Oq$k0">
              <ref role="3cqZAo" node="6khVixyj$xD" resolve="concept" />
            </node>
            <node concept="2Zo12i" id="6khVixyj_3Z" role="2OqNvi">
              <node concept="chp4Y" id="6khVixyj_aT" role="2Zo12j">
                <ref role="cht4Q" to="j0tk:6khVixyiJSf" resolve="AntimicrobialSusceptibilityData" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="6khVixyj$xD" role="3clF46">
        <property role="TrG5h" value="concept" />
        <node concept="3bZ5Sz" id="6khVixyj$xE" role="1tU5fm">
          <ref role="3bZ5Sy" to="7f9y:1mAGFBKqQrZ" resolve="DataValue" />
        </node>
      </node>
      <node concept="10P_77" id="6khVixyj$xF" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="6khVixyjKWk">
    <property role="3GE5qa" value="base.constraints.antimicrobialSensitivity" />
    <ref role="13h7C2" to="j0tk:6khVixyjFdS" resolve="AntimicrobialSensitivityEqualTo" />
    <node concept="13hLZK" id="6khVixyjKWl" role="13h7CW">
      <node concept="3clFbS" id="6khVixyjKWm" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6khVixyjKWD" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="6khVixyjKWE" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixyjKWJ" role="3clF47">
        <node concept="3clFbF" id="6khVixyjLa_" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixyjLhx" role="3clFbG">
            <node concept="BsUDl" id="6khVixyjLa$" role="2Oq$k0">
              <ref role="37wK5l" node="1Hxyv4EQSwS" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixyjLni" role="2OqNvi">
              <ref role="37wK5l" to="gtaw:6khVixyjFOn" resolve="getAntimicrobialSusceptibilityEqualToAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixyjKWK" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixyjKWP" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="6khVixyjKWQ" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixyjKWV" role="3clF47">
        <node concept="3clFbF" id="6khVixyjKX0" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixyjLs4" role="3clFbG">
            <node concept="BsUDl" id="6khVixyjLs5" role="2Oq$k0">
              <ref role="37wK5l" node="1Hxyv4EQSwS" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixyjLs6" role="2OqNvi">
              <ref role="37wK5l" to="gtaw:6khVixyjFSl" resolve="getAntimicrobialSusceptibilityEqualToDescription" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixyjKWW" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixyjKX1" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getGenerationAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:4QUW3efxqUv" resolve="getGenerationAlias" />
      <node concept="3Tm1VV" id="6khVixyjKX2" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixyjKX7" role="3clF47">
        <node concept="3clFbF" id="6khVixyjKXc" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixyjLtW" role="3clFbG">
            <node concept="BsUDl" id="6khVixyjLuW" role="2Oq$k0">
              <ref role="37wK5l" node="1Hxyv4EQSBV" resolve="getGenerationTranslations" />
            </node>
            <node concept="liA8E" id="6khVixyjLtY" role="2OqNvi">
              <ref role="37wK5l" to="gtaw:6khVixyjFOn" resolve="getAntimicrobialSusceptibilityEqualToAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixyjKX8" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixyjKXd" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="toDisplayString" />
      <ref role="13i0hy" to="1yj:499Gn2DGTs1" resolve="toDisplayString" />
      <node concept="3Tm1VV" id="6khVixyjKXe" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixyjKXp" role="3clF47">
        <node concept="3clFbF" id="6khVixyjLvN" role="3cqZAp">
          <node concept="2YIFZM" id="6khVixyjLw9" role="3clFbG">
            <ref role="37wK5l" to="wyt6:~String.format(java.lang.String,java.lang.Object...):java.lang.String" resolve="format" />
            <ref role="1Pybhc" to="wyt6:~String" resolve="String" />
            <node concept="Xl_RD" id="6khVixyjLxc" role="37wK5m">
              <property role="Xl_RC" value="%s %s" />
            </node>
            <node concept="BsUDl" id="6khVixyjLDn" role="37wK5m">
              <ref role="37wK5l" node="1Hxyv4EQSwS" resolve="getDisplayTranslations" />
            </node>
            <node concept="2OqwBi" id="6khVixyjZf3" role="37wK5m">
              <node concept="2OqwBi" id="6khVixyjM27" role="2Oq$k0">
                <node concept="13iPFW" id="6khVixyjLNT" role="2Oq$k0" />
                <node concept="3TrEf2" id="6khVixyjYIu" role="2OqNvi">
                  <ref role="3Tt5mk" to="j0tk:6khVixyjNBG" resolve="sensitivity" />
                </node>
              </node>
              <node concept="2qgKlT" id="6khVixyjZT9" role="2OqNvi">
                <ref role="37wK5l" to="1yj:499Gn2DGTs1" resolve="toDisplayString" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixyjKXq" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixyjKXv" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="toGenerationString" />
      <ref role="13i0hy" to="1yj:499Gn2DGU_t" resolve="toGenerationString" />
      <node concept="3Tm1VV" id="6khVixyjKXw" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixyjKXF" role="3clF47">
        <node concept="3clFbF" id="6khVixyjKXK" role="3cqZAp">
          <node concept="2YIFZM" id="6khVixyk060" role="3clFbG">
            <ref role="37wK5l" to="wyt6:~String.format(java.lang.String,java.lang.Object...):java.lang.String" resolve="format" />
            <ref role="1Pybhc" to="wyt6:~String" resolve="String" />
            <node concept="Xl_RD" id="6khVixyk061" role="37wK5m">
              <property role="Xl_RC" value="%s %s" />
            </node>
            <node concept="BsUDl" id="6khVixyk0s_" role="37wK5m">
              <ref role="37wK5l" to="1yj:4QUW3efxqUv" resolve="getGenerationAlias" />
            </node>
            <node concept="2OqwBi" id="6khVixyk063" role="37wK5m">
              <node concept="2OqwBi" id="6khVixyk064" role="2Oq$k0">
                <node concept="13iPFW" id="6khVixyk065" role="2Oq$k0" />
                <node concept="3TrEf2" id="6khVixyk066" role="2OqNvi">
                  <ref role="3Tt5mk" to="j0tk:6khVixyjNBG" resolve="sensitivity" />
                </node>
              </node>
              <node concept="2qgKlT" id="6khVixyk0PS" role="2OqNvi">
                <ref role="37wK5l" to="1yj:499Gn2DGU_t" resolve="toGenerationString" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixyjKXG" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="6khVixykiR0">
    <property role="3GE5qa" value="base.literals" />
    <ref role="13h7C2" to="j0tk:6khVixyjMoL" resolve="IntermediateAntimicrobialSensitivity" />
    <node concept="13hLZK" id="6khVixykiR1" role="13h7CW">
      <node concept="3clFbS" id="6khVixykiR2" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6khVixykiRn" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="6khVixykiRo" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixykiRt" role="3clF47">
        <node concept="3clFbF" id="6khVixykj1b" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixykj7Z" role="3clFbG">
            <node concept="BsUDl" id="6khVixykj1a" role="2Oq$k0">
              <ref role="37wK5l" node="1Hxyv4EQSwS" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixykjdK" role="2OqNvi">
              <ref role="37wK5l" to="gtaw:6khVixyjMON" resolve="getIntermediateAntimicrobialSusceptibilityAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixykiRu" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixykiRz" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="6khVixykiR$" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixykiRD" role="3clF47">
        <node concept="3clFbF" id="6khVixykjeq" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixykjes" role="3clFbG">
            <node concept="BsUDl" id="6khVixykjet" role="2Oq$k0">
              <ref role="37wK5l" node="1Hxyv4EQSwS" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixykjeu" role="2OqNvi">
              <ref role="37wK5l" to="gtaw:6khVixyjN6p" resolve="getIntermediateAntimicrobialSusceptibilityDescription" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixykiRE" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixykiRJ" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getGenerationAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:4QUW3efxqUv" resolve="getGenerationAlias" />
      <node concept="3Tm1VV" id="6khVixykiRK" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixykiRP" role="3clF47">
        <node concept="3clFbF" id="6khVixykjgd" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixykjgf" role="3clFbG">
            <node concept="BsUDl" id="6khVixykjhg" role="2Oq$k0">
              <ref role="37wK5l" node="1Hxyv4EQSBV" resolve="getGenerationTranslations" />
            </node>
            <node concept="liA8E" id="6khVixykjgh" role="2OqNvi">
              <ref role="37wK5l" to="gtaw:6khVixyjMON" resolve="getIntermediateAntimicrobialSusceptibilityAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixykiRQ" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="6khVixykjiJ">
    <property role="3GE5qa" value="base.literals" />
    <ref role="13h7C2" to="j0tk:6khVixyjMoI" resolve="AntimicrobialSensitivityLiteral" />
    <node concept="13hLZK" id="6khVixykjiK" role="13h7CW">
      <node concept="3clFbS" id="6khVixykjiL" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6khVixykjiU" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="toDisplayString" />
      <ref role="13i0hy" to="1yj:499Gn2DGTs1" resolve="toDisplayString" />
      <node concept="3Tm1VV" id="6khVixykjiV" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixykjj6" role="3clF47">
        <node concept="3clFbF" id="6khVixykjpH" role="3cqZAp">
          <node concept="BsUDl" id="6khVixykjpG" role="3clFbG">
            <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixykjj7" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixykjjc" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="toGenerationString" />
      <ref role="13i0hy" to="1yj:499Gn2DGU_t" resolve="toGenerationString" />
      <node concept="3Tm1VV" id="6khVixykjjd" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixykjjo" role="3clF47">
        <node concept="3clFbF" id="6khVixykjjt" role="3cqZAp">
          <node concept="BsUDl" id="6khVixykjqe" role="3clFbG">
            <ref role="37wK5l" to="1yj:4QUW3efxqUv" resolve="getGenerationAlias" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixykjjp" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="6khVixykjrF">
    <property role="3GE5qa" value="base.literals" />
    <ref role="13h7C2" to="j0tk:6khVixyjMoJ" resolve="ResistantAntimicrobialSensitivity" />
    <node concept="13hLZK" id="6khVixykjrG" role="13h7CW">
      <node concept="3clFbS" id="6khVixykjrH" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6khVixykjrQ" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="6khVixykjrR" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixykjrW" role="3clF47">
        <node concept="3clFbF" id="6khVixykj$Q" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixykjFE" role="3clFbG">
            <node concept="BsUDl" id="6khVixykj$P" role="2Oq$k0">
              <ref role="37wK5l" node="1Hxyv4EQSwS" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixykjLr" role="2OqNvi">
              <ref role="37wK5l" to="gtaw:6khVixyjNgP" resolve="getResistantAntimicrobialSusceptibilityAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixykjrX" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixykjs2" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="6khVixykjs3" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixykjs8" role="3clF47">
        <node concept="3clFbF" id="6khVixykjMe" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixykjTi" role="3clFbG">
            <node concept="BsUDl" id="6khVixykjMd" role="2Oq$k0">
              <ref role="37wK5l" node="1Hxyv4EQSwS" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixykjZb" role="2OqNvi">
              <ref role="37wK5l" to="gtaw:6khVixyjNyz" resolve="getResistantAntimicrobialSusceptibilityDescription" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixykjs9" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixykjse" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getGenerationAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:4QUW3efxqUv" resolve="getGenerationAlias" />
      <node concept="3Tm1VV" id="6khVixykjsf" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixykjsk" role="3clF47">
        <node concept="3clFbF" id="6khVixykjZY" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixykk6M" role="3clFbG">
            <node concept="BsUDl" id="6khVixykjZX" role="2Oq$k0">
              <ref role="37wK5l" node="1Hxyv4EQSBV" resolve="getGenerationTranslations" />
            </node>
            <node concept="liA8E" id="6khVixykkcz" role="2OqNvi">
              <ref role="37wK5l" to="gtaw:6khVixyjNgP" resolve="getResistantAntimicrobialSusceptibilityAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixykjsl" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="6khVixykkdQ">
    <property role="3GE5qa" value="base.literals" />
    <ref role="13h7C2" to="j0tk:6khVixyjMoK" resolve="SensitiveAntimicrobialSensitivity" />
    <node concept="13hLZK" id="6khVixykkdR" role="13h7CW">
      <node concept="3clFbS" id="6khVixykkdS" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6khVixykke1" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="6khVixykke2" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixykke7" role="3clF47">
        <node concept="3clFbF" id="6khVixykkn1" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixykktX" role="3clFbG">
            <node concept="BsUDl" id="6khVixykkn0" role="2Oq$k0">
              <ref role="37wK5l" node="1Hxyv4EQSwS" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixykkzI" role="2OqNvi">
              <ref role="37wK5l" to="gtaw:6khVixyjMzo" resolve="getSensitiveAntimicrobialSusceptibilityAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixykke8" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixykked" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="6khVixykkee" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixykkej" role="3clF47">
        <node concept="3clFbF" id="6khVixykk$x" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixykkF_" role="3clFbG">
            <node concept="BsUDl" id="6khVixykk$w" role="2Oq$k0">
              <ref role="37wK5l" node="1Hxyv4EQSwS" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixykkLu" role="2OqNvi">
              <ref role="37wK5l" to="gtaw:6khVixyjMBR" resolve="getSensitiveAntimicrobialSusceptibilityDescription" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixykkek" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixykkep" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getGenerationAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:4QUW3efxqUv" resolve="getGenerationAlias" />
      <node concept="3Tm1VV" id="6khVixykkeq" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixykkev" role="3clF47">
        <node concept="3clFbF" id="6khVixykkMh" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixykkT5" role="3clFbG">
            <node concept="BsUDl" id="6khVixykkMg" role="2Oq$k0">
              <ref role="37wK5l" node="1Hxyv4EQSBV" resolve="getGenerationTranslations" />
            </node>
            <node concept="liA8E" id="6khVixykkYQ" role="2OqNvi">
              <ref role="37wK5l" to="gtaw:6khVixyjMzo" resolve="getSensitiveAntimicrobialSusceptibilityAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixykkew" role="3clF45" />
    </node>
  </node>
</model>

