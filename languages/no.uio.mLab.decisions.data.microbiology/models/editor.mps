<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:051276b6-4500-4c71-bf34-f01e6d2f16c4(no.uio.mLab.decisions.data.microbiology.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="11" />
    <use id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions" version="4" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="j0tk" ref="r:2895f9f0-ca08-4015-b25a-0b35f51e5738(no.uio.mLab.decisions.data.microbiology.structure)" implicit="true" />
    <import index="1yj" ref="r:0c887d67-81ab-420f-9a35-a9f6bf96c975(no.uio.mLab.shared.behavior)" implicit="true" />
    <import index="duqf" ref="r:2c3800ab-cd1f-48ee-8876-69529185b5c0(no.uio.mLab.decisions.data.microbiology.behavior)" implicit="true" />
    <import index="uubs" ref="r:481ae0fe-9100-4bb8-9488-731d6cfeba53(no.uio.mLab.shared.editor)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="2000375450116454183" name="jetbrains.mps.lang.editor.structure.ISubstituteMenu" flags="ng" index="22mbnS">
        <child id="414384289274416996" name="parts" index="3ft7WO" />
      </concept>
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi" />
      <concept id="1140524381322" name="jetbrains.mps.lang.editor.structure.CellModel_ListWithRole" flags="ng" index="2czfm3">
        <property id="1140524450557" name="separatorText" index="2czwfO" />
        <child id="1140524464360" name="cellLayout" index="2czzBx" />
      </concept>
      <concept id="1106270549637" name="jetbrains.mps.lang.editor.structure.CellLayout_Horizontal" flags="nn" index="2iRfu4" />
      <concept id="1142886221719" name="jetbrains.mps.lang.editor.structure.QueryFunction_NodeCondition" flags="in" index="pkWqt" />
      <concept id="1142886811589" name="jetbrains.mps.lang.editor.structure.ConceptFunctionParameter_node" flags="nn" index="pncrf" />
      <concept id="1177327570013" name="jetbrains.mps.lang.editor.structure.QueryFunction_SubstituteMenu_Substitute" flags="in" index="ucgPf" />
      <concept id="8478191136883534237" name="jetbrains.mps.lang.editor.structure.IExtensibleSubstituteMenuPart" flags="ng" index="upBLQ">
        <child id="8478191136883534238" name="features" index="upBLP" />
      </concept>
      <concept id="1177335944525" name="jetbrains.mps.lang.editor.structure.QueryFunction_SubstituteMenu_SubstituteString" flags="in" index="uGdhv" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="8383079901754291618" name="jetbrains.mps.lang.editor.structure.CellModel_NextEditor" flags="ng" index="B$lHz" />
      <concept id="1078939183254" name="jetbrains.mps.lang.editor.structure.CellModel_Component" flags="sg" stub="3162947552742194261" index="PMmxH">
        <reference id="1078939183255" name="editorComponent" index="PMmxG" />
      </concept>
      <concept id="1630016958697344083" name="jetbrains.mps.lang.editor.structure.IMenu_Concept" flags="ng" index="2ZABuq">
        <reference id="6591946374543067572" name="conceptDeclaration" index="aqKnT" />
      </concept>
      <concept id="8998492695583125082" name="jetbrains.mps.lang.editor.structure.SubstituteFeature_MatchingText" flags="ng" index="16NfWO">
        <child id="8998492695583129244" name="query" index="16NeZM" />
      </concept>
      <concept id="8998492695583129971" name="jetbrains.mps.lang.editor.structure.SubstituteFeature_DescriptionText" flags="ng" index="16NL0t">
        <child id="8998492695583129972" name="query" index="16NL0q" />
      </concept>
      <concept id="7342352913006985483" name="jetbrains.mps.lang.editor.structure.SubstituteMenuPart_Action" flags="ng" index="3eGOop">
        <child id="8612453216082699922" name="substituteHandler" index="3aKz83" />
      </concept>
      <concept id="1381004262292414836" name="jetbrains.mps.lang.editor.structure.ICellStyle" flags="ng" index="1k5N5V">
        <reference id="1381004262292426837" name="parentStyleClass" index="1k5W1q" />
      </concept>
      <concept id="3308396621974580100" name="jetbrains.mps.lang.editor.structure.SubstituteMenu_Default" flags="ng" index="3p36aQ" />
      <concept id="5425882385312046132" name="jetbrains.mps.lang.editor.structure.QueryFunctionParameter_SubstituteMenu_CurrentTargetNode" flags="nn" index="1yR$tW" />
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <property id="1139852716018" name="noTargetText" index="1$x2rV" />
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389214265" name="jetbrains.mps.lang.editor.structure.EditorCellModel" flags="ng" index="3EYTF0">
        <child id="1142887637401" name="renderingCondition" index="pqm2j" />
      </concept>
      <concept id="1073389446423" name="jetbrains.mps.lang.editor.structure.CellModel_Collection" flags="sn" stub="3013115976261988961" index="3EZMnI">
        <child id="1106270802874" name="cellLayout" index="2iSdaV" />
        <child id="1073389446424" name="childCellModel" index="3EZMnx" />
      </concept>
      <concept id="1073389577006" name="jetbrains.mps.lang.editor.structure.CellModel_Constant" flags="sn" stub="3610246225209162225" index="3F0ifn">
        <property id="1073389577007" name="text" index="3F0ifm" />
      </concept>
      <concept id="1073389882823" name="jetbrains.mps.lang.editor.structure.CellModel_RefNode" flags="sg" stub="730538219795960754" index="3F1sOY" />
      <concept id="1073390211982" name="jetbrains.mps.lang.editor.structure.CellModel_RefNodeList" flags="sg" stub="2794558372793454595" index="3F2HdR" />
      <concept id="1225898583838" name="jetbrains.mps.lang.editor.structure.ReadOnlyModelAccessor" flags="ng" index="1HfYo3">
        <child id="1225898971709" name="getter" index="1Hhtcw" />
      </concept>
      <concept id="1225900081164" name="jetbrains.mps.lang.editor.structure.CellModel_ReadOnlyModelAccessor" flags="sg" stub="3708815482283559694" index="1HlG4h">
        <child id="1225900141900" name="modelAccessor" index="1HlULh" />
      </concept>
      <concept id="1176717841777" name="jetbrains.mps.lang.editor.structure.QueryFunction_ModelAccess_Getter" flags="in" index="3TQlhw" />
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1173175405605" name="jetbrains.mps.baseLanguage.structure.ArrayAccessExpression" flags="nn" index="AH0OO">
        <child id="1173175577737" name="index" index="AHEQo" />
        <child id="1173175590490" name="array" index="AHHXb" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
      </concept>
    </language>
    <language id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions">
      <concept id="5979988948250981289" name="jetbrains.mps.lang.actions.structure.SNodeCreatorAndInitializer" flags="nn" index="2fJWfE">
        <child id="3757480014665187678" name="prototype" index="1wAG5O" />
      </concept>
      <concept id="767145758118872830" name="jetbrains.mps.lang.actions.structure.NF_Link_SetNewChildOperation" flags="nn" index="2DeJnY" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="7453996997717780434" name="jetbrains.mps.lang.smodel.structure.Node_GetSConceptOperation" flags="nn" index="2yIwOk" />
      <concept id="2644386474300074836" name="jetbrains.mps.lang.smodel.structure.ConceptIdRefExpression" flags="nn" index="35c_gC">
        <reference id="2644386474300074837" name="conceptDeclaration" index="35c_gD" />
      </concept>
      <concept id="1172008320231" name="jetbrains.mps.lang.smodel.structure.Node_IsNotNullOperation" flags="nn" index="3x8VRR" />
      <concept id="1180636770613" name="jetbrains.mps.lang.smodel.structure.SNodeCreator" flags="nn" index="3zrR0B">
        <child id="1180636770616" name="createdType" index="3zrR0E" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="24kQdi" id="6khVixyaFJN">
    <property role="3GE5qa" value="base.data.cultureFindings" />
    <ref role="1XX52x" to="j0tk:6khVixyaF5m" resolve="FindingsFromAnyCultureTest" />
    <node concept="3EZMnI" id="6khVixyaFJP" role="2wV5jI">
      <node concept="2iRfu4" id="6khVixyaFJQ" role="2iSdaV" />
      <node concept="B$lHz" id="6khVixyaFJV" role="3EZMnx" />
      <node concept="3F1sOY" id="6khVixybjWn" role="3EZMnx">
        <ref role="1NtTu8" to="j0tk:6khVixybjNf" resolve="specimenTypeReference" />
        <node concept="pkWqt" id="2FjKBCR2KJB" role="pqm2j">
          <node concept="3clFbS" id="2FjKBCR2KJC" role="2VODD2">
            <node concept="3clFbF" id="2FjKBCR2KQV" role="3cqZAp">
              <node concept="2OqwBi" id="2FjKBCR2Mrh" role="3clFbG">
                <node concept="2OqwBi" id="2FjKBCR2L9K" role="2Oq$k0">
                  <node concept="pncrf" id="2FjKBCR2KQU" role="2Oq$k0" />
                  <node concept="3TrEf2" id="2FjKBCR2LFh" role="2OqNvi">
                    <ref role="3Tt5mk" to="j0tk:6khVixybjNf" resolve="specimenTypeReference" />
                  </node>
                </node>
                <node concept="3x8VRR" id="2FjKBCR2N2w" role="2OqNvi" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="6khVixyaUEp">
    <property role="3GE5qa" value="base.data.cultureFindings" />
    <ref role="aqKnT" to="j0tk:6khVixyaF5m" resolve="FindingsFromAnyCultureTest" />
    <node concept="3eGOop" id="6khVixyaUEq" role="3ft7WO">
      <node concept="ucgPf" id="6khVixyaUEr" role="3aKz83">
        <node concept="3clFbS" id="6khVixyaUEs" role="2VODD2">
          <node concept="3clFbF" id="6khVixyaUJT" role="3cqZAp">
            <node concept="2ShNRf" id="6khVixyaUJR" role="3clFbG">
              <node concept="2fJWfE" id="6khVixyaUSv" role="2ShVmc">
                <node concept="3Tqbb2" id="6khVixyaUSx" role="3zrR0E">
                  <ref role="ehGHo" to="j0tk:6khVixyaF5m" resolve="FindingsFromAnyCultureTest" />
                </node>
                <node concept="1yR$tW" id="6khVixyaV6a" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="6khVixyaVbN" role="upBLP">
        <node concept="uGdhv" id="6khVixyaVhu" role="16NeZM">
          <node concept="3clFbS" id="6khVixyaVhw" role="2VODD2">
            <node concept="3clFbF" id="6khVixyaVq6" role="3cqZAp">
              <node concept="2OqwBi" id="6khVixyaVXz" role="3clFbG">
                <node concept="35c_gC" id="6khVixyaVq5" role="2Oq$k0">
                  <ref role="35c_gD" to="j0tk:6khVixyaF5m" resolve="FindingsFromAnyCultureTest" />
                </node>
                <node concept="2qgKlT" id="6khVixyaWvs" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="6khVixyaWZg" role="upBLP">
        <node concept="uGdhv" id="6khVixyaX58" role="16NL0q">
          <node concept="3clFbS" id="6khVixyaX5a" role="2VODD2">
            <node concept="3clFbF" id="6khVixyaXdK" role="3cqZAp">
              <node concept="2OqwBi" id="6khVixyaXSh" role="3clFbG">
                <node concept="35c_gC" id="6khVixyaXdJ" role="2Oq$k0">
                  <ref role="35c_gD" to="j0tk:6khVixyaF5m" resolve="FindingsFromAnyCultureTest" />
                </node>
                <node concept="2qgKlT" id="6khVixyaYqa" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3eGOop" id="2FjKBCR2Ssh" role="3ft7WO">
      <node concept="ucgPf" id="2FjKBCR2Ssi" role="3aKz83">
        <node concept="3clFbS" id="2FjKBCR2Ssj" role="2VODD2">
          <node concept="3cpWs8" id="2FjKBCR2UAj" role="3cqZAp">
            <node concept="3cpWsn" id="2FjKBCR2UAm" role="3cpWs9">
              <property role="TrG5h" value="node" />
              <node concept="3Tqbb2" id="2FjKBCR2UAi" role="1tU5fm">
                <ref role="ehGHo" to="j0tk:6khVixyaF5m" resolve="FindingsFromAnyCultureTest" />
              </node>
              <node concept="2ShNRf" id="2FjKBCR2Ssl" role="33vP2m">
                <node concept="2fJWfE" id="2FjKBCR2Ssm" role="2ShVmc">
                  <node concept="3Tqbb2" id="2FjKBCR2Ssn" role="3zrR0E">
                    <ref role="ehGHo" to="j0tk:6khVixyaF5m" resolve="FindingsFromAnyCultureTest" />
                  </node>
                  <node concept="1yR$tW" id="2FjKBCR2Sso" role="1wAG5O" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="2FjKBCR2V3_" role="3cqZAp">
            <node concept="2OqwBi" id="2FjKBCR2WqC" role="3clFbG">
              <node concept="2OqwBi" id="2FjKBCR2VlJ" role="2Oq$k0">
                <node concept="37vLTw" id="2FjKBCR2V3z" role="2Oq$k0">
                  <ref role="3cqZAo" node="2FjKBCR2UAm" resolve="node" />
                </node>
                <node concept="3TrEf2" id="2FjKBCR2VM0" role="2OqNvi">
                  <ref role="3Tt5mk" to="j0tk:6khVixybjNf" resolve="specimenTypeReference" />
                </node>
              </node>
              <node concept="2DeJnY" id="2FjKBCR2WWy" role="2OqNvi" />
            </node>
          </node>
          <node concept="3clFbF" id="2FjKBCR2Xlk" role="3cqZAp">
            <node concept="37vLTw" id="2FjKBCR2Xli" role="3clFbG">
              <ref role="3cqZAo" node="2FjKBCR2UAm" resolve="node" />
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="2FjKBCR2Ssp" role="upBLP">
        <node concept="uGdhv" id="2FjKBCR2Ssq" role="16NeZM">
          <node concept="3clFbS" id="2FjKBCR2Ssr" role="2VODD2">
            <node concept="3clFbF" id="2FjKBCR2Sss" role="3cqZAp">
              <node concept="2OqwBi" id="2FjKBCR2Sst" role="3clFbG">
                <node concept="35c_gC" id="2FjKBCR2Ssu" role="2Oq$k0">
                  <ref role="35c_gD" to="j0tk:6khVixyaF5m" resolve="FindingsFromAnyCultureTest" />
                </node>
                <node concept="2qgKlT" id="2FjKBCR2Uct" role="2OqNvi">
                  <ref role="37wK5l" to="duqf:2FjKBCR2T0q" resolve="getDisplayAliasWithSpecimenType" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="2FjKBCR2Ssw" role="upBLP">
        <node concept="uGdhv" id="2FjKBCR2Ssx" role="16NL0q">
          <node concept="3clFbS" id="2FjKBCR2Ssy" role="2VODD2">
            <node concept="3clFbF" id="2FjKBCR2Ssz" role="3cqZAp">
              <node concept="2OqwBi" id="2FjKBCR2Ss$" role="3clFbG">
                <node concept="35c_gC" id="2FjKBCR2Ss_" role="2Oq$k0">
                  <ref role="35c_gD" to="j0tk:6khVixyaF5m" resolve="FindingsFromAnyCultureTest" />
                </node>
                <node concept="2qgKlT" id="2FjKBCR2SsA" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="6khVixybci$">
    <property role="3GE5qa" value="base.constraints.cultureFindings.complex" />
    <ref role="aqKnT" to="j0tk:6khVixyb4G4" resolve="CultureFindingsIncludeAnyOf" />
    <node concept="3eGOop" id="6khVixybci_" role="3ft7WO">
      <node concept="ucgPf" id="6khVixybciA" role="3aKz83">
        <node concept="3clFbS" id="6khVixybciB" role="2VODD2">
          <node concept="3clFbF" id="6khVixybcn2" role="3cqZAp">
            <node concept="2ShNRf" id="6khVixybcn0" role="3clFbG">
              <node concept="2fJWfE" id="6khVixybcuz" role="2ShVmc">
                <node concept="3Tqbb2" id="6khVixybcu_" role="3zrR0E">
                  <ref role="ehGHo" to="j0tk:6khVixyb4G4" resolve="CultureFindingsIncludeAnyOf" />
                </node>
                <node concept="1yR$tW" id="6khVixybcDD" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="6khVixybcIg" role="upBLP">
        <node concept="uGdhv" id="6khVixybcMT" role="16NeZM">
          <node concept="3clFbS" id="6khVixybcMV" role="2VODD2">
            <node concept="3clFbF" id="6khVixybcVx" role="3cqZAp">
              <node concept="2OqwBi" id="6khVixybd$6" role="3clFbG">
                <node concept="35c_gC" id="6khVixybcVw" role="2Oq$k0">
                  <ref role="35c_gD" to="j0tk:6khVixyb4G4" resolve="CultureFindingsIncludeAnyOf" />
                </node>
                <node concept="2qgKlT" id="6khVixybe1A" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="6khVixybek3" role="upBLP">
        <node concept="uGdhv" id="6khVixybeoT" role="16NL0q">
          <node concept="3clFbS" id="6khVixybeoV" role="2VODD2">
            <node concept="3clFbF" id="6khVixybexx" role="3cqZAp">
              <node concept="2OqwBi" id="6khVixybfa6" role="3clFbG">
                <node concept="35c_gC" id="6khVixybexw" role="2Oq$k0">
                  <ref role="35c_gD" to="j0tk:6khVixyb4G4" resolve="CultureFindingsIncludeAnyOf" />
                </node>
                <node concept="2qgKlT" id="6khVixybfBA" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="6khVixycqk7">
    <property role="3GE5qa" value="base.constraints.cultureFindings.complex" />
    <ref role="1XX52x" to="j0tk:6khVixycqjC" resolve="ComplexCultureFindingsConstraint" />
    <node concept="3EZMnI" id="6khVixycqk9" role="2wV5jI">
      <node concept="2iRfu4" id="6khVixycqka" role="2iSdaV" />
      <node concept="B$lHz" id="6khVixycqkf" role="3EZMnx" />
      <node concept="3F0ifn" id="6khVixycqkl" role="3EZMnx">
        <property role="3F0ifm" value="[" />
        <ref role="1k5W1q" to="uubs:17XAtu7SK6X" resolve="LeftBracket" />
      </node>
      <node concept="3F2HdR" id="6khVixycqkt" role="3EZMnx">
        <property role="2czwfO" value="," />
        <ref role="1NtTu8" to="j0tk:6khVixycqjF" resolve="pathogensReferences" />
        <node concept="2iRfu4" id="6khVixycqkv" role="2czzBx" />
      </node>
      <node concept="3F0ifn" id="6khVixycqkE" role="3EZMnx">
        <property role="3F0ifm" value="]" />
        <ref role="1k5W1q" to="uubs:17XAtu7SK88" resolve="RightBracket" />
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="6khVixycJmS">
    <property role="3GE5qa" value="base.constraints.cultureFindings.complex" />
    <ref role="aqKnT" to="j0tk:6khVixycq0_" resolve="CultureFindingsIncludeAllOf" />
    <node concept="3eGOop" id="6khVixycJmT" role="3ft7WO">
      <node concept="ucgPf" id="6khVixycJmU" role="3aKz83">
        <node concept="3clFbS" id="6khVixycJmV" role="2VODD2">
          <node concept="3clFbF" id="6khVixycJsq" role="3cqZAp">
            <node concept="2ShNRf" id="6khVixycJso" role="3clFbG">
              <node concept="2fJWfE" id="6khVixycJ_2" role="2ShVmc">
                <node concept="3Tqbb2" id="6khVixycJ_4" role="3zrR0E">
                  <ref role="ehGHo" to="j0tk:6khVixycq0_" resolve="CultureFindingsIncludeAllOf" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="6khVixycJKd" role="upBLP">
        <node concept="uGdhv" id="6khVixycJPR" role="16NeZM">
          <node concept="3clFbS" id="6khVixycJPT" role="2VODD2">
            <node concept="3clFbF" id="6khVixycJYv" role="3cqZAp">
              <node concept="2OqwBi" id="6khVixycKD8" role="3clFbG">
                <node concept="35c_gC" id="6khVixycJYu" role="2Oq$k0">
                  <ref role="35c_gD" to="j0tk:6khVixycq0_" resolve="CultureFindingsIncludeAllOf" />
                </node>
                <node concept="2qgKlT" id="6khVixycLbm" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="6khVixycLvi" role="upBLP">
        <node concept="uGdhv" id="6khVixycL_9" role="16NL0q">
          <node concept="3clFbS" id="6khVixycL_b" role="2VODD2">
            <node concept="3clFbF" id="6khVixycLHL" role="3cqZAp">
              <node concept="2OqwBi" id="6khVixycMoq" role="3clFbG">
                <node concept="35c_gC" id="6khVixycLHK" role="2Oq$k0">
                  <ref role="35c_gD" to="j0tk:6khVixycq0_" resolve="CultureFindingsIncludeAllOf" />
                </node>
                <node concept="2qgKlT" id="6khVixycMUC" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="6khVixyd33j">
    <property role="3GE5qa" value="base.constraints.cultureFindings" />
    <ref role="aqKnT" to="j0tk:6khVixycP8V" resolve="CultureFindingsIncludePathogen" />
    <node concept="3eGOop" id="6khVixyd33k" role="3ft7WO">
      <node concept="16NfWO" id="6khVixyd57U" role="upBLP">
        <node concept="uGdhv" id="6khVixyd5dv" role="16NeZM">
          <node concept="3clFbS" id="6khVixyd5dx" role="2VODD2">
            <node concept="3clFbF" id="6khVixyd5m7" role="3cqZAp">
              <node concept="2OqwBi" id="6khVixyd60b" role="3clFbG">
                <node concept="35c_gC" id="6khVixyd5m6" role="2Oq$k0">
                  <ref role="35c_gD" to="j0tk:6khVixycP8V" resolve="CultureFindingsIncludePathogen" />
                </node>
                <node concept="2qgKlT" id="6khVixyd6x3" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="ucgPf" id="6khVixyd33l" role="3aKz83">
        <node concept="3clFbS" id="6khVixyd33m" role="2VODD2">
          <node concept="3clFbF" id="6khVixyd38z" role="3cqZAp">
            <node concept="2ShNRf" id="6khVixyd38x" role="3clFbG">
              <node concept="2fJWfE" id="6khVixyd3gQ" role="2ShVmc">
                <node concept="3Tqbb2" id="6khVixyd3gS" role="3zrR0E">
                  <ref role="ehGHo" to="j0tk:6khVixycP8V" resolve="CultureFindingsIncludePathogen" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="6khVixyd3rt" role="upBLP">
        <node concept="uGdhv" id="6khVixyd3wP" role="16NL0q">
          <node concept="3clFbS" id="6khVixyd3wR" role="2VODD2">
            <node concept="3clFbF" id="6khVixyd3Dt" role="3cqZAp">
              <node concept="2OqwBi" id="6khVixyd4jx" role="3clFbG">
                <node concept="35c_gC" id="6khVixyd3Ds" role="2Oq$k0">
                  <ref role="35c_gD" to="j0tk:6khVixycP8V" resolve="CultureFindingsIncludePathogen" />
                </node>
                <node concept="2qgKlT" id="6khVixyd4Op" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="6khVixyd8Lj">
    <property role="3GE5qa" value="base.constraints.cultureFindings" />
    <ref role="1XX52x" to="j0tk:6khVixycP8V" resolve="CultureFindingsIncludePathogen" />
    <node concept="3EZMnI" id="6khVixyd8Ll" role="2wV5jI">
      <node concept="2iRfu4" id="6khVixyd8Lm" role="2iSdaV" />
      <node concept="B$lHz" id="6khVixyd8Lr" role="3EZMnx" />
      <node concept="3F1sOY" id="6khVixyd8Lx" role="3EZMnx">
        <ref role="1NtTu8" to="j0tk:6khVixycReJ" resolve="pathogenReference" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="6khVixyj7GW">
    <property role="3GE5qa" value="base.data.antimicrobialSusceptibility" />
    <ref role="1XX52x" to="j0tk:6khVixyiJy9" resolve="AntimicrobialSusceptibilityOfPathogenFromAnyCultureTestAndSpecimenType" />
    <node concept="3EZMnI" id="6khVixyj7GY" role="2wV5jI">
      <node concept="2iRfu4" id="6khVixyj7GZ" role="2iSdaV" />
      <node concept="B$lHz" id="6khVixyj7H4" role="3EZMnx" />
      <node concept="3F1sOY" id="6khVixyj7Ha" role="3EZMnx">
        <ref role="1NtTu8" to="j0tk:6khVixyiJya" resolve="pathogenReference" />
      </node>
      <node concept="1HlG4h" id="6khVixyGNy2" role="3EZMnx">
        <ref role="1k5W1q" to="uubs:2XLt5KUltJN" resolve="DataValueKeyword" />
        <node concept="1HfYo3" id="6khVixyGNy4" role="1HlULh">
          <node concept="3TQlhw" id="6khVixyGNy6" role="1Hhtcw">
            <node concept="3clFbS" id="6khVixyGNy8" role="2VODD2">
              <node concept="3clFbF" id="6khVixyH1je" role="3cqZAp">
                <node concept="AH0OO" id="2FjKBCR6NQi" role="3clFbG">
                  <node concept="3cmrfG" id="2FjKBCR6O4T" role="AHEQo">
                    <property role="3cmrfH" value="1" />
                  </node>
                  <node concept="2OqwBi" id="2FjKBCR6MPO" role="AHHXb">
                    <node concept="2OqwBi" id="6khVixyH1_S" role="2Oq$k0">
                      <node concept="pncrf" id="6khVixyH1jd" role="2Oq$k0" />
                      <node concept="2yIwOk" id="6khVixyH28I" role="2OqNvi" />
                    </node>
                    <node concept="2qgKlT" id="2FjKBCR6NqB" role="2OqNvi">
                      <ref role="37wK5l" to="1yj:4B5aqq3ZFNV" resolve="getDisplayKeywords" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3F1sOY" id="6khVixyHo5X" role="3EZMnx">
        <ref role="1NtTu8" to="j0tk:6khVixyGI0c" resolve="antimicrobialReference" />
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="6khVixyjmoD">
    <property role="3GE5qa" value="base.data.antimicrobialSusceptibility" />
    <ref role="aqKnT" to="j0tk:6khVixyiJy9" resolve="AntimicrobialSusceptibilityOfPathogenFromAnyCultureTestAndSpecimenType" />
    <node concept="3eGOop" id="6khVixyjmoE" role="3ft7WO">
      <node concept="ucgPf" id="6khVixyjmoF" role="3aKz83">
        <node concept="3clFbS" id="6khVixyjmoG" role="2VODD2">
          <node concept="3clFbF" id="6khVixyjmtT" role="3cqZAp">
            <node concept="2ShNRf" id="6khVixyjmtR" role="3clFbG">
              <node concept="2fJWfE" id="6khVixyjmAf" role="2ShVmc">
                <node concept="3Tqbb2" id="6khVixyjmAh" role="3zrR0E">
                  <ref role="ehGHo" to="j0tk:6khVixyiJy9" resolve="AntimicrobialSusceptibilityOfPathogenFromAnyCultureTestAndSpecimenType" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="6khVixyjmKQ" role="upBLP">
        <node concept="uGdhv" id="6khVixyjn1a" role="16NeZM">
          <node concept="3clFbS" id="6khVixyjn1c" role="2VODD2">
            <node concept="3clFbF" id="6khVixyjn9M" role="3cqZAp">
              <node concept="2OqwBi" id="6khVixyjo2S" role="3clFbG">
                <node concept="35c_gC" id="6khVixyjn9L" role="2Oq$k0">
                  <ref role="35c_gD" to="j0tk:6khVixyiJy9" resolve="AntimicrobialSusceptibilityOfPathogenFromAnyCultureTestAndSpecimenType" />
                </node>
                <node concept="2qgKlT" id="6khVixyjozK" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="6khVixyjpb5" role="upBLP">
        <node concept="uGdhv" id="6khVixyjpgR" role="16NL0q">
          <node concept="3clFbS" id="6khVixyjpgT" role="2VODD2">
            <node concept="3clFbF" id="6khVixyjppv" role="3cqZAp">
              <node concept="2OqwBi" id="6khVixyjq40" role="3clFbG">
                <node concept="35c_gC" id="6khVixyjppu" role="2Oq$k0">
                  <ref role="35c_gD" to="j0tk:6khVixyiJy9" resolve="AntimicrobialSusceptibilityOfPathogenFromAnyCultureTestAndSpecimenType" />
                </node>
                <node concept="2qgKlT" id="6khVixyjq_T" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="6khVixykak4">
    <property role="3GE5qa" value="base.constraints.antimicrobialSensitivity" />
    <ref role="1XX52x" to="j0tk:6khVixyjFdS" resolve="AntimicrobialSensitivityEqualTo" />
    <node concept="3EZMnI" id="6khVixykak6" role="2wV5jI">
      <node concept="2iRfu4" id="6khVixykak7" role="2iSdaV" />
      <node concept="B$lHz" id="6khVixykakc" role="3EZMnx" />
      <node concept="3F1sOY" id="6khVixykaki" role="3EZMnx">
        <property role="1$x2rV" value="&lt;&lt; ... &gt;&gt;" />
        <ref role="1NtTu8" to="j0tk:6khVixyjNBG" resolve="sensitivity" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="6khVixykd9h">
    <property role="3GE5qa" value="base.literals" />
    <ref role="1XX52x" to="j0tk:6khVixyjMoI" resolve="AntimicrobialSensitivityLiteral" />
    <node concept="PMmxH" id="6khVixykfZK" role="2wV5jI">
      <ref role="PMmxG" to="uubs:5ZQBr_XMEtE" resolve="TranslatedAlias" />
      <ref role="1k5W1q" to="uubs:6khVixykMkg" resolve="Literal" />
    </node>
  </node>
  <node concept="3p36aQ" id="6khVixykrSg">
    <property role="3GE5qa" value="base.literals" />
    <ref role="aqKnT" to="j0tk:6khVixyjMoK" resolve="SensitiveAntimicrobialSensitivity" />
    <node concept="3eGOop" id="6khVixykrSh" role="3ft7WO">
      <node concept="ucgPf" id="6khVixykrSi" role="3aKz83">
        <node concept="3clFbS" id="6khVixykrSj" role="2VODD2">
          <node concept="3clFbF" id="6khVixykrXw" role="3cqZAp">
            <node concept="2ShNRf" id="6khVixykrXu" role="3clFbG">
              <node concept="2fJWfE" id="6khVixyks5N" role="2ShVmc">
                <node concept="3Tqbb2" id="6khVixyks5P" role="3zrR0E">
                  <ref role="ehGHo" to="j0tk:6khVixyjMoK" resolve="SensitiveAntimicrobialSensitivity" />
                </node>
                <node concept="1yR$tW" id="6khVixyksiQ" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="6khVixyksof" role="upBLP">
        <node concept="uGdhv" id="6khVixykstE" role="16NeZM">
          <node concept="3clFbS" id="6khVixykstG" role="2VODD2">
            <node concept="3clFbF" id="6khVixyksAi" role="3cqZAp">
              <node concept="2OqwBi" id="6khVixyktgm" role="3clFbG">
                <node concept="35c_gC" id="6khVixyksAh" role="2Oq$k0">
                  <ref role="35c_gD" to="j0tk:6khVixyjMoK" resolve="SensitiveAntimicrobialSensitivity" />
                </node>
                <node concept="2qgKlT" id="6khVixyktLb" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="6khVixyku4G" role="upBLP">
        <node concept="uGdhv" id="6khVixykuak" role="16NL0q">
          <node concept="3clFbS" id="6khVixykuam" role="2VODD2">
            <node concept="3clFbF" id="6khVixykuiW" role="3cqZAp">
              <node concept="2OqwBi" id="6khVixykuX0" role="3clFbG">
                <node concept="35c_gC" id="6khVixykuiV" role="2Oq$k0">
                  <ref role="35c_gD" to="j0tk:6khVixyjMoK" resolve="SensitiveAntimicrobialSensitivity" />
                </node>
                <node concept="2qgKlT" id="6khVixykvtP" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="6khVixykvH3">
    <property role="3GE5qa" value="base.literals" />
    <ref role="aqKnT" to="j0tk:6khVixyjMoJ" resolve="ResistantAntimicrobialSensitivity" />
    <node concept="3eGOop" id="6khVixykvH4" role="3ft7WO">
      <node concept="ucgPf" id="6khVixykvH5" role="3aKz83">
        <node concept="3clFbS" id="6khVixykvH6" role="2VODD2">
          <node concept="3clFbF" id="6khVixykvH7" role="3cqZAp">
            <node concept="2ShNRf" id="6khVixykvH8" role="3clFbG">
              <node concept="2fJWfE" id="6khVixykvH9" role="2ShVmc">
                <node concept="3Tqbb2" id="6khVixykvHa" role="3zrR0E">
                  <ref role="ehGHo" to="j0tk:6khVixyjMoJ" resolve="ResistantAntimicrobialSensitivity" />
                </node>
                <node concept="1yR$tW" id="6khVixykvHb" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="6khVixykvHc" role="upBLP">
        <node concept="uGdhv" id="6khVixykvHd" role="16NeZM">
          <node concept="3clFbS" id="6khVixykvHe" role="2VODD2">
            <node concept="3clFbF" id="6khVixykvHf" role="3cqZAp">
              <node concept="2OqwBi" id="6khVixykvHg" role="3clFbG">
                <node concept="35c_gC" id="6khVixykvHh" role="2Oq$k0">
                  <ref role="35c_gD" to="j0tk:6khVixyjMoJ" resolve="ResistantAntimicrobialSensitivity" />
                </node>
                <node concept="2qgKlT" id="6khVixykvHi" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="6khVixykvHj" role="upBLP">
        <node concept="uGdhv" id="6khVixykvHk" role="16NL0q">
          <node concept="3clFbS" id="6khVixykvHl" role="2VODD2">
            <node concept="3clFbF" id="6khVixykvHm" role="3cqZAp">
              <node concept="2OqwBi" id="6khVixykvHn" role="3clFbG">
                <node concept="35c_gC" id="6khVixykvHo" role="2Oq$k0">
                  <ref role="35c_gD" to="j0tk:6khVixyjMoJ" resolve="ResistantAntimicrobialSensitivity" />
                </node>
                <node concept="2qgKlT" id="6khVixykvHp" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="6khVixykwS6">
    <property role="3GE5qa" value="base.literals" />
    <ref role="aqKnT" to="j0tk:6khVixyjMoL" resolve="IntermediateAntimicrobialSensitivity" />
    <node concept="3eGOop" id="6khVixykwS7" role="3ft7WO">
      <node concept="ucgPf" id="6khVixykwS8" role="3aKz83">
        <node concept="3clFbS" id="6khVixykwS9" role="2VODD2">
          <node concept="3clFbF" id="6khVixykwSa" role="3cqZAp">
            <node concept="2ShNRf" id="6khVixykwSb" role="3clFbG">
              <node concept="2fJWfE" id="6khVixykwSc" role="2ShVmc">
                <node concept="3Tqbb2" id="6khVixykwSd" role="3zrR0E">
                  <ref role="ehGHo" to="j0tk:6khVixyjMoL" resolve="IntermediateAntimicrobialSensitivity" />
                </node>
                <node concept="1yR$tW" id="6khVixykwSe" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="6khVixykwSf" role="upBLP">
        <node concept="uGdhv" id="6khVixykwSg" role="16NeZM">
          <node concept="3clFbS" id="6khVixykwSh" role="2VODD2">
            <node concept="3clFbF" id="6khVixykwSi" role="3cqZAp">
              <node concept="2OqwBi" id="6khVixykwSj" role="3clFbG">
                <node concept="35c_gC" id="6khVixykwSk" role="2Oq$k0">
                  <ref role="35c_gD" to="j0tk:6khVixyjMoL" resolve="IntermediateAntimicrobialSensitivity" />
                </node>
                <node concept="2qgKlT" id="6khVixykwSl" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="6khVixykwSm" role="upBLP">
        <node concept="uGdhv" id="6khVixykwSn" role="16NL0q">
          <node concept="3clFbS" id="6khVixykwSo" role="2VODD2">
            <node concept="3clFbF" id="6khVixykwSp" role="3cqZAp">
              <node concept="2OqwBi" id="6khVixykwSq" role="3clFbG">
                <node concept="35c_gC" id="6khVixykwSr" role="2Oq$k0">
                  <ref role="35c_gD" to="j0tk:6khVixyjMoL" resolve="IntermediateAntimicrobialSensitivity" />
                </node>
                <node concept="2qgKlT" id="6khVixykwSs" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="6khVixykA4s">
    <property role="3GE5qa" value="base.constraints.antimicrobialSensitivity" />
    <ref role="aqKnT" to="j0tk:6khVixyjFdS" resolve="AntimicrobialSensitivityEqualTo" />
    <node concept="3eGOop" id="6khVixykA4t" role="3ft7WO">
      <node concept="ucgPf" id="6khVixykA4u" role="3aKz83">
        <node concept="3clFbS" id="6khVixykA4v" role="2VODD2">
          <node concept="3clFbF" id="6khVixykA4w" role="3cqZAp">
            <node concept="2ShNRf" id="6khVixykA4x" role="3clFbG">
              <node concept="2fJWfE" id="6khVixykA4y" role="2ShVmc">
                <node concept="3Tqbb2" id="6khVixykA4z" role="3zrR0E">
                  <ref role="ehGHo" to="j0tk:6khVixyjFdS" resolve="AntimicrobialSensitivityEqualTo" />
                </node>
                <node concept="1yR$tW" id="6khVixykA4$" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="6khVixykA4_" role="upBLP">
        <node concept="uGdhv" id="6khVixykA4A" role="16NeZM">
          <node concept="3clFbS" id="6khVixykA4B" role="2VODD2">
            <node concept="3clFbF" id="6khVixykA4C" role="3cqZAp">
              <node concept="2OqwBi" id="6khVixykA4D" role="3clFbG">
                <node concept="35c_gC" id="6khVixykA4E" role="2Oq$k0">
                  <ref role="35c_gD" to="j0tk:6khVixyjFdS" resolve="AntimicrobialSensitivityEqualTo" />
                </node>
                <node concept="2qgKlT" id="6khVixykA4F" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="6khVixykA4G" role="upBLP">
        <node concept="uGdhv" id="6khVixykA4H" role="16NL0q">
          <node concept="3clFbS" id="6khVixykA4I" role="2VODD2">
            <node concept="3clFbF" id="6khVixykA4J" role="3cqZAp">
              <node concept="2OqwBi" id="6khVixykA4K" role="3clFbG">
                <node concept="35c_gC" id="6khVixykA4L" role="2Oq$k0">
                  <ref role="35c_gD" to="j0tk:6khVixyjFdS" resolve="AntimicrobialSensitivityEqualTo" />
                </node>
                <node concept="2qgKlT" id="6khVixykA4M" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

