<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:2895f9f0-ca08-4015-b25a-0b35f51e5738(no.uio.mLab.decisions.data.microbiology.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="6" />
    <devkit ref="78434eb8-b0e5-444b-850d-e7c4ad2da9ab(jetbrains.mps.devkit.aspect.structure)" />
  </languages>
  <imports>
    <import index="vbok" ref="r:6cc208d3-a370-49ca-a6ac-ded97805a924(no.uio.mLab.shared.structure)" />
    <import index="7f9y" ref="r:790ac37e-42d3-4dd3-977c-e8fe9cc56a45(no.uio.mLab.decisions.core.structure)" />
    <import index="ruww" ref="r:d7787915-5b9a-4713-a6ab-6345c5e1ec03(no.uio.mLab.decisions.references.laboratoryTest.structure)" />
    <import index="ok0m" ref="r:4db7f216-3df2-435e-8be1-a0ab7e4a26b5(no.uio.mLab.decisions.references.microbiology.structure)" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <property id="4628067390765956807" name="final" index="R5$K2" />
        <property id="4628067390765956802" name="abstract" index="R5$K7" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
      </concept>
      <concept id="1169125989551" name="jetbrains.mps.lang.structure.structure.InterfaceConceptDeclaration" flags="ig" index="PlHQZ">
        <child id="1169127546356" name="extends" index="PrDN$" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="PlHQZ" id="1Hxyv4EUOl1">
    <property role="EcuMT" value="1973009780665173313" />
    <property role="TrG5h" value="ITranslatableBiochemistryConcept" />
    <property role="3GE5qa" value="shared" />
    <node concept="PrWs8" id="1Hxyv4EUOlc" role="PrDN$">
      <ref role="PrY4T" to="vbok:5Wfdz$0vc1Z" resolve="ITranslatableConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="6khVixyaF5m">
    <property role="EcuMT" value="7282862830136242518" />
    <property role="3GE5qa" value="base.data.cultureFindings" />
    <property role="TrG5h" value="FindingsFromAnyCultureTest" />
    <ref role="1TJDcQ" node="6khVixyaF5n" resolve="CultureFindingsData" />
    <node concept="PrWs8" id="6khVixyaOfT" role="PzmwI">
      <ref role="PrY4T" node="1Hxyv4EUOl1" resolve="ITranslatableBiochemistryConcept" />
    </node>
    <node concept="1TJgyj" id="6khVixybjNf" role="1TKVEi">
      <property role="IQ2ns" value="7282862830136409295" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="specimenTypeReference" />
      <property role="20lbJX" value="0..1" />
      <ref role="20lvS9" to="ruww:6khVixyauId" resolve="SpecimenTypeReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="6khVixyb4G4">
    <property role="EcuMT" value="7282862830136347396" />
    <property role="3GE5qa" value="base.constraints.cultureFindings.complex" />
    <property role="TrG5h" value="CultureFindingsIncludeAnyOf" />
    <ref role="1TJDcQ" node="6khVixycqjC" resolve="ComplexCultureFindingsConstraint" />
    <node concept="PrWs8" id="6khVixyb84q" role="PzmwI">
      <ref role="PrY4T" node="1Hxyv4EUOl1" resolve="ITranslatableBiochemistryConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="6khVixybgYp">
    <property role="EcuMT" value="7282862830136397721" />
    <property role="3GE5qa" value="base.constraints.cultureFindings" />
    <property role="R5$K7" value="true" />
    <property role="R5$K2" value="false" />
    <property role="TrG5h" value="CultureFindingsConstraint" />
    <ref role="1TJDcQ" to="7f9y:1mAGFBKqQs5" resolve="Constraint" />
  </node>
  <node concept="1TIwiD" id="6khVixyaF5n">
    <property role="EcuMT" value="7282862830136242519" />
    <property role="3GE5qa" value="base.data.cultureFindings" />
    <property role="TrG5h" value="CultureFindingsData" />
    <property role="R5$K7" value="true" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" to="7f9y:1mAGFBLqAeU" resolve="NonBooleanDataValue" />
  </node>
  <node concept="1TIwiD" id="6khVixycq0_">
    <property role="EcuMT" value="7282862830136696869" />
    <property role="3GE5qa" value="base.constraints.cultureFindings.complex" />
    <property role="TrG5h" value="CultureFindingsIncludeAllOf" />
    <ref role="1TJDcQ" node="6khVixycqjC" resolve="ComplexCultureFindingsConstraint" />
    <node concept="PrWs8" id="6khVixycq0B" role="PzmwI">
      <ref role="PrY4T" node="1Hxyv4EUOl1" resolve="ITranslatableBiochemistryConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="6khVixycqjC">
    <property role="EcuMT" value="7282862830136698088" />
    <property role="3GE5qa" value="base.constraints.cultureFindings.complex" />
    <property role="TrG5h" value="ComplexCultureFindingsConstraint" />
    <property role="R5$K7" value="true" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" node="6khVixybgYp" resolve="CultureFindingsConstraint" />
    <node concept="1TJgyj" id="6khVixycqjF" role="1TKVEi">
      <property role="IQ2ns" value="7282862830136698091" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="pathogensReferences" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" to="ok0m:6khVixyapzE" resolve="PathogenReference" />
    </node>
    <node concept="PrWs8" id="6khVixycqjE" role="PzmwI">
      <ref role="PrY4T" node="1Hxyv4EUOl1" resolve="ITranslatableBiochemistryConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="6khVixycP8V">
    <property role="EcuMT" value="7282862830136807995" />
    <property role="3GE5qa" value="base.constraints.cultureFindings" />
    <property role="TrG5h" value="CultureFindingsIncludePathogen" />
    <ref role="1TJDcQ" node="6khVixybgYp" resolve="CultureFindingsConstraint" />
    <node concept="PrWs8" id="6khVixycPvT" role="PzmwI">
      <ref role="PrY4T" node="1Hxyv4EUOl1" resolve="ITranslatableBiochemistryConcept" />
    </node>
    <node concept="1TJgyj" id="6khVixycReJ" role="1TKVEi">
      <property role="IQ2ns" value="7282862830136816559" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="pathogenReference" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="ok0m:6khVixyapzE" resolve="PathogenReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="6khVixyiJy9">
    <property role="EcuMT" value="7282862830138357897" />
    <property role="3GE5qa" value="base.data.antimicrobialSusceptibility" />
    <property role="TrG5h" value="AntimicrobialSusceptibilityOfPathogenFromAnyCultureTestAndSpecimenType" />
    <ref role="1TJDcQ" node="6khVixyiJSf" resolve="AntimicrobialSusceptibilityData" />
    <node concept="1TJgyj" id="6khVixyiJya" role="1TKVEi">
      <property role="IQ2ns" value="7282862830138357898" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="pathogenReference" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="ok0m:6khVixyapzE" resolve="PathogenReference" />
    </node>
    <node concept="1TJgyj" id="6khVixyGI0c" role="1TKVEi">
      <property role="IQ2ns" value="7282862830145167372" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="antimicrobialReference" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="ok0m:6khVixyGBAo" resolve="AntimicrobialReference" />
    </node>
    <node concept="PrWs8" id="6khVixyiOU1" role="PzmwI">
      <ref role="PrY4T" node="1Hxyv4EUOl1" resolve="ITranslatableBiochemistryConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="6khVixyiJSf">
    <property role="EcuMT" value="7282862830138359311" />
    <property role="3GE5qa" value="base.data.antimicrobialSusceptibility" />
    <property role="TrG5h" value="AntimicrobialSusceptibilityData" />
    <property role="R5$K7" value="true" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" to="7f9y:1mAGFBLqAeU" resolve="NonBooleanDataValue" />
  </node>
  <node concept="1TIwiD" id="6khVixyj$wV">
    <property role="EcuMT" value="7282862830138574907" />
    <property role="3GE5qa" value="base.constraints.antimicrobialSensitivity" />
    <property role="TrG5h" value="AntimicrobialSensitivityConstraint" />
    <property role="R5$K7" value="true" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" to="7f9y:1mAGFBKqQs5" resolve="Constraint" />
  </node>
  <node concept="1TIwiD" id="6khVixyjFdS">
    <property role="EcuMT" value="7282862830138602360" />
    <property role="3GE5qa" value="base.constraints.antimicrobialSensitivity" />
    <property role="TrG5h" value="AntimicrobialSensitivityEqualTo" />
    <ref role="1TJDcQ" node="6khVixyj$wV" resolve="AntimicrobialSensitivityConstraint" />
    <node concept="PrWs8" id="6khVixyjKWv" role="PzmwI">
      <ref role="PrY4T" node="1Hxyv4EUOl1" resolve="ITranslatableBiochemistryConcept" />
    </node>
    <node concept="1TJgyj" id="6khVixyjNBG" role="1TKVEi">
      <property role="IQ2ns" value="7282862830138636780" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="sensitivity" />
      <ref role="20lvS9" node="6khVixyjMoI" resolve="AntimicrobialSensitivityLiteral" />
    </node>
  </node>
  <node concept="1TIwiD" id="6khVixyjMoI">
    <property role="EcuMT" value="7282862830138631726" />
    <property role="3GE5qa" value="base.literals" />
    <property role="TrG5h" value="AntimicrobialSensitivityLiteral" />
    <property role="R5$K7" value="true" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" to="7f9y:5Wfdz$0qdiH" resolve="Literal" />
    <node concept="PrWs8" id="6khVixykiRd" role="PzmwI">
      <ref role="PrY4T" node="1Hxyv4EUOl1" resolve="ITranslatableBiochemistryConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="6khVixyjMoJ">
    <property role="EcuMT" value="7282862830138631727" />
    <property role="3GE5qa" value="base.literals" />
    <property role="TrG5h" value="ResistantAntimicrobialSensitivity" />
    <ref role="1TJDcQ" node="6khVixyjMoI" resolve="AntimicrobialSensitivityLiteral" />
  </node>
  <node concept="1TIwiD" id="6khVixyjMoK">
    <property role="EcuMT" value="7282862830138631728" />
    <property role="3GE5qa" value="base.literals" />
    <property role="TrG5h" value="SensitiveAntimicrobialSensitivity" />
    <ref role="1TJDcQ" node="6khVixyjMoI" resolve="AntimicrobialSensitivityLiteral" />
  </node>
  <node concept="1TIwiD" id="6khVixyjMoL">
    <property role="EcuMT" value="7282862830138631729" />
    <property role="3GE5qa" value="base.literals" />
    <property role="TrG5h" value="IntermediateAntimicrobialSensitivity" />
    <ref role="1TJDcQ" node="6khVixyjMoI" resolve="AntimicrobialSensitivityLiteral" />
  </node>
</model>

