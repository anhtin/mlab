<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:8ca2c605-045f-4975-a8b1-e0310faaf919(no.uio.mLab.decisions.data.microbiology.translations)">
  <persistence version="9" />
  <languages>
    <use id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage" version="6" />
  </languages>
  <imports>
    <import index="9igz" ref="r:0ba33904-c124-449b-bde4-640f55a71953(no.uio.mLab.shared.translations)" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1465982738277781862" name="jetbrains.mps.baseLanguage.structure.PlaceholderMember" flags="ng" index="2tJIrI" />
      <concept id="1188207840427" name="jetbrains.mps.baseLanguage.structure.AnnotationInstance" flags="nn" index="2AHcQZ">
        <reference id="1188208074048" name="annotation" index="2AI5Lk" />
      </concept>
      <concept id="1188208481402" name="jetbrains.mps.baseLanguage.structure.HasAnnotation" flags="ng" index="2AJDlI">
        <child id="1188208488637" name="annotation" index="2AJF6D" />
      </concept>
      <concept id="2820489544401957797" name="jetbrains.mps.baseLanguage.structure.DefaultClassCreator" flags="nn" index="HV5vD">
        <reference id="2820489544401957798" name="classifier" index="HV5vE" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1070462154015" name="jetbrains.mps.baseLanguage.structure.StaticFieldDeclaration" flags="ig" index="Wx3nA">
        <property id="6468716278899126575" name="isVolatile" index="2dlcS1" />
        <property id="6468716278899125786" name="isTransient" index="2dld4O" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1081236700938" name="jetbrains.mps.baseLanguage.structure.StaticMethodDeclaration" flags="ig" index="2YIFZL" />
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu">
        <child id="1095933932569" name="implementedInterface" index="EKbjA" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <property id="1176718929932" name="isFinal" index="3TUv4t" />
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <property id="4276006055363816570" name="isSynchronized" index="od$2w" />
        <property id="1181808852946" name="isFinal" index="DiZV1" />
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123165" name="jetbrains.mps.baseLanguage.structure.InstanceMethodDeclaration" flags="ig" index="3clFb_">
        <property id="1178608670077" name="isAbstract" index="1EzhhJ" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1107461130800" name="jetbrains.mps.baseLanguage.structure.Classifier" flags="ng" index="3pOWGL">
        <child id="5375687026011219971" name="member" index="jymVt" unordered="true" />
      </concept>
      <concept id="7812454656619025416" name="jetbrains.mps.baseLanguage.structure.MethodDeclaration" flags="ng" index="1rXfSm">
        <property id="8355037393041754995" name="isNative" index="2aFKle" />
      </concept>
      <concept id="7812454656619025412" name="jetbrains.mps.baseLanguage.structure.LocalMethodCall" flags="nn" index="1rXfSq" />
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1107796713796" name="jetbrains.mps.baseLanguage.structure.Interface" flags="ig" index="3HP615" />
      <concept id="1163670490218" name="jetbrains.mps.baseLanguage.structure.SwitchStatement" flags="nn" index="3KaCP$">
        <child id="1163670592366" name="defaultBlock" index="3Kb1Dw" />
        <child id="1163670766145" name="expression" index="3KbGdf" />
        <child id="1163670772911" name="case" index="3KbHQx" />
      </concept>
      <concept id="1163670641947" name="jetbrains.mps.baseLanguage.structure.SwitchCase" flags="ng" index="3KbdKl">
        <child id="1163670677455" name="expression" index="3Kbmr1" />
        <child id="1163670683720" name="body" index="3Kbo56" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
      <concept id="1146644623116" name="jetbrains.mps.baseLanguage.structure.PrivateVisibility" flags="nn" index="3Tm6S6" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="312cEu" id="4zMac8rUNsN">
    <property role="TrG5h" value="MicrobiologyDataTranslationProvider" />
    <property role="3GE5qa" value="internationalization" />
    <node concept="Wx3nA" id="4zMac8rVBeK" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="TrG5h" value="displayTranslations" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm1VV" id="4zMac8rVBei" role="1B3o_S" />
      <node concept="3uibUv" id="4zMac8rVBeC" role="1tU5fm">
        <ref role="3uigEE" node="4zMac8rUNtP" resolve="IMicrobiologyDataTranslations" />
      </node>
      <node concept="1rXfSq" id="4zMac8rVBfh" role="33vP2m">
        <ref role="37wK5l" node="4zMac8rUNuE" resolve="init" />
        <node concept="10M0yZ" id="ofcHXfTMGc" role="37wK5m">
          <ref role="3cqZAo" to="9igz:5fXsrCVmzVm" resolve="displayLanguage" />
          <ref role="1PxDUh" to="9igz:5fXsrCVmxTN" resolve="TranslationConfiguration" />
        </node>
      </node>
    </node>
    <node concept="Wx3nA" id="1Hxyv4DUmfq" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="TrG5h" value="generationTranslations" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm1VV" id="1Hxyv4DUmfr" role="1B3o_S" />
      <node concept="3uibUv" id="1Hxyv4DUmfs" role="1tU5fm">
        <ref role="3uigEE" node="4zMac8rUNtP" resolve="IMicrobiologyDataTranslations" />
      </node>
      <node concept="1rXfSq" id="1Hxyv4DUmft" role="33vP2m">
        <ref role="37wK5l" node="4zMac8rUNuE" resolve="init" />
        <node concept="10M0yZ" id="ofcHXfTMH1" role="37wK5m">
          <ref role="3cqZAo" to="9igz:5fXsrCVmzVN" resolve="generationLanguage" />
          <ref role="1PxDUh" to="9igz:5fXsrCVmxTN" resolve="TranslationConfiguration" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="4zMac8rUNtp" role="jymVt" />
    <node concept="2YIFZL" id="4zMac8rUNuE" role="jymVt">
      <property role="TrG5h" value="init" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="4zMac8rUNuH" role="3clF47">
        <node concept="3KaCP$" id="4zMac8rUNv7" role="3cqZAp">
          <node concept="3KbdKl" id="4zMac8rUNXu" role="3KbHQx">
            <node concept="Xl_RD" id="4zMac8rUNXU" role="3Kbmr1">
              <property role="Xl_RC" value="no" />
            </node>
            <node concept="3clFbS" id="4zMac8rUNXw" role="3Kbo56">
              <node concept="3cpWs6" id="4zMac8rUNYt" role="3cqZAp">
                <node concept="2ShNRf" id="4zMac8rUO3y" role="3cqZAk">
                  <node concept="HV5vD" id="4zMac8rUP5z" role="2ShVmc">
                    <ref role="HV5vE" node="4zMac8rUO1O" resolve="NoMicrobiologyDataTranslations" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="4zMac8rUNYX" role="3Kb1Dw">
            <node concept="3cpWs6" id="4zMac8rUNZC" role="3cqZAp">
              <node concept="2ShNRf" id="4zMac8rUP6U" role="3cqZAk">
                <node concept="HV5vD" id="4zMac8rUPap" role="2ShVmc">
                  <ref role="HV5vE" node="4zMac8rUO0_" resolve="EnMicrobiologyDataTranslations" />
                </node>
              </node>
            </node>
          </node>
          <node concept="37vLTw" id="1Hxyv4DUmde" role="3KbGdf">
            <ref role="3cqZAo" node="1Hxyv4DUm7P" resolve="language" />
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="4zMac8rW809" role="1B3o_S" />
      <node concept="3uibUv" id="4zMac8rUNuz" role="3clF45">
        <ref role="3uigEE" node="4zMac8rUNtP" resolve="IMicrobiologyDataTranslations" />
      </node>
      <node concept="37vLTG" id="1Hxyv4DUm7P" role="3clF46">
        <property role="TrG5h" value="language" />
        <node concept="17QB3L" id="1Hxyv4DUm7O" role="1tU5fm" />
      </node>
    </node>
    <node concept="3Tm1VV" id="4zMac8rUNsO" role="1B3o_S" />
  </node>
  <node concept="3HP615" id="4zMac8rUNtP">
    <property role="TrG5h" value="IMicrobiologyDataTranslations" />
    <property role="3GE5qa" value="internationalization" />
    <node concept="3clFb_" id="6khVixyaM_f" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getFindingsFromAnyCultureTestAlias" />
      <node concept="3clFbS" id="6khVixyaM_i" role="3clF47" />
      <node concept="3Tm1VV" id="6khVixyaM_j" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyaM_6" role="3clF45" />
    </node>
    <node concept="3clFb_" id="6khVixyaMAm" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getFindingsFromAnyCultureTestDescription" />
      <node concept="3clFbS" id="6khVixyaMAp" role="3clF47" />
      <node concept="3Tm1VV" id="6khVixyaMAq" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyaM_Q" role="3clF45" />
    </node>
    <node concept="3clFb_" id="2FjKBCR2CG5" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getFindingsFromAnyCultureTestFromSpecimenTypeAlias" />
      <node concept="3clFbS" id="2FjKBCR2CG6" role="3clF47" />
      <node concept="3Tm1VV" id="2FjKBCR2CG7" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCR2CG8" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="6khVixyjFiI" role="jymVt" />
    <node concept="3clFb_" id="6khVixybDN7" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getCultureFindingsIncludeAllOfAlias" />
      <node concept="3clFbS" id="6khVixybDN8" role="3clF47" />
      <node concept="3Tm1VV" id="6khVixybDN9" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixybDNa" role="3clF45" />
    </node>
    <node concept="3clFb_" id="6khVixybDNb" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getCultureFindingsIncludeAllOfDescription" />
      <node concept="3clFbS" id="6khVixybDNc" role="3clF47" />
      <node concept="3Tm1VV" id="6khVixybDNd" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixybDNe" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="6khVixybDLY" role="jymVt" />
    <node concept="3clFb_" id="6khVixyb4Uu" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getCultureFindingsIncludeAnyOfAlias" />
      <node concept="3clFbS" id="6khVixyb4Ux" role="3clF47" />
      <node concept="3Tm1VV" id="6khVixyb4Uy" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyb4U4" role="3clF45" />
    </node>
    <node concept="3clFb_" id="6khVixyb4Wd" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getCultureFindingsIncludeAnyOfDescription" />
      <node concept="3clFbS" id="6khVixyb4Wg" role="3clF47" />
      <node concept="3Tm1VV" id="6khVixyb4Wh" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyb4VF" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="6khVixybDGl" role="jymVt" />
    <node concept="3clFb_" id="6khVixybDEr" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getCultureFindingsIncludePathogenAlias" />
      <node concept="3clFbS" id="6khVixybDEu" role="3clF47" />
      <node concept="3Tm1VV" id="6khVixybDEv" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixybDDx" role="3clF45" />
    </node>
    <node concept="3clFb_" id="6khVixybDJn" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getCultureFindingsIncludePathogenDescription" />
      <node concept="3clFbS" id="6khVixybDJq" role="3clF47" />
      <node concept="3Tm1VV" id="6khVixybDJr" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixybDIk" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="6khVixyiKls" role="jymVt" />
    <node concept="3clFb_" id="6khVixyiKpk" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getAntimicrobialSusceptibilityOfPathogenFromAnyCultureTestAndSpecimenTypeAlias" />
      <node concept="3clFbS" id="6khVixyiKpn" role="3clF47" />
      <node concept="3Tm1VV" id="6khVixyiKpo" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyiKnZ" role="3clF45" />
    </node>
    <node concept="3clFb_" id="6khVixyiKs1" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getAntimicrobialSusceptibilityOfPathogenFromAnyCultureTestAndSpecimenTypeDescription" />
      <node concept="3clFbS" id="6khVixyiKs2" role="3clF47" />
      <node concept="3Tm1VV" id="6khVixyiKs3" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyiKs4" role="3clF45" />
    </node>
    <node concept="3clFb_" id="6khVixyGIrk" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getAntimicrobialSusceptibilityOfPathogenFromAnyCultureTestAndSpecimenTypeForAntimicrobialKeyword" />
      <node concept="3clFbS" id="6khVixyGIrn" role="3clF47" />
      <node concept="3Tm1VV" id="6khVixyGIro" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyGIoj" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="6khVixyjFjG" role="jymVt" />
    <node concept="3clFb_" id="6khVixyjFOn" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getAntimicrobialSusceptibilityEqualToAlias" />
      <node concept="3clFbS" id="6khVixyjFOq" role="3clF47" />
      <node concept="3Tm1VV" id="6khVixyjFOr" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyjFD3" role="3clF45" />
    </node>
    <node concept="3clFb_" id="6khVixyjFSl" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getAntimicrobialSusceptibilityEqualToDescription" />
      <node concept="3clFbS" id="6khVixyjFSm" role="3clF47" />
      <node concept="3Tm1VV" id="6khVixyjFSn" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyjFSo" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="6khVixyjMtQ" role="jymVt" />
    <node concept="3clFb_" id="6khVixyjMzo" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getSensitiveAntimicrobialSusceptibilityAlias" />
      <node concept="3clFbS" id="6khVixyjMzr" role="3clF47" />
      <node concept="3Tm1VV" id="6khVixyjMzs" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyjMx9" role="3clF45" />
    </node>
    <node concept="3clFb_" id="6khVixyjMBR" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getSensitiveAntimicrobialSusceptibilityDescription" />
      <node concept="3clFbS" id="6khVixyjMBS" role="3clF47" />
      <node concept="3Tm1VV" id="6khVixyjMBT" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyjMBU" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="6khVixyjMYC" role="jymVt" />
    <node concept="3clFb_" id="6khVixyjMON" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getIntermediateAntimicrobialSusceptibilityAlias" />
      <node concept="3clFbS" id="6khVixyjMOQ" role="3clF47" />
      <node concept="3Tm1VV" id="6khVixyjMOR" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyjMMk" role="3clF45" />
    </node>
    <node concept="3clFb_" id="6khVixyjN6p" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getIntermediateAntimicrobialSusceptibilityDescription" />
      <node concept="3clFbS" id="6khVixyjN6s" role="3clF47" />
      <node concept="3Tm1VV" id="6khVixyjN6t" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyjN3L" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="6khVixyjNbh" role="jymVt" />
    <node concept="3clFb_" id="6khVixyjNgP" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getResistantAntimicrobialSusceptibilityAlias" />
      <node concept="3clFbS" id="6khVixyjNgS" role="3clF47" />
      <node concept="3Tm1VV" id="6khVixyjNgT" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyjNe4" role="3clF45" />
    </node>
    <node concept="3clFb_" id="6khVixyjNyz" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getResistantAntimicrobialSusceptibilityDescription" />
      <node concept="3clFbS" id="6khVixyjNyA" role="3clF47" />
      <node concept="3Tm1VV" id="6khVixyjNyB" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyjNvE" role="3clF45" />
    </node>
    <node concept="3Tm1VV" id="4zMac8rUNtQ" role="1B3o_S" />
  </node>
  <node concept="312cEu" id="4zMac8rUO0_">
    <property role="TrG5h" value="EnMicrobiologyDataTranslations" />
    <property role="3GE5qa" value="localization" />
    <node concept="3Tm1VV" id="4zMac8rUO0A" role="1B3o_S" />
    <node concept="3uibUv" id="4zMac8rUO1F" role="EKbjA">
      <ref role="3uigEE" node="4zMac8rUNtP" resolve="IMicrobiologyDataTranslations" />
    </node>
    <node concept="3clFb_" id="6khVixyaN4R" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getFindingsFromAnyCultureTestAlias" />
      <node concept="3Tm1VV" id="6khVixyaN4T" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyaN4U" role="3clF45" />
      <node concept="3clFbS" id="6khVixyaN4V" role="3clF47">
        <node concept="3clFbF" id="6khVixyaNa9" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixyaNa8" role="3clFbG">
            <property role="Xl_RC" value="culture findings" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixyaN4W" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="6khVixyaN4X" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getFindingsFromAnyCultureTestDescription" />
      <node concept="3Tm1VV" id="6khVixyaN4Z" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyaN50" role="3clF45" />
      <node concept="3clFbS" id="6khVixyaN51" role="3clF47">
        <node concept="3clFbF" id="6khVixyaNsj" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixyaNsi" role="3clFbG">
            <property role="Xl_RC" value="check suspected pathogen from culture testing" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixyaN52" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="2FjKBCR2DB9" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getFindingsFromAnyCultureTestFromSpecimenTypeAlias" />
      <node concept="3Tm1VV" id="2FjKBCR2DBb" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCR2DBc" role="3clF45" />
      <node concept="3clFbS" id="2FjKBCR2DBd" role="3clF47">
        <node concept="3clFbF" id="2FjKBCR2E0I" role="3cqZAp">
          <node concept="Xl_RD" id="2FjKBCR2E0H" role="3clFbG">
            <property role="Xl_RC" value="culture findings from" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="2FjKBCR2DBe" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="6khVixyb4ZT" role="jymVt" />
    <node concept="3clFb_" id="6khVixybEqD" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getCultureFindingsIncludeAllOfAlias" />
      <node concept="3Tm1VV" id="6khVixybEqF" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixybEqG" role="3clF45" />
      <node concept="3clFbS" id="6khVixybEqH" role="3clF47">
        <node concept="3clFbF" id="6khVixybEHQ" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixybEHP" role="3clFbG">
            <property role="Xl_RC" value="include all of" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixybEqI" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="6khVixybEqJ" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getCultureFindingsIncludeAllOfDescription" />
      <node concept="3Tm1VV" id="6khVixybEqL" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixybEqM" role="3clF45" />
      <node concept="3clFbS" id="6khVixybEqN" role="3clF47">
        <node concept="3clFbF" id="6khVixybEJ9" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixybEJ8" role="3clFbG">
            <property role="Xl_RC" value="check that findings include all listed pathogens" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixybEqO" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="6khVixybEdt" role="jymVt" />
    <node concept="3clFb_" id="6khVixyb52i" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getCultureFindingsIncludeAnyOfAlias" />
      <node concept="3Tm1VV" id="6khVixyb52k" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyb52l" role="3clF45" />
      <node concept="3clFbS" id="6khVixyb52m" role="3clF47">
        <node concept="3clFbF" id="6khVixyb57T" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixyb57S" role="3clFbG">
            <property role="Xl_RC" value="include any of" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixyb52n" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="6khVixyb52o" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getCultureFindingsIncludeAnyOfDescription" />
      <node concept="3Tm1VV" id="6khVixyb52q" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyb52r" role="3clF45" />
      <node concept="3clFbS" id="6khVixyb52s" role="3clF47">
        <node concept="3clFbF" id="6khVixyb5cV" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixyb5cU" role="3clFbG">
            <property role="Xl_RC" value="check that findings include any of the listed pathogens" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixyb52t" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="6khVixybEPU" role="jymVt" />
    <node concept="3clFb_" id="6khVixybF7o" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getCultureFindingsIncludePathogenAlias" />
      <node concept="3Tm1VV" id="6khVixybF7q" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixybF7r" role="3clF45" />
      <node concept="3clFbS" id="6khVixybF7s" role="3clF47">
        <node concept="3clFbF" id="6khVixybFvf" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixybFve" role="3clFbG">
            <property role="Xl_RC" value="include pathogen" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixybF7t" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="6khVixybF7u" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getCultureFindingsIncludePathogenDescription" />
      <node concept="3Tm1VV" id="6khVixybF7w" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixybF7x" role="3clF45" />
      <node concept="3clFbS" id="6khVixybF7y" role="3clF47">
        <node concept="3clFbF" id="6khVixybFwy" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixybFwx" role="3clFbG">
            <property role="Xl_RC" value="check that findings include a specific pathogen" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixybF7z" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="6khVixyiKIs" role="jymVt" />
    <node concept="3clFb_" id="6khVixyiKSj" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getAntimicrobialSusceptibilityOfPathogenFromAnyCultureTestAndSpecimenTypeAlias" />
      <node concept="3Tm1VV" id="6khVixyiKSl" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyiKSm" role="3clF45" />
      <node concept="3clFbS" id="6khVixyiKSn" role="3clF47">
        <node concept="3clFbF" id="6khVixyiL6G" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixyiL6F" role="3clFbG">
            <property role="Xl_RC" value="susceptibility of" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixyiKSo" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="6khVixyiKSp" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getAntimicrobialSusceptibilityOfPathogenFromAnyCultureTestAndSpecimenTypeDescription" />
      <node concept="3Tm1VV" id="6khVixyiKSr" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyiKSs" role="3clF45" />
      <node concept="3clFbS" id="6khVixyiKSt" role="3clF47">
        <node concept="3clFbF" id="6khVixyiL8A" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixyiL8_" role="3clFbG">
            <property role="Xl_RC" value="check susceptibility of detected pathogen for antimicrobial" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixyiKSu" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="6khVixyGKRG" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getAntimicrobialSusceptibilityOfPathogenFromAnyCultureTestAndSpecimenTypeForAntimicrobialKeyword" />
      <node concept="3Tm1VV" id="6khVixyGKRI" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyGKRJ" role="3clF45" />
      <node concept="3clFbS" id="6khVixyGKRK" role="3clF47">
        <node concept="3clFbF" id="6khVixyGLl1" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixyGLl0" role="3clFbG">
            <property role="Xl_RC" value="for" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixyGKRL" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="6khVixyjGfz" role="jymVt" />
    <node concept="3clFb_" id="6khVixyjGuJ" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getAntimicrobialSusceptibilityEqualToAlias" />
      <node concept="3Tm1VV" id="6khVixyjGuL" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyjGuM" role="3clF45" />
      <node concept="3clFbS" id="6khVixyjGuN" role="3clF47">
        <node concept="3clFbF" id="6khVixyjGNp" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixyjGNo" role="3clFbG">
            <property role="Xl_RC" value="=" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixyjGuO" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="6khVixyjGuP" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getAntimicrobialSusceptibilityEqualToDescription" />
      <node concept="3Tm1VV" id="6khVixyjGuR" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyjGuS" role="3clF45" />
      <node concept="3clFbS" id="6khVixyjGuT" role="3clF47">
        <node concept="3clFbF" id="6khVixyjGO5" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixyjGO4" role="3clFbG">
            <property role="Xl_RC" value="check that susceptibility is of a category" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixyjGuU" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="6khVixyjOok" role="jymVt" />
    <node concept="3clFb_" id="6khVixyjODD" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getSensitiveAntimicrobialSusceptibilityAlias" />
      <node concept="3Tm1VV" id="6khVixyjODF" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyjODG" role="3clF45" />
      <node concept="3clFbS" id="6khVixyjODH" role="3clF47">
        <node concept="3clFbF" id="6khVixyjP6k" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixyjP6j" role="3clFbG">
            <property role="Xl_RC" value="SENSITIVE" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixyjODI" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="6khVixyjODJ" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getSensitiveAntimicrobialSusceptibilityDescription" />
      <node concept="3Tm1VV" id="6khVixyjODL" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyjODM" role="3clF45" />
      <node concept="3clFbS" id="6khVixyjODN" role="3clF47">
        <node concept="3clFbF" id="6khVixyjP7B" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixyjP7A" role="3clFbG">
            <property role="Xl_RC" value="" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixyjODO" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="6khVixyjP7S" role="jymVt" />
    <node concept="3clFb_" id="6khVixyjODP" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getIntermediateAntimicrobialSusceptibilityAlias" />
      <node concept="3Tm1VV" id="6khVixyjODR" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyjODS" role="3clF45" />
      <node concept="3clFbS" id="6khVixyjODT" role="3clF47">
        <node concept="3clFbF" id="6khVixyjPv_" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixyjPv$" role="3clFbG">
            <property role="Xl_RC" value="INTERMEDIATE" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixyjODU" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="6khVixyjODV" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getIntermediateAntimicrobialSusceptibilityDescription" />
      <node concept="3Tm1VV" id="6khVixyjODX" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyjODY" role="3clF45" />
      <node concept="3clFbS" id="6khVixyjODZ" role="3clF47">
        <node concept="3clFbF" id="6khVixyjPwF" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixyjPwE" role="3clFbG">
            <property role="Xl_RC" value="" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixyjOE0" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="6khVixyjPwW" role="jymVt" />
    <node concept="3clFb_" id="6khVixyjOE1" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getResistantAntimicrobialSusceptibilityAlias" />
      <node concept="3Tm1VV" id="6khVixyjOE3" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyjOE4" role="3clF45" />
      <node concept="3clFbS" id="6khVixyjOE5" role="3clF47">
        <node concept="3clFbF" id="6khVixyjPSI" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixyjPSH" role="3clFbG">
            <property role="Xl_RC" value="RESISTANT" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixyjOE6" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="6khVixyjOE7" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getResistantAntimicrobialSusceptibilityDescription" />
      <node concept="3Tm1VV" id="6khVixyjOE9" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyjOEa" role="3clF45" />
      <node concept="3clFbS" id="6khVixyjOEb" role="3clF47">
        <node concept="3clFbF" id="6khVixyjPV2" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixyjPV1" role="3clFbG">
            <property role="Xl_RC" value="" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixyjOEc" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="4zMac8rUO1O">
    <property role="TrG5h" value="NoMicrobiologyDataTranslations" />
    <property role="3GE5qa" value="localization" />
    <node concept="3Tm1VV" id="4zMac8rUO1P" role="1B3o_S" />
    <node concept="3uibUv" id="4zMac8rUO2g" role="EKbjA">
      <ref role="3uigEE" node="4zMac8rUNtP" resolve="IMicrobiologyDataTranslations" />
    </node>
    <node concept="3clFb_" id="6khVixyaMJF" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getFindingsFromAnyCultureTestAlias" />
      <node concept="3Tm1VV" id="6khVixyaMJH" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyaMJI" role="3clF45" />
      <node concept="3clFbS" id="6khVixyaMJJ" role="3clF47">
        <node concept="3clFbF" id="6khVixyaMP8" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixyaMP7" role="3clFbG">
            <property role="Xl_RC" value="dyrkningsfunn" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixyaMJK" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="6khVixyaMJL" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getFindingsFromAnyCultureTestDescription" />
      <node concept="3Tm1VV" id="6khVixyaMJN" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyaMJO" role="3clF45" />
      <node concept="3clFbS" id="6khVixyaMJP" role="3clF47">
        <node concept="3clFbF" id="6khVixyaMUz" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixyaMUy" role="3clFbG">
            <property role="Xl_RC" value="sjekk funn fra dyrkninger" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixyaMJQ" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="2FjKBCR2IJ7" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getFindingsFromAnyCultureTestFromSpecimenTypeAlias" />
      <node concept="3Tm1VV" id="2FjKBCR2IJ9" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCR2IJa" role="3clF45" />
      <node concept="3clFbS" id="2FjKBCR2IJb" role="3clF47">
        <node concept="3clFbF" id="2FjKBCR2J8G" role="3cqZAp">
          <node concept="Xl_RD" id="2FjKBCR2J8F" role="3clFbG">
            <property role="Xl_RC" value="dyrkningsfunn fra" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="2FjKBCR2IJc" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="6khVixyb7vI" role="jymVt" />
    <node concept="3clFb_" id="6khVixybGyT" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getCultureFindingsIncludeAllOfAlias" />
      <node concept="3Tm1VV" id="6khVixybGyV" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixybGyW" role="3clF45" />
      <node concept="3clFbS" id="6khVixybGyX" role="3clF47">
        <node concept="3clFbF" id="6khVixybGHw" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixybGHv" role="3clFbG">
            <property role="Xl_RC" value="inkluderer alle" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixybGyY" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="6khVixybGyZ" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getCultureFindingsIncludeAllOfDescription" />
      <node concept="3Tm1VV" id="6khVixybGz1" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixybGz2" role="3clF45" />
      <node concept="3clFbS" id="6khVixybGz3" role="3clF47">
        <node concept="3clFbF" id="6khVixybGIA" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixybGI_" role="3clFbG">
            <property role="Xl_RC" value="sjekk at funn inkluderer alle listede patogener" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixybGz4" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="6khVixybGsg" role="jymVt" />
    <node concept="3clFb_" id="6khVixyb7qm" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getCultureFindingsIncludeAnyOfAlias" />
      <node concept="3Tm1VV" id="6khVixyb7qo" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyb7qp" role="3clF45" />
      <node concept="3clFbS" id="6khVixyb7qq" role="3clF47">
        <node concept="3clFbF" id="6khVixyb7CA" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixyb7C_" role="3clFbG">
            <property role="Xl_RC" value="inkluderer en av" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixyb7qr" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="6khVixyb7qs" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getCultureFindingsIncludeAnyOfDescription" />
      <node concept="3Tm1VV" id="6khVixyb7qu" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyb7qv" role="3clF45" />
      <node concept="3clFbS" id="6khVixyb7qw" role="3clF47">
        <node concept="3clFbF" id="6khVixyb7Di" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixyb7Dh" role="3clFbG">
            <property role="Xl_RC" value="sjekk at funn inkluderer en av listede patogener" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixyb7qx" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="6khVixybGUf" role="jymVt" />
    <node concept="3clFb_" id="6khVixybH31" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getCultureFindingsIncludePathogenAlias" />
      <node concept="3Tm1VV" id="6khVixybH33" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixybH34" role="3clF45" />
      <node concept="3clFbS" id="6khVixybH35" role="3clF47">
        <node concept="3clFbF" id="6khVixybHgz" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixybHgy" role="3clFbG">
            <property role="Xl_RC" value="inkluderer patogen" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixybH36" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="6khVixybH37" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getCultureFindingsIncludePathogenDescription" />
      <node concept="3Tm1VV" id="6khVixybH39" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixybH3a" role="3clF45" />
      <node concept="3clFbS" id="6khVixybH3b" role="3clF47">
        <node concept="3clFbF" id="6khVixybHhf" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixybHhe" role="3clFbG">
            <property role="Xl_RC" value="sjekk at funn inkluderer et spesifikt patogen" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixybH3c" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="6khVixyiNsg" role="jymVt" />
    <node concept="3clFb_" id="6khVixyiNA7" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getAntimicrobialSusceptibilityOfPathogenFromAnyCultureTestAndSpecimenTypeAlias" />
      <node concept="3Tm1VV" id="6khVixyiNA9" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyiNAa" role="3clF45" />
      <node concept="3clFbS" id="6khVixyiNAb" role="3clF47">
        <node concept="3clFbF" id="6khVixyiNSC" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixyiNSB" role="3clFbG">
            <property role="Xl_RC" value="resistensbestemmelse på" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixyiNAc" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="6khVixyiNAd" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getAntimicrobialSusceptibilityOfPathogenFromAnyCultureTestAndSpecimenTypeDescription" />
      <node concept="3Tm1VV" id="6khVixyiNAf" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyiNAg" role="3clF45" />
      <node concept="3clFbS" id="6khVixyiNAh" role="3clF47">
        <node concept="3clFbF" id="6khVixyiNUy" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixyiNUx" role="3clFbG">
            <property role="Xl_RC" value="sjekk resistensbestemmelse på patogen for medikament" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixyiNAi" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="2FjKBCQS93v" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getAntimicrobialSusceptibilityOfPathogenFromAnyCultureTestAndSpecimenTypeForAntimicrobialKeyword" />
      <node concept="3Tm1VV" id="2FjKBCQS93w" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCQS93x" role="3clF45" />
      <node concept="3clFbS" id="2FjKBCQS93y" role="3clF47">
        <node concept="3clFbF" id="2FjKBCQS93z" role="3cqZAp">
          <node concept="Xl_RD" id="2FjKBCQS93$" role="3clFbG">
            <property role="Xl_RC" value="for" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="2FjKBCQS93_" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="6khVixyjH75" role="jymVt" />
    <node concept="3clFb_" id="6khVixyjHnU" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getAntimicrobialSusceptibilityEqualToAlias" />
      <node concept="3Tm1VV" id="6khVixyjHnW" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyjHnX" role="3clF45" />
      <node concept="3clFbS" id="6khVixyjHnY" role="3clF47">
        <node concept="3clFbF" id="6khVixyjHXH" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixyjHXG" role="3clFbG">
            <property role="Xl_RC" value="=" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixyjHnZ" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="6khVixyjHo0" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getAntimicrobialSusceptibilityEqualToDescription" />
      <node concept="3Tm1VV" id="6khVixyjHo2" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyjHo3" role="3clF45" />
      <node concept="3clFbS" id="6khVixyjHo4" role="3clF47">
        <node concept="3clFbF" id="6khVixyjHYp" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixyjHYo" role="3clFbG">
            <property role="Xl_RC" value="sjekk at resistensbestemmelsen er av en kategori" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixyjHo5" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="6khVixyjR4F" role="jymVt" />
    <node concept="3clFb_" id="6khVixyjRBf" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getSensitiveAntimicrobialSusceptibilityAlias" />
      <node concept="3Tm1VV" id="6khVixyjRBh" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyjRBi" role="3clF45" />
      <node concept="3clFbS" id="6khVixyjRBj" role="3clF47">
        <node concept="3clFbF" id="6khVixyjSrk" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixyjSrj" role="3clFbG">
            <property role="Xl_RC" value="SENSITIV" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixyjRBk" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="6khVixyjRBl" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getSensitiveAntimicrobialSusceptibilityDescription" />
      <node concept="3Tm1VV" id="6khVixyjRBn" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyjRBo" role="3clF45" />
      <node concept="3clFbS" id="6khVixyjRBp" role="3clF47">
        <node concept="3clFbF" id="6khVixyjSsq" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixyjSsp" role="3clFbG">
            <property role="Xl_RC" value="" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixyjRBq" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="6khVixyjVDQ" role="jymVt" />
    <node concept="3clFb_" id="6khVixyjRBr" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getIntermediateAntimicrobialSusceptibilityAlias" />
      <node concept="3Tm1VV" id="6khVixyjRBt" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyjRBu" role="3clF45" />
      <node concept="3clFbS" id="6khVixyjRBv" role="3clF47">
        <node concept="3clFbF" id="6khVixyjVCn" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixyjVCm" role="3clFbG">
            <property role="Xl_RC" value="MODERAT" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixyjRBw" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="6khVixyjRBx" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getIntermediateAntimicrobialSusceptibilityDescription" />
      <node concept="3Tm1VV" id="6khVixyjRBz" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyjRB$" role="3clF45" />
      <node concept="3clFbS" id="6khVixyjRB_" role="3clF47">
        <node concept="3clFbF" id="6khVixyjW1_" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixyjW1$" role="3clFbG">
            <property role="Xl_RC" value="" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixyjRBA" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="6khVixyjW1Q" role="jymVt" />
    <node concept="3clFb_" id="6khVixyjRBB" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getResistantAntimicrobialSusceptibilityAlias" />
      <node concept="3Tm1VV" id="6khVixyjRBD" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyjRBE" role="3clF45" />
      <node concept="3clFbS" id="6khVixyjRBF" role="3clF47">
        <node concept="3clFbF" id="6khVixyjWpC" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixyjWpB" role="3clFbG">
            <property role="Xl_RC" value="RESISTENT" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixyjRBG" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="6khVixyjRBH" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getResistantAntimicrobialSusceptibilityDescription" />
      <node concept="3Tm1VV" id="6khVixyjRBJ" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyjRBK" role="3clF45" />
      <node concept="3clFbS" id="6khVixyjRBL" role="3clF47">
        <node concept="3clFbF" id="6khVixyjWqV" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixyjWqU" role="3clFbG">
            <property role="Xl_RC" value="" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixyjRBM" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
  </node>
</model>

