package no.uio.mLab.decisions.data.microbiology.translations;

/*Generated by MPS */

import no.uio.mLab.shared.translations.TranslationConfiguration;

public class MicrobiologyDataTranslationProvider {
  public static IMicrobiologyDataTranslations displayTranslations = init(TranslationConfiguration.displayLanguage);
  public static IMicrobiologyDataTranslations generationTranslations = init(TranslationConfiguration.generationLanguage);

  private static IMicrobiologyDataTranslations init(String language) {
    switch (language) {
      case "no":
        return new NoMicrobiologyDataTranslations();
      default:
        return new EnMicrobiologyDataTranslations();
    }
  }
}
