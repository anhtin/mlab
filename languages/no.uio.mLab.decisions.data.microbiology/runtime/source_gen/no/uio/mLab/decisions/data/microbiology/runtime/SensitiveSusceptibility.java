package no.uio.mLab.decisions.data.microbiology.runtime;

/*Generated by MPS */


public class SensitiveSusceptibility extends AntimicrobialSusceptibility {
  private static SensitiveSusceptibility instance;

  private SensitiveSusceptibility() {
  }

  public static SensitiveSusceptibility getInstance() {
    if (instance == null) {
      instance = new SensitiveSusceptibility();
    }
    return instance;
  }
}
