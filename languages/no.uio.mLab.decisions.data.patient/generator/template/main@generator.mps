<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:8221de78-5672-4394-9cdd-a4ce38951a3a(main@generator)">
  <persistence version="9" />
  <languages>
    <devkit ref="a2eb3a43-fcc2-4200-80dc-c60110c4862d(jetbrains.mps.devkit.templates)" />
  </languages>
  <imports>
    <import index="2tx7" ref="r:b9eb9a75-8428-4de0-95e9-56a3344fc1cd(no.uio.mLab.decisions.data.patient.structure)" />
    <import index="gb20" ref="r:1f3d7978-c4d8-49f2-9740-c6bcb0182cce(no.uio.mLab.decisions.data.patient.runtime)" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="2820489544401957797" name="jetbrains.mps.baseLanguage.structure.DefaultClassCreator" flags="nn" index="HV5vD">
        <reference id="2820489544401957798" name="classifier" index="HV5vE" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1212685548494" name="jetbrains.mps.baseLanguage.structure.ClassCreator" flags="nn" index="1pGfFk" />
    </language>
    <language id="b401a680-8325-4110-8fd3-84331ff25bef" name="jetbrains.mps.lang.generator">
      <concept id="1114706874351" name="jetbrains.mps.lang.generator.structure.CopySrcNodeMacro" flags="ln" index="29HgVG">
        <child id="1168024447342" name="sourceNodeQuery" index="3NFExx" />
      </concept>
      <concept id="1095416546421" name="jetbrains.mps.lang.generator.structure.MappingConfiguration" flags="ig" index="bUwia">
        <child id="1167328349397" name="reductionMappingRule" index="3acgRq" />
        <child id="1195502100749" name="preMappingScript" index="1puA0r" />
      </concept>
      <concept id="1177093525992" name="jetbrains.mps.lang.generator.structure.InlineTemplate_RuleConsequence" flags="lg" index="gft3U">
        <child id="1177093586806" name="templateNode" index="gfFT$" />
      </concept>
      <concept id="1167169188348" name="jetbrains.mps.lang.generator.structure.TemplateFunctionParameter_sourceNode" flags="nn" index="30H73N" />
      <concept id="1167169308231" name="jetbrains.mps.lang.generator.structure.BaseMappingRule" flags="ng" index="30H$t8">
        <reference id="1167169349424" name="applicableConcept" index="30HIoZ" />
      </concept>
      <concept id="1167327847730" name="jetbrains.mps.lang.generator.structure.Reduction_MappingRule" flags="lg" index="3aamgX">
        <child id="1169672767469" name="ruleConsequence" index="1lVwrX" />
      </concept>
      <concept id="1195499912406" name="jetbrains.mps.lang.generator.structure.MappingScript" flags="lg" index="1pmfR0">
        <property id="1195595592106" name="scriptKind" index="1v3f2W" />
        <child id="1195501105008" name="codeBlock" index="1pqMTA" />
      </concept>
      <concept id="1195500722856" name="jetbrains.mps.lang.generator.structure.MappingScript_CodeBlock" flags="in" index="1pplIY" />
      <concept id="1195502151594" name="jetbrains.mps.lang.generator.structure.MappingScriptReference" flags="lg" index="1puMqW">
        <reference id="1195502167610" name="mappingScript" index="1puQsG" />
      </concept>
      <concept id="1168024337012" name="jetbrains.mps.lang.generator.structure.SourceSubstituteMacro_SourceNodeQuery" flags="in" index="3NFfHV" />
    </language>
    <language id="760a0a8c-eabb-4521-8bfd-65db761a9ba3" name="jetbrains.mps.baseLanguage.logging">
      <concept id="6332851714983831325" name="jetbrains.mps.baseLanguage.logging.structure.MsgStatement" flags="ng" index="2xdQw9">
        <property id="6332851714983843871" name="severity" index="2xdLsb" />
        <child id="5721587534047265374" name="message" index="9lYJi" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="bUwia" id="3bOTPdWRfLj">
    <property role="TrG5h" value="dataPatientMain" />
    <node concept="1puMqW" id="6_a_4w5pvmt" role="1puA0r">
      <ref role="1puQsG" node="6_a_4w5pvmw" resolve="dataPatientScript" />
    </node>
    <node concept="3aamgX" id="ofcHXgas3t" role="3acgRq">
      <ref role="30HIoZ" to="2tx7:3bOTPdWSbEf" resolve="PatientAge" />
      <node concept="gft3U" id="ofcHXgas50" role="1lVwrX">
        <node concept="2ShNRf" id="ofcHXgas56" role="gfFT$">
          <node concept="HV5vD" id="ofcHXgas8m" role="2ShVmc">
            <ref role="HV5vE" to="gb20:ofcHXgar$B" resolve="PatientAge" />
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="4B5aqq6xzJl" role="3acgRq">
      <ref role="30HIoZ" to="2tx7:1mAGFBJ1sqP" resolve="PatientGender" />
      <node concept="gft3U" id="4B5aqq6xzK6" role="1lVwrX">
        <node concept="2ShNRf" id="4B5aqq6xzKc" role="gfFT$">
          <node concept="HV5vD" id="4B5aqq6xzNo" role="2ShVmc">
            <ref role="HV5vE" to="gb20:ofcHXgarFi" resolve="PatientGender" />
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="ofcHXg7dPr" role="3acgRq">
      <ref role="30HIoZ" to="2tx7:1mAGFBJbKj6" resolve="PatientGenderEqualsTo" />
      <node concept="gft3U" id="ofcHXg7dPv" role="1lVwrX">
        <node concept="2ShNRf" id="ofcHXg7dP_" role="gfFT$">
          <node concept="1pGfFk" id="ofcHXg7dYH" role="2ShVmc">
            <ref role="37wK5l" to="gb20:ofcHXg79Id" resolve="PatientGenderEqualTo" />
            <node concept="2ShNRf" id="ofcHXg7dYQ" role="37wK5m">
              <node concept="HV5vD" id="ofcHXg7e4x" role="2ShVmc">
                <ref role="HV5vE" to="gb20:ofcHXg79jx" resolve="GenderLiteral" />
              </node>
              <node concept="29HgVG" id="ofcHXg7e4O" role="lGtFl">
                <node concept="3NFfHV" id="ofcHXg7e4P" role="3NFExx">
                  <node concept="3clFbS" id="ofcHXg7e4Q" role="2VODD2">
                    <node concept="3clFbF" id="ofcHXg7e4W" role="3cqZAp">
                      <node concept="2OqwBi" id="ofcHXg7e4R" role="3clFbG">
                        <node concept="3TrEf2" id="ofcHXg7e4U" role="2OqNvi">
                          <ref role="3Tt5mk" to="2tx7:1mAGFBJbKjH" resolve="gender" />
                        </node>
                        <node concept="30H73N" id="ofcHXg7e4V" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="ofcHXg7eus" role="3acgRq">
      <ref role="30HIoZ" to="2tx7:1mAGFBJ1sr1" resolve="FemaleGender" />
      <node concept="gft3U" id="ofcHXg7eH7" role="1lVwrX">
        <node concept="2YIFZM" id="2XLt5KWSCqB" role="gfFT$">
          <ref role="37wK5l" to="gb20:2XLt5KWSAZc" resolve="getInstance" />
          <ref role="1Pybhc" to="gb20:ofcHXg7ddB" resolve="FemaleGender" />
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="ofcHXg7ecs" role="3acgRq">
      <ref role="30HIoZ" to="2tx7:1mAGFBJ1sr0" resolve="MaleGender" />
      <node concept="gft3U" id="ofcHXg7er3" role="1lVwrX">
        <node concept="2YIFZM" id="2XLt5KWSCra" role="gfFT$">
          <ref role="37wK5l" to="gb20:2XLt5KWSACo" resolve="getInstance" />
          <ref role="1Pybhc" to="gb20:ofcHXg7dcR" resolve="MaleGender" />
        </node>
      </node>
    </node>
  </node>
  <node concept="1pmfR0" id="6_a_4w5pvmw">
    <property role="TrG5h" value="dataPatientScript" />
    <property role="1v3f2W" value="pre_processing" />
    <node concept="1pplIY" id="6_a_4w5pvmx" role="1pqMTA">
      <node concept="3clFbS" id="6_a_4w5pvmy" role="2VODD2">
        <node concept="2xdQw9" id="6_a_4w5pvmG" role="3cqZAp">
          <property role="2xdLsb" value="debug" />
          <node concept="Xl_RD" id="6_a_4w5pvmI" role="9lYJi">
            <property role="Xl_RC" value="data.patient" />
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

