<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:b9eb9a75-8428-4de0-95e9-56a3344fc1cd(no.uio.mLab.decisions.data.patient.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="6" />
    <devkit ref="78434eb8-b0e5-444b-850d-e7c4ad2da9ab(jetbrains.mps.devkit.aspect.structure)" />
  </languages>
  <imports>
    <import index="7f9y" ref="r:790ac37e-42d3-4dd3-977c-e8fe9cc56a45(no.uio.mLab.decisions.core.structure)" />
    <import index="vbok" ref="r:6cc208d3-a370-49ca-a6ac-ded97805a924(no.uio.mLab.shared.structure)" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <property id="4628067390765956807" name="final" index="R5$K2" />
        <property id="4628067390765956802" name="abstract" index="R5$K7" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
      </concept>
      <concept id="1169125989551" name="jetbrains.mps.lang.structure.structure.InterfaceConceptDeclaration" flags="ig" index="PlHQZ">
        <child id="1169127546356" name="extends" index="PrDN$" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="3bOTPdWSbEf">
    <property role="EcuMT" value="3671813941977201295" />
    <property role="3GE5qa" value="base.data" />
    <property role="TrG5h" value="PatientAge" />
    <ref role="1TJDcQ" to="7f9y:1mAGFBKuWHF" resolve="TimeSpanData" />
    <node concept="PrWs8" id="1Hxyv4EV$ow" role="PzmwI">
      <ref role="PrY4T" node="1Hxyv4EVtNh" resolve="ITranslatablePatientConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="1mAGFBJ1sqP">
    <property role="EcuMT" value="1560130832579282613" />
    <property role="3GE5qa" value="base.data" />
    <property role="TrG5h" value="PatientGender" />
    <ref role="1TJDcQ" to="7f9y:1mAGFBLqAeU" resolve="NonBooleanDataValue" />
    <node concept="PrWs8" id="1Hxyv4EV$Sq" role="PzmwI">
      <ref role="PrY4T" node="1Hxyv4EVtNh" resolve="ITranslatablePatientConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="1mAGFBJ1sqX">
    <property role="EcuMT" value="1560130832579282621" />
    <property role="3GE5qa" value="base.literals" />
    <property role="R5$K7" value="true" />
    <property role="R5$K2" value="false" />
    <property role="TrG5h" value="GenderLiteral" />
    <ref role="1TJDcQ" to="7f9y:5Wfdz$0qdiH" resolve="Literal" />
  </node>
  <node concept="1TIwiD" id="1mAGFBJ1sr0">
    <property role="EcuMT" value="1560130832579282624" />
    <property role="3GE5qa" value="base.literals" />
    <property role="TrG5h" value="MaleGender" />
    <ref role="1TJDcQ" node="1mAGFBJ1sqX" resolve="GenderLiteral" />
    <node concept="PrWs8" id="1Hxyv4EVy1y" role="PzmwI">
      <ref role="PrY4T" node="1Hxyv4EVtNh" resolve="ITranslatablePatientConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="1mAGFBJ1sr1">
    <property role="EcuMT" value="1560130832579282625" />
    <property role="3GE5qa" value="base.literals" />
    <property role="TrG5h" value="FemaleGender" />
    <ref role="1TJDcQ" node="1mAGFBJ1sqX" resolve="GenderLiteral" />
    <node concept="PrWs8" id="1Hxyv4EVxsK" role="PzmwI">
      <ref role="PrY4T" node="1Hxyv4EVtNh" resolve="ITranslatablePatientConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="1mAGFBJbKj6">
    <property role="EcuMT" value="1560130832581985478" />
    <property role="3GE5qa" value="base.constraints" />
    <property role="TrG5h" value="PatientGenderEqualTo" />
    <ref role="1TJDcQ" node="1mAGFBJdO57" resolve="PatientGenderConstraint" />
    <node concept="1TJgyj" id="1mAGFBJbKjH" role="1TKVEi">
      <property role="IQ2ns" value="1560130832581985517" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="gender" />
      <ref role="20lvS9" node="1mAGFBJ1sqX" resolve="GenderLiteral" />
    </node>
    <node concept="PrWs8" id="1Hxyv4EVy$H" role="PzmwI">
      <ref role="PrY4T" node="1Hxyv4EVtNh" resolve="ITranslatablePatientConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="1mAGFBJdO57">
    <property role="EcuMT" value="1560130832582525255" />
    <property role="3GE5qa" value="base.constraints" />
    <property role="TrG5h" value="PatientGenderConstraint" />
    <property role="R5$K7" value="true" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" to="7f9y:1mAGFBKqQs5" resolve="Constraint" />
  </node>
  <node concept="PlHQZ" id="1Hxyv4EVtNh">
    <property role="EcuMT" value="1973009780665343185" />
    <property role="TrG5h" value="ITranslatablePatientConcept" />
    <node concept="PrWs8" id="1Hxyv4EVtNs" role="PrDN$">
      <ref role="PrY4T" to="vbok:5Wfdz$0vc1Z" resolve="ITranslatableConcept" />
    </node>
  </node>
</model>

