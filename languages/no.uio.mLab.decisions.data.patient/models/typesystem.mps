<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:471358d4-7080-4097-9907-9e5877538fcd(no.uio.mLab.decisions.data.patient.typesystem)">
  <persistence version="9" />
  <languages>
    <use id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem" version="1" />
    <devkit ref="00000000-0000-4000-0000-1de82b3a4936(jetbrains.mps.devkit.aspect.typesystem)" />
  </languages>
  <imports>
    <import index="2tx7" ref="r:b9eb9a75-8428-4de0-95e9-56a3344fc1cd(no.uio.mLab.decisions.data.patient.structure)" implicit="true" />
    <import index="9n9h" ref="r:22f74c93-b2ac-407a-a77f-6316653afdfa(no.uio.mLab.decisions.data.patient.behavior)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
      </concept>
    </language>
    <language id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem">
      <concept id="1175517767210" name="jetbrains.mps.lang.typesystem.structure.ReportErrorStatement" flags="nn" index="2MkqsV">
        <child id="1175517851849" name="errorString" index="2MkJ7o" />
      </concept>
      <concept id="1227096774658" name="jetbrains.mps.lang.typesystem.structure.MessageStatement" flags="ng" index="2OEH$v">
        <child id="1227096802790" name="nodeToReport" index="2OEOjV" />
      </concept>
      <concept id="1195213580585" name="jetbrains.mps.lang.typesystem.structure.AbstractCheckingRule" flags="ig" index="18hYwZ">
        <child id="1195213635060" name="body" index="18ibNy" />
      </concept>
      <concept id="1195214364922" name="jetbrains.mps.lang.typesystem.structure.NonTypesystemRule" flags="ig" index="18kY7G" />
      <concept id="1174642788531" name="jetbrains.mps.lang.typesystem.structure.ConceptReference" flags="ig" index="1YaCAy">
        <reference id="1174642800329" name="concept" index="1YaFvo" />
      </concept>
      <concept id="1174648085619" name="jetbrains.mps.lang.typesystem.structure.AbstractRule" flags="ng" index="1YuPPy">
        <child id="1174648101952" name="applicableNode" index="1YuTPh" />
      </concept>
      <concept id="1174650418652" name="jetbrains.mps.lang.typesystem.structure.ApplicableNodeReference" flags="nn" index="1YBJjd">
        <reference id="1174650432090" name="applicableNode" index="1YBMHb" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="7453996997717780434" name="jetbrains.mps.lang.smodel.structure.Node_GetSConceptOperation" flags="nn" index="2yIwOk" />
      <concept id="1171999116870" name="jetbrains.mps.lang.smodel.structure.Node_IsNullOperation" flags="nn" index="3w_OXm" />
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="18kY7G" id="1mAGFBK2RBB">
    <property role="TrG5h" value="check_PatientGenderEqualsTo" />
    <property role="3GE5qa" value="base.constraints" />
    <node concept="3clFbS" id="1mAGFBK2RBC" role="18ibNy">
      <node concept="3clFbJ" id="1mAGFBK2RBS" role="3cqZAp">
        <node concept="2OqwBi" id="1mAGFBK2Sk2" role="3clFbw">
          <node concept="2OqwBi" id="1mAGFBK2RN9" role="2Oq$k0">
            <node concept="1YBJjd" id="1mAGFBK2RC4" role="2Oq$k0">
              <ref role="1YBMHb" node="1mAGFBK2RBE" resolve="patientGenderEqualsTo" />
            </node>
            <node concept="3TrEf2" id="1mAGFBK2RYB" role="2OqNvi">
              <ref role="3Tt5mk" to="2tx7:1mAGFBJbKjH" resolve="gender" />
            </node>
          </node>
          <node concept="3w_OXm" id="1mAGFBK2SxV" role="2OqNvi" />
        </node>
        <node concept="3clFbS" id="1mAGFBK2RBU" role="3clFbx">
          <node concept="2MkqsV" id="1mAGFBK2SCJ" role="3cqZAp">
            <node concept="2OqwBi" id="1mAGFBK2THK" role="2MkJ7o">
              <node concept="2OqwBi" id="1mAGFBK2SO3" role="2Oq$k0">
                <node concept="1YBJjd" id="1mAGFBK2SCV" role="2Oq$k0">
                  <ref role="1YBMHb" node="1mAGFBK2RBE" resolve="patientGenderEqualsTo" />
                </node>
                <node concept="2yIwOk" id="1mAGFBK2Tjo" role="2OqNvi" />
              </node>
              <node concept="2qgKlT" id="1mAGFBK2UnY" role="2OqNvi">
                <ref role="37wK5l" to="9n9h:1mAGFBK2M5H" resolve="getMissingValueError" />
              </node>
            </node>
            <node concept="1YBJjd" id="1mAGFBK2Usb" role="2OEOjV">
              <ref role="1YBMHb" node="1mAGFBK2RBE" resolve="patientGenderEqualsTo" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="1mAGFBK2RBE" role="1YuTPh">
      <property role="TrG5h" value="patientGenderEqualsTo" />
      <ref role="1YaFvo" to="2tx7:1mAGFBJbKj6" resolve="PatientGenderEqualTo" />
    </node>
  </node>
</model>

