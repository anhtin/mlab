<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:cac6a8de-341e-464b-9501-83ed1e7323df(no.uio.mLab.data.patient.runtime)">
  <persistence version="9" />
  <languages>
    <use id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage" version="6" />
  </languages>
  <imports>
    <import index="9igz" ref="r:0ba33904-c124-449b-bde4-640f55a71953(no.uio.mLab.shared.translations)" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1465982738277781862" name="jetbrains.mps.baseLanguage.structure.PlaceholderMember" flags="ng" index="2tJIrI" />
      <concept id="1188207840427" name="jetbrains.mps.baseLanguage.structure.AnnotationInstance" flags="nn" index="2AHcQZ">
        <reference id="1188208074048" name="annotation" index="2AI5Lk" />
      </concept>
      <concept id="1188208481402" name="jetbrains.mps.baseLanguage.structure.HasAnnotation" flags="ng" index="2AJDlI">
        <child id="1188208488637" name="annotation" index="2AJF6D" />
      </concept>
      <concept id="2820489544401957797" name="jetbrains.mps.baseLanguage.structure.DefaultClassCreator" flags="nn" index="HV5vD">
        <reference id="2820489544401957798" name="classifier" index="HV5vE" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1070462154015" name="jetbrains.mps.baseLanguage.structure.StaticFieldDeclaration" flags="ig" index="Wx3nA">
        <property id="6468716278899126575" name="isVolatile" index="2dlcS1" />
        <property id="6468716278899125786" name="isTransient" index="2dld4O" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1081236700938" name="jetbrains.mps.baseLanguage.structure.StaticMethodDeclaration" flags="ig" index="2YIFZL" />
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu">
        <child id="1095933932569" name="implementedInterface" index="EKbjA" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <property id="1176718929932" name="isFinal" index="3TUv4t" />
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <property id="4276006055363816570" name="isSynchronized" index="od$2w" />
        <property id="1181808852946" name="isFinal" index="DiZV1" />
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123165" name="jetbrains.mps.baseLanguage.structure.InstanceMethodDeclaration" flags="ig" index="3clFb_">
        <property id="1178608670077" name="isAbstract" index="1EzhhJ" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1107461130800" name="jetbrains.mps.baseLanguage.structure.Classifier" flags="ng" index="3pOWGL">
        <child id="5375687026011219971" name="member" index="jymVt" unordered="true" />
      </concept>
      <concept id="7812454656619025416" name="jetbrains.mps.baseLanguage.structure.MethodDeclaration" flags="ng" index="1rXfSm">
        <property id="8355037393041754995" name="isNative" index="2aFKle" />
      </concept>
      <concept id="7812454656619025412" name="jetbrains.mps.baseLanguage.structure.LocalMethodCall" flags="nn" index="1rXfSq" />
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1107796713796" name="jetbrains.mps.baseLanguage.structure.Interface" flags="ig" index="3HP615" />
      <concept id="1163670490218" name="jetbrains.mps.baseLanguage.structure.SwitchStatement" flags="nn" index="3KaCP$">
        <child id="1163670592366" name="defaultBlock" index="3Kb1Dw" />
        <child id="1163670766145" name="expression" index="3KbGdf" />
        <child id="1163670772911" name="case" index="3KbHQx" />
      </concept>
      <concept id="1163670641947" name="jetbrains.mps.baseLanguage.structure.SwitchCase" flags="ng" index="3KbdKl">
        <child id="1163670677455" name="expression" index="3Kbmr1" />
        <child id="1163670683720" name="body" index="3Kbo56" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
      <concept id="1146644623116" name="jetbrains.mps.baseLanguage.structure.PrivateVisibility" flags="nn" index="3Tm6S6" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
      <concept id="709746936026466394" name="jetbrains.mps.lang.core.structure.ChildAttribute" flags="ng" index="3VBwX9">
        <property id="709746936026609031" name="linkId" index="3V$3ak" />
        <property id="709746936026609029" name="linkRole" index="3V$3am" />
      </concept>
      <concept id="4452961908202556907" name="jetbrains.mps.lang.core.structure.BaseCommentAttribute" flags="ng" index="1X3_iC">
        <child id="3078666699043039389" name="commentedNode" index="8Wnug" />
      </concept>
    </language>
  </registry>
  <node concept="3HP615" id="4zMac8rUNtP">
    <property role="TrG5h" value="IPatientTranslations" />
    <property role="3GE5qa" value="internationalization" />
    <node concept="1X3_iC" id="1mAGFBK2Mvl" role="lGtFl">
      <property role="3V$3am" value="member" />
      <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1107461130800/5375687026011219971" />
      <node concept="2tJIrI" id="1mAGFBK2MtQ" role="8Wnug" />
    </node>
    <node concept="3clFb_" id="3bOTPdWRg67" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getPatientAgeAlias" />
      <node concept="3clFbS" id="3bOTPdWRg6a" role="3clF47" />
      <node concept="3Tm1VV" id="3bOTPdWRg6b" role="1B3o_S" />
      <node concept="17QB3L" id="3bOTPdWRg5Q" role="3clF45" />
    </node>
    <node concept="3clFb_" id="3bOTPdWRg9v" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getPatientAgeDescription" />
      <node concept="3clFbS" id="3bOTPdWRg9y" role="3clF47" />
      <node concept="3Tm1VV" id="3bOTPdWRg9z" role="1B3o_S" />
      <node concept="17QB3L" id="3bOTPdWRg8X" role="3clF45" />
    </node>
    <node concept="3clFb_" id="1mAGFBJ0wgq" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getPatientAgeUnit" />
      <node concept="3clFbS" id="1mAGFBJ0wgt" role="3clF47" />
      <node concept="3Tm1VV" id="1mAGFBJ0wgu" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBJ0wfv" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="3bOTPdWRgah" role="jymVt" />
    <node concept="1X3_iC" id="1mAGFBK2Myn" role="lGtFl">
      <property role="3V$3am" value="member" />
      <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1107461130800/5375687026011219971" />
      <node concept="2tJIrI" id="1mAGFBK2MwQ" role="8Wnug" />
    </node>
    <node concept="3clFb_" id="3bOTPdWRgcb" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getPatientGenderAlias" />
      <node concept="3clFbS" id="3bOTPdWRgce" role="3clF47" />
      <node concept="3Tm1VV" id="3bOTPdWRgcf" role="1B3o_S" />
      <node concept="17QB3L" id="3bOTPdWRgbw" role="3clF45" />
    </node>
    <node concept="3clFb_" id="3bOTPdWRgfB" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getPatientGenderDescription" />
      <node concept="3clFbS" id="3bOTPdWRgfE" role="3clF47" />
      <node concept="3Tm1VV" id="3bOTPdWRgfF" role="1B3o_S" />
      <node concept="17QB3L" id="3bOTPdWRgeO" role="3clF45" />
    </node>
    <node concept="1X3_iC" id="1mAGFBK2M_t" role="lGtFl">
      <property role="3V$3am" value="member" />
      <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1107461130800/5375687026011219971" />
      <node concept="2tJIrI" id="1mAGFBK2MzU" role="8Wnug" />
    </node>
    <node concept="3clFb_" id="1mAGFBJbR0Q" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getPatientGenderEqualityAlias" />
      <node concept="3clFbS" id="1mAGFBJbR0T" role="3clF47" />
      <node concept="3Tm1VV" id="1mAGFBJbR0U" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBJbQZN" role="3clF45" />
    </node>
    <node concept="3clFb_" id="1mAGFBJbR8e" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getPatientGenderEqualityDescription" />
      <node concept="3clFbS" id="1mAGFBJbR8f" role="3clF47" />
      <node concept="3Tm1VV" id="1mAGFBJbR8g" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBJbR8h" role="3clF45" />
    </node>
    <node concept="3clFb_" id="1mAGFBK2MHv" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getPatientGenderMissingValueError" />
      <node concept="3clFbS" id="1mAGFBK2MHy" role="3clF47" />
      <node concept="3Tm1VV" id="1mAGFBK2MHz" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBK2MFP" role="3clF45" />
    </node>
    <node concept="1X3_iC" id="1mAGFBK2MCB" role="lGtFl">
      <property role="3V$3am" value="member" />
      <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1107461130800/5375687026011219971" />
      <node concept="2tJIrI" id="1mAGFBK2MB2" role="8Wnug" />
    </node>
    <node concept="3clFb_" id="1mAGFBJda20" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getMaleGenderAlias" />
      <node concept="3clFbS" id="1mAGFBJda23" role="3clF47" />
      <node concept="3Tm1VV" id="1mAGFBJda24" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBJda0H" role="3clF45" />
    </node>
    <node concept="3clFb_" id="1mAGFBJda7u" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getMaleGenderDescription" />
      <node concept="3clFbS" id="1mAGFBJda7x" role="3clF47" />
      <node concept="3Tm1VV" id="1mAGFBJda7y" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBJda63" role="3clF45" />
    </node>
    <node concept="3clFb_" id="1mAGFBJdaj$" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getFemaleGenderAlias" />
      <node concept="3clFbS" id="1mAGFBJdajB" role="3clF47" />
      <node concept="3Tm1VV" id="1mAGFBJdajC" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBJdabL" role="3clF45" />
    </node>
    <node concept="3clFb_" id="1mAGFBJdapM" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getFemaleGenderDescription" />
      <node concept="3clFbS" id="1mAGFBJdapP" role="3clF47" />
      <node concept="3Tm1VV" id="1mAGFBJdapQ" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBJdao7" role="3clF45" />
    </node>
    <node concept="3Tm1VV" id="4zMac8rUNtQ" role="1B3o_S" />
  </node>
  <node concept="312cEu" id="4zMac8rUNsN">
    <property role="TrG5h" value="PatientTranslationProvider" />
    <property role="3GE5qa" value="internationalization" />
    <node concept="Wx3nA" id="4zMac8rVBeK" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="TrG5h" value="displayTranslations" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm1VV" id="4zMac8rVBei" role="1B3o_S" />
      <node concept="3uibUv" id="ofcHXfQ51I" role="1tU5fm">
        <ref role="3uigEE" node="4zMac8rUNtP" resolve="IPatientTranslations" />
      </node>
      <node concept="1rXfSq" id="4zMac8rVBfh" role="33vP2m">
        <ref role="37wK5l" node="4zMac8rUNuE" resolve="init" />
        <node concept="10M0yZ" id="ofcHXfV2R0" role="37wK5m">
          <ref role="3cqZAo" to="9igz:5fXsrCVmzVm" resolve="displayLanguage" />
          <ref role="1PxDUh" to="9igz:5fXsrCVmxTN" resolve="TranslationConfiguration" />
        </node>
      </node>
    </node>
    <node concept="Wx3nA" id="1Hxyv4DUj3E" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="TrG5h" value="generationTranslations" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm1VV" id="1Hxyv4DUj3F" role="1B3o_S" />
      <node concept="3uibUv" id="ofcHXfQ526" role="1tU5fm">
        <ref role="3uigEE" node="4zMac8rUNtP" resolve="IPatientTranslations" />
      </node>
      <node concept="1rXfSq" id="1Hxyv4DUj3H" role="33vP2m">
        <ref role="37wK5l" node="4zMac8rUNuE" resolve="init" />
        <node concept="10M0yZ" id="ofcHXfV2RL" role="37wK5m">
          <ref role="3cqZAo" to="9igz:5fXsrCVmzVN" resolve="generationLanguage" />
          <ref role="1PxDUh" to="9igz:5fXsrCVmxTN" resolve="TranslationConfiguration" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="4zMac8rUNtp" role="jymVt" />
    <node concept="2YIFZL" id="4zMac8rUNuE" role="jymVt">
      <property role="TrG5h" value="init" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="4zMac8rUNuH" role="3clF47">
        <node concept="3KaCP$" id="4zMac8rUNv7" role="3cqZAp">
          <node concept="3KbdKl" id="4zMac8rUNXu" role="3KbHQx">
            <node concept="Xl_RD" id="4zMac8rUNXU" role="3Kbmr1">
              <property role="Xl_RC" value="no" />
            </node>
            <node concept="3clFbS" id="4zMac8rUNXw" role="3Kbo56">
              <node concept="3cpWs6" id="4zMac8rUNYt" role="3cqZAp">
                <node concept="2ShNRf" id="4zMac8rUO3y" role="3cqZAk">
                  <node concept="HV5vD" id="4zMac8rUP5z" role="2ShVmc">
                    <ref role="HV5vE" node="4zMac8rUO1O" resolve="NoPatientTranslations" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="4zMac8rUNYX" role="3Kb1Dw">
            <node concept="3cpWs6" id="4zMac8rUNZC" role="3cqZAp">
              <node concept="2ShNRf" id="4zMac8rUP6U" role="3cqZAk">
                <node concept="HV5vD" id="4zMac8rUPap" role="2ShVmc">
                  <ref role="HV5vE" node="4zMac8rUO0_" resolve="EnPatientTranslations" />
                </node>
              </node>
            </node>
          </node>
          <node concept="37vLTw" id="1Hxyv4DUjd8" role="3KbGdf">
            <ref role="3cqZAo" node="1Hxyv4DUj7k" resolve="language" />
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="4zMac8rW809" role="1B3o_S" />
      <node concept="3uibUv" id="4zMac8rUNuz" role="3clF45">
        <ref role="3uigEE" node="4zMac8rUNtP" resolve="IPatientTranslations" />
      </node>
      <node concept="37vLTG" id="1Hxyv4DUj7k" role="3clF46">
        <property role="TrG5h" value="language" />
        <node concept="17QB3L" id="1Hxyv4DUj7j" role="1tU5fm" />
      </node>
    </node>
    <node concept="3Tm1VV" id="4zMac8rUNsO" role="1B3o_S" />
  </node>
  <node concept="312cEu" id="4zMac8rUO0_">
    <property role="TrG5h" value="EnPatientTranslations" />
    <property role="3GE5qa" value="localization" />
    <node concept="3Tm1VV" id="4zMac8rUO0A" role="1B3o_S" />
    <node concept="3uibUv" id="4zMac8rUO1F" role="EKbjA">
      <ref role="3uigEE" node="4zMac8rUNtP" resolve="IPatientTranslations" />
    </node>
    <node concept="1X3_iC" id="1mAGFBK2P8B" role="lGtFl">
      <property role="3V$3am" value="member" />
      <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1107461130800/5375687026011219971" />
      <node concept="2tJIrI" id="3bOTPdWRgKJ" role="8Wnug" />
    </node>
    <node concept="3clFb_" id="3bOTPdWRgBc" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getPatientAgeAlias" />
      <node concept="3Tm1VV" id="3bOTPdWRgBe" role="1B3o_S" />
      <node concept="17QB3L" id="3bOTPdWRgBf" role="3clF45" />
      <node concept="3clFbS" id="3bOTPdWRgBg" role="3clF47">
        <node concept="3clFbF" id="3bOTPdWRgSn" role="3cqZAp">
          <node concept="Xl_RD" id="3bOTPdWRgSm" role="3clFbG">
            <property role="Xl_RC" value="patient age" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="3bOTPdWRgBh" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="3bOTPdWRgBi" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getPatientAgeDescription" />
      <node concept="3Tm1VV" id="3bOTPdWRgBk" role="1B3o_S" />
      <node concept="17QB3L" id="3bOTPdWRgBl" role="3clF45" />
      <node concept="3clFbS" id="3bOTPdWRgBm" role="3clF47">
        <node concept="3clFbF" id="3bOTPdWRgTE" role="3cqZAp">
          <node concept="Xl_RD" id="3bOTPdWRgTD" role="3clFbG">
            <property role="Xl_RC" value="check patient's age at the time of sampling" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="3bOTPdWRgBn" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="1mAGFBJ0wsy" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getPatientAgeUnit" />
      <node concept="3Tm1VV" id="1mAGFBJ0ws$" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBJ0ws_" role="3clF45" />
      <node concept="3clFbS" id="1mAGFBJ0wsA" role="3clF47">
        <node concept="3clFbF" id="1mAGFBJ0w_L" role="3cqZAp">
          <node concept="Xl_RD" id="1mAGFBJ0w_K" role="3clFbG">
            <property role="Xl_RC" value="years" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="1mAGFBJ0wsB" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="3bOTPdWRgVm" role="jymVt" />
    <node concept="1X3_iC" id="1mAGFBK2Pwn" role="lGtFl">
      <property role="3V$3am" value="member" />
      <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1107461130800/5375687026011219971" />
      <node concept="2tJIrI" id="1mAGFBK2Pkv" role="8Wnug" />
    </node>
    <node concept="3clFb_" id="3bOTPdWRgBo" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getPatientGenderAlias" />
      <node concept="3Tm1VV" id="3bOTPdWRgBq" role="1B3o_S" />
      <node concept="17QB3L" id="3bOTPdWRgBr" role="3clF45" />
      <node concept="3clFbS" id="3bOTPdWRgBs" role="3clF47">
        <node concept="3clFbF" id="3bOTPdWRh2f" role="3cqZAp">
          <node concept="Xl_RD" id="3bOTPdWRh2e" role="3clFbG">
            <property role="Xl_RC" value="patient gender" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="3bOTPdWRgBt" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="3bOTPdWRgBu" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getPatientGenderDescription" />
      <node concept="3Tm1VV" id="3bOTPdWRgBw" role="1B3o_S" />
      <node concept="17QB3L" id="3bOTPdWRgBx" role="3clF45" />
      <node concept="3clFbS" id="3bOTPdWRgBy" role="3clF47">
        <node concept="3clFbF" id="3bOTPdWRh3l" role="3cqZAp">
          <node concept="Xl_RD" id="3bOTPdWRh3k" role="3clFbG">
            <property role="Xl_RC" value="check patient's gender" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="3bOTPdWRgBz" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="1X3_iC" id="1mAGFBK2PSb" role="lGtFl">
      <property role="3V$3am" value="member" />
      <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1107461130800/5375687026011219971" />
      <node concept="2tJIrI" id="1mAGFBK2PGh" role="8Wnug" />
    </node>
    <node concept="3clFb_" id="1mAGFBJbSjy" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getPatientGenderEqualityAlias" />
      <node concept="3Tm1VV" id="1mAGFBJbSj$" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBJbSj_" role="3clF45" />
      <node concept="3clFbS" id="1mAGFBJbSjA" role="3clF47">
        <node concept="3clFbF" id="1mAGFBJbSvp" role="3cqZAp">
          <node concept="Xl_RD" id="1mAGFBJbSvo" role="3clFbG">
            <property role="Xl_RC" value="=" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="1mAGFBJbSjB" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="1mAGFBJbSjC" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getPatientGenderEqualityDescription" />
      <node concept="3Tm1VV" id="1mAGFBJbSjE" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBJbSjF" role="3clF45" />
      <node concept="3clFbS" id="1mAGFBJbSjG" role="3clF47">
        <node concept="3clFbF" id="1mAGFBJbSw5" role="3cqZAp">
          <node concept="Xl_RD" id="1mAGFBJbSw4" role="3clFbG">
            <property role="Xl_RC" value="check patient's gender" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="1mAGFBJbSjH" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="1mAGFBK2Qs4" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getPatientGenderMissingValueError" />
      <node concept="3Tm1VV" id="1mAGFBK2Qs6" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBK2Qs7" role="3clF45" />
      <node concept="3clFbS" id="1mAGFBK2Qs8" role="3clF47">
        <node concept="3clFbF" id="1mAGFBK2QFE" role="3cqZAp">
          <node concept="Xl_RD" id="1mAGFBK2QFD" role="3clFbG">
            <property role="Xl_RC" value="missing gender" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="1mAGFBK2Qs9" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="1X3_iC" id="1mAGFBK2Qg3" role="lGtFl">
      <property role="3V$3am" value="member" />
      <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1107461130800/5375687026011219971" />
      <node concept="2tJIrI" id="1mAGFBK2Q47" role="8Wnug" />
    </node>
    <node concept="3clFb_" id="1mAGFBJdbbm" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getMaleGenderAlias" />
      <node concept="3Tm1VV" id="1mAGFBJdbbo" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBJdbbp" role="3clF45" />
      <node concept="3clFbS" id="1mAGFBJdbbq" role="3clF47">
        <node concept="3clFbF" id="1mAGFBJdbst" role="3cqZAp">
          <node concept="Xl_RD" id="1mAGFBJdbss" role="3clFbG">
            <property role="Xl_RC" value="MALE" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="1mAGFBJdbbr" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="1mAGFBJdbbs" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getMaleGenderDescription" />
      <node concept="3Tm1VV" id="1mAGFBJdbbu" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBJdbbv" role="3clF45" />
      <node concept="3clFbS" id="1mAGFBJdbbw" role="3clF47">
        <node concept="3clFbF" id="1mAGFBJdbtm" role="3cqZAp">
          <node concept="Xl_RD" id="1mAGFBJdbtl" role="3clFbG" />
        </node>
      </node>
      <node concept="2AHcQZ" id="1mAGFBJdbbx" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="1mAGFBJdbby" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getFemaleGenderAlias" />
      <node concept="3Tm1VV" id="1mAGFBJdbb$" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBJdbb_" role="3clF45" />
      <node concept="3clFbS" id="1mAGFBJdbbA" role="3clF47">
        <node concept="3clFbF" id="1mAGFBJdbu2" role="3cqZAp">
          <node concept="Xl_RD" id="1mAGFBJdbu1" role="3clFbG">
            <property role="Xl_RC" value="FEMALE" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="1mAGFBJdbbB" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="1mAGFBJdbbC" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getFemaleGenderDescription" />
      <node concept="3Tm1VV" id="1mAGFBJdbbE" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBJdbbF" role="3clF45" />
      <node concept="3clFbS" id="1mAGFBJdbbG" role="3clF47">
        <node concept="3clFbF" id="1mAGFBJdbuV" role="3cqZAp">
          <node concept="Xl_RD" id="1mAGFBJdbuU" role="3clFbG">
            <property role="Xl_RC" value="" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="1mAGFBJdbbH" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="4zMac8rUO1O">
    <property role="TrG5h" value="NoPatientTranslations" />
    <property role="3GE5qa" value="localization" />
    <node concept="3Tm1VV" id="4zMac8rUO1P" role="1B3o_S" />
    <node concept="3uibUv" id="4zMac8rUO2g" role="EKbjA">
      <ref role="3uigEE" node="4zMac8rUNtP" resolve="IPatientTranslations" />
    </node>
    <node concept="1X3_iC" id="1mAGFBK2Nfl" role="lGtFl">
      <property role="3V$3am" value="member" />
      <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1107461130800/5375687026011219971" />
      <node concept="2tJIrI" id="3bOTPdWRhzc" role="8Wnug" />
    </node>
    <node concept="3clFb_" id="3bOTPdWRhoC" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getPatientAgeAlias" />
      <node concept="3Tm1VV" id="3bOTPdWRhoE" role="1B3o_S" />
      <node concept="17QB3L" id="3bOTPdWRhoF" role="3clF45" />
      <node concept="3clFbS" id="3bOTPdWRhoG" role="3clF47">
        <node concept="3clFbF" id="3bOTPdWRhDN" role="3cqZAp">
          <node concept="Xl_RD" id="3bOTPdWRhDM" role="3clFbG">
            <property role="Xl_RC" value="pasientalder" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="3bOTPdWRhoH" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="3bOTPdWRhoI" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getPatientAgeDescription" />
      <node concept="3Tm1VV" id="3bOTPdWRhoK" role="1B3o_S" />
      <node concept="17QB3L" id="3bOTPdWRhoL" role="3clF45" />
      <node concept="3clFbS" id="3bOTPdWRhoM" role="3clF47">
        <node concept="3clFbF" id="3bOTPdWRhKV" role="3cqZAp">
          <node concept="Xl_RD" id="3bOTPdWRhKU" role="3clFbG">
            <property role="Xl_RC" value="sjekk pasienten sin alder ved prøvetaking" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="3bOTPdWRhoN" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="1mAGFBJ0wIb" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getPatientAgeUnit" />
      <node concept="3Tm1VV" id="1mAGFBJ0wId" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBJ0wIe" role="3clF45" />
      <node concept="3clFbS" id="1mAGFBJ0wIf" role="3clF47">
        <node concept="3clFbF" id="1mAGFBJ0wRq" role="3cqZAp">
          <node concept="Xl_RD" id="1mAGFBJ0wRp" role="3clFbG">
            <property role="Xl_RC" value="år" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="1mAGFBJ0wIg" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="3bOTPdWRhEh" role="jymVt" />
    <node concept="1X3_iC" id="1mAGFBK2Nsv" role="lGtFl">
      <property role="3V$3am" value="member" />
      <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1107461130800/5375687026011219971" />
      <node concept="2tJIrI" id="1mAGFBK2Nrd" role="8Wnug" />
    </node>
    <node concept="3clFb_" id="3bOTPdWRhoO" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getPatientGenderAlias" />
      <node concept="3Tm1VV" id="3bOTPdWRhoQ" role="1B3o_S" />
      <node concept="17QB3L" id="3bOTPdWRhoR" role="3clF45" />
      <node concept="3clFbS" id="3bOTPdWRhoS" role="3clF47">
        <node concept="3clFbF" id="3bOTPdWRhMP" role="3cqZAp">
          <node concept="Xl_RD" id="3bOTPdWRhMO" role="3clFbG">
            <property role="Xl_RC" value="pasientkjønn" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="3bOTPdWRhoT" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="3bOTPdWRhoU" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getPatientGenderDescription" />
      <node concept="3Tm1VV" id="3bOTPdWRhoW" role="1B3o_S" />
      <node concept="17QB3L" id="3bOTPdWRhoX" role="3clF45" />
      <node concept="3clFbS" id="3bOTPdWRhoY" role="3clF47">
        <node concept="3clFbF" id="3bOTPdWRhOW" role="3cqZAp">
          <node concept="Xl_RD" id="3bOTPdWRhOV" role="3clFbG">
            <property role="Xl_RC" value="kjønnet til pasienten" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="3bOTPdWRhoZ" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="1X3_iC" id="1mAGFBK2NOj" role="lGtFl">
      <property role="3V$3am" value="member" />
      <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1107461130800/5375687026011219971" />
      <node concept="2tJIrI" id="1mAGFBK2NCp" role="8Wnug" />
    </node>
    <node concept="3clFb_" id="1mAGFBJbRwL" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getPatientGenderEqualityAlias" />
      <node concept="3Tm1VV" id="1mAGFBJbRwN" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBJbRwO" role="3clF45" />
      <node concept="3clFbS" id="1mAGFBJbRwP" role="3clF47">
        <node concept="3clFbF" id="1mAGFBJbRGC" role="3cqZAp">
          <node concept="Xl_RD" id="1mAGFBJbRGB" role="3clFbG">
            <property role="Xl_RC" value="=" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="1mAGFBJbRwQ" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="1mAGFBJbRwR" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getPatientGenderEqualityDescription" />
      <node concept="3Tm1VV" id="1mAGFBJbRwT" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBJbRwU" role="3clF45" />
      <node concept="3clFbS" id="1mAGFBJbRwV" role="3clF47">
        <node concept="3clFbF" id="1mAGFBJbRHk" role="3cqZAp">
          <node concept="Xl_RD" id="1mAGFBJbRHj" role="3clFbG">
            <property role="Xl_RC" value="sjekk om pasientens kjønn" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="1mAGFBJbRwW" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="1mAGFBK2Ooc" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getPatientGenderMissingValueError" />
      <node concept="3Tm1VV" id="1mAGFBK2Ooe" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBK2Oof" role="3clF45" />
      <node concept="3clFbS" id="1mAGFBK2Oog" role="3clF47">
        <node concept="3clFbF" id="1mAGFBK2OBM" role="3cqZAp">
          <node concept="Xl_RD" id="1mAGFBK2OBL" role="3clFbG">
            <property role="Xl_RC" value="mangler kjønn" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="1mAGFBK2Ooh" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="1X3_iC" id="1mAGFBK2Ocb" role="lGtFl">
      <property role="3V$3am" value="member" />
      <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1107461130800/5375687026011219971" />
      <node concept="2tJIrI" id="1mAGFBK2O0f" role="8Wnug" />
    </node>
    <node concept="3clFb_" id="1mAGFBJdaEv" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getMaleGenderAlias" />
      <node concept="3Tm1VV" id="1mAGFBJdaEx" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBJdaEy" role="3clF45" />
      <node concept="3clFbS" id="1mAGFBJdaEz" role="3clF47">
        <node concept="3clFbF" id="1mAGFBJdaVA" role="3cqZAp">
          <node concept="Xl_RD" id="1mAGFBJdaV_" role="3clFbG">
            <property role="Xl_RC" value="MANN" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="1mAGFBJdaE$" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="1mAGFBJdaE_" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getMaleGenderDescription" />
      <node concept="3Tm1VV" id="1mAGFBJdaEB" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBJdaEC" role="3clF45" />
      <node concept="3clFbS" id="1mAGFBJdaED" role="3clF47">
        <node concept="3clFbF" id="1mAGFBJdaWi" role="3cqZAp">
          <node concept="Xl_RD" id="1mAGFBJdaWh" role="3clFbG">
            <property role="Xl_RC" value="" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="1mAGFBJdaEE" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="1mAGFBJdaEF" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getFemaleGenderAlias" />
      <node concept="3Tm1VV" id="1mAGFBJdaEH" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBJdaEI" role="3clF45" />
      <node concept="3clFbS" id="1mAGFBJdaEJ" role="3clF47">
        <node concept="3clFbF" id="1mAGFBJdaWL" role="3cqZAp">
          <node concept="Xl_RD" id="1mAGFBJdaWK" role="3clFbG">
            <property role="Xl_RC" value="KVINNE" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="1mAGFBJdaEK" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="1mAGFBJdaEL" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getFemaleGenderDescription" />
      <node concept="3Tm1VV" id="1mAGFBJdaEN" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBJdaEO" role="3clF45" />
      <node concept="3clFbS" id="1mAGFBJdaEP" role="3clF47">
        <node concept="3clFbF" id="1mAGFBJdaXt" role="3cqZAp">
          <node concept="Xl_RD" id="1mAGFBJdaXs" role="3clFbG">
            <property role="Xl_RC" value="" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="1mAGFBJdaEQ" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
  </node>
</model>

