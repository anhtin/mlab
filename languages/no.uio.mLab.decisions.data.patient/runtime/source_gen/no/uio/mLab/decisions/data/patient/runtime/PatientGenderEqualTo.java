package no.uio.mLab.decisions.data.patient.runtime;

/*Generated by MPS */

import no.uio.mLab.decisions.core.runtime.Constraint;
import java.util.Objects;

public class PatientGenderEqualTo extends Constraint {
  private GenderLiteral gender;

  public PatientGenderEqualTo(GenderLiteral genderConstraint) {
    this.gender = genderConstraint;
  }

  public GenderLiteral getGender() {
    return this.gender;
  }

  @Override
  public boolean isSatisfiedBy(Object value) {
    if (value instanceof GenderLiteral) {
      return this.gender.equals(value);
    }
    return false;
  }


  @Override
  public int hashCode() {
    return Objects.hash(this.getClass(), this.gender);
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
      return true;
    }
    if (object == null || this.getClass() != object.getClass()) {
      return false;
    }
    PatientGenderEqualTo other = (PatientGenderEqualTo) object;
    return Objects.equals(this.gender, other.gender);
  }

}
