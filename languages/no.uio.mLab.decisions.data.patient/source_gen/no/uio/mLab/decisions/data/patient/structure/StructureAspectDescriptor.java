package no.uio.mLab.decisions.data.patient.structure;

/*Generated by MPS */

import jetbrains.mps.smodel.runtime.BaseStructureAspectDescriptor;
import jetbrains.mps.smodel.runtime.ConceptDescriptor;
import java.util.Collection;
import java.util.Arrays;
import org.jetbrains.annotations.Nullable;
import jetbrains.mps.smodel.adapter.ids.SConceptId;
import org.jetbrains.mps.openapi.language.SAbstractConcept;
import jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2;

public class StructureAspectDescriptor extends BaseStructureAspectDescriptor {
  /*package*/ final ConceptDescriptor myConceptFemaleGender = createDescriptorForFemaleGender();
  /*package*/ final ConceptDescriptor myConceptGenderLiteral = createDescriptorForGenderLiteral();
  /*package*/ final ConceptDescriptor myConceptITranslatablePatientConcept = createDescriptorForITranslatablePatientConcept();
  /*package*/ final ConceptDescriptor myConceptMaleGender = createDescriptorForMaleGender();
  /*package*/ final ConceptDescriptor myConceptPatientAge = createDescriptorForPatientAge();
  /*package*/ final ConceptDescriptor myConceptPatientGender = createDescriptorForPatientGender();
  /*package*/ final ConceptDescriptor myConceptPatientGenderConstraint = createDescriptorForPatientGenderConstraint();
  /*package*/ final ConceptDescriptor myConceptPatientGenderEqualTo = createDescriptorForPatientGenderEqualTo();
  private final LanguageConceptSwitch myIndexSwitch;

  public StructureAspectDescriptor() {
    myIndexSwitch = new LanguageConceptSwitch();
  }

  @Override
  public Collection<ConceptDescriptor> getDescriptors() {
    return Arrays.asList(myConceptFemaleGender, myConceptGenderLiteral, myConceptITranslatablePatientConcept, myConceptMaleGender, myConceptPatientAge, myConceptPatientGender, myConceptPatientGenderConstraint, myConceptPatientGenderEqualTo);
  }

  @Override
  @Nullable
  public ConceptDescriptor getDescriptor(SConceptId id) {
    switch (myIndexSwitch.index(id)) {
      case LanguageConceptSwitch.FemaleGender:
        return myConceptFemaleGender;
      case LanguageConceptSwitch.GenderLiteral:
        return myConceptGenderLiteral;
      case LanguageConceptSwitch.ITranslatablePatientConcept:
        return myConceptITranslatablePatientConcept;
      case LanguageConceptSwitch.MaleGender:
        return myConceptMaleGender;
      case LanguageConceptSwitch.PatientAge:
        return myConceptPatientAge;
      case LanguageConceptSwitch.PatientGender:
        return myConceptPatientGender;
      case LanguageConceptSwitch.PatientGenderConstraint:
        return myConceptPatientGenderConstraint;
      case LanguageConceptSwitch.PatientGenderEqualTo:
        return myConceptPatientGenderEqualTo;
      default:
        return null;
    }
  }

  /*package*/ int internalIndex(SAbstractConcept c) {
    return myIndexSwitch.index(c);
  }

  private static ConceptDescriptor createDescriptorForFemaleGender() {
    ConceptDescriptorBuilder2 b = new ConceptDescriptorBuilder2("no.uio.mLab.decisions.data.patient", "FemaleGender", 0xf130674d23fb4e9bL, 0xa200548653f5fd0bL, 0x15a6b2b9ef05c6c1L);
    b.class_(false, false, false);
    b.super_("no.uio.mLab.decisions.data.patient.structure.GenderLiteral", 0xf130674d23fb4e9bL, 0xa200548653f5fd0bL, 0x15a6b2b9ef05c6bdL);
    b.parent(0xf130674d23fb4e9bL, 0xa200548653f5fd0bL, 0x1b6189f12aeddcd1L);
    b.origin("r:b9eb9a75-8428-4de0-95e9-56a3344fc1cd(no.uio.mLab.decisions.data.patient.structure)/1560130832579282625");
    b.version(2);
    return b.create();
  }
  private static ConceptDescriptor createDescriptorForGenderLiteral() {
    ConceptDescriptorBuilder2 b = new ConceptDescriptorBuilder2("no.uio.mLab.decisions.data.patient", "GenderLiteral", 0xf130674d23fb4e9bL, 0xa200548653f5fd0bL, 0x15a6b2b9ef05c6bdL);
    b.class_(false, true, false);
    b.super_("no.uio.mLab.decisions.core.structure.Literal", 0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x5f0f36390068d4adL);
    b.origin("r:b9eb9a75-8428-4de0-95e9-56a3344fc1cd(no.uio.mLab.decisions.data.patient.structure)/1560130832579282621");
    b.version(2);
    return b.create();
  }
  private static ConceptDescriptor createDescriptorForITranslatablePatientConcept() {
    ConceptDescriptorBuilder2 b = new ConceptDescriptorBuilder2("no.uio.mLab.decisions.data.patient", "ITranslatablePatientConcept", 0xf130674d23fb4e9bL, 0xa200548653f5fd0bL, 0x1b6189f12aeddcd1L);
    b.interface_();
    b.parent(0x6adcef385cdc48dcL, 0xb225be76276615fdL, 0x5f0f3639007cc07fL);
    b.origin("r:b9eb9a75-8428-4de0-95e9-56a3344fc1cd(no.uio.mLab.decisions.data.patient.structure)/1973009780665343185");
    b.version(2);
    return b.create();
  }
  private static ConceptDescriptor createDescriptorForMaleGender() {
    ConceptDescriptorBuilder2 b = new ConceptDescriptorBuilder2("no.uio.mLab.decisions.data.patient", "MaleGender", 0xf130674d23fb4e9bL, 0xa200548653f5fd0bL, 0x15a6b2b9ef05c6c0L);
    b.class_(false, false, false);
    b.super_("no.uio.mLab.decisions.data.patient.structure.GenderLiteral", 0xf130674d23fb4e9bL, 0xa200548653f5fd0bL, 0x15a6b2b9ef05c6bdL);
    b.parent(0xf130674d23fb4e9bL, 0xa200548653f5fd0bL, 0x1b6189f12aeddcd1L);
    b.origin("r:b9eb9a75-8428-4de0-95e9-56a3344fc1cd(no.uio.mLab.decisions.data.patient.structure)/1560130832579282624");
    b.version(2);
    return b.create();
  }
  private static ConceptDescriptor createDescriptorForPatientAge() {
    ConceptDescriptorBuilder2 b = new ConceptDescriptorBuilder2("no.uio.mLab.decisions.data.patient", "PatientAge", 0xf130674d23fb4e9bL, 0xa200548653f5fd0bL, 0x32f4e7537ce0ba8fL);
    b.class_(false, false, false);
    b.super_("no.uio.mLab.decisions.core.structure.TimeSpanData", 0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x15a6b2b9f07bcb6bL);
    b.parent(0xf130674d23fb4e9bL, 0xa200548653f5fd0bL, 0x1b6189f12aeddcd1L);
    b.origin("r:b9eb9a75-8428-4de0-95e9-56a3344fc1cd(no.uio.mLab.decisions.data.patient.structure)/3671813941977201295");
    b.version(2);
    return b.create();
  }
  private static ConceptDescriptor createDescriptorForPatientGender() {
    ConceptDescriptorBuilder2 b = new ConceptDescriptorBuilder2("no.uio.mLab.decisions.data.patient", "PatientGender", 0xf130674d23fb4e9bL, 0xa200548653f5fd0bL, 0x15a6b2b9ef05c6b5L);
    b.class_(false, false, false);
    b.super_("no.uio.mLab.decisions.core.structure.NonBooleanDataValue", 0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x15a6b2b9f16a63baL);
    b.parent(0xf130674d23fb4e9bL, 0xa200548653f5fd0bL, 0x1b6189f12aeddcd1L);
    b.origin("r:b9eb9a75-8428-4de0-95e9-56a3344fc1cd(no.uio.mLab.decisions.data.patient.structure)/1560130832579282613");
    b.version(2);
    return b.create();
  }
  private static ConceptDescriptor createDescriptorForPatientGenderConstraint() {
    ConceptDescriptorBuilder2 b = new ConceptDescriptorBuilder2("no.uio.mLab.decisions.data.patient", "PatientGenderConstraint", 0xf130674d23fb4e9bL, 0xa200548653f5fd0bL, 0x15a6b2b9ef374147L);
    b.class_(false, true, false);
    b.super_("no.uio.mLab.decisions.core.structure.Constraint", 0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x15a6b2b9f06b6705L);
    b.origin("r:b9eb9a75-8428-4de0-95e9-56a3344fc1cd(no.uio.mLab.decisions.data.patient.structure)/1560130832582525255");
    b.version(2);
    return b.create();
  }
  private static ConceptDescriptor createDescriptorForPatientGenderEqualTo() {
    ConceptDescriptorBuilder2 b = new ConceptDescriptorBuilder2("no.uio.mLab.decisions.data.patient", "PatientGenderEqualTo", 0xf130674d23fb4e9bL, 0xa200548653f5fd0bL, 0x15a6b2b9ef2f04c6L);
    b.class_(false, false, false);
    b.super_("no.uio.mLab.decisions.data.patient.structure.PatientGenderConstraint", 0xf130674d23fb4e9bL, 0xa200548653f5fd0bL, 0x15a6b2b9ef374147L);
    b.parent(0xf130674d23fb4e9bL, 0xa200548653f5fd0bL, 0x1b6189f12aeddcd1L);
    b.origin("r:b9eb9a75-8428-4de0-95e9-56a3344fc1cd(no.uio.mLab.decisions.data.patient.structure)/1560130832581985478");
    b.version(2);
    b.aggregate("gender", 0x15a6b2b9ef2f04edL).target(0xf130674d23fb4e9bL, 0xa200548653f5fd0bL, 0x15a6b2b9ef05c6bdL).optional(true).ordered(true).multiple(false).origin("1560130832581985517").done();
    return b.create();
  }
}
