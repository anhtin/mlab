<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:bc31e7b1-6b8e-4a25-9943-b82891a130dd(no.uio.mLab.decisions.references.biochemistry.actions)">
  <persistence version="9" />
  <languages>
    <use id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions" version="4" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports />
  <registry />
</model>

