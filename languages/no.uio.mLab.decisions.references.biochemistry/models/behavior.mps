<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:4cfef53b-5513-4a64-88b3-e6092f728466(no.uio.mLab.decisions.references.biochemistry.behavior)">
  <persistence version="9" />
  <languages>
    <use id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior" version="1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="sjef" ref="r:41fb4fd1-54ea-414d-96b0-253aa6734191(no.uio.mLab.decisions.references.biochemistry.translations)" />
    <import index="wb6c" ref="r:19d15037-83c1-4ae2-b66a-763828cb8279(no.uio.mLab.decisions.core.behavior)" />
    <import index="1yj" ref="r:0c887d67-81ab-420f-9a35-a9f6bf96c975(no.uio.mLab.shared.behavior)" />
    <import index="vbok" ref="r:6cc208d3-a370-49ca-a6ac-ded97805a924(no.uio.mLab.shared.structure)" />
    <import index="7f9y" ref="r:790ac37e-42d3-4dd3-977c-e8fe9cc56a45(no.uio.mLab.decisions.core.structure)" />
    <import index="flit" ref="r:a6507f20-2eae-4f1e-aa64-edce1d2dcb45(no.uio.mLab.decisions.references.biochemistry.structure)" />
    <import index="4v7t" ref="r:3abf6acc-a4c5-4ee7-8b22-79c4941d18e6(no.uio.mLab.entities.biochemistry.structure)" />
    <import index="kkto" ref="r:01f3534b-874e-42dc-a9b5-7540591f45bb(no.uio.mLab.entities.core.structure)" />
  </imports>
  <registry>
    <language id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior">
      <concept id="1225194240794" name="jetbrains.mps.lang.behavior.structure.ConceptBehavior" flags="ng" index="13h7C7">
        <reference id="1225194240799" name="concept" index="13h7C2" />
        <child id="1225194240805" name="method" index="13h7CS" />
        <child id="1225194240801" name="constructor" index="13h7CW" />
      </concept>
      <concept id="1225194413805" name="jetbrains.mps.lang.behavior.structure.ConceptConstructorDeclaration" flags="in" index="13hLZK" />
      <concept id="1225194472830" name="jetbrains.mps.lang.behavior.structure.ConceptMethodDeclaration" flags="ng" index="13i0hz">
        <property id="5864038008284099149" name="isStatic" index="2Ki8OM" />
        <property id="1225194472832" name="isVirtual" index="13i0it" />
        <property id="1225194472834" name="isAbstract" index="13i0iv" />
        <reference id="1225194472831" name="overriddenMethod" index="13i0hy" />
      </concept>
      <concept id="1225194691553" name="jetbrains.mps.lang.behavior.structure.ThisNodeExpression" flags="nn" index="13iPFW" />
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1070534644030" name="jetbrains.mps.baseLanguage.structure.BooleanType" flags="in" index="10P_77" />
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123137" name="jetbrains.mps.baseLanguage.structure.BooleanConstant" flags="nn" index="3clFbT">
        <property id="1068580123138" name="value" index="3clFbU" />
      </concept>
      <concept id="1068581517677" name="jetbrains.mps.baseLanguage.structure.VoidType" flags="in" index="3cqZAl" />
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
    </language>
    <language id="3a13115c-633c-4c5c-bbcc-75c4219e9555" name="jetbrains.mps.lang.quotation">
      <concept id="5455284157994012186" name="jetbrains.mps.lang.quotation.structure.NodeBuilderInitLink" flags="ng" index="2pIpSj">
        <reference id="5455284157994012188" name="link" index="2pIpSl" />
      </concept>
      <concept id="5455284157993911097" name="jetbrains.mps.lang.quotation.structure.NodeBuilderInitPart" flags="ng" index="2pJxcK">
        <child id="5455284157993911094" name="expression" index="2pJxcZ" />
      </concept>
      <concept id="5455284157993863837" name="jetbrains.mps.lang.quotation.structure.NodeBuilder" flags="nn" index="2pJPEk">
        <child id="5455284157993863838" name="quotedNode" index="2pJPEn" />
      </concept>
      <concept id="5455284157993863840" name="jetbrains.mps.lang.quotation.structure.NodeBuilderNode" flags="nn" index="2pJPED">
        <reference id="5455284157993910961" name="concept" index="2pJxaS" />
        <child id="5455284157993911099" name="values" index="2pJxcM" />
      </concept>
      <concept id="8182547171709752110" name="jetbrains.mps.lang.quotation.structure.NodeBuilderExpression" flags="nn" index="36biLy">
        <child id="8182547171709752112" name="expression" index="36biLW" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="2396822768958367367" name="jetbrains.mps.lang.smodel.structure.AbstractTypeCastExpression" flags="nn" index="$5XWr">
        <child id="6733348108486823193" name="leftExpression" index="1m5AlR" />
        <child id="3906496115198199033" name="conceptArgument" index="3oSUPX" />
      </concept>
      <concept id="2644386474300074836" name="jetbrains.mps.lang.smodel.structure.ConceptIdRefExpression" flags="nn" index="35c_gC">
        <reference id="2644386474300074837" name="conceptDeclaration" index="35c_gD" />
      </concept>
      <concept id="6677504323281689838" name="jetbrains.mps.lang.smodel.structure.SConceptType" flags="in" index="3bZ5Sz">
        <reference id="6677504323281689839" name="conceptDeclaraton" index="3bZ5Sy" />
      </concept>
      <concept id="1140131837776" name="jetbrains.mps.lang.smodel.structure.Node_ReplaceWithAnotherOperation" flags="nn" index="1P9Npp">
        <child id="1140131861877" name="replacementNode" index="1P9ThW" />
      </concept>
      <concept id="1140137987495" name="jetbrains.mps.lang.smodel.structure.SNodeTypeCastExpression" flags="nn" index="1PxgMI" />
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="13h7C7" id="4QUW3eduJZD">
    <property role="3GE5qa" value="shared" />
    <ref role="13h7C2" to="flit:4QUW3eduJLv" resolve="ITranslatableBiochemistryReferenceConcept" />
    <node concept="13i0hz" id="4QUW3eduTpX" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayTranslations" />
      <property role="2Ki8OM" value="true" />
      <node concept="3Tm1VV" id="4QUW3eduTpY" role="1B3o_S" />
      <node concept="3uibUv" id="4QUW3eduTqd" role="3clF45">
        <ref role="3uigEE" to="sjef:4zMac8rUNtP" resolve="IBiochemistryReferenceTranslations" />
      </node>
      <node concept="3clFbS" id="4QUW3eduTq0" role="3clF47">
        <node concept="3clFbF" id="4QUW3eduTrK" role="3cqZAp">
          <node concept="10M0yZ" id="4QUW3eduTso" role="3clFbG">
            <ref role="3cqZAo" to="sjef:4zMac8rVBeK" resolve="displayTranslations" />
            <ref role="1PxDUh" to="sjef:4zMac8rUNsN" resolve="BiochemistryReferenceTranslationProvider" />
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="4QUW3eduTsX" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getGenerationTranslations" />
      <node concept="3Tm1VV" id="4QUW3eduTsY" role="1B3o_S" />
      <node concept="3uibUv" id="4QUW3eduTtz" role="3clF45">
        <ref role="3uigEE" to="sjef:4zMac8rUNtP" resolve="IBiochemistryReferenceTranslations" />
      </node>
      <node concept="3clFbS" id="4QUW3eduTt0" role="3clF47">
        <node concept="3clFbF" id="4QUW3eduTu9" role="3cqZAp">
          <node concept="10M0yZ" id="4QUW3eduTuJ" role="3clFbG">
            <ref role="3cqZAo" to="sjef:1Hxyv4DUmfq" resolve="generationTranslations" />
            <ref role="1PxDUh" to="sjef:4zMac8rUNsN" resolve="BiochemistryReferenceTranslationProvider" />
          </node>
        </node>
      </node>
    </node>
    <node concept="13hLZK" id="4QUW3eduJZE" role="13h7CW">
      <node concept="3clFbS" id="4QUW3eduJZF" role="2VODD2" />
    </node>
  </node>
  <node concept="13h7C7" id="4B5aqq2zOym">
    <property role="3GE5qa" value="aspects.variables" />
    <ref role="13h7C2" to="flit:4QUW3edEuTG" resolve="BiochemistryTestVariable" />
    <node concept="13hLZK" id="4B5aqq2zOyn" role="13h7CW">
      <node concept="3clFbS" id="4B5aqq2zOyo" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="4B5aqq2zOyx" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="4B5aqq2zOyy" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq2zOyB" role="3clF47">
        <node concept="3clFbF" id="4B5aqq2zOKk" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq2zOKj" role="3clFbG">
            <property role="Xl_RC" value="biochemistry test" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="4B5aqq2zOyC" role="3clF45" />
    </node>
    <node concept="13i0hz" id="4B5aqq2zOyH" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="4B5aqq2zOyI" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq2zOyN" role="3clF47">
        <node concept="3clFbF" id="4B5aqq2zOyS" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq2zOL_" role="3clFbG">
            <property role="Xl_RC" value="" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="4B5aqq2zOyO" role="3clF45" />
    </node>
    <node concept="13i0hz" id="4B5aqq2zOyT" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getReferenceDescription" />
      <property role="2Ki8OM" value="false" />
      <ref role="13i0hy" to="1yj:4V3GMfXuP$X" resolve="getReferenceDescription" />
      <node concept="3Tm1VV" id="4B5aqq2zOyU" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq2zOz5" role="3clF47">
        <node concept="3clFbF" id="4B5aqq2zOza" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq2zOLQ" role="3clFbG">
            <property role="Xl_RC" value="biochemistry test" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="4B5aqq2zOz6" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="6LTgXmNQTbC">
    <property role="3GE5qa" value="aspects.variables" />
    <ref role="13h7C2" to="flit:4QUW3edLlxR" resolve="BiochemistrySpecialistTypeVariable" />
    <node concept="13hLZK" id="6LTgXmNQTbD" role="13h7CW">
      <node concept="3clFbS" id="6LTgXmNQTbE" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6LTgXmNQTbN" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="6LTgXmNQTbO" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNQTbT" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNQTgy" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNQTgx" role="3clFbG">
            <property role="Xl_RC" value="biochemistry specialist type" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmNQTbU" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmNQTbZ" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="6LTgXmNQTc0" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNQTc5" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNQTlU" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNQTlT" role="3clFbG">
            <property role="Xl_RC" value="" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmNQTc6" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmNQTmt" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getReferenceDescription" />
      <property role="2Ki8OM" value="false" />
      <ref role="13i0hy" to="1yj:4V3GMfXuP$X" resolve="getReferenceDescription" />
      <node concept="3Tm1VV" id="6LTgXmNQTmu" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNQTmD" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNQTxy" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNQTxx" role="3clFbG">
            <property role="Xl_RC" value="biochemistry specialist type" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmNQTmE" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="6LTgXmNUZzm">
    <property role="3GE5qa" value="aspects.patterns" />
    <ref role="13h7C2" to="flit:6LTgXmNUZyu" resolve="WildcardBiochemistryTestPattern" />
    <node concept="13hLZK" id="6LTgXmNUZzn" role="13h7CW">
      <node concept="3clFbS" id="6LTgXmNUZzo" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6LTgXmNUZ$_" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="6LTgXmNUZ$A" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNUZ$F" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNUZ$K" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNV4aM" role="3clFbG">
            <property role="Xl_RC" value="*" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmNUZ$G" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmNUZ$L" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="6LTgXmNUZ$M" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNUZ$R" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNUZ$W" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNV4bq" role="3clFbG">
            <property role="Xl_RC" value="any biochemistry test" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmNUZ$S" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmNUZ$p" role="13h7CS">
      <property role="TrG5h" value="hasPattern" />
      <property role="13i0it" value="false" />
      <property role="13i0iv" value="false" />
      <ref role="13i0hy" to="wb6c:6LTgXmNRv0n" resolve="hasPattern" />
      <node concept="3Tm1VV" id="6LTgXmNUZ$q" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNUZ$v" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNUZ$$" role="3cqZAp">
          <node concept="3clFbT" id="6LTgXmNV05f" role="3clFbG">
            <property role="3clFbU" value="true" />
          </node>
        </node>
      </node>
      <node concept="10P_77" id="6LTgXmNUZ$w" role="3clF45" />
    </node>
    <node concept="13i0hz" id="2FjKBCOhj1E" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getMatchEntityConcept" />
      <ref role="13i0hy" to="wb6c:65epL7MnLJr" resolve="getMatchEntityConcept" />
      <node concept="3Tm1VV" id="2FjKBCOhj1F" role="1B3o_S" />
      <node concept="3clFbS" id="2FjKBCOhj1K" role="3clF47">
        <node concept="3clFbF" id="2FjKBCOhjep" role="3cqZAp">
          <node concept="35c_gC" id="2FjKBCOhjeo" role="3clFbG">
            <ref role="35c_gD" to="4v7t:5Wfdz$0qWIu" resolve="BiochemistryTest" />
          </node>
        </node>
      </node>
      <node concept="3bZ5Sz" id="2FjKBCOhj1L" role="3clF45">
        <ref role="3bZ5Sy" to="kkto:4QUW3edDqMR" resolve="Entity" />
      </node>
    </node>
  </node>
  <node concept="13h7C7" id="1I84Bf8k6tT">
    <property role="3GE5qa" value="aspects.references.biochemistryTest" />
    <ref role="13h7C2" to="flit:4QUW3efwCu7" resolve="AspectBiochemistryTestReference" />
    <node concept="13hLZK" id="1I84Bf8k6tU" role="13h7CW">
      <node concept="3clFbS" id="1I84Bf8k6tV" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="1I84Bf8k6u4" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="replaceWith" />
      <ref role="13i0hy" to="wb6c:1I84Bf8ilTw" resolve="replaceWith" />
      <node concept="3Tm1VV" id="1I84Bf8k6u5" role="1B3o_S" />
      <node concept="3clFbS" id="1I84Bf8k6uf" role="3clF47">
        <node concept="3clFbF" id="1I84Bf8k6zP" role="3cqZAp">
          <node concept="2OqwBi" id="1I84Bf8k6Mx" role="3clFbG">
            <node concept="13iPFW" id="1I84Bf8k6zO" role="2Oq$k0" />
            <node concept="1P9Npp" id="1I84Bf8k7b9" role="2OqNvi">
              <node concept="2pJPEk" id="1I84Bf8k7do" role="1P9ThW">
                <node concept="2pJPED" id="1I84Bf8k7iF" role="2pJPEn">
                  <ref role="2pJxaS" to="flit:4QUW3efwCua" resolve="EntityBiochemistryTestReference" />
                  <node concept="2pIpSj" id="1I84Bf8k7mP" role="2pJxcM">
                    <ref role="2pIpSl" to="flit:4QUW3efwCub" resolve="target" />
                    <node concept="36biLy" id="1I84Bf8k7r3" role="2pJxcZ">
                      <node concept="1PxgMI" id="1I84Bf8k7Og" role="36biLW">
                        <node concept="chp4Y" id="1I84Bf8k7ON" role="3oSUPX">
                          <ref role="cht4Q" to="4v7t:5Wfdz$0qWIu" resolve="BiochemistryTest" />
                        </node>
                        <node concept="37vLTw" id="1I84Bf8k7us" role="1m5AlR">
                          <ref role="3cqZAo" node="1I84Bf8k6ug" resolve="node" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="1I84Bf8k6ug" role="3clF46">
        <property role="TrG5h" value="node" />
        <node concept="3Tqbb2" id="1I84Bf8k6uh" role="1tU5fm" />
      </node>
      <node concept="3cqZAl" id="1I84Bf8k6ui" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="1I84Bf8kbWi">
    <property role="3GE5qa" value="aspects.references.biochemistryTest" />
    <ref role="13h7C2" to="flit:4V3GMfXst2L" resolve="AspectBiochemistryTestWithNumberResultReference" />
    <node concept="13hLZK" id="1I84Bf8kbWj" role="13h7CW">
      <node concept="3clFbS" id="1I84Bf8kbWk" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="1I84Bf8kbWt" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="replaceWith" />
      <ref role="13i0hy" to="wb6c:1I84Bf8ilTw" resolve="replaceWith" />
      <node concept="3Tm1VV" id="1I84Bf8kbWu" role="1B3o_S" />
      <node concept="3clFbS" id="1I84Bf8kbWC" role="3clF47">
        <node concept="3clFbF" id="1I84Bf8kc2e" role="3cqZAp">
          <node concept="2OqwBi" id="1I84Bf8kcga" role="3clFbG">
            <node concept="13iPFW" id="1I84Bf8kc2d" role="2Oq$k0" />
            <node concept="1P9Npp" id="1I84Bf8kwvu" role="2OqNvi">
              <node concept="2pJPEk" id="1I84Bf8kcKx" role="1P9ThW">
                <node concept="2pJPED" id="1I84Bf8kd1S" role="2pJPEn">
                  <ref role="2pJxaS" to="flit:4V3GMfXrGkh" resolve="EntityBiochemistryTestWithNumberResultReference" />
                  <node concept="2pIpSj" id="1I84Bf8kda9" role="2pJxcM">
                    <ref role="2pIpSl" to="flit:4V3GMfXrGDW" resolve="target" />
                    <node concept="36biLy" id="1I84Bf8kdiu" role="2pJxcZ">
                      <node concept="1PxgMI" id="1I84Bf8kdx8" role="36biLW">
                        <node concept="chp4Y" id="1I84Bf8kdxF" role="3oSUPX">
                          <ref role="cht4Q" to="4v7t:5Wfdz$0qWIu" resolve="BiochemistryTest" />
                        </node>
                        <node concept="37vLTw" id="1I84Bf8kdiL" role="1m5AlR">
                          <ref role="3cqZAo" node="1I84Bf8kbWD" resolve="node" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="1I84Bf8kbWD" role="3clF46">
        <property role="TrG5h" value="node" />
        <node concept="3Tqbb2" id="1I84Bf8kbWE" role="1tU5fm" />
      </node>
      <node concept="3cqZAl" id="1I84Bf8kbWF" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="1I84Bf8knxR">
    <property role="3GE5qa" value="aspects.references" />
    <ref role="13h7C2" to="flit:4QUW3efwCu3" resolve="AspectBiochemistrySpecialistTypeReference" />
    <node concept="13hLZK" id="1I84Bf8knxS" role="13h7CW">
      <node concept="3clFbS" id="1I84Bf8knxT" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="1I84Bf8kny2" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="replaceWith" />
      <ref role="13i0hy" to="wb6c:1I84Bf8ilTw" resolve="replaceWith" />
      <node concept="3Tm1VV" id="1I84Bf8kny3" role="1B3o_S" />
      <node concept="3clFbS" id="1I84Bf8knyd" role="3clF47">
        <node concept="3clFbF" id="1I84Bf8kqRS" role="3cqZAp">
          <node concept="2OqwBi" id="1I84Bf8kr7i" role="3clFbG">
            <node concept="13iPFW" id="1I84Bf8kqRQ" role="2Oq$k0" />
            <node concept="1P9Npp" id="1I84Bf8krAF" role="2OqNvi">
              <node concept="2pJPEk" id="1I84Bf8kooK" role="1P9ThW">
                <node concept="2pJPED" id="1I84Bf8koEM" role="2pJPEn">
                  <ref role="2pJxaS" to="flit:4QUW3efwCu0" resolve="EntityBiochemistrySpecialistTypeReference" />
                  <node concept="2pIpSj" id="1I84Bf8koM0" role="2pJxcM">
                    <ref role="2pIpSl" to="flit:4QUW3efwCu1" resolve="target" />
                    <node concept="36biLy" id="1I84Bf8koUB" role="2pJxcZ">
                      <node concept="1PxgMI" id="1I84Bf8kp4n" role="36biLW">
                        <node concept="chp4Y" id="1I84Bf8kp4U" role="3oSUPX">
                          <ref role="cht4Q" to="4v7t:1mAGFBLdp3F" resolve="BiochemistrySpecialistType" />
                        </node>
                        <node concept="37vLTw" id="1I84Bf8koWf" role="1m5AlR">
                          <ref role="3cqZAo" node="1I84Bf8knye" resolve="node" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="1I84Bf8knye" role="3clF46">
        <property role="TrG5h" value="node" />
        <node concept="3Tqbb2" id="1I84Bf8knyf" role="1tU5fm" />
      </node>
      <node concept="3cqZAl" id="1I84Bf8knyg" role="3clF45" />
    </node>
  </node>
</model>

