<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:675ac6dc-418a-4ce4-8080-6ef8d97b7a81(no.uio.mLab.decisions.references.biochemistry.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="11" />
    <use id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions" version="4" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="i7z6" ref="r:515ff38d-92f9-43f3-aa4c-c5bec29efe22(no.uio.mLab.decisions.core.editor)" />
    <import index="flit" ref="r:a6507f20-2eae-4f1e-aa64-edce1d2dcb45(no.uio.mLab.decisions.references.biochemistry.structure)" implicit="true" />
    <import index="1yj" ref="r:0c887d67-81ab-420f-9a35-a9f6bf96c975(no.uio.mLab.shared.behavior)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="2000375450116454183" name="jetbrains.mps.lang.editor.structure.ISubstituteMenu" flags="ng" index="22mbnS">
        <child id="414384289274416996" name="parts" index="3ft7WO" />
      </concept>
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi" />
      <concept id="1177327570013" name="jetbrains.mps.lang.editor.structure.QueryFunction_SubstituteMenu_Substitute" flags="in" index="ucgPf" />
      <concept id="8478191136883534237" name="jetbrains.mps.lang.editor.structure.IExtensibleSubstituteMenuPart" flags="ng" index="upBLQ">
        <child id="8478191136883534238" name="features" index="upBLP" />
      </concept>
      <concept id="1177335944525" name="jetbrains.mps.lang.editor.structure.QueryFunction_SubstituteMenu_SubstituteString" flags="in" index="uGdhv" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="1078938745671" name="jetbrains.mps.lang.editor.structure.EditorComponentDeclaration" flags="ig" index="PKFIW">
        <child id="7033942394258392116" name="overridenEditorComponent" index="1PM95z" />
      </concept>
      <concept id="615427434521884870" name="jetbrains.mps.lang.editor.structure.SubstituteMenuPart_Subconcepts" flags="ng" index="2VfDsV" />
      <concept id="1186414536763" name="jetbrains.mps.lang.editor.structure.BooleanStyleSheetItem" flags="ln" index="VOi$J">
        <property id="1186414551515" name="flag" index="VOm3f" />
      </concept>
      <concept id="1186414860679" name="jetbrains.mps.lang.editor.structure.EditableStyleClassItem" flags="ln" index="VPxyj" />
      <concept id="1630016958697344083" name="jetbrains.mps.lang.editor.structure.IMenu_Concept" flags="ng" index="2ZABuq">
        <reference id="6591946374543067572" name="conceptDeclaration" index="aqKnT" />
      </concept>
      <concept id="8998492695583125082" name="jetbrains.mps.lang.editor.structure.SubstituteFeature_MatchingText" flags="ng" index="16NfWO">
        <child id="8998492695583129244" name="query" index="16NeZM" />
      </concept>
      <concept id="8998492695583129971" name="jetbrains.mps.lang.editor.structure.SubstituteFeature_DescriptionText" flags="ng" index="16NL0t">
        <child id="8998492695583129972" name="query" index="16NL0q" />
      </concept>
      <concept id="7342352913006985483" name="jetbrains.mps.lang.editor.structure.SubstituteMenuPart_Action" flags="ng" index="3eGOop">
        <child id="8612453216082699922" name="substituteHandler" index="3aKz83" />
      </concept>
      <concept id="3308396621974580100" name="jetbrains.mps.lang.editor.structure.SubstituteMenu_Default" flags="ng" index="3p36aQ" />
      <concept id="5425882385312046132" name="jetbrains.mps.lang.editor.structure.QueryFunctionParameter_SubstituteMenu_CurrentTargetNode" flags="nn" index="1yR$tW" />
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <property id="1139852716018" name="noTargetText" index="1$x2rV" />
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389577006" name="jetbrains.mps.lang.editor.structure.CellModel_Constant" flags="sn" stub="3610246225209162225" index="3F0ifn">
        <property id="1082639509531" name="nullText" index="ilYzB" />
        <property id="1073389577007" name="text" index="3F0ifm" />
      </concept>
      <concept id="1219418625346" name="jetbrains.mps.lang.editor.structure.IStyleContainer" flags="ng" index="3F0Thp">
        <child id="1219418656006" name="styleItem" index="3F10Kt" />
      </concept>
      <concept id="1073389882823" name="jetbrains.mps.lang.editor.structure.CellModel_RefNode" flags="sg" stub="730538219795960754" index="3F1sOY" />
      <concept id="7033942394256351208" name="jetbrains.mps.lang.editor.structure.EditorComponentDeclarationReference" flags="ng" index="1PE4EZ">
        <reference id="7033942394256351817" name="editorComponent" index="1PE7su" />
      </concept>
      <concept id="4307758654696938365" name="jetbrains.mps.lang.editor.structure.QueryFunction_SubstituteMenu_RefPresentation" flags="ig" index="1WAQ3h" />
      <concept id="4307758654696952957" name="jetbrains.mps.lang.editor.structure.QueryFunctionParameter_SubstituteMenu_ReferencedNode" flags="ng" index="1WAUZh" />
      <concept id="8428109087107030357" name="jetbrains.mps.lang.editor.structure.SubstituteMenuPart_ReferenceScope" flags="ng" index="3XHNnq">
        <reference id="8428109087107339113" name="reference" index="3XGfJA" />
        <child id="4307758654694907855" name="descriptionTextFunction" index="1WZ6hz" />
      </concept>
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
      </concept>
    </language>
    <language id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions">
      <concept id="5979988948250981289" name="jetbrains.mps.lang.actions.structure.SNodeCreatorAndInitializer" flags="nn" index="2fJWfE">
        <child id="3757480014665187678" name="prototype" index="1wAG5O" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="2644386474300074836" name="jetbrains.mps.lang.smodel.structure.ConceptIdRefExpression" flags="nn" index="35c_gC">
        <reference id="2644386474300074837" name="conceptDeclaration" index="35c_gD" />
      </concept>
      <concept id="1180636770613" name="jetbrains.mps.lang.smodel.structure.SNodeCreator" flags="nn" index="3zrR0B">
        <child id="1180636770616" name="createdType" index="3zrR0E" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="3p36aQ" id="4QUW3efGu4T">
    <property role="3GE5qa" value="shared.references.biochemistryTest" />
    <ref role="aqKnT" to="flit:4QUW3efwCu6" resolve="BiochemistryTestReference" />
    <node concept="2VfDsV" id="65epL7MINoh" role="3ft7WO" />
  </node>
  <node concept="3p36aQ" id="4V3GMfXrXQh">
    <property role="3GE5qa" value="base.references.biochemistryTest" />
    <ref role="aqKnT" to="flit:4V3GMfXrGkh" resolve="EntityBiochemistryTestWithNumberResultReference" />
    <node concept="3XHNnq" id="4V3GMfXrXQi" role="3ft7WO">
      <ref role="3XGfJA" to="flit:4V3GMfXrGDW" resolve="target" />
      <node concept="1WAQ3h" id="4V3GMfXrYii" role="1WZ6hz">
        <node concept="3clFbS" id="4V3GMfXrYij" role="2VODD2">
          <node concept="3clFbF" id="4V3GMfXrYqS" role="3cqZAp">
            <node concept="2OqwBi" id="4V3GMfXMGy2" role="3clFbG">
              <node concept="1WAUZh" id="4V3GMfXMGb2" role="2Oq$k0" />
              <node concept="2qgKlT" id="4V3GMfXMGZx" role="2OqNvi">
                <ref role="37wK5l" to="1yj:4V3GMfXuP$X" resolve="getReferenceDescription" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="4V3GMfXuH_E">
    <property role="3GE5qa" value="base.references.biochemistryTest" />
    <ref role="aqKnT" to="flit:4QUW3efwCua" resolve="EntityBiochemistryTestReference" />
    <node concept="3XHNnq" id="4V3GMfXuH_F" role="3ft7WO">
      <ref role="3XGfJA" to="flit:4QUW3efwCub" resolve="target" />
      <node concept="1WAQ3h" id="4V3GMfXM_U$" role="1WZ6hz">
        <node concept="3clFbS" id="4V3GMfXM_U_" role="2VODD2">
          <node concept="3clFbF" id="4V3GMfXMA3a" role="3cqZAp">
            <node concept="2OqwBi" id="4V3GMfXMAqb" role="3clFbG">
              <node concept="1WAUZh" id="4V3GMfXMA39" role="2Oq$k0" />
              <node concept="2qgKlT" id="4V3GMfXMARE" role="2OqNvi">
                <ref role="37wK5l" to="1yj:4V3GMfXuP$X" resolve="getReferenceDescription" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="4V3GMfX_vv5">
    <property role="3GE5qa" value="aspects.references.biochemistryTest" />
    <ref role="aqKnT" to="flit:4QUW3efwCu7" resolve="AspectBiochemistryTestReference" />
    <node concept="3XHNnq" id="4V3GMfXOej2" role="3ft7WO">
      <ref role="3XGfJA" to="flit:4QUW3efwCu8" resolve="target" />
      <node concept="1WAQ3h" id="4V3GMfXOej3" role="1WZ6hz">
        <node concept="3clFbS" id="4V3GMfXOej4" role="2VODD2">
          <node concept="3clFbF" id="4V3GMfXOej5" role="3cqZAp">
            <node concept="2OqwBi" id="4V3GMfXOej6" role="3clFbG">
              <node concept="1WAUZh" id="4V3GMfXOej7" role="2Oq$k0" />
              <node concept="2qgKlT" id="4V3GMfXOej8" role="2OqNvi">
                <ref role="37wK5l" to="1yj:4V3GMfXuP$X" resolve="getReferenceDescription" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="4V3GMfX_vQR">
    <property role="3GE5qa" value="aspects.references.biochemistryTest" />
    <ref role="aqKnT" to="flit:4V3GMfXst2L" resolve="AspectBiochemistryTestWithNumberResultReference" />
    <node concept="3XHNnq" id="4V3GMfXOdUV" role="3ft7WO">
      <ref role="3XGfJA" to="flit:4V3GMfXumVD" resolve="target" />
      <node concept="1WAQ3h" id="4V3GMfXOdUW" role="1WZ6hz">
        <node concept="3clFbS" id="4V3GMfXOdUX" role="2VODD2">
          <node concept="3clFbF" id="4V3GMfXOdUY" role="3cqZAp">
            <node concept="2OqwBi" id="4V3GMfXOdUZ" role="3clFbG">
              <node concept="1WAUZh" id="4V3GMfXOdV0" role="2Oq$k0" />
              <node concept="2qgKlT" id="4V3GMfXOdV1" role="2OqNvi">
                <ref role="37wK5l" to="1yj:4V3GMfXuP$X" resolve="getReferenceDescription" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="4B5aqq2zKQ$">
    <property role="3GE5qa" value="aspects.variables" />
    <ref role="aqKnT" to="flit:4QUW3edEuTG" resolve="BiochemistryTestVariable" />
    <node concept="3eGOop" id="4B5aqq2zKQ_" role="3ft7WO">
      <node concept="ucgPf" id="4B5aqq2zKQA" role="3aKz83">
        <node concept="3clFbS" id="4B5aqq2zKQB" role="2VODD2">
          <node concept="3clFbF" id="4B5aqq2zKVM" role="3cqZAp">
            <node concept="2ShNRf" id="4B5aqq2zKVK" role="3clFbG">
              <node concept="2fJWfE" id="4B5aqq2zL46" role="2ShVmc">
                <node concept="3Tqbb2" id="4B5aqq2zL48" role="3zrR0E">
                  <ref role="ehGHo" to="flit:4QUW3edEuTG" resolve="BiochemistryTestVariable" />
                </node>
                <node concept="1yR$tW" id="4B5aqq2zLh4" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="4B5aqq2zLmr" role="upBLP">
        <node concept="uGdhv" id="4B5aqq2zLrO" role="16NeZM">
          <node concept="3clFbS" id="4B5aqq2zLrQ" role="2VODD2">
            <node concept="3clFbF" id="4B5aqq2zL$s" role="3cqZAp">
              <node concept="2OqwBi" id="4B5aqq2zM7t" role="3clFbG">
                <node concept="35c_gC" id="4B5aqq2zL$r" role="2Oq$k0">
                  <ref role="35c_gD" to="flit:4QUW3edEuTG" resolve="BiochemistryTestVariable" />
                </node>
                <node concept="2qgKlT" id="4B5aqq2zMBX" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="4B5aqq2zMVp" role="upBLP">
        <node concept="uGdhv" id="4B5aqq2zN0Z" role="16NL0q">
          <node concept="3clFbS" id="4B5aqq2zN11" role="2VODD2">
            <node concept="3clFbF" id="4B5aqq2zN9B" role="3cqZAp">
              <node concept="2OqwBi" id="4B5aqq2zNNz" role="3clFbG">
                <node concept="35c_gC" id="4B5aqq2zN9A" role="2Oq$k0">
                  <ref role="35c_gD" to="flit:4QUW3edEuTG" resolve="BiochemistryTestVariable" />
                </node>
                <node concept="2qgKlT" id="4B5aqq2zOk3" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="4B5aqq2$plS">
    <property role="3GE5qa" value="shared.references.biochemistryTest" />
    <ref role="aqKnT" to="flit:4V3GMfXJ6OE" resolve="BiochemistryTestWithNumberResultReference" />
    <node concept="2VfDsV" id="6LTgXmNR1M5" role="3ft7WO" />
  </node>
  <node concept="3p36aQ" id="6LTgXmNQTzD">
    <property role="3GE5qa" value="aspects.variables" />
    <ref role="aqKnT" to="flit:4QUW3edLlxR" resolve="BiochemistrySpecialistTypeVariable" />
    <node concept="3eGOop" id="6LTgXmNQTzE" role="3ft7WO">
      <node concept="ucgPf" id="6LTgXmNQTzF" role="3aKz83">
        <node concept="3clFbS" id="6LTgXmNQTzG" role="2VODD2">
          <node concept="3clFbF" id="6LTgXmNQTCE" role="3cqZAp">
            <node concept="2ShNRf" id="6LTgXmNQTCC" role="3clFbG">
              <node concept="2fJWfE" id="6LTgXmNQTKI" role="2ShVmc">
                <node concept="3Tqbb2" id="6LTgXmNQTKK" role="3zrR0E">
                  <ref role="ehGHo" to="flit:4QUW3edLlxR" resolve="BiochemistrySpecialistTypeVariable" />
                </node>
                <node concept="1yR$tW" id="6LTgXmNQTX6" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="6LTgXmNQU2e" role="upBLP">
        <node concept="uGdhv" id="6LTgXmNQU7p" role="16NeZM">
          <node concept="3clFbS" id="6LTgXmNQU7r" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmNQUg3" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmNQUQR" role="3clFbG">
                <node concept="35c_gC" id="6LTgXmNQUg2" role="2Oq$k0">
                  <ref role="35c_gD" to="flit:4QUW3edLlxR" resolve="BiochemistrySpecialistTypeVariable" />
                </node>
                <node concept="2qgKlT" id="6LTgXmNQVmp" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="6LTgXmNQVDD" role="upBLP">
        <node concept="uGdhv" id="6LTgXmNQVJ8" role="16NL0q">
          <node concept="3clFbS" id="6LTgXmNQVJa" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmNQVRM" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmNQWqw" role="3clFbG">
                <node concept="35c_gC" id="6LTgXmNQVRL" role="2Oq$k0">
                  <ref role="35c_gD" to="flit:4QUW3edLlxR" resolve="BiochemistrySpecialistTypeVariable" />
                </node>
                <node concept="2qgKlT" id="6LTgXmNQWU5" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="6LTgXmNRl4m">
    <property role="3GE5qa" value="aspects.patterns" />
    <ref role="aqKnT" to="flit:6LTgXmNRl3W" resolve="BiochemistryTestPattern" />
    <node concept="2VfDsV" id="6LTgXmNRl8M" role="3ft7WO" />
  </node>
  <node concept="24kQdi" id="6LTgXmNRl9d">
    <property role="3GE5qa" value="aspects.patterns" />
    <ref role="1XX52x" to="flit:6LTgXmNRl3W" resolve="BiochemistryTestPattern" />
    <node concept="3F0ifn" id="6LTgXmNRl9f" role="2wV5jI">
      <property role="ilYzB" value="&lt;&lt; ... &gt;&gt;" />
      <node concept="VPxyj" id="6LTgXmNRl9i" role="3F10Kt">
        <property role="VOm3f" value="true" />
      </node>
    </node>
  </node>
  <node concept="PKFIW" id="6LTgXmNRpo3">
    <property role="3GE5qa" value="aspects.variables" />
    <property role="TrG5h" value="Pattern_BiochemistryTestVariable_EditorComponent" />
    <ref role="1XX52x" to="flit:4QUW3edEuTG" resolve="BiochemistryTestVariable" />
    <node concept="3F1sOY" id="6LTgXmNRpo9" role="2wV5jI">
      <property role="1$x2rV" value="&lt;&lt; ... &gt;&gt;" />
      <ref role="1NtTu8" to="flit:6LTgXmNRnp3" resolve="pattern" />
    </node>
    <node concept="1PE4EZ" id="6LTgXmNRpo7" role="1PM95z">
      <ref role="1PE7su" to="i7z6:6LTgXmNpPu9" resolve="Pattern_AspectVariable_EditorComponent" />
    </node>
  </node>
  <node concept="24kQdi" id="6LTgXmNUZyS">
    <property role="3GE5qa" value="aspects.patterns" />
    <ref role="1XX52x" to="flit:6LTgXmNUZyu" resolve="WildcardBiochemistryTestPattern" />
    <node concept="3F0ifn" id="6LTgXmNUZyU" role="2wV5jI">
      <property role="3F0ifm" value="*" />
    </node>
  </node>
  <node concept="3p36aQ" id="6LTgXmNV7YN">
    <property role="3GE5qa" value="aspects.patterns" />
    <ref role="aqKnT" to="flit:6LTgXmNUZyu" resolve="WildcardBiochemistryTestPattern" />
    <node concept="3eGOop" id="6LTgXmNV7YQ" role="3ft7WO">
      <node concept="ucgPf" id="6LTgXmNV7YR" role="3aKz83">
        <node concept="3clFbS" id="6LTgXmNV7YS" role="2VODD2">
          <node concept="3clFbF" id="6LTgXmNV83N" role="3cqZAp">
            <node concept="2ShNRf" id="6LTgXmNV83L" role="3clFbG">
              <node concept="2fJWfE" id="6LTgXmNV8bO" role="2ShVmc">
                <node concept="3Tqbb2" id="6LTgXmNV8bQ" role="3zrR0E">
                  <ref role="ehGHo" to="flit:6LTgXmNUZyu" resolve="WildcardBiochemistryTestPattern" />
                </node>
                <node concept="1yR$tW" id="6LTgXmNV8lR" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="6LTgXmNV8qY" role="upBLP">
        <node concept="uGdhv" id="6LTgXmNV8w7" role="16NeZM">
          <node concept="3clFbS" id="6LTgXmNV8w9" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmNV8CJ" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmNV9ie" role="3clFbG">
                <node concept="35c_gC" id="6LTgXmNV8CI" role="2Oq$k0">
                  <ref role="35c_gD" to="flit:6LTgXmNUZyu" resolve="WildcardBiochemistryTestPattern" />
                </node>
                <node concept="2qgKlT" id="6LTgXmNV9LH" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="6LTgXmNVa4N" role="upBLP">
        <node concept="uGdhv" id="6LTgXmNVaa9" role="16NL0q">
          <node concept="3clFbS" id="6LTgXmNVaab" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmNVaiL" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmNVaTz" role="3clFbG">
                <node concept="35c_gC" id="6LTgXmNVaiK" role="2Oq$k0">
                  <ref role="35c_gD" to="flit:6LTgXmNUZyu" resolve="WildcardBiochemistryTestPattern" />
                </node>
                <node concept="2qgKlT" id="6LTgXmNVboZ" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

