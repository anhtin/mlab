<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:a6507f20-2eae-4f1e-aa64-edce1d2dcb45(no.uio.mLab.decisions.references.biochemistry.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="6" />
    <devkit ref="78434eb8-b0e5-444b-850d-e7c4ad2da9ab(jetbrains.mps.devkit.aspect.structure)" />
  </languages>
  <imports>
    <import index="vbok" ref="r:6cc208d3-a370-49ca-a6ac-ded97805a924(no.uio.mLab.shared.structure)" />
    <import index="4v7t" ref="r:3abf6acc-a4c5-4ee7-8b22-79c4941d18e6(no.uio.mLab.entities.biochemistry.structure)" />
    <import index="ruww" ref="r:d7787915-5b9a-4713-a6ab-6345c5e1ec03(no.uio.mLab.decisions.references.laboratoryTest.structure)" />
    <import index="hpcx" ref="r:2530bb31-0779-45e3-bc3f-01507d6b2623(no.uio.mLab.decisions.references.specialistType.structure)" />
    <import index="7f9y" ref="r:790ac37e-42d3-4dd3-977c-e8fe9cc56a45(no.uio.mLab.decisions.core.structure)" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <property id="4628067390765956807" name="final" index="R5$K2" />
        <property id="4628067390765956802" name="abstract" index="R5$K7" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
      </concept>
      <concept id="1169125989551" name="jetbrains.mps.lang.structure.structure.InterfaceConceptDeclaration" flags="ig" index="PlHQZ">
        <child id="1169127546356" name="extends" index="PrDN$" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599698500" name="specializedLink" index="20ksaX" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="PlHQZ" id="4QUW3eduJLv">
    <property role="EcuMT" value="5601053190796278879" />
    <property role="TrG5h" value="ITranslatableBiochemistryReferenceConcept" />
    <property role="3GE5qa" value="shared" />
    <node concept="PrWs8" id="4QUW3eduJLw" role="PrDN$">
      <ref role="PrY4T" to="vbok:5Wfdz$0vc1Z" resolve="ITranslatableConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="4QUW3edEuTG">
    <property role="EcuMT" value="5601053190799355500" />
    <property role="TrG5h" value="BiochemistryTestVariable" />
    <property role="3GE5qa" value="aspects.variables" />
    <ref role="1TJDcQ" to="ruww:4QUW3edDVyK" resolve="LaboratoryTestVariable" />
    <node concept="1TJgyj" id="6LTgXmNRnp3" role="1TKVEi">
      <property role="IQ2ns" value="7816353213402936899" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="pattern" />
      <ref role="20ksaX" to="ruww:6LTgXmNpAAv" resolve="pattern" />
      <ref role="20lvS9" node="6LTgXmNRl3W" resolve="BiochemistryTestPattern" />
    </node>
  </node>
  <node concept="1TIwiD" id="4QUW3edLlxR">
    <property role="EcuMT" value="5601053190801152119" />
    <property role="3GE5qa" value="aspects.variables" />
    <property role="TrG5h" value="BiochemistrySpecialistTypeVariable" />
    <ref role="1TJDcQ" to="7f9y:4QUW3edDXqS" resolve="AspectVariable" />
  </node>
  <node concept="1TIwiD" id="4QUW3efwBD9">
    <property role="EcuMT" value="5601053190830324297" />
    <property role="TrG5h" value="BiochemistrySpecialistTypeReference" />
    <property role="R5$K7" value="true" />
    <property role="R5$K2" value="false" />
    <property role="3GE5qa" value="shared.references.specialistType" />
    <ref role="1TJDcQ" to="hpcx:4QUW3efwBDa" resolve="SpecialistTypeReference" />
    <node concept="PrWs8" id="4QUW3efwCue" role="PzmwI">
      <ref role="PrY4T" node="4QUW3eduJLv" resolve="ITranslatableBiochemistryReferenceConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="4QUW3efwCu0">
    <property role="EcuMT" value="5601053190830327680" />
    <property role="3GE5qa" value="base.references" />
    <property role="TrG5h" value="EntityBiochemistrySpecialistTypeReference" />
    <ref role="1TJDcQ" node="4QUW3efwBD9" resolve="BiochemistrySpecialistTypeReference" />
    <node concept="1TJgyj" id="4QUW3efwCu1" role="1TKVEi">
      <property role="IQ2ns" value="5601053190830327681" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="target" />
      <ref role="20lvS9" to="4v7t:1mAGFBLdp3F" resolve="BiochemistrySpecialistType" />
      <ref role="20ksaX" to="7f9y:4QUW3efv2jv" resolve="target" />
    </node>
    <node concept="PrWs8" id="6LTgXmMD1EW" role="PzmwI">
      <ref role="PrY4T" to="7f9y:6LTgXmMvpq9" resolve="IEntityReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="4QUW3efwCu3">
    <property role="EcuMT" value="5601053190830327683" />
    <property role="3GE5qa" value="aspects.references" />
    <property role="TrG5h" value="AspectBiochemistrySpecialistTypeReference" />
    <ref role="1TJDcQ" node="4QUW3efwBD9" resolve="BiochemistrySpecialistTypeReference" />
    <node concept="1TJgyj" id="4QUW3efwCu4" role="1TKVEi">
      <property role="IQ2ns" value="5601053190830327684" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="target" />
      <ref role="20lvS9" node="4QUW3edLlxR" resolve="BiochemistrySpecialistTypeVariable" />
      <ref role="20ksaX" to="7f9y:6LTgXmMAS1z" resolve="target" />
    </node>
    <node concept="PrWs8" id="6LTgXmMD1EZ" role="PzmwI">
      <ref role="PrY4T" to="7f9y:6LTgXmMAn$t" resolve="IAspectVariableReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="4QUW3efwCu6">
    <property role="EcuMT" value="5601053190830327686" />
    <property role="3GE5qa" value="shared.references.biochemistryTest" />
    <property role="TrG5h" value="BiochemistryTestReference" />
    <property role="R5$K7" value="true" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" to="ruww:4QUW3efwB30" resolve="LaboratoryTestReference" />
    <node concept="PrWs8" id="4QUW3efwCud" role="PzmwI">
      <ref role="PrY4T" node="4QUW3eduJLv" resolve="ITranslatableBiochemistryReferenceConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="4QUW3efwCu7">
    <property role="EcuMT" value="5601053190830327687" />
    <property role="3GE5qa" value="aspects.references.biochemistryTest" />
    <property role="TrG5h" value="AspectBiochemistryTestReference" />
    <ref role="1TJDcQ" node="4QUW3efwCu6" resolve="BiochemistryTestReference" />
    <node concept="1TJgyj" id="4QUW3efwCu8" role="1TKVEi">
      <property role="IQ2ns" value="5601053190830327688" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="target" />
      <ref role="20lvS9" node="4QUW3edEuTG" resolve="BiochemistryTestVariable" />
      <ref role="20ksaX" to="7f9y:6LTgXmMAS1z" resolve="target" />
    </node>
    <node concept="PrWs8" id="6LTgXmMD1F2" role="PzmwI">
      <ref role="PrY4T" to="7f9y:6LTgXmMAn$t" resolve="IAspectVariableReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="4QUW3efwCua">
    <property role="EcuMT" value="5601053190830327690" />
    <property role="3GE5qa" value="base.references.biochemistryTest" />
    <property role="TrG5h" value="EntityBiochemistryTestReference" />
    <ref role="1TJDcQ" node="4QUW3efwCu6" resolve="BiochemistryTestReference" />
    <node concept="1TJgyj" id="4QUW3efwCub" role="1TKVEi">
      <property role="IQ2ns" value="5601053190830327691" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="target" />
      <ref role="20lvS9" to="4v7t:5Wfdz$0qWIu" resolve="BiochemistryTest" />
      <ref role="20ksaX" to="7f9y:4QUW3efv2jv" resolve="target" />
    </node>
    <node concept="PrWs8" id="6LTgXmMD1F8" role="PzmwI">
      <ref role="PrY4T" to="7f9y:6LTgXmMvpq9" resolve="IEntityReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="4V3GMfXrGkh">
    <property role="EcuMT" value="5675576922574079249" />
    <property role="3GE5qa" value="base.references.biochemistryTest" />
    <property role="TrG5h" value="EntityBiochemistryTestWithNumberResultReference" />
    <ref role="1TJDcQ" node="4V3GMfXJ6OE" resolve="BiochemistryTestWithNumberResultReference" />
    <node concept="1TJgyj" id="4V3GMfXrGDW" role="1TKVEi">
      <property role="IQ2ns" value="5675576922574080636" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="target" />
      <ref role="20lvS9" to="4v7t:5Wfdz$0qWIu" resolve="BiochemistryTest" />
      <ref role="20ksaX" to="7f9y:4QUW3efv2jv" resolve="target" />
    </node>
    <node concept="PrWs8" id="6LTgXmMD1Fb" role="PzmwI">
      <ref role="PrY4T" to="7f9y:6LTgXmMvpq9" resolve="IEntityReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="4V3GMfXst2L">
    <property role="EcuMT" value="5675576922574278833" />
    <property role="3GE5qa" value="aspects.references.biochemistryTest" />
    <property role="TrG5h" value="AspectBiochemistryTestWithNumberResultReference" />
    <ref role="1TJDcQ" node="4V3GMfXJ6OE" resolve="BiochemistryTestWithNumberResultReference" />
    <node concept="1TJgyj" id="4V3GMfXumVD" role="1TKVEi">
      <property role="IQ2ns" value="5675576922574778089" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="target" />
      <ref role="20ksaX" to="7f9y:6LTgXmMAS1z" resolve="target" />
      <ref role="20lvS9" node="4QUW3edEuTG" resolve="BiochemistryTestVariable" />
    </node>
    <node concept="PrWs8" id="6LTgXmMD1F5" role="PzmwI">
      <ref role="PrY4T" to="7f9y:6LTgXmMAn$t" resolve="IAspectVariableReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="4V3GMfXJ6OE">
    <property role="EcuMT" value="5675576922579168554" />
    <property role="3GE5qa" value="shared.references.biochemistryTest" />
    <property role="TrG5h" value="BiochemistryTestWithNumberResultReference" />
    <property role="R5$K7" value="true" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" to="ruww:4V3GMfXHy69" resolve="LaboratoryTestWithNumberResultReference" />
  </node>
  <node concept="1TIwiD" id="6LTgXmNRl3W">
    <property role="EcuMT" value="7816353213402927356" />
    <property role="3GE5qa" value="aspects.patterns" />
    <property role="TrG5h" value="BiochemistryTestPattern" />
    <ref role="1TJDcQ" to="ruww:6LTgXmNpAAy" resolve="LaboratoryTestPattern" />
  </node>
  <node concept="1TIwiD" id="6LTgXmNUZyu">
    <property role="EcuMT" value="7816353213403887774" />
    <property role="3GE5qa" value="aspects.patterns" />
    <property role="TrG5h" value="WildcardBiochemistryTestPattern" />
    <ref role="1TJDcQ" node="6LTgXmNRl3W" resolve="BiochemistryTestPattern" />
    <node concept="PrWs8" id="2FjKBCOhiQx" role="PzmwI">
      <ref role="PrY4T" to="7f9y:65epL7Ml5Dl" resolve="IWildcardPattern" />
    </node>
  </node>
</model>

