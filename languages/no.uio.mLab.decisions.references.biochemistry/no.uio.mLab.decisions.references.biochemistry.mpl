<?xml version="1.0" encoding="UTF-8"?>
<language namespace="no.uio.mLab.decisions.references.biochemistry" uuid="76146113-297e-4cbe-99cf-714ae175bb83" languageVersion="0" moduleVersion="0">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
  </models>
  <accessoryModels />
  <generators>
    <generator alias="main" namespace="no.uio.mLab.decisions.references.biochemistry#01" uuid="4a931145-1da0-4ad2-aa4a-46cc36fd9df4">
      <models>
        <modelRoot contentPath="${module}/generator/template" type="default">
          <sourceRoot location="." />
        </modelRoot>
      </models>
      <external-templates />
      <dependencies>
        <dependency reexport="false" scope="design">64d670b1-5071-4e14-8e60-d83a30068000(no.uio.mLab.decisions.references.specialistType#01)</dependency>
        <dependency reexport="false" scope="design">d341ef82-6312-4e96-b325-35a1d771e469(no.uio.mLab.decisions.references.laboratoryTest#01)</dependency>
      </dependencies>
      <languageVersions>
        <language slang="l:f3061a53-9226-4cc5-a443-f952ceaf5816:jetbrains.mps.baseLanguage" version="6" />
        <language slang="l:fd392034-7849-419d-9071-12563d152375:jetbrains.mps.baseLanguage.closures" version="0" />
        <language slang="l:83888646-71ce-4f1c-9c53-c54016f6ad4f:jetbrains.mps.baseLanguage.collections" version="0" />
        <language slang="l:f2801650-65d5-424e-bb1b-463a8781b786:jetbrains.mps.baseLanguage.javadoc" version="2" />
        <language slang="l:760a0a8c-eabb-4521-8bfd-65db761a9ba3:jetbrains.mps.baseLanguage.logging" version="0" />
        <language slang="l:a247e09e-2435-45ba-b8d2-07e93feba96a:jetbrains.mps.baseLanguage.tuples" version="0" />
        <language slang="l:ceab5195-25ea-4f22-9b92-103b95ca8c0c:jetbrains.mps.lang.core" version="1" />
        <language slang="l:b401a680-8325-4110-8fd3-84331ff25bef:jetbrains.mps.lang.generator" version="0" />
        <language slang="l:d7706f63-9be2-479c-a3da-ae92af1e64d5:jetbrains.mps.lang.generator.generationContext" version="0" />
        <language slang="l:289fcc83-6543-41e8-a5ca-768235715ce4:jetbrains.mps.lang.generator.generationParameters" version="0" />
        <language slang="l:446c26eb-2b7b-4bf0-9b35-f83fa582753e:jetbrains.mps.lang.modelapi" version="0" />
        <language slang="l:3a13115c-633c-4c5c-bbcc-75c4219e9555:jetbrains.mps.lang.quotation" version="0" />
        <language slang="l:13744753-c81f-424a-9c1b-cf8943bf4e86:jetbrains.mps.lang.sharedConcepts" version="0" />
        <language slang="l:7866978e-a0f0-4cc7-81bc-4d213d9375e1:jetbrains.mps.lang.smodel" version="11" />
        <language slang="l:c7fb639f-be78-4307-89b0-b5959c3fa8c8:jetbrains.mps.lang.text" version="0" />
        <language slang="l:9ded098b-ad6a-4657-bfd9-48636cfe8bc3:jetbrains.mps.lang.traceable" version="0" />
      </languageVersions>
      <dependencyVersions>
        <module reference="3f233e7f-b8a6-46d2-a57f-795d56775243(Annotations)" version="0" />
        <module reference="6354ebe7-c22a-4a0f-ac54-50b52ab9b065(JDK)" version="0" />
        <module reference="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea(MPS.Core)" version="0" />
        <module reference="8865b7a8-5271-43d3-884c-6fd1d9cfdd34(MPS.OpenAPI)" version="0" />
        <module reference="b4d28e19-7d2d-47e9-943e-3a41f97a0e52(com.mbeddr.mpsutil.plantuml.node)" version="0" />
        <module reference="23865718-e2ed-41b5-a132-0da1d04e266d(jetbrains.mps.ide.httpsupport.manager)" version="0" />
        <module reference="ae6d8005-36be-4cb6-945b-8c8cfc033c51(jetbrains.mps.ide.httpsupport.runtime)" version="0" />
        <module reference="ceab5195-25ea-4f22-9b92-103b95ca8c0c(jetbrains.mps.lang.core)" version="0" />
        <module reference="c610a4eb-c47c-4f95-b568-11236971769c(no.uio.mLab.decisions.core)" version="0" />
        <module reference="76146113-297e-4cbe-99cf-714ae175bb83(no.uio.mLab.decisions.references.biochemistry)" version="0" />
        <module reference="4a931145-1da0-4ad2-aa4a-46cc36fd9df4(no.uio.mLab.decisions.references.biochemistry#01)" version="0" />
        <module reference="912d06ac-ba61-4c9d-83de-64565f12e479(no.uio.mLab.decisions.references.biochemistry.runtime)" version="0" />
        <module reference="4a652d55-3684-4d2d-98c9-2ef46f124c44(no.uio.mLab.decisions.references.laboratoryTest)" version="0" />
        <module reference="d341ef82-6312-4e96-b325-35a1d771e469(no.uio.mLab.decisions.references.laboratoryTest#01)" version="0" />
        <module reference="0e664af0-ccb5-427d-94ac-6f90ac2b6424(no.uio.mLab.decisions.references.specialistType)" version="0" />
        <module reference="64d670b1-5071-4e14-8e60-d83a30068000(no.uio.mLab.decisions.references.specialistType#01)" version="0" />
        <module reference="18921d4c-b19f-48c6-88d6-cfedfa641899(no.uio.mLab.references.core)" version="0" />
        <module reference="6adcef38-5cdc-48dc-b225-be76276615fd(no.uio.mLab.shared)" version="0" />
      </dependencyVersions>
      <mapping-priorities>
        <mapping-priority-rule kind="strictly_together">
          <greater-priority-mapping>
            <generator generatorUID="4a931145-1da0-4ad2-aa4a-46cc36fd9df4(no.uio.mLab.decisions.references.biochemistry#01)" />
            <external-mapping>
              <mapping-node modelUID="r:70abd5f9-eec8-40f9-a5c3-7360f846285f(main@generator)" nodeID="5601053190796278878" />
            </external-mapping>
          </greater-priority-mapping>
          <lesser-priority-mapping>
            <mapping-set>
              <mapping-set-element>
                <generator generatorUID="64d670b1-5071-4e14-8e60-d83a30068000(no.uio.mLab.decisions.references.specialistType#01)" />
                <external-mapping>
                  <mapping-node modelUID="r:3092020d-ed76-440f-b5d2-eb8c0fa4a839(main@generator)" nodeID="5601053190799200192" />
                </external-mapping>
              </mapping-set-element>
              <mapping-set-element>
                <generator generatorUID="d341ef82-6312-4e96-b325-35a1d771e469(no.uio.mLab.decisions.references.laboratoryTest#01)" />
                <external-mapping>
                  <mapping-node modelUID="r:fc22de85-2e59-42fa-9fb8-b337e6546f50(main@generator)" nodeID="5601053190796232178" />
                </external-mapping>
              </mapping-set-element>
            </mapping-set>
          </lesser-priority-mapping>
        </mapping-priority-rule>
      </mapping-priorities>
    </generator>
  </generators>
  <sourcePath />
  <dependencies>
    <dependency reexport="false">284ce324-ec91-4245-a186-fefe6b9a1ea8(no.uio.mLab.entities.biochemistry)</dependency>
    <dependency reexport="false">32326140-de40-4a18-bd07-16491f087f22(no.uio.mLab.decisions.references.biochemistry.translations)</dependency>
    <dependency reexport="false">c610a4eb-c47c-4f95-b568-11236971769c(no.uio.mLab.decisions.core)</dependency>
    <dependency reexport="false">33c0f773-be29-48e3-9587-40dc4b3c54f0(no.uio.mLab.entities.core)</dependency>
  </dependencies>
  <languageVersions>
    <language slang="l:f3061a53-9226-4cc5-a443-f952ceaf5816:jetbrains.mps.baseLanguage" version="6" />
    <language slang="l:443f4c36-fcf5-4eb6-9500-8d06ed259e3e:jetbrains.mps.baseLanguage.classifiers" version="0" />
    <language slang="l:fd392034-7849-419d-9071-12563d152375:jetbrains.mps.baseLanguage.closures" version="0" />
    <language slang="l:83888646-71ce-4f1c-9c53-c54016f6ad4f:jetbrains.mps.baseLanguage.collections" version="0" />
    <language slang="l:f2801650-65d5-424e-bb1b-463a8781b786:jetbrains.mps.baseLanguage.javadoc" version="2" />
    <language slang="l:760a0a8c-eabb-4521-8bfd-65db761a9ba3:jetbrains.mps.baseLanguage.logging" version="0" />
    <language slang="l:a247e09e-2435-45ba-b8d2-07e93feba96a:jetbrains.mps.baseLanguage.tuples" version="0" />
    <language slang="l:aee9cad2-acd4-4608-aef2-0004f6a1cdbd:jetbrains.mps.lang.actions" version="4" />
    <language slang="l:af65afd8-f0dd-4942-87d9-63a55f2a9db1:jetbrains.mps.lang.behavior" version="1" />
    <language slang="l:3f4bc5f5-c6c1-4a28-8b10-c83066ffa4a1:jetbrains.mps.lang.constraints" version="4" />
    <language slang="l:ceab5195-25ea-4f22-9b92-103b95ca8c0c:jetbrains.mps.lang.core" version="1" />
    <language slang="l:f4ad079d-bc71-4ffb-9600-9328705cf998:jetbrains.mps.lang.descriptor" version="0" />
    <language slang="l:18bc6592-03a6-4e29-a83a-7ff23bde13ba:jetbrains.mps.lang.editor" version="11" />
    <language slang="l:446c26eb-2b7b-4bf0-9b35-f83fa582753e:jetbrains.mps.lang.modelapi" version="0" />
    <language slang="l:d4615e3b-d671-4ba9-af01-2b78369b0ba7:jetbrains.mps.lang.pattern" version="1" />
    <language slang="l:3a13115c-633c-4c5c-bbcc-75c4219e9555:jetbrains.mps.lang.quotation" version="0" />
    <language slang="l:982eb8df-2c96-4bd7-9963-11712ea622e5:jetbrains.mps.lang.resources" version="2" />
    <language slang="l:7866978e-a0f0-4cc7-81bc-4d213d9375e1:jetbrains.mps.lang.smodel" version="11" />
    <language slang="l:c72da2b9-7cce-4447-8389-f407dc1158b7:jetbrains.mps.lang.structure" version="6" />
    <language slang="l:c7fb639f-be78-4307-89b0-b5959c3fa8c8:jetbrains.mps.lang.text" version="0" />
    <language slang="l:9ded098b-ad6a-4657-bfd9-48636cfe8bc3:jetbrains.mps.lang.traceable" version="0" />
    <language slang="l:7a5dda62-9140-4668-ab76-d5ed1746f2b2:jetbrains.mps.lang.typesystem" version="1" />
  </languageVersions>
  <dependencyVersions>
    <module reference="3f233e7f-b8a6-46d2-a57f-795d56775243(Annotations)" version="0" />
    <module reference="6354ebe7-c22a-4a0f-ac54-50b52ab9b065(JDK)" version="0" />
    <module reference="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea(MPS.Core)" version="0" />
    <module reference="8865b7a8-5271-43d3-884c-6fd1d9cfdd34(MPS.OpenAPI)" version="0" />
    <module reference="b4d28e19-7d2d-47e9-943e-3a41f97a0e52(com.mbeddr.mpsutil.plantuml.node)" version="0" />
    <module reference="23865718-e2ed-41b5-a132-0da1d04e266d(jetbrains.mps.ide.httpsupport.manager)" version="0" />
    <module reference="ae6d8005-36be-4cb6-945b-8c8cfc033c51(jetbrains.mps.ide.httpsupport.runtime)" version="0" />
    <module reference="ceab5195-25ea-4f22-9b92-103b95ca8c0c(jetbrains.mps.lang.core)" version="0" />
    <module reference="a9e4c532-c5f5-4bb7-99ef-42abb73bbb70(jetbrains.mps.lang.descriptor.aspects)" version="0" />
    <module reference="c610a4eb-c47c-4f95-b568-11236971769c(no.uio.mLab.decisions.core)" version="0" />
    <module reference="76146113-297e-4cbe-99cf-714ae175bb83(no.uio.mLab.decisions.references.biochemistry)" version="0" />
    <module reference="32326140-de40-4a18-bd07-16491f087f22(no.uio.mLab.decisions.references.biochemistry.translations)" version="0" />
    <module reference="4a652d55-3684-4d2d-98c9-2ef46f124c44(no.uio.mLab.decisions.references.laboratoryTest)" version="0" />
    <module reference="0e664af0-ccb5-427d-94ac-6f90ac2b6424(no.uio.mLab.decisions.references.specialistType)" version="0" />
    <module reference="284ce324-ec91-4245-a186-fefe6b9a1ea8(no.uio.mLab.entities.biochemistry)" version="0" />
    <module reference="33c0f773-be29-48e3-9587-40dc4b3c54f0(no.uio.mLab.entities.core)" version="0" />
    <module reference="cae652fa-7f6e-4122-8bd5-0b26c8b4eec8(no.uio.mLab.entities.laboratoryTest)" version="0" />
    <module reference="0c42db4c-2bfa-4af0-b368-f89dcb171d67(no.uio.mLab.entities.specialistType)" version="0" />
    <module reference="18921d4c-b19f-48c6-88d6-cfedfa641899(no.uio.mLab.references.core)" version="0" />
    <module reference="6adcef38-5cdc-48dc-b225-be76276615fd(no.uio.mLab.shared)" version="0" />
  </dependencyVersions>
  <runtime>
    <dependency reexport="false">912d06ac-ba61-4c9d-83de-64565f12e479(no.uio.mLab.decisions.references.biochemistry.runtime)</dependency>
  </runtime>
  <extendedLanguages>
    <extendedLanguage>4a652d55-3684-4d2d-98c9-2ef46f124c44(no.uio.mLab.decisions.references.laboratoryTest)</extendedLanguage>
    <extendedLanguage>0e664af0-ccb5-427d-94ac-6f90ac2b6424(no.uio.mLab.decisions.references.specialistType)</extendedLanguage>
  </extendedLanguages>
</language>

