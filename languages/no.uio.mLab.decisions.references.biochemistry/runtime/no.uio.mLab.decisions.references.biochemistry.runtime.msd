<?xml version="1.0" encoding="UTF-8"?>
<solution name="no.uio.mLab.decisions.references.biochemistry.runtime" uuid="912d06ac-ba61-4c9d-83de-64565f12e479" moduleVersion="0" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
  </models>
  <sourcePath />
  <languageVersions />
  <dependencyVersions>
    <module reference="912d06ac-ba61-4c9d-83de-64565f12e479(no.uio.mLab.decisions.references.biochemistry.runtime)" version="0" />
  </dependencyVersions>
</solution>

