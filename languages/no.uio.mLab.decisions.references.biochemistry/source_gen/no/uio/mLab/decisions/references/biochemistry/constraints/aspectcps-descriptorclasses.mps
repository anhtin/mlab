<?xml version="1.0" encoding="UTF-8"?>
<model ref="00000000-0000-4000-5f02-5beb5f025beb/i:fbaf59e(checkpoints/no.uio.mLab.decisions.references.biochemistry.constraints@descriptorclasses)">
  <persistence version="9" />
  <attribute name="checkpoint" value="DescriptorClasses" />
  <attribute name="generation-plan" value="AspectCPS" />
  <languages />
  <imports>
    <import index="n2tl" ref="r:62707f82-6ccf-4b1b-b39a-a18182073111(no.uio.mLab.decisions.references.biochemistry.constraints)" />
    <import index="c17a" ref="8865b7a8-5271-43d3-884c-6fd1d9cfdd34/java:org.jetbrains.mps.openapi.language(MPS.OpenAPI/)" />
    <import index="flit" ref="r:a6507f20-2eae-4f1e-aa64-edce1d2dcb45(no.uio.mLab.decisions.references.biochemistry.structure)" />
    <import index="2k9e" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.smodel.adapter.structure(MPS.Core/)" />
    <import index="ze1i" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.smodel.runtime(MPS.Core/)" />
    <import index="e8bb" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.smodel.adapter.ids(MPS.Core/)" />
    <import index="33ny" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.util(JDK/)" />
    <import index="79pl" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.smodel.runtime.base(MPS.Core/)" />
    <import index="35tq" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.scope(MPS.Core/)" />
    <import index="mhfm" ref="3f233e7f-b8a6-46d2-a57f-795d56775243/java:org.jetbrains.annotations(Annotations/)" />
    <import index="mhbf" ref="8865b7a8-5271-43d3-884c-6fd1d9cfdd34/java:org.jetbrains.mps.openapi.model(MPS.OpenAPI/)" />
    <import index="w1kc" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.smodel(MPS.Core/)" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1082485599095" name="jetbrains.mps.baseLanguage.structure.BlockStatement" flags="nn" index="9aQIb">
        <child id="1082485599096" name="statements" index="9aQI4" />
      </concept>
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1465982738277781862" name="jetbrains.mps.baseLanguage.structure.PlaceholderMember" flags="ng" index="2tJIrI" />
      <concept id="1188207840427" name="jetbrains.mps.baseLanguage.structure.AnnotationInstance" flags="nn" index="2AHcQZ">
        <reference id="1188208074048" name="annotation" index="2AI5Lk" />
      </concept>
      <concept id="1188208481402" name="jetbrains.mps.baseLanguage.structure.HasAnnotation" flags="ng" index="2AJDlI">
        <child id="1188208488637" name="annotation" index="2AJF6D" />
      </concept>
      <concept id="1224848483129" name="jetbrains.mps.baseLanguage.structure.IBLDeprecatable" flags="ng" index="IEa8$">
        <property id="1224848525476" name="isDeprecated" index="IEkAT" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1070475354124" name="jetbrains.mps.baseLanguage.structure.ThisExpression" flags="nn" index="Xjq3P" />
      <concept id="1070475587102" name="jetbrains.mps.baseLanguage.structure.SuperConstructorInvocation" flags="nn" index="XkiVB" />
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1182160077978" name="jetbrains.mps.baseLanguage.structure.AnonymousClassCreator" flags="nn" index="YeOm9">
        <child id="1182160096073" name="cls" index="YeSDq" />
      </concept>
      <concept id="1081236700938" name="jetbrains.mps.baseLanguage.structure.StaticMethodDeclaration" flags="ig" index="2YIFZL" />
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1070534058343" name="jetbrains.mps.baseLanguage.structure.NullLiteral" flags="nn" index="10Nm6u" />
      <concept id="1070534644030" name="jetbrains.mps.baseLanguage.structure.BooleanType" flags="in" index="10P_77" />
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu">
        <child id="1165602531693" name="superclass" index="1zkMxy" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <property id="1176718929932" name="isFinal" index="3TUv4t" />
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="4269842503726207156" name="jetbrains.mps.baseLanguage.structure.LongLiteral" flags="nn" index="1adDum">
        <property id="4269842503726207157" name="value" index="1adDun" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <property id="4276006055363816570" name="isSynchronized" index="od$2w" />
        <property id="1181808852946" name="isFinal" index="DiZV1" />
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123165" name="jetbrains.mps.baseLanguage.structure.InstanceMethodDeclaration" flags="ig" index="3clFb_">
        <property id="1178608670077" name="isAbstract" index="1EzhhJ" />
      </concept>
      <concept id="1068580123152" name="jetbrains.mps.baseLanguage.structure.EqualsExpression" flags="nn" index="3clFbC" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123137" name="jetbrains.mps.baseLanguage.structure.BooleanConstant" flags="nn" index="3clFbT">
        <property id="1068580123138" name="value" index="3clFbU" />
      </concept>
      <concept id="1068580123140" name="jetbrains.mps.baseLanguage.structure.ConstructorDeclaration" flags="ig" index="3clFbW" />
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1068581517677" name="jetbrains.mps.baseLanguage.structure.VoidType" flags="in" index="3cqZAl" />
      <concept id="1081516740877" name="jetbrains.mps.baseLanguage.structure.NotExpression" flags="nn" index="3fqX7Q">
        <child id="1081516765348" name="expression" index="3fr31v" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1212685548494" name="jetbrains.mps.baseLanguage.structure.ClassCreator" flags="nn" index="1pGfFk">
        <child id="1212687122400" name="typeParameter" index="1pMfVU" />
      </concept>
      <concept id="1107461130800" name="jetbrains.mps.baseLanguage.structure.Classifier" flags="ng" index="3pOWGL">
        <property id="521412098689998745" name="nonStatic" index="2bfB8j" />
        <child id="5375687026011219971" name="member" index="jymVt" unordered="true" />
      </concept>
      <concept id="7812454656619025412" name="jetbrains.mps.baseLanguage.structure.LocalMethodCall" flags="nn" index="1rXfSq" />
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
        <child id="1109201940907" name="parameter" index="11_B2D" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1073239437375" name="jetbrains.mps.baseLanguage.structure.NotEqualsExpression" flags="nn" index="3y3z36" />
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1163668896201" name="jetbrains.mps.baseLanguage.structure.TernaryOperatorExpression" flags="nn" index="3K4zz7">
        <child id="1163668914799" name="condition" index="3K4Cdx" />
        <child id="1163668922816" name="ifTrue" index="3K4E3e" />
        <child id="1163668934364" name="ifFalse" index="3K4GZi" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
      <concept id="1146644623116" name="jetbrains.mps.baseLanguage.structure.PrivateVisibility" flags="nn" index="3Tm6S6" />
      <concept id="1146644641414" name="jetbrains.mps.baseLanguage.structure.ProtectedVisibility" flags="nn" index="3Tmbuc" />
      <concept id="1080120340718" name="jetbrains.mps.baseLanguage.structure.AndExpression" flags="nn" index="1Wc70l" />
      <concept id="1170345865475" name="jetbrains.mps.baseLanguage.structure.AnonymousClass" flags="ig" index="1Y3b0j">
        <reference id="1170346070688" name="classifier" index="1Y3XeK" />
        <child id="1201186121363" name="typeParameter" index="2Ghqu4" />
      </concept>
    </language>
    <language id="b401a680-8325-4110-8fd3-84331ff25bef" name="jetbrains.mps.lang.generator">
      <concept id="7980339663309897032" name="jetbrains.mps.lang.generator.structure.OriginTrace" flags="ng" index="cd27G">
        <child id="7980339663309897037" name="origin" index="cd27D" />
      </concept>
      <concept id="3864140621129707969" name="jetbrains.mps.lang.generator.structure.GeneratorDebug_Mappings" flags="nn" index="39dXUE" />
      <concept id="3637169702552512264" name="jetbrains.mps.lang.generator.structure.ElementaryNodeId" flags="ng" index="3u3nmq">
        <property id="3637169702552512269" name="nodeId" index="3u3nmv" />
      </concept>
    </language>
    <language id="df345b11-b8c7-4213-ac66-48d2a9b75d88" name="jetbrains.mps.baseLanguageInternal">
      <concept id="1238251434034" name="jetbrains.mps.baseLanguageInternal.structure.ExtractToConstantExpression" flags="ng" index="1dyn4i">
        <property id="1238251449050" name="fieldName" index="1dyqJU" />
        <property id="8835849473318867199" name="makeUnique" index="1zomUR" />
        <child id="1238251454130" name="expression" index="1dyrYi" />
      </concept>
      <concept id="1173996401517" name="jetbrains.mps.baseLanguageInternal.structure.InternalNewExpression" flags="nn" index="1nCR9W">
        <property id="1173996588177" name="fqClassName" index="1nD$Q0" />
        <child id="1179332974947" name="type" index="2lIhxL" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="2644386474300074836" name="jetbrains.mps.lang.smodel.structure.ConceptIdRefExpression" flags="nn" index="35c_gC">
        <reference id="2644386474300074837" name="conceptDeclaration" index="35c_gD" />
      </concept>
      <concept id="6677504323281689838" name="jetbrains.mps.lang.smodel.structure.SConceptType" flags="in" index="3bZ5Sz" />
      <concept id="1154546950173" name="jetbrains.mps.lang.smodel.structure.ConceptReference" flags="ng" index="3gn64h">
        <reference id="1154546997487" name="concept" index="3gnhBz" />
      </concept>
      <concept id="1139621453865" name="jetbrains.mps.lang.smodel.structure.Node_IsInstanceOfOperation" flags="nn" index="1mIQ4w">
        <child id="1177027386292" name="conceptArgument" index="cj9EA" />
      </concept>
      <concept id="6039268229364358244" name="jetbrains.mps.lang.smodel.structure.ExactConceptCase" flags="ng" index="1pnPoh">
        <child id="6039268229364358388" name="body" index="1pnPq1" />
        <child id="6039268229364358387" name="concept" index="1pnPq6" />
      </concept>
      <concept id="5944356402132808749" name="jetbrains.mps.lang.smodel.structure.ConceptSwitchStatement" flags="nn" index="1_3QMa">
        <child id="6039268229365417680" name="defaultBlock" index="1prKM_" />
        <child id="5944356402132808753" name="case" index="1_3QMm" />
        <child id="5944356402132808752" name="expression" index="1_3QMn" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="312cEu" id="0">
    <property role="3GE5qa" value="aspects.references.biochemistryTest" />
    <property role="TrG5h" value="AspectBiochemistryTestReference_Constraints" />
    <node concept="3Tm1VV" id="1" role="1B3o_S">
      <node concept="cd27G" id="7" role="lGtFl">
        <node concept="3u3nmq" id="8" role="cd27D">
          <property role="3u3nmv" value="5601053190833464083" />
        </node>
      </node>
    </node>
    <node concept="3uibUv" id="2" role="1zkMxy">
      <ref role="3uigEE" to="79pl:~BaseConstraintsDescriptor" resolve="BaseConstraintsDescriptor" />
      <node concept="cd27G" id="9" role="lGtFl">
        <node concept="3u3nmq" id="a" role="cd27D">
          <property role="3u3nmv" value="5601053190833464083" />
        </node>
      </node>
    </node>
    <node concept="3clFbW" id="3" role="jymVt">
      <node concept="3cqZAl" id="b" role="3clF45">
        <node concept="cd27G" id="f" role="lGtFl">
          <node concept="3u3nmq" id="g" role="cd27D">
            <property role="3u3nmv" value="5601053190833464083" />
          </node>
        </node>
      </node>
      <node concept="3clFbS" id="c" role="3clF47">
        <node concept="XkiVB" id="h" role="3cqZAp">
          <ref role="37wK5l" to="79pl:~BaseConstraintsDescriptor.&lt;init&gt;(org.jetbrains.mps.openapi.language.SAbstractConcept)" resolve="BaseConstraintsDescriptor" />
          <node concept="2YIFZM" id="j" role="37wK5m">
            <ref role="1Pybhc" to="2k9e:~MetaAdapterFactory" resolve="MetaAdapterFactory" />
            <ref role="37wK5l" to="2k9e:~MetaAdapterFactory.getConcept(long,long,long,java.lang.String):org.jetbrains.mps.openapi.language.SConcept" resolve="getConcept" />
            <node concept="1adDum" id="l" role="37wK5m">
              <property role="1adDun" value="0x76146113297e4cbeL" />
              <node concept="cd27G" id="q" role="lGtFl">
                <node concept="3u3nmq" id="r" role="cd27D">
                  <property role="3u3nmv" value="5601053190833464083" />
                </node>
              </node>
            </node>
            <node concept="1adDum" id="m" role="37wK5m">
              <property role="1adDun" value="0x99cf714ae175bb83L" />
              <node concept="cd27G" id="s" role="lGtFl">
                <node concept="3u3nmq" id="t" role="cd27D">
                  <property role="3u3nmv" value="5601053190833464083" />
                </node>
              </node>
            </node>
            <node concept="1adDum" id="n" role="37wK5m">
              <property role="1adDun" value="0x4dbaf0338f828787L" />
              <node concept="cd27G" id="u" role="lGtFl">
                <node concept="3u3nmq" id="v" role="cd27D">
                  <property role="3u3nmv" value="5601053190833464083" />
                </node>
              </node>
            </node>
            <node concept="Xl_RD" id="o" role="37wK5m">
              <property role="Xl_RC" value="no.uio.mLab.decisions.references.biochemistry.structure.AspectBiochemistryTestReference" />
              <node concept="cd27G" id="w" role="lGtFl">
                <node concept="3u3nmq" id="x" role="cd27D">
                  <property role="3u3nmv" value="5601053190833464083" />
                </node>
              </node>
            </node>
            <node concept="cd27G" id="p" role="lGtFl">
              <node concept="3u3nmq" id="y" role="cd27D">
                <property role="3u3nmv" value="5601053190833464083" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="k" role="lGtFl">
            <node concept="3u3nmq" id="z" role="cd27D">
              <property role="3u3nmv" value="5601053190833464083" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="i" role="lGtFl">
          <node concept="3u3nmq" id="$" role="cd27D">
            <property role="3u3nmv" value="5601053190833464083" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="d" role="1B3o_S">
        <node concept="cd27G" id="_" role="lGtFl">
          <node concept="3u3nmq" id="A" role="cd27D">
            <property role="3u3nmv" value="5601053190833464083" />
          </node>
        </node>
      </node>
      <node concept="cd27G" id="e" role="lGtFl">
        <node concept="3u3nmq" id="B" role="cd27D">
          <property role="3u3nmv" value="5601053190833464083" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="4" role="jymVt">
      <node concept="cd27G" id="C" role="lGtFl">
        <node concept="3u3nmq" id="D" role="cd27D">
          <property role="3u3nmv" value="5601053190833464083" />
        </node>
      </node>
    </node>
    <node concept="3clFb_" id="5" role="jymVt">
      <property role="IEkAT" value="false" />
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="getSpecifiedReferences" />
      <property role="DiZV1" value="false" />
      <node concept="3Tmbuc" id="E" role="1B3o_S">
        <node concept="cd27G" id="J" role="lGtFl">
          <node concept="3u3nmq" id="K" role="cd27D">
            <property role="3u3nmv" value="5601053190833464083" />
          </node>
        </node>
      </node>
      <node concept="3uibUv" id="F" role="3clF45">
        <ref role="3uigEE" to="33ny:~Map" resolve="Map" />
        <node concept="3uibUv" id="L" role="11_B2D">
          <ref role="3uigEE" to="c17a:~SReferenceLink" resolve="SReferenceLink" />
          <node concept="cd27G" id="O" role="lGtFl">
            <node concept="3u3nmq" id="P" role="cd27D">
              <property role="3u3nmv" value="5601053190833464083" />
            </node>
          </node>
        </node>
        <node concept="3uibUv" id="M" role="11_B2D">
          <ref role="3uigEE" to="ze1i:~ReferenceConstraintsDescriptor" resolve="ReferenceConstraintsDescriptor" />
          <node concept="cd27G" id="Q" role="lGtFl">
            <node concept="3u3nmq" id="R" role="cd27D">
              <property role="3u3nmv" value="5601053190833464083" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="N" role="lGtFl">
          <node concept="3u3nmq" id="S" role="cd27D">
            <property role="3u3nmv" value="5601053190833464083" />
          </node>
        </node>
      </node>
      <node concept="3clFbS" id="G" role="3clF47">
        <node concept="3cpWs8" id="T" role="3cqZAp">
          <node concept="3cpWsn" id="X" role="3cpWs9">
            <property role="TrG5h" value="references" />
            <node concept="3uibUv" id="Z" role="1tU5fm">
              <ref role="3uigEE" to="33ny:~Map" resolve="Map" />
              <node concept="3uibUv" id="12" role="11_B2D">
                <ref role="3uigEE" to="c17a:~SReferenceLink" resolve="SReferenceLink" />
                <node concept="cd27G" id="15" role="lGtFl">
                  <node concept="3u3nmq" id="16" role="cd27D">
                    <property role="3u3nmv" value="5601053190833464083" />
                  </node>
                </node>
              </node>
              <node concept="3uibUv" id="13" role="11_B2D">
                <ref role="3uigEE" to="ze1i:~ReferenceConstraintsDescriptor" resolve="ReferenceConstraintsDescriptor" />
                <node concept="cd27G" id="17" role="lGtFl">
                  <node concept="3u3nmq" id="18" role="cd27D">
                    <property role="3u3nmv" value="5601053190833464083" />
                  </node>
                </node>
              </node>
              <node concept="cd27G" id="14" role="lGtFl">
                <node concept="3u3nmq" id="19" role="cd27D">
                  <property role="3u3nmv" value="5601053190833464083" />
                </node>
              </node>
            </node>
            <node concept="2ShNRf" id="10" role="33vP2m">
              <node concept="1pGfFk" id="1a" role="2ShVmc">
                <ref role="37wK5l" to="33ny:~HashMap.&lt;init&gt;()" resolve="HashMap" />
                <node concept="3uibUv" id="1c" role="1pMfVU">
                  <ref role="3uigEE" to="c17a:~SReferenceLink" resolve="SReferenceLink" />
                  <node concept="cd27G" id="1f" role="lGtFl">
                    <node concept="3u3nmq" id="1g" role="cd27D">
                      <property role="3u3nmv" value="5601053190833464083" />
                    </node>
                  </node>
                </node>
                <node concept="3uibUv" id="1d" role="1pMfVU">
                  <ref role="3uigEE" to="ze1i:~ReferenceConstraintsDescriptor" resolve="ReferenceConstraintsDescriptor" />
                  <node concept="cd27G" id="1h" role="lGtFl">
                    <node concept="3u3nmq" id="1i" role="cd27D">
                      <property role="3u3nmv" value="5601053190833464083" />
                    </node>
                  </node>
                </node>
                <node concept="cd27G" id="1e" role="lGtFl">
                  <node concept="3u3nmq" id="1j" role="cd27D">
                    <property role="3u3nmv" value="5601053190833464083" />
                  </node>
                </node>
              </node>
              <node concept="cd27G" id="1b" role="lGtFl">
                <node concept="3u3nmq" id="1k" role="cd27D">
                  <property role="3u3nmv" value="5601053190833464083" />
                </node>
              </node>
            </node>
            <node concept="cd27G" id="11" role="lGtFl">
              <node concept="3u3nmq" id="1l" role="cd27D">
                <property role="3u3nmv" value="5601053190833464083" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="Y" role="lGtFl">
            <node concept="3u3nmq" id="1m" role="cd27D">
              <property role="3u3nmv" value="5601053190833464083" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="U" role="3cqZAp">
          <node concept="2OqwBi" id="1n" role="3clFbG">
            <node concept="37vLTw" id="1p" role="2Oq$k0">
              <ref role="3cqZAo" node="X" resolve="references" />
              <node concept="cd27G" id="1s" role="lGtFl">
                <node concept="3u3nmq" id="1t" role="cd27D">
                  <property role="3u3nmv" value="5601053190833464083" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="1q" role="2OqNvi">
              <ref role="37wK5l" to="33ny:~Map.put(java.lang.Object,java.lang.Object):java.lang.Object" resolve="put" />
              <node concept="2YIFZM" id="1u" role="37wK5m">
                <ref role="1Pybhc" to="2k9e:~MetaAdapterFactory" resolve="MetaAdapterFactory" />
                <ref role="37wK5l" to="2k9e:~MetaAdapterFactory.getReferenceLink(long,long,long,long,java.lang.String):org.jetbrains.mps.openapi.language.SReferenceLink" resolve="getReferenceLink" />
                <node concept="1adDum" id="1x" role="37wK5m">
                  <property role="1adDun" value="0xc610a4ebc47c4f95L" />
                  <node concept="cd27G" id="1B" role="lGtFl">
                    <node concept="3u3nmq" id="1C" role="cd27D">
                      <property role="3u3nmv" value="5601053190833464083" />
                    </node>
                  </node>
                </node>
                <node concept="1adDum" id="1y" role="37wK5m">
                  <property role="1adDun" value="0xb56811236971769cL" />
                  <node concept="cd27G" id="1D" role="lGtFl">
                    <node concept="3u3nmq" id="1E" role="cd27D">
                      <property role="3u3nmv" value="5601053190833464083" />
                    </node>
                  </node>
                </node>
                <node concept="1adDum" id="1z" role="37wK5m">
                  <property role="1adDun" value="0x6c7943d5b299791dL" />
                  <node concept="cd27G" id="1F" role="lGtFl">
                    <node concept="3u3nmq" id="1G" role="cd27D">
                      <property role="3u3nmv" value="5601053190833464083" />
                    </node>
                  </node>
                </node>
                <node concept="1adDum" id="1$" role="37wK5m">
                  <property role="1adDun" value="0x6c7943d5b29b8063L" />
                  <node concept="cd27G" id="1H" role="lGtFl">
                    <node concept="3u3nmq" id="1I" role="cd27D">
                      <property role="3u3nmv" value="5601053190833464083" />
                    </node>
                  </node>
                </node>
                <node concept="Xl_RD" id="1_" role="37wK5m">
                  <property role="Xl_RC" value="target" />
                  <node concept="cd27G" id="1J" role="lGtFl">
                    <node concept="3u3nmq" id="1K" role="cd27D">
                      <property role="3u3nmv" value="5601053190833464083" />
                    </node>
                  </node>
                </node>
                <node concept="cd27G" id="1A" role="lGtFl">
                  <node concept="3u3nmq" id="1L" role="cd27D">
                    <property role="3u3nmv" value="5601053190833464083" />
                  </node>
                </node>
              </node>
              <node concept="2ShNRf" id="1v" role="37wK5m">
                <node concept="YeOm9" id="1M" role="2ShVmc">
                  <node concept="1Y3b0j" id="1O" role="YeSDq">
                    <property role="2bfB8j" value="true" />
                    <ref role="1Y3XeK" to="79pl:~BaseReferenceConstraintsDescriptor" resolve="BaseReferenceConstraintsDescriptor" />
                    <ref role="37wK5l" to="79pl:~BaseReferenceConstraintsDescriptor.&lt;init&gt;(jetbrains.mps.smodel.adapter.ids.SReferenceLinkId,jetbrains.mps.smodel.runtime.ConstraintsDescriptor)" resolve="BaseReferenceConstraintsDescriptor" />
                    <node concept="2YIFZM" id="1Q" role="37wK5m">
                      <ref role="1Pybhc" to="e8bb:~MetaIdFactory" resolve="MetaIdFactory" />
                      <ref role="37wK5l" to="e8bb:~MetaIdFactory.refId(long,long,long,long):jetbrains.mps.smodel.adapter.ids.SReferenceLinkId" resolve="refId" />
                      <node concept="1adDum" id="1W" role="37wK5m">
                        <property role="1adDun" value="0xc610a4ebc47c4f95L" />
                        <node concept="cd27G" id="21" role="lGtFl">
                          <node concept="3u3nmq" id="22" role="cd27D">
                            <property role="3u3nmv" value="5601053190833464083" />
                          </node>
                        </node>
                      </node>
                      <node concept="1adDum" id="1X" role="37wK5m">
                        <property role="1adDun" value="0xb56811236971769cL" />
                        <node concept="cd27G" id="23" role="lGtFl">
                          <node concept="3u3nmq" id="24" role="cd27D">
                            <property role="3u3nmv" value="5601053190833464083" />
                          </node>
                        </node>
                      </node>
                      <node concept="1adDum" id="1Y" role="37wK5m">
                        <property role="1adDun" value="0x6c7943d5b299791dL" />
                        <node concept="cd27G" id="25" role="lGtFl">
                          <node concept="3u3nmq" id="26" role="cd27D">
                            <property role="3u3nmv" value="5601053190833464083" />
                          </node>
                        </node>
                      </node>
                      <node concept="1adDum" id="1Z" role="37wK5m">
                        <property role="1adDun" value="0x6c7943d5b29b8063L" />
                        <node concept="cd27G" id="27" role="lGtFl">
                          <node concept="3u3nmq" id="28" role="cd27D">
                            <property role="3u3nmv" value="5601053190833464083" />
                          </node>
                        </node>
                      </node>
                      <node concept="cd27G" id="20" role="lGtFl">
                        <node concept="3u3nmq" id="29" role="cd27D">
                          <property role="3u3nmv" value="5601053190833464083" />
                        </node>
                      </node>
                    </node>
                    <node concept="3Tm1VV" id="1R" role="1B3o_S">
                      <node concept="cd27G" id="2a" role="lGtFl">
                        <node concept="3u3nmq" id="2b" role="cd27D">
                          <property role="3u3nmv" value="5601053190833464083" />
                        </node>
                      </node>
                    </node>
                    <node concept="Xjq3P" id="1S" role="37wK5m">
                      <node concept="cd27G" id="2c" role="lGtFl">
                        <node concept="3u3nmq" id="2d" role="cd27D">
                          <property role="3u3nmv" value="5601053190833464083" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFb_" id="1T" role="jymVt">
                      <property role="IEkAT" value="false" />
                      <property role="1EzhhJ" value="false" />
                      <property role="TrG5h" value="hasOwnScopeProvider" />
                      <property role="DiZV1" value="false" />
                      <node concept="3Tm1VV" id="2e" role="1B3o_S">
                        <node concept="cd27G" id="2j" role="lGtFl">
                          <node concept="3u3nmq" id="2k" role="cd27D">
                            <property role="3u3nmv" value="5601053190833464083" />
                          </node>
                        </node>
                      </node>
                      <node concept="10P_77" id="2f" role="3clF45">
                        <node concept="cd27G" id="2l" role="lGtFl">
                          <node concept="3u3nmq" id="2m" role="cd27D">
                            <property role="3u3nmv" value="5601053190833464083" />
                          </node>
                        </node>
                      </node>
                      <node concept="3clFbS" id="2g" role="3clF47">
                        <node concept="3clFbF" id="2n" role="3cqZAp">
                          <node concept="3clFbT" id="2p" role="3clFbG">
                            <property role="3clFbU" value="true" />
                            <node concept="cd27G" id="2r" role="lGtFl">
                              <node concept="3u3nmq" id="2s" role="cd27D">
                                <property role="3u3nmv" value="5601053190833464083" />
                              </node>
                            </node>
                          </node>
                          <node concept="cd27G" id="2q" role="lGtFl">
                            <node concept="3u3nmq" id="2t" role="cd27D">
                              <property role="3u3nmv" value="5601053190833464083" />
                            </node>
                          </node>
                        </node>
                        <node concept="cd27G" id="2o" role="lGtFl">
                          <node concept="3u3nmq" id="2u" role="cd27D">
                            <property role="3u3nmv" value="5601053190833464083" />
                          </node>
                        </node>
                      </node>
                      <node concept="2AHcQZ" id="2h" role="2AJF6D">
                        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
                        <node concept="cd27G" id="2v" role="lGtFl">
                          <node concept="3u3nmq" id="2w" role="cd27D">
                            <property role="3u3nmv" value="5601053190833464083" />
                          </node>
                        </node>
                      </node>
                      <node concept="cd27G" id="2i" role="lGtFl">
                        <node concept="3u3nmq" id="2x" role="cd27D">
                          <property role="3u3nmv" value="5601053190833464083" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFb_" id="1U" role="jymVt">
                      <property role="IEkAT" value="false" />
                      <property role="1EzhhJ" value="false" />
                      <property role="TrG5h" value="getScopeProvider" />
                      <property role="DiZV1" value="false" />
                      <node concept="3Tm1VV" id="2y" role="1B3o_S">
                        <node concept="cd27G" id="2C" role="lGtFl">
                          <node concept="3u3nmq" id="2D" role="cd27D">
                            <property role="3u3nmv" value="5601053190833464083" />
                          </node>
                        </node>
                      </node>
                      <node concept="3uibUv" id="2z" role="3clF45">
                        <ref role="3uigEE" to="ze1i:~ReferenceScopeProvider" resolve="ReferenceScopeProvider" />
                        <node concept="cd27G" id="2E" role="lGtFl">
                          <node concept="3u3nmq" id="2F" role="cd27D">
                            <property role="3u3nmv" value="5601053190833464083" />
                          </node>
                        </node>
                      </node>
                      <node concept="2AHcQZ" id="2$" role="2AJF6D">
                        <ref role="2AI5Lk" to="mhfm:~Nullable" resolve="Nullable" />
                        <node concept="cd27G" id="2G" role="lGtFl">
                          <node concept="3u3nmq" id="2H" role="cd27D">
                            <property role="3u3nmv" value="5601053190833464083" />
                          </node>
                        </node>
                      </node>
                      <node concept="3clFbS" id="2_" role="3clF47">
                        <node concept="3cpWs6" id="2I" role="3cqZAp">
                          <node concept="2ShNRf" id="2K" role="3cqZAk">
                            <node concept="YeOm9" id="2M" role="2ShVmc">
                              <node concept="1Y3b0j" id="2O" role="YeSDq">
                                <property role="2bfB8j" value="true" />
                                <ref role="1Y3XeK" to="79pl:~BaseScopeProvider" resolve="BaseScopeProvider" />
                                <ref role="37wK5l" to="79pl:~BaseScopeProvider.&lt;init&gt;()" resolve="BaseScopeProvider" />
                                <node concept="3Tm1VV" id="2Q" role="1B3o_S">
                                  <node concept="cd27G" id="2U" role="lGtFl">
                                    <node concept="3u3nmq" id="2V" role="cd27D">
                                      <property role="3u3nmv" value="5601053190833464083" />
                                    </node>
                                  </node>
                                </node>
                                <node concept="3clFb_" id="2R" role="jymVt">
                                  <property role="TrG5h" value="getSearchScopeValidatorNode" />
                                  <node concept="3Tm1VV" id="2W" role="1B3o_S">
                                    <node concept="cd27G" id="31" role="lGtFl">
                                      <node concept="3u3nmq" id="32" role="cd27D">
                                        <property role="3u3nmv" value="5601053190833464083" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="3clFbS" id="2X" role="3clF47">
                                    <node concept="3cpWs6" id="33" role="3cqZAp">
                                      <node concept="1dyn4i" id="35" role="3cqZAk">
                                        <property role="1zomUR" value="true" />
                                        <property role="1dyqJU" value="breakingNode" />
                                        <node concept="2ShNRf" id="37" role="1dyrYi">
                                          <node concept="1pGfFk" id="39" role="2ShVmc">
                                            <ref role="37wK5l" to="w1kc:~SNodePointer.&lt;init&gt;(java.lang.String,java.lang.String)" resolve="SNodePointer" />
                                            <node concept="Xl_RD" id="3b" role="37wK5m">
                                              <property role="Xl_RC" value="r:62707f82-6ccf-4b1b-b39a-a18182073111(no.uio.mLab.decisions.references.biochemistry.constraints)" />
                                              <node concept="cd27G" id="3e" role="lGtFl">
                                                <node concept="3u3nmq" id="3f" role="cd27D">
                                                  <property role="3u3nmv" value="5601053190833464083" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="Xl_RD" id="3c" role="37wK5m">
                                              <property role="Xl_RC" value="5601053190833464085" />
                                              <node concept="cd27G" id="3g" role="lGtFl">
                                                <node concept="3u3nmq" id="3h" role="cd27D">
                                                  <property role="3u3nmv" value="5601053190833464083" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="cd27G" id="3d" role="lGtFl">
                                              <node concept="3u3nmq" id="3i" role="cd27D">
                                                <property role="3u3nmv" value="5601053190833464083" />
                                              </node>
                                            </node>
                                          </node>
                                          <node concept="cd27G" id="3a" role="lGtFl">
                                            <node concept="3u3nmq" id="3j" role="cd27D">
                                              <property role="3u3nmv" value="5601053190833464083" />
                                            </node>
                                          </node>
                                        </node>
                                        <node concept="cd27G" id="38" role="lGtFl">
                                          <node concept="3u3nmq" id="3k" role="cd27D">
                                            <property role="3u3nmv" value="5601053190833464083" />
                                          </node>
                                        </node>
                                      </node>
                                      <node concept="cd27G" id="36" role="lGtFl">
                                        <node concept="3u3nmq" id="3l" role="cd27D">
                                          <property role="3u3nmv" value="5601053190833464083" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="cd27G" id="34" role="lGtFl">
                                      <node concept="3u3nmq" id="3m" role="cd27D">
                                        <property role="3u3nmv" value="5601053190833464083" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="3uibUv" id="2Y" role="3clF45">
                                    <ref role="3uigEE" to="mhbf:~SNodeReference" resolve="SNodeReference" />
                                    <node concept="cd27G" id="3n" role="lGtFl">
                                      <node concept="3u3nmq" id="3o" role="cd27D">
                                        <property role="3u3nmv" value="5601053190833464083" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="2AHcQZ" id="2Z" role="2AJF6D">
                                    <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
                                    <node concept="cd27G" id="3p" role="lGtFl">
                                      <node concept="3u3nmq" id="3q" role="cd27D">
                                        <property role="3u3nmv" value="5601053190833464083" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="cd27G" id="30" role="lGtFl">
                                    <node concept="3u3nmq" id="3r" role="cd27D">
                                      <property role="3u3nmv" value="5601053190833464083" />
                                    </node>
                                  </node>
                                </node>
                                <node concept="3clFb_" id="2S" role="jymVt">
                                  <property role="IEkAT" value="false" />
                                  <property role="1EzhhJ" value="false" />
                                  <property role="TrG5h" value="createScope" />
                                  <property role="DiZV1" value="false" />
                                  <node concept="37vLTG" id="3s" role="3clF46">
                                    <property role="TrG5h" value="operationContext" />
                                    <property role="3TUv4t" value="true" />
                                    <node concept="3uibUv" id="3z" role="1tU5fm">
                                      <ref role="3uigEE" to="w1kc:~IOperationContext" resolve="IOperationContext" />
                                      <node concept="cd27G" id="3_" role="lGtFl">
                                        <node concept="3u3nmq" id="3A" role="cd27D">
                                          <property role="3u3nmv" value="5601053190833464083" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="cd27G" id="3$" role="lGtFl">
                                      <node concept="3u3nmq" id="3B" role="cd27D">
                                        <property role="3u3nmv" value="5601053190833464083" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="37vLTG" id="3t" role="3clF46">
                                    <property role="TrG5h" value="_context" />
                                    <property role="3TUv4t" value="true" />
                                    <node concept="3uibUv" id="3C" role="1tU5fm">
                                      <ref role="3uigEE" to="ze1i:~ReferenceConstraintsContext" resolve="ReferenceConstraintsContext" />
                                      <node concept="cd27G" id="3E" role="lGtFl">
                                        <node concept="3u3nmq" id="3F" role="cd27D">
                                          <property role="3u3nmv" value="5601053190833464083" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="cd27G" id="3D" role="lGtFl">
                                      <node concept="3u3nmq" id="3G" role="cd27D">
                                        <property role="3u3nmv" value="5601053190833464083" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="3Tm1VV" id="3u" role="1B3o_S">
                                    <node concept="cd27G" id="3H" role="lGtFl">
                                      <node concept="3u3nmq" id="3I" role="cd27D">
                                        <property role="3u3nmv" value="5601053190833464083" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="3uibUv" id="3v" role="3clF45">
                                    <ref role="3uigEE" to="35tq:~Scope" resolve="Scope" />
                                    <node concept="cd27G" id="3J" role="lGtFl">
                                      <node concept="3u3nmq" id="3K" role="cd27D">
                                        <property role="3u3nmv" value="5601053190833464083" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="3clFbS" id="3w" role="3clF47">
                                    <node concept="9aQIb" id="3L" role="3cqZAp">
                                      <node concept="3clFbS" id="3N" role="9aQI4">
                                        <node concept="3cpWs8" id="3P" role="3cqZAp">
                                          <node concept="3cpWsn" id="3S" role="3cpWs9">
                                            <property role="TrG5h" value="scope" />
                                            <node concept="3uibUv" id="3U" role="1tU5fm">
                                              <ref role="3uigEE" to="35tq:~Scope" resolve="Scope" />
                                              <node concept="cd27G" id="3X" role="lGtFl">
                                                <node concept="3u3nmq" id="3Y" role="cd27D">
                                                  <property role="3u3nmv" value="5601053190833464083" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="2YIFZM" id="3V" role="33vP2m">
                                              <ref role="1Pybhc" to="35tq:~Scope" resolve="Scope" />
                                              <ref role="37wK5l" to="35tq:~Scope.getScope(org.jetbrains.mps.openapi.model.SNode,org.jetbrains.mps.openapi.language.SContainmentLink,int,org.jetbrains.mps.openapi.language.SAbstractConcept):jetbrains.mps.scope.Scope" resolve="getScope" />
                                              <node concept="2OqwBi" id="3Z" role="37wK5m">
                                                <node concept="37vLTw" id="44" role="2Oq$k0">
                                                  <ref role="3cqZAo" node="3t" resolve="_context" />
                                                  <node concept="cd27G" id="47" role="lGtFl">
                                                    <node concept="3u3nmq" id="48" role="cd27D">
                                                      <property role="3u3nmv" value="5601053190833464083" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="liA8E" id="45" role="2OqNvi">
                                                  <ref role="37wK5l" to="ze1i:~ReferenceConstraintsContext.getContextNode():org.jetbrains.mps.openapi.model.SNode" resolve="getContextNode" />
                                                  <node concept="cd27G" id="49" role="lGtFl">
                                                    <node concept="3u3nmq" id="4a" role="cd27D">
                                                      <property role="3u3nmv" value="5601053190833464083" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="cd27G" id="46" role="lGtFl">
                                                  <node concept="3u3nmq" id="4b" role="cd27D">
                                                    <property role="3u3nmv" value="5601053190833464083" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="2OqwBi" id="40" role="37wK5m">
                                                <node concept="liA8E" id="4c" role="2OqNvi">
                                                  <ref role="37wK5l" to="ze1i:~ReferenceConstraintsContext.getContainmentLink():org.jetbrains.mps.openapi.language.SContainmentLink" resolve="getContainmentLink" />
                                                  <node concept="cd27G" id="4f" role="lGtFl">
                                                    <node concept="3u3nmq" id="4g" role="cd27D">
                                                      <property role="3u3nmv" value="5601053190833464083" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="37vLTw" id="4d" role="2Oq$k0">
                                                  <ref role="3cqZAo" node="3t" resolve="_context" />
                                                  <node concept="cd27G" id="4h" role="lGtFl">
                                                    <node concept="3u3nmq" id="4i" role="cd27D">
                                                      <property role="3u3nmv" value="5601053190833464083" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="cd27G" id="4e" role="lGtFl">
                                                  <node concept="3u3nmq" id="4j" role="cd27D">
                                                    <property role="3u3nmv" value="5601053190833464083" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="2OqwBi" id="41" role="37wK5m">
                                                <node concept="37vLTw" id="4k" role="2Oq$k0">
                                                  <ref role="3cqZAo" node="3t" resolve="_context" />
                                                  <node concept="cd27G" id="4n" role="lGtFl">
                                                    <node concept="3u3nmq" id="4o" role="cd27D">
                                                      <property role="3u3nmv" value="5601053190833464083" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="liA8E" id="4l" role="2OqNvi">
                                                  <ref role="37wK5l" to="ze1i:~ReferenceConstraintsContext.getPosition():int" resolve="getPosition" />
                                                  <node concept="cd27G" id="4p" role="lGtFl">
                                                    <node concept="3u3nmq" id="4q" role="cd27D">
                                                      <property role="3u3nmv" value="5601053190833464083" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="cd27G" id="4m" role="lGtFl">
                                                  <node concept="3u3nmq" id="4r" role="cd27D">
                                                    <property role="3u3nmv" value="5601053190833464083" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="35c_gC" id="42" role="37wK5m">
                                                <ref role="35c_gD" to="flit:4QUW3edEuTG" resolve="BiochemistryTestVariable" />
                                                <node concept="cd27G" id="4s" role="lGtFl">
                                                  <node concept="3u3nmq" id="4t" role="cd27D">
                                                    <property role="3u3nmv" value="5601053190833464083" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="cd27G" id="43" role="lGtFl">
                                                <node concept="3u3nmq" id="4u" role="cd27D">
                                                  <property role="3u3nmv" value="5601053190833464083" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="cd27G" id="3W" role="lGtFl">
                                              <node concept="3u3nmq" id="4v" role="cd27D">
                                                <property role="3u3nmv" value="5601053190833464083" />
                                              </node>
                                            </node>
                                          </node>
                                          <node concept="cd27G" id="3T" role="lGtFl">
                                            <node concept="3u3nmq" id="4w" role="cd27D">
                                              <property role="3u3nmv" value="5601053190833464083" />
                                            </node>
                                          </node>
                                        </node>
                                        <node concept="3cpWs6" id="3Q" role="3cqZAp">
                                          <node concept="3K4zz7" id="4x" role="3cqZAk">
                                            <node concept="2ShNRf" id="4z" role="3K4E3e">
                                              <node concept="1pGfFk" id="4B" role="2ShVmc">
                                                <ref role="37wK5l" to="35tq:~EmptyScope.&lt;init&gt;()" resolve="EmptyScope" />
                                                <node concept="cd27G" id="4D" role="lGtFl">
                                                  <node concept="3u3nmq" id="4E" role="cd27D">
                                                    <property role="3u3nmv" value="5601053190833464083" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="cd27G" id="4C" role="lGtFl">
                                                <node concept="3u3nmq" id="4F" role="cd27D">
                                                  <property role="3u3nmv" value="5601053190833464083" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="37vLTw" id="4$" role="3K4GZi">
                                              <ref role="3cqZAo" node="3S" resolve="scope" />
                                              <node concept="cd27G" id="4G" role="lGtFl">
                                                <node concept="3u3nmq" id="4H" role="cd27D">
                                                  <property role="3u3nmv" value="5601053190833464083" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="3clFbC" id="4_" role="3K4Cdx">
                                              <node concept="10Nm6u" id="4I" role="3uHU7w">
                                                <node concept="cd27G" id="4L" role="lGtFl">
                                                  <node concept="3u3nmq" id="4M" role="cd27D">
                                                    <property role="3u3nmv" value="5601053190833464083" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="37vLTw" id="4J" role="3uHU7B">
                                                <ref role="3cqZAo" node="3S" resolve="scope" />
                                                <node concept="cd27G" id="4N" role="lGtFl">
                                                  <node concept="3u3nmq" id="4O" role="cd27D">
                                                    <property role="3u3nmv" value="5601053190833464083" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="cd27G" id="4K" role="lGtFl">
                                                <node concept="3u3nmq" id="4P" role="cd27D">
                                                  <property role="3u3nmv" value="5601053190833464083" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="cd27G" id="4A" role="lGtFl">
                                              <node concept="3u3nmq" id="4Q" role="cd27D">
                                                <property role="3u3nmv" value="5601053190833464083" />
                                              </node>
                                            </node>
                                          </node>
                                          <node concept="cd27G" id="4y" role="lGtFl">
                                            <node concept="3u3nmq" id="4R" role="cd27D">
                                              <property role="3u3nmv" value="5601053190833464083" />
                                            </node>
                                          </node>
                                        </node>
                                        <node concept="cd27G" id="3R" role="lGtFl">
                                          <node concept="3u3nmq" id="4S" role="cd27D">
                                            <property role="3u3nmv" value="5601053190833464083" />
                                          </node>
                                        </node>
                                      </node>
                                      <node concept="cd27G" id="3O" role="lGtFl">
                                        <node concept="3u3nmq" id="4T" role="cd27D">
                                          <property role="3u3nmv" value="5601053190833464083" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="cd27G" id="3M" role="lGtFl">
                                      <node concept="3u3nmq" id="4U" role="cd27D">
                                        <property role="3u3nmv" value="5601053190833464083" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="2AHcQZ" id="3x" role="2AJF6D">
                                    <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
                                    <node concept="cd27G" id="4V" role="lGtFl">
                                      <node concept="3u3nmq" id="4W" role="cd27D">
                                        <property role="3u3nmv" value="5601053190833464083" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="cd27G" id="3y" role="lGtFl">
                                    <node concept="3u3nmq" id="4X" role="cd27D">
                                      <property role="3u3nmv" value="5601053190833464083" />
                                    </node>
                                  </node>
                                </node>
                                <node concept="cd27G" id="2T" role="lGtFl">
                                  <node concept="3u3nmq" id="4Y" role="cd27D">
                                    <property role="3u3nmv" value="5601053190833464083" />
                                  </node>
                                </node>
                              </node>
                              <node concept="cd27G" id="2P" role="lGtFl">
                                <node concept="3u3nmq" id="4Z" role="cd27D">
                                  <property role="3u3nmv" value="5601053190833464083" />
                                </node>
                              </node>
                            </node>
                            <node concept="cd27G" id="2N" role="lGtFl">
                              <node concept="3u3nmq" id="50" role="cd27D">
                                <property role="3u3nmv" value="5601053190833464083" />
                              </node>
                            </node>
                          </node>
                          <node concept="cd27G" id="2L" role="lGtFl">
                            <node concept="3u3nmq" id="51" role="cd27D">
                              <property role="3u3nmv" value="5601053190833464083" />
                            </node>
                          </node>
                        </node>
                        <node concept="cd27G" id="2J" role="lGtFl">
                          <node concept="3u3nmq" id="52" role="cd27D">
                            <property role="3u3nmv" value="5601053190833464083" />
                          </node>
                        </node>
                      </node>
                      <node concept="2AHcQZ" id="2A" role="2AJF6D">
                        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
                        <node concept="cd27G" id="53" role="lGtFl">
                          <node concept="3u3nmq" id="54" role="cd27D">
                            <property role="3u3nmv" value="5601053190833464083" />
                          </node>
                        </node>
                      </node>
                      <node concept="cd27G" id="2B" role="lGtFl">
                        <node concept="3u3nmq" id="55" role="cd27D">
                          <property role="3u3nmv" value="5601053190833464083" />
                        </node>
                      </node>
                    </node>
                    <node concept="cd27G" id="1V" role="lGtFl">
                      <node concept="3u3nmq" id="56" role="cd27D">
                        <property role="3u3nmv" value="5601053190833464083" />
                      </node>
                    </node>
                  </node>
                  <node concept="cd27G" id="1P" role="lGtFl">
                    <node concept="3u3nmq" id="57" role="cd27D">
                      <property role="3u3nmv" value="5601053190833464083" />
                    </node>
                  </node>
                </node>
                <node concept="cd27G" id="1N" role="lGtFl">
                  <node concept="3u3nmq" id="58" role="cd27D">
                    <property role="3u3nmv" value="5601053190833464083" />
                  </node>
                </node>
              </node>
              <node concept="cd27G" id="1w" role="lGtFl">
                <node concept="3u3nmq" id="59" role="cd27D">
                  <property role="3u3nmv" value="5601053190833464083" />
                </node>
              </node>
            </node>
            <node concept="cd27G" id="1r" role="lGtFl">
              <node concept="3u3nmq" id="5a" role="cd27D">
                <property role="3u3nmv" value="5601053190833464083" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="1o" role="lGtFl">
            <node concept="3u3nmq" id="5b" role="cd27D">
              <property role="3u3nmv" value="5601053190833464083" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="V" role="3cqZAp">
          <node concept="37vLTw" id="5c" role="3clFbG">
            <ref role="3cqZAo" node="X" resolve="references" />
            <node concept="cd27G" id="5e" role="lGtFl">
              <node concept="3u3nmq" id="5f" role="cd27D">
                <property role="3u3nmv" value="5601053190833464083" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="5d" role="lGtFl">
            <node concept="3u3nmq" id="5g" role="cd27D">
              <property role="3u3nmv" value="5601053190833464083" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="W" role="lGtFl">
          <node concept="3u3nmq" id="5h" role="cd27D">
            <property role="3u3nmv" value="5601053190833464083" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="H" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <node concept="cd27G" id="5i" role="lGtFl">
          <node concept="3u3nmq" id="5j" role="cd27D">
            <property role="3u3nmv" value="5601053190833464083" />
          </node>
        </node>
      </node>
      <node concept="cd27G" id="I" role="lGtFl">
        <node concept="3u3nmq" id="5k" role="cd27D">
          <property role="3u3nmv" value="5601053190833464083" />
        </node>
      </node>
    </node>
    <node concept="cd27G" id="6" role="lGtFl">
      <node concept="3u3nmq" id="5l" role="cd27D">
        <property role="3u3nmv" value="5601053190833464083" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="5m">
    <property role="3GE5qa" value="aspects.references.biochemistryTest" />
    <property role="TrG5h" value="AspectBiochemistryTestWithNumberResultReference_Constraints" />
    <node concept="3Tm1VV" id="5n" role="1B3o_S">
      <node concept="cd27G" id="5t" role="lGtFl">
        <node concept="3u3nmq" id="5u" role="cd27D">
          <property role="3u3nmv" value="5675576922574815233" />
        </node>
      </node>
    </node>
    <node concept="3uibUv" id="5o" role="1zkMxy">
      <ref role="3uigEE" to="79pl:~BaseConstraintsDescriptor" resolve="BaseConstraintsDescriptor" />
      <node concept="cd27G" id="5v" role="lGtFl">
        <node concept="3u3nmq" id="5w" role="cd27D">
          <property role="3u3nmv" value="5675576922574815233" />
        </node>
      </node>
    </node>
    <node concept="3clFbW" id="5p" role="jymVt">
      <node concept="3cqZAl" id="5x" role="3clF45">
        <node concept="cd27G" id="5_" role="lGtFl">
          <node concept="3u3nmq" id="5A" role="cd27D">
            <property role="3u3nmv" value="5675576922574815233" />
          </node>
        </node>
      </node>
      <node concept="3clFbS" id="5y" role="3clF47">
        <node concept="XkiVB" id="5B" role="3cqZAp">
          <ref role="37wK5l" to="79pl:~BaseConstraintsDescriptor.&lt;init&gt;(org.jetbrains.mps.openapi.language.SAbstractConcept)" resolve="BaseConstraintsDescriptor" />
          <node concept="2YIFZM" id="5D" role="37wK5m">
            <ref role="1Pybhc" to="2k9e:~MetaAdapterFactory" resolve="MetaAdapterFactory" />
            <ref role="37wK5l" to="2k9e:~MetaAdapterFactory.getConcept(long,long,long,java.lang.String):org.jetbrains.mps.openapi.language.SConcept" resolve="getConcept" />
            <node concept="1adDum" id="5F" role="37wK5m">
              <property role="1adDun" value="0x76146113297e4cbeL" />
              <node concept="cd27G" id="5K" role="lGtFl">
                <node concept="3u3nmq" id="5L" role="cd27D">
                  <property role="3u3nmv" value="5675576922574815233" />
                </node>
              </node>
            </node>
            <node concept="1adDum" id="5G" role="37wK5m">
              <property role="1adDun" value="0x99cf714ae175bb83L" />
              <node concept="cd27G" id="5M" role="lGtFl">
                <node concept="3u3nmq" id="5N" role="cd27D">
                  <property role="3u3nmv" value="5675576922574815233" />
                </node>
              </node>
            </node>
            <node concept="1adDum" id="5H" role="37wK5m">
              <property role="1adDun" value="0x4ec3b323fd71d0b1L" />
              <node concept="cd27G" id="5O" role="lGtFl">
                <node concept="3u3nmq" id="5P" role="cd27D">
                  <property role="3u3nmv" value="5675576922574815233" />
                </node>
              </node>
            </node>
            <node concept="Xl_RD" id="5I" role="37wK5m">
              <property role="Xl_RC" value="no.uio.mLab.decisions.references.biochemistry.structure.AspectBiochemistryTestWithNumberResultReference" />
              <node concept="cd27G" id="5Q" role="lGtFl">
                <node concept="3u3nmq" id="5R" role="cd27D">
                  <property role="3u3nmv" value="5675576922574815233" />
                </node>
              </node>
            </node>
            <node concept="cd27G" id="5J" role="lGtFl">
              <node concept="3u3nmq" id="5S" role="cd27D">
                <property role="3u3nmv" value="5675576922574815233" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="5E" role="lGtFl">
            <node concept="3u3nmq" id="5T" role="cd27D">
              <property role="3u3nmv" value="5675576922574815233" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="5C" role="lGtFl">
          <node concept="3u3nmq" id="5U" role="cd27D">
            <property role="3u3nmv" value="5675576922574815233" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="5z" role="1B3o_S">
        <node concept="cd27G" id="5V" role="lGtFl">
          <node concept="3u3nmq" id="5W" role="cd27D">
            <property role="3u3nmv" value="5675576922574815233" />
          </node>
        </node>
      </node>
      <node concept="cd27G" id="5$" role="lGtFl">
        <node concept="3u3nmq" id="5X" role="cd27D">
          <property role="3u3nmv" value="5675576922574815233" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="5q" role="jymVt">
      <node concept="cd27G" id="5Y" role="lGtFl">
        <node concept="3u3nmq" id="5Z" role="cd27D">
          <property role="3u3nmv" value="5675576922574815233" />
        </node>
      </node>
    </node>
    <node concept="3clFb_" id="5r" role="jymVt">
      <property role="IEkAT" value="false" />
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="getSpecifiedReferences" />
      <property role="DiZV1" value="false" />
      <node concept="3Tmbuc" id="60" role="1B3o_S">
        <node concept="cd27G" id="65" role="lGtFl">
          <node concept="3u3nmq" id="66" role="cd27D">
            <property role="3u3nmv" value="5675576922574815233" />
          </node>
        </node>
      </node>
      <node concept="3uibUv" id="61" role="3clF45">
        <ref role="3uigEE" to="33ny:~Map" resolve="Map" />
        <node concept="3uibUv" id="67" role="11_B2D">
          <ref role="3uigEE" to="c17a:~SReferenceLink" resolve="SReferenceLink" />
          <node concept="cd27G" id="6a" role="lGtFl">
            <node concept="3u3nmq" id="6b" role="cd27D">
              <property role="3u3nmv" value="5675576922574815233" />
            </node>
          </node>
        </node>
        <node concept="3uibUv" id="68" role="11_B2D">
          <ref role="3uigEE" to="ze1i:~ReferenceConstraintsDescriptor" resolve="ReferenceConstraintsDescriptor" />
          <node concept="cd27G" id="6c" role="lGtFl">
            <node concept="3u3nmq" id="6d" role="cd27D">
              <property role="3u3nmv" value="5675576922574815233" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="69" role="lGtFl">
          <node concept="3u3nmq" id="6e" role="cd27D">
            <property role="3u3nmv" value="5675576922574815233" />
          </node>
        </node>
      </node>
      <node concept="3clFbS" id="62" role="3clF47">
        <node concept="3cpWs8" id="6f" role="3cqZAp">
          <node concept="3cpWsn" id="6j" role="3cpWs9">
            <property role="TrG5h" value="references" />
            <node concept="3uibUv" id="6l" role="1tU5fm">
              <ref role="3uigEE" to="33ny:~Map" resolve="Map" />
              <node concept="3uibUv" id="6o" role="11_B2D">
                <ref role="3uigEE" to="c17a:~SReferenceLink" resolve="SReferenceLink" />
                <node concept="cd27G" id="6r" role="lGtFl">
                  <node concept="3u3nmq" id="6s" role="cd27D">
                    <property role="3u3nmv" value="5675576922574815233" />
                  </node>
                </node>
              </node>
              <node concept="3uibUv" id="6p" role="11_B2D">
                <ref role="3uigEE" to="ze1i:~ReferenceConstraintsDescriptor" resolve="ReferenceConstraintsDescriptor" />
                <node concept="cd27G" id="6t" role="lGtFl">
                  <node concept="3u3nmq" id="6u" role="cd27D">
                    <property role="3u3nmv" value="5675576922574815233" />
                  </node>
                </node>
              </node>
              <node concept="cd27G" id="6q" role="lGtFl">
                <node concept="3u3nmq" id="6v" role="cd27D">
                  <property role="3u3nmv" value="5675576922574815233" />
                </node>
              </node>
            </node>
            <node concept="2ShNRf" id="6m" role="33vP2m">
              <node concept="1pGfFk" id="6w" role="2ShVmc">
                <ref role="37wK5l" to="33ny:~HashMap.&lt;init&gt;()" resolve="HashMap" />
                <node concept="3uibUv" id="6y" role="1pMfVU">
                  <ref role="3uigEE" to="c17a:~SReferenceLink" resolve="SReferenceLink" />
                  <node concept="cd27G" id="6_" role="lGtFl">
                    <node concept="3u3nmq" id="6A" role="cd27D">
                      <property role="3u3nmv" value="5675576922574815233" />
                    </node>
                  </node>
                </node>
                <node concept="3uibUv" id="6z" role="1pMfVU">
                  <ref role="3uigEE" to="ze1i:~ReferenceConstraintsDescriptor" resolve="ReferenceConstraintsDescriptor" />
                  <node concept="cd27G" id="6B" role="lGtFl">
                    <node concept="3u3nmq" id="6C" role="cd27D">
                      <property role="3u3nmv" value="5675576922574815233" />
                    </node>
                  </node>
                </node>
                <node concept="cd27G" id="6$" role="lGtFl">
                  <node concept="3u3nmq" id="6D" role="cd27D">
                    <property role="3u3nmv" value="5675576922574815233" />
                  </node>
                </node>
              </node>
              <node concept="cd27G" id="6x" role="lGtFl">
                <node concept="3u3nmq" id="6E" role="cd27D">
                  <property role="3u3nmv" value="5675576922574815233" />
                </node>
              </node>
            </node>
            <node concept="cd27G" id="6n" role="lGtFl">
              <node concept="3u3nmq" id="6F" role="cd27D">
                <property role="3u3nmv" value="5675576922574815233" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="6k" role="lGtFl">
            <node concept="3u3nmq" id="6G" role="cd27D">
              <property role="3u3nmv" value="5675576922574815233" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="6g" role="3cqZAp">
          <node concept="2OqwBi" id="6H" role="3clFbG">
            <node concept="37vLTw" id="6J" role="2Oq$k0">
              <ref role="3cqZAo" node="6j" resolve="references" />
              <node concept="cd27G" id="6M" role="lGtFl">
                <node concept="3u3nmq" id="6N" role="cd27D">
                  <property role="3u3nmv" value="5675576922574815233" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="6K" role="2OqNvi">
              <ref role="37wK5l" to="33ny:~Map.put(java.lang.Object,java.lang.Object):java.lang.Object" resolve="put" />
              <node concept="2YIFZM" id="6O" role="37wK5m">
                <ref role="1Pybhc" to="2k9e:~MetaAdapterFactory" resolve="MetaAdapterFactory" />
                <ref role="37wK5l" to="2k9e:~MetaAdapterFactory.getReferenceLink(long,long,long,long,java.lang.String):org.jetbrains.mps.openapi.language.SReferenceLink" resolve="getReferenceLink" />
                <node concept="1adDum" id="6R" role="37wK5m">
                  <property role="1adDun" value="0xc610a4ebc47c4f95L" />
                  <node concept="cd27G" id="6X" role="lGtFl">
                    <node concept="3u3nmq" id="6Y" role="cd27D">
                      <property role="3u3nmv" value="5675576922574815233" />
                    </node>
                  </node>
                </node>
                <node concept="1adDum" id="6S" role="37wK5m">
                  <property role="1adDun" value="0xb56811236971769cL" />
                  <node concept="cd27G" id="6Z" role="lGtFl">
                    <node concept="3u3nmq" id="70" role="cd27D">
                      <property role="3u3nmv" value="5675576922574815233" />
                    </node>
                  </node>
                </node>
                <node concept="1adDum" id="6T" role="37wK5m">
                  <property role="1adDun" value="0x6c7943d5b299791dL" />
                  <node concept="cd27G" id="71" role="lGtFl">
                    <node concept="3u3nmq" id="72" role="cd27D">
                      <property role="3u3nmv" value="5675576922574815233" />
                    </node>
                  </node>
                </node>
                <node concept="1adDum" id="6U" role="37wK5m">
                  <property role="1adDun" value="0x6c7943d5b29b8063L" />
                  <node concept="cd27G" id="73" role="lGtFl">
                    <node concept="3u3nmq" id="74" role="cd27D">
                      <property role="3u3nmv" value="5675576922574815233" />
                    </node>
                  </node>
                </node>
                <node concept="Xl_RD" id="6V" role="37wK5m">
                  <property role="Xl_RC" value="target" />
                  <node concept="cd27G" id="75" role="lGtFl">
                    <node concept="3u3nmq" id="76" role="cd27D">
                      <property role="3u3nmv" value="5675576922574815233" />
                    </node>
                  </node>
                </node>
                <node concept="cd27G" id="6W" role="lGtFl">
                  <node concept="3u3nmq" id="77" role="cd27D">
                    <property role="3u3nmv" value="5675576922574815233" />
                  </node>
                </node>
              </node>
              <node concept="2ShNRf" id="6P" role="37wK5m">
                <node concept="YeOm9" id="78" role="2ShVmc">
                  <node concept="1Y3b0j" id="7a" role="YeSDq">
                    <property role="2bfB8j" value="true" />
                    <ref role="1Y3XeK" to="79pl:~BaseReferenceConstraintsDescriptor" resolve="BaseReferenceConstraintsDescriptor" />
                    <ref role="37wK5l" to="79pl:~BaseReferenceConstraintsDescriptor.&lt;init&gt;(jetbrains.mps.smodel.adapter.ids.SReferenceLinkId,jetbrains.mps.smodel.runtime.ConstraintsDescriptor)" resolve="BaseReferenceConstraintsDescriptor" />
                    <node concept="2YIFZM" id="7c" role="37wK5m">
                      <ref role="1Pybhc" to="e8bb:~MetaIdFactory" resolve="MetaIdFactory" />
                      <ref role="37wK5l" to="e8bb:~MetaIdFactory.refId(long,long,long,long):jetbrains.mps.smodel.adapter.ids.SReferenceLinkId" resolve="refId" />
                      <node concept="1adDum" id="7i" role="37wK5m">
                        <property role="1adDun" value="0xc610a4ebc47c4f95L" />
                        <node concept="cd27G" id="7n" role="lGtFl">
                          <node concept="3u3nmq" id="7o" role="cd27D">
                            <property role="3u3nmv" value="5675576922574815233" />
                          </node>
                        </node>
                      </node>
                      <node concept="1adDum" id="7j" role="37wK5m">
                        <property role="1adDun" value="0xb56811236971769cL" />
                        <node concept="cd27G" id="7p" role="lGtFl">
                          <node concept="3u3nmq" id="7q" role="cd27D">
                            <property role="3u3nmv" value="5675576922574815233" />
                          </node>
                        </node>
                      </node>
                      <node concept="1adDum" id="7k" role="37wK5m">
                        <property role="1adDun" value="0x6c7943d5b299791dL" />
                        <node concept="cd27G" id="7r" role="lGtFl">
                          <node concept="3u3nmq" id="7s" role="cd27D">
                            <property role="3u3nmv" value="5675576922574815233" />
                          </node>
                        </node>
                      </node>
                      <node concept="1adDum" id="7l" role="37wK5m">
                        <property role="1adDun" value="0x6c7943d5b29b8063L" />
                        <node concept="cd27G" id="7t" role="lGtFl">
                          <node concept="3u3nmq" id="7u" role="cd27D">
                            <property role="3u3nmv" value="5675576922574815233" />
                          </node>
                        </node>
                      </node>
                      <node concept="cd27G" id="7m" role="lGtFl">
                        <node concept="3u3nmq" id="7v" role="cd27D">
                          <property role="3u3nmv" value="5675576922574815233" />
                        </node>
                      </node>
                    </node>
                    <node concept="3Tm1VV" id="7d" role="1B3o_S">
                      <node concept="cd27G" id="7w" role="lGtFl">
                        <node concept="3u3nmq" id="7x" role="cd27D">
                          <property role="3u3nmv" value="5675576922574815233" />
                        </node>
                      </node>
                    </node>
                    <node concept="Xjq3P" id="7e" role="37wK5m">
                      <node concept="cd27G" id="7y" role="lGtFl">
                        <node concept="3u3nmq" id="7z" role="cd27D">
                          <property role="3u3nmv" value="5675576922574815233" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFb_" id="7f" role="jymVt">
                      <property role="IEkAT" value="false" />
                      <property role="1EzhhJ" value="false" />
                      <property role="TrG5h" value="hasOwnScopeProvider" />
                      <property role="DiZV1" value="false" />
                      <node concept="3Tm1VV" id="7$" role="1B3o_S">
                        <node concept="cd27G" id="7D" role="lGtFl">
                          <node concept="3u3nmq" id="7E" role="cd27D">
                            <property role="3u3nmv" value="5675576922574815233" />
                          </node>
                        </node>
                      </node>
                      <node concept="10P_77" id="7_" role="3clF45">
                        <node concept="cd27G" id="7F" role="lGtFl">
                          <node concept="3u3nmq" id="7G" role="cd27D">
                            <property role="3u3nmv" value="5675576922574815233" />
                          </node>
                        </node>
                      </node>
                      <node concept="3clFbS" id="7A" role="3clF47">
                        <node concept="3clFbF" id="7H" role="3cqZAp">
                          <node concept="3clFbT" id="7J" role="3clFbG">
                            <property role="3clFbU" value="true" />
                            <node concept="cd27G" id="7L" role="lGtFl">
                              <node concept="3u3nmq" id="7M" role="cd27D">
                                <property role="3u3nmv" value="5675576922574815233" />
                              </node>
                            </node>
                          </node>
                          <node concept="cd27G" id="7K" role="lGtFl">
                            <node concept="3u3nmq" id="7N" role="cd27D">
                              <property role="3u3nmv" value="5675576922574815233" />
                            </node>
                          </node>
                        </node>
                        <node concept="cd27G" id="7I" role="lGtFl">
                          <node concept="3u3nmq" id="7O" role="cd27D">
                            <property role="3u3nmv" value="5675576922574815233" />
                          </node>
                        </node>
                      </node>
                      <node concept="2AHcQZ" id="7B" role="2AJF6D">
                        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
                        <node concept="cd27G" id="7P" role="lGtFl">
                          <node concept="3u3nmq" id="7Q" role="cd27D">
                            <property role="3u3nmv" value="5675576922574815233" />
                          </node>
                        </node>
                      </node>
                      <node concept="cd27G" id="7C" role="lGtFl">
                        <node concept="3u3nmq" id="7R" role="cd27D">
                          <property role="3u3nmv" value="5675576922574815233" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFb_" id="7g" role="jymVt">
                      <property role="IEkAT" value="false" />
                      <property role="1EzhhJ" value="false" />
                      <property role="TrG5h" value="getScopeProvider" />
                      <property role="DiZV1" value="false" />
                      <node concept="3Tm1VV" id="7S" role="1B3o_S">
                        <node concept="cd27G" id="7Y" role="lGtFl">
                          <node concept="3u3nmq" id="7Z" role="cd27D">
                            <property role="3u3nmv" value="5675576922574815233" />
                          </node>
                        </node>
                      </node>
                      <node concept="3uibUv" id="7T" role="3clF45">
                        <ref role="3uigEE" to="ze1i:~ReferenceScopeProvider" resolve="ReferenceScopeProvider" />
                        <node concept="cd27G" id="80" role="lGtFl">
                          <node concept="3u3nmq" id="81" role="cd27D">
                            <property role="3u3nmv" value="5675576922574815233" />
                          </node>
                        </node>
                      </node>
                      <node concept="2AHcQZ" id="7U" role="2AJF6D">
                        <ref role="2AI5Lk" to="mhfm:~Nullable" resolve="Nullable" />
                        <node concept="cd27G" id="82" role="lGtFl">
                          <node concept="3u3nmq" id="83" role="cd27D">
                            <property role="3u3nmv" value="5675576922574815233" />
                          </node>
                        </node>
                      </node>
                      <node concept="3clFbS" id="7V" role="3clF47">
                        <node concept="3cpWs6" id="84" role="3cqZAp">
                          <node concept="2ShNRf" id="86" role="3cqZAk">
                            <node concept="YeOm9" id="88" role="2ShVmc">
                              <node concept="1Y3b0j" id="8a" role="YeSDq">
                                <property role="2bfB8j" value="true" />
                                <ref role="1Y3XeK" to="79pl:~BaseScopeProvider" resolve="BaseScopeProvider" />
                                <ref role="37wK5l" to="79pl:~BaseScopeProvider.&lt;init&gt;()" resolve="BaseScopeProvider" />
                                <node concept="3Tm1VV" id="8c" role="1B3o_S">
                                  <node concept="cd27G" id="8g" role="lGtFl">
                                    <node concept="3u3nmq" id="8h" role="cd27D">
                                      <property role="3u3nmv" value="5675576922574815233" />
                                    </node>
                                  </node>
                                </node>
                                <node concept="3clFb_" id="8d" role="jymVt">
                                  <property role="TrG5h" value="getSearchScopeValidatorNode" />
                                  <node concept="3Tm1VV" id="8i" role="1B3o_S">
                                    <node concept="cd27G" id="8n" role="lGtFl">
                                      <node concept="3u3nmq" id="8o" role="cd27D">
                                        <property role="3u3nmv" value="5675576922574815233" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="3clFbS" id="8j" role="3clF47">
                                    <node concept="3cpWs6" id="8p" role="3cqZAp">
                                      <node concept="1dyn4i" id="8r" role="3cqZAk">
                                        <property role="1zomUR" value="true" />
                                        <property role="1dyqJU" value="breakingNode" />
                                        <node concept="2ShNRf" id="8t" role="1dyrYi">
                                          <node concept="1pGfFk" id="8v" role="2ShVmc">
                                            <ref role="37wK5l" to="w1kc:~SNodePointer.&lt;init&gt;(java.lang.String,java.lang.String)" resolve="SNodePointer" />
                                            <node concept="Xl_RD" id="8x" role="37wK5m">
                                              <property role="Xl_RC" value="r:62707f82-6ccf-4b1b-b39a-a18182073111(no.uio.mLab.decisions.references.biochemistry.constraints)" />
                                              <node concept="cd27G" id="8$" role="lGtFl">
                                                <node concept="3u3nmq" id="8_" role="cd27D">
                                                  <property role="3u3nmv" value="5675576922574815233" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="Xl_RD" id="8y" role="37wK5m">
                                              <property role="Xl_RC" value="5675576922574815235" />
                                              <node concept="cd27G" id="8A" role="lGtFl">
                                                <node concept="3u3nmq" id="8B" role="cd27D">
                                                  <property role="3u3nmv" value="5675576922574815233" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="cd27G" id="8z" role="lGtFl">
                                              <node concept="3u3nmq" id="8C" role="cd27D">
                                                <property role="3u3nmv" value="5675576922574815233" />
                                              </node>
                                            </node>
                                          </node>
                                          <node concept="cd27G" id="8w" role="lGtFl">
                                            <node concept="3u3nmq" id="8D" role="cd27D">
                                              <property role="3u3nmv" value="5675576922574815233" />
                                            </node>
                                          </node>
                                        </node>
                                        <node concept="cd27G" id="8u" role="lGtFl">
                                          <node concept="3u3nmq" id="8E" role="cd27D">
                                            <property role="3u3nmv" value="5675576922574815233" />
                                          </node>
                                        </node>
                                      </node>
                                      <node concept="cd27G" id="8s" role="lGtFl">
                                        <node concept="3u3nmq" id="8F" role="cd27D">
                                          <property role="3u3nmv" value="5675576922574815233" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="cd27G" id="8q" role="lGtFl">
                                      <node concept="3u3nmq" id="8G" role="cd27D">
                                        <property role="3u3nmv" value="5675576922574815233" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="3uibUv" id="8k" role="3clF45">
                                    <ref role="3uigEE" to="mhbf:~SNodeReference" resolve="SNodeReference" />
                                    <node concept="cd27G" id="8H" role="lGtFl">
                                      <node concept="3u3nmq" id="8I" role="cd27D">
                                        <property role="3u3nmv" value="5675576922574815233" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="2AHcQZ" id="8l" role="2AJF6D">
                                    <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
                                    <node concept="cd27G" id="8J" role="lGtFl">
                                      <node concept="3u3nmq" id="8K" role="cd27D">
                                        <property role="3u3nmv" value="5675576922574815233" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="cd27G" id="8m" role="lGtFl">
                                    <node concept="3u3nmq" id="8L" role="cd27D">
                                      <property role="3u3nmv" value="5675576922574815233" />
                                    </node>
                                  </node>
                                </node>
                                <node concept="3clFb_" id="8e" role="jymVt">
                                  <property role="IEkAT" value="false" />
                                  <property role="1EzhhJ" value="false" />
                                  <property role="TrG5h" value="createScope" />
                                  <property role="DiZV1" value="false" />
                                  <node concept="37vLTG" id="8M" role="3clF46">
                                    <property role="TrG5h" value="operationContext" />
                                    <property role="3TUv4t" value="true" />
                                    <node concept="3uibUv" id="8T" role="1tU5fm">
                                      <ref role="3uigEE" to="w1kc:~IOperationContext" resolve="IOperationContext" />
                                      <node concept="cd27G" id="8V" role="lGtFl">
                                        <node concept="3u3nmq" id="8W" role="cd27D">
                                          <property role="3u3nmv" value="5675576922574815233" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="cd27G" id="8U" role="lGtFl">
                                      <node concept="3u3nmq" id="8X" role="cd27D">
                                        <property role="3u3nmv" value="5675576922574815233" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="37vLTG" id="8N" role="3clF46">
                                    <property role="TrG5h" value="_context" />
                                    <property role="3TUv4t" value="true" />
                                    <node concept="3uibUv" id="8Y" role="1tU5fm">
                                      <ref role="3uigEE" to="ze1i:~ReferenceConstraintsContext" resolve="ReferenceConstraintsContext" />
                                      <node concept="cd27G" id="90" role="lGtFl">
                                        <node concept="3u3nmq" id="91" role="cd27D">
                                          <property role="3u3nmv" value="5675576922574815233" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="cd27G" id="8Z" role="lGtFl">
                                      <node concept="3u3nmq" id="92" role="cd27D">
                                        <property role="3u3nmv" value="5675576922574815233" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="3Tm1VV" id="8O" role="1B3o_S">
                                    <node concept="cd27G" id="93" role="lGtFl">
                                      <node concept="3u3nmq" id="94" role="cd27D">
                                        <property role="3u3nmv" value="5675576922574815233" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="3uibUv" id="8P" role="3clF45">
                                    <ref role="3uigEE" to="35tq:~Scope" resolve="Scope" />
                                    <node concept="cd27G" id="95" role="lGtFl">
                                      <node concept="3u3nmq" id="96" role="cd27D">
                                        <property role="3u3nmv" value="5675576922574815233" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="3clFbS" id="8Q" role="3clF47">
                                    <node concept="9aQIb" id="97" role="3cqZAp">
                                      <node concept="3clFbS" id="99" role="9aQI4">
                                        <node concept="3cpWs8" id="9b" role="3cqZAp">
                                          <node concept="3cpWsn" id="9e" role="3cpWs9">
                                            <property role="TrG5h" value="scope" />
                                            <node concept="3uibUv" id="9g" role="1tU5fm">
                                              <ref role="3uigEE" to="35tq:~Scope" resolve="Scope" />
                                              <node concept="cd27G" id="9j" role="lGtFl">
                                                <node concept="3u3nmq" id="9k" role="cd27D">
                                                  <property role="3u3nmv" value="5675576922574815233" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="2YIFZM" id="9h" role="33vP2m">
                                              <ref role="1Pybhc" to="35tq:~Scope" resolve="Scope" />
                                              <ref role="37wK5l" to="35tq:~Scope.getScope(org.jetbrains.mps.openapi.model.SNode,org.jetbrains.mps.openapi.language.SContainmentLink,int,org.jetbrains.mps.openapi.language.SAbstractConcept):jetbrains.mps.scope.Scope" resolve="getScope" />
                                              <node concept="2OqwBi" id="9l" role="37wK5m">
                                                <node concept="37vLTw" id="9q" role="2Oq$k0">
                                                  <ref role="3cqZAo" node="8N" resolve="_context" />
                                                  <node concept="cd27G" id="9t" role="lGtFl">
                                                    <node concept="3u3nmq" id="9u" role="cd27D">
                                                      <property role="3u3nmv" value="5675576922574815233" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="liA8E" id="9r" role="2OqNvi">
                                                  <ref role="37wK5l" to="ze1i:~ReferenceConstraintsContext.getContextNode():org.jetbrains.mps.openapi.model.SNode" resolve="getContextNode" />
                                                  <node concept="cd27G" id="9v" role="lGtFl">
                                                    <node concept="3u3nmq" id="9w" role="cd27D">
                                                      <property role="3u3nmv" value="5675576922574815233" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="cd27G" id="9s" role="lGtFl">
                                                  <node concept="3u3nmq" id="9x" role="cd27D">
                                                    <property role="3u3nmv" value="5675576922574815233" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="2OqwBi" id="9m" role="37wK5m">
                                                <node concept="liA8E" id="9y" role="2OqNvi">
                                                  <ref role="37wK5l" to="ze1i:~ReferenceConstraintsContext.getContainmentLink():org.jetbrains.mps.openapi.language.SContainmentLink" resolve="getContainmentLink" />
                                                  <node concept="cd27G" id="9_" role="lGtFl">
                                                    <node concept="3u3nmq" id="9A" role="cd27D">
                                                      <property role="3u3nmv" value="5675576922574815233" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="37vLTw" id="9z" role="2Oq$k0">
                                                  <ref role="3cqZAo" node="8N" resolve="_context" />
                                                  <node concept="cd27G" id="9B" role="lGtFl">
                                                    <node concept="3u3nmq" id="9C" role="cd27D">
                                                      <property role="3u3nmv" value="5675576922574815233" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="cd27G" id="9$" role="lGtFl">
                                                  <node concept="3u3nmq" id="9D" role="cd27D">
                                                    <property role="3u3nmv" value="5675576922574815233" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="2OqwBi" id="9n" role="37wK5m">
                                                <node concept="37vLTw" id="9E" role="2Oq$k0">
                                                  <ref role="3cqZAo" node="8N" resolve="_context" />
                                                  <node concept="cd27G" id="9H" role="lGtFl">
                                                    <node concept="3u3nmq" id="9I" role="cd27D">
                                                      <property role="3u3nmv" value="5675576922574815233" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="liA8E" id="9F" role="2OqNvi">
                                                  <ref role="37wK5l" to="ze1i:~ReferenceConstraintsContext.getPosition():int" resolve="getPosition" />
                                                  <node concept="cd27G" id="9J" role="lGtFl">
                                                    <node concept="3u3nmq" id="9K" role="cd27D">
                                                      <property role="3u3nmv" value="5675576922574815233" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="cd27G" id="9G" role="lGtFl">
                                                  <node concept="3u3nmq" id="9L" role="cd27D">
                                                    <property role="3u3nmv" value="5675576922574815233" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="35c_gC" id="9o" role="37wK5m">
                                                <ref role="35c_gD" to="flit:4QUW3edEuTG" resolve="BiochemistryTestVariable" />
                                                <node concept="cd27G" id="9M" role="lGtFl">
                                                  <node concept="3u3nmq" id="9N" role="cd27D">
                                                    <property role="3u3nmv" value="5675576922574815233" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="cd27G" id="9p" role="lGtFl">
                                                <node concept="3u3nmq" id="9O" role="cd27D">
                                                  <property role="3u3nmv" value="5675576922574815233" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="cd27G" id="9i" role="lGtFl">
                                              <node concept="3u3nmq" id="9P" role="cd27D">
                                                <property role="3u3nmv" value="5675576922574815233" />
                                              </node>
                                            </node>
                                          </node>
                                          <node concept="cd27G" id="9f" role="lGtFl">
                                            <node concept="3u3nmq" id="9Q" role="cd27D">
                                              <property role="3u3nmv" value="5675576922574815233" />
                                            </node>
                                          </node>
                                        </node>
                                        <node concept="3cpWs6" id="9c" role="3cqZAp">
                                          <node concept="3K4zz7" id="9R" role="3cqZAk">
                                            <node concept="2ShNRf" id="9T" role="3K4E3e">
                                              <node concept="1pGfFk" id="9X" role="2ShVmc">
                                                <ref role="37wK5l" to="35tq:~EmptyScope.&lt;init&gt;()" resolve="EmptyScope" />
                                                <node concept="cd27G" id="9Z" role="lGtFl">
                                                  <node concept="3u3nmq" id="a0" role="cd27D">
                                                    <property role="3u3nmv" value="5675576922574815233" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="cd27G" id="9Y" role="lGtFl">
                                                <node concept="3u3nmq" id="a1" role="cd27D">
                                                  <property role="3u3nmv" value="5675576922574815233" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="37vLTw" id="9U" role="3K4GZi">
                                              <ref role="3cqZAo" node="9e" resolve="scope" />
                                              <node concept="cd27G" id="a2" role="lGtFl">
                                                <node concept="3u3nmq" id="a3" role="cd27D">
                                                  <property role="3u3nmv" value="5675576922574815233" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="3clFbC" id="9V" role="3K4Cdx">
                                              <node concept="10Nm6u" id="a4" role="3uHU7w">
                                                <node concept="cd27G" id="a7" role="lGtFl">
                                                  <node concept="3u3nmq" id="a8" role="cd27D">
                                                    <property role="3u3nmv" value="5675576922574815233" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="37vLTw" id="a5" role="3uHU7B">
                                                <ref role="3cqZAo" node="9e" resolve="scope" />
                                                <node concept="cd27G" id="a9" role="lGtFl">
                                                  <node concept="3u3nmq" id="aa" role="cd27D">
                                                    <property role="3u3nmv" value="5675576922574815233" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="cd27G" id="a6" role="lGtFl">
                                                <node concept="3u3nmq" id="ab" role="cd27D">
                                                  <property role="3u3nmv" value="5675576922574815233" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="cd27G" id="9W" role="lGtFl">
                                              <node concept="3u3nmq" id="ac" role="cd27D">
                                                <property role="3u3nmv" value="5675576922574815233" />
                                              </node>
                                            </node>
                                          </node>
                                          <node concept="cd27G" id="9S" role="lGtFl">
                                            <node concept="3u3nmq" id="ad" role="cd27D">
                                              <property role="3u3nmv" value="5675576922574815233" />
                                            </node>
                                          </node>
                                        </node>
                                        <node concept="cd27G" id="9d" role="lGtFl">
                                          <node concept="3u3nmq" id="ae" role="cd27D">
                                            <property role="3u3nmv" value="5675576922574815233" />
                                          </node>
                                        </node>
                                      </node>
                                      <node concept="cd27G" id="9a" role="lGtFl">
                                        <node concept="3u3nmq" id="af" role="cd27D">
                                          <property role="3u3nmv" value="5675576922574815233" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="cd27G" id="98" role="lGtFl">
                                      <node concept="3u3nmq" id="ag" role="cd27D">
                                        <property role="3u3nmv" value="5675576922574815233" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="2AHcQZ" id="8R" role="2AJF6D">
                                    <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
                                    <node concept="cd27G" id="ah" role="lGtFl">
                                      <node concept="3u3nmq" id="ai" role="cd27D">
                                        <property role="3u3nmv" value="5675576922574815233" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="cd27G" id="8S" role="lGtFl">
                                    <node concept="3u3nmq" id="aj" role="cd27D">
                                      <property role="3u3nmv" value="5675576922574815233" />
                                    </node>
                                  </node>
                                </node>
                                <node concept="cd27G" id="8f" role="lGtFl">
                                  <node concept="3u3nmq" id="ak" role="cd27D">
                                    <property role="3u3nmv" value="5675576922574815233" />
                                  </node>
                                </node>
                              </node>
                              <node concept="cd27G" id="8b" role="lGtFl">
                                <node concept="3u3nmq" id="al" role="cd27D">
                                  <property role="3u3nmv" value="5675576922574815233" />
                                </node>
                              </node>
                            </node>
                            <node concept="cd27G" id="89" role="lGtFl">
                              <node concept="3u3nmq" id="am" role="cd27D">
                                <property role="3u3nmv" value="5675576922574815233" />
                              </node>
                            </node>
                          </node>
                          <node concept="cd27G" id="87" role="lGtFl">
                            <node concept="3u3nmq" id="an" role="cd27D">
                              <property role="3u3nmv" value="5675576922574815233" />
                            </node>
                          </node>
                        </node>
                        <node concept="cd27G" id="85" role="lGtFl">
                          <node concept="3u3nmq" id="ao" role="cd27D">
                            <property role="3u3nmv" value="5675576922574815233" />
                          </node>
                        </node>
                      </node>
                      <node concept="2AHcQZ" id="7W" role="2AJF6D">
                        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
                        <node concept="cd27G" id="ap" role="lGtFl">
                          <node concept="3u3nmq" id="aq" role="cd27D">
                            <property role="3u3nmv" value="5675576922574815233" />
                          </node>
                        </node>
                      </node>
                      <node concept="cd27G" id="7X" role="lGtFl">
                        <node concept="3u3nmq" id="ar" role="cd27D">
                          <property role="3u3nmv" value="5675576922574815233" />
                        </node>
                      </node>
                    </node>
                    <node concept="cd27G" id="7h" role="lGtFl">
                      <node concept="3u3nmq" id="as" role="cd27D">
                        <property role="3u3nmv" value="5675576922574815233" />
                      </node>
                    </node>
                  </node>
                  <node concept="cd27G" id="7b" role="lGtFl">
                    <node concept="3u3nmq" id="at" role="cd27D">
                      <property role="3u3nmv" value="5675576922574815233" />
                    </node>
                  </node>
                </node>
                <node concept="cd27G" id="79" role="lGtFl">
                  <node concept="3u3nmq" id="au" role="cd27D">
                    <property role="3u3nmv" value="5675576922574815233" />
                  </node>
                </node>
              </node>
              <node concept="cd27G" id="6Q" role="lGtFl">
                <node concept="3u3nmq" id="av" role="cd27D">
                  <property role="3u3nmv" value="5675576922574815233" />
                </node>
              </node>
            </node>
            <node concept="cd27G" id="6L" role="lGtFl">
              <node concept="3u3nmq" id="aw" role="cd27D">
                <property role="3u3nmv" value="5675576922574815233" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="6I" role="lGtFl">
            <node concept="3u3nmq" id="ax" role="cd27D">
              <property role="3u3nmv" value="5675576922574815233" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="6h" role="3cqZAp">
          <node concept="37vLTw" id="ay" role="3clFbG">
            <ref role="3cqZAo" node="6j" resolve="references" />
            <node concept="cd27G" id="a$" role="lGtFl">
              <node concept="3u3nmq" id="a_" role="cd27D">
                <property role="3u3nmv" value="5675576922574815233" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="az" role="lGtFl">
            <node concept="3u3nmq" id="aA" role="cd27D">
              <property role="3u3nmv" value="5675576922574815233" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="6i" role="lGtFl">
          <node concept="3u3nmq" id="aB" role="cd27D">
            <property role="3u3nmv" value="5675576922574815233" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="63" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <node concept="cd27G" id="aC" role="lGtFl">
          <node concept="3u3nmq" id="aD" role="cd27D">
            <property role="3u3nmv" value="5675576922574815233" />
          </node>
        </node>
      </node>
      <node concept="cd27G" id="64" role="lGtFl">
        <node concept="3u3nmq" id="aE" role="cd27D">
          <property role="3u3nmv" value="5675576922574815233" />
        </node>
      </node>
    </node>
    <node concept="cd27G" id="5s" role="lGtFl">
      <node concept="3u3nmq" id="aF" role="cd27D">
        <property role="3u3nmv" value="5675576922574815233" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="aG">
    <property role="3GE5qa" value="aspects.patterns" />
    <property role="TrG5h" value="BiochemistryTestPattern_Constraints" />
    <node concept="3Tm1VV" id="aH" role="1B3o_S">
      <node concept="cd27G" id="aO" role="lGtFl">
        <node concept="3u3nmq" id="aP" role="cd27D">
          <property role="3u3nmv" value="7816353213402936928" />
        </node>
      </node>
    </node>
    <node concept="3uibUv" id="aI" role="1zkMxy">
      <ref role="3uigEE" to="79pl:~BaseConstraintsDescriptor" resolve="BaseConstraintsDescriptor" />
      <node concept="cd27G" id="aQ" role="lGtFl">
        <node concept="3u3nmq" id="aR" role="cd27D">
          <property role="3u3nmv" value="7816353213402936928" />
        </node>
      </node>
    </node>
    <node concept="3clFbW" id="aJ" role="jymVt">
      <node concept="3cqZAl" id="aS" role="3clF45">
        <node concept="cd27G" id="aW" role="lGtFl">
          <node concept="3u3nmq" id="aX" role="cd27D">
            <property role="3u3nmv" value="7816353213402936928" />
          </node>
        </node>
      </node>
      <node concept="3clFbS" id="aT" role="3clF47">
        <node concept="XkiVB" id="aY" role="3cqZAp">
          <ref role="37wK5l" to="79pl:~BaseConstraintsDescriptor.&lt;init&gt;(org.jetbrains.mps.openapi.language.SAbstractConcept)" resolve="BaseConstraintsDescriptor" />
          <node concept="2YIFZM" id="b0" role="37wK5m">
            <ref role="1Pybhc" to="2k9e:~MetaAdapterFactory" resolve="MetaAdapterFactory" />
            <ref role="37wK5l" to="2k9e:~MetaAdapterFactory.getConcept(long,long,long,java.lang.String):org.jetbrains.mps.openapi.language.SConcept" resolve="getConcept" />
            <node concept="1adDum" id="b2" role="37wK5m">
              <property role="1adDun" value="0x76146113297e4cbeL" />
              <node concept="cd27G" id="b7" role="lGtFl">
                <node concept="3u3nmq" id="b8" role="cd27D">
                  <property role="3u3nmv" value="7816353213402936928" />
                </node>
              </node>
            </node>
            <node concept="1adDum" id="b3" role="37wK5m">
              <property role="1adDun" value="0x99cf714ae175bb83L" />
              <node concept="cd27G" id="b9" role="lGtFl">
                <node concept="3u3nmq" id="ba" role="cd27D">
                  <property role="3u3nmv" value="7816353213402936928" />
                </node>
              </node>
            </node>
            <node concept="1adDum" id="b4" role="37wK5m">
              <property role="1adDun" value="0x6c7943d5b3dd50fcL" />
              <node concept="cd27G" id="bb" role="lGtFl">
                <node concept="3u3nmq" id="bc" role="cd27D">
                  <property role="3u3nmv" value="7816353213402936928" />
                </node>
              </node>
            </node>
            <node concept="Xl_RD" id="b5" role="37wK5m">
              <property role="Xl_RC" value="no.uio.mLab.decisions.references.biochemistry.structure.BiochemistryTestPattern" />
              <node concept="cd27G" id="bd" role="lGtFl">
                <node concept="3u3nmq" id="be" role="cd27D">
                  <property role="3u3nmv" value="7816353213402936928" />
                </node>
              </node>
            </node>
            <node concept="cd27G" id="b6" role="lGtFl">
              <node concept="3u3nmq" id="bf" role="cd27D">
                <property role="3u3nmv" value="7816353213402936928" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="b1" role="lGtFl">
            <node concept="3u3nmq" id="bg" role="cd27D">
              <property role="3u3nmv" value="7816353213402936928" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="aZ" role="lGtFl">
          <node concept="3u3nmq" id="bh" role="cd27D">
            <property role="3u3nmv" value="7816353213402936928" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="aU" role="1B3o_S">
        <node concept="cd27G" id="bi" role="lGtFl">
          <node concept="3u3nmq" id="bj" role="cd27D">
            <property role="3u3nmv" value="7816353213402936928" />
          </node>
        </node>
      </node>
      <node concept="cd27G" id="aV" role="lGtFl">
        <node concept="3u3nmq" id="bk" role="cd27D">
          <property role="3u3nmv" value="7816353213402936928" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="aK" role="jymVt">
      <node concept="cd27G" id="bl" role="lGtFl">
        <node concept="3u3nmq" id="bm" role="cd27D">
          <property role="3u3nmv" value="7816353213402936928" />
        </node>
      </node>
    </node>
    <node concept="3clFb_" id="aL" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="calculateCanBeChildConstraint" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3Tmbuc" id="bn" role="1B3o_S">
        <node concept="cd27G" id="bs" role="lGtFl">
          <node concept="3u3nmq" id="bt" role="cd27D">
            <property role="3u3nmv" value="7816353213402936928" />
          </node>
        </node>
      </node>
      <node concept="3uibUv" id="bo" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConstraintFunction" resolve="ConstraintFunction" />
        <node concept="3uibUv" id="bu" role="11_B2D">
          <ref role="3uigEE" to="ze1i:~ConstraintContext_CanBeChild" resolve="ConstraintContext_CanBeChild" />
          <node concept="cd27G" id="bx" role="lGtFl">
            <node concept="3u3nmq" id="by" role="cd27D">
              <property role="3u3nmv" value="7816353213402936928" />
            </node>
          </node>
        </node>
        <node concept="3uibUv" id="bv" role="11_B2D">
          <ref role="3uigEE" to="wyt6:~Boolean" resolve="Boolean" />
          <node concept="cd27G" id="bz" role="lGtFl">
            <node concept="3u3nmq" id="b$" role="cd27D">
              <property role="3u3nmv" value="7816353213402936928" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="bw" role="lGtFl">
          <node concept="3u3nmq" id="b_" role="cd27D">
            <property role="3u3nmv" value="7816353213402936928" />
          </node>
        </node>
      </node>
      <node concept="3clFbS" id="bp" role="3clF47">
        <node concept="3clFbF" id="bA" role="3cqZAp">
          <node concept="2ShNRf" id="bC" role="3clFbG">
            <node concept="YeOm9" id="bE" role="2ShVmc">
              <node concept="1Y3b0j" id="bG" role="YeSDq">
                <property role="2bfB8j" value="true" />
                <ref role="1Y3XeK" to="ze1i:~ConstraintFunction" resolve="ConstraintFunction" />
                <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
                <node concept="3Tm1VV" id="bI" role="1B3o_S">
                  <node concept="cd27G" id="bN" role="lGtFl">
                    <node concept="3u3nmq" id="bO" role="cd27D">
                      <property role="3u3nmv" value="7816353213402936928" />
                    </node>
                  </node>
                </node>
                <node concept="3clFb_" id="bJ" role="jymVt">
                  <property role="1EzhhJ" value="false" />
                  <property role="TrG5h" value="invoke" />
                  <property role="DiZV1" value="false" />
                  <property role="od$2w" value="false" />
                  <node concept="3Tm1VV" id="bP" role="1B3o_S">
                    <node concept="cd27G" id="bW" role="lGtFl">
                      <node concept="3u3nmq" id="bX" role="cd27D">
                        <property role="3u3nmv" value="7816353213402936928" />
                      </node>
                    </node>
                  </node>
                  <node concept="2AHcQZ" id="bQ" role="2AJF6D">
                    <ref role="2AI5Lk" to="mhfm:~NotNull" resolve="NotNull" />
                    <node concept="cd27G" id="bY" role="lGtFl">
                      <node concept="3u3nmq" id="bZ" role="cd27D">
                        <property role="3u3nmv" value="7816353213402936928" />
                      </node>
                    </node>
                  </node>
                  <node concept="3uibUv" id="bR" role="3clF45">
                    <ref role="3uigEE" to="wyt6:~Boolean" resolve="Boolean" />
                    <node concept="cd27G" id="c0" role="lGtFl">
                      <node concept="3u3nmq" id="c1" role="cd27D">
                        <property role="3u3nmv" value="7816353213402936928" />
                      </node>
                    </node>
                  </node>
                  <node concept="37vLTG" id="bS" role="3clF46">
                    <property role="TrG5h" value="context" />
                    <node concept="3uibUv" id="c2" role="1tU5fm">
                      <ref role="3uigEE" to="ze1i:~ConstraintContext_CanBeChild" resolve="ConstraintContext_CanBeChild" />
                      <node concept="cd27G" id="c5" role="lGtFl">
                        <node concept="3u3nmq" id="c6" role="cd27D">
                          <property role="3u3nmv" value="7816353213402936928" />
                        </node>
                      </node>
                    </node>
                    <node concept="2AHcQZ" id="c3" role="2AJF6D">
                      <ref role="2AI5Lk" to="mhfm:~NotNull" resolve="NotNull" />
                      <node concept="cd27G" id="c7" role="lGtFl">
                        <node concept="3u3nmq" id="c8" role="cd27D">
                          <property role="3u3nmv" value="7816353213402936928" />
                        </node>
                      </node>
                    </node>
                    <node concept="cd27G" id="c4" role="lGtFl">
                      <node concept="3u3nmq" id="c9" role="cd27D">
                        <property role="3u3nmv" value="7816353213402936928" />
                      </node>
                    </node>
                  </node>
                  <node concept="37vLTG" id="bT" role="3clF46">
                    <property role="TrG5h" value="checkingNodeContext" />
                    <node concept="3uibUv" id="ca" role="1tU5fm">
                      <ref role="3uigEE" to="ze1i:~CheckingNodeContext" resolve="CheckingNodeContext" />
                      <node concept="cd27G" id="cd" role="lGtFl">
                        <node concept="3u3nmq" id="ce" role="cd27D">
                          <property role="3u3nmv" value="7816353213402936928" />
                        </node>
                      </node>
                    </node>
                    <node concept="2AHcQZ" id="cb" role="2AJF6D">
                      <ref role="2AI5Lk" to="mhfm:~Nullable" resolve="Nullable" />
                      <node concept="cd27G" id="cf" role="lGtFl">
                        <node concept="3u3nmq" id="cg" role="cd27D">
                          <property role="3u3nmv" value="7816353213402936928" />
                        </node>
                      </node>
                    </node>
                    <node concept="cd27G" id="cc" role="lGtFl">
                      <node concept="3u3nmq" id="ch" role="cd27D">
                        <property role="3u3nmv" value="7816353213402936928" />
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbS" id="bU" role="3clF47">
                    <node concept="3cpWs8" id="ci" role="3cqZAp">
                      <node concept="3cpWsn" id="co" role="3cpWs9">
                        <property role="TrG5h" value="result" />
                        <node concept="10P_77" id="cq" role="1tU5fm">
                          <node concept="cd27G" id="ct" role="lGtFl">
                            <node concept="3u3nmq" id="cu" role="cd27D">
                              <property role="3u3nmv" value="7816353213402936928" />
                            </node>
                          </node>
                        </node>
                        <node concept="1rXfSq" id="cr" role="33vP2m">
                          <ref role="37wK5l" node="aM" resolve="staticCanBeAChild" />
                          <node concept="2OqwBi" id="cv" role="37wK5m">
                            <node concept="37vLTw" id="c$" role="2Oq$k0">
                              <ref role="3cqZAo" node="bS" resolve="context" />
                              <node concept="cd27G" id="cB" role="lGtFl">
                                <node concept="3u3nmq" id="cC" role="cd27D">
                                  <property role="3u3nmv" value="7816353213402936928" />
                                </node>
                              </node>
                            </node>
                            <node concept="liA8E" id="c_" role="2OqNvi">
                              <ref role="37wK5l" to="ze1i:~ConstraintContext_CanBeChild.getNode():org.jetbrains.mps.openapi.model.SNode" resolve="getNode" />
                              <node concept="cd27G" id="cD" role="lGtFl">
                                <node concept="3u3nmq" id="cE" role="cd27D">
                                  <property role="3u3nmv" value="7816353213402936928" />
                                </node>
                              </node>
                            </node>
                            <node concept="cd27G" id="cA" role="lGtFl">
                              <node concept="3u3nmq" id="cF" role="cd27D">
                                <property role="3u3nmv" value="7816353213402936928" />
                              </node>
                            </node>
                          </node>
                          <node concept="2OqwBi" id="cw" role="37wK5m">
                            <node concept="37vLTw" id="cG" role="2Oq$k0">
                              <ref role="3cqZAo" node="bS" resolve="context" />
                              <node concept="cd27G" id="cJ" role="lGtFl">
                                <node concept="3u3nmq" id="cK" role="cd27D">
                                  <property role="3u3nmv" value="7816353213402936928" />
                                </node>
                              </node>
                            </node>
                            <node concept="liA8E" id="cH" role="2OqNvi">
                              <ref role="37wK5l" to="ze1i:~ConstraintContext_CanBeChild.getParentNode():org.jetbrains.mps.openapi.model.SNode" resolve="getParentNode" />
                              <node concept="cd27G" id="cL" role="lGtFl">
                                <node concept="3u3nmq" id="cM" role="cd27D">
                                  <property role="3u3nmv" value="7816353213402936928" />
                                </node>
                              </node>
                            </node>
                            <node concept="cd27G" id="cI" role="lGtFl">
                              <node concept="3u3nmq" id="cN" role="cd27D">
                                <property role="3u3nmv" value="7816353213402936928" />
                              </node>
                            </node>
                          </node>
                          <node concept="2OqwBi" id="cx" role="37wK5m">
                            <node concept="37vLTw" id="cO" role="2Oq$k0">
                              <ref role="3cqZAo" node="bS" resolve="context" />
                              <node concept="cd27G" id="cR" role="lGtFl">
                                <node concept="3u3nmq" id="cS" role="cd27D">
                                  <property role="3u3nmv" value="7816353213402936928" />
                                </node>
                              </node>
                            </node>
                            <node concept="liA8E" id="cP" role="2OqNvi">
                              <ref role="37wK5l" to="ze1i:~ConstraintContext_CanBeChild.getConcept():org.jetbrains.mps.openapi.language.SAbstractConcept" resolve="getConcept" />
                              <node concept="cd27G" id="cT" role="lGtFl">
                                <node concept="3u3nmq" id="cU" role="cd27D">
                                  <property role="3u3nmv" value="7816353213402936928" />
                                </node>
                              </node>
                            </node>
                            <node concept="cd27G" id="cQ" role="lGtFl">
                              <node concept="3u3nmq" id="cV" role="cd27D">
                                <property role="3u3nmv" value="7816353213402936928" />
                              </node>
                            </node>
                          </node>
                          <node concept="2OqwBi" id="cy" role="37wK5m">
                            <node concept="37vLTw" id="cW" role="2Oq$k0">
                              <ref role="3cqZAo" node="bS" resolve="context" />
                              <node concept="cd27G" id="cZ" role="lGtFl">
                                <node concept="3u3nmq" id="d0" role="cd27D">
                                  <property role="3u3nmv" value="7816353213402936928" />
                                </node>
                              </node>
                            </node>
                            <node concept="liA8E" id="cX" role="2OqNvi">
                              <ref role="37wK5l" to="ze1i:~ConstraintContext_CanBeChild.getLink():org.jetbrains.mps.openapi.language.SContainmentLink" resolve="getLink" />
                              <node concept="cd27G" id="d1" role="lGtFl">
                                <node concept="3u3nmq" id="d2" role="cd27D">
                                  <property role="3u3nmv" value="7816353213402936928" />
                                </node>
                              </node>
                            </node>
                            <node concept="cd27G" id="cY" role="lGtFl">
                              <node concept="3u3nmq" id="d3" role="cd27D">
                                <property role="3u3nmv" value="7816353213402936928" />
                              </node>
                            </node>
                          </node>
                          <node concept="cd27G" id="cz" role="lGtFl">
                            <node concept="3u3nmq" id="d4" role="cd27D">
                              <property role="3u3nmv" value="7816353213402936928" />
                            </node>
                          </node>
                        </node>
                        <node concept="cd27G" id="cs" role="lGtFl">
                          <node concept="3u3nmq" id="d5" role="cd27D">
                            <property role="3u3nmv" value="7816353213402936928" />
                          </node>
                        </node>
                      </node>
                      <node concept="cd27G" id="cp" role="lGtFl">
                        <node concept="3u3nmq" id="d6" role="cd27D">
                          <property role="3u3nmv" value="7816353213402936928" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbH" id="cj" role="3cqZAp">
                      <node concept="cd27G" id="d7" role="lGtFl">
                        <node concept="3u3nmq" id="d8" role="cd27D">
                          <property role="3u3nmv" value="7816353213402936928" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbJ" id="ck" role="3cqZAp">
                      <node concept="3clFbS" id="d9" role="3clFbx">
                        <node concept="3clFbF" id="dc" role="3cqZAp">
                          <node concept="2OqwBi" id="de" role="3clFbG">
                            <node concept="37vLTw" id="dg" role="2Oq$k0">
                              <ref role="3cqZAo" node="bT" resolve="checkingNodeContext" />
                              <node concept="cd27G" id="dj" role="lGtFl">
                                <node concept="3u3nmq" id="dk" role="cd27D">
                                  <property role="3u3nmv" value="7816353213402936928" />
                                </node>
                              </node>
                            </node>
                            <node concept="liA8E" id="dh" role="2OqNvi">
                              <ref role="37wK5l" to="ze1i:~CheckingNodeContext.setBreakingNode(org.jetbrains.mps.openapi.model.SNodeReference):void" resolve="setBreakingNode" />
                              <node concept="1dyn4i" id="dl" role="37wK5m">
                                <property role="1dyqJU" value="canBeChildBreakingPoint" />
                                <node concept="2ShNRf" id="dn" role="1dyrYi">
                                  <node concept="1pGfFk" id="dp" role="2ShVmc">
                                    <ref role="37wK5l" to="w1kc:~SNodePointer.&lt;init&gt;(java.lang.String,java.lang.String)" resolve="SNodePointer" />
                                    <node concept="Xl_RD" id="dr" role="37wK5m">
                                      <property role="Xl_RC" value="r:62707f82-6ccf-4b1b-b39a-a18182073111(no.uio.mLab.decisions.references.biochemistry.constraints)" />
                                      <node concept="cd27G" id="du" role="lGtFl">
                                        <node concept="3u3nmq" id="dv" role="cd27D">
                                          <property role="3u3nmv" value="7816353213402936928" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="Xl_RD" id="ds" role="37wK5m">
                                      <property role="Xl_RC" value="7816353213402936929" />
                                      <node concept="cd27G" id="dw" role="lGtFl">
                                        <node concept="3u3nmq" id="dx" role="cd27D">
                                          <property role="3u3nmv" value="7816353213402936928" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="cd27G" id="dt" role="lGtFl">
                                      <node concept="3u3nmq" id="dy" role="cd27D">
                                        <property role="3u3nmv" value="7816353213402936928" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="cd27G" id="dq" role="lGtFl">
                                    <node concept="3u3nmq" id="dz" role="cd27D">
                                      <property role="3u3nmv" value="7816353213402936928" />
                                    </node>
                                  </node>
                                </node>
                                <node concept="cd27G" id="do" role="lGtFl">
                                  <node concept="3u3nmq" id="d$" role="cd27D">
                                    <property role="3u3nmv" value="7816353213402936928" />
                                  </node>
                                </node>
                              </node>
                              <node concept="cd27G" id="dm" role="lGtFl">
                                <node concept="3u3nmq" id="d_" role="cd27D">
                                  <property role="3u3nmv" value="7816353213402936928" />
                                </node>
                              </node>
                            </node>
                            <node concept="cd27G" id="di" role="lGtFl">
                              <node concept="3u3nmq" id="dA" role="cd27D">
                                <property role="3u3nmv" value="7816353213402936928" />
                              </node>
                            </node>
                          </node>
                          <node concept="cd27G" id="df" role="lGtFl">
                            <node concept="3u3nmq" id="dB" role="cd27D">
                              <property role="3u3nmv" value="7816353213402936928" />
                            </node>
                          </node>
                        </node>
                        <node concept="cd27G" id="dd" role="lGtFl">
                          <node concept="3u3nmq" id="dC" role="cd27D">
                            <property role="3u3nmv" value="7816353213402936928" />
                          </node>
                        </node>
                      </node>
                      <node concept="1Wc70l" id="da" role="3clFbw">
                        <node concept="3y3z36" id="dD" role="3uHU7w">
                          <node concept="10Nm6u" id="dG" role="3uHU7w">
                            <node concept="cd27G" id="dJ" role="lGtFl">
                              <node concept="3u3nmq" id="dK" role="cd27D">
                                <property role="3u3nmv" value="7816353213402936928" />
                              </node>
                            </node>
                          </node>
                          <node concept="37vLTw" id="dH" role="3uHU7B">
                            <ref role="3cqZAo" node="bT" resolve="checkingNodeContext" />
                            <node concept="cd27G" id="dL" role="lGtFl">
                              <node concept="3u3nmq" id="dM" role="cd27D">
                                <property role="3u3nmv" value="7816353213402936928" />
                              </node>
                            </node>
                          </node>
                          <node concept="cd27G" id="dI" role="lGtFl">
                            <node concept="3u3nmq" id="dN" role="cd27D">
                              <property role="3u3nmv" value="7816353213402936928" />
                            </node>
                          </node>
                        </node>
                        <node concept="3fqX7Q" id="dE" role="3uHU7B">
                          <node concept="37vLTw" id="dO" role="3fr31v">
                            <ref role="3cqZAo" node="co" resolve="result" />
                            <node concept="cd27G" id="dQ" role="lGtFl">
                              <node concept="3u3nmq" id="dR" role="cd27D">
                                <property role="3u3nmv" value="7816353213402936928" />
                              </node>
                            </node>
                          </node>
                          <node concept="cd27G" id="dP" role="lGtFl">
                            <node concept="3u3nmq" id="dS" role="cd27D">
                              <property role="3u3nmv" value="7816353213402936928" />
                            </node>
                          </node>
                        </node>
                        <node concept="cd27G" id="dF" role="lGtFl">
                          <node concept="3u3nmq" id="dT" role="cd27D">
                            <property role="3u3nmv" value="7816353213402936928" />
                          </node>
                        </node>
                      </node>
                      <node concept="cd27G" id="db" role="lGtFl">
                        <node concept="3u3nmq" id="dU" role="cd27D">
                          <property role="3u3nmv" value="7816353213402936928" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbH" id="cl" role="3cqZAp">
                      <node concept="cd27G" id="dV" role="lGtFl">
                        <node concept="3u3nmq" id="dW" role="cd27D">
                          <property role="3u3nmv" value="7816353213402936928" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbF" id="cm" role="3cqZAp">
                      <node concept="37vLTw" id="dX" role="3clFbG">
                        <ref role="3cqZAo" node="co" resolve="result" />
                        <node concept="cd27G" id="dZ" role="lGtFl">
                          <node concept="3u3nmq" id="e0" role="cd27D">
                            <property role="3u3nmv" value="7816353213402936928" />
                          </node>
                        </node>
                      </node>
                      <node concept="cd27G" id="dY" role="lGtFl">
                        <node concept="3u3nmq" id="e1" role="cd27D">
                          <property role="3u3nmv" value="7816353213402936928" />
                        </node>
                      </node>
                    </node>
                    <node concept="cd27G" id="cn" role="lGtFl">
                      <node concept="3u3nmq" id="e2" role="cd27D">
                        <property role="3u3nmv" value="7816353213402936928" />
                      </node>
                    </node>
                  </node>
                  <node concept="cd27G" id="bV" role="lGtFl">
                    <node concept="3u3nmq" id="e3" role="cd27D">
                      <property role="3u3nmv" value="7816353213402936928" />
                    </node>
                  </node>
                </node>
                <node concept="3uibUv" id="bK" role="2Ghqu4">
                  <ref role="3uigEE" to="ze1i:~ConstraintContext_CanBeChild" resolve="ConstraintContext_CanBeChild" />
                  <node concept="cd27G" id="e4" role="lGtFl">
                    <node concept="3u3nmq" id="e5" role="cd27D">
                      <property role="3u3nmv" value="7816353213402936928" />
                    </node>
                  </node>
                </node>
                <node concept="3uibUv" id="bL" role="2Ghqu4">
                  <ref role="3uigEE" to="wyt6:~Boolean" resolve="Boolean" />
                  <node concept="cd27G" id="e6" role="lGtFl">
                    <node concept="3u3nmq" id="e7" role="cd27D">
                      <property role="3u3nmv" value="7816353213402936928" />
                    </node>
                  </node>
                </node>
                <node concept="cd27G" id="bM" role="lGtFl">
                  <node concept="3u3nmq" id="e8" role="cd27D">
                    <property role="3u3nmv" value="7816353213402936928" />
                  </node>
                </node>
              </node>
              <node concept="cd27G" id="bH" role="lGtFl">
                <node concept="3u3nmq" id="e9" role="cd27D">
                  <property role="3u3nmv" value="7816353213402936928" />
                </node>
              </node>
            </node>
            <node concept="cd27G" id="bF" role="lGtFl">
              <node concept="3u3nmq" id="ea" role="cd27D">
                <property role="3u3nmv" value="7816353213402936928" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="bD" role="lGtFl">
            <node concept="3u3nmq" id="eb" role="cd27D">
              <property role="3u3nmv" value="7816353213402936928" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="bB" role="lGtFl">
          <node concept="3u3nmq" id="ec" role="cd27D">
            <property role="3u3nmv" value="7816353213402936928" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="bq" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <node concept="cd27G" id="ed" role="lGtFl">
          <node concept="3u3nmq" id="ee" role="cd27D">
            <property role="3u3nmv" value="7816353213402936928" />
          </node>
        </node>
      </node>
      <node concept="cd27G" id="br" role="lGtFl">
        <node concept="3u3nmq" id="ef" role="cd27D">
          <property role="3u3nmv" value="7816353213402936928" />
        </node>
      </node>
    </node>
    <node concept="2YIFZL" id="aM" role="jymVt">
      <property role="TrG5h" value="staticCanBeAChild" />
      <node concept="10P_77" id="eg" role="3clF45">
        <node concept="cd27G" id="eo" role="lGtFl">
          <node concept="3u3nmq" id="ep" role="cd27D">
            <property role="3u3nmv" value="7816353213402936928" />
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="eh" role="1B3o_S">
        <node concept="cd27G" id="eq" role="lGtFl">
          <node concept="3u3nmq" id="er" role="cd27D">
            <property role="3u3nmv" value="7816353213402936928" />
          </node>
        </node>
      </node>
      <node concept="3clFbS" id="ei" role="3clF47">
        <node concept="3clFbF" id="es" role="3cqZAp">
          <node concept="2OqwBi" id="eu" role="3clFbG">
            <node concept="37vLTw" id="ew" role="2Oq$k0">
              <ref role="3cqZAo" node="ek" resolve="parentNode" />
              <node concept="cd27G" id="ez" role="lGtFl">
                <node concept="3u3nmq" id="e$" role="cd27D">
                  <property role="3u3nmv" value="7816353213402937396" />
                </node>
              </node>
            </node>
            <node concept="1mIQ4w" id="ex" role="2OqNvi">
              <node concept="chp4Y" id="e_" role="cj9EA">
                <ref role="cht4Q" to="flit:4QUW3edEuTG" resolve="BiochemistryTestVariable" />
                <node concept="cd27G" id="eB" role="lGtFl">
                  <node concept="3u3nmq" id="eC" role="cd27D">
                    <property role="3u3nmv" value="7816353213402940004" />
                  </node>
                </node>
              </node>
              <node concept="cd27G" id="eA" role="lGtFl">
                <node concept="3u3nmq" id="eD" role="cd27D">
                  <property role="3u3nmv" value="7816353213402939191" />
                </node>
              </node>
            </node>
            <node concept="cd27G" id="ey" role="lGtFl">
              <node concept="3u3nmq" id="eE" role="cd27D">
                <property role="3u3nmv" value="7816353213402938381" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="ev" role="lGtFl">
            <node concept="3u3nmq" id="eF" role="cd27D">
              <property role="3u3nmv" value="7816353213402937397" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="et" role="lGtFl">
          <node concept="3u3nmq" id="eG" role="cd27D">
            <property role="3u3nmv" value="7816353213402936930" />
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="ej" role="3clF46">
        <property role="TrG5h" value="node" />
        <node concept="3uibUv" id="eH" role="1tU5fm">
          <ref role="3uigEE" to="mhbf:~SNode" resolve="SNode" />
          <node concept="cd27G" id="eJ" role="lGtFl">
            <node concept="3u3nmq" id="eK" role="cd27D">
              <property role="3u3nmv" value="7816353213402936928" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="eI" role="lGtFl">
          <node concept="3u3nmq" id="eL" role="cd27D">
            <property role="3u3nmv" value="7816353213402936928" />
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="ek" role="3clF46">
        <property role="TrG5h" value="parentNode" />
        <node concept="3uibUv" id="eM" role="1tU5fm">
          <ref role="3uigEE" to="mhbf:~SNode" resolve="SNode" />
          <node concept="cd27G" id="eO" role="lGtFl">
            <node concept="3u3nmq" id="eP" role="cd27D">
              <property role="3u3nmv" value="7816353213402936928" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="eN" role="lGtFl">
          <node concept="3u3nmq" id="eQ" role="cd27D">
            <property role="3u3nmv" value="7816353213402936928" />
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="el" role="3clF46">
        <property role="TrG5h" value="childConcept" />
        <node concept="3uibUv" id="eR" role="1tU5fm">
          <ref role="3uigEE" to="c17a:~SAbstractConcept" resolve="SAbstractConcept" />
          <node concept="cd27G" id="eT" role="lGtFl">
            <node concept="3u3nmq" id="eU" role="cd27D">
              <property role="3u3nmv" value="7816353213402936928" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="eS" role="lGtFl">
          <node concept="3u3nmq" id="eV" role="cd27D">
            <property role="3u3nmv" value="7816353213402936928" />
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="em" role="3clF46">
        <property role="TrG5h" value="link" />
        <node concept="3uibUv" id="eW" role="1tU5fm">
          <ref role="3uigEE" to="c17a:~SContainmentLink" resolve="SContainmentLink" />
          <node concept="cd27G" id="eY" role="lGtFl">
            <node concept="3u3nmq" id="eZ" role="cd27D">
              <property role="3u3nmv" value="7816353213402936928" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="eX" role="lGtFl">
          <node concept="3u3nmq" id="f0" role="cd27D">
            <property role="3u3nmv" value="7816353213402936928" />
          </node>
        </node>
      </node>
      <node concept="cd27G" id="en" role="lGtFl">
        <node concept="3u3nmq" id="f1" role="cd27D">
          <property role="3u3nmv" value="7816353213402936928" />
        </node>
      </node>
    </node>
    <node concept="cd27G" id="aN" role="lGtFl">
      <node concept="3u3nmq" id="f2" role="cd27D">
        <property role="3u3nmv" value="7816353213402936928" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="f3">
    <property role="TrG5h" value="ConstraintsAspectDescriptor" />
    <property role="3GE5qa" value="Constraints" />
    <node concept="3uibUv" id="f4" role="1zkMxy">
      <ref role="3uigEE" to="ze1i:~BaseConstraintsAspectDescriptor" resolve="BaseConstraintsAspectDescriptor" />
    </node>
    <node concept="3Tm1VV" id="f5" role="1B3o_S" />
    <node concept="3clFbW" id="f6" role="jymVt">
      <node concept="3cqZAl" id="f9" role="3clF45" />
      <node concept="3Tm1VV" id="fa" role="1B3o_S" />
      <node concept="3clFbS" id="fb" role="3clF47" />
    </node>
    <node concept="2tJIrI" id="f7" role="jymVt" />
    <node concept="3clFb_" id="f8" role="jymVt">
      <property role="IEkAT" value="false" />
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="getConstraints" />
      <property role="DiZV1" value="false" />
      <node concept="2AHcQZ" id="fc" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
      <node concept="3Tm1VV" id="fd" role="1B3o_S" />
      <node concept="3uibUv" id="fe" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConstraintsDescriptor" resolve="ConstraintsDescriptor" />
      </node>
      <node concept="37vLTG" id="ff" role="3clF46">
        <property role="TrG5h" value="concept" />
        <node concept="3bZ5Sz" id="fh" role="1tU5fm" />
      </node>
      <node concept="3clFbS" id="fg" role="3clF47">
        <node concept="1_3QMa" id="fi" role="3cqZAp">
          <node concept="37vLTw" id="fk" role="1_3QMn">
            <ref role="3cqZAo" node="ff" resolve="concept" />
          </node>
          <node concept="1pnPoh" id="fl" role="1_3QMm">
            <node concept="3clFbS" id="fp" role="1pnPq1">
              <node concept="3cpWs6" id="fr" role="3cqZAp">
                <node concept="1nCR9W" id="fs" role="3cqZAk">
                  <property role="1nD$Q0" value="no.uio.mLab.decisions.references.biochemistry.constraints.AspectBiochemistryTestReference_Constraints" />
                  <node concept="3uibUv" id="ft" role="2lIhxL">
                    <ref role="3uigEE" to="ze1i:~ConstraintsDescriptor" resolve="ConstraintsDescriptor" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3gn64h" id="fq" role="1pnPq6">
              <ref role="3gnhBz" to="flit:4QUW3efwCu7" resolve="AspectBiochemistryTestReference" />
            </node>
          </node>
          <node concept="1pnPoh" id="fm" role="1_3QMm">
            <node concept="3clFbS" id="fu" role="1pnPq1">
              <node concept="3cpWs6" id="fw" role="3cqZAp">
                <node concept="1nCR9W" id="fx" role="3cqZAk">
                  <property role="1nD$Q0" value="no.uio.mLab.decisions.references.biochemistry.constraints.AspectBiochemistryTestWithNumberResultReference_Constraints" />
                  <node concept="3uibUv" id="fy" role="2lIhxL">
                    <ref role="3uigEE" to="ze1i:~ConstraintsDescriptor" resolve="ConstraintsDescriptor" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3gn64h" id="fv" role="1pnPq6">
              <ref role="3gnhBz" to="flit:4V3GMfXst2L" resolve="AspectBiochemistryTestWithNumberResultReference" />
            </node>
          </node>
          <node concept="1pnPoh" id="fn" role="1_3QMm">
            <node concept="3clFbS" id="fz" role="1pnPq1">
              <node concept="3cpWs6" id="f_" role="3cqZAp">
                <node concept="1nCR9W" id="fA" role="3cqZAk">
                  <property role="1nD$Q0" value="no.uio.mLab.decisions.references.biochemistry.constraints.BiochemistryTestPattern_Constraints" />
                  <node concept="3uibUv" id="fB" role="2lIhxL">
                    <ref role="3uigEE" to="ze1i:~ConstraintsDescriptor" resolve="ConstraintsDescriptor" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3gn64h" id="f$" role="1pnPq6">
              <ref role="3gnhBz" to="flit:6LTgXmNRl3W" resolve="BiochemistryTestPattern" />
            </node>
          </node>
          <node concept="3clFbS" id="fo" role="1prKM_" />
        </node>
        <node concept="3cpWs6" id="fj" role="3cqZAp">
          <node concept="2ShNRf" id="fC" role="3cqZAk">
            <node concept="1pGfFk" id="fD" role="2ShVmc">
              <ref role="37wK5l" to="79pl:~BaseConstraintsDescriptor.&lt;init&gt;(org.jetbrains.mps.openapi.language.SAbstractConcept)" resolve="BaseConstraintsDescriptor" />
              <node concept="37vLTw" id="fE" role="37wK5m">
                <ref role="3cqZAo" node="ff" resolve="concept" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="39dXUE" id="fF" />
</model>

