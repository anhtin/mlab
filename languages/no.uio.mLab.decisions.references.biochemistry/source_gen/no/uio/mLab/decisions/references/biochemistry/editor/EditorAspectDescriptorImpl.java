package no.uio.mLab.decisions.references.biochemistry.editor;

/*Generated by MPS */

import jetbrains.mps.nodeEditor.EditorAspectDescriptorBase;
import org.jetbrains.annotations.NotNull;
import java.util.Collection;
import jetbrains.mps.openapi.editor.descriptor.ConceptEditor;
import org.jetbrains.mps.openapi.language.SAbstractConcept;
import java.util.Collections;
import jetbrains.mps.openapi.editor.descriptor.ConceptEditorComponent;
import jetbrains.mps.openapi.editor.descriptor.SubstituteMenu;
import jetbrains.mps.lang.smodel.ConceptSwitchIndex;
import jetbrains.mps.lang.smodel.ConceptSwitchIndexBuilder;
import jetbrains.mps.smodel.adapter.ids.MetaIdFactory;

public class EditorAspectDescriptorImpl extends EditorAspectDescriptorBase {
  @NotNull
  public Collection<ConceptEditor> getDeclaredEditors(SAbstractConcept concept) {
    SAbstractConcept cncpt = ((SAbstractConcept) concept);
    switch (conceptIndex.index(cncpt)) {
      case 0:
        return Collections.<ConceptEditor>singletonList(new BiochemistryTestPattern_Editor());
      case 1:
        return Collections.<ConceptEditor>singletonList(new WildcardBiochemistryTestPattern_Editor());
      default:
    }
    return Collections.<ConceptEditor>emptyList();
  }

  @NotNull
  public Collection<ConceptEditorComponent> getDeclaredEditorComponents(SAbstractConcept concept, String editorComponentId) {
    SAbstractConcept cncpt = ((SAbstractConcept) concept);
    switch (conceptIndex1.index(cncpt)) {
      case 0:
        if (true) {
          if ("no.uio.mLab.decisions.core.editor.Pattern_AspectVariable_EditorComponent".equals(editorComponentId)) {
            return Collections.<ConceptEditorComponent>singletonList(new Pattern_BiochemistryTestVariable_EditorComponent());
          }
        }
        break;
      default:
    }
    return Collections.<ConceptEditorComponent>emptyList();
  }

  @NotNull
  @Override
  public Collection<SubstituteMenu> getDeclaredDefaultSubstituteMenus(SAbstractConcept concept) {
    SAbstractConcept cncpt = concept;
    switch (conceptIndex2.index(cncpt)) {
      case 0:
        return Collections.<SubstituteMenu>singletonList(new AspectBiochemistryTestReference_SubstituteMenu());
      case 1:
        return Collections.<SubstituteMenu>singletonList(new AspectBiochemistryTestWithNumberResultReference_SubstituteMenu());
      case 2:
        return Collections.<SubstituteMenu>singletonList(new BiochemistrySpecialistTypeVariable_SubstituteMenu());
      case 3:
        return Collections.<SubstituteMenu>singletonList(new BiochemistryTestPattern_SubstituteMenu());
      case 4:
        return Collections.<SubstituteMenu>singletonList(new BiochemistryTestReference_SubstituteMenu());
      case 5:
        return Collections.<SubstituteMenu>singletonList(new BiochemistryTestVariable_SubstituteMenu());
      case 6:
        return Collections.<SubstituteMenu>singletonList(new BiochemistryTestWithNumberResultReference_SubstituteMenu());
      case 7:
        return Collections.<SubstituteMenu>singletonList(new EntityBiochemistryTestReference_SubstituteMenu());
      case 8:
        return Collections.<SubstituteMenu>singletonList(new EntityBiochemistryTestWithNumberResultReference_SubstituteMenu());
      case 9:
        return Collections.<SubstituteMenu>singletonList(new WildcardBiochemistryTestPattern_SubstituteMenu());
      default:
    }
    return Collections.<SubstituteMenu>emptyList();
  }

  private static final ConceptSwitchIndex conceptIndex = new ConceptSwitchIndexBuilder().put(MetaIdFactory.conceptId(0x76146113297e4cbeL, 0x99cf714ae175bb83L, 0x6c7943d5b3dd50fcL), MetaIdFactory.conceptId(0x76146113297e4cbeL, 0x99cf714ae175bb83L, 0x6c7943d5b3ebf89eL)).seal();
  private static final ConceptSwitchIndex conceptIndex1 = new ConceptSwitchIndexBuilder().put(MetaIdFactory.conceptId(0x76146113297e4cbeL, 0x99cf714ae175bb83L, 0x4dbaf0338da9ee6cL)).seal();
  private static final ConceptSwitchIndex conceptIndex2 = new ConceptSwitchIndexBuilder().put(MetaIdFactory.conceptId(0x76146113297e4cbeL, 0x99cf714ae175bb83L, 0x4dbaf0338f828787L), MetaIdFactory.conceptId(0x76146113297e4cbeL, 0x99cf714ae175bb83L, 0x4ec3b323fd71d0b1L), MetaIdFactory.conceptId(0x76146113297e4cbeL, 0x99cf714ae175bb83L, 0x4dbaf0338dc55877L), MetaIdFactory.conceptId(0x76146113297e4cbeL, 0x99cf714ae175bb83L, 0x6c7943d5b3dd50fcL), MetaIdFactory.conceptId(0x76146113297e4cbeL, 0x99cf714ae175bb83L, 0x4dbaf0338f828786L), MetaIdFactory.conceptId(0x76146113297e4cbeL, 0x99cf714ae175bb83L, 0x4dbaf0338da9ee6cL), MetaIdFactory.conceptId(0x76146113297e4cbeL, 0x99cf714ae175bb83L, 0x4ec3b323fdbc6d2aL), MetaIdFactory.conceptId(0x76146113297e4cbeL, 0x99cf714ae175bb83L, 0x4dbaf0338f82878aL), MetaIdFactory.conceptId(0x76146113297e4cbeL, 0x99cf714ae175bb83L, 0x4ec3b323fd6ec511L), MetaIdFactory.conceptId(0x76146113297e4cbeL, 0x99cf714ae175bb83L, 0x6c7943d5b3ebf89eL)).seal();
}
