<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:e3723b2c-f747-4846-9cf4-4a6dab4bac5a(no.uio.mLab.decisions.references.laboratoryTest.behavior)">
  <persistence version="9" />
  <languages>
    <use id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior" version="1" />
    <use id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage" version="6" />
    <use id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel" version="11" />
    <use id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions" version="4" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="mmhz" ref="r:e27f2141-c05d-487b-904a-4a3994dcfe64(no.uio.mLab.decisions.references.laboratoryTest.translations)" />
    <import index="wb6c" ref="r:19d15037-83c1-4ae2-b66a-763828cb8279(no.uio.mLab.decisions.core.behavior)" />
    <import index="1yj" ref="r:0c887d67-81ab-420f-9a35-a9f6bf96c975(no.uio.mLab.shared.behavior)" />
    <import index="vbok" ref="r:6cc208d3-a370-49ca-a6ac-ded97805a924(no.uio.mLab.shared.structure)" />
    <import index="aia3" ref="r:856e974b-6e42-49cc-99cd-c3adb4161c21(no.uio.mLab.decisions.data.laboratoryTest.structure)" />
    <import index="ruww" ref="r:d7787915-5b9a-4713-a6ab-6345c5e1ec03(no.uio.mLab.decisions.references.laboratoryTest.structure)" />
    <import index="7f9y" ref="r:790ac37e-42d3-4dd3-977c-e8fe9cc56a45(no.uio.mLab.decisions.core.structure)" />
    <import index="veyb" ref="r:f97bcc9c-211d-441a-840a-958b18315a7e(no.uio.mLab.decisions.data.laboratoryTest.textualResult.structure)" />
    <import index="ydq9" ref="r:1329ca29-1e66-45a8-882e-5bcc59a0ad4c(no.uio.mLab.decisions.data.laboratoryTest.numberResult.structure)" />
    <import index="xvtf" ref="r:506bae70-6209-41ca-8d8b-9690b3810379(no.uio.mLab.entities.laboratoryTest.structure)" />
    <import index="kkto" ref="r:01f3534b-874e-42dc-a9b5-7540591f45bb(no.uio.mLab.entities.core.structure)" />
  </imports>
  <registry>
    <language id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior">
      <concept id="6496299201655527393" name="jetbrains.mps.lang.behavior.structure.LocalBehaviorMethodCall" flags="nn" index="BsUDl" />
      <concept id="1225194240794" name="jetbrains.mps.lang.behavior.structure.ConceptBehavior" flags="ng" index="13h7C7">
        <reference id="1225194240799" name="concept" index="13h7C2" />
        <child id="1225194240805" name="method" index="13h7CS" />
        <child id="1225194240801" name="constructor" index="13h7CW" />
      </concept>
      <concept id="1225194413805" name="jetbrains.mps.lang.behavior.structure.ConceptConstructorDeclaration" flags="in" index="13hLZK" />
      <concept id="1225194472830" name="jetbrains.mps.lang.behavior.structure.ConceptMethodDeclaration" flags="ng" index="13i0hz">
        <property id="5864038008284099149" name="isStatic" index="2Ki8OM" />
        <property id="1225194472832" name="isVirtual" index="13i0it" />
        <property id="1225194472834" name="isAbstract" index="13i0iv" />
        <reference id="1225194472831" name="overriddenMethod" index="13i0hy" />
      </concept>
      <concept id="1225194691553" name="jetbrains.mps.lang.behavior.structure.ThisNodeExpression" flags="nn" index="13iPFW" />
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1070534644030" name="jetbrains.mps.baseLanguage.structure.BooleanType" flags="in" index="10P_77" />
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123137" name="jetbrains.mps.baseLanguage.structure.BooleanConstant" flags="nn" index="3clFbT">
        <property id="1068580123138" name="value" index="3clFbU" />
      </concept>
      <concept id="1068581517677" name="jetbrains.mps.baseLanguage.structure.VoidType" flags="in" index="3cqZAl" />
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
      </concept>
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
    </language>
    <language id="3a13115c-633c-4c5c-bbcc-75c4219e9555" name="jetbrains.mps.lang.quotation">
      <concept id="5455284157994012186" name="jetbrains.mps.lang.quotation.structure.NodeBuilderInitLink" flags="ng" index="2pIpSj">
        <reference id="5455284157994012188" name="link" index="2pIpSl" />
      </concept>
      <concept id="5455284157993911097" name="jetbrains.mps.lang.quotation.structure.NodeBuilderInitPart" flags="ng" index="2pJxcK">
        <child id="5455284157993911094" name="expression" index="2pJxcZ" />
      </concept>
      <concept id="5455284157993863837" name="jetbrains.mps.lang.quotation.structure.NodeBuilder" flags="nn" index="2pJPEk">
        <child id="5455284157993863838" name="quotedNode" index="2pJPEn" />
      </concept>
      <concept id="5455284157993863840" name="jetbrains.mps.lang.quotation.structure.NodeBuilderNode" flags="nn" index="2pJPED">
        <reference id="5455284157993910961" name="concept" index="2pJxaS" />
        <child id="5455284157993911099" name="values" index="2pJxcM" />
      </concept>
      <concept id="8182547171709752110" name="jetbrains.mps.lang.quotation.structure.NodeBuilderExpression" flags="nn" index="36biLy">
        <child id="8182547171709752112" name="expression" index="36biLW" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="2396822768958367367" name="jetbrains.mps.lang.smodel.structure.AbstractTypeCastExpression" flags="nn" index="$5XWr">
        <child id="6733348108486823193" name="leftExpression" index="1m5AlR" />
        <child id="3906496115198199033" name="conceptArgument" index="3oSUPX" />
      </concept>
      <concept id="2644386474300074836" name="jetbrains.mps.lang.smodel.structure.ConceptIdRefExpression" flags="nn" index="35c_gC">
        <reference id="2644386474300074837" name="conceptDeclaration" index="35c_gD" />
      </concept>
      <concept id="6677504323281689838" name="jetbrains.mps.lang.smodel.structure.SConceptType" flags="in" index="3bZ5Sz">
        <reference id="6677504323281689839" name="conceptDeclaraton" index="3bZ5Sy" />
      </concept>
      <concept id="1140131837776" name="jetbrains.mps.lang.smodel.structure.Node_ReplaceWithAnotherOperation" flags="nn" index="1P9Npp">
        <child id="1140131861877" name="replacementNode" index="1P9ThW" />
      </concept>
      <concept id="1140137987495" name="jetbrains.mps.lang.smodel.structure.SNodeTypeCastExpression" flags="nn" index="1PxgMI" />
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="13h7C7" id="4QUW3eduAib">
    <property role="3GE5qa" value="shared" />
    <ref role="13h7C2" to="ruww:4QUW3eduAhL" resolve="ITranslatableLaboratoryTestReferenceConcept" />
    <node concept="13i0hz" id="4QUW3eduAim" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayTranslations" />
      <property role="2Ki8OM" value="true" />
      <node concept="3Tm1VV" id="4QUW3eduAin" role="1B3o_S" />
      <node concept="3uibUv" id="4QUW3eduAU4" role="3clF45">
        <ref role="3uigEE" to="mmhz:4zMac8rUNtP" resolve="ILaboratoryTestReferenceTranslations" />
      </node>
      <node concept="3clFbS" id="4QUW3eduAip" role="3clF47">
        <node concept="3clFbF" id="4QUW3eduAV_" role="3cqZAp">
          <node concept="10M0yZ" id="4QUW3eduAWd" role="3clFbG">
            <ref role="3cqZAo" to="mmhz:4zMac8rVBeK" resolve="displayTranslations" />
            <ref role="1PxDUh" to="mmhz:4zMac8rUNsN" resolve="LaboratoryTestReferenceTranslationProvider" />
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="4QUW3eduAWM" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getGenerationTranslations" />
      <property role="2Ki8OM" value="true" />
      <node concept="3Tm1VV" id="4QUW3eduAWN" role="1B3o_S" />
      <node concept="3uibUv" id="4QUW3eduAWO" role="3clF45">
        <ref role="3uigEE" to="mmhz:4zMac8rUNtP" resolve="ILaboratoryTestReferenceTranslations" />
      </node>
      <node concept="3clFbS" id="4QUW3eduAWP" role="3clF47">
        <node concept="3clFbF" id="4QUW3eduAWQ" role="3cqZAp">
          <node concept="10M0yZ" id="4QUW3eduAZN" role="3clFbG">
            <ref role="3cqZAo" to="mmhz:1Hxyv4DUmfq" resolve="generationTranslations" />
            <ref role="1PxDUh" to="mmhz:4zMac8rUNsN" resolve="LaboratoryTestReferenceTranslationProvider" />
          </node>
        </node>
      </node>
    </node>
    <node concept="13hLZK" id="4QUW3eduAic" role="13h7CW">
      <node concept="3clFbS" id="4QUW3eduAid" role="2VODD2" />
    </node>
  </node>
  <node concept="13h7C7" id="4V3GMfXHyB9">
    <property role="3GE5qa" value="shared.references.laboratoryTest" />
    <ref role="13h7C2" to="ruww:6khVixyauIe" resolve="EntitySpecimenTypeReference" />
    <node concept="13i0hz" id="4V3GMfXHyBk" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getMissingTargetError" />
      <ref role="13i0hy" to="wb6c:4QUW3edDA0E" resolve="getMissingTargetError" />
      <node concept="3Tm1VV" id="4V3GMfXHyBl" role="1B3o_S" />
      <node concept="3clFbS" id="4V3GMfXHyBm" role="3clF47">
        <node concept="3clFbF" id="6LTgXmMLquQ" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmMLrkY" role="3clFbG">
            <property role="Xl_RC" value="target" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="4V3GMfXHyBr" role="3clF45" />
    </node>
    <node concept="13hLZK" id="4V3GMfXHyBa" role="13h7CW">
      <node concept="3clFbS" id="4V3GMfXHyBb" role="2VODD2" />
    </node>
  </node>
  <node concept="13h7C7" id="6khVixya$xO">
    <property role="3GE5qa" value="aspect.variables.specimenType" />
    <ref role="13h7C2" to="ruww:6khVixyavhY" resolve="SpecimenTypeVariable" />
    <node concept="13hLZK" id="6khVixya$xP" role="13h7CW">
      <node concept="3clFbS" id="6khVixya$xQ" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6LTgXmNJVpM" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="6LTgXmNJVpN" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNJVpS" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNJVpX" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNJVyV" role="3clFbG">
            <property role="Xl_RC" value="specimen type" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmNJVpT" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmNJVpY" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="6LTgXmNJVpZ" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNJVq4" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNJVq9" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNJVzC" role="3clFbG">
            <property role="Xl_RC" value="" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmNJVq5" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixya$xZ" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getReferenceDescription" />
      <property role="2Ki8OM" value="false" />
      <ref role="13i0hy" to="1yj:4V3GMfXuP$X" resolve="getReferenceDescription" />
      <node concept="3Tm1VV" id="6khVixya$y0" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixya$yb" role="3clF47">
        <node concept="3clFbF" id="6khVixya$yg" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixya$B4" role="3clFbG">
            <property role="Xl_RC" value="specimen type" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixya$yc" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="4B5aqq2xIF7">
    <property role="3GE5qa" value="aspect.variables.laboratoryTest" />
    <ref role="13h7C2" to="ruww:4QUW3edDVyK" resolve="LaboratoryTestVariable" />
    <node concept="13hLZK" id="4B5aqq2xIF8" role="13h7CW">
      <node concept="3clFbS" id="4B5aqq2xIF9" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="4B5aqq2xIFi" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="4B5aqq2xIFj" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq2xIFo" role="3clF47">
        <node concept="3clFbF" id="4B5aqq2xIJr" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq2xIJq" role="3clFbG">
            <property role="Xl_RC" value="laboratory test" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="4B5aqq2xIFp" role="3clF45" />
    </node>
    <node concept="13i0hz" id="4B5aqq2xIFu" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="4B5aqq2xIFv" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq2xIF$" role="3clF47">
        <node concept="3clFbF" id="4B5aqq2xIFD" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq2xIKl" role="3clFbG">
            <property role="Xl_RC" value="" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="4B5aqq2xIF_" role="3clF45" />
    </node>
    <node concept="13i0hz" id="4B5aqq2_K4A" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getReferenceDescription" />
      <property role="2Ki8OM" value="false" />
      <ref role="13i0hy" to="1yj:4V3GMfXuP$X" resolve="getReferenceDescription" />
      <node concept="3Tm1VV" id="4B5aqq2_K4B" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq2_K4M" role="3clF47">
        <node concept="3clFbF" id="4B5aqq2_K9O" role="3cqZAp">
          <node concept="Xl_RD" id="4B5aqq2_K9N" role="3clFbG">
            <property role="Xl_RC" value="laboratory test" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="4B5aqq2_K4N" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="6LTgXmMQEdQ">
    <property role="3GE5qa" value="aspect.patterns.laboratoryTest" />
    <ref role="13h7C2" to="ruww:6LTgXmMPUpt" resolve="WildcardLaboratoryTestPattern" />
    <node concept="13i0hz" id="6LTgXmNiD3C" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="6LTgXmNiD3D" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNiD3I" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNiD$U" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNiD$T" role="3clFbG">
            <property role="Xl_RC" value="any laboratory test" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmNiD3J" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmNUOG2" role="13h7CS">
      <property role="TrG5h" value="hasPattern" />
      <property role="13i0it" value="false" />
      <property role="13i0iv" value="false" />
      <ref role="13i0hy" to="wb6c:6LTgXmNRv0n" resolve="hasPattern" />
      <node concept="3Tm1VV" id="6LTgXmNUOG3" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNUOG8" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNUONj" role="3cqZAp">
          <node concept="3clFbT" id="6LTgXmNUONi" role="3clFbG">
            <property role="3clFbU" value="true" />
          </node>
        </node>
      </node>
      <node concept="10P_77" id="6LTgXmNUOG9" role="3clF45" />
    </node>
    <node concept="13i0hz" id="65epL7Mpo12" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getMatchEntityConcept" />
      <ref role="13i0hy" to="wb6c:65epL7MnLJr" resolve="getMatchEntityConcept" />
      <node concept="3Tm1VV" id="65epL7Mpo13" role="1B3o_S" />
      <node concept="3clFbS" id="65epL7Mpo18" role="3clF47">
        <node concept="3clFbF" id="65epL7Mpo67" role="3cqZAp">
          <node concept="35c_gC" id="65epL7Mpo66" role="3clFbG">
            <ref role="35c_gD" to="xvtf:1mAGFBLav_o" resolve="LaboratoryTest" />
          </node>
        </node>
      </node>
      <node concept="3bZ5Sz" id="65epL7Mpo19" role="3clF45">
        <ref role="3bZ5Sy" to="kkto:4QUW3edDqMR" resolve="Entity" />
      </node>
    </node>
    <node concept="13hLZK" id="6LTgXmMQEdR" role="13h7CW">
      <node concept="3clFbS" id="6LTgXmMQEdS" role="2VODD2" />
    </node>
  </node>
  <node concept="13h7C7" id="6LTgXmN8wmb">
    <property role="3GE5qa" value="shared.references.laboratoryTest" />
    <ref role="13h7C2" to="ruww:4QUW3efwB30" resolve="LaboratoryTestReference" />
    <node concept="13hLZK" id="6LTgXmN8wmc" role="13h7CW">
      <node concept="3clFbS" id="6LTgXmN8wmd" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6LTgXmN8wmm" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getMissingTargetError" />
      <ref role="13i0hy" to="wb6c:4QUW3edDA0E" resolve="getMissingTargetError" />
      <node concept="3Tm1VV" id="6LTgXmN8wmn" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmN8wmu" role="3clF47">
        <node concept="3clFbF" id="6LTgXmN8wuX" role="3cqZAp">
          <node concept="2OqwBi" id="6LTgXmN8wA5" role="3clFbG">
            <node concept="BsUDl" id="6LTgXmN8wuW" role="2Oq$k0">
              <ref role="37wK5l" node="4QUW3eduAim" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6LTgXmN8wG5" role="2OqNvi">
              <ref role="37wK5l" to="mmhz:4QUW3efWXMP" resolve="getLaboratoryTestReferenceMissingTestError" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmN8wmv" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="6LTgXmNfDLa">
    <property role="3GE5qa" value="shared.references.laboratoryTest" />
    <ref role="13h7C2" to="ruww:4V3GMfXHy69" resolve="LaboratoryTestWithNumberResultReference" />
    <node concept="13hLZK" id="6LTgXmNfDLb" role="13h7CW">
      <node concept="3clFbS" id="6LTgXmNfDLc" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6LTgXmNfDLw" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getMissingTargetError" />
      <ref role="13i0hy" to="wb6c:4QUW3edDA0E" resolve="getMissingTargetError" />
      <node concept="3Tm1VV" id="6LTgXmNfDLx" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNfDLC" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNfDPZ" role="3cqZAp">
          <node concept="2OqwBi" id="6LTgXmNfDWZ" role="3clFbG">
            <node concept="BsUDl" id="6LTgXmNfDPY" role="2Oq$k0">
              <ref role="37wK5l" node="4QUW3eduAim" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6LTgXmNfE2Z" role="2OqNvi">
              <ref role="37wK5l" to="mmhz:4QUW3efWXMP" resolve="getLaboratoryTestReferenceMissingTestError" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmNfDLD" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="6LTgXmNfE8n">
    <property role="3GE5qa" value="shared.references.laboratoryTest" />
    <ref role="13h7C2" to="ruww:4V3GMfXHyx1" resolve="LaboratoryTestWithTextResultReference" />
    <node concept="13hLZK" id="6LTgXmNfE8o" role="13h7CW">
      <node concept="3clFbS" id="6LTgXmNfE8p" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6LTgXmNfE8y" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getMissingTargetError" />
      <ref role="13i0hy" to="wb6c:4QUW3edDA0E" resolve="getMissingTargetError" />
      <node concept="3Tm1VV" id="6LTgXmNfE8z" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNfE8E" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNfEcS" role="3cqZAp">
          <node concept="2OqwBi" id="6LTgXmNfEcU" role="3clFbG">
            <node concept="BsUDl" id="6LTgXmNfEcV" role="2Oq$k0">
              <ref role="37wK5l" node="4QUW3eduAim" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6LTgXmNfEcW" role="2OqNvi">
              <ref role="37wK5l" to="mmhz:4QUW3efWXMP" resolve="getLaboratoryTestReferenceMissingTestError" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmNfE8F" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="6LTgXmNfJqJ">
    <property role="3GE5qa" value="shared.references.specimenType" />
    <ref role="13h7C2" to="ruww:6khVixyauId" resolve="SpecimenTypeReference" />
    <node concept="13hLZK" id="6LTgXmNfJqK" role="13h7CW">
      <node concept="3clFbS" id="6LTgXmNfJqL" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6LTgXmNfJqU" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getMissingTargetError" />
      <ref role="13i0hy" to="wb6c:4QUW3edDA0E" resolve="getMissingTargetError" />
      <node concept="3Tm1VV" id="6LTgXmNfJqV" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNfJr2" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNfJwa" role="3cqZAp">
          <node concept="2OqwBi" id="6LTgXmNfJB8" role="3clFbG">
            <node concept="BsUDl" id="6LTgXmNfJw9" role="2Oq$k0">
              <ref role="37wK5l" node="4QUW3eduAim" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6LTgXmNfJH2" role="2OqNvi">
              <ref role="37wK5l" to="mmhz:6LTgXmNfILC" resolve="getSpecimentTypeReferenceMissingTestError" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmNfJr3" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="6LTgXmNQzie">
    <property role="3GE5qa" value="aspect.patterns.specimenType" />
    <ref role="13h7C2" to="ruww:6LTgXmNQnWm" resolve="WildcardSpecimenTypePattern" />
    <node concept="13hLZK" id="6LTgXmNQzif" role="13h7CW">
      <node concept="3clFbS" id="6LTgXmNQzig" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6LTgXmNQzip" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="6LTgXmNQziq" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNQziv" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNQzn8" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNQzn7" role="3clFbG">
            <property role="Xl_RC" value="*" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmNQziw" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmNQzi_" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="6LTgXmNQziA" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNQziF" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNQziK" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNQznP" role="3clFbG">
            <property role="Xl_RC" value="any specimen type" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmNQziG" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmNUUgI" role="13h7CS">
      <property role="TrG5h" value="hasPattern" />
      <property role="13i0it" value="false" />
      <property role="13i0iv" value="false" />
      <ref role="13i0hy" to="wb6c:6LTgXmNRv0n" resolve="hasPattern" />
      <node concept="3Tm1VV" id="6LTgXmNUUgJ" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNUUgO" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNUUmV" role="3cqZAp">
          <node concept="3clFbT" id="6LTgXmNUUmU" role="3clFbG">
            <property role="3clFbU" value="true" />
          </node>
        </node>
      </node>
      <node concept="10P_77" id="6LTgXmNUUgP" role="3clF45" />
    </node>
    <node concept="13i0hz" id="65epL7MpeT6" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getMatchEntityConcept" />
      <ref role="13i0hy" to="wb6c:65epL7MnLJr" resolve="getMatchEntityConcept" />
      <node concept="3Tm1VV" id="65epL7MpeT7" role="1B3o_S" />
      <node concept="3clFbS" id="65epL7MpeTc" role="3clF47">
        <node concept="3clFbF" id="65epL7Mpf8d" role="3cqZAp">
          <node concept="35c_gC" id="65epL7Mpf8c" role="3clFbG">
            <ref role="35c_gD" to="xvtf:6khVixya4DS" resolve="SpecimenType" />
          </node>
        </node>
      </node>
      <node concept="3bZ5Sz" id="65epL7MpeTd" role="3clF45">
        <ref role="3bZ5Sy" to="kkto:4QUW3edDqMR" resolve="Entity" />
      </node>
    </node>
  </node>
  <node concept="13h7C7" id="6LTgXmNXzbE">
    <property role="3GE5qa" value="aspect.patterns.laboratoryTest" />
    <ref role="13h7C2" to="ruww:6LTgXmNXs4N" resolve="SelectedLaboratoryTestPattern" />
    <node concept="13hLZK" id="6LTgXmNXzbF" role="13h7CW">
      <node concept="3clFbS" id="6LTgXmNXzbG" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6LTgXmNXzc1" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="6LTgXmNXzc2" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNXzc7" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNXzcc" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNXztC" role="3clFbG">
            <property role="Xl_RC" value="selected laboratory tests" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmNXzc8" role="3clF45" />
    </node>
    <node concept="13i0hz" id="65epL7MuoeG" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getMissingEntities" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="wb6c:65epL7MsXaV" resolve="getMissingEntitiesError" />
      <node concept="3Tm1VV" id="65epL7MuoeH" role="1B3o_S" />
      <node concept="3clFbS" id="65epL7MuoeM" role="3clF47">
        <node concept="3clFbF" id="65epL7Muomw" role="3cqZAp">
          <node concept="Xl_RD" id="65epL7Muomv" role="3clFbG">
            <property role="Xl_RC" value="missing laboratory test" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="65epL7MuoeN" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmNXzcd" role="13h7CS">
      <property role="TrG5h" value="hasPattern" />
      <property role="13i0it" value="false" />
      <property role="13i0iv" value="false" />
      <ref role="13i0hy" to="wb6c:6LTgXmNRv0n" resolve="hasPattern" />
      <node concept="3Tm1VV" id="6LTgXmNXzce" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNXzcj" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNXzxT" role="3cqZAp">
          <node concept="3clFbT" id="6LTgXmNXzxS" role="3clFbG">
            <property role="3clFbU" value="true" />
          </node>
        </node>
      </node>
      <node concept="10P_77" id="6LTgXmNXzck" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="1I84Bf8kIP4">
    <property role="3GE5qa" value="aspect.references.laboratoryTest" />
    <ref role="13h7C2" to="ruww:4QUW3efwB3a" resolve="AspectLaboratoryTestReference" />
    <node concept="13hLZK" id="1I84Bf8kIP5" role="13h7CW">
      <node concept="3clFbS" id="1I84Bf8kIP6" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="1I84Bf8kIPf" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="replaceWith" />
      <ref role="13i0hy" to="wb6c:1I84Bf8ilTw" resolve="replaceWith" />
      <node concept="3Tm1VV" id="1I84Bf8kIPg" role="1B3o_S" />
      <node concept="3clFbS" id="1I84Bf8kIPq" role="3clF47">
        <node concept="3clFbF" id="1I84Bf8kIV0" role="3cqZAp">
          <node concept="2OqwBi" id="1I84Bf8kJ8i" role="3clFbG">
            <node concept="13iPFW" id="1I84Bf8kIUZ" role="2Oq$k0" />
            <node concept="1P9Npp" id="1I84Bf8kJtC" role="2OqNvi">
              <node concept="2pJPEk" id="1I84Bf8kJvR" role="1P9ThW">
                <node concept="2pJPED" id="1I84Bf8kJy6" role="2pJPEn">
                  <ref role="2pJxaS" to="ruww:1I84Bf8kDsK" resolve="EntityLaboratoryTestReference" />
                  <node concept="2pIpSj" id="1I84Bf8kJza" role="2pJxcM">
                    <ref role="2pIpSl" to="ruww:1I84Bf8kDsL" resolve="target" />
                    <node concept="36biLy" id="1I84Bf8kJB7" role="2pJxcZ">
                      <node concept="1PxgMI" id="1I84Bf8kJSB" role="36biLW">
                        <node concept="chp4Y" id="1I84Bf8kJTa" role="3oSUPX">
                          <ref role="cht4Q" to="xvtf:1mAGFBLav_o" resolve="LaboratoryTest" />
                        </node>
                        <node concept="37vLTw" id="1I84Bf8kJEf" role="1m5AlR">
                          <ref role="3cqZAo" node="1I84Bf8kIPr" resolve="node" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="1I84Bf8kIPr" role="3clF46">
        <property role="TrG5h" value="node" />
        <node concept="3Tqbb2" id="1I84Bf8kIPs" role="1tU5fm" />
      </node>
      <node concept="3cqZAl" id="1I84Bf8kIPt" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="1I84Bf8kPS7">
    <property role="3GE5qa" value="aspect.references.laboratoryTest" />
    <ref role="13h7C2" to="ruww:4B5aqq2QgEr" resolve="AspectLaboratoryTestWithNumberResultReference" />
    <node concept="13hLZK" id="1I84Bf8kPS8" role="13h7CW">
      <node concept="3clFbS" id="1I84Bf8kPS9" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="1I84Bf8kPSi" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="replaceWith" />
      <ref role="13i0hy" to="wb6c:1I84Bf8ilTw" resolve="replaceWith" />
      <node concept="3Tm1VV" id="1I84Bf8kPSj" role="1B3o_S" />
      <node concept="3clFbS" id="1I84Bf8kPSt" role="3clF47">
        <node concept="3clFbF" id="1I84Bf8kPY3" role="3cqZAp">
          <node concept="2OqwBi" id="1I84Bf8kQbl" role="3clFbG">
            <node concept="13iPFW" id="1I84Bf8kPY2" role="2Oq$k0" />
            <node concept="1P9Npp" id="1I84Bf8kQwF" role="2OqNvi">
              <node concept="2pJPEk" id="1I84Bf8kQyU" role="1P9ThW">
                <node concept="2pJPED" id="1I84Bf8kQ_9" role="2pJPEn">
                  <ref role="2pJxaS" to="ruww:1I84Bf8kDtd" resolve="EntityLaboratoryTestWithNumberResultReference" />
                  <node concept="2pIpSj" id="1I84Bf8kQAd" role="2pJxcM">
                    <ref role="2pIpSl" to="ruww:1I84Bf8kDte" resolve="target" />
                    <node concept="36biLy" id="1I84Bf8kQBl" role="2pJxcZ">
                      <node concept="1PxgMI" id="1I84Bf8kQM_" role="36biLW">
                        <node concept="chp4Y" id="1I84Bf8kQN8" role="3oSUPX">
                          <ref role="cht4Q" to="xvtf:1mAGFBLav_o" resolve="LaboratoryTest" />
                        </node>
                        <node concept="37vLTw" id="1I84Bf8kQEt" role="1m5AlR">
                          <ref role="3cqZAo" node="1I84Bf8kPSu" resolve="node" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="1I84Bf8kPSu" role="3clF46">
        <property role="TrG5h" value="node" />
        <node concept="3Tqbb2" id="1I84Bf8kPSv" role="1tU5fm" />
      </node>
      <node concept="3cqZAl" id="1I84Bf8kPSw" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="1I84Bf8kUbD">
    <property role="3GE5qa" value="aspect.references.laboratoryTest" />
    <ref role="13h7C2" to="ruww:4B5aqq2QgEt" resolve="AspectLaboratoryTestWithTextResultReference" />
    <node concept="13hLZK" id="1I84Bf8kUbE" role="13h7CW">
      <node concept="3clFbS" id="1I84Bf8kUbF" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="1I84Bf8kUbO" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="replaceWith" />
      <ref role="13i0hy" to="wb6c:1I84Bf8ilTw" resolve="replaceWith" />
      <node concept="3Tm1VV" id="1I84Bf8kUbP" role="1B3o_S" />
      <node concept="3clFbS" id="1I84Bf8kUbZ" role="3clF47">
        <node concept="3clFbF" id="1I84Bf8kUh_" role="3cqZAp">
          <node concept="2OqwBi" id="1I84Bf8kU$B" role="3clFbG">
            <node concept="13iPFW" id="1I84Bf8kUh$" role="2Oq$k0" />
            <node concept="1P9Npp" id="1I84Bf8kUTX" role="2OqNvi">
              <node concept="2pJPEk" id="1I84Bf8kUWc" role="1P9ThW">
                <node concept="2pJPED" id="1I84Bf8kVck" role="2pJPEn">
                  <ref role="2pJxaS" to="ruww:1I84Bf8kDtg" resolve="EntityLaboratoryTestWithTextResultReference" />
                  <node concept="2pIpSj" id="1I84Bf8l3o2" role="2pJxcM">
                    <ref role="2pIpSl" to="ruww:1I84Bf8kDth" resolve="target" />
                    <node concept="36biLy" id="1I84Bf8l3rZ" role="2pJxcZ">
                      <node concept="1PxgMI" id="1I84Bf8l3Bf" role="36biLW">
                        <node concept="chp4Y" id="1I84Bf8l3BM" role="3oSUPX">
                          <ref role="cht4Q" to="xvtf:1mAGFBLav_o" resolve="LaboratoryTest" />
                        </node>
                        <node concept="37vLTw" id="1I84Bf8l3v7" role="1m5AlR">
                          <ref role="3cqZAo" node="1I84Bf8kUc0" resolve="node" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="1I84Bf8kUc0" role="3clF46">
        <property role="TrG5h" value="node" />
        <node concept="3Tqbb2" id="1I84Bf8kUc1" role="1tU5fm" />
      </node>
      <node concept="3cqZAl" id="1I84Bf8kUc2" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="1I84Bf8leQo">
    <property role="3GE5qa" value="aspect.references.specimenType" />
    <ref role="13h7C2" to="ruww:6khVixyavhZ" resolve="AspectSpecimenTypeReference" />
    <node concept="13hLZK" id="1I84Bf8leQp" role="13h7CW">
      <node concept="3clFbS" id="1I84Bf8leQq" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="1I84Bf8leQz" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="replaceWith" />
      <ref role="13i0hy" to="wb6c:1I84Bf8ilTw" resolve="replaceWith" />
      <node concept="3Tm1VV" id="1I84Bf8leQ$" role="1B3o_S" />
      <node concept="3clFbS" id="1I84Bf8leQI" role="3clF47">
        <node concept="3clFbF" id="1I84Bf8leWj" role="3cqZAp">
          <node concept="2OqwBi" id="1I84Bf8lfco" role="3clFbG">
            <node concept="13iPFW" id="1I84Bf8leWd" role="2Oq$k0" />
            <node concept="1P9Npp" id="1I84Bf8lfxI" role="2OqNvi">
              <node concept="2pJPEk" id="1I84Bf8lfzX" role="1P9ThW">
                <node concept="2pJPED" id="1I84Bf8lfAc" role="2pJPEn">
                  <ref role="2pJxaS" to="ruww:6khVixyauIe" resolve="EntitySpecimenTypeReference" />
                  <node concept="2pIpSj" id="1I84Bf8lfBg" role="2pJxcM">
                    <ref role="2pIpSl" to="ruww:6khVixyauIf" resolve="target" />
                    <node concept="36biLy" id="1I84Bf8lfFd" role="2pJxcZ">
                      <node concept="1PxgMI" id="1I84Bf8lfQt" role="36biLW">
                        <node concept="chp4Y" id="1I84Bf8lfR0" role="3oSUPX">
                          <ref role="cht4Q" to="xvtf:6khVixya4DS" resolve="SpecimenType" />
                        </node>
                        <node concept="37vLTw" id="1I84Bf8lfIl" role="1m5AlR">
                          <ref role="3cqZAo" node="1I84Bf8leQJ" resolve="node" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="1I84Bf8leQJ" role="3clF46">
        <property role="TrG5h" value="node" />
        <node concept="3Tqbb2" id="1I84Bf8leQK" role="1tU5fm" />
      </node>
      <node concept="3cqZAl" id="1I84Bf8leQL" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="6LTgXmNFWkX">
    <property role="3GE5qa" value="aspect.variables.laboratoryTest" />
    <ref role="13h7C2" to="ruww:4B5aqq2QmL6" resolve="LaboratoryTestWithTextResultVariable" />
    <node concept="13i0hz" id="6LTgXmNFWl8" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="6LTgXmNFWl9" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNFWla" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNFWlb" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNFWlc" role="3clFbG">
            <property role="Xl_RC" value="laboratory test with text result" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmNFWld" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmNFWle" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="6LTgXmNFWlf" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNFWlg" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNFWlh" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNFWli" role="3clFbG">
            <property role="Xl_RC" value="" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmNFWlj" role="3clF45" />
    </node>
    <node concept="13hLZK" id="6LTgXmNFWkY" role="13h7CW">
      <node concept="3clFbS" id="6LTgXmNFWkZ" role="2VODD2" />
    </node>
  </node>
  <node concept="13h7C7" id="6LTgXmN$n3p">
    <property role="3GE5qa" value="aspect.variables.laboratoryTest" />
    <ref role="13h7C2" to="ruww:4B5aqq2QmL5" resolve="LaboratoryTestWithNumberResultVariable" />
    <node concept="13hLZK" id="6LTgXmN$n3q" role="13h7CW">
      <node concept="3clFbS" id="6LTgXmN$n3r" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6LTgXmN$n3$" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="6LTgXmN$n3_" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmN$n3E" role="3clF47">
        <node concept="3clFbF" id="6LTgXmN$n3J" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmN$n94" role="3clFbG">
            <property role="Xl_RC" value="laboratory test with number result" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmN$n3F" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmN$n3K" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="6LTgXmN$n3L" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmN$n3Q" role="3clF47">
        <node concept="3clFbF" id="6LTgXmN$n3V" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmN$n8L" role="3clFbG">
            <property role="Xl_RC" value="" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmN$n3R" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="65epL7Muqdu">
    <property role="3GE5qa" value="aspect.patterns.laboratoryTest.withNumberResult" />
    <ref role="13h7C2" to="ruww:65epL7MkNmL" resolve="SelectedLaboratoryTestWithNumberPattern" />
    <node concept="13hLZK" id="65epL7Muqdv" role="13h7CW">
      <node concept="3clFbS" id="65epL7Muqdw" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="65epL7M_4$F" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="65epL7M_4$G" role="1B3o_S" />
      <node concept="3clFbS" id="65epL7M_4$H" role="3clF47">
        <node concept="3clFbF" id="65epL7M_4$I" role="3cqZAp">
          <node concept="Xl_RD" id="65epL7M_4$J" role="3clFbG">
            <property role="Xl_RC" value="selected laboratory tests" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="65epL7M_4$K" role="3clF45" />
    </node>
    <node concept="13i0hz" id="65epL7MuqdP" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getMissingEntities" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="wb6c:65epL7MsXaV" resolve="getMissingEntitiesError" />
      <node concept="3Tm1VV" id="65epL7MuqdQ" role="1B3o_S" />
      <node concept="3clFbS" id="65epL7MuqdV" role="3clF47">
        <node concept="3clFbF" id="65epL7M$B_j" role="3cqZAp">
          <node concept="Xl_RD" id="65epL7M$B_i" role="3clFbG">
            <property role="Xl_RC" value="missing laboratory test" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="65epL7MuqdW" role="3clF45" />
    </node>
    <node concept="13i0hz" id="65epL7MuqdD" role="13h7CS">
      <property role="TrG5h" value="hasPattern" />
      <property role="13i0it" value="false" />
      <property role="13i0iv" value="false" />
      <ref role="13i0hy" to="wb6c:6LTgXmNRv0n" resolve="hasPattern" />
      <node concept="3Tm1VV" id="65epL7MuqdE" role="1B3o_S" />
      <node concept="3clFbS" id="65epL7MuqdJ" role="3clF47">
        <node concept="3clFbF" id="65epL7MuqpV" role="3cqZAp">
          <node concept="3clFbT" id="65epL7MuqpU" role="3clFbG">
            <property role="3clFbU" value="true" />
          </node>
        </node>
      </node>
      <node concept="10P_77" id="65epL7MuqdK" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="65epL7MAJ$f">
    <property role="3GE5qa" value="aspect.patterns.laboratoryTest.withNumberResult" />
    <ref role="13h7C2" to="ruww:65epL7MkNmN" resolve="WildcardLaboratoryTestWithNumberPattern" />
    <node concept="13hLZK" id="65epL7MAJ$g" role="13h7CW">
      <node concept="3clFbS" id="65epL7MAJ$h" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="65epL7MAJ$A" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="65epL7MAJ$B" role="1B3o_S" />
      <node concept="3clFbS" id="65epL7MAJ$G" role="3clF47">
        <node concept="3clFbF" id="65epL7MAJ$L" role="3cqZAp">
          <node concept="Xl_RD" id="65epL7MAKIH" role="3clFbG">
            <property role="Xl_RC" value="any laboratory test" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="65epL7MAJ$H" role="3clF45" />
    </node>
    <node concept="13i0hz" id="65epL7MAKAK" role="13h7CS">
      <property role="TrG5h" value="hasPattern" />
      <property role="13i0it" value="false" />
      <property role="13i0iv" value="false" />
      <ref role="13i0hy" to="wb6c:6LTgXmNRv0n" resolve="hasPattern" />
      <node concept="3Tm1VV" id="65epL7MAKAL" role="1B3o_S" />
      <node concept="3clFbS" id="65epL7MAKAQ" role="3clF47">
        <node concept="3clFbF" id="65epL7MAKIg" role="3cqZAp">
          <node concept="3clFbT" id="65epL7MAKIf" role="3clFbG">
            <property role="3clFbU" value="true" />
          </node>
        </node>
      </node>
      <node concept="10P_77" id="65epL7MAKAR" role="3clF45" />
    </node>
    <node concept="13i0hz" id="65epL7MAJUv" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getMatchEntityConcept" />
      <ref role="13i0hy" to="wb6c:65epL7MnLJr" resolve="getMatchEntityConcept" />
      <node concept="3Tm1VV" id="65epL7MAJUw" role="1B3o_S" />
      <node concept="3clFbS" id="65epL7MAJU_" role="3clF47">
        <node concept="3clFbF" id="65epL7MAJUE" role="3cqZAp">
          <node concept="35c_gC" id="65epL7MAKJ$" role="3clFbG">
            <ref role="35c_gD" to="xvtf:1mAGFBLav_o" resolve="LaboratoryTest" />
          </node>
        </node>
      </node>
      <node concept="3bZ5Sz" id="65epL7MAJUA" role="3clF45">
        <ref role="3bZ5Sy" to="kkto:4QUW3edDqMR" resolve="Entity" />
      </node>
    </node>
  </node>
  <node concept="13h7C7" id="65epL7MAXLU">
    <property role="3GE5qa" value="aspect.patterns.laboratoryTest.withTextResult" />
    <ref role="13h7C2" to="ruww:65epL7MjP7y" resolve="SelectedLaboratoryTestWithTextPattern" />
    <node concept="13hLZK" id="65epL7MAXLV" role="13h7CW">
      <node concept="3clFbS" id="65epL7MAXLW" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="65epL7MAXMt" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="65epL7MAXMu" role="1B3o_S" />
      <node concept="3clFbS" id="65epL7MAXMz" role="3clF47">
        <node concept="3clFbF" id="65epL7MAXMC" role="3cqZAp">
          <node concept="Xl_RD" id="65epL7MAYmE" role="3clFbG">
            <property role="Xl_RC" value="selected laboratory tests" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="65epL7MAXM$" role="3clF45" />
    </node>
    <node concept="13i0hz" id="65epL7MAXM5" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getMissingEntitiesError" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="wb6c:65epL7MsXaV" resolve="getMissingEntitiesError" />
      <node concept="3Tm1VV" id="65epL7MAXM6" role="1B3o_S" />
      <node concept="3clFbS" id="65epL7MAXMb" role="3clF47">
        <node concept="3clFbF" id="65epL7MAXMg" role="3cqZAp">
          <node concept="Xl_RD" id="65epL7MAYoj" role="3clFbG">
            <property role="Xl_RC" value="missing laboratory test" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="65epL7MAXMc" role="3clF45" />
    </node>
    <node concept="13i0hz" id="65epL7MAXMh" role="13h7CS">
      <property role="TrG5h" value="hasPattern" />
      <property role="13i0it" value="false" />
      <property role="13i0iv" value="false" />
      <ref role="13i0hy" to="wb6c:6LTgXmNRv0n" resolve="hasPattern" />
      <node concept="3Tm1VV" id="65epL7MAXMi" role="1B3o_S" />
      <node concept="3clFbS" id="65epL7MAXMn" role="3clF47">
        <node concept="3clFbF" id="65epL7MAXMs" role="3cqZAp">
          <node concept="3clFbT" id="65epL7MAYpl" role="3clFbG">
            <property role="3clFbU" value="true" />
          </node>
        </node>
      </node>
      <node concept="10P_77" id="65epL7MAXMo" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="65epL7ME3cg">
    <property role="3GE5qa" value="aspect.patterns.laboratoryTest.withTextResult" />
    <ref role="13h7C2" to="ruww:65epL7MjP7$" resolve="WildcardLaboratoryTestWithTextPattern" />
    <node concept="13hLZK" id="65epL7ME3ch" role="13h7CW">
      <node concept="3clFbS" id="65epL7ME3ci" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="65epL7ME3cB" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="65epL7ME3cC" role="1B3o_S" />
      <node concept="3clFbS" id="65epL7ME3cH" role="3clF47">
        <node concept="3clFbF" id="65epL7ME3cM" role="3cqZAp">
          <node concept="Xl_RD" id="65epL7ME3z4" role="3clFbG">
            <property role="Xl_RC" value="any laboratory test" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="65epL7ME3cI" role="3clF45" />
    </node>
    <node concept="13i0hz" id="65epL7ME3cN" role="13h7CS">
      <property role="TrG5h" value="hasPattern" />
      <property role="13i0it" value="false" />
      <property role="13i0iv" value="false" />
      <ref role="13i0hy" to="wb6c:6LTgXmNRv0n" resolve="hasPattern" />
      <node concept="3Tm1VV" id="65epL7ME3cO" role="1B3o_S" />
      <node concept="3clFbS" id="65epL7ME3cT" role="3clF47">
        <node concept="3clFbF" id="65epL7ME3cY" role="3cqZAp">
          <node concept="3clFbT" id="65epL7ME3zI" role="3clFbG">
            <property role="3clFbU" value="true" />
          </node>
        </node>
      </node>
      <node concept="10P_77" id="65epL7ME3cU" role="3clF45" />
    </node>
    <node concept="13i0hz" id="65epL7ME3cr" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getMatchEntityConcept" />
      <ref role="13i0hy" to="wb6c:65epL7MnLJr" resolve="getMatchEntityConcept" />
      <node concept="3Tm1VV" id="65epL7ME3cs" role="1B3o_S" />
      <node concept="3clFbS" id="65epL7ME3cx" role="3clF47">
        <node concept="3clFbF" id="65epL7ME3zY" role="3cqZAp">
          <node concept="35c_gC" id="65epL7ME3zX" role="3clFbG">
            <ref role="35c_gD" to="xvtf:1mAGFBLav_o" resolve="LaboratoryTest" />
          </node>
        </node>
      </node>
      <node concept="3bZ5Sz" id="65epL7ME3cy" role="3clF45">
        <ref role="3bZ5Sy" to="kkto:4QUW3edDqMR" resolve="Entity" />
      </node>
    </node>
  </node>
</model>

