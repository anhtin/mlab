<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:865d2abf-67d0-483c-9091-77e78d1ba1d0(no.uio.mLab.decisions.references.laboratoryTest.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="11" />
    <use id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions" version="4" />
    <use id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage" version="6" />
    <use id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel" version="11" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="aia3" ref="r:856e974b-6e42-49cc-99cd-c3adb4161c21(no.uio.mLab.decisions.data.laboratoryTest.structure)" />
    <import index="1yj" ref="r:0c887d67-81ab-420f-9a35-a9f6bf96c975(no.uio.mLab.shared.behavior)" />
    <import index="uubs" ref="r:481ae0fe-9100-4bb8-9488-731d6cfeba53(no.uio.mLab.shared.editor)" />
    <import index="veyb" ref="r:f97bcc9c-211d-441a-840a-958b18315a7e(no.uio.mLab.decisions.data.laboratoryTest.textualResult.structure)" />
    <import index="ydq9" ref="r:1329ca29-1e66-45a8-882e-5bcc59a0ad4c(no.uio.mLab.decisions.data.laboratoryTest.numberResult.structure)" />
    <import index="i7z6" ref="r:515ff38d-92f9-43f3-aa4c-c5bec29efe22(no.uio.mLab.decisions.core.editor)" />
    <import index="ruww" ref="r:d7787915-5b9a-4713-a6ab-6345c5e1ec03(no.uio.mLab.decisions.references.laboratoryTest.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="2000375450116454183" name="jetbrains.mps.lang.editor.structure.ISubstituteMenu" flags="ng" index="22mbnS">
        <child id="414384289274416996" name="parts" index="3ft7WO" />
      </concept>
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi" />
      <concept id="1140524381322" name="jetbrains.mps.lang.editor.structure.CellModel_ListWithRole" flags="ng" index="2czfm3">
        <property id="1140524450557" name="separatorText" index="2czwfO" />
        <child id="1140524464360" name="cellLayout" index="2czzBx" />
      </concept>
      <concept id="1106270549637" name="jetbrains.mps.lang.editor.structure.CellLayout_Horizontal" flags="nn" index="2iRfu4" />
      <concept id="1177327570013" name="jetbrains.mps.lang.editor.structure.QueryFunction_SubstituteMenu_Substitute" flags="in" index="ucgPf" />
      <concept id="8478191136883534237" name="jetbrains.mps.lang.editor.structure.IExtensibleSubstituteMenuPart" flags="ng" index="upBLQ">
        <child id="8478191136883534238" name="features" index="upBLP" />
      </concept>
      <concept id="1177335944525" name="jetbrains.mps.lang.editor.structure.QueryFunction_SubstituteMenu_SubstituteString" flags="in" index="uGdhv" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="6718020819487620876" name="jetbrains.mps.lang.editor.structure.TransformationMenuReference_Default" flags="ng" index="A1WHr" />
      <concept id="1078938745671" name="jetbrains.mps.lang.editor.structure.EditorComponentDeclaration" flags="ig" index="PKFIW">
        <child id="7033942394258392116" name="overridenEditorComponent" index="1PM95z" />
      </concept>
      <concept id="615427434521884870" name="jetbrains.mps.lang.editor.structure.SubstituteMenuPart_Subconcepts" flags="ng" index="2VfDsV" />
      <concept id="1186414536763" name="jetbrains.mps.lang.editor.structure.BooleanStyleSheetItem" flags="ln" index="VOi$J">
        <property id="1186414551515" name="flag" index="VOm3f" />
      </concept>
      <concept id="1186414860679" name="jetbrains.mps.lang.editor.structure.EditableStyleClassItem" flags="ln" index="VPxyj" />
      <concept id="1630016958697718209" name="jetbrains.mps.lang.editor.structure.IMenuReference_Default" flags="ng" index="2Z_bC8">
        <reference id="1630016958698373342" name="concept" index="2ZyFGn" />
      </concept>
      <concept id="1630016958697344083" name="jetbrains.mps.lang.editor.structure.IMenu_Concept" flags="ng" index="2ZABuq">
        <reference id="6591946374543067572" name="conceptDeclaration" index="aqKnT" />
      </concept>
      <concept id="8998492695583125082" name="jetbrains.mps.lang.editor.structure.SubstituteFeature_MatchingText" flags="ng" index="16NfWO">
        <child id="8998492695583129244" name="query" index="16NeZM" />
      </concept>
      <concept id="8998492695583129971" name="jetbrains.mps.lang.editor.structure.SubstituteFeature_DescriptionText" flags="ng" index="16NL0t">
        <child id="8998492695583129972" name="query" index="16NL0q" />
      </concept>
      <concept id="7342352913006985483" name="jetbrains.mps.lang.editor.structure.SubstituteMenuPart_Action" flags="ng" index="3eGOop">
        <child id="8612453216082699922" name="substituteHandler" index="3aKz83" />
      </concept>
      <concept id="3308396621974580100" name="jetbrains.mps.lang.editor.structure.SubstituteMenu_Default" flags="ng" index="3p36aQ" />
      <concept id="5425882385312046132" name="jetbrains.mps.lang.editor.structure.QueryFunctionParameter_SubstituteMenu_CurrentTargetNode" flags="nn" index="1yR$tW" />
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <property id="1139852716018" name="noTargetText" index="1$x2rV" />
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389214265" name="jetbrains.mps.lang.editor.structure.EditorCellModel" flags="ng" index="3EYTF0">
        <child id="4202667662392416064" name="transformationMenu" index="3vIgyS" />
      </concept>
      <concept id="1073389577006" name="jetbrains.mps.lang.editor.structure.CellModel_Constant" flags="sn" stub="3610246225209162225" index="3F0ifn">
        <property id="1082639509531" name="nullText" index="ilYzB" />
      </concept>
      <concept id="1219418625346" name="jetbrains.mps.lang.editor.structure.IStyleContainer" flags="ng" index="3F0Thp">
        <child id="1219418656006" name="styleItem" index="3F10Kt" />
      </concept>
      <concept id="1073389882823" name="jetbrains.mps.lang.editor.structure.CellModel_RefNode" flags="sg" stub="730538219795960754" index="3F1sOY" />
      <concept id="1073390211982" name="jetbrains.mps.lang.editor.structure.CellModel_RefNodeList" flags="sg" stub="2794558372793454595" index="3F2HdR" />
      <concept id="7033942394256351208" name="jetbrains.mps.lang.editor.structure.EditorComponentDeclarationReference" flags="ng" index="1PE4EZ">
        <reference id="7033942394256351817" name="editorComponent" index="1PE7su" />
      </concept>
      <concept id="4307758654696938365" name="jetbrains.mps.lang.editor.structure.QueryFunction_SubstituteMenu_RefPresentation" flags="ig" index="1WAQ3h" />
      <concept id="4307758654696952957" name="jetbrains.mps.lang.editor.structure.QueryFunctionParameter_SubstituteMenu_ReferencedNode" flags="ng" index="1WAUZh" />
      <concept id="8428109087107030357" name="jetbrains.mps.lang.editor.structure.SubstituteMenuPart_ReferenceScope" flags="ng" index="3XHNnq">
        <reference id="8428109087107339113" name="reference" index="3XGfJA" />
        <child id="4307758654694907855" name="descriptionTextFunction" index="1WZ6hz" />
      </concept>
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
      </concept>
    </language>
    <language id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions">
      <concept id="5979988948250981289" name="jetbrains.mps.lang.actions.structure.SNodeCreatorAndInitializer" flags="nn" index="2fJWfE">
        <child id="3757480014665187678" name="prototype" index="1wAG5O" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="2644386474300074836" name="jetbrains.mps.lang.smodel.structure.ConceptIdRefExpression" flags="nn" index="35c_gC">
        <reference id="2644386474300074837" name="conceptDeclaration" index="35c_gD" />
      </concept>
      <concept id="1180636770613" name="jetbrains.mps.lang.smodel.structure.SNodeCreator" flags="nn" index="3zrR0B">
        <child id="1180636770616" name="createdType" index="3zrR0E" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
      <concept id="3717301156197626279" name="jetbrains.mps.lang.core.structure.BasePlaceholder" flags="ng" index="3DQ70j" />
      <concept id="709746936026466394" name="jetbrains.mps.lang.core.structure.ChildAttribute" flags="ng" index="3VBwX9">
        <property id="709746936026609031" name="linkId" index="3V$3ak" />
        <property id="709746936026609029" name="linkRole" index="3V$3am" />
      </concept>
    </language>
  </registry>
  <node concept="3p36aQ" id="6khVixyauII">
    <property role="3GE5qa" value="base.references" />
    <ref role="aqKnT" to="ruww:6khVixyauIe" resolve="EntitySpecimenTypeReference" />
    <node concept="3XHNnq" id="6khVixyauIJ" role="3ft7WO">
      <ref role="3XGfJA" to="ruww:6khVixyauIf" resolve="target" />
      <node concept="1WAQ3h" id="6khVixyauIT" role="1WZ6hz">
        <node concept="3clFbS" id="6khVixyauIU" role="2VODD2">
          <node concept="3clFbF" id="6khVixyaqV1" role="3cqZAp">
            <node concept="2OqwBi" id="6khVixyargn" role="3clFbG">
              <node concept="1WAUZh" id="6khVixyaqV0" role="2Oq$k0" />
              <node concept="2qgKlT" id="6khVixyarE8" role="2OqNvi">
                <ref role="37wK5l" to="1yj:4V3GMfXuP$X" resolve="getReferenceDescription" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="6khVixyaBGL">
    <property role="3GE5qa" value="shared.references.specimenType" />
    <ref role="aqKnT" to="ruww:6khVixyauId" resolve="SpecimenTypeReference" />
    <node concept="2VfDsV" id="6LTgXmNfTmt" role="3ft7WO" />
  </node>
  <node concept="3p36aQ" id="4B5aqq2zb8E">
    <property role="3GE5qa" value="aspect.variables.laboratoryTest" />
    <ref role="aqKnT" to="ruww:4QUW3edDVyK" resolve="LaboratoryTestVariable" />
    <node concept="2VfDsV" id="4B5aqq2zFOD" role="3ft7WO" />
    <node concept="3eGOop" id="4B5aqq2zb8F" role="3ft7WO">
      <node concept="ucgPf" id="4B5aqq2zb8G" role="3aKz83">
        <node concept="3clFbS" id="4B5aqq2zb8H" role="2VODD2">
          <node concept="3clFbF" id="6LTgXmNALI9" role="3cqZAp">
            <node concept="2ShNRf" id="6LTgXmN_P5o" role="3clFbG">
              <node concept="2fJWfE" id="6LTgXmN_P5p" role="2ShVmc">
                <node concept="3Tqbb2" id="6LTgXmN_P5q" role="3zrR0E">
                  <ref role="ehGHo" to="ruww:4QUW3edDVyK" resolve="LaboratoryTestVariable" />
                </node>
                <node concept="1yR$tW" id="6LTgXmN_P5r" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="4B5aqq2zbEd" role="upBLP">
        <node concept="uGdhv" id="4B5aqq2zbJm" role="16NeZM">
          <node concept="3clFbS" id="4B5aqq2zbJo" role="2VODD2">
            <node concept="3clFbF" id="4B5aqq2zbRY" role="3cqZAp">
              <node concept="2OqwBi" id="4B5aqq2zcxt" role="3clFbG">
                <node concept="35c_gC" id="4B5aqq2zbRX" role="2Oq$k0">
                  <ref role="35c_gD" to="ruww:4QUW3edDVyK" resolve="LaboratoryTestVariable" />
                </node>
                <node concept="2qgKlT" id="4B5aqq2zd0X" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="4B5aqq2zdk4" role="upBLP">
        <node concept="uGdhv" id="4B5aqq2zdpq" role="16NL0q">
          <node concept="3clFbS" id="4B5aqq2zdps" role="2VODD2">
            <node concept="3clFbF" id="4B5aqq2zdy2" role="3cqZAp">
              <node concept="2OqwBi" id="4B5aqq2zeey" role="3clFbG">
                <node concept="35c_gC" id="4B5aqq2zdy1" role="2Oq$k0">
                  <ref role="35c_gD" to="ruww:4QUW3edDVyK" resolve="LaboratoryTestVariable" />
                </node>
                <node concept="2qgKlT" id="4B5aqq2zeI2" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="6LTgXmMSXg6">
    <property role="3GE5qa" value="shared.references.laboratoryTest" />
    <ref role="aqKnT" to="ruww:4QUW3efwB30" resolve="LaboratoryTestReference" />
    <node concept="2VfDsV" id="6LTgXmMT1Jn" role="3ft7WO" />
  </node>
  <node concept="3p36aQ" id="6LTgXmNeAKi">
    <property role="3GE5qa" value="shared.references.laboratoryTest" />
    <ref role="aqKnT" to="ruww:4V3GMfXHyx1" resolve="LaboratoryTestWithTextResultReference" />
    <node concept="2VfDsV" id="6LTgXmNeFiz" role="3ft7WO" />
  </node>
  <node concept="3p36aQ" id="6LTgXmNf_5l">
    <property role="3GE5qa" value="shared.references.laboratoryTest" />
    <ref role="aqKnT" to="ruww:4V3GMfXHy69" resolve="LaboratoryTestWithNumberResultReference" />
    <node concept="2VfDsV" id="6LTgXmNf_5m" role="3ft7WO" />
  </node>
  <node concept="3p36aQ" id="6LTgXmNiJ0C">
    <property role="3GE5qa" value="aspect.patterns.laboratoryTest" />
    <ref role="aqKnT" to="ruww:6LTgXmMPUpt" resolve="WildcardLaboratoryTestPattern" />
    <node concept="3eGOop" id="6LTgXmNiJ0D" role="3ft7WO">
      <node concept="ucgPf" id="6LTgXmNiJ0E" role="3aKz83">
        <node concept="3clFbS" id="6LTgXmNiJ0F" role="2VODD2">
          <node concept="3clFbF" id="6LTgXmNiJ6J" role="3cqZAp">
            <node concept="2ShNRf" id="6LTgXmNiJ6H" role="3clFbG">
              <node concept="2fJWfE" id="6LTgXmNiJfT" role="2ShVmc">
                <node concept="3Tqbb2" id="6LTgXmNiJfV" role="3zrR0E">
                  <ref role="ehGHo" to="ruww:6LTgXmMPUpt" resolve="WildcardLaboratoryTestPattern" />
                </node>
                <node concept="1yR$tW" id="6LTgXmNiJv0" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="6LTgXmNiJ_e" role="upBLP">
        <node concept="uGdhv" id="6LTgXmNiJFv" role="16NeZM">
          <node concept="3clFbS" id="6LTgXmNiJFx" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmNiJO9" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmNiKvQ" role="3clFbG">
                <node concept="35c_gC" id="6LTgXmNiJO8" role="2Oq$k0">
                  <ref role="35c_gD" to="ruww:6LTgXmMPUpt" resolve="WildcardLaboratoryTestPattern" />
                </node>
                <node concept="2qgKlT" id="6LTgXmNiL4w" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="6LTgXmNiLpp" role="upBLP">
        <node concept="uGdhv" id="6LTgXmNiLvY" role="16NL0q">
          <node concept="3clFbS" id="6LTgXmNiLw0" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmNiLCC" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmNiMcZ" role="3clFbG">
                <node concept="35c_gC" id="6LTgXmNiLCB" role="2Oq$k0">
                  <ref role="35c_gD" to="ruww:6LTgXmMPUpt" resolve="WildcardLaboratoryTestPattern" />
                </node>
                <node concept="2qgKlT" id="6LTgXmNiMLD" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="6LTgXmNpAAZ">
    <property role="3GE5qa" value="aspect.patterns.laboratoryTest" />
    <ref role="1XX52x" to="ruww:6LTgXmNpAAy" resolve="LaboratoryTestPattern" />
    <node concept="3F0ifn" id="6LTgXmNpAB1" role="2wV5jI">
      <property role="ilYzB" value="&lt;&lt; ... &gt;&gt;" />
      <node concept="VPxyj" id="6LTgXmNpKE5" role="3F10Kt">
        <property role="VOm3f" value="true" />
      </node>
      <node concept="A1WHr" id="6LTgXmNpKE8" role="3vIgyS">
        <ref role="2ZyFGn" to="ruww:6LTgXmNpAAy" resolve="LaboratoryTestPattern" />
      </node>
    </node>
  </node>
  <node concept="PKFIW" id="6LTgXmNr2Se">
    <property role="3GE5qa" value="aspect.variables.laboratoryTest" />
    <property role="TrG5h" value="Pattern_LaboratoryTestVariable_EditorComponent" />
    <ref role="1XX52x" to="ruww:4QUW3edDVyK" resolve="LaboratoryTestVariable" />
    <node concept="3F1sOY" id="6LTgXmNr2Sg" role="2wV5jI">
      <property role="1$x2rV" value="&lt;&lt; ... &gt;&gt;" />
      <ref role="1NtTu8" to="ruww:6LTgXmNpAAv" resolve="pattern" />
    </node>
    <node concept="1PE4EZ" id="6LTgXmNra57" role="1PM95z">
      <ref role="1PE7su" to="i7z6:6LTgXmNpPu9" resolve="Pattern_AspectVariable_EditorComponent" />
    </node>
  </node>
  <node concept="3p36aQ" id="6LTgXmNuoIW">
    <property role="3GE5qa" value="aspect.patterns.laboratoryTest" />
    <ref role="aqKnT" to="ruww:6LTgXmNpAAy" resolve="LaboratoryTestPattern" />
    <node concept="2VfDsV" id="6LTgXmNv8ie" role="3ft7WO" />
  </node>
  <node concept="3p36aQ" id="6LTgXmNJV$h">
    <property role="3GE5qa" value="aspect.variables.specimenType" />
    <ref role="aqKnT" to="ruww:6khVixyavhY" resolve="SpecimenTypeVariable" />
    <node concept="3eGOop" id="6LTgXmNJV$i" role="3ft7WO">
      <node concept="ucgPf" id="6LTgXmNJV$j" role="3aKz83">
        <node concept="3clFbS" id="6LTgXmNJV$k" role="2VODD2">
          <node concept="3clFbF" id="6LTgXmNJVDi" role="3cqZAp">
            <node concept="2ShNRf" id="6LTgXmNJVDg" role="3clFbG">
              <node concept="2fJWfE" id="6LTgXmNJVLp" role="2ShVmc">
                <node concept="3Tqbb2" id="6LTgXmNJVLr" role="3zrR0E">
                  <ref role="ehGHo" to="ruww:6khVixyavhY" resolve="SpecimenTypeVariable" />
                </node>
                <node concept="1yR$tW" id="6LTgXmNJVVu" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="6LTgXmNJW0A" role="upBLP">
        <node concept="uGdhv" id="6LTgXmNJW5L" role="16NeZM">
          <node concept="3clFbS" id="6LTgXmNJW5N" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmNJWer" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmNJWSd" role="3clFbG">
                <node concept="35c_gC" id="6LTgXmNJWeq" role="2Oq$k0">
                  <ref role="35c_gD" to="ruww:6khVixyavhY" resolve="SpecimenTypeVariable" />
                </node>
                <node concept="2qgKlT" id="6LTgXmNJXnM" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="6LTgXmNJXF2" role="upBLP">
        <node concept="uGdhv" id="6LTgXmNJXKx" role="16NL0q">
          <node concept="3clFbS" id="6LTgXmNJXKz" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmNJXTb" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmNJYyG" role="3clFbG">
                <node concept="35c_gC" id="6LTgXmNJXTa" role="2Oq$k0">
                  <ref role="35c_gD" to="ruww:6khVixyavhY" resolve="SpecimenTypeVariable" />
                </node>
                <node concept="2qgKlT" id="6LTgXmNJZ2h" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="PKFIW" id="6LTgXmNMlmz">
    <property role="3GE5qa" value="aspect.variables.specimenType" />
    <property role="TrG5h" value="Pattern_SpecimenTypeVariable_EditorComponent" />
    <ref role="1XX52x" to="ruww:6khVixyavhY" resolve="SpecimenTypeVariable" />
    <node concept="3F1sOY" id="6LTgXmNMlmC" role="2wV5jI">
      <property role="1$x2rV" value="&lt;&lt; ... &gt;&gt;" />
      <ref role="1NtTu8" to="ruww:6LTgXmNQ8lp" resolve="pattern" />
    </node>
    <node concept="1PE4EZ" id="6LTgXmNMlm_" role="1PM95z">
      <ref role="1PE7su" to="i7z6:6LTgXmNpPu9" resolve="Pattern_AspectVariable_EditorComponent" />
    </node>
  </node>
  <node concept="3p36aQ" id="6LTgXmNQhUe">
    <property role="3GE5qa" value="aspect.patterns.specimenType" />
    <ref role="aqKnT" to="ruww:6LTgXmNQ8ls" resolve="SpecimenTypePattern" />
    <node concept="2VfDsV" id="6LTgXmNQhUf" role="3ft7WO" />
  </node>
  <node concept="3p36aQ" id="6LTgXmNQIz9">
    <property role="3GE5qa" value="aspect.patterns.specimenType" />
    <ref role="aqKnT" to="ruww:6LTgXmNQnWm" resolve="WildcardSpecimenTypePattern" />
    <node concept="3eGOop" id="6LTgXmNQIzl" role="3ft7WO">
      <node concept="ucgPf" id="6LTgXmNQIzm" role="3aKz83">
        <node concept="3clFbS" id="6LTgXmNQIzn" role="2VODD2">
          <node concept="3clFbF" id="6LTgXmNQIC5" role="3cqZAp">
            <node concept="2ShNRf" id="6LTgXmNQIC3" role="3clFbG">
              <node concept="2fJWfE" id="6LTgXmNQIJT" role="2ShVmc">
                <node concept="3Tqbb2" id="6LTgXmNQIJV" role="3zrR0E">
                  <ref role="ehGHo" to="ruww:6LTgXmNQnWm" resolve="WildcardSpecimenTypePattern" />
                </node>
                <node concept="1yR$tW" id="6LTgXmNQIVD" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="6LTgXmNQJ0x" role="upBLP">
        <node concept="uGdhv" id="6LTgXmNQJ5s" role="16NeZM">
          <node concept="3clFbS" id="6LTgXmNQJ5u" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmNQJe6" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmNQJOt" role="3clFbG">
                <node concept="35c_gC" id="6LTgXmNQJe5" role="2Oq$k0">
                  <ref role="35c_gD" to="ruww:6LTgXmNQnWm" resolve="WildcardSpecimenTypePattern" />
                </node>
                <node concept="2qgKlT" id="6LTgXmNQKiX" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="6LTgXmNQK_Q" role="upBLP">
        <node concept="uGdhv" id="6LTgXmNQKF5" role="16NL0q">
          <node concept="3clFbS" id="6LTgXmNQKF7" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmNQKNJ" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmNQLvI" role="3clFbG">
                <node concept="35c_gC" id="6LTgXmNQKNI" role="2Oq$k0">
                  <ref role="35c_gD" to="ruww:6LTgXmNQnWm" resolve="WildcardSpecimenTypePattern" />
                </node>
                <node concept="2qgKlT" id="6LTgXmNQLYh" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="6LTgXmNXW$z">
    <property role="3GE5qa" value="aspect.patterns.laboratoryTest" />
    <ref role="aqKnT" to="ruww:6LTgXmNXs4N" resolve="SelectedLaboratoryTestPattern" />
    <node concept="3eGOop" id="6LTgXmNXW$$" role="3ft7WO">
      <node concept="ucgPf" id="6LTgXmNXW$_" role="3aKz83">
        <node concept="3clFbS" id="6LTgXmNXW$A" role="2VODD2">
          <node concept="3clFbF" id="6LTgXmNXWDh" role="3cqZAp">
            <node concept="2ShNRf" id="6LTgXmNXWDf" role="3clFbG">
              <node concept="2fJWfE" id="6LTgXmNXWL5" role="2ShVmc">
                <node concept="3Tqbb2" id="6LTgXmNXWL7" role="3zrR0E">
                  <ref role="ehGHo" to="ruww:6LTgXmNXs4N" resolve="SelectedLaboratoryTestPattern" />
                </node>
                <node concept="1yR$tW" id="6LTgXmNXWWN" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="6LTgXmNXX1E" role="upBLP">
        <node concept="uGdhv" id="6LTgXmNXX6z" role="16NeZM">
          <node concept="3clFbS" id="6LTgXmNXX6_" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmNXXfb" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmNXXIP" role="3clFbG">
                <node concept="35c_gC" id="6LTgXmNXXfa" role="2Oq$k0">
                  <ref role="35c_gD" to="ruww:6LTgXmNXs4N" resolve="SelectedLaboratoryTestPattern" />
                </node>
                <node concept="2qgKlT" id="6LTgXmNXYdg" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="6LTgXmNXYw0" role="upBLP">
        <node concept="uGdhv" id="6LTgXmNXY_6" role="16NL0q">
          <node concept="3clFbS" id="6LTgXmNXY_8" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmNXYHI" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmNXZk3" role="3clFbG">
                <node concept="35c_gC" id="6LTgXmNXYHH" role="2Oq$k0">
                  <ref role="35c_gD" to="ruww:6LTgXmNXs4N" resolve="SelectedLaboratoryTestPattern" />
                </node>
                <node concept="2qgKlT" id="6LTgXmNXZMu" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="4B5aqq2QiTl">
    <property role="3GE5qa" value="aspect.references.laboratoryTest" />
    <ref role="aqKnT" to="ruww:4B5aqq2QgEt" resolve="AspectLaboratoryTestWithTextResultReference" />
    <node concept="3XHNnq" id="4B5aqq2QiTm" role="3ft7WO">
      <ref role="3XGfJA" to="ruww:4B5aqq2QgEu" resolve="target" />
      <node concept="1WAQ3h" id="4B5aqq2QiTn" role="1WZ6hz">
        <node concept="3clFbS" id="4B5aqq2QiTo" role="2VODD2">
          <node concept="3clFbF" id="4B5aqq2QiTp" role="3cqZAp">
            <node concept="2OqwBi" id="4B5aqq2QiTq" role="3clFbG">
              <node concept="1WAUZh" id="4B5aqq2QiTr" role="2Oq$k0" />
              <node concept="2qgKlT" id="4B5aqq2QiTs" role="2OqNvi">
                <ref role="37wK5l" to="1yj:4V3GMfXuP$X" resolve="getReferenceDescription" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="4B5aqq2QhNI">
    <property role="3GE5qa" value="aspect.references.laboratoryTest" />
    <ref role="aqKnT" to="ruww:4B5aqq2QgEr" resolve="AspectLaboratoryTestWithNumberResultReference" />
    <node concept="3XHNnq" id="4B5aqq2QhNJ" role="3ft7WO">
      <ref role="3XGfJA" to="ruww:4B5aqq2QgEs" resolve="target" />
      <node concept="1WAQ3h" id="4B5aqq2QhNL" role="1WZ6hz">
        <node concept="3clFbS" id="4B5aqq2QhNM" role="2VODD2">
          <node concept="3clFbF" id="4B5aqq2QhWn" role="3cqZAp">
            <node concept="2OqwBi" id="4B5aqq2QihG" role="3clFbG">
              <node concept="1WAUZh" id="4B5aqq2QhWm" role="2Oq$k0" />
              <node concept="2qgKlT" id="4B5aqq2QiFt" role="2OqNvi">
                <ref role="37wK5l" to="1yj:4V3GMfXuP$X" resolve="getReferenceDescription" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="6khVixyavit">
    <property role="3GE5qa" value="aspect.references.specimenType" />
    <ref role="aqKnT" to="ruww:6khVixyavhZ" resolve="AspectSpecimenTypeReference" />
    <node concept="3XHNnq" id="6khVixyaviu" role="3ft7WO">
      <ref role="3XGfJA" to="ruww:6khVixyavi0" resolve="target" />
      <node concept="1WAQ3h" id="6khVixyaviw" role="1WZ6hz">
        <node concept="3clFbS" id="6khVixyavix" role="2VODD2">
          <node concept="3clFbF" id="6khVixyavr5" role="3cqZAp">
            <node concept="2OqwBi" id="6khVixyavr7" role="3clFbG">
              <node concept="1WAUZh" id="6khVixyavr8" role="2Oq$k0" />
              <node concept="2qgKlT" id="6khVixyavr9" role="2OqNvi">
                <ref role="37wK5l" to="1yj:4V3GMfXuP$X" resolve="getReferenceDescription" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="4V3GMfX_h9i">
    <property role="3GE5qa" value="aspect.references.laboratoryTest" />
    <ref role="aqKnT" to="ruww:4QUW3efwB3a" resolve="AspectLaboratoryTestReference" />
    <node concept="3XHNnq" id="4V3GMfXNXaH" role="3ft7WO">
      <ref role="3XGfJA" to="ruww:4QUW3efwB3b" resolve="target" />
      <node concept="1WAQ3h" id="4V3GMfXNXaU" role="1WZ6hz">
        <node concept="3clFbS" id="4V3GMfXNXaV" role="2VODD2">
          <node concept="3clFbF" id="4V3GMfXNXjw" role="3cqZAp">
            <node concept="2OqwBi" id="4V3GMfXNX$x" role="3clFbG">
              <node concept="1WAUZh" id="4V3GMfXNXjv" role="2Oq$k0" />
              <node concept="2qgKlT" id="4V3GMfXNY2A" role="2OqNvi">
                <ref role="37wK5l" to="1yj:4V3GMfXuP$X" resolve="getReferenceDescription" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="1I84Bf8kDtc">
    <property role="3GE5qa" value="base.references.laboratoryTest" />
    <ref role="aqKnT" to="ruww:1I84Bf8kDsK" resolve="EntityLaboratoryTestReference" />
  </node>
  <node concept="3p36aQ" id="1I84Bf8kDtG">
    <property role="3GE5qa" value="base.references.laboratoryTest" />
    <ref role="aqKnT" to="ruww:1I84Bf8kDtg" resolve="EntityLaboratoryTestWithTextResultReference" />
  </node>
  <node concept="3p36aQ" id="1I84Bf8kDu6">
    <property role="3GE5qa" value="base.references.laboratoryTest" />
    <ref role="aqKnT" to="ruww:1I84Bf8kDtd" resolve="EntityLaboratoryTestWithNumberResultReference" />
  </node>
  <node concept="3p36aQ" id="6LTgXmNFWkz">
    <property role="3GE5qa" value="aspect.variables.laboratoryTest" />
    <ref role="aqKnT" to="ruww:4B5aqq2QmL6" resolve="LaboratoryTestWithTextResultVariable" />
    <node concept="3eGOop" id="6LTgXmNFWEl" role="3ft7WO">
      <node concept="ucgPf" id="6LTgXmNFWEm" role="3aKz83">
        <node concept="3clFbS" id="6LTgXmNFWEn" role="2VODD2">
          <node concept="3clFbF" id="6LTgXmNFWEo" role="3cqZAp">
            <node concept="2ShNRf" id="6LTgXmNFWEp" role="3clFbG">
              <node concept="2fJWfE" id="6LTgXmNFWEq" role="2ShVmc">
                <node concept="3Tqbb2" id="6LTgXmNFWEr" role="3zrR0E">
                  <ref role="ehGHo" to="ruww:4B5aqq2QmL6" resolve="LaboratoryTestWithTextResultVariable" />
                </node>
                <node concept="1yR$tW" id="6LTgXmNFWEs" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="6LTgXmNFWEt" role="upBLP">
        <node concept="uGdhv" id="6LTgXmNFWEu" role="16NeZM">
          <node concept="3clFbS" id="6LTgXmNFWEv" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmNFWEw" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmNFWEx" role="3clFbG">
                <node concept="35c_gC" id="6LTgXmNFWEy" role="2Oq$k0">
                  <ref role="35c_gD" to="ruww:4B5aqq2QmL6" resolve="LaboratoryTestWithTextResultVariable" />
                </node>
                <node concept="2qgKlT" id="6LTgXmNFWEz" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="6LTgXmNFWE$" role="upBLP">
        <node concept="uGdhv" id="6LTgXmNFWE_" role="16NL0q">
          <node concept="3clFbS" id="6LTgXmNFWEA" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmNFWEB" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmNFWEC" role="3clFbG">
                <node concept="35c_gC" id="6LTgXmNFWED" role="2Oq$k0">
                  <ref role="35c_gD" to="ruww:4B5aqq2QmL6" resolve="LaboratoryTestWithTextResultVariable" />
                </node>
                <node concept="2qgKlT" id="6LTgXmNFWEE" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="PKFIW" id="65epL7MjUSw">
    <property role="3GE5qa" value="aspect.variables.laboratoryTest" />
    <property role="TrG5h" value="Pattern_LaboratoryTestWithTextResultVariable_EditorComponent" />
    <ref role="1XX52x" to="ruww:4B5aqq2QmL6" resolve="LaboratoryTestWithTextResultVariable" />
    <node concept="3F1sOY" id="65epL7MjUS$" role="2wV5jI">
      <property role="1$x2rV" value="&lt;&lt; ... &gt;&gt;" />
      <ref role="1NtTu8" to="ruww:65epL7MjUbm" resolve="pattern" />
    </node>
    <node concept="1PE4EZ" id="65epL7MjUSB" role="1PM95z">
      <ref role="1PE7su" to="i7z6:6LTgXmNpPu9" resolve="Pattern_AspectVariable_EditorComponent" />
    </node>
  </node>
  <node concept="3p36aQ" id="6LTgXmN$rPS">
    <property role="3GE5qa" value="aspect.variables.laboratoryTest" />
    <ref role="aqKnT" to="ruww:4B5aqq2QmL5" resolve="LaboratoryTestWithNumberResultVariable" />
    <node concept="3eGOop" id="6LTgXmN$rPT" role="3ft7WO">
      <node concept="ucgPf" id="6LTgXmN$rPU" role="3aKz83">
        <node concept="3clFbS" id="6LTgXmN$rPV" role="2VODD2">
          <node concept="3clFbF" id="6LTgXmN$rV9" role="3cqZAp">
            <node concept="2ShNRf" id="6LTgXmN$rV7" role="3clFbG">
              <node concept="2fJWfE" id="6LTgXmN$s3t" role="2ShVmc">
                <node concept="3Tqbb2" id="6LTgXmN$s3v" role="3zrR0E">
                  <ref role="ehGHo" to="ruww:4B5aqq2QmL5" resolve="LaboratoryTestWithNumberResultVariable" />
                </node>
                <node concept="1yR$tW" id="6LTgXmN$se2" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="6LTgXmN$sjq" role="upBLP">
        <node concept="uGdhv" id="6LTgXmN$soP" role="16NeZM">
          <node concept="3clFbS" id="6LTgXmN$soR" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmN$sxv" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmN$tjt" role="3clFbG">
                <node concept="35c_gC" id="6LTgXmN$sxu" role="2Oq$k0">
                  <ref role="35c_gD" to="ruww:4B5aqq2QmL5" resolve="LaboratoryTestWithNumberResultVariable" />
                </node>
                <node concept="2qgKlT" id="6LTgXmN$tO2" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="6LTgXmN$un_" role="upBLP">
        <node concept="uGdhv" id="6LTgXmN$utk" role="16NL0q">
          <node concept="3clFbS" id="6LTgXmN$utm" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmN$u_Y" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmN$vfV" role="3clFbG">
                <node concept="35c_gC" id="6LTgXmN$u_X" role="2Oq$k0">
                  <ref role="35c_gD" to="ruww:4B5aqq2QmL5" resolve="LaboratoryTestWithNumberResultVariable" />
                </node>
                <node concept="2qgKlT" id="6LTgXmN$vKw" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="65epL7MjP8v">
    <property role="3GE5qa" value="aspect.patterns.laboratoryTest.withTextResult" />
    <ref role="aqKnT" to="ruww:65epL7MjP7x" resolve="LaboratoryTestWithTextResultPattern" />
    <node concept="2VfDsV" id="65epL7MjZnr" role="3ft7WO" />
  </node>
  <node concept="24kQdi" id="65epL7MjP7Y">
    <property role="3GE5qa" value="aspect.patterns.laboratoryTest.withTextResult" />
    <ref role="1XX52x" to="ruww:65epL7MjP7x" resolve="LaboratoryTestWithTextResultPattern" />
    <node concept="3F0ifn" id="65epL7MjP80" role="2wV5jI">
      <property role="ilYzB" value="&lt;&lt; ... &gt;&gt;" />
      <node concept="VPxyj" id="65epL7MjP81" role="3F10Kt">
        <property role="VOm3f" value="true" />
      </node>
      <node concept="A1WHr" id="65epL7MjP82" role="3vIgyS">
        <ref role="2ZyFGn" to="ruww:65epL7MjP7x" resolve="LaboratoryTestWithTextResultPattern" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="65epL7MkNnd">
    <property role="3GE5qa" value="aspect.patterns.laboratoryTest.withNumberResult" />
    <ref role="1XX52x" to="ruww:65epL7MkNmK" resolve="LaboratoryTestWithNumberResultPattern" />
    <node concept="3F0ifn" id="65epL7MkNnf" role="2wV5jI">
      <property role="ilYzB" value="&lt;&lt; ... &gt;&gt;" />
      <node concept="VPxyj" id="65epL7MkNni" role="3F10Kt">
        <property role="VOm3f" value="true" />
      </node>
      <node concept="A1WHr" id="65epL7MkNnk" role="3vIgyS">
        <ref role="2ZyFGn" to="ruww:65epL7MkNmK" resolve="LaboratoryTestWithNumberResultPattern" />
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="65epL7MkNnJ">
    <property role="3GE5qa" value="aspect.patterns.laboratoryTest.withNumberResult" />
    <ref role="aqKnT" to="ruww:65epL7MkNmK" resolve="LaboratoryTestWithNumberResultPattern" />
    <node concept="2VfDsV" id="65epL7MkNnK" role="3ft7WO" />
  </node>
  <node concept="PKFIW" id="65epL7MkZGh">
    <property role="3GE5qa" value="aspect.variables.laboratoryTest" />
    <property role="TrG5h" value="Pattern_LaboratoryTestWithNumberResultVariable_EditorComponent" />
    <ref role="1XX52x" to="ruww:4B5aqq2QmL5" resolve="LaboratoryTestWithNumberResultVariable" />
    <node concept="3F1sOY" id="65epL7MkZGj" role="2wV5jI">
      <property role="1$x2rV" value="&lt;&lt; ... &gt;&gt;" />
      <ref role="1NtTu8" to="ruww:65epL7MkZFO" resolve="pattern" />
    </node>
    <node concept="1PE4EZ" id="65epL7MkZGl" role="1PM95z">
      <ref role="1PE7su" to="i7z6:6LTgXmNpPu9" resolve="Pattern_AspectVariable_EditorComponent" />
    </node>
  </node>
  <node concept="PKFIW" id="65epL7MsIew">
    <property role="3GE5qa" value="aspect.patterns.laboratoryTest" />
    <property role="TrG5h" value="Entities_SelectedLaboratoryTestPattner_EditorComponent" />
    <ref role="1XX52x" to="ruww:6LTgXmNXs4N" resolve="SelectedLaboratoryTestPattern" />
    <node concept="3F2HdR" id="6LTgXmNXs5t" role="2wV5jI">
      <property role="2czwfO" value="," />
      <ref role="1NtTu8" to="ruww:6LTgXmNXs4O" resolve="tests" />
      <node concept="2iRfu4" id="6LTgXmNXs5v" role="2czzBx" />
    </node>
    <node concept="1PE4EZ" id="65epL7MsIe$" role="1PM95z">
      <ref role="1PE7su" to="i7z6:65epL7MrZhM" resolve="Entities_ISelectedEntitiesPattern_EditorComponent" />
    </node>
  </node>
  <node concept="PKFIW" id="65epL7MuqcU">
    <property role="3GE5qa" value="aspect.patterns.laboratoryTest.withNumberResult" />
    <property role="TrG5h" value="Entities_SelectedLaboratoryTestWithNumberResultPattern_EditorComponent" />
    <ref role="1XX52x" to="ruww:65epL7MkNmL" resolve="SelectedLaboratoryTestWithNumberPattern" />
    <node concept="3F2HdR" id="65epL7MuqcY" role="2wV5jI">
      <property role="2czwfO" value="," />
      <ref role="1NtTu8" to="ruww:65epL7MkNmM" resolve="tests" />
      <node concept="2iRfu4" id="65epL7Muqd1" role="2czzBx" />
    </node>
    <node concept="1PE4EZ" id="65epL7MuqcW" role="1PM95z">
      <ref role="1PE7su" to="i7z6:65epL7MrZhM" resolve="Entities_ISelectedEntitiesPattern_EditorComponent" />
    </node>
  </node>
  <node concept="3p36aQ" id="65epL7MAA8J">
    <property role="3GE5qa" value="aspect.patterns.laboratoryTest.withNumberResult" />
    <ref role="aqKnT" to="ruww:65epL7MkNmL" resolve="SelectedLaboratoryTestWithNumberPattern" />
    <node concept="3eGOop" id="65epL7MAA8K" role="3ft7WO">
      <node concept="ucgPf" id="65epL7MAA8L" role="3aKz83">
        <node concept="3clFbS" id="65epL7MAA8M" role="2VODD2">
          <node concept="3clFbF" id="65epL7MAAeh" role="3cqZAp">
            <node concept="2ShNRf" id="65epL7MAAef" role="3clFbG">
              <node concept="2fJWfE" id="65epL7MAAmQ" role="2ShVmc">
                <node concept="3Tqbb2" id="65epL7MAAmS" role="3zrR0E">
                  <ref role="ehGHo" to="ruww:65epL7MkNmL" resolve="SelectedLaboratoryTestWithNumberPattern" />
                </node>
                <node concept="1yR$tW" id="65epL7MAA$A" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="65epL7MAAEh" role="upBLP">
        <node concept="uGdhv" id="65epL7MAAJY" role="16NeZM">
          <node concept="3clFbS" id="65epL7MAAK0" role="2VODD2">
            <node concept="3clFbF" id="65epL7MAASA" role="3cqZAp">
              <node concept="2OqwBi" id="65epL7MABzf" role="3clFbG">
                <node concept="35c_gC" id="65epL7MAAS_" role="2Oq$k0">
                  <ref role="35c_gD" to="ruww:65epL7MkNmL" resolve="SelectedLaboratoryTestWithNumberPattern" />
                </node>
                <node concept="2qgKlT" id="65epL7MAC5s" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="65epL7MACpq" role="upBLP">
        <node concept="uGdhv" id="65epL7MACvk" role="16NL0q">
          <node concept="3clFbS" id="65epL7MACvm" role="2VODD2">
            <node concept="3clFbF" id="65epL7MACBW" role="3cqZAp">
              <node concept="2OqwBi" id="65epL7MADbw" role="3clFbG">
                <node concept="35c_gC" id="65epL7MACBV" role="2Oq$k0">
                  <ref role="35c_gD" to="ruww:65epL7MkNmL" resolve="SelectedLaboratoryTestWithNumberPattern" />
                </node>
                <node concept="2qgKlT" id="65epL7MADHH" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="65epL7MANJI">
    <property role="3GE5qa" value="aspect.patterns.laboratoryTest.withNumberResult" />
    <ref role="aqKnT" to="ruww:65epL7MkNmN" resolve="WildcardLaboratoryTestWithNumberPattern" />
    <node concept="3eGOop" id="65epL7MANJJ" role="3ft7WO">
      <node concept="16NfWO" id="65epL7MAQuB" role="upBLP">
        <node concept="uGdhv" id="65epL7MAQ$y" role="16NeZM">
          <node concept="3clFbS" id="65epL7MAQ$$" role="2VODD2">
            <node concept="3clFbF" id="65epL7MAQHa" role="3cqZAp">
              <node concept="2OqwBi" id="65epL7MARe1" role="3clFbG">
                <node concept="35c_gC" id="65epL7MAQH9" role="2Oq$k0">
                  <ref role="35c_gD" to="ruww:65epL7MkNmN" resolve="WildcardLaboratoryTestWithNumberPattern" />
                </node>
                <node concept="2qgKlT" id="65epL7MARKe" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="ucgPf" id="65epL7MANJK" role="3aKz83">
        <node concept="3clFbS" id="65epL7MANJL" role="2VODD2">
          <node concept="3clFbF" id="65epL7MANPg" role="3cqZAp">
            <node concept="2ShNRf" id="65epL7MANPe" role="3clFbG">
              <node concept="2fJWfE" id="65epL7MANXP" role="2ShVmc">
                <node concept="3Tqbb2" id="65epL7MANXR" role="3zrR0E">
                  <ref role="ehGHo" to="ruww:65epL7MkNmN" resolve="WildcardLaboratoryTestWithNumberPattern" />
                </node>
                <node concept="1yR$tW" id="65epL7MAO90" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="65epL7MAOeF" role="upBLP">
        <node concept="uGdhv" id="65epL7MAOko" role="16NL0q">
          <node concept="3clFbS" id="65epL7MAOkq" role="2VODD2">
            <node concept="3clFbF" id="65epL7MAOt0" role="3cqZAp">
              <node concept="2OqwBi" id="65epL7MAOXR" role="3clFbG">
                <node concept="35c_gC" id="65epL7MAOsZ" role="2Oq$k0">
                  <ref role="35c_gD" to="ruww:65epL7MkNmN" resolve="WildcardLaboratoryTestWithNumberPattern" />
                </node>
                <node concept="2qgKlT" id="65epL7MAQaC" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3DQ70j" id="65epL7MAPIa" role="lGtFl">
        <property role="3V$3am" value="features" />
        <property role="3V$3ak" value="18bc6592-03a6-4e29-a83a-7ff23bde13ba/8478191136883534237/8478191136883534238" />
      </node>
    </node>
  </node>
  <node concept="PKFIW" id="65epL7MAXLj">
    <property role="3GE5qa" value="aspect.patterns.laboratoryTest.withTextResult" />
    <property role="TrG5h" value="Entitites_SelectedLaboratoryTestWithTextPattern" />
    <ref role="1XX52x" to="ruww:65epL7MjP7y" resolve="SelectedLaboratoryTestWithTextPattern" />
    <node concept="3F2HdR" id="65epL7MAXLn" role="2wV5jI">
      <property role="2czwfO" value="," />
      <ref role="1NtTu8" to="ruww:65epL7MjP7z" resolve="tests" />
      <node concept="2iRfu4" id="65epL7MAXLq" role="2czzBx" />
    </node>
    <node concept="1PE4EZ" id="65epL7MAXLl" role="1PM95z">
      <ref role="1PE7su" to="i7z6:65epL7MrZhM" resolve="Entities_ISelectedEntitiesPattern_EditorComponent" />
    </node>
  </node>
  <node concept="3p36aQ" id="65epL7MB6IN">
    <property role="3GE5qa" value="aspect.patterns.laboratoryTest.withTextResult" />
    <ref role="aqKnT" to="ruww:65epL7MjP7y" resolve="SelectedLaboratoryTestWithTextPattern" />
    <node concept="3eGOop" id="65epL7MB6IO" role="3ft7WO">
      <node concept="ucgPf" id="65epL7MB6IP" role="3aKz83">
        <node concept="3clFbS" id="65epL7MB6IQ" role="2VODD2">
          <node concept="3clFbF" id="65epL7MB6Ol" role="3cqZAp">
            <node concept="2ShNRf" id="65epL7MB6Oj" role="3clFbG">
              <node concept="2fJWfE" id="65epL7MB6WU" role="2ShVmc">
                <node concept="3Tqbb2" id="65epL7MB6WW" role="3zrR0E">
                  <ref role="ehGHo" to="ruww:65epL7MjP7y" resolve="SelectedLaboratoryTestWithTextPattern" />
                </node>
                <node concept="1yR$tW" id="65epL7MB785" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="65epL7MB7dK" role="upBLP">
        <node concept="uGdhv" id="65epL7MB7jt" role="16NeZM">
          <node concept="3clFbS" id="65epL7MB7jv" role="2VODD2">
            <node concept="3clFbF" id="65epL7MB7s5" role="3cqZAp">
              <node concept="2OqwBi" id="65epL7MB802" role="3clFbG">
                <node concept="35c_gC" id="65epL7MB7s4" role="2Oq$k0">
                  <ref role="35c_gD" to="ruww:65epL7MjP7y" resolve="SelectedLaboratoryTestWithTextPattern" />
                </node>
                <node concept="2qgKlT" id="65epL7MB8yc" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="65epL7MB8Qa" role="upBLP">
        <node concept="uGdhv" id="65epL7MB8W4" role="16NL0q">
          <node concept="3clFbS" id="65epL7MB8W6" role="2VODD2">
            <node concept="3clFbF" id="65epL7MB94G" role="3cqZAp">
              <node concept="2OqwBi" id="65epL7MB9GC" role="3clFbG">
                <node concept="35c_gC" id="65epL7MB94F" role="2Oq$k0">
                  <ref role="35c_gD" to="ruww:65epL7MjP7y" resolve="SelectedLaboratoryTestWithTextPattern" />
                </node>
                <node concept="2qgKlT" id="65epL7MBaeP" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="65epL7ME5Y9">
    <property role="3GE5qa" value="aspect.patterns.laboratoryTest.withTextResult" />
    <ref role="aqKnT" to="ruww:65epL7MjP7$" resolve="WildcardLaboratoryTestWithTextPattern" />
    <node concept="3eGOop" id="65epL7ME5Ya" role="3ft7WO">
      <node concept="ucgPf" id="65epL7ME5Yb" role="3aKz83">
        <node concept="3clFbS" id="65epL7ME5Yc" role="2VODD2">
          <node concept="3clFbF" id="65epL7ME63F" role="3cqZAp">
            <node concept="2ShNRf" id="65epL7ME63D" role="3clFbG">
              <node concept="2fJWfE" id="65epL7ME6cj" role="2ShVmc">
                <node concept="3Tqbb2" id="65epL7ME6cl" role="3zrR0E">
                  <ref role="ehGHo" to="ruww:65epL7MjP7$" resolve="WildcardLaboratoryTestWithTextPattern" />
                </node>
                <node concept="1yR$tW" id="65epL7ME6nu" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="65epL7ME6t9" role="upBLP">
        <node concept="uGdhv" id="65epL7ME6yQ" role="16NeZM">
          <node concept="3clFbS" id="65epL7ME6yS" role="2VODD2">
            <node concept="3clFbF" id="65epL7ME6Fu" role="3cqZAp">
              <node concept="2OqwBi" id="65epL7ME7jq" role="3clFbG">
                <node concept="35c_gC" id="65epL7ME6Ft" role="2Oq$k0">
                  <ref role="35c_gD" to="ruww:65epL7MjP7$" resolve="WildcardLaboratoryTestWithTextPattern" />
                </node>
                <node concept="2qgKlT" id="65epL7ME7PB" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="65epL7ME8LF" role="upBLP">
        <node concept="uGdhv" id="65epL7ME8R_" role="16NL0q">
          <node concept="3clFbS" id="65epL7ME8RB" role="2VODD2">
            <node concept="3clFbF" id="65epL7ME90d" role="3cqZAp">
              <node concept="2OqwBi" id="65epL7ME9C9" role="3clFbG">
                <node concept="35c_gC" id="65epL7ME90c" role="2Oq$k0">
                  <ref role="35c_gD" to="ruww:65epL7MjP7$" resolve="WildcardLaboratoryTestWithTextPattern" />
                </node>
                <node concept="2qgKlT" id="65epL7MEaam" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

