<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:d7787915-5b9a-4713-a6ab-6345c5e1ec03(no.uio.mLab.decisions.references.laboratoryTest.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="6" />
    <devkit ref="78434eb8-b0e5-444b-850d-e7c4ad2da9ab(jetbrains.mps.devkit.aspect.structure)" />
  </languages>
  <imports>
    <import index="xvtf" ref="r:506bae70-6209-41ca-8d8b-9690b3810379(no.uio.mLab.entities.laboratoryTest.structure)" />
    <import index="vbok" ref="r:6cc208d3-a370-49ca-a6ac-ded97805a924(no.uio.mLab.shared.structure)" />
    <import index="7f9y" ref="r:790ac37e-42d3-4dd3-977c-e8fe9cc56a45(no.uio.mLab.decisions.core.structure)" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <property id="4628067390765956807" name="final" index="R5$K2" />
        <property id="4628067390765956802" name="abstract" index="R5$K7" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
      </concept>
      <concept id="1169125989551" name="jetbrains.mps.lang.structure.structure.InterfaceConceptDeclaration" flags="ig" index="PlHQZ">
        <child id="1169127546356" name="extends" index="PrDN$" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599698500" name="specializedLink" index="20ksaX" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="PlHQZ" id="4QUW3eduAhL">
    <property role="EcuMT" value="5601053190796239985" />
    <property role="TrG5h" value="ITranslatableLaboratoryTestReferenceConcept" />
    <property role="3GE5qa" value="shared" />
    <node concept="PrWs8" id="4QUW3edwZl$" role="PrDN$">
      <ref role="PrY4T" to="vbok:5Wfdz$0vc1Z" resolve="ITranslatableConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="4QUW3edDVyK">
    <property role="EcuMT" value="5601053190799210672" />
    <property role="3GE5qa" value="aspect.variables.laboratoryTest" />
    <property role="TrG5h" value="LaboratoryTestVariable" />
    <ref role="1TJDcQ" to="7f9y:4QUW3edDXqS" resolve="AspectVariable" />
    <node concept="1TJgyj" id="6LTgXmNpAAv" role="1TKVEi">
      <property role="IQ2ns" value="7816353213395134879" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="pattern" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="6LTgXmNpAAy" resolve="LaboratoryTestPattern" />
      <ref role="20ksaX" to="7f9y:6LTgXmNnjBG" resolve="pattern" />
    </node>
  </node>
  <node concept="1TIwiD" id="4QUW3efwB30">
    <property role="EcuMT" value="5601053190830321856" />
    <property role="TrG5h" value="LaboratoryTestReference" />
    <property role="R5$K7" value="false" />
    <property role="R5$K2" value="false" />
    <property role="3GE5qa" value="shared.references.laboratoryTest" />
    <ref role="1TJDcQ" to="7f9y:4QUW3efv2iG" resolve="Reference" />
    <node concept="PrWs8" id="4QUW3efwB31" role="PzmwI">
      <ref role="PrY4T" node="4QUW3eduAhL" resolve="ITranslatableLaboratoryTestReferenceConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="4V3GMfXHy69">
    <property role="EcuMT" value="5675576922578755977" />
    <property role="3GE5qa" value="shared.references.laboratoryTest" />
    <property role="TrG5h" value="LaboratoryTestWithNumberResultReference" />
    <property role="R5$K7" value="false" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" to="7f9y:4QUW3efv2iG" resolve="Reference" />
    <node concept="PrWs8" id="4V3GMfXHzqo" role="PzmwI">
      <ref role="PrY4T" node="4QUW3eduAhL" resolve="ITranslatableLaboratoryTestReferenceConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="4V3GMfXHyx1">
    <property role="EcuMT" value="5675576922578757697" />
    <property role="3GE5qa" value="shared.references.laboratoryTest" />
    <property role="TrG5h" value="LaboratoryTestWithTextResultReference" />
    <property role="R5$K7" value="false" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" to="7f9y:4QUW3efv2iG" resolve="Reference" />
    <node concept="PrWs8" id="4V3GMfXHzqq" role="PzmwI">
      <ref role="PrY4T" node="4QUW3eduAhL" resolve="ITranslatableLaboratoryTestReferenceConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="6khVixyauId">
    <property role="EcuMT" value="7282862830136191885" />
    <property role="3GE5qa" value="shared.references.specimenType" />
    <property role="TrG5h" value="SpecimenTypeReference" />
    <ref role="1TJDcQ" to="7f9y:4QUW3efv2iG" resolve="Reference" />
    <node concept="PrWs8" id="6LTgXmNfJvQ" role="PzmwI">
      <ref role="PrY4T" node="4QUW3eduAhL" resolve="ITranslatableLaboratoryTestReferenceConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="6khVixyauIe">
    <property role="EcuMT" value="7282862830136191886" />
    <property role="3GE5qa" value="base.references" />
    <property role="TrG5h" value="EntitySpecimenTypeReference" />
    <ref role="1TJDcQ" node="6khVixyauId" resolve="SpecimenTypeReference" />
    <node concept="1TJgyj" id="6khVixyauIf" role="1TKVEi">
      <property role="IQ2ns" value="7282862830136191887" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="target" />
      <ref role="20lvS9" to="xvtf:6khVixya4DS" resolve="SpecimenType" />
      <ref role="20ksaX" to="7f9y:4QUW3efv2jv" resolve="target" />
    </node>
    <node concept="PrWs8" id="6LTgXmMAn$q" role="PzmwI">
      <ref role="PrY4T" to="7f9y:6LTgXmMvpq9" resolve="IEntityReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="6khVixyavhY">
    <property role="EcuMT" value="7282862830136194174" />
    <property role="3GE5qa" value="aspect.variables.specimenType" />
    <property role="TrG5h" value="SpecimenTypeVariable" />
    <ref role="1TJDcQ" to="7f9y:4QUW3edDXqS" resolve="AspectVariable" />
    <node concept="1TJgyj" id="6LTgXmNQ8lp" role="1TKVEi">
      <property role="IQ2ns" value="7816353213402613081" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="pattern" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="6LTgXmNQ8ls" resolve="SpecimenTypePattern" />
      <ref role="20ksaX" to="7f9y:6LTgXmNnjBG" resolve="pattern" />
    </node>
  </node>
  <node concept="1TIwiD" id="6LTgXmMPUpt">
    <property role="EcuMT" value="7816353213385778781" />
    <property role="3GE5qa" value="aspect.patterns.laboratoryTest" />
    <property role="TrG5h" value="WildcardLaboratoryTestPattern" />
    <ref role="1TJDcQ" node="6LTgXmNpAAy" resolve="LaboratoryTestPattern" />
    <node concept="PrWs8" id="65epL7Mov9Z" role="PzmwI">
      <ref role="PrY4T" to="7f9y:65epL7Ml5Dl" resolve="IWildcardPattern" />
    </node>
  </node>
  <node concept="1TIwiD" id="6LTgXmNpAAy">
    <property role="EcuMT" value="7816353213395134882" />
    <property role="3GE5qa" value="aspect.patterns.laboratoryTest" />
    <property role="TrG5h" value="LaboratoryTestPattern" />
    <property role="R5$K7" value="false" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" to="7f9y:6LTgXmNrfh_" resolve="AspectVariablePattern" />
  </node>
  <node concept="1TIwiD" id="6LTgXmNQ8ls">
    <property role="EcuMT" value="7816353213402613084" />
    <property role="3GE5qa" value="aspect.patterns.specimenType" />
    <property role="TrG5h" value="SpecimenTypePattern" />
    <ref role="1TJDcQ" to="7f9y:6LTgXmNrfh_" resolve="AspectVariablePattern" />
  </node>
  <node concept="1TIwiD" id="6LTgXmNQnWm">
    <property role="EcuMT" value="7816353213402677014" />
    <property role="3GE5qa" value="aspect.patterns.specimenType" />
    <property role="TrG5h" value="WildcardSpecimenTypePattern" />
    <ref role="1TJDcQ" node="6LTgXmNQ8ls" resolve="SpecimenTypePattern" />
    <node concept="PrWs8" id="65epL7MpeJd" role="PzmwI">
      <ref role="PrY4T" to="7f9y:65epL7Ml5Dl" resolve="IWildcardPattern" />
    </node>
  </node>
  <node concept="1TIwiD" id="6LTgXmNXs4N">
    <property role="EcuMT" value="7816353213404528947" />
    <property role="3GE5qa" value="aspect.patterns.laboratoryTest" />
    <property role="TrG5h" value="SelectedLaboratoryTestPattern" />
    <ref role="1TJDcQ" node="6LTgXmNpAAy" resolve="LaboratoryTestPattern" />
    <node concept="1TJgyj" id="6LTgXmNXs4O" role="1TKVEi">
      <property role="IQ2ns" value="7816353213404528948" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="tests" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="4QUW3efwB30" resolve="LaboratoryTestReference" />
      <ref role="20ksaX" to="7f9y:65epL7MqHQ9" resolve="entities" />
    </node>
    <node concept="PrWs8" id="65epL7MqHPw" role="PzmwI">
      <ref role="PrY4T" to="7f9y:65epL7Mprkt" resolve="ISelectedEntityPattern" />
    </node>
  </node>
  <node concept="1TIwiD" id="4B5aqq2QgEt">
    <property role="EcuMT" value="5315700730317310621" />
    <property role="3GE5qa" value="aspect.references.laboratoryTest" />
    <property role="TrG5h" value="AspectLaboratoryTestWithTextResultReference" />
    <ref role="1TJDcQ" node="4V3GMfXHyx1" resolve="LaboratoryTestWithTextResultReference" />
    <node concept="1TJgyj" id="4B5aqq2QgEu" role="1TKVEi">
      <property role="IQ2ns" value="5315700730317310622" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="target" />
      <ref role="20lvS9" node="4B5aqq2QmL6" resolve="LaboratoryTestWithTextResultVariable" />
      <ref role="20ksaX" to="7f9y:6LTgXmMAS1z" resolve="target" />
    </node>
    <node concept="PrWs8" id="6LTgXmMCRdl" role="PzmwI">
      <ref role="PrY4T" to="7f9y:6LTgXmMAn$t" resolve="IAspectVariableReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="4B5aqq2QgEr">
    <property role="EcuMT" value="5315700730317310619" />
    <property role="3GE5qa" value="aspect.references.laboratoryTest" />
    <property role="TrG5h" value="AspectLaboratoryTestWithNumberResultReference" />
    <ref role="1TJDcQ" node="4V3GMfXHy69" resolve="LaboratoryTestWithNumberResultReference" />
    <node concept="1TJgyj" id="4B5aqq2QgEs" role="1TKVEi">
      <property role="IQ2ns" value="5315700730317310620" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="target" />
      <ref role="20lvS9" node="4B5aqq2QmL5" resolve="LaboratoryTestWithNumberResultVariable" />
      <ref role="20ksaX" to="7f9y:6LTgXmMAS1z" resolve="target" />
    </node>
    <node concept="PrWs8" id="6LTgXmMCRdi" role="PzmwI">
      <ref role="PrY4T" to="7f9y:6LTgXmMAn$t" resolve="IAspectVariableReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="6khVixyavhZ">
    <property role="EcuMT" value="7282862830136194175" />
    <property role="3GE5qa" value="aspect.references.specimenType" />
    <property role="TrG5h" value="AspectSpecimenTypeReference" />
    <ref role="1TJDcQ" node="6khVixyauId" resolve="SpecimenTypeReference" />
    <node concept="1TJgyj" id="6khVixyavi0" role="1TKVEi">
      <property role="IQ2ns" value="7282862830136194176" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="target" />
      <ref role="20lvS9" node="6khVixyavhY" resolve="SpecimenTypeVariable" />
      <ref role="20ksaX" to="7f9y:6LTgXmMAS1z" resolve="target" />
    </node>
    <node concept="PrWs8" id="6LTgXmMCRdo" role="PzmwI">
      <ref role="PrY4T" to="7f9y:6LTgXmMAn$t" resolve="IAspectVariableReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="4QUW3efwB3a">
    <property role="EcuMT" value="5601053190830321866" />
    <property role="3GE5qa" value="aspect.references.laboratoryTest" />
    <property role="TrG5h" value="AspectLaboratoryTestReference" />
    <ref role="1TJDcQ" node="4QUW3efwB30" resolve="LaboratoryTestReference" />
    <node concept="1TJgyj" id="4QUW3efwB3b" role="1TKVEi">
      <property role="IQ2ns" value="5601053190830321867" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="target" />
      <ref role="20lvS9" node="4QUW3edDVyK" resolve="LaboratoryTestVariable" />
      <ref role="20ksaX" to="7f9y:6LTgXmMAS1z" resolve="target" />
    </node>
    <node concept="PrWs8" id="6LTgXmMCRdf" role="PzmwI">
      <ref role="PrY4T" to="7f9y:6LTgXmMAn$t" resolve="IAspectVariableReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="1I84Bf8kDsK">
    <property role="EcuMT" value="1983855924348098352" />
    <property role="3GE5qa" value="base.references.laboratoryTest" />
    <property role="TrG5h" value="EntityLaboratoryTestReference" />
    <ref role="1TJDcQ" node="4QUW3efwB30" resolve="LaboratoryTestReference" />
    <node concept="1TJgyj" id="1I84Bf8kDsL" role="1TKVEi">
      <property role="IQ2ns" value="1983855924348098353" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="target" />
      <ref role="20ksaX" to="7f9y:4QUW3efv2jv" resolve="target" />
      <ref role="20lvS9" to="xvtf:1mAGFBLav_o" resolve="LaboratoryTest" />
    </node>
    <node concept="PrWs8" id="1I84Bf8kDsM" role="PzmwI">
      <ref role="PrY4T" to="7f9y:6LTgXmMvpq9" resolve="IEntityReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="1I84Bf8kDtd">
    <property role="EcuMT" value="1983855924348098381" />
    <property role="3GE5qa" value="base.references.laboratoryTest" />
    <property role="TrG5h" value="EntityLaboratoryTestWithNumberResultReference" />
    <ref role="1TJDcQ" node="4V3GMfXHy69" resolve="LaboratoryTestWithNumberResultReference" />
    <node concept="1TJgyj" id="1I84Bf8kDte" role="1TKVEi">
      <property role="IQ2ns" value="1983855924348098382" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="target" />
      <ref role="20lvS9" to="xvtf:1mAGFBLav_o" resolve="LaboratoryTest" />
      <ref role="20ksaX" to="7f9y:4QUW3efv2jv" resolve="target" />
    </node>
    <node concept="PrWs8" id="1I84Bf8kDtf" role="PzmwI">
      <ref role="PrY4T" to="7f9y:6LTgXmMvpq9" resolve="IEntityReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="1I84Bf8kDtg">
    <property role="EcuMT" value="1983855924348098384" />
    <property role="3GE5qa" value="base.references.laboratoryTest" />
    <property role="TrG5h" value="EntityLaboratoryTestWithTextResultReference" />
    <ref role="1TJDcQ" node="4V3GMfXHyx1" resolve="LaboratoryTestWithTextResultReference" />
    <node concept="1TJgyj" id="1I84Bf8kDth" role="1TKVEi">
      <property role="IQ2ns" value="1983855924348098385" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="target" />
      <ref role="20lvS9" to="xvtf:1mAGFBLav_o" resolve="LaboratoryTest" />
      <ref role="20ksaX" to="7f9y:4QUW3efv2jv" resolve="target" />
    </node>
    <node concept="PrWs8" id="1I84Bf8kDti" role="PzmwI">
      <ref role="PrY4T" to="7f9y:6LTgXmMvpq9" resolve="IEntityReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="4B5aqq2QmL6">
    <property role="EcuMT" value="5315700730317335622" />
    <property role="3GE5qa" value="aspect.variables.laboratoryTest" />
    <property role="TrG5h" value="LaboratoryTestWithTextResultVariable" />
    <ref role="1TJDcQ" node="4QUW3edDVyK" resolve="LaboratoryTestVariable" />
    <node concept="1TJgyj" id="65epL7MjUbm" role="1TKVEi">
      <property role="IQ2ns" value="7011654996639851222" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="pattern" />
      <ref role="20ksaX" node="6LTgXmNpAAv" resolve="pattern" />
      <ref role="20lvS9" node="65epL7MjP7x" resolve="LaboratoryTestWithTextResultPattern" />
    </node>
  </node>
  <node concept="1TIwiD" id="4B5aqq2QmL5">
    <property role="EcuMT" value="5315700730317335621" />
    <property role="3GE5qa" value="aspect.variables.laboratoryTest" />
    <property role="TrG5h" value="LaboratoryTestWithNumberResultVariable" />
    <ref role="1TJDcQ" node="4QUW3edDVyK" resolve="LaboratoryTestVariable" />
    <node concept="1TJgyj" id="65epL7MkZFO" role="1TKVEi">
      <property role="IQ2ns" value="7011654996640135924" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="pattern" />
      <ref role="20ksaX" node="6LTgXmNpAAv" resolve="pattern" />
      <ref role="20lvS9" node="65epL7MkNmK" resolve="LaboratoryTestWithNumberResultPattern" />
    </node>
  </node>
  <node concept="1TIwiD" id="65epL7MjP7$">
    <property role="EcuMT" value="7011654996639830500" />
    <property role="3GE5qa" value="aspect.patterns.laboratoryTest.withTextResult" />
    <property role="TrG5h" value="WildcardLaboratoryTestWithTextPattern" />
    <ref role="1TJDcQ" node="65epL7MjP7x" resolve="LaboratoryTestWithTextResultPattern" />
    <node concept="PrWs8" id="65epL7MBgwF" role="PzmwI">
      <ref role="PrY4T" to="7f9y:65epL7Ml5Dl" resolve="IWildcardPattern" />
    </node>
  </node>
  <node concept="1TIwiD" id="65epL7MjP7y">
    <property role="EcuMT" value="7011654996639830498" />
    <property role="3GE5qa" value="aspect.patterns.laboratoryTest.withTextResult" />
    <property role="TrG5h" value="SelectedLaboratoryTestWithTextPattern" />
    <ref role="1TJDcQ" node="65epL7MjP7x" resolve="LaboratoryTestWithTextResultPattern" />
    <node concept="1TJgyj" id="65epL7MjP7z" role="1TKVEi">
      <property role="IQ2ns" value="7011654996639830499" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="tests" />
      <property role="20lbJX" value="0..n" />
      <ref role="20ksaX" to="7f9y:65epL7MqHQ9" resolve="entities" />
      <ref role="20lvS9" node="4V3GMfXHyx1" resolve="LaboratoryTestWithTextResultReference" />
    </node>
    <node concept="PrWs8" id="65epL7MAXLu" role="PzmwI">
      <ref role="PrY4T" to="7f9y:65epL7Mprkt" resolve="ISelectedEntityPattern" />
    </node>
  </node>
  <node concept="1TIwiD" id="65epL7MjP7x">
    <property role="EcuMT" value="7011654996639830497" />
    <property role="3GE5qa" value="aspect.patterns.laboratoryTest.withTextResult" />
    <property role="TrG5h" value="LaboratoryTestWithTextResultPattern" />
    <property role="R5$K7" value="false" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" node="6LTgXmNpAAy" resolve="LaboratoryTestPattern" />
  </node>
  <node concept="1TIwiD" id="65epL7MkNmK">
    <property role="EcuMT" value="7011654996640085424" />
    <property role="3GE5qa" value="aspect.patterns.laboratoryTest.withNumberResult" />
    <property role="TrG5h" value="LaboratoryTestWithNumberResultPattern" />
    <property role="R5$K7" value="false" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" node="6LTgXmNpAAy" resolve="LaboratoryTestPattern" />
  </node>
  <node concept="1TIwiD" id="65epL7MkNmL">
    <property role="EcuMT" value="7011654996640085425" />
    <property role="3GE5qa" value="aspect.patterns.laboratoryTest.withNumberResult" />
    <property role="TrG5h" value="SelectedLaboratoryTestWithNumberPattern" />
    <ref role="1TJDcQ" node="65epL7MkNmK" resolve="LaboratoryTestWithNumberResultPattern" />
    <node concept="1TJgyj" id="65epL7MkNmM" role="1TKVEi">
      <property role="IQ2ns" value="7011654996640085426" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="tests" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="4V3GMfXHy69" resolve="LaboratoryTestWithNumberResultReference" />
      <ref role="20ksaX" to="7f9y:65epL7MqHQ9" resolve="entities" />
    </node>
    <node concept="PrWs8" id="65epL7Muqd3" role="PzmwI">
      <ref role="PrY4T" to="7f9y:65epL7Mprkt" resolve="ISelectedEntityPattern" />
    </node>
  </node>
  <node concept="1TIwiD" id="65epL7MkNmN">
    <property role="EcuMT" value="7011654996640085427" />
    <property role="3GE5qa" value="aspect.patterns.laboratoryTest.withNumberResult" />
    <property role="TrG5h" value="WildcardLaboratoryTestWithNumberPattern" />
    <ref role="1TJDcQ" node="65epL7MkNmK" resolve="LaboratoryTestWithNumberResultPattern" />
    <node concept="PrWs8" id="65epL7MAJH6" role="PzmwI">
      <ref role="PrY4T" to="7f9y:65epL7Ml5Dl" resolve="IWildcardPattern" />
    </node>
  </node>
</model>

