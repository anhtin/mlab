<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:e27f2141-c05d-487b-904a-4a3994dcfe64(no.uio.mLab.decisions.references.laboratoryTest.translations)">
  <persistence version="9" />
  <languages>
    <use id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage" version="6" />
  </languages>
  <imports>
    <import index="9igz" ref="r:0ba33904-c124-449b-bde4-640f55a71953(no.uio.mLab.shared.translations)" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1465982738277781862" name="jetbrains.mps.baseLanguage.structure.PlaceholderMember" flags="ng" index="2tJIrI" />
      <concept id="1188207840427" name="jetbrains.mps.baseLanguage.structure.AnnotationInstance" flags="nn" index="2AHcQZ">
        <reference id="1188208074048" name="annotation" index="2AI5Lk" />
      </concept>
      <concept id="1188208481402" name="jetbrains.mps.baseLanguage.structure.HasAnnotation" flags="ng" index="2AJDlI">
        <child id="1188208488637" name="annotation" index="2AJF6D" />
      </concept>
      <concept id="2820489544401957797" name="jetbrains.mps.baseLanguage.structure.DefaultClassCreator" flags="nn" index="HV5vD">
        <reference id="2820489544401957798" name="classifier" index="HV5vE" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1070462154015" name="jetbrains.mps.baseLanguage.structure.StaticFieldDeclaration" flags="ig" index="Wx3nA">
        <property id="6468716278899126575" name="isVolatile" index="2dlcS1" />
        <property id="6468716278899125786" name="isTransient" index="2dld4O" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1081236700938" name="jetbrains.mps.baseLanguage.structure.StaticMethodDeclaration" flags="ig" index="2YIFZL" />
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu">
        <child id="1095933932569" name="implementedInterface" index="EKbjA" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <property id="1176718929932" name="isFinal" index="3TUv4t" />
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <property id="4276006055363816570" name="isSynchronized" index="od$2w" />
        <property id="1181808852946" name="isFinal" index="DiZV1" />
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123165" name="jetbrains.mps.baseLanguage.structure.InstanceMethodDeclaration" flags="ig" index="3clFb_">
        <property id="1178608670077" name="isAbstract" index="1EzhhJ" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1107461130800" name="jetbrains.mps.baseLanguage.structure.Classifier" flags="ng" index="3pOWGL">
        <child id="5375687026011219971" name="member" index="jymVt" unordered="true" />
      </concept>
      <concept id="7812454656619025416" name="jetbrains.mps.baseLanguage.structure.MethodDeclaration" flags="ng" index="1rXfSm">
        <property id="8355037393041754995" name="isNative" index="2aFKle" />
      </concept>
      <concept id="7812454656619025412" name="jetbrains.mps.baseLanguage.structure.LocalMethodCall" flags="nn" index="1rXfSq" />
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1107796713796" name="jetbrains.mps.baseLanguage.structure.Interface" flags="ig" index="3HP615" />
      <concept id="1163670490218" name="jetbrains.mps.baseLanguage.structure.SwitchStatement" flags="nn" index="3KaCP$">
        <child id="1163670592366" name="defaultBlock" index="3Kb1Dw" />
        <child id="1163670766145" name="expression" index="3KbGdf" />
        <child id="1163670772911" name="case" index="3KbHQx" />
      </concept>
      <concept id="1163670641947" name="jetbrains.mps.baseLanguage.structure.SwitchCase" flags="ng" index="3KbdKl">
        <child id="1163670677455" name="expression" index="3Kbmr1" />
        <child id="1163670683720" name="body" index="3Kbo56" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
      <concept id="1146644623116" name="jetbrains.mps.baseLanguage.structure.PrivateVisibility" flags="nn" index="3Tm6S6" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="3HP615" id="4zMac8rUNtP">
    <property role="TrG5h" value="ILaboratoryTestReferenceTranslations" />
    <property role="3GE5qa" value="internationalization" />
    <node concept="3Tm1VV" id="4zMac8rUNtQ" role="1B3o_S" />
    <node concept="3clFb_" id="4QUW3efWXMP" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestReferenceMissingTestError" />
      <node concept="3clFbS" id="4QUW3efWXMS" role="3clF47" />
      <node concept="3Tm1VV" id="4QUW3efWXMT" role="1B3o_S" />
      <node concept="17QB3L" id="4QUW3efWXMG" role="3clF45" />
    </node>
    <node concept="3clFb_" id="6LTgXmNfILC" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getSpecimentTypeReferenceMissingTestError" />
      <node concept="3clFbS" id="6LTgXmNfILD" role="3clF47" />
      <node concept="3Tm1VV" id="6LTgXmNfILE" role="1B3o_S" />
      <node concept="17QB3L" id="6LTgXmNfILF" role="3clF45" />
    </node>
  </node>
  <node concept="312cEu" id="4zMac8rUNsN">
    <property role="TrG5h" value="LaboratoryTestReferenceTranslationProvider" />
    <property role="3GE5qa" value="internationalization" />
    <node concept="Wx3nA" id="4zMac8rVBeK" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="TrG5h" value="displayTranslations" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm1VV" id="4zMac8rVBei" role="1B3o_S" />
      <node concept="3uibUv" id="4zMac8rVBeC" role="1tU5fm">
        <ref role="3uigEE" node="4zMac8rUNtP" resolve="ILaboratoryTestReferenceTranslations" />
      </node>
      <node concept="1rXfSq" id="4zMac8rVBfh" role="33vP2m">
        <ref role="37wK5l" node="4zMac8rUNuE" resolve="init" />
        <node concept="10M0yZ" id="ofcHXfWil$" role="37wK5m">
          <ref role="3cqZAo" to="9igz:5fXsrCVmzVm" resolve="displayLanguage" />
          <ref role="1PxDUh" to="9igz:5fXsrCVmxTN" resolve="TranslationConfiguration" />
        </node>
      </node>
    </node>
    <node concept="Wx3nA" id="1Hxyv4DUmfq" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="TrG5h" value="generationTranslations" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm1VV" id="1Hxyv4DUmfr" role="1B3o_S" />
      <node concept="3uibUv" id="1Hxyv4DUmfs" role="1tU5fm">
        <ref role="3uigEE" node="4zMac8rUNtP" resolve="ILaboratoryTestReferenceTranslations" />
      </node>
      <node concept="1rXfSq" id="1Hxyv4DUmft" role="33vP2m">
        <ref role="37wK5l" node="4zMac8rUNuE" resolve="init" />
        <node concept="10M0yZ" id="ofcHXfWimD" role="37wK5m">
          <ref role="3cqZAo" to="9igz:5fXsrCVmzVN" resolve="generationLanguage" />
          <ref role="1PxDUh" to="9igz:5fXsrCVmxTN" resolve="TranslationConfiguration" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="4zMac8rUNtp" role="jymVt" />
    <node concept="2YIFZL" id="4zMac8rUNuE" role="jymVt">
      <property role="TrG5h" value="init" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="4zMac8rUNuH" role="3clF47">
        <node concept="3KaCP$" id="4zMac8rUNv7" role="3cqZAp">
          <node concept="3KbdKl" id="4zMac8rUNXu" role="3KbHQx">
            <node concept="Xl_RD" id="4zMac8rUNXU" role="3Kbmr1">
              <property role="Xl_RC" value="no" />
            </node>
            <node concept="3clFbS" id="4zMac8rUNXw" role="3Kbo56">
              <node concept="3cpWs6" id="4zMac8rUNYt" role="3cqZAp">
                <node concept="2ShNRf" id="4zMac8rUO3y" role="3cqZAk">
                  <node concept="HV5vD" id="4zMac8rUP5z" role="2ShVmc">
                    <ref role="HV5vE" node="4zMac8rUO1O" resolve="NoLaboratoryTestReferenceTranslations" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="4zMac8rUNYX" role="3Kb1Dw">
            <node concept="3cpWs6" id="4zMac8rUNZC" role="3cqZAp">
              <node concept="2ShNRf" id="4zMac8rUP6U" role="3cqZAk">
                <node concept="HV5vD" id="4zMac8rUPap" role="2ShVmc">
                  <ref role="HV5vE" node="4zMac8rUO0_" resolve="EnLaboratoryTestReferenceTranslations" />
                </node>
              </node>
            </node>
          </node>
          <node concept="37vLTw" id="1Hxyv4DUmde" role="3KbGdf">
            <ref role="3cqZAo" node="1Hxyv4DUm7P" resolve="language" />
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="4zMac8rW809" role="1B3o_S" />
      <node concept="3uibUv" id="4zMac8rUNuz" role="3clF45">
        <ref role="3uigEE" node="4zMac8rUNtP" resolve="ILaboratoryTestReferenceTranslations" />
      </node>
      <node concept="37vLTG" id="1Hxyv4DUm7P" role="3clF46">
        <property role="TrG5h" value="language" />
        <node concept="17QB3L" id="1Hxyv4DUm7O" role="1tU5fm" />
      </node>
    </node>
    <node concept="3Tm1VV" id="4zMac8rUNsO" role="1B3o_S" />
  </node>
  <node concept="312cEu" id="4zMac8rUO0_">
    <property role="TrG5h" value="EnLaboratoryTestReferenceTranslations" />
    <property role="3GE5qa" value="localization" />
    <node concept="3Tm1VV" id="4zMac8rUO0A" role="1B3o_S" />
    <node concept="3uibUv" id="4zMac8rUO1F" role="EKbjA">
      <ref role="3uigEE" node="4zMac8rUNtP" resolve="ILaboratoryTestReferenceTranslations" />
    </node>
    <node concept="3clFb_" id="4QUW3efWXNt" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestReferenceMissingTestError" />
      <node concept="3Tm1VV" id="4QUW3efWXNv" role="1B3o_S" />
      <node concept="17QB3L" id="4QUW3efWXNw" role="3clF45" />
      <node concept="3clFbS" id="4QUW3efWXNx" role="3clF47">
        <node concept="3clFbF" id="4QUW3efWXPa" role="3cqZAp">
          <node concept="Xl_RD" id="4QUW3efWXP9" role="3clFbG">
            <property role="Xl_RC" value="missing test" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4QUW3efWXNy" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="6LTgXmNfIT3" role="jymVt" />
    <node concept="3clFb_" id="6LTgXmNfIQ7" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getSpecimentTypeReferenceMissingTestError" />
      <node concept="3Tm1VV" id="6LTgXmNfIQ9" role="1B3o_S" />
      <node concept="17QB3L" id="6LTgXmNfIQa" role="3clF45" />
      <node concept="3clFbS" id="6LTgXmNfIQb" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNfIVH" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNfIVG" role="3clFbG">
            <property role="Xl_RC" value="missing specimen type" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6LTgXmNfIQc" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="4zMac8rUO1O">
    <property role="TrG5h" value="NoLaboratoryTestReferenceTranslations" />
    <property role="3GE5qa" value="localization" />
    <node concept="3Tm1VV" id="4zMac8rUO1P" role="1B3o_S" />
    <node concept="3uibUv" id="4zMac8rUO2g" role="EKbjA">
      <ref role="3uigEE" node="4zMac8rUNtP" resolve="ILaboratoryTestReferenceTranslations" />
    </node>
    <node concept="3clFb_" id="4QUW3efWXQu" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestReferenceMissingTestError" />
      <node concept="3Tm1VV" id="4QUW3efWXQw" role="1B3o_S" />
      <node concept="17QB3L" id="4QUW3efWXQx" role="3clF45" />
      <node concept="3clFbS" id="4QUW3efWXQy" role="3clF47">
        <node concept="3clFbF" id="4QUW3efWXSb" role="3cqZAp">
          <node concept="Xl_RD" id="4QUW3efWXSa" role="3clFbG">
            <property role="Xl_RC" value="mangler analyse" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="4QUW3efWXQz" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="6LTgXmNfIYq" role="jymVt" />
    <node concept="3clFb_" id="6LTgXmNfIZQ" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getSpecimentTypeReferenceMissingTestError" />
      <node concept="3Tm1VV" id="6LTgXmNfIZS" role="1B3o_S" />
      <node concept="17QB3L" id="6LTgXmNfIZT" role="3clF45" />
      <node concept="3clFbS" id="6LTgXmNfIZU" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNfJ32" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNfJ31" role="3clFbG">
            <property role="Xl_RC" value="mangler materiale" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6LTgXmNfIZV" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
  </node>
</model>

