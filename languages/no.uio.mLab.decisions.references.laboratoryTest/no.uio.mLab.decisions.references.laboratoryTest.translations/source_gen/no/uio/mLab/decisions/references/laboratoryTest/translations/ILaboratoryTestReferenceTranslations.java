package no.uio.mLab.decisions.references.laboratoryTest.translations;

/*Generated by MPS */


public interface ILaboratoryTestReferenceTranslations {
  String getLaboratoryTestReferenceMissingTestError();
  String getSpecimentTypeReferenceMissingTestError();
}
