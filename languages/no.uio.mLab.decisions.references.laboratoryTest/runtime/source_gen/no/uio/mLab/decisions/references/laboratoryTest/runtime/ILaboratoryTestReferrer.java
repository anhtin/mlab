package no.uio.mLab.decisions.references.laboratoryTest.runtime;

/*Generated by MPS */


public interface ILaboratoryTestReferrer {
  String getLaboratoryTestName();
}
