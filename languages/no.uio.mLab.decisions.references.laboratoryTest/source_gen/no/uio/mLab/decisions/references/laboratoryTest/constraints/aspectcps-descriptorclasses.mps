<?xml version="1.0" encoding="UTF-8"?>
<model ref="00000000-0000-4000-5f02-5beb5f025beb/i:f95fe30(checkpoints/no.uio.mLab.decisions.references.laboratoryTest.constraints@descriptorclasses)">
  <persistence version="9" />
  <attribute name="checkpoint" value="DescriptorClasses" />
  <attribute name="generation-plan" value="AspectCPS" />
  <languages />
  <imports>
    <import index="dehz" ref="r:e4a7a6f9-a1de-4f08-9b98-b5f2b9ecf3a4(no.uio.mLab.decisions.references.laboratoryTest.constraints)" />
    <import index="c17a" ref="8865b7a8-5271-43d3-884c-6fd1d9cfdd34/java:org.jetbrains.mps.openapi.language(MPS.OpenAPI/)" />
    <import index="2k9e" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.smodel.adapter.structure(MPS.Core/)" />
    <import index="ze1i" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.smodel.runtime(MPS.Core/)" />
    <import index="e8bb" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.smodel.adapter.ids(MPS.Core/)" />
    <import index="33ny" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.util(JDK/)" />
    <import index="79pl" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.smodel.runtime.base(MPS.Core/)" />
    <import index="ruww" ref="r:d7787915-5b9a-4713-a6ab-6345c5e1ec03(no.uio.mLab.decisions.references.laboratoryTest.structure)" />
    <import index="35tq" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.scope(MPS.Core/)" />
    <import index="mhfm" ref="3f233e7f-b8a6-46d2-a57f-795d56775243/java:org.jetbrains.annotations(Annotations/)" />
    <import index="mhbf" ref="8865b7a8-5271-43d3-884c-6fd1d9cfdd34/java:org.jetbrains.mps.openapi.model(MPS.OpenAPI/)" />
    <import index="w1kc" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.smodel(MPS.Core/)" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1082485599095" name="jetbrains.mps.baseLanguage.structure.BlockStatement" flags="nn" index="9aQIb">
        <child id="1082485599096" name="statements" index="9aQI4" />
      </concept>
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1465982738277781862" name="jetbrains.mps.baseLanguage.structure.PlaceholderMember" flags="ng" index="2tJIrI" />
      <concept id="1188207840427" name="jetbrains.mps.baseLanguage.structure.AnnotationInstance" flags="nn" index="2AHcQZ">
        <reference id="1188208074048" name="annotation" index="2AI5Lk" />
      </concept>
      <concept id="1188208481402" name="jetbrains.mps.baseLanguage.structure.HasAnnotation" flags="ng" index="2AJDlI">
        <child id="1188208488637" name="annotation" index="2AJF6D" />
      </concept>
      <concept id="1224848483129" name="jetbrains.mps.baseLanguage.structure.IBLDeprecatable" flags="ng" index="IEa8$">
        <property id="1224848525476" name="isDeprecated" index="IEkAT" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1070475354124" name="jetbrains.mps.baseLanguage.structure.ThisExpression" flags="nn" index="Xjq3P" />
      <concept id="1070475587102" name="jetbrains.mps.baseLanguage.structure.SuperConstructorInvocation" flags="nn" index="XkiVB" />
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1182160077978" name="jetbrains.mps.baseLanguage.structure.AnonymousClassCreator" flags="nn" index="YeOm9">
        <child id="1182160096073" name="cls" index="YeSDq" />
      </concept>
      <concept id="1081236700938" name="jetbrains.mps.baseLanguage.structure.StaticMethodDeclaration" flags="ig" index="2YIFZL" />
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1070534058343" name="jetbrains.mps.baseLanguage.structure.NullLiteral" flags="nn" index="10Nm6u" />
      <concept id="1070534644030" name="jetbrains.mps.baseLanguage.structure.BooleanType" flags="in" index="10P_77" />
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu">
        <child id="1165602531693" name="superclass" index="1zkMxy" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <property id="1176718929932" name="isFinal" index="3TUv4t" />
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="4269842503726207156" name="jetbrains.mps.baseLanguage.structure.LongLiteral" flags="nn" index="1adDum">
        <property id="4269842503726207157" name="value" index="1adDun" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <property id="4276006055363816570" name="isSynchronized" index="od$2w" />
        <property id="1181808852946" name="isFinal" index="DiZV1" />
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123165" name="jetbrains.mps.baseLanguage.structure.InstanceMethodDeclaration" flags="ig" index="3clFb_">
        <property id="1178608670077" name="isAbstract" index="1EzhhJ" />
      </concept>
      <concept id="1068580123152" name="jetbrains.mps.baseLanguage.structure.EqualsExpression" flags="nn" index="3clFbC" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123137" name="jetbrains.mps.baseLanguage.structure.BooleanConstant" flags="nn" index="3clFbT">
        <property id="1068580123138" name="value" index="3clFbU" />
      </concept>
      <concept id="1068580123140" name="jetbrains.mps.baseLanguage.structure.ConstructorDeclaration" flags="ig" index="3clFbW" />
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1068581517677" name="jetbrains.mps.baseLanguage.structure.VoidType" flags="in" index="3cqZAl" />
      <concept id="1081516740877" name="jetbrains.mps.baseLanguage.structure.NotExpression" flags="nn" index="3fqX7Q">
        <child id="1081516765348" name="expression" index="3fr31v" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1212685548494" name="jetbrains.mps.baseLanguage.structure.ClassCreator" flags="nn" index="1pGfFk">
        <child id="1212687122400" name="typeParameter" index="1pMfVU" />
      </concept>
      <concept id="1107461130800" name="jetbrains.mps.baseLanguage.structure.Classifier" flags="ng" index="3pOWGL">
        <property id="521412098689998745" name="nonStatic" index="2bfB8j" />
        <child id="5375687026011219971" name="member" index="jymVt" unordered="true" />
      </concept>
      <concept id="7812454656619025412" name="jetbrains.mps.baseLanguage.structure.LocalMethodCall" flags="nn" index="1rXfSq" />
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
        <child id="1109201940907" name="parameter" index="11_B2D" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1073239437375" name="jetbrains.mps.baseLanguage.structure.NotEqualsExpression" flags="nn" index="3y3z36" />
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1163668896201" name="jetbrains.mps.baseLanguage.structure.TernaryOperatorExpression" flags="nn" index="3K4zz7">
        <child id="1163668914799" name="condition" index="3K4Cdx" />
        <child id="1163668922816" name="ifTrue" index="3K4E3e" />
        <child id="1163668934364" name="ifFalse" index="3K4GZi" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
      <concept id="1146644623116" name="jetbrains.mps.baseLanguage.structure.PrivateVisibility" flags="nn" index="3Tm6S6" />
      <concept id="1146644641414" name="jetbrains.mps.baseLanguage.structure.ProtectedVisibility" flags="nn" index="3Tmbuc" />
      <concept id="1080120340718" name="jetbrains.mps.baseLanguage.structure.AndExpression" flags="nn" index="1Wc70l" />
      <concept id="1170345865475" name="jetbrains.mps.baseLanguage.structure.AnonymousClass" flags="ig" index="1Y3b0j">
        <reference id="1170346070688" name="classifier" index="1Y3XeK" />
        <child id="1201186121363" name="typeParameter" index="2Ghqu4" />
      </concept>
    </language>
    <language id="b401a680-8325-4110-8fd3-84331ff25bef" name="jetbrains.mps.lang.generator">
      <concept id="7980339663309897032" name="jetbrains.mps.lang.generator.structure.OriginTrace" flags="ng" index="cd27G">
        <child id="7980339663309897037" name="origin" index="cd27D" />
      </concept>
      <concept id="3864140621129707969" name="jetbrains.mps.lang.generator.structure.GeneratorDebug_Mappings" flags="nn" index="39dXUE" />
      <concept id="3637169702552512264" name="jetbrains.mps.lang.generator.structure.ElementaryNodeId" flags="ng" index="3u3nmq">
        <property id="3637169702552512269" name="nodeId" index="3u3nmv" />
      </concept>
    </language>
    <language id="df345b11-b8c7-4213-ac66-48d2a9b75d88" name="jetbrains.mps.baseLanguageInternal">
      <concept id="1238251434034" name="jetbrains.mps.baseLanguageInternal.structure.ExtractToConstantExpression" flags="ng" index="1dyn4i">
        <property id="1238251449050" name="fieldName" index="1dyqJU" />
        <property id="8835849473318867199" name="makeUnique" index="1zomUR" />
        <child id="1238251454130" name="expression" index="1dyrYi" />
      </concept>
      <concept id="1173996401517" name="jetbrains.mps.baseLanguageInternal.structure.InternalNewExpression" flags="nn" index="1nCR9W">
        <property id="1173996588177" name="fqClassName" index="1nD$Q0" />
        <child id="1179332974947" name="type" index="2lIhxL" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="2644386474300074836" name="jetbrains.mps.lang.smodel.structure.ConceptIdRefExpression" flags="nn" index="35c_gC">
        <reference id="2644386474300074837" name="conceptDeclaration" index="35c_gD" />
      </concept>
      <concept id="6677504323281689838" name="jetbrains.mps.lang.smodel.structure.SConceptType" flags="in" index="3bZ5Sz" />
      <concept id="1154546950173" name="jetbrains.mps.lang.smodel.structure.ConceptReference" flags="ng" index="3gn64h">
        <reference id="1154546997487" name="concept" index="3gnhBz" />
      </concept>
      <concept id="1139621453865" name="jetbrains.mps.lang.smodel.structure.Node_IsInstanceOfOperation" flags="nn" index="1mIQ4w">
        <child id="1177027386292" name="conceptArgument" index="cj9EA" />
      </concept>
      <concept id="6039268229364358244" name="jetbrains.mps.lang.smodel.structure.ExactConceptCase" flags="ng" index="1pnPoh">
        <child id="6039268229364358388" name="body" index="1pnPq1" />
        <child id="6039268229364358387" name="concept" index="1pnPq6" />
      </concept>
      <concept id="5944356402132808749" name="jetbrains.mps.lang.smodel.structure.ConceptSwitchStatement" flags="nn" index="1_3QMa">
        <child id="6039268229365417680" name="defaultBlock" index="1prKM_" />
        <child id="5944356402132808753" name="case" index="1_3QMm" />
        <child id="5944356402132808752" name="expression" index="1_3QMn" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="312cEu" id="0">
    <property role="3GE5qa" value="aspect.references.laboratoryTest" />
    <property role="TrG5h" value="AspectLaboratoryTestReference_Constraints" />
    <node concept="3Tm1VV" id="1" role="1B3o_S">
      <node concept="cd27G" id="7" role="lGtFl">
        <node concept="3u3nmq" id="8" role="cd27D">
          <property role="3u3nmv" value="5675576922574855628" />
        </node>
      </node>
    </node>
    <node concept="3uibUv" id="2" role="1zkMxy">
      <ref role="3uigEE" to="79pl:~BaseConstraintsDescriptor" resolve="BaseConstraintsDescriptor" />
      <node concept="cd27G" id="9" role="lGtFl">
        <node concept="3u3nmq" id="a" role="cd27D">
          <property role="3u3nmv" value="5675576922574855628" />
        </node>
      </node>
    </node>
    <node concept="3clFbW" id="3" role="jymVt">
      <node concept="3cqZAl" id="b" role="3clF45">
        <node concept="cd27G" id="f" role="lGtFl">
          <node concept="3u3nmq" id="g" role="cd27D">
            <property role="3u3nmv" value="5675576922574855628" />
          </node>
        </node>
      </node>
      <node concept="3clFbS" id="c" role="3clF47">
        <node concept="XkiVB" id="h" role="3cqZAp">
          <ref role="37wK5l" to="79pl:~BaseConstraintsDescriptor.&lt;init&gt;(org.jetbrains.mps.openapi.language.SAbstractConcept)" resolve="BaseConstraintsDescriptor" />
          <node concept="2YIFZM" id="j" role="37wK5m">
            <ref role="1Pybhc" to="2k9e:~MetaAdapterFactory" resolve="MetaAdapterFactory" />
            <ref role="37wK5l" to="2k9e:~MetaAdapterFactory.getConcept(long,long,long,java.lang.String):org.jetbrains.mps.openapi.language.SConcept" resolve="getConcept" />
            <node concept="1adDum" id="l" role="37wK5m">
              <property role="1adDun" value="0x4a652d5536844d2dL" />
              <node concept="cd27G" id="q" role="lGtFl">
                <node concept="3u3nmq" id="r" role="cd27D">
                  <property role="3u3nmv" value="5675576922574855628" />
                </node>
              </node>
            </node>
            <node concept="1adDum" id="m" role="37wK5m">
              <property role="1adDun" value="0x98c92ef46f124c44L" />
              <node concept="cd27G" id="s" role="lGtFl">
                <node concept="3u3nmq" id="t" role="cd27D">
                  <property role="3u3nmv" value="5675576922574855628" />
                </node>
              </node>
            </node>
            <node concept="1adDum" id="n" role="37wK5m">
              <property role="1adDun" value="0x4dbaf0338f8270caL" />
              <node concept="cd27G" id="u" role="lGtFl">
                <node concept="3u3nmq" id="v" role="cd27D">
                  <property role="3u3nmv" value="5675576922574855628" />
                </node>
              </node>
            </node>
            <node concept="Xl_RD" id="o" role="37wK5m">
              <property role="Xl_RC" value="no.uio.mLab.decisions.references.laboratoryTest.structure.AspectLaboratoryTestReference" />
              <node concept="cd27G" id="w" role="lGtFl">
                <node concept="3u3nmq" id="x" role="cd27D">
                  <property role="3u3nmv" value="5675576922574855628" />
                </node>
              </node>
            </node>
            <node concept="cd27G" id="p" role="lGtFl">
              <node concept="3u3nmq" id="y" role="cd27D">
                <property role="3u3nmv" value="5675576922574855628" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="k" role="lGtFl">
            <node concept="3u3nmq" id="z" role="cd27D">
              <property role="3u3nmv" value="5675576922574855628" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="i" role="lGtFl">
          <node concept="3u3nmq" id="$" role="cd27D">
            <property role="3u3nmv" value="5675576922574855628" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="d" role="1B3o_S">
        <node concept="cd27G" id="_" role="lGtFl">
          <node concept="3u3nmq" id="A" role="cd27D">
            <property role="3u3nmv" value="5675576922574855628" />
          </node>
        </node>
      </node>
      <node concept="cd27G" id="e" role="lGtFl">
        <node concept="3u3nmq" id="B" role="cd27D">
          <property role="3u3nmv" value="5675576922574855628" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="4" role="jymVt">
      <node concept="cd27G" id="C" role="lGtFl">
        <node concept="3u3nmq" id="D" role="cd27D">
          <property role="3u3nmv" value="5675576922574855628" />
        </node>
      </node>
    </node>
    <node concept="3clFb_" id="5" role="jymVt">
      <property role="IEkAT" value="false" />
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="getSpecifiedReferences" />
      <property role="DiZV1" value="false" />
      <node concept="3Tmbuc" id="E" role="1B3o_S">
        <node concept="cd27G" id="J" role="lGtFl">
          <node concept="3u3nmq" id="K" role="cd27D">
            <property role="3u3nmv" value="5675576922574855628" />
          </node>
        </node>
      </node>
      <node concept="3uibUv" id="F" role="3clF45">
        <ref role="3uigEE" to="33ny:~Map" resolve="Map" />
        <node concept="3uibUv" id="L" role="11_B2D">
          <ref role="3uigEE" to="c17a:~SReferenceLink" resolve="SReferenceLink" />
          <node concept="cd27G" id="O" role="lGtFl">
            <node concept="3u3nmq" id="P" role="cd27D">
              <property role="3u3nmv" value="5675576922574855628" />
            </node>
          </node>
        </node>
        <node concept="3uibUv" id="M" role="11_B2D">
          <ref role="3uigEE" to="ze1i:~ReferenceConstraintsDescriptor" resolve="ReferenceConstraintsDescriptor" />
          <node concept="cd27G" id="Q" role="lGtFl">
            <node concept="3u3nmq" id="R" role="cd27D">
              <property role="3u3nmv" value="5675576922574855628" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="N" role="lGtFl">
          <node concept="3u3nmq" id="S" role="cd27D">
            <property role="3u3nmv" value="5675576922574855628" />
          </node>
        </node>
      </node>
      <node concept="3clFbS" id="G" role="3clF47">
        <node concept="3cpWs8" id="T" role="3cqZAp">
          <node concept="3cpWsn" id="X" role="3cpWs9">
            <property role="TrG5h" value="references" />
            <node concept="3uibUv" id="Z" role="1tU5fm">
              <ref role="3uigEE" to="33ny:~Map" resolve="Map" />
              <node concept="3uibUv" id="12" role="11_B2D">
                <ref role="3uigEE" to="c17a:~SReferenceLink" resolve="SReferenceLink" />
                <node concept="cd27G" id="15" role="lGtFl">
                  <node concept="3u3nmq" id="16" role="cd27D">
                    <property role="3u3nmv" value="5675576922574855628" />
                  </node>
                </node>
              </node>
              <node concept="3uibUv" id="13" role="11_B2D">
                <ref role="3uigEE" to="ze1i:~ReferenceConstraintsDescriptor" resolve="ReferenceConstraintsDescriptor" />
                <node concept="cd27G" id="17" role="lGtFl">
                  <node concept="3u3nmq" id="18" role="cd27D">
                    <property role="3u3nmv" value="5675576922574855628" />
                  </node>
                </node>
              </node>
              <node concept="cd27G" id="14" role="lGtFl">
                <node concept="3u3nmq" id="19" role="cd27D">
                  <property role="3u3nmv" value="5675576922574855628" />
                </node>
              </node>
            </node>
            <node concept="2ShNRf" id="10" role="33vP2m">
              <node concept="1pGfFk" id="1a" role="2ShVmc">
                <ref role="37wK5l" to="33ny:~HashMap.&lt;init&gt;()" resolve="HashMap" />
                <node concept="3uibUv" id="1c" role="1pMfVU">
                  <ref role="3uigEE" to="c17a:~SReferenceLink" resolve="SReferenceLink" />
                  <node concept="cd27G" id="1f" role="lGtFl">
                    <node concept="3u3nmq" id="1g" role="cd27D">
                      <property role="3u3nmv" value="5675576922574855628" />
                    </node>
                  </node>
                </node>
                <node concept="3uibUv" id="1d" role="1pMfVU">
                  <ref role="3uigEE" to="ze1i:~ReferenceConstraintsDescriptor" resolve="ReferenceConstraintsDescriptor" />
                  <node concept="cd27G" id="1h" role="lGtFl">
                    <node concept="3u3nmq" id="1i" role="cd27D">
                      <property role="3u3nmv" value="5675576922574855628" />
                    </node>
                  </node>
                </node>
                <node concept="cd27G" id="1e" role="lGtFl">
                  <node concept="3u3nmq" id="1j" role="cd27D">
                    <property role="3u3nmv" value="5675576922574855628" />
                  </node>
                </node>
              </node>
              <node concept="cd27G" id="1b" role="lGtFl">
                <node concept="3u3nmq" id="1k" role="cd27D">
                  <property role="3u3nmv" value="5675576922574855628" />
                </node>
              </node>
            </node>
            <node concept="cd27G" id="11" role="lGtFl">
              <node concept="3u3nmq" id="1l" role="cd27D">
                <property role="3u3nmv" value="5675576922574855628" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="Y" role="lGtFl">
            <node concept="3u3nmq" id="1m" role="cd27D">
              <property role="3u3nmv" value="5675576922574855628" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="U" role="3cqZAp">
          <node concept="2OqwBi" id="1n" role="3clFbG">
            <node concept="37vLTw" id="1p" role="2Oq$k0">
              <ref role="3cqZAo" node="X" resolve="references" />
              <node concept="cd27G" id="1s" role="lGtFl">
                <node concept="3u3nmq" id="1t" role="cd27D">
                  <property role="3u3nmv" value="5675576922574855628" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="1q" role="2OqNvi">
              <ref role="37wK5l" to="33ny:~Map.put(java.lang.Object,java.lang.Object):java.lang.Object" resolve="put" />
              <node concept="2YIFZM" id="1u" role="37wK5m">
                <ref role="1Pybhc" to="2k9e:~MetaAdapterFactory" resolve="MetaAdapterFactory" />
                <ref role="37wK5l" to="2k9e:~MetaAdapterFactory.getReferenceLink(long,long,long,long,java.lang.String):org.jetbrains.mps.openapi.language.SReferenceLink" resolve="getReferenceLink" />
                <node concept="1adDum" id="1x" role="37wK5m">
                  <property role="1adDun" value="0xc610a4ebc47c4f95L" />
                  <node concept="cd27G" id="1B" role="lGtFl">
                    <node concept="3u3nmq" id="1C" role="cd27D">
                      <property role="3u3nmv" value="5675576922574855628" />
                    </node>
                  </node>
                </node>
                <node concept="1adDum" id="1y" role="37wK5m">
                  <property role="1adDun" value="0xb56811236971769cL" />
                  <node concept="cd27G" id="1D" role="lGtFl">
                    <node concept="3u3nmq" id="1E" role="cd27D">
                      <property role="3u3nmv" value="5675576922574855628" />
                    </node>
                  </node>
                </node>
                <node concept="1adDum" id="1z" role="37wK5m">
                  <property role="1adDun" value="0x6c7943d5b299791dL" />
                  <node concept="cd27G" id="1F" role="lGtFl">
                    <node concept="3u3nmq" id="1G" role="cd27D">
                      <property role="3u3nmv" value="5675576922574855628" />
                    </node>
                  </node>
                </node>
                <node concept="1adDum" id="1$" role="37wK5m">
                  <property role="1adDun" value="0x6c7943d5b29b8063L" />
                  <node concept="cd27G" id="1H" role="lGtFl">
                    <node concept="3u3nmq" id="1I" role="cd27D">
                      <property role="3u3nmv" value="5675576922574855628" />
                    </node>
                  </node>
                </node>
                <node concept="Xl_RD" id="1_" role="37wK5m">
                  <property role="Xl_RC" value="target" />
                  <node concept="cd27G" id="1J" role="lGtFl">
                    <node concept="3u3nmq" id="1K" role="cd27D">
                      <property role="3u3nmv" value="5675576922574855628" />
                    </node>
                  </node>
                </node>
                <node concept="cd27G" id="1A" role="lGtFl">
                  <node concept="3u3nmq" id="1L" role="cd27D">
                    <property role="3u3nmv" value="5675576922574855628" />
                  </node>
                </node>
              </node>
              <node concept="2ShNRf" id="1v" role="37wK5m">
                <node concept="YeOm9" id="1M" role="2ShVmc">
                  <node concept="1Y3b0j" id="1O" role="YeSDq">
                    <property role="2bfB8j" value="true" />
                    <ref role="1Y3XeK" to="79pl:~BaseReferenceConstraintsDescriptor" resolve="BaseReferenceConstraintsDescriptor" />
                    <ref role="37wK5l" to="79pl:~BaseReferenceConstraintsDescriptor.&lt;init&gt;(jetbrains.mps.smodel.adapter.ids.SReferenceLinkId,jetbrains.mps.smodel.runtime.ConstraintsDescriptor)" resolve="BaseReferenceConstraintsDescriptor" />
                    <node concept="2YIFZM" id="1Q" role="37wK5m">
                      <ref role="1Pybhc" to="e8bb:~MetaIdFactory" resolve="MetaIdFactory" />
                      <ref role="37wK5l" to="e8bb:~MetaIdFactory.refId(long,long,long,long):jetbrains.mps.smodel.adapter.ids.SReferenceLinkId" resolve="refId" />
                      <node concept="1adDum" id="1W" role="37wK5m">
                        <property role="1adDun" value="0xc610a4ebc47c4f95L" />
                        <node concept="cd27G" id="21" role="lGtFl">
                          <node concept="3u3nmq" id="22" role="cd27D">
                            <property role="3u3nmv" value="5675576922574855628" />
                          </node>
                        </node>
                      </node>
                      <node concept="1adDum" id="1X" role="37wK5m">
                        <property role="1adDun" value="0xb56811236971769cL" />
                        <node concept="cd27G" id="23" role="lGtFl">
                          <node concept="3u3nmq" id="24" role="cd27D">
                            <property role="3u3nmv" value="5675576922574855628" />
                          </node>
                        </node>
                      </node>
                      <node concept="1adDum" id="1Y" role="37wK5m">
                        <property role="1adDun" value="0x6c7943d5b299791dL" />
                        <node concept="cd27G" id="25" role="lGtFl">
                          <node concept="3u3nmq" id="26" role="cd27D">
                            <property role="3u3nmv" value="5675576922574855628" />
                          </node>
                        </node>
                      </node>
                      <node concept="1adDum" id="1Z" role="37wK5m">
                        <property role="1adDun" value="0x6c7943d5b29b8063L" />
                        <node concept="cd27G" id="27" role="lGtFl">
                          <node concept="3u3nmq" id="28" role="cd27D">
                            <property role="3u3nmv" value="5675576922574855628" />
                          </node>
                        </node>
                      </node>
                      <node concept="cd27G" id="20" role="lGtFl">
                        <node concept="3u3nmq" id="29" role="cd27D">
                          <property role="3u3nmv" value="5675576922574855628" />
                        </node>
                      </node>
                    </node>
                    <node concept="3Tm1VV" id="1R" role="1B3o_S">
                      <node concept="cd27G" id="2a" role="lGtFl">
                        <node concept="3u3nmq" id="2b" role="cd27D">
                          <property role="3u3nmv" value="5675576922574855628" />
                        </node>
                      </node>
                    </node>
                    <node concept="Xjq3P" id="1S" role="37wK5m">
                      <node concept="cd27G" id="2c" role="lGtFl">
                        <node concept="3u3nmq" id="2d" role="cd27D">
                          <property role="3u3nmv" value="5675576922574855628" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFb_" id="1T" role="jymVt">
                      <property role="IEkAT" value="false" />
                      <property role="1EzhhJ" value="false" />
                      <property role="TrG5h" value="hasOwnScopeProvider" />
                      <property role="DiZV1" value="false" />
                      <node concept="3Tm1VV" id="2e" role="1B3o_S">
                        <node concept="cd27G" id="2j" role="lGtFl">
                          <node concept="3u3nmq" id="2k" role="cd27D">
                            <property role="3u3nmv" value="5675576922574855628" />
                          </node>
                        </node>
                      </node>
                      <node concept="10P_77" id="2f" role="3clF45">
                        <node concept="cd27G" id="2l" role="lGtFl">
                          <node concept="3u3nmq" id="2m" role="cd27D">
                            <property role="3u3nmv" value="5675576922574855628" />
                          </node>
                        </node>
                      </node>
                      <node concept="3clFbS" id="2g" role="3clF47">
                        <node concept="3clFbF" id="2n" role="3cqZAp">
                          <node concept="3clFbT" id="2p" role="3clFbG">
                            <property role="3clFbU" value="true" />
                            <node concept="cd27G" id="2r" role="lGtFl">
                              <node concept="3u3nmq" id="2s" role="cd27D">
                                <property role="3u3nmv" value="5675576922574855628" />
                              </node>
                            </node>
                          </node>
                          <node concept="cd27G" id="2q" role="lGtFl">
                            <node concept="3u3nmq" id="2t" role="cd27D">
                              <property role="3u3nmv" value="5675576922574855628" />
                            </node>
                          </node>
                        </node>
                        <node concept="cd27G" id="2o" role="lGtFl">
                          <node concept="3u3nmq" id="2u" role="cd27D">
                            <property role="3u3nmv" value="5675576922574855628" />
                          </node>
                        </node>
                      </node>
                      <node concept="2AHcQZ" id="2h" role="2AJF6D">
                        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
                        <node concept="cd27G" id="2v" role="lGtFl">
                          <node concept="3u3nmq" id="2w" role="cd27D">
                            <property role="3u3nmv" value="5675576922574855628" />
                          </node>
                        </node>
                      </node>
                      <node concept="cd27G" id="2i" role="lGtFl">
                        <node concept="3u3nmq" id="2x" role="cd27D">
                          <property role="3u3nmv" value="5675576922574855628" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFb_" id="1U" role="jymVt">
                      <property role="IEkAT" value="false" />
                      <property role="1EzhhJ" value="false" />
                      <property role="TrG5h" value="getScopeProvider" />
                      <property role="DiZV1" value="false" />
                      <node concept="3Tm1VV" id="2y" role="1B3o_S">
                        <node concept="cd27G" id="2C" role="lGtFl">
                          <node concept="3u3nmq" id="2D" role="cd27D">
                            <property role="3u3nmv" value="5675576922574855628" />
                          </node>
                        </node>
                      </node>
                      <node concept="3uibUv" id="2z" role="3clF45">
                        <ref role="3uigEE" to="ze1i:~ReferenceScopeProvider" resolve="ReferenceScopeProvider" />
                        <node concept="cd27G" id="2E" role="lGtFl">
                          <node concept="3u3nmq" id="2F" role="cd27D">
                            <property role="3u3nmv" value="5675576922574855628" />
                          </node>
                        </node>
                      </node>
                      <node concept="2AHcQZ" id="2$" role="2AJF6D">
                        <ref role="2AI5Lk" to="mhfm:~Nullable" resolve="Nullable" />
                        <node concept="cd27G" id="2G" role="lGtFl">
                          <node concept="3u3nmq" id="2H" role="cd27D">
                            <property role="3u3nmv" value="5675576922574855628" />
                          </node>
                        </node>
                      </node>
                      <node concept="3clFbS" id="2_" role="3clF47">
                        <node concept="3cpWs6" id="2I" role="3cqZAp">
                          <node concept="2ShNRf" id="2K" role="3cqZAk">
                            <node concept="YeOm9" id="2M" role="2ShVmc">
                              <node concept="1Y3b0j" id="2O" role="YeSDq">
                                <property role="2bfB8j" value="true" />
                                <ref role="1Y3XeK" to="79pl:~BaseScopeProvider" resolve="BaseScopeProvider" />
                                <ref role="37wK5l" to="79pl:~BaseScopeProvider.&lt;init&gt;()" resolve="BaseScopeProvider" />
                                <node concept="3Tm1VV" id="2Q" role="1B3o_S">
                                  <node concept="cd27G" id="2U" role="lGtFl">
                                    <node concept="3u3nmq" id="2V" role="cd27D">
                                      <property role="3u3nmv" value="5675576922574855628" />
                                    </node>
                                  </node>
                                </node>
                                <node concept="3clFb_" id="2R" role="jymVt">
                                  <property role="TrG5h" value="getSearchScopeValidatorNode" />
                                  <node concept="3Tm1VV" id="2W" role="1B3o_S">
                                    <node concept="cd27G" id="31" role="lGtFl">
                                      <node concept="3u3nmq" id="32" role="cd27D">
                                        <property role="3u3nmv" value="5675576922574855628" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="3clFbS" id="2X" role="3clF47">
                                    <node concept="3cpWs6" id="33" role="3cqZAp">
                                      <node concept="1dyn4i" id="35" role="3cqZAk">
                                        <property role="1zomUR" value="true" />
                                        <property role="1dyqJU" value="breakingNode" />
                                        <node concept="2ShNRf" id="37" role="1dyrYi">
                                          <node concept="1pGfFk" id="39" role="2ShVmc">
                                            <ref role="37wK5l" to="w1kc:~SNodePointer.&lt;init&gt;(java.lang.String,java.lang.String)" resolve="SNodePointer" />
                                            <node concept="Xl_RD" id="3b" role="37wK5m">
                                              <property role="Xl_RC" value="r:e4a7a6f9-a1de-4f08-9b98-b5f2b9ecf3a4(no.uio.mLab.decisions.references.laboratoryTest.constraints)" />
                                              <node concept="cd27G" id="3e" role="lGtFl">
                                                <node concept="3u3nmq" id="3f" role="cd27D">
                                                  <property role="3u3nmv" value="5675576922574855628" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="Xl_RD" id="3c" role="37wK5m">
                                              <property role="Xl_RC" value="5675576922574855631" />
                                              <node concept="cd27G" id="3g" role="lGtFl">
                                                <node concept="3u3nmq" id="3h" role="cd27D">
                                                  <property role="3u3nmv" value="5675576922574855628" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="cd27G" id="3d" role="lGtFl">
                                              <node concept="3u3nmq" id="3i" role="cd27D">
                                                <property role="3u3nmv" value="5675576922574855628" />
                                              </node>
                                            </node>
                                          </node>
                                          <node concept="cd27G" id="3a" role="lGtFl">
                                            <node concept="3u3nmq" id="3j" role="cd27D">
                                              <property role="3u3nmv" value="5675576922574855628" />
                                            </node>
                                          </node>
                                        </node>
                                        <node concept="cd27G" id="38" role="lGtFl">
                                          <node concept="3u3nmq" id="3k" role="cd27D">
                                            <property role="3u3nmv" value="5675576922574855628" />
                                          </node>
                                        </node>
                                      </node>
                                      <node concept="cd27G" id="36" role="lGtFl">
                                        <node concept="3u3nmq" id="3l" role="cd27D">
                                          <property role="3u3nmv" value="5675576922574855628" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="cd27G" id="34" role="lGtFl">
                                      <node concept="3u3nmq" id="3m" role="cd27D">
                                        <property role="3u3nmv" value="5675576922574855628" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="3uibUv" id="2Y" role="3clF45">
                                    <ref role="3uigEE" to="mhbf:~SNodeReference" resolve="SNodeReference" />
                                    <node concept="cd27G" id="3n" role="lGtFl">
                                      <node concept="3u3nmq" id="3o" role="cd27D">
                                        <property role="3u3nmv" value="5675576922574855628" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="2AHcQZ" id="2Z" role="2AJF6D">
                                    <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
                                    <node concept="cd27G" id="3p" role="lGtFl">
                                      <node concept="3u3nmq" id="3q" role="cd27D">
                                        <property role="3u3nmv" value="5675576922574855628" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="cd27G" id="30" role="lGtFl">
                                    <node concept="3u3nmq" id="3r" role="cd27D">
                                      <property role="3u3nmv" value="5675576922574855628" />
                                    </node>
                                  </node>
                                </node>
                                <node concept="3clFb_" id="2S" role="jymVt">
                                  <property role="IEkAT" value="false" />
                                  <property role="1EzhhJ" value="false" />
                                  <property role="TrG5h" value="createScope" />
                                  <property role="DiZV1" value="false" />
                                  <node concept="37vLTG" id="3s" role="3clF46">
                                    <property role="TrG5h" value="operationContext" />
                                    <property role="3TUv4t" value="true" />
                                    <node concept="3uibUv" id="3z" role="1tU5fm">
                                      <ref role="3uigEE" to="w1kc:~IOperationContext" resolve="IOperationContext" />
                                      <node concept="cd27G" id="3_" role="lGtFl">
                                        <node concept="3u3nmq" id="3A" role="cd27D">
                                          <property role="3u3nmv" value="5675576922574855628" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="cd27G" id="3$" role="lGtFl">
                                      <node concept="3u3nmq" id="3B" role="cd27D">
                                        <property role="3u3nmv" value="5675576922574855628" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="37vLTG" id="3t" role="3clF46">
                                    <property role="TrG5h" value="_context" />
                                    <property role="3TUv4t" value="true" />
                                    <node concept="3uibUv" id="3C" role="1tU5fm">
                                      <ref role="3uigEE" to="ze1i:~ReferenceConstraintsContext" resolve="ReferenceConstraintsContext" />
                                      <node concept="cd27G" id="3E" role="lGtFl">
                                        <node concept="3u3nmq" id="3F" role="cd27D">
                                          <property role="3u3nmv" value="5675576922574855628" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="cd27G" id="3D" role="lGtFl">
                                      <node concept="3u3nmq" id="3G" role="cd27D">
                                        <property role="3u3nmv" value="5675576922574855628" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="3Tm1VV" id="3u" role="1B3o_S">
                                    <node concept="cd27G" id="3H" role="lGtFl">
                                      <node concept="3u3nmq" id="3I" role="cd27D">
                                        <property role="3u3nmv" value="5675576922574855628" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="3uibUv" id="3v" role="3clF45">
                                    <ref role="3uigEE" to="35tq:~Scope" resolve="Scope" />
                                    <node concept="cd27G" id="3J" role="lGtFl">
                                      <node concept="3u3nmq" id="3K" role="cd27D">
                                        <property role="3u3nmv" value="5675576922574855628" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="3clFbS" id="3w" role="3clF47">
                                    <node concept="9aQIb" id="3L" role="3cqZAp">
                                      <node concept="3clFbS" id="3N" role="9aQI4">
                                        <node concept="3cpWs8" id="3P" role="3cqZAp">
                                          <node concept="3cpWsn" id="3S" role="3cpWs9">
                                            <property role="TrG5h" value="scope" />
                                            <node concept="3uibUv" id="3U" role="1tU5fm">
                                              <ref role="3uigEE" to="35tq:~Scope" resolve="Scope" />
                                              <node concept="cd27G" id="3X" role="lGtFl">
                                                <node concept="3u3nmq" id="3Y" role="cd27D">
                                                  <property role="3u3nmv" value="5675576922574855628" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="2YIFZM" id="3V" role="33vP2m">
                                              <ref role="1Pybhc" to="35tq:~Scope" resolve="Scope" />
                                              <ref role="37wK5l" to="35tq:~Scope.getScope(org.jetbrains.mps.openapi.model.SNode,org.jetbrains.mps.openapi.language.SContainmentLink,int,org.jetbrains.mps.openapi.language.SAbstractConcept):jetbrains.mps.scope.Scope" resolve="getScope" />
                                              <node concept="2OqwBi" id="3Z" role="37wK5m">
                                                <node concept="37vLTw" id="44" role="2Oq$k0">
                                                  <ref role="3cqZAo" node="3t" resolve="_context" />
                                                  <node concept="cd27G" id="47" role="lGtFl">
                                                    <node concept="3u3nmq" id="48" role="cd27D">
                                                      <property role="3u3nmv" value="5675576922574855628" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="liA8E" id="45" role="2OqNvi">
                                                  <ref role="37wK5l" to="ze1i:~ReferenceConstraintsContext.getContextNode():org.jetbrains.mps.openapi.model.SNode" resolve="getContextNode" />
                                                  <node concept="cd27G" id="49" role="lGtFl">
                                                    <node concept="3u3nmq" id="4a" role="cd27D">
                                                      <property role="3u3nmv" value="5675576922574855628" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="cd27G" id="46" role="lGtFl">
                                                  <node concept="3u3nmq" id="4b" role="cd27D">
                                                    <property role="3u3nmv" value="5675576922574855628" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="2OqwBi" id="40" role="37wK5m">
                                                <node concept="liA8E" id="4c" role="2OqNvi">
                                                  <ref role="37wK5l" to="ze1i:~ReferenceConstraintsContext.getContainmentLink():org.jetbrains.mps.openapi.language.SContainmentLink" resolve="getContainmentLink" />
                                                  <node concept="cd27G" id="4f" role="lGtFl">
                                                    <node concept="3u3nmq" id="4g" role="cd27D">
                                                      <property role="3u3nmv" value="5675576922574855628" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="37vLTw" id="4d" role="2Oq$k0">
                                                  <ref role="3cqZAo" node="3t" resolve="_context" />
                                                  <node concept="cd27G" id="4h" role="lGtFl">
                                                    <node concept="3u3nmq" id="4i" role="cd27D">
                                                      <property role="3u3nmv" value="5675576922574855628" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="cd27G" id="4e" role="lGtFl">
                                                  <node concept="3u3nmq" id="4j" role="cd27D">
                                                    <property role="3u3nmv" value="5675576922574855628" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="2OqwBi" id="41" role="37wK5m">
                                                <node concept="37vLTw" id="4k" role="2Oq$k0">
                                                  <ref role="3cqZAo" node="3t" resolve="_context" />
                                                  <node concept="cd27G" id="4n" role="lGtFl">
                                                    <node concept="3u3nmq" id="4o" role="cd27D">
                                                      <property role="3u3nmv" value="5675576922574855628" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="liA8E" id="4l" role="2OqNvi">
                                                  <ref role="37wK5l" to="ze1i:~ReferenceConstraintsContext.getPosition():int" resolve="getPosition" />
                                                  <node concept="cd27G" id="4p" role="lGtFl">
                                                    <node concept="3u3nmq" id="4q" role="cd27D">
                                                      <property role="3u3nmv" value="5675576922574855628" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="cd27G" id="4m" role="lGtFl">
                                                  <node concept="3u3nmq" id="4r" role="cd27D">
                                                    <property role="3u3nmv" value="5675576922574855628" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="35c_gC" id="42" role="37wK5m">
                                                <ref role="35c_gD" to="ruww:4QUW3edDVyK" resolve="LaboratoryTestVariable" />
                                                <node concept="cd27G" id="4s" role="lGtFl">
                                                  <node concept="3u3nmq" id="4t" role="cd27D">
                                                    <property role="3u3nmv" value="5675576922574855628" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="cd27G" id="43" role="lGtFl">
                                                <node concept="3u3nmq" id="4u" role="cd27D">
                                                  <property role="3u3nmv" value="5675576922574855628" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="cd27G" id="3W" role="lGtFl">
                                              <node concept="3u3nmq" id="4v" role="cd27D">
                                                <property role="3u3nmv" value="5675576922574855628" />
                                              </node>
                                            </node>
                                          </node>
                                          <node concept="cd27G" id="3T" role="lGtFl">
                                            <node concept="3u3nmq" id="4w" role="cd27D">
                                              <property role="3u3nmv" value="5675576922574855628" />
                                            </node>
                                          </node>
                                        </node>
                                        <node concept="3cpWs6" id="3Q" role="3cqZAp">
                                          <node concept="3K4zz7" id="4x" role="3cqZAk">
                                            <node concept="2ShNRf" id="4z" role="3K4E3e">
                                              <node concept="1pGfFk" id="4B" role="2ShVmc">
                                                <ref role="37wK5l" to="35tq:~EmptyScope.&lt;init&gt;()" resolve="EmptyScope" />
                                                <node concept="cd27G" id="4D" role="lGtFl">
                                                  <node concept="3u3nmq" id="4E" role="cd27D">
                                                    <property role="3u3nmv" value="5675576922574855628" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="cd27G" id="4C" role="lGtFl">
                                                <node concept="3u3nmq" id="4F" role="cd27D">
                                                  <property role="3u3nmv" value="5675576922574855628" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="37vLTw" id="4$" role="3K4GZi">
                                              <ref role="3cqZAo" node="3S" resolve="scope" />
                                              <node concept="cd27G" id="4G" role="lGtFl">
                                                <node concept="3u3nmq" id="4H" role="cd27D">
                                                  <property role="3u3nmv" value="5675576922574855628" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="3clFbC" id="4_" role="3K4Cdx">
                                              <node concept="10Nm6u" id="4I" role="3uHU7w">
                                                <node concept="cd27G" id="4L" role="lGtFl">
                                                  <node concept="3u3nmq" id="4M" role="cd27D">
                                                    <property role="3u3nmv" value="5675576922574855628" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="37vLTw" id="4J" role="3uHU7B">
                                                <ref role="3cqZAo" node="3S" resolve="scope" />
                                                <node concept="cd27G" id="4N" role="lGtFl">
                                                  <node concept="3u3nmq" id="4O" role="cd27D">
                                                    <property role="3u3nmv" value="5675576922574855628" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="cd27G" id="4K" role="lGtFl">
                                                <node concept="3u3nmq" id="4P" role="cd27D">
                                                  <property role="3u3nmv" value="5675576922574855628" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="cd27G" id="4A" role="lGtFl">
                                              <node concept="3u3nmq" id="4Q" role="cd27D">
                                                <property role="3u3nmv" value="5675576922574855628" />
                                              </node>
                                            </node>
                                          </node>
                                          <node concept="cd27G" id="4y" role="lGtFl">
                                            <node concept="3u3nmq" id="4R" role="cd27D">
                                              <property role="3u3nmv" value="5675576922574855628" />
                                            </node>
                                          </node>
                                        </node>
                                        <node concept="cd27G" id="3R" role="lGtFl">
                                          <node concept="3u3nmq" id="4S" role="cd27D">
                                            <property role="3u3nmv" value="5675576922574855628" />
                                          </node>
                                        </node>
                                      </node>
                                      <node concept="cd27G" id="3O" role="lGtFl">
                                        <node concept="3u3nmq" id="4T" role="cd27D">
                                          <property role="3u3nmv" value="5675576922574855628" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="cd27G" id="3M" role="lGtFl">
                                      <node concept="3u3nmq" id="4U" role="cd27D">
                                        <property role="3u3nmv" value="5675576922574855628" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="2AHcQZ" id="3x" role="2AJF6D">
                                    <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
                                    <node concept="cd27G" id="4V" role="lGtFl">
                                      <node concept="3u3nmq" id="4W" role="cd27D">
                                        <property role="3u3nmv" value="5675576922574855628" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="cd27G" id="3y" role="lGtFl">
                                    <node concept="3u3nmq" id="4X" role="cd27D">
                                      <property role="3u3nmv" value="5675576922574855628" />
                                    </node>
                                  </node>
                                </node>
                                <node concept="cd27G" id="2T" role="lGtFl">
                                  <node concept="3u3nmq" id="4Y" role="cd27D">
                                    <property role="3u3nmv" value="5675576922574855628" />
                                  </node>
                                </node>
                              </node>
                              <node concept="cd27G" id="2P" role="lGtFl">
                                <node concept="3u3nmq" id="4Z" role="cd27D">
                                  <property role="3u3nmv" value="5675576922574855628" />
                                </node>
                              </node>
                            </node>
                            <node concept="cd27G" id="2N" role="lGtFl">
                              <node concept="3u3nmq" id="50" role="cd27D">
                                <property role="3u3nmv" value="5675576922574855628" />
                              </node>
                            </node>
                          </node>
                          <node concept="cd27G" id="2L" role="lGtFl">
                            <node concept="3u3nmq" id="51" role="cd27D">
                              <property role="3u3nmv" value="5675576922574855628" />
                            </node>
                          </node>
                        </node>
                        <node concept="cd27G" id="2J" role="lGtFl">
                          <node concept="3u3nmq" id="52" role="cd27D">
                            <property role="3u3nmv" value="5675576922574855628" />
                          </node>
                        </node>
                      </node>
                      <node concept="2AHcQZ" id="2A" role="2AJF6D">
                        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
                        <node concept="cd27G" id="53" role="lGtFl">
                          <node concept="3u3nmq" id="54" role="cd27D">
                            <property role="3u3nmv" value="5675576922574855628" />
                          </node>
                        </node>
                      </node>
                      <node concept="cd27G" id="2B" role="lGtFl">
                        <node concept="3u3nmq" id="55" role="cd27D">
                          <property role="3u3nmv" value="5675576922574855628" />
                        </node>
                      </node>
                    </node>
                    <node concept="cd27G" id="1V" role="lGtFl">
                      <node concept="3u3nmq" id="56" role="cd27D">
                        <property role="3u3nmv" value="5675576922574855628" />
                      </node>
                    </node>
                  </node>
                  <node concept="cd27G" id="1P" role="lGtFl">
                    <node concept="3u3nmq" id="57" role="cd27D">
                      <property role="3u3nmv" value="5675576922574855628" />
                    </node>
                  </node>
                </node>
                <node concept="cd27G" id="1N" role="lGtFl">
                  <node concept="3u3nmq" id="58" role="cd27D">
                    <property role="3u3nmv" value="5675576922574855628" />
                  </node>
                </node>
              </node>
              <node concept="cd27G" id="1w" role="lGtFl">
                <node concept="3u3nmq" id="59" role="cd27D">
                  <property role="3u3nmv" value="5675576922574855628" />
                </node>
              </node>
            </node>
            <node concept="cd27G" id="1r" role="lGtFl">
              <node concept="3u3nmq" id="5a" role="cd27D">
                <property role="3u3nmv" value="5675576922574855628" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="1o" role="lGtFl">
            <node concept="3u3nmq" id="5b" role="cd27D">
              <property role="3u3nmv" value="5675576922574855628" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="V" role="3cqZAp">
          <node concept="37vLTw" id="5c" role="3clFbG">
            <ref role="3cqZAo" node="X" resolve="references" />
            <node concept="cd27G" id="5e" role="lGtFl">
              <node concept="3u3nmq" id="5f" role="cd27D">
                <property role="3u3nmv" value="5675576922574855628" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="5d" role="lGtFl">
            <node concept="3u3nmq" id="5g" role="cd27D">
              <property role="3u3nmv" value="5675576922574855628" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="W" role="lGtFl">
          <node concept="3u3nmq" id="5h" role="cd27D">
            <property role="3u3nmv" value="5675576922574855628" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="H" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <node concept="cd27G" id="5i" role="lGtFl">
          <node concept="3u3nmq" id="5j" role="cd27D">
            <property role="3u3nmv" value="5675576922574855628" />
          </node>
        </node>
      </node>
      <node concept="cd27G" id="I" role="lGtFl">
        <node concept="3u3nmq" id="5k" role="cd27D">
          <property role="3u3nmv" value="5675576922574855628" />
        </node>
      </node>
    </node>
    <node concept="cd27G" id="6" role="lGtFl">
      <node concept="3u3nmq" id="5l" role="cd27D">
        <property role="3u3nmv" value="5675576922574855628" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="5m">
    <property role="3GE5qa" value="aspect.references.laboratoryTest" />
    <property role="TrG5h" value="AspectLaboratoryTestWithNumberResultReference_Constraints" />
    <node concept="3Tm1VV" id="5n" role="1B3o_S">
      <node concept="cd27G" id="5t" role="lGtFl">
        <node concept="3u3nmq" id="5u" role="cd27D">
          <property role="3u3nmv" value="5315700730317313420" />
        </node>
      </node>
    </node>
    <node concept="3uibUv" id="5o" role="1zkMxy">
      <ref role="3uigEE" to="79pl:~BaseConstraintsDescriptor" resolve="BaseConstraintsDescriptor" />
      <node concept="cd27G" id="5v" role="lGtFl">
        <node concept="3u3nmq" id="5w" role="cd27D">
          <property role="3u3nmv" value="5315700730317313420" />
        </node>
      </node>
    </node>
    <node concept="3clFbW" id="5p" role="jymVt">
      <node concept="3cqZAl" id="5x" role="3clF45">
        <node concept="cd27G" id="5_" role="lGtFl">
          <node concept="3u3nmq" id="5A" role="cd27D">
            <property role="3u3nmv" value="5315700730317313420" />
          </node>
        </node>
      </node>
      <node concept="3clFbS" id="5y" role="3clF47">
        <node concept="XkiVB" id="5B" role="3cqZAp">
          <ref role="37wK5l" to="79pl:~BaseConstraintsDescriptor.&lt;init&gt;(org.jetbrains.mps.openapi.language.SAbstractConcept)" resolve="BaseConstraintsDescriptor" />
          <node concept="2YIFZM" id="5D" role="37wK5m">
            <ref role="1Pybhc" to="2k9e:~MetaAdapterFactory" resolve="MetaAdapterFactory" />
            <ref role="37wK5l" to="2k9e:~MetaAdapterFactory.getConcept(long,long,long,java.lang.String):org.jetbrains.mps.openapi.language.SConcept" resolve="getConcept" />
            <node concept="1adDum" id="5F" role="37wK5m">
              <property role="1adDun" value="0x4a652d5536844d2dL" />
              <node concept="cd27G" id="5K" role="lGtFl">
                <node concept="3u3nmq" id="5L" role="cd27D">
                  <property role="3u3nmv" value="5315700730317313420" />
                </node>
              </node>
            </node>
            <node concept="1adDum" id="5G" role="37wK5m">
              <property role="1adDun" value="0x98c92ef46f124c44L" />
              <node concept="cd27G" id="5M" role="lGtFl">
                <node concept="3u3nmq" id="5N" role="cd27D">
                  <property role="3u3nmv" value="5315700730317313420" />
                </node>
              </node>
            </node>
            <node concept="1adDum" id="5H" role="37wK5m">
              <property role="1adDun" value="0x49c529a682d90a9bL" />
              <node concept="cd27G" id="5O" role="lGtFl">
                <node concept="3u3nmq" id="5P" role="cd27D">
                  <property role="3u3nmv" value="5315700730317313420" />
                </node>
              </node>
            </node>
            <node concept="Xl_RD" id="5I" role="37wK5m">
              <property role="Xl_RC" value="no.uio.mLab.decisions.references.laboratoryTest.structure.AspectLaboratoryTestWithNumberResultReference" />
              <node concept="cd27G" id="5Q" role="lGtFl">
                <node concept="3u3nmq" id="5R" role="cd27D">
                  <property role="3u3nmv" value="5315700730317313420" />
                </node>
              </node>
            </node>
            <node concept="cd27G" id="5J" role="lGtFl">
              <node concept="3u3nmq" id="5S" role="cd27D">
                <property role="3u3nmv" value="5315700730317313420" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="5E" role="lGtFl">
            <node concept="3u3nmq" id="5T" role="cd27D">
              <property role="3u3nmv" value="5315700730317313420" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="5C" role="lGtFl">
          <node concept="3u3nmq" id="5U" role="cd27D">
            <property role="3u3nmv" value="5315700730317313420" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="5z" role="1B3o_S">
        <node concept="cd27G" id="5V" role="lGtFl">
          <node concept="3u3nmq" id="5W" role="cd27D">
            <property role="3u3nmv" value="5315700730317313420" />
          </node>
        </node>
      </node>
      <node concept="cd27G" id="5$" role="lGtFl">
        <node concept="3u3nmq" id="5X" role="cd27D">
          <property role="3u3nmv" value="5315700730317313420" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="5q" role="jymVt">
      <node concept="cd27G" id="5Y" role="lGtFl">
        <node concept="3u3nmq" id="5Z" role="cd27D">
          <property role="3u3nmv" value="5315700730317313420" />
        </node>
      </node>
    </node>
    <node concept="3clFb_" id="5r" role="jymVt">
      <property role="IEkAT" value="false" />
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="getSpecifiedReferences" />
      <property role="DiZV1" value="false" />
      <node concept="3Tmbuc" id="60" role="1B3o_S">
        <node concept="cd27G" id="65" role="lGtFl">
          <node concept="3u3nmq" id="66" role="cd27D">
            <property role="3u3nmv" value="5315700730317313420" />
          </node>
        </node>
      </node>
      <node concept="3uibUv" id="61" role="3clF45">
        <ref role="3uigEE" to="33ny:~Map" resolve="Map" />
        <node concept="3uibUv" id="67" role="11_B2D">
          <ref role="3uigEE" to="c17a:~SReferenceLink" resolve="SReferenceLink" />
          <node concept="cd27G" id="6a" role="lGtFl">
            <node concept="3u3nmq" id="6b" role="cd27D">
              <property role="3u3nmv" value="5315700730317313420" />
            </node>
          </node>
        </node>
        <node concept="3uibUv" id="68" role="11_B2D">
          <ref role="3uigEE" to="ze1i:~ReferenceConstraintsDescriptor" resolve="ReferenceConstraintsDescriptor" />
          <node concept="cd27G" id="6c" role="lGtFl">
            <node concept="3u3nmq" id="6d" role="cd27D">
              <property role="3u3nmv" value="5315700730317313420" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="69" role="lGtFl">
          <node concept="3u3nmq" id="6e" role="cd27D">
            <property role="3u3nmv" value="5315700730317313420" />
          </node>
        </node>
      </node>
      <node concept="3clFbS" id="62" role="3clF47">
        <node concept="3cpWs8" id="6f" role="3cqZAp">
          <node concept="3cpWsn" id="6j" role="3cpWs9">
            <property role="TrG5h" value="references" />
            <node concept="3uibUv" id="6l" role="1tU5fm">
              <ref role="3uigEE" to="33ny:~Map" resolve="Map" />
              <node concept="3uibUv" id="6o" role="11_B2D">
                <ref role="3uigEE" to="c17a:~SReferenceLink" resolve="SReferenceLink" />
                <node concept="cd27G" id="6r" role="lGtFl">
                  <node concept="3u3nmq" id="6s" role="cd27D">
                    <property role="3u3nmv" value="5315700730317313420" />
                  </node>
                </node>
              </node>
              <node concept="3uibUv" id="6p" role="11_B2D">
                <ref role="3uigEE" to="ze1i:~ReferenceConstraintsDescriptor" resolve="ReferenceConstraintsDescriptor" />
                <node concept="cd27G" id="6t" role="lGtFl">
                  <node concept="3u3nmq" id="6u" role="cd27D">
                    <property role="3u3nmv" value="5315700730317313420" />
                  </node>
                </node>
              </node>
              <node concept="cd27G" id="6q" role="lGtFl">
                <node concept="3u3nmq" id="6v" role="cd27D">
                  <property role="3u3nmv" value="5315700730317313420" />
                </node>
              </node>
            </node>
            <node concept="2ShNRf" id="6m" role="33vP2m">
              <node concept="1pGfFk" id="6w" role="2ShVmc">
                <ref role="37wK5l" to="33ny:~HashMap.&lt;init&gt;()" resolve="HashMap" />
                <node concept="3uibUv" id="6y" role="1pMfVU">
                  <ref role="3uigEE" to="c17a:~SReferenceLink" resolve="SReferenceLink" />
                  <node concept="cd27G" id="6_" role="lGtFl">
                    <node concept="3u3nmq" id="6A" role="cd27D">
                      <property role="3u3nmv" value="5315700730317313420" />
                    </node>
                  </node>
                </node>
                <node concept="3uibUv" id="6z" role="1pMfVU">
                  <ref role="3uigEE" to="ze1i:~ReferenceConstraintsDescriptor" resolve="ReferenceConstraintsDescriptor" />
                  <node concept="cd27G" id="6B" role="lGtFl">
                    <node concept="3u3nmq" id="6C" role="cd27D">
                      <property role="3u3nmv" value="5315700730317313420" />
                    </node>
                  </node>
                </node>
                <node concept="cd27G" id="6$" role="lGtFl">
                  <node concept="3u3nmq" id="6D" role="cd27D">
                    <property role="3u3nmv" value="5315700730317313420" />
                  </node>
                </node>
              </node>
              <node concept="cd27G" id="6x" role="lGtFl">
                <node concept="3u3nmq" id="6E" role="cd27D">
                  <property role="3u3nmv" value="5315700730317313420" />
                </node>
              </node>
            </node>
            <node concept="cd27G" id="6n" role="lGtFl">
              <node concept="3u3nmq" id="6F" role="cd27D">
                <property role="3u3nmv" value="5315700730317313420" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="6k" role="lGtFl">
            <node concept="3u3nmq" id="6G" role="cd27D">
              <property role="3u3nmv" value="5315700730317313420" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="6g" role="3cqZAp">
          <node concept="2OqwBi" id="6H" role="3clFbG">
            <node concept="37vLTw" id="6J" role="2Oq$k0">
              <ref role="3cqZAo" node="6j" resolve="references" />
              <node concept="cd27G" id="6M" role="lGtFl">
                <node concept="3u3nmq" id="6N" role="cd27D">
                  <property role="3u3nmv" value="5315700730317313420" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="6K" role="2OqNvi">
              <ref role="37wK5l" to="33ny:~Map.put(java.lang.Object,java.lang.Object):java.lang.Object" resolve="put" />
              <node concept="2YIFZM" id="6O" role="37wK5m">
                <ref role="1Pybhc" to="2k9e:~MetaAdapterFactory" resolve="MetaAdapterFactory" />
                <ref role="37wK5l" to="2k9e:~MetaAdapterFactory.getReferenceLink(long,long,long,long,java.lang.String):org.jetbrains.mps.openapi.language.SReferenceLink" resolve="getReferenceLink" />
                <node concept="1adDum" id="6R" role="37wK5m">
                  <property role="1adDun" value="0xc610a4ebc47c4f95L" />
                  <node concept="cd27G" id="6X" role="lGtFl">
                    <node concept="3u3nmq" id="6Y" role="cd27D">
                      <property role="3u3nmv" value="5315700730317313420" />
                    </node>
                  </node>
                </node>
                <node concept="1adDum" id="6S" role="37wK5m">
                  <property role="1adDun" value="0xb56811236971769cL" />
                  <node concept="cd27G" id="6Z" role="lGtFl">
                    <node concept="3u3nmq" id="70" role="cd27D">
                      <property role="3u3nmv" value="5315700730317313420" />
                    </node>
                  </node>
                </node>
                <node concept="1adDum" id="6T" role="37wK5m">
                  <property role="1adDun" value="0x6c7943d5b299791dL" />
                  <node concept="cd27G" id="71" role="lGtFl">
                    <node concept="3u3nmq" id="72" role="cd27D">
                      <property role="3u3nmv" value="5315700730317313420" />
                    </node>
                  </node>
                </node>
                <node concept="1adDum" id="6U" role="37wK5m">
                  <property role="1adDun" value="0x6c7943d5b29b8063L" />
                  <node concept="cd27G" id="73" role="lGtFl">
                    <node concept="3u3nmq" id="74" role="cd27D">
                      <property role="3u3nmv" value="5315700730317313420" />
                    </node>
                  </node>
                </node>
                <node concept="Xl_RD" id="6V" role="37wK5m">
                  <property role="Xl_RC" value="target" />
                  <node concept="cd27G" id="75" role="lGtFl">
                    <node concept="3u3nmq" id="76" role="cd27D">
                      <property role="3u3nmv" value="5315700730317313420" />
                    </node>
                  </node>
                </node>
                <node concept="cd27G" id="6W" role="lGtFl">
                  <node concept="3u3nmq" id="77" role="cd27D">
                    <property role="3u3nmv" value="5315700730317313420" />
                  </node>
                </node>
              </node>
              <node concept="2ShNRf" id="6P" role="37wK5m">
                <node concept="YeOm9" id="78" role="2ShVmc">
                  <node concept="1Y3b0j" id="7a" role="YeSDq">
                    <property role="2bfB8j" value="true" />
                    <ref role="1Y3XeK" to="79pl:~BaseReferenceConstraintsDescriptor" resolve="BaseReferenceConstraintsDescriptor" />
                    <ref role="37wK5l" to="79pl:~BaseReferenceConstraintsDescriptor.&lt;init&gt;(jetbrains.mps.smodel.adapter.ids.SReferenceLinkId,jetbrains.mps.smodel.runtime.ConstraintsDescriptor)" resolve="BaseReferenceConstraintsDescriptor" />
                    <node concept="2YIFZM" id="7c" role="37wK5m">
                      <ref role="1Pybhc" to="e8bb:~MetaIdFactory" resolve="MetaIdFactory" />
                      <ref role="37wK5l" to="e8bb:~MetaIdFactory.refId(long,long,long,long):jetbrains.mps.smodel.adapter.ids.SReferenceLinkId" resolve="refId" />
                      <node concept="1adDum" id="7i" role="37wK5m">
                        <property role="1adDun" value="0xc610a4ebc47c4f95L" />
                        <node concept="cd27G" id="7n" role="lGtFl">
                          <node concept="3u3nmq" id="7o" role="cd27D">
                            <property role="3u3nmv" value="5315700730317313420" />
                          </node>
                        </node>
                      </node>
                      <node concept="1adDum" id="7j" role="37wK5m">
                        <property role="1adDun" value="0xb56811236971769cL" />
                        <node concept="cd27G" id="7p" role="lGtFl">
                          <node concept="3u3nmq" id="7q" role="cd27D">
                            <property role="3u3nmv" value="5315700730317313420" />
                          </node>
                        </node>
                      </node>
                      <node concept="1adDum" id="7k" role="37wK5m">
                        <property role="1adDun" value="0x6c7943d5b299791dL" />
                        <node concept="cd27G" id="7r" role="lGtFl">
                          <node concept="3u3nmq" id="7s" role="cd27D">
                            <property role="3u3nmv" value="5315700730317313420" />
                          </node>
                        </node>
                      </node>
                      <node concept="1adDum" id="7l" role="37wK5m">
                        <property role="1adDun" value="0x6c7943d5b29b8063L" />
                        <node concept="cd27G" id="7t" role="lGtFl">
                          <node concept="3u3nmq" id="7u" role="cd27D">
                            <property role="3u3nmv" value="5315700730317313420" />
                          </node>
                        </node>
                      </node>
                      <node concept="cd27G" id="7m" role="lGtFl">
                        <node concept="3u3nmq" id="7v" role="cd27D">
                          <property role="3u3nmv" value="5315700730317313420" />
                        </node>
                      </node>
                    </node>
                    <node concept="3Tm1VV" id="7d" role="1B3o_S">
                      <node concept="cd27G" id="7w" role="lGtFl">
                        <node concept="3u3nmq" id="7x" role="cd27D">
                          <property role="3u3nmv" value="5315700730317313420" />
                        </node>
                      </node>
                    </node>
                    <node concept="Xjq3P" id="7e" role="37wK5m">
                      <node concept="cd27G" id="7y" role="lGtFl">
                        <node concept="3u3nmq" id="7z" role="cd27D">
                          <property role="3u3nmv" value="5315700730317313420" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFb_" id="7f" role="jymVt">
                      <property role="IEkAT" value="false" />
                      <property role="1EzhhJ" value="false" />
                      <property role="TrG5h" value="hasOwnScopeProvider" />
                      <property role="DiZV1" value="false" />
                      <node concept="3Tm1VV" id="7$" role="1B3o_S">
                        <node concept="cd27G" id="7D" role="lGtFl">
                          <node concept="3u3nmq" id="7E" role="cd27D">
                            <property role="3u3nmv" value="5315700730317313420" />
                          </node>
                        </node>
                      </node>
                      <node concept="10P_77" id="7_" role="3clF45">
                        <node concept="cd27G" id="7F" role="lGtFl">
                          <node concept="3u3nmq" id="7G" role="cd27D">
                            <property role="3u3nmv" value="5315700730317313420" />
                          </node>
                        </node>
                      </node>
                      <node concept="3clFbS" id="7A" role="3clF47">
                        <node concept="3clFbF" id="7H" role="3cqZAp">
                          <node concept="3clFbT" id="7J" role="3clFbG">
                            <property role="3clFbU" value="true" />
                            <node concept="cd27G" id="7L" role="lGtFl">
                              <node concept="3u3nmq" id="7M" role="cd27D">
                                <property role="3u3nmv" value="5315700730317313420" />
                              </node>
                            </node>
                          </node>
                          <node concept="cd27G" id="7K" role="lGtFl">
                            <node concept="3u3nmq" id="7N" role="cd27D">
                              <property role="3u3nmv" value="5315700730317313420" />
                            </node>
                          </node>
                        </node>
                        <node concept="cd27G" id="7I" role="lGtFl">
                          <node concept="3u3nmq" id="7O" role="cd27D">
                            <property role="3u3nmv" value="5315700730317313420" />
                          </node>
                        </node>
                      </node>
                      <node concept="2AHcQZ" id="7B" role="2AJF6D">
                        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
                        <node concept="cd27G" id="7P" role="lGtFl">
                          <node concept="3u3nmq" id="7Q" role="cd27D">
                            <property role="3u3nmv" value="5315700730317313420" />
                          </node>
                        </node>
                      </node>
                      <node concept="cd27G" id="7C" role="lGtFl">
                        <node concept="3u3nmq" id="7R" role="cd27D">
                          <property role="3u3nmv" value="5315700730317313420" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFb_" id="7g" role="jymVt">
                      <property role="IEkAT" value="false" />
                      <property role="1EzhhJ" value="false" />
                      <property role="TrG5h" value="getScopeProvider" />
                      <property role="DiZV1" value="false" />
                      <node concept="3Tm1VV" id="7S" role="1B3o_S">
                        <node concept="cd27G" id="7Y" role="lGtFl">
                          <node concept="3u3nmq" id="7Z" role="cd27D">
                            <property role="3u3nmv" value="5315700730317313420" />
                          </node>
                        </node>
                      </node>
                      <node concept="3uibUv" id="7T" role="3clF45">
                        <ref role="3uigEE" to="ze1i:~ReferenceScopeProvider" resolve="ReferenceScopeProvider" />
                        <node concept="cd27G" id="80" role="lGtFl">
                          <node concept="3u3nmq" id="81" role="cd27D">
                            <property role="3u3nmv" value="5315700730317313420" />
                          </node>
                        </node>
                      </node>
                      <node concept="2AHcQZ" id="7U" role="2AJF6D">
                        <ref role="2AI5Lk" to="mhfm:~Nullable" resolve="Nullable" />
                        <node concept="cd27G" id="82" role="lGtFl">
                          <node concept="3u3nmq" id="83" role="cd27D">
                            <property role="3u3nmv" value="5315700730317313420" />
                          </node>
                        </node>
                      </node>
                      <node concept="3clFbS" id="7V" role="3clF47">
                        <node concept="3cpWs6" id="84" role="3cqZAp">
                          <node concept="2ShNRf" id="86" role="3cqZAk">
                            <node concept="YeOm9" id="88" role="2ShVmc">
                              <node concept="1Y3b0j" id="8a" role="YeSDq">
                                <property role="2bfB8j" value="true" />
                                <ref role="1Y3XeK" to="79pl:~BaseScopeProvider" resolve="BaseScopeProvider" />
                                <ref role="37wK5l" to="79pl:~BaseScopeProvider.&lt;init&gt;()" resolve="BaseScopeProvider" />
                                <node concept="3Tm1VV" id="8c" role="1B3o_S">
                                  <node concept="cd27G" id="8g" role="lGtFl">
                                    <node concept="3u3nmq" id="8h" role="cd27D">
                                      <property role="3u3nmv" value="5315700730317313420" />
                                    </node>
                                  </node>
                                </node>
                                <node concept="3clFb_" id="8d" role="jymVt">
                                  <property role="TrG5h" value="getSearchScopeValidatorNode" />
                                  <node concept="3Tm1VV" id="8i" role="1B3o_S">
                                    <node concept="cd27G" id="8n" role="lGtFl">
                                      <node concept="3u3nmq" id="8o" role="cd27D">
                                        <property role="3u3nmv" value="5315700730317313420" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="3clFbS" id="8j" role="3clF47">
                                    <node concept="3cpWs6" id="8p" role="3cqZAp">
                                      <node concept="1dyn4i" id="8r" role="3cqZAk">
                                        <property role="1zomUR" value="true" />
                                        <property role="1dyqJU" value="breakingNode" />
                                        <node concept="2ShNRf" id="8t" role="1dyrYi">
                                          <node concept="1pGfFk" id="8v" role="2ShVmc">
                                            <ref role="37wK5l" to="w1kc:~SNodePointer.&lt;init&gt;(java.lang.String,java.lang.String)" resolve="SNodePointer" />
                                            <node concept="Xl_RD" id="8x" role="37wK5m">
                                              <property role="Xl_RC" value="r:e4a7a6f9-a1de-4f08-9b98-b5f2b9ecf3a4(no.uio.mLab.decisions.references.laboratoryTest.constraints)" />
                                              <node concept="cd27G" id="8$" role="lGtFl">
                                                <node concept="3u3nmq" id="8_" role="cd27D">
                                                  <property role="3u3nmv" value="5315700730317313420" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="Xl_RD" id="8y" role="37wK5m">
                                              <property role="Xl_RC" value="5315700730317313423" />
                                              <node concept="cd27G" id="8A" role="lGtFl">
                                                <node concept="3u3nmq" id="8B" role="cd27D">
                                                  <property role="3u3nmv" value="5315700730317313420" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="cd27G" id="8z" role="lGtFl">
                                              <node concept="3u3nmq" id="8C" role="cd27D">
                                                <property role="3u3nmv" value="5315700730317313420" />
                                              </node>
                                            </node>
                                          </node>
                                          <node concept="cd27G" id="8w" role="lGtFl">
                                            <node concept="3u3nmq" id="8D" role="cd27D">
                                              <property role="3u3nmv" value="5315700730317313420" />
                                            </node>
                                          </node>
                                        </node>
                                        <node concept="cd27G" id="8u" role="lGtFl">
                                          <node concept="3u3nmq" id="8E" role="cd27D">
                                            <property role="3u3nmv" value="5315700730317313420" />
                                          </node>
                                        </node>
                                      </node>
                                      <node concept="cd27G" id="8s" role="lGtFl">
                                        <node concept="3u3nmq" id="8F" role="cd27D">
                                          <property role="3u3nmv" value="5315700730317313420" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="cd27G" id="8q" role="lGtFl">
                                      <node concept="3u3nmq" id="8G" role="cd27D">
                                        <property role="3u3nmv" value="5315700730317313420" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="3uibUv" id="8k" role="3clF45">
                                    <ref role="3uigEE" to="mhbf:~SNodeReference" resolve="SNodeReference" />
                                    <node concept="cd27G" id="8H" role="lGtFl">
                                      <node concept="3u3nmq" id="8I" role="cd27D">
                                        <property role="3u3nmv" value="5315700730317313420" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="2AHcQZ" id="8l" role="2AJF6D">
                                    <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
                                    <node concept="cd27G" id="8J" role="lGtFl">
                                      <node concept="3u3nmq" id="8K" role="cd27D">
                                        <property role="3u3nmv" value="5315700730317313420" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="cd27G" id="8m" role="lGtFl">
                                    <node concept="3u3nmq" id="8L" role="cd27D">
                                      <property role="3u3nmv" value="5315700730317313420" />
                                    </node>
                                  </node>
                                </node>
                                <node concept="3clFb_" id="8e" role="jymVt">
                                  <property role="IEkAT" value="false" />
                                  <property role="1EzhhJ" value="false" />
                                  <property role="TrG5h" value="createScope" />
                                  <property role="DiZV1" value="false" />
                                  <node concept="37vLTG" id="8M" role="3clF46">
                                    <property role="TrG5h" value="operationContext" />
                                    <property role="3TUv4t" value="true" />
                                    <node concept="3uibUv" id="8T" role="1tU5fm">
                                      <ref role="3uigEE" to="w1kc:~IOperationContext" resolve="IOperationContext" />
                                      <node concept="cd27G" id="8V" role="lGtFl">
                                        <node concept="3u3nmq" id="8W" role="cd27D">
                                          <property role="3u3nmv" value="5315700730317313420" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="cd27G" id="8U" role="lGtFl">
                                      <node concept="3u3nmq" id="8X" role="cd27D">
                                        <property role="3u3nmv" value="5315700730317313420" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="37vLTG" id="8N" role="3clF46">
                                    <property role="TrG5h" value="_context" />
                                    <property role="3TUv4t" value="true" />
                                    <node concept="3uibUv" id="8Y" role="1tU5fm">
                                      <ref role="3uigEE" to="ze1i:~ReferenceConstraintsContext" resolve="ReferenceConstraintsContext" />
                                      <node concept="cd27G" id="90" role="lGtFl">
                                        <node concept="3u3nmq" id="91" role="cd27D">
                                          <property role="3u3nmv" value="5315700730317313420" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="cd27G" id="8Z" role="lGtFl">
                                      <node concept="3u3nmq" id="92" role="cd27D">
                                        <property role="3u3nmv" value="5315700730317313420" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="3Tm1VV" id="8O" role="1B3o_S">
                                    <node concept="cd27G" id="93" role="lGtFl">
                                      <node concept="3u3nmq" id="94" role="cd27D">
                                        <property role="3u3nmv" value="5315700730317313420" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="3uibUv" id="8P" role="3clF45">
                                    <ref role="3uigEE" to="35tq:~Scope" resolve="Scope" />
                                    <node concept="cd27G" id="95" role="lGtFl">
                                      <node concept="3u3nmq" id="96" role="cd27D">
                                        <property role="3u3nmv" value="5315700730317313420" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="3clFbS" id="8Q" role="3clF47">
                                    <node concept="9aQIb" id="97" role="3cqZAp">
                                      <node concept="3clFbS" id="99" role="9aQI4">
                                        <node concept="3cpWs8" id="9b" role="3cqZAp">
                                          <node concept="3cpWsn" id="9e" role="3cpWs9">
                                            <property role="TrG5h" value="scope" />
                                            <node concept="3uibUv" id="9g" role="1tU5fm">
                                              <ref role="3uigEE" to="35tq:~Scope" resolve="Scope" />
                                              <node concept="cd27G" id="9j" role="lGtFl">
                                                <node concept="3u3nmq" id="9k" role="cd27D">
                                                  <property role="3u3nmv" value="5315700730317313420" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="2YIFZM" id="9h" role="33vP2m">
                                              <ref role="1Pybhc" to="35tq:~Scope" resolve="Scope" />
                                              <ref role="37wK5l" to="35tq:~Scope.getScope(org.jetbrains.mps.openapi.model.SNode,org.jetbrains.mps.openapi.language.SContainmentLink,int,org.jetbrains.mps.openapi.language.SAbstractConcept):jetbrains.mps.scope.Scope" resolve="getScope" />
                                              <node concept="2OqwBi" id="9l" role="37wK5m">
                                                <node concept="37vLTw" id="9q" role="2Oq$k0">
                                                  <ref role="3cqZAo" node="8N" resolve="_context" />
                                                  <node concept="cd27G" id="9t" role="lGtFl">
                                                    <node concept="3u3nmq" id="9u" role="cd27D">
                                                      <property role="3u3nmv" value="5315700730317313420" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="liA8E" id="9r" role="2OqNvi">
                                                  <ref role="37wK5l" to="ze1i:~ReferenceConstraintsContext.getContextNode():org.jetbrains.mps.openapi.model.SNode" resolve="getContextNode" />
                                                  <node concept="cd27G" id="9v" role="lGtFl">
                                                    <node concept="3u3nmq" id="9w" role="cd27D">
                                                      <property role="3u3nmv" value="5315700730317313420" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="cd27G" id="9s" role="lGtFl">
                                                  <node concept="3u3nmq" id="9x" role="cd27D">
                                                    <property role="3u3nmv" value="5315700730317313420" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="2OqwBi" id="9m" role="37wK5m">
                                                <node concept="liA8E" id="9y" role="2OqNvi">
                                                  <ref role="37wK5l" to="ze1i:~ReferenceConstraintsContext.getContainmentLink():org.jetbrains.mps.openapi.language.SContainmentLink" resolve="getContainmentLink" />
                                                  <node concept="cd27G" id="9_" role="lGtFl">
                                                    <node concept="3u3nmq" id="9A" role="cd27D">
                                                      <property role="3u3nmv" value="5315700730317313420" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="37vLTw" id="9z" role="2Oq$k0">
                                                  <ref role="3cqZAo" node="8N" resolve="_context" />
                                                  <node concept="cd27G" id="9B" role="lGtFl">
                                                    <node concept="3u3nmq" id="9C" role="cd27D">
                                                      <property role="3u3nmv" value="5315700730317313420" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="cd27G" id="9$" role="lGtFl">
                                                  <node concept="3u3nmq" id="9D" role="cd27D">
                                                    <property role="3u3nmv" value="5315700730317313420" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="2OqwBi" id="9n" role="37wK5m">
                                                <node concept="37vLTw" id="9E" role="2Oq$k0">
                                                  <ref role="3cqZAo" node="8N" resolve="_context" />
                                                  <node concept="cd27G" id="9H" role="lGtFl">
                                                    <node concept="3u3nmq" id="9I" role="cd27D">
                                                      <property role="3u3nmv" value="5315700730317313420" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="liA8E" id="9F" role="2OqNvi">
                                                  <ref role="37wK5l" to="ze1i:~ReferenceConstraintsContext.getPosition():int" resolve="getPosition" />
                                                  <node concept="cd27G" id="9J" role="lGtFl">
                                                    <node concept="3u3nmq" id="9K" role="cd27D">
                                                      <property role="3u3nmv" value="5315700730317313420" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="cd27G" id="9G" role="lGtFl">
                                                  <node concept="3u3nmq" id="9L" role="cd27D">
                                                    <property role="3u3nmv" value="5315700730317313420" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="35c_gC" id="9o" role="37wK5m">
                                                <ref role="35c_gD" to="ruww:4B5aqq2QmL5" resolve="LaboratoryTestWithNumberResultVariable" />
                                                <node concept="cd27G" id="9M" role="lGtFl">
                                                  <node concept="3u3nmq" id="9N" role="cd27D">
                                                    <property role="3u3nmv" value="5315700730317313420" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="cd27G" id="9p" role="lGtFl">
                                                <node concept="3u3nmq" id="9O" role="cd27D">
                                                  <property role="3u3nmv" value="5315700730317313420" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="cd27G" id="9i" role="lGtFl">
                                              <node concept="3u3nmq" id="9P" role="cd27D">
                                                <property role="3u3nmv" value="5315700730317313420" />
                                              </node>
                                            </node>
                                          </node>
                                          <node concept="cd27G" id="9f" role="lGtFl">
                                            <node concept="3u3nmq" id="9Q" role="cd27D">
                                              <property role="3u3nmv" value="5315700730317313420" />
                                            </node>
                                          </node>
                                        </node>
                                        <node concept="3cpWs6" id="9c" role="3cqZAp">
                                          <node concept="3K4zz7" id="9R" role="3cqZAk">
                                            <node concept="2ShNRf" id="9T" role="3K4E3e">
                                              <node concept="1pGfFk" id="9X" role="2ShVmc">
                                                <ref role="37wK5l" to="35tq:~EmptyScope.&lt;init&gt;()" resolve="EmptyScope" />
                                                <node concept="cd27G" id="9Z" role="lGtFl">
                                                  <node concept="3u3nmq" id="a0" role="cd27D">
                                                    <property role="3u3nmv" value="5315700730317313420" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="cd27G" id="9Y" role="lGtFl">
                                                <node concept="3u3nmq" id="a1" role="cd27D">
                                                  <property role="3u3nmv" value="5315700730317313420" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="37vLTw" id="9U" role="3K4GZi">
                                              <ref role="3cqZAo" node="9e" resolve="scope" />
                                              <node concept="cd27G" id="a2" role="lGtFl">
                                                <node concept="3u3nmq" id="a3" role="cd27D">
                                                  <property role="3u3nmv" value="5315700730317313420" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="3clFbC" id="9V" role="3K4Cdx">
                                              <node concept="10Nm6u" id="a4" role="3uHU7w">
                                                <node concept="cd27G" id="a7" role="lGtFl">
                                                  <node concept="3u3nmq" id="a8" role="cd27D">
                                                    <property role="3u3nmv" value="5315700730317313420" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="37vLTw" id="a5" role="3uHU7B">
                                                <ref role="3cqZAo" node="9e" resolve="scope" />
                                                <node concept="cd27G" id="a9" role="lGtFl">
                                                  <node concept="3u3nmq" id="aa" role="cd27D">
                                                    <property role="3u3nmv" value="5315700730317313420" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="cd27G" id="a6" role="lGtFl">
                                                <node concept="3u3nmq" id="ab" role="cd27D">
                                                  <property role="3u3nmv" value="5315700730317313420" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="cd27G" id="9W" role="lGtFl">
                                              <node concept="3u3nmq" id="ac" role="cd27D">
                                                <property role="3u3nmv" value="5315700730317313420" />
                                              </node>
                                            </node>
                                          </node>
                                          <node concept="cd27G" id="9S" role="lGtFl">
                                            <node concept="3u3nmq" id="ad" role="cd27D">
                                              <property role="3u3nmv" value="5315700730317313420" />
                                            </node>
                                          </node>
                                        </node>
                                        <node concept="cd27G" id="9d" role="lGtFl">
                                          <node concept="3u3nmq" id="ae" role="cd27D">
                                            <property role="3u3nmv" value="5315700730317313420" />
                                          </node>
                                        </node>
                                      </node>
                                      <node concept="cd27G" id="9a" role="lGtFl">
                                        <node concept="3u3nmq" id="af" role="cd27D">
                                          <property role="3u3nmv" value="5315700730317313420" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="cd27G" id="98" role="lGtFl">
                                      <node concept="3u3nmq" id="ag" role="cd27D">
                                        <property role="3u3nmv" value="5315700730317313420" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="2AHcQZ" id="8R" role="2AJF6D">
                                    <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
                                    <node concept="cd27G" id="ah" role="lGtFl">
                                      <node concept="3u3nmq" id="ai" role="cd27D">
                                        <property role="3u3nmv" value="5315700730317313420" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="cd27G" id="8S" role="lGtFl">
                                    <node concept="3u3nmq" id="aj" role="cd27D">
                                      <property role="3u3nmv" value="5315700730317313420" />
                                    </node>
                                  </node>
                                </node>
                                <node concept="cd27G" id="8f" role="lGtFl">
                                  <node concept="3u3nmq" id="ak" role="cd27D">
                                    <property role="3u3nmv" value="5315700730317313420" />
                                  </node>
                                </node>
                              </node>
                              <node concept="cd27G" id="8b" role="lGtFl">
                                <node concept="3u3nmq" id="al" role="cd27D">
                                  <property role="3u3nmv" value="5315700730317313420" />
                                </node>
                              </node>
                            </node>
                            <node concept="cd27G" id="89" role="lGtFl">
                              <node concept="3u3nmq" id="am" role="cd27D">
                                <property role="3u3nmv" value="5315700730317313420" />
                              </node>
                            </node>
                          </node>
                          <node concept="cd27G" id="87" role="lGtFl">
                            <node concept="3u3nmq" id="an" role="cd27D">
                              <property role="3u3nmv" value="5315700730317313420" />
                            </node>
                          </node>
                        </node>
                        <node concept="cd27G" id="85" role="lGtFl">
                          <node concept="3u3nmq" id="ao" role="cd27D">
                            <property role="3u3nmv" value="5315700730317313420" />
                          </node>
                        </node>
                      </node>
                      <node concept="2AHcQZ" id="7W" role="2AJF6D">
                        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
                        <node concept="cd27G" id="ap" role="lGtFl">
                          <node concept="3u3nmq" id="aq" role="cd27D">
                            <property role="3u3nmv" value="5315700730317313420" />
                          </node>
                        </node>
                      </node>
                      <node concept="cd27G" id="7X" role="lGtFl">
                        <node concept="3u3nmq" id="ar" role="cd27D">
                          <property role="3u3nmv" value="5315700730317313420" />
                        </node>
                      </node>
                    </node>
                    <node concept="cd27G" id="7h" role="lGtFl">
                      <node concept="3u3nmq" id="as" role="cd27D">
                        <property role="3u3nmv" value="5315700730317313420" />
                      </node>
                    </node>
                  </node>
                  <node concept="cd27G" id="7b" role="lGtFl">
                    <node concept="3u3nmq" id="at" role="cd27D">
                      <property role="3u3nmv" value="5315700730317313420" />
                    </node>
                  </node>
                </node>
                <node concept="cd27G" id="79" role="lGtFl">
                  <node concept="3u3nmq" id="au" role="cd27D">
                    <property role="3u3nmv" value="5315700730317313420" />
                  </node>
                </node>
              </node>
              <node concept="cd27G" id="6Q" role="lGtFl">
                <node concept="3u3nmq" id="av" role="cd27D">
                  <property role="3u3nmv" value="5315700730317313420" />
                </node>
              </node>
            </node>
            <node concept="cd27G" id="6L" role="lGtFl">
              <node concept="3u3nmq" id="aw" role="cd27D">
                <property role="3u3nmv" value="5315700730317313420" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="6I" role="lGtFl">
            <node concept="3u3nmq" id="ax" role="cd27D">
              <property role="3u3nmv" value="5315700730317313420" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="6h" role="3cqZAp">
          <node concept="37vLTw" id="ay" role="3clFbG">
            <ref role="3cqZAo" node="6j" resolve="references" />
            <node concept="cd27G" id="a$" role="lGtFl">
              <node concept="3u3nmq" id="a_" role="cd27D">
                <property role="3u3nmv" value="5315700730317313420" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="az" role="lGtFl">
            <node concept="3u3nmq" id="aA" role="cd27D">
              <property role="3u3nmv" value="5315700730317313420" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="6i" role="lGtFl">
          <node concept="3u3nmq" id="aB" role="cd27D">
            <property role="3u3nmv" value="5315700730317313420" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="63" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <node concept="cd27G" id="aC" role="lGtFl">
          <node concept="3u3nmq" id="aD" role="cd27D">
            <property role="3u3nmv" value="5315700730317313420" />
          </node>
        </node>
      </node>
      <node concept="cd27G" id="64" role="lGtFl">
        <node concept="3u3nmq" id="aE" role="cd27D">
          <property role="3u3nmv" value="5315700730317313420" />
        </node>
      </node>
    </node>
    <node concept="cd27G" id="5s" role="lGtFl">
      <node concept="3u3nmq" id="aF" role="cd27D">
        <property role="3u3nmv" value="5315700730317313420" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="aG">
    <property role="3GE5qa" value="aspect.references.laboratoryTest" />
    <property role="TrG5h" value="AspectLaboratoryTestWithTextResultReference_Constraints" />
    <node concept="3Tm1VV" id="aH" role="1B3o_S">
      <node concept="cd27G" id="aN" role="lGtFl">
        <node concept="3u3nmq" id="aO" role="cd27D">
          <property role="3u3nmv" value="5315700730317313450" />
        </node>
      </node>
    </node>
    <node concept="3uibUv" id="aI" role="1zkMxy">
      <ref role="3uigEE" to="79pl:~BaseConstraintsDescriptor" resolve="BaseConstraintsDescriptor" />
      <node concept="cd27G" id="aP" role="lGtFl">
        <node concept="3u3nmq" id="aQ" role="cd27D">
          <property role="3u3nmv" value="5315700730317313450" />
        </node>
      </node>
    </node>
    <node concept="3clFbW" id="aJ" role="jymVt">
      <node concept="3cqZAl" id="aR" role="3clF45">
        <node concept="cd27G" id="aV" role="lGtFl">
          <node concept="3u3nmq" id="aW" role="cd27D">
            <property role="3u3nmv" value="5315700730317313450" />
          </node>
        </node>
      </node>
      <node concept="3clFbS" id="aS" role="3clF47">
        <node concept="XkiVB" id="aX" role="3cqZAp">
          <ref role="37wK5l" to="79pl:~BaseConstraintsDescriptor.&lt;init&gt;(org.jetbrains.mps.openapi.language.SAbstractConcept)" resolve="BaseConstraintsDescriptor" />
          <node concept="2YIFZM" id="aZ" role="37wK5m">
            <ref role="1Pybhc" to="2k9e:~MetaAdapterFactory" resolve="MetaAdapterFactory" />
            <ref role="37wK5l" to="2k9e:~MetaAdapterFactory.getConcept(long,long,long,java.lang.String):org.jetbrains.mps.openapi.language.SConcept" resolve="getConcept" />
            <node concept="1adDum" id="b1" role="37wK5m">
              <property role="1adDun" value="0x4a652d5536844d2dL" />
              <node concept="cd27G" id="b6" role="lGtFl">
                <node concept="3u3nmq" id="b7" role="cd27D">
                  <property role="3u3nmv" value="5315700730317313450" />
                </node>
              </node>
            </node>
            <node concept="1adDum" id="b2" role="37wK5m">
              <property role="1adDun" value="0x98c92ef46f124c44L" />
              <node concept="cd27G" id="b8" role="lGtFl">
                <node concept="3u3nmq" id="b9" role="cd27D">
                  <property role="3u3nmv" value="5315700730317313450" />
                </node>
              </node>
            </node>
            <node concept="1adDum" id="b3" role="37wK5m">
              <property role="1adDun" value="0x49c529a682d90a9dL" />
              <node concept="cd27G" id="ba" role="lGtFl">
                <node concept="3u3nmq" id="bb" role="cd27D">
                  <property role="3u3nmv" value="5315700730317313450" />
                </node>
              </node>
            </node>
            <node concept="Xl_RD" id="b4" role="37wK5m">
              <property role="Xl_RC" value="no.uio.mLab.decisions.references.laboratoryTest.structure.AspectLaboratoryTestWithTextResultReference" />
              <node concept="cd27G" id="bc" role="lGtFl">
                <node concept="3u3nmq" id="bd" role="cd27D">
                  <property role="3u3nmv" value="5315700730317313450" />
                </node>
              </node>
            </node>
            <node concept="cd27G" id="b5" role="lGtFl">
              <node concept="3u3nmq" id="be" role="cd27D">
                <property role="3u3nmv" value="5315700730317313450" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="b0" role="lGtFl">
            <node concept="3u3nmq" id="bf" role="cd27D">
              <property role="3u3nmv" value="5315700730317313450" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="aY" role="lGtFl">
          <node concept="3u3nmq" id="bg" role="cd27D">
            <property role="3u3nmv" value="5315700730317313450" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="aT" role="1B3o_S">
        <node concept="cd27G" id="bh" role="lGtFl">
          <node concept="3u3nmq" id="bi" role="cd27D">
            <property role="3u3nmv" value="5315700730317313450" />
          </node>
        </node>
      </node>
      <node concept="cd27G" id="aU" role="lGtFl">
        <node concept="3u3nmq" id="bj" role="cd27D">
          <property role="3u3nmv" value="5315700730317313450" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="aK" role="jymVt">
      <node concept="cd27G" id="bk" role="lGtFl">
        <node concept="3u3nmq" id="bl" role="cd27D">
          <property role="3u3nmv" value="5315700730317313450" />
        </node>
      </node>
    </node>
    <node concept="3clFb_" id="aL" role="jymVt">
      <property role="IEkAT" value="false" />
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="getSpecifiedReferences" />
      <property role="DiZV1" value="false" />
      <node concept="3Tmbuc" id="bm" role="1B3o_S">
        <node concept="cd27G" id="br" role="lGtFl">
          <node concept="3u3nmq" id="bs" role="cd27D">
            <property role="3u3nmv" value="5315700730317313450" />
          </node>
        </node>
      </node>
      <node concept="3uibUv" id="bn" role="3clF45">
        <ref role="3uigEE" to="33ny:~Map" resolve="Map" />
        <node concept="3uibUv" id="bt" role="11_B2D">
          <ref role="3uigEE" to="c17a:~SReferenceLink" resolve="SReferenceLink" />
          <node concept="cd27G" id="bw" role="lGtFl">
            <node concept="3u3nmq" id="bx" role="cd27D">
              <property role="3u3nmv" value="5315700730317313450" />
            </node>
          </node>
        </node>
        <node concept="3uibUv" id="bu" role="11_B2D">
          <ref role="3uigEE" to="ze1i:~ReferenceConstraintsDescriptor" resolve="ReferenceConstraintsDescriptor" />
          <node concept="cd27G" id="by" role="lGtFl">
            <node concept="3u3nmq" id="bz" role="cd27D">
              <property role="3u3nmv" value="5315700730317313450" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="bv" role="lGtFl">
          <node concept="3u3nmq" id="b$" role="cd27D">
            <property role="3u3nmv" value="5315700730317313450" />
          </node>
        </node>
      </node>
      <node concept="3clFbS" id="bo" role="3clF47">
        <node concept="3cpWs8" id="b_" role="3cqZAp">
          <node concept="3cpWsn" id="bD" role="3cpWs9">
            <property role="TrG5h" value="references" />
            <node concept="3uibUv" id="bF" role="1tU5fm">
              <ref role="3uigEE" to="33ny:~Map" resolve="Map" />
              <node concept="3uibUv" id="bI" role="11_B2D">
                <ref role="3uigEE" to="c17a:~SReferenceLink" resolve="SReferenceLink" />
                <node concept="cd27G" id="bL" role="lGtFl">
                  <node concept="3u3nmq" id="bM" role="cd27D">
                    <property role="3u3nmv" value="5315700730317313450" />
                  </node>
                </node>
              </node>
              <node concept="3uibUv" id="bJ" role="11_B2D">
                <ref role="3uigEE" to="ze1i:~ReferenceConstraintsDescriptor" resolve="ReferenceConstraintsDescriptor" />
                <node concept="cd27G" id="bN" role="lGtFl">
                  <node concept="3u3nmq" id="bO" role="cd27D">
                    <property role="3u3nmv" value="5315700730317313450" />
                  </node>
                </node>
              </node>
              <node concept="cd27G" id="bK" role="lGtFl">
                <node concept="3u3nmq" id="bP" role="cd27D">
                  <property role="3u3nmv" value="5315700730317313450" />
                </node>
              </node>
            </node>
            <node concept="2ShNRf" id="bG" role="33vP2m">
              <node concept="1pGfFk" id="bQ" role="2ShVmc">
                <ref role="37wK5l" to="33ny:~HashMap.&lt;init&gt;()" resolve="HashMap" />
                <node concept="3uibUv" id="bS" role="1pMfVU">
                  <ref role="3uigEE" to="c17a:~SReferenceLink" resolve="SReferenceLink" />
                  <node concept="cd27G" id="bV" role="lGtFl">
                    <node concept="3u3nmq" id="bW" role="cd27D">
                      <property role="3u3nmv" value="5315700730317313450" />
                    </node>
                  </node>
                </node>
                <node concept="3uibUv" id="bT" role="1pMfVU">
                  <ref role="3uigEE" to="ze1i:~ReferenceConstraintsDescriptor" resolve="ReferenceConstraintsDescriptor" />
                  <node concept="cd27G" id="bX" role="lGtFl">
                    <node concept="3u3nmq" id="bY" role="cd27D">
                      <property role="3u3nmv" value="5315700730317313450" />
                    </node>
                  </node>
                </node>
                <node concept="cd27G" id="bU" role="lGtFl">
                  <node concept="3u3nmq" id="bZ" role="cd27D">
                    <property role="3u3nmv" value="5315700730317313450" />
                  </node>
                </node>
              </node>
              <node concept="cd27G" id="bR" role="lGtFl">
                <node concept="3u3nmq" id="c0" role="cd27D">
                  <property role="3u3nmv" value="5315700730317313450" />
                </node>
              </node>
            </node>
            <node concept="cd27G" id="bH" role="lGtFl">
              <node concept="3u3nmq" id="c1" role="cd27D">
                <property role="3u3nmv" value="5315700730317313450" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="bE" role="lGtFl">
            <node concept="3u3nmq" id="c2" role="cd27D">
              <property role="3u3nmv" value="5315700730317313450" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="bA" role="3cqZAp">
          <node concept="2OqwBi" id="c3" role="3clFbG">
            <node concept="37vLTw" id="c5" role="2Oq$k0">
              <ref role="3cqZAo" node="bD" resolve="references" />
              <node concept="cd27G" id="c8" role="lGtFl">
                <node concept="3u3nmq" id="c9" role="cd27D">
                  <property role="3u3nmv" value="5315700730317313450" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="c6" role="2OqNvi">
              <ref role="37wK5l" to="33ny:~Map.put(java.lang.Object,java.lang.Object):java.lang.Object" resolve="put" />
              <node concept="2YIFZM" id="ca" role="37wK5m">
                <ref role="1Pybhc" to="2k9e:~MetaAdapterFactory" resolve="MetaAdapterFactory" />
                <ref role="37wK5l" to="2k9e:~MetaAdapterFactory.getReferenceLink(long,long,long,long,java.lang.String):org.jetbrains.mps.openapi.language.SReferenceLink" resolve="getReferenceLink" />
                <node concept="1adDum" id="cd" role="37wK5m">
                  <property role="1adDun" value="0xc610a4ebc47c4f95L" />
                  <node concept="cd27G" id="cj" role="lGtFl">
                    <node concept="3u3nmq" id="ck" role="cd27D">
                      <property role="3u3nmv" value="5315700730317313450" />
                    </node>
                  </node>
                </node>
                <node concept="1adDum" id="ce" role="37wK5m">
                  <property role="1adDun" value="0xb56811236971769cL" />
                  <node concept="cd27G" id="cl" role="lGtFl">
                    <node concept="3u3nmq" id="cm" role="cd27D">
                      <property role="3u3nmv" value="5315700730317313450" />
                    </node>
                  </node>
                </node>
                <node concept="1adDum" id="cf" role="37wK5m">
                  <property role="1adDun" value="0x6c7943d5b299791dL" />
                  <node concept="cd27G" id="cn" role="lGtFl">
                    <node concept="3u3nmq" id="co" role="cd27D">
                      <property role="3u3nmv" value="5315700730317313450" />
                    </node>
                  </node>
                </node>
                <node concept="1adDum" id="cg" role="37wK5m">
                  <property role="1adDun" value="0x6c7943d5b29b8063L" />
                  <node concept="cd27G" id="cp" role="lGtFl">
                    <node concept="3u3nmq" id="cq" role="cd27D">
                      <property role="3u3nmv" value="5315700730317313450" />
                    </node>
                  </node>
                </node>
                <node concept="Xl_RD" id="ch" role="37wK5m">
                  <property role="Xl_RC" value="target" />
                  <node concept="cd27G" id="cr" role="lGtFl">
                    <node concept="3u3nmq" id="cs" role="cd27D">
                      <property role="3u3nmv" value="5315700730317313450" />
                    </node>
                  </node>
                </node>
                <node concept="cd27G" id="ci" role="lGtFl">
                  <node concept="3u3nmq" id="ct" role="cd27D">
                    <property role="3u3nmv" value="5315700730317313450" />
                  </node>
                </node>
              </node>
              <node concept="2ShNRf" id="cb" role="37wK5m">
                <node concept="YeOm9" id="cu" role="2ShVmc">
                  <node concept="1Y3b0j" id="cw" role="YeSDq">
                    <property role="2bfB8j" value="true" />
                    <ref role="1Y3XeK" to="79pl:~BaseReferenceConstraintsDescriptor" resolve="BaseReferenceConstraintsDescriptor" />
                    <ref role="37wK5l" to="79pl:~BaseReferenceConstraintsDescriptor.&lt;init&gt;(jetbrains.mps.smodel.adapter.ids.SReferenceLinkId,jetbrains.mps.smodel.runtime.ConstraintsDescriptor)" resolve="BaseReferenceConstraintsDescriptor" />
                    <node concept="2YIFZM" id="cy" role="37wK5m">
                      <ref role="1Pybhc" to="e8bb:~MetaIdFactory" resolve="MetaIdFactory" />
                      <ref role="37wK5l" to="e8bb:~MetaIdFactory.refId(long,long,long,long):jetbrains.mps.smodel.adapter.ids.SReferenceLinkId" resolve="refId" />
                      <node concept="1adDum" id="cC" role="37wK5m">
                        <property role="1adDun" value="0xc610a4ebc47c4f95L" />
                        <node concept="cd27G" id="cH" role="lGtFl">
                          <node concept="3u3nmq" id="cI" role="cd27D">
                            <property role="3u3nmv" value="5315700730317313450" />
                          </node>
                        </node>
                      </node>
                      <node concept="1adDum" id="cD" role="37wK5m">
                        <property role="1adDun" value="0xb56811236971769cL" />
                        <node concept="cd27G" id="cJ" role="lGtFl">
                          <node concept="3u3nmq" id="cK" role="cd27D">
                            <property role="3u3nmv" value="5315700730317313450" />
                          </node>
                        </node>
                      </node>
                      <node concept="1adDum" id="cE" role="37wK5m">
                        <property role="1adDun" value="0x6c7943d5b299791dL" />
                        <node concept="cd27G" id="cL" role="lGtFl">
                          <node concept="3u3nmq" id="cM" role="cd27D">
                            <property role="3u3nmv" value="5315700730317313450" />
                          </node>
                        </node>
                      </node>
                      <node concept="1adDum" id="cF" role="37wK5m">
                        <property role="1adDun" value="0x6c7943d5b29b8063L" />
                        <node concept="cd27G" id="cN" role="lGtFl">
                          <node concept="3u3nmq" id="cO" role="cd27D">
                            <property role="3u3nmv" value="5315700730317313450" />
                          </node>
                        </node>
                      </node>
                      <node concept="cd27G" id="cG" role="lGtFl">
                        <node concept="3u3nmq" id="cP" role="cd27D">
                          <property role="3u3nmv" value="5315700730317313450" />
                        </node>
                      </node>
                    </node>
                    <node concept="3Tm1VV" id="cz" role="1B3o_S">
                      <node concept="cd27G" id="cQ" role="lGtFl">
                        <node concept="3u3nmq" id="cR" role="cd27D">
                          <property role="3u3nmv" value="5315700730317313450" />
                        </node>
                      </node>
                    </node>
                    <node concept="Xjq3P" id="c$" role="37wK5m">
                      <node concept="cd27G" id="cS" role="lGtFl">
                        <node concept="3u3nmq" id="cT" role="cd27D">
                          <property role="3u3nmv" value="5315700730317313450" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFb_" id="c_" role="jymVt">
                      <property role="IEkAT" value="false" />
                      <property role="1EzhhJ" value="false" />
                      <property role="TrG5h" value="hasOwnScopeProvider" />
                      <property role="DiZV1" value="false" />
                      <node concept="3Tm1VV" id="cU" role="1B3o_S">
                        <node concept="cd27G" id="cZ" role="lGtFl">
                          <node concept="3u3nmq" id="d0" role="cd27D">
                            <property role="3u3nmv" value="5315700730317313450" />
                          </node>
                        </node>
                      </node>
                      <node concept="10P_77" id="cV" role="3clF45">
                        <node concept="cd27G" id="d1" role="lGtFl">
                          <node concept="3u3nmq" id="d2" role="cd27D">
                            <property role="3u3nmv" value="5315700730317313450" />
                          </node>
                        </node>
                      </node>
                      <node concept="3clFbS" id="cW" role="3clF47">
                        <node concept="3clFbF" id="d3" role="3cqZAp">
                          <node concept="3clFbT" id="d5" role="3clFbG">
                            <property role="3clFbU" value="true" />
                            <node concept="cd27G" id="d7" role="lGtFl">
                              <node concept="3u3nmq" id="d8" role="cd27D">
                                <property role="3u3nmv" value="5315700730317313450" />
                              </node>
                            </node>
                          </node>
                          <node concept="cd27G" id="d6" role="lGtFl">
                            <node concept="3u3nmq" id="d9" role="cd27D">
                              <property role="3u3nmv" value="5315700730317313450" />
                            </node>
                          </node>
                        </node>
                        <node concept="cd27G" id="d4" role="lGtFl">
                          <node concept="3u3nmq" id="da" role="cd27D">
                            <property role="3u3nmv" value="5315700730317313450" />
                          </node>
                        </node>
                      </node>
                      <node concept="2AHcQZ" id="cX" role="2AJF6D">
                        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
                        <node concept="cd27G" id="db" role="lGtFl">
                          <node concept="3u3nmq" id="dc" role="cd27D">
                            <property role="3u3nmv" value="5315700730317313450" />
                          </node>
                        </node>
                      </node>
                      <node concept="cd27G" id="cY" role="lGtFl">
                        <node concept="3u3nmq" id="dd" role="cd27D">
                          <property role="3u3nmv" value="5315700730317313450" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFb_" id="cA" role="jymVt">
                      <property role="IEkAT" value="false" />
                      <property role="1EzhhJ" value="false" />
                      <property role="TrG5h" value="getScopeProvider" />
                      <property role="DiZV1" value="false" />
                      <node concept="3Tm1VV" id="de" role="1B3o_S">
                        <node concept="cd27G" id="dk" role="lGtFl">
                          <node concept="3u3nmq" id="dl" role="cd27D">
                            <property role="3u3nmv" value="5315700730317313450" />
                          </node>
                        </node>
                      </node>
                      <node concept="3uibUv" id="df" role="3clF45">
                        <ref role="3uigEE" to="ze1i:~ReferenceScopeProvider" resolve="ReferenceScopeProvider" />
                        <node concept="cd27G" id="dm" role="lGtFl">
                          <node concept="3u3nmq" id="dn" role="cd27D">
                            <property role="3u3nmv" value="5315700730317313450" />
                          </node>
                        </node>
                      </node>
                      <node concept="2AHcQZ" id="dg" role="2AJF6D">
                        <ref role="2AI5Lk" to="mhfm:~Nullable" resolve="Nullable" />
                        <node concept="cd27G" id="do" role="lGtFl">
                          <node concept="3u3nmq" id="dp" role="cd27D">
                            <property role="3u3nmv" value="5315700730317313450" />
                          </node>
                        </node>
                      </node>
                      <node concept="3clFbS" id="dh" role="3clF47">
                        <node concept="3cpWs6" id="dq" role="3cqZAp">
                          <node concept="2ShNRf" id="ds" role="3cqZAk">
                            <node concept="YeOm9" id="du" role="2ShVmc">
                              <node concept="1Y3b0j" id="dw" role="YeSDq">
                                <property role="2bfB8j" value="true" />
                                <ref role="1Y3XeK" to="79pl:~BaseScopeProvider" resolve="BaseScopeProvider" />
                                <ref role="37wK5l" to="79pl:~BaseScopeProvider.&lt;init&gt;()" resolve="BaseScopeProvider" />
                                <node concept="3Tm1VV" id="dy" role="1B3o_S">
                                  <node concept="cd27G" id="dA" role="lGtFl">
                                    <node concept="3u3nmq" id="dB" role="cd27D">
                                      <property role="3u3nmv" value="5315700730317313450" />
                                    </node>
                                  </node>
                                </node>
                                <node concept="3clFb_" id="dz" role="jymVt">
                                  <property role="TrG5h" value="getSearchScopeValidatorNode" />
                                  <node concept="3Tm1VV" id="dC" role="1B3o_S">
                                    <node concept="cd27G" id="dH" role="lGtFl">
                                      <node concept="3u3nmq" id="dI" role="cd27D">
                                        <property role="3u3nmv" value="5315700730317313450" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="3clFbS" id="dD" role="3clF47">
                                    <node concept="3cpWs6" id="dJ" role="3cqZAp">
                                      <node concept="1dyn4i" id="dL" role="3cqZAk">
                                        <property role="1zomUR" value="true" />
                                        <property role="1dyqJU" value="breakingNode" />
                                        <node concept="2ShNRf" id="dN" role="1dyrYi">
                                          <node concept="1pGfFk" id="dP" role="2ShVmc">
                                            <ref role="37wK5l" to="w1kc:~SNodePointer.&lt;init&gt;(java.lang.String,java.lang.String)" resolve="SNodePointer" />
                                            <node concept="Xl_RD" id="dR" role="37wK5m">
                                              <property role="Xl_RC" value="r:e4a7a6f9-a1de-4f08-9b98-b5f2b9ecf3a4(no.uio.mLab.decisions.references.laboratoryTest.constraints)" />
                                              <node concept="cd27G" id="dU" role="lGtFl">
                                                <node concept="3u3nmq" id="dV" role="cd27D">
                                                  <property role="3u3nmv" value="5315700730317313450" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="Xl_RD" id="dS" role="37wK5m">
                                              <property role="Xl_RC" value="5315700730317313452" />
                                              <node concept="cd27G" id="dW" role="lGtFl">
                                                <node concept="3u3nmq" id="dX" role="cd27D">
                                                  <property role="3u3nmv" value="5315700730317313450" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="cd27G" id="dT" role="lGtFl">
                                              <node concept="3u3nmq" id="dY" role="cd27D">
                                                <property role="3u3nmv" value="5315700730317313450" />
                                              </node>
                                            </node>
                                          </node>
                                          <node concept="cd27G" id="dQ" role="lGtFl">
                                            <node concept="3u3nmq" id="dZ" role="cd27D">
                                              <property role="3u3nmv" value="5315700730317313450" />
                                            </node>
                                          </node>
                                        </node>
                                        <node concept="cd27G" id="dO" role="lGtFl">
                                          <node concept="3u3nmq" id="e0" role="cd27D">
                                            <property role="3u3nmv" value="5315700730317313450" />
                                          </node>
                                        </node>
                                      </node>
                                      <node concept="cd27G" id="dM" role="lGtFl">
                                        <node concept="3u3nmq" id="e1" role="cd27D">
                                          <property role="3u3nmv" value="5315700730317313450" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="cd27G" id="dK" role="lGtFl">
                                      <node concept="3u3nmq" id="e2" role="cd27D">
                                        <property role="3u3nmv" value="5315700730317313450" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="3uibUv" id="dE" role="3clF45">
                                    <ref role="3uigEE" to="mhbf:~SNodeReference" resolve="SNodeReference" />
                                    <node concept="cd27G" id="e3" role="lGtFl">
                                      <node concept="3u3nmq" id="e4" role="cd27D">
                                        <property role="3u3nmv" value="5315700730317313450" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="2AHcQZ" id="dF" role="2AJF6D">
                                    <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
                                    <node concept="cd27G" id="e5" role="lGtFl">
                                      <node concept="3u3nmq" id="e6" role="cd27D">
                                        <property role="3u3nmv" value="5315700730317313450" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="cd27G" id="dG" role="lGtFl">
                                    <node concept="3u3nmq" id="e7" role="cd27D">
                                      <property role="3u3nmv" value="5315700730317313450" />
                                    </node>
                                  </node>
                                </node>
                                <node concept="3clFb_" id="d$" role="jymVt">
                                  <property role="IEkAT" value="false" />
                                  <property role="1EzhhJ" value="false" />
                                  <property role="TrG5h" value="createScope" />
                                  <property role="DiZV1" value="false" />
                                  <node concept="37vLTG" id="e8" role="3clF46">
                                    <property role="TrG5h" value="operationContext" />
                                    <property role="3TUv4t" value="true" />
                                    <node concept="3uibUv" id="ef" role="1tU5fm">
                                      <ref role="3uigEE" to="w1kc:~IOperationContext" resolve="IOperationContext" />
                                      <node concept="cd27G" id="eh" role="lGtFl">
                                        <node concept="3u3nmq" id="ei" role="cd27D">
                                          <property role="3u3nmv" value="5315700730317313450" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="cd27G" id="eg" role="lGtFl">
                                      <node concept="3u3nmq" id="ej" role="cd27D">
                                        <property role="3u3nmv" value="5315700730317313450" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="37vLTG" id="e9" role="3clF46">
                                    <property role="TrG5h" value="_context" />
                                    <property role="3TUv4t" value="true" />
                                    <node concept="3uibUv" id="ek" role="1tU5fm">
                                      <ref role="3uigEE" to="ze1i:~ReferenceConstraintsContext" resolve="ReferenceConstraintsContext" />
                                      <node concept="cd27G" id="em" role="lGtFl">
                                        <node concept="3u3nmq" id="en" role="cd27D">
                                          <property role="3u3nmv" value="5315700730317313450" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="cd27G" id="el" role="lGtFl">
                                      <node concept="3u3nmq" id="eo" role="cd27D">
                                        <property role="3u3nmv" value="5315700730317313450" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="3Tm1VV" id="ea" role="1B3o_S">
                                    <node concept="cd27G" id="ep" role="lGtFl">
                                      <node concept="3u3nmq" id="eq" role="cd27D">
                                        <property role="3u3nmv" value="5315700730317313450" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="3uibUv" id="eb" role="3clF45">
                                    <ref role="3uigEE" to="35tq:~Scope" resolve="Scope" />
                                    <node concept="cd27G" id="er" role="lGtFl">
                                      <node concept="3u3nmq" id="es" role="cd27D">
                                        <property role="3u3nmv" value="5315700730317313450" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="3clFbS" id="ec" role="3clF47">
                                    <node concept="9aQIb" id="et" role="3cqZAp">
                                      <node concept="3clFbS" id="ev" role="9aQI4">
                                        <node concept="3cpWs8" id="ex" role="3cqZAp">
                                          <node concept="3cpWsn" id="e$" role="3cpWs9">
                                            <property role="TrG5h" value="scope" />
                                            <node concept="3uibUv" id="eA" role="1tU5fm">
                                              <ref role="3uigEE" to="35tq:~Scope" resolve="Scope" />
                                              <node concept="cd27G" id="eD" role="lGtFl">
                                                <node concept="3u3nmq" id="eE" role="cd27D">
                                                  <property role="3u3nmv" value="5315700730317313450" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="2YIFZM" id="eB" role="33vP2m">
                                              <ref role="1Pybhc" to="35tq:~Scope" resolve="Scope" />
                                              <ref role="37wK5l" to="35tq:~Scope.getScope(org.jetbrains.mps.openapi.model.SNode,org.jetbrains.mps.openapi.language.SContainmentLink,int,org.jetbrains.mps.openapi.language.SAbstractConcept):jetbrains.mps.scope.Scope" resolve="getScope" />
                                              <node concept="2OqwBi" id="eF" role="37wK5m">
                                                <node concept="37vLTw" id="eK" role="2Oq$k0">
                                                  <ref role="3cqZAo" node="e9" resolve="_context" />
                                                  <node concept="cd27G" id="eN" role="lGtFl">
                                                    <node concept="3u3nmq" id="eO" role="cd27D">
                                                      <property role="3u3nmv" value="5315700730317313450" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="liA8E" id="eL" role="2OqNvi">
                                                  <ref role="37wK5l" to="ze1i:~ReferenceConstraintsContext.getContextNode():org.jetbrains.mps.openapi.model.SNode" resolve="getContextNode" />
                                                  <node concept="cd27G" id="eP" role="lGtFl">
                                                    <node concept="3u3nmq" id="eQ" role="cd27D">
                                                      <property role="3u3nmv" value="5315700730317313450" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="cd27G" id="eM" role="lGtFl">
                                                  <node concept="3u3nmq" id="eR" role="cd27D">
                                                    <property role="3u3nmv" value="5315700730317313450" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="2OqwBi" id="eG" role="37wK5m">
                                                <node concept="liA8E" id="eS" role="2OqNvi">
                                                  <ref role="37wK5l" to="ze1i:~ReferenceConstraintsContext.getContainmentLink():org.jetbrains.mps.openapi.language.SContainmentLink" resolve="getContainmentLink" />
                                                  <node concept="cd27G" id="eV" role="lGtFl">
                                                    <node concept="3u3nmq" id="eW" role="cd27D">
                                                      <property role="3u3nmv" value="5315700730317313450" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="37vLTw" id="eT" role="2Oq$k0">
                                                  <ref role="3cqZAo" node="e9" resolve="_context" />
                                                  <node concept="cd27G" id="eX" role="lGtFl">
                                                    <node concept="3u3nmq" id="eY" role="cd27D">
                                                      <property role="3u3nmv" value="5315700730317313450" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="cd27G" id="eU" role="lGtFl">
                                                  <node concept="3u3nmq" id="eZ" role="cd27D">
                                                    <property role="3u3nmv" value="5315700730317313450" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="2OqwBi" id="eH" role="37wK5m">
                                                <node concept="37vLTw" id="f0" role="2Oq$k0">
                                                  <ref role="3cqZAo" node="e9" resolve="_context" />
                                                  <node concept="cd27G" id="f3" role="lGtFl">
                                                    <node concept="3u3nmq" id="f4" role="cd27D">
                                                      <property role="3u3nmv" value="5315700730317313450" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="liA8E" id="f1" role="2OqNvi">
                                                  <ref role="37wK5l" to="ze1i:~ReferenceConstraintsContext.getPosition():int" resolve="getPosition" />
                                                  <node concept="cd27G" id="f5" role="lGtFl">
                                                    <node concept="3u3nmq" id="f6" role="cd27D">
                                                      <property role="3u3nmv" value="5315700730317313450" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="cd27G" id="f2" role="lGtFl">
                                                  <node concept="3u3nmq" id="f7" role="cd27D">
                                                    <property role="3u3nmv" value="5315700730317313450" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="35c_gC" id="eI" role="37wK5m">
                                                <ref role="35c_gD" to="ruww:4B5aqq2QmL6" resolve="LaboratoryTestWithTextResultVariable" />
                                                <node concept="cd27G" id="f8" role="lGtFl">
                                                  <node concept="3u3nmq" id="f9" role="cd27D">
                                                    <property role="3u3nmv" value="5315700730317313450" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="cd27G" id="eJ" role="lGtFl">
                                                <node concept="3u3nmq" id="fa" role="cd27D">
                                                  <property role="3u3nmv" value="5315700730317313450" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="cd27G" id="eC" role="lGtFl">
                                              <node concept="3u3nmq" id="fb" role="cd27D">
                                                <property role="3u3nmv" value="5315700730317313450" />
                                              </node>
                                            </node>
                                          </node>
                                          <node concept="cd27G" id="e_" role="lGtFl">
                                            <node concept="3u3nmq" id="fc" role="cd27D">
                                              <property role="3u3nmv" value="5315700730317313450" />
                                            </node>
                                          </node>
                                        </node>
                                        <node concept="3cpWs6" id="ey" role="3cqZAp">
                                          <node concept="3K4zz7" id="fd" role="3cqZAk">
                                            <node concept="2ShNRf" id="ff" role="3K4E3e">
                                              <node concept="1pGfFk" id="fj" role="2ShVmc">
                                                <ref role="37wK5l" to="35tq:~EmptyScope.&lt;init&gt;()" resolve="EmptyScope" />
                                                <node concept="cd27G" id="fl" role="lGtFl">
                                                  <node concept="3u3nmq" id="fm" role="cd27D">
                                                    <property role="3u3nmv" value="5315700730317313450" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="cd27G" id="fk" role="lGtFl">
                                                <node concept="3u3nmq" id="fn" role="cd27D">
                                                  <property role="3u3nmv" value="5315700730317313450" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="37vLTw" id="fg" role="3K4GZi">
                                              <ref role="3cqZAo" node="e$" resolve="scope" />
                                              <node concept="cd27G" id="fo" role="lGtFl">
                                                <node concept="3u3nmq" id="fp" role="cd27D">
                                                  <property role="3u3nmv" value="5315700730317313450" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="3clFbC" id="fh" role="3K4Cdx">
                                              <node concept="10Nm6u" id="fq" role="3uHU7w">
                                                <node concept="cd27G" id="ft" role="lGtFl">
                                                  <node concept="3u3nmq" id="fu" role="cd27D">
                                                    <property role="3u3nmv" value="5315700730317313450" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="37vLTw" id="fr" role="3uHU7B">
                                                <ref role="3cqZAo" node="e$" resolve="scope" />
                                                <node concept="cd27G" id="fv" role="lGtFl">
                                                  <node concept="3u3nmq" id="fw" role="cd27D">
                                                    <property role="3u3nmv" value="5315700730317313450" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="cd27G" id="fs" role="lGtFl">
                                                <node concept="3u3nmq" id="fx" role="cd27D">
                                                  <property role="3u3nmv" value="5315700730317313450" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="cd27G" id="fi" role="lGtFl">
                                              <node concept="3u3nmq" id="fy" role="cd27D">
                                                <property role="3u3nmv" value="5315700730317313450" />
                                              </node>
                                            </node>
                                          </node>
                                          <node concept="cd27G" id="fe" role="lGtFl">
                                            <node concept="3u3nmq" id="fz" role="cd27D">
                                              <property role="3u3nmv" value="5315700730317313450" />
                                            </node>
                                          </node>
                                        </node>
                                        <node concept="cd27G" id="ez" role="lGtFl">
                                          <node concept="3u3nmq" id="f$" role="cd27D">
                                            <property role="3u3nmv" value="5315700730317313450" />
                                          </node>
                                        </node>
                                      </node>
                                      <node concept="cd27G" id="ew" role="lGtFl">
                                        <node concept="3u3nmq" id="f_" role="cd27D">
                                          <property role="3u3nmv" value="5315700730317313450" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="cd27G" id="eu" role="lGtFl">
                                      <node concept="3u3nmq" id="fA" role="cd27D">
                                        <property role="3u3nmv" value="5315700730317313450" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="2AHcQZ" id="ed" role="2AJF6D">
                                    <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
                                    <node concept="cd27G" id="fB" role="lGtFl">
                                      <node concept="3u3nmq" id="fC" role="cd27D">
                                        <property role="3u3nmv" value="5315700730317313450" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="cd27G" id="ee" role="lGtFl">
                                    <node concept="3u3nmq" id="fD" role="cd27D">
                                      <property role="3u3nmv" value="5315700730317313450" />
                                    </node>
                                  </node>
                                </node>
                                <node concept="cd27G" id="d_" role="lGtFl">
                                  <node concept="3u3nmq" id="fE" role="cd27D">
                                    <property role="3u3nmv" value="5315700730317313450" />
                                  </node>
                                </node>
                              </node>
                              <node concept="cd27G" id="dx" role="lGtFl">
                                <node concept="3u3nmq" id="fF" role="cd27D">
                                  <property role="3u3nmv" value="5315700730317313450" />
                                </node>
                              </node>
                            </node>
                            <node concept="cd27G" id="dv" role="lGtFl">
                              <node concept="3u3nmq" id="fG" role="cd27D">
                                <property role="3u3nmv" value="5315700730317313450" />
                              </node>
                            </node>
                          </node>
                          <node concept="cd27G" id="dt" role="lGtFl">
                            <node concept="3u3nmq" id="fH" role="cd27D">
                              <property role="3u3nmv" value="5315700730317313450" />
                            </node>
                          </node>
                        </node>
                        <node concept="cd27G" id="dr" role="lGtFl">
                          <node concept="3u3nmq" id="fI" role="cd27D">
                            <property role="3u3nmv" value="5315700730317313450" />
                          </node>
                        </node>
                      </node>
                      <node concept="2AHcQZ" id="di" role="2AJF6D">
                        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
                        <node concept="cd27G" id="fJ" role="lGtFl">
                          <node concept="3u3nmq" id="fK" role="cd27D">
                            <property role="3u3nmv" value="5315700730317313450" />
                          </node>
                        </node>
                      </node>
                      <node concept="cd27G" id="dj" role="lGtFl">
                        <node concept="3u3nmq" id="fL" role="cd27D">
                          <property role="3u3nmv" value="5315700730317313450" />
                        </node>
                      </node>
                    </node>
                    <node concept="cd27G" id="cB" role="lGtFl">
                      <node concept="3u3nmq" id="fM" role="cd27D">
                        <property role="3u3nmv" value="5315700730317313450" />
                      </node>
                    </node>
                  </node>
                  <node concept="cd27G" id="cx" role="lGtFl">
                    <node concept="3u3nmq" id="fN" role="cd27D">
                      <property role="3u3nmv" value="5315700730317313450" />
                    </node>
                  </node>
                </node>
                <node concept="cd27G" id="cv" role="lGtFl">
                  <node concept="3u3nmq" id="fO" role="cd27D">
                    <property role="3u3nmv" value="5315700730317313450" />
                  </node>
                </node>
              </node>
              <node concept="cd27G" id="cc" role="lGtFl">
                <node concept="3u3nmq" id="fP" role="cd27D">
                  <property role="3u3nmv" value="5315700730317313450" />
                </node>
              </node>
            </node>
            <node concept="cd27G" id="c7" role="lGtFl">
              <node concept="3u3nmq" id="fQ" role="cd27D">
                <property role="3u3nmv" value="5315700730317313450" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="c4" role="lGtFl">
            <node concept="3u3nmq" id="fR" role="cd27D">
              <property role="3u3nmv" value="5315700730317313450" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="bB" role="3cqZAp">
          <node concept="37vLTw" id="fS" role="3clFbG">
            <ref role="3cqZAo" node="bD" resolve="references" />
            <node concept="cd27G" id="fU" role="lGtFl">
              <node concept="3u3nmq" id="fV" role="cd27D">
                <property role="3u3nmv" value="5315700730317313450" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="fT" role="lGtFl">
            <node concept="3u3nmq" id="fW" role="cd27D">
              <property role="3u3nmv" value="5315700730317313450" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="bC" role="lGtFl">
          <node concept="3u3nmq" id="fX" role="cd27D">
            <property role="3u3nmv" value="5315700730317313450" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="bp" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <node concept="cd27G" id="fY" role="lGtFl">
          <node concept="3u3nmq" id="fZ" role="cd27D">
            <property role="3u3nmv" value="5315700730317313450" />
          </node>
        </node>
      </node>
      <node concept="cd27G" id="bq" role="lGtFl">
        <node concept="3u3nmq" id="g0" role="cd27D">
          <property role="3u3nmv" value="5315700730317313450" />
        </node>
      </node>
    </node>
    <node concept="cd27G" id="aM" role="lGtFl">
      <node concept="3u3nmq" id="g1" role="cd27D">
        <property role="3u3nmv" value="5315700730317313450" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="g2">
    <property role="3GE5qa" value="aspect.references.specimenType" />
    <property role="TrG5h" value="AspectSpecimenTypeReference_Constraints" />
    <node concept="3Tm1VV" id="g3" role="1B3o_S">
      <node concept="cd27G" id="g9" role="lGtFl">
        <node concept="3u3nmq" id="ga" role="cd27D">
          <property role="3u3nmv" value="7282862830136195762" />
        </node>
      </node>
    </node>
    <node concept="3uibUv" id="g4" role="1zkMxy">
      <ref role="3uigEE" to="79pl:~BaseConstraintsDescriptor" resolve="BaseConstraintsDescriptor" />
      <node concept="cd27G" id="gb" role="lGtFl">
        <node concept="3u3nmq" id="gc" role="cd27D">
          <property role="3u3nmv" value="7282862830136195762" />
        </node>
      </node>
    </node>
    <node concept="3clFbW" id="g5" role="jymVt">
      <node concept="3cqZAl" id="gd" role="3clF45">
        <node concept="cd27G" id="gh" role="lGtFl">
          <node concept="3u3nmq" id="gi" role="cd27D">
            <property role="3u3nmv" value="7282862830136195762" />
          </node>
        </node>
      </node>
      <node concept="3clFbS" id="ge" role="3clF47">
        <node concept="XkiVB" id="gj" role="3cqZAp">
          <ref role="37wK5l" to="79pl:~BaseConstraintsDescriptor.&lt;init&gt;(org.jetbrains.mps.openapi.language.SAbstractConcept)" resolve="BaseConstraintsDescriptor" />
          <node concept="2YIFZM" id="gl" role="37wK5m">
            <ref role="1Pybhc" to="2k9e:~MetaAdapterFactory" resolve="MetaAdapterFactory" />
            <ref role="37wK5l" to="2k9e:~MetaAdapterFactory.getConcept(long,long,long,java.lang.String):org.jetbrains.mps.openapi.language.SConcept" resolve="getConcept" />
            <node concept="1adDum" id="gn" role="37wK5m">
              <property role="1adDun" value="0x4a652d5536844d2dL" />
              <node concept="cd27G" id="gs" role="lGtFl">
                <node concept="3u3nmq" id="gt" role="cd27D">
                  <property role="3u3nmv" value="7282862830136195762" />
                </node>
              </node>
            </node>
            <node concept="1adDum" id="go" role="37wK5m">
              <property role="1adDun" value="0x98c92ef46f124c44L" />
              <node concept="cd27G" id="gu" role="lGtFl">
                <node concept="3u3nmq" id="gv" role="cd27D">
                  <property role="3u3nmv" value="7282862830136195762" />
                </node>
              </node>
            </node>
            <node concept="1adDum" id="gp" role="37wK5m">
              <property role="1adDun" value="0x6511ed286229f47fL" />
              <node concept="cd27G" id="gw" role="lGtFl">
                <node concept="3u3nmq" id="gx" role="cd27D">
                  <property role="3u3nmv" value="7282862830136195762" />
                </node>
              </node>
            </node>
            <node concept="Xl_RD" id="gq" role="37wK5m">
              <property role="Xl_RC" value="no.uio.mLab.decisions.references.laboratoryTest.structure.AspectSpecimenTypeReference" />
              <node concept="cd27G" id="gy" role="lGtFl">
                <node concept="3u3nmq" id="gz" role="cd27D">
                  <property role="3u3nmv" value="7282862830136195762" />
                </node>
              </node>
            </node>
            <node concept="cd27G" id="gr" role="lGtFl">
              <node concept="3u3nmq" id="g$" role="cd27D">
                <property role="3u3nmv" value="7282862830136195762" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="gm" role="lGtFl">
            <node concept="3u3nmq" id="g_" role="cd27D">
              <property role="3u3nmv" value="7282862830136195762" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="gk" role="lGtFl">
          <node concept="3u3nmq" id="gA" role="cd27D">
            <property role="3u3nmv" value="7282862830136195762" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="gf" role="1B3o_S">
        <node concept="cd27G" id="gB" role="lGtFl">
          <node concept="3u3nmq" id="gC" role="cd27D">
            <property role="3u3nmv" value="7282862830136195762" />
          </node>
        </node>
      </node>
      <node concept="cd27G" id="gg" role="lGtFl">
        <node concept="3u3nmq" id="gD" role="cd27D">
          <property role="3u3nmv" value="7282862830136195762" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="g6" role="jymVt">
      <node concept="cd27G" id="gE" role="lGtFl">
        <node concept="3u3nmq" id="gF" role="cd27D">
          <property role="3u3nmv" value="7282862830136195762" />
        </node>
      </node>
    </node>
    <node concept="3clFb_" id="g7" role="jymVt">
      <property role="IEkAT" value="false" />
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="getSpecifiedReferences" />
      <property role="DiZV1" value="false" />
      <node concept="3Tmbuc" id="gG" role="1B3o_S">
        <node concept="cd27G" id="gL" role="lGtFl">
          <node concept="3u3nmq" id="gM" role="cd27D">
            <property role="3u3nmv" value="7282862830136195762" />
          </node>
        </node>
      </node>
      <node concept="3uibUv" id="gH" role="3clF45">
        <ref role="3uigEE" to="33ny:~Map" resolve="Map" />
        <node concept="3uibUv" id="gN" role="11_B2D">
          <ref role="3uigEE" to="c17a:~SReferenceLink" resolve="SReferenceLink" />
          <node concept="cd27G" id="gQ" role="lGtFl">
            <node concept="3u3nmq" id="gR" role="cd27D">
              <property role="3u3nmv" value="7282862830136195762" />
            </node>
          </node>
        </node>
        <node concept="3uibUv" id="gO" role="11_B2D">
          <ref role="3uigEE" to="ze1i:~ReferenceConstraintsDescriptor" resolve="ReferenceConstraintsDescriptor" />
          <node concept="cd27G" id="gS" role="lGtFl">
            <node concept="3u3nmq" id="gT" role="cd27D">
              <property role="3u3nmv" value="7282862830136195762" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="gP" role="lGtFl">
          <node concept="3u3nmq" id="gU" role="cd27D">
            <property role="3u3nmv" value="7282862830136195762" />
          </node>
        </node>
      </node>
      <node concept="3clFbS" id="gI" role="3clF47">
        <node concept="3cpWs8" id="gV" role="3cqZAp">
          <node concept="3cpWsn" id="gZ" role="3cpWs9">
            <property role="TrG5h" value="references" />
            <node concept="3uibUv" id="h1" role="1tU5fm">
              <ref role="3uigEE" to="33ny:~Map" resolve="Map" />
              <node concept="3uibUv" id="h4" role="11_B2D">
                <ref role="3uigEE" to="c17a:~SReferenceLink" resolve="SReferenceLink" />
                <node concept="cd27G" id="h7" role="lGtFl">
                  <node concept="3u3nmq" id="h8" role="cd27D">
                    <property role="3u3nmv" value="7282862830136195762" />
                  </node>
                </node>
              </node>
              <node concept="3uibUv" id="h5" role="11_B2D">
                <ref role="3uigEE" to="ze1i:~ReferenceConstraintsDescriptor" resolve="ReferenceConstraintsDescriptor" />
                <node concept="cd27G" id="h9" role="lGtFl">
                  <node concept="3u3nmq" id="ha" role="cd27D">
                    <property role="3u3nmv" value="7282862830136195762" />
                  </node>
                </node>
              </node>
              <node concept="cd27G" id="h6" role="lGtFl">
                <node concept="3u3nmq" id="hb" role="cd27D">
                  <property role="3u3nmv" value="7282862830136195762" />
                </node>
              </node>
            </node>
            <node concept="2ShNRf" id="h2" role="33vP2m">
              <node concept="1pGfFk" id="hc" role="2ShVmc">
                <ref role="37wK5l" to="33ny:~HashMap.&lt;init&gt;()" resolve="HashMap" />
                <node concept="3uibUv" id="he" role="1pMfVU">
                  <ref role="3uigEE" to="c17a:~SReferenceLink" resolve="SReferenceLink" />
                  <node concept="cd27G" id="hh" role="lGtFl">
                    <node concept="3u3nmq" id="hi" role="cd27D">
                      <property role="3u3nmv" value="7282862830136195762" />
                    </node>
                  </node>
                </node>
                <node concept="3uibUv" id="hf" role="1pMfVU">
                  <ref role="3uigEE" to="ze1i:~ReferenceConstraintsDescriptor" resolve="ReferenceConstraintsDescriptor" />
                  <node concept="cd27G" id="hj" role="lGtFl">
                    <node concept="3u3nmq" id="hk" role="cd27D">
                      <property role="3u3nmv" value="7282862830136195762" />
                    </node>
                  </node>
                </node>
                <node concept="cd27G" id="hg" role="lGtFl">
                  <node concept="3u3nmq" id="hl" role="cd27D">
                    <property role="3u3nmv" value="7282862830136195762" />
                  </node>
                </node>
              </node>
              <node concept="cd27G" id="hd" role="lGtFl">
                <node concept="3u3nmq" id="hm" role="cd27D">
                  <property role="3u3nmv" value="7282862830136195762" />
                </node>
              </node>
            </node>
            <node concept="cd27G" id="h3" role="lGtFl">
              <node concept="3u3nmq" id="hn" role="cd27D">
                <property role="3u3nmv" value="7282862830136195762" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="h0" role="lGtFl">
            <node concept="3u3nmq" id="ho" role="cd27D">
              <property role="3u3nmv" value="7282862830136195762" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="gW" role="3cqZAp">
          <node concept="2OqwBi" id="hp" role="3clFbG">
            <node concept="37vLTw" id="hr" role="2Oq$k0">
              <ref role="3cqZAo" node="gZ" resolve="references" />
              <node concept="cd27G" id="hu" role="lGtFl">
                <node concept="3u3nmq" id="hv" role="cd27D">
                  <property role="3u3nmv" value="7282862830136195762" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="hs" role="2OqNvi">
              <ref role="37wK5l" to="33ny:~Map.put(java.lang.Object,java.lang.Object):java.lang.Object" resolve="put" />
              <node concept="2YIFZM" id="hw" role="37wK5m">
                <ref role="1Pybhc" to="2k9e:~MetaAdapterFactory" resolve="MetaAdapterFactory" />
                <ref role="37wK5l" to="2k9e:~MetaAdapterFactory.getReferenceLink(long,long,long,long,java.lang.String):org.jetbrains.mps.openapi.language.SReferenceLink" resolve="getReferenceLink" />
                <node concept="1adDum" id="hz" role="37wK5m">
                  <property role="1adDun" value="0xc610a4ebc47c4f95L" />
                  <node concept="cd27G" id="hD" role="lGtFl">
                    <node concept="3u3nmq" id="hE" role="cd27D">
                      <property role="3u3nmv" value="7282862830136195762" />
                    </node>
                  </node>
                </node>
                <node concept="1adDum" id="h$" role="37wK5m">
                  <property role="1adDun" value="0xb56811236971769cL" />
                  <node concept="cd27G" id="hF" role="lGtFl">
                    <node concept="3u3nmq" id="hG" role="cd27D">
                      <property role="3u3nmv" value="7282862830136195762" />
                    </node>
                  </node>
                </node>
                <node concept="1adDum" id="h_" role="37wK5m">
                  <property role="1adDun" value="0x6c7943d5b299791dL" />
                  <node concept="cd27G" id="hH" role="lGtFl">
                    <node concept="3u3nmq" id="hI" role="cd27D">
                      <property role="3u3nmv" value="7282862830136195762" />
                    </node>
                  </node>
                </node>
                <node concept="1adDum" id="hA" role="37wK5m">
                  <property role="1adDun" value="0x6c7943d5b29b8063L" />
                  <node concept="cd27G" id="hJ" role="lGtFl">
                    <node concept="3u3nmq" id="hK" role="cd27D">
                      <property role="3u3nmv" value="7282862830136195762" />
                    </node>
                  </node>
                </node>
                <node concept="Xl_RD" id="hB" role="37wK5m">
                  <property role="Xl_RC" value="target" />
                  <node concept="cd27G" id="hL" role="lGtFl">
                    <node concept="3u3nmq" id="hM" role="cd27D">
                      <property role="3u3nmv" value="7282862830136195762" />
                    </node>
                  </node>
                </node>
                <node concept="cd27G" id="hC" role="lGtFl">
                  <node concept="3u3nmq" id="hN" role="cd27D">
                    <property role="3u3nmv" value="7282862830136195762" />
                  </node>
                </node>
              </node>
              <node concept="2ShNRf" id="hx" role="37wK5m">
                <node concept="YeOm9" id="hO" role="2ShVmc">
                  <node concept="1Y3b0j" id="hQ" role="YeSDq">
                    <property role="2bfB8j" value="true" />
                    <ref role="1Y3XeK" to="79pl:~BaseReferenceConstraintsDescriptor" resolve="BaseReferenceConstraintsDescriptor" />
                    <ref role="37wK5l" to="79pl:~BaseReferenceConstraintsDescriptor.&lt;init&gt;(jetbrains.mps.smodel.adapter.ids.SReferenceLinkId,jetbrains.mps.smodel.runtime.ConstraintsDescriptor)" resolve="BaseReferenceConstraintsDescriptor" />
                    <node concept="2YIFZM" id="hS" role="37wK5m">
                      <ref role="1Pybhc" to="e8bb:~MetaIdFactory" resolve="MetaIdFactory" />
                      <ref role="37wK5l" to="e8bb:~MetaIdFactory.refId(long,long,long,long):jetbrains.mps.smodel.adapter.ids.SReferenceLinkId" resolve="refId" />
                      <node concept="1adDum" id="hY" role="37wK5m">
                        <property role="1adDun" value="0xc610a4ebc47c4f95L" />
                        <node concept="cd27G" id="i3" role="lGtFl">
                          <node concept="3u3nmq" id="i4" role="cd27D">
                            <property role="3u3nmv" value="7282862830136195762" />
                          </node>
                        </node>
                      </node>
                      <node concept="1adDum" id="hZ" role="37wK5m">
                        <property role="1adDun" value="0xb56811236971769cL" />
                        <node concept="cd27G" id="i5" role="lGtFl">
                          <node concept="3u3nmq" id="i6" role="cd27D">
                            <property role="3u3nmv" value="7282862830136195762" />
                          </node>
                        </node>
                      </node>
                      <node concept="1adDum" id="i0" role="37wK5m">
                        <property role="1adDun" value="0x6c7943d5b299791dL" />
                        <node concept="cd27G" id="i7" role="lGtFl">
                          <node concept="3u3nmq" id="i8" role="cd27D">
                            <property role="3u3nmv" value="7282862830136195762" />
                          </node>
                        </node>
                      </node>
                      <node concept="1adDum" id="i1" role="37wK5m">
                        <property role="1adDun" value="0x6c7943d5b29b8063L" />
                        <node concept="cd27G" id="i9" role="lGtFl">
                          <node concept="3u3nmq" id="ia" role="cd27D">
                            <property role="3u3nmv" value="7282862830136195762" />
                          </node>
                        </node>
                      </node>
                      <node concept="cd27G" id="i2" role="lGtFl">
                        <node concept="3u3nmq" id="ib" role="cd27D">
                          <property role="3u3nmv" value="7282862830136195762" />
                        </node>
                      </node>
                    </node>
                    <node concept="3Tm1VV" id="hT" role="1B3o_S">
                      <node concept="cd27G" id="ic" role="lGtFl">
                        <node concept="3u3nmq" id="id" role="cd27D">
                          <property role="3u3nmv" value="7282862830136195762" />
                        </node>
                      </node>
                    </node>
                    <node concept="Xjq3P" id="hU" role="37wK5m">
                      <node concept="cd27G" id="ie" role="lGtFl">
                        <node concept="3u3nmq" id="if" role="cd27D">
                          <property role="3u3nmv" value="7282862830136195762" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFb_" id="hV" role="jymVt">
                      <property role="IEkAT" value="false" />
                      <property role="1EzhhJ" value="false" />
                      <property role="TrG5h" value="hasOwnScopeProvider" />
                      <property role="DiZV1" value="false" />
                      <node concept="3Tm1VV" id="ig" role="1B3o_S">
                        <node concept="cd27G" id="il" role="lGtFl">
                          <node concept="3u3nmq" id="im" role="cd27D">
                            <property role="3u3nmv" value="7282862830136195762" />
                          </node>
                        </node>
                      </node>
                      <node concept="10P_77" id="ih" role="3clF45">
                        <node concept="cd27G" id="in" role="lGtFl">
                          <node concept="3u3nmq" id="io" role="cd27D">
                            <property role="3u3nmv" value="7282862830136195762" />
                          </node>
                        </node>
                      </node>
                      <node concept="3clFbS" id="ii" role="3clF47">
                        <node concept="3clFbF" id="ip" role="3cqZAp">
                          <node concept="3clFbT" id="ir" role="3clFbG">
                            <property role="3clFbU" value="true" />
                            <node concept="cd27G" id="it" role="lGtFl">
                              <node concept="3u3nmq" id="iu" role="cd27D">
                                <property role="3u3nmv" value="7282862830136195762" />
                              </node>
                            </node>
                          </node>
                          <node concept="cd27G" id="is" role="lGtFl">
                            <node concept="3u3nmq" id="iv" role="cd27D">
                              <property role="3u3nmv" value="7282862830136195762" />
                            </node>
                          </node>
                        </node>
                        <node concept="cd27G" id="iq" role="lGtFl">
                          <node concept="3u3nmq" id="iw" role="cd27D">
                            <property role="3u3nmv" value="7282862830136195762" />
                          </node>
                        </node>
                      </node>
                      <node concept="2AHcQZ" id="ij" role="2AJF6D">
                        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
                        <node concept="cd27G" id="ix" role="lGtFl">
                          <node concept="3u3nmq" id="iy" role="cd27D">
                            <property role="3u3nmv" value="7282862830136195762" />
                          </node>
                        </node>
                      </node>
                      <node concept="cd27G" id="ik" role="lGtFl">
                        <node concept="3u3nmq" id="iz" role="cd27D">
                          <property role="3u3nmv" value="7282862830136195762" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFb_" id="hW" role="jymVt">
                      <property role="IEkAT" value="false" />
                      <property role="1EzhhJ" value="false" />
                      <property role="TrG5h" value="getScopeProvider" />
                      <property role="DiZV1" value="false" />
                      <node concept="3Tm1VV" id="i$" role="1B3o_S">
                        <node concept="cd27G" id="iE" role="lGtFl">
                          <node concept="3u3nmq" id="iF" role="cd27D">
                            <property role="3u3nmv" value="7282862830136195762" />
                          </node>
                        </node>
                      </node>
                      <node concept="3uibUv" id="i_" role="3clF45">
                        <ref role="3uigEE" to="ze1i:~ReferenceScopeProvider" resolve="ReferenceScopeProvider" />
                        <node concept="cd27G" id="iG" role="lGtFl">
                          <node concept="3u3nmq" id="iH" role="cd27D">
                            <property role="3u3nmv" value="7282862830136195762" />
                          </node>
                        </node>
                      </node>
                      <node concept="2AHcQZ" id="iA" role="2AJF6D">
                        <ref role="2AI5Lk" to="mhfm:~Nullable" resolve="Nullable" />
                        <node concept="cd27G" id="iI" role="lGtFl">
                          <node concept="3u3nmq" id="iJ" role="cd27D">
                            <property role="3u3nmv" value="7282862830136195762" />
                          </node>
                        </node>
                      </node>
                      <node concept="3clFbS" id="iB" role="3clF47">
                        <node concept="3cpWs6" id="iK" role="3cqZAp">
                          <node concept="2ShNRf" id="iM" role="3cqZAk">
                            <node concept="YeOm9" id="iO" role="2ShVmc">
                              <node concept="1Y3b0j" id="iQ" role="YeSDq">
                                <property role="2bfB8j" value="true" />
                                <ref role="1Y3XeK" to="79pl:~BaseScopeProvider" resolve="BaseScopeProvider" />
                                <ref role="37wK5l" to="79pl:~BaseScopeProvider.&lt;init&gt;()" resolve="BaseScopeProvider" />
                                <node concept="3Tm1VV" id="iS" role="1B3o_S">
                                  <node concept="cd27G" id="iW" role="lGtFl">
                                    <node concept="3u3nmq" id="iX" role="cd27D">
                                      <property role="3u3nmv" value="7282862830136195762" />
                                    </node>
                                  </node>
                                </node>
                                <node concept="3clFb_" id="iT" role="jymVt">
                                  <property role="TrG5h" value="getSearchScopeValidatorNode" />
                                  <node concept="3Tm1VV" id="iY" role="1B3o_S">
                                    <node concept="cd27G" id="j3" role="lGtFl">
                                      <node concept="3u3nmq" id="j4" role="cd27D">
                                        <property role="3u3nmv" value="7282862830136195762" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="3clFbS" id="iZ" role="3clF47">
                                    <node concept="3cpWs6" id="j5" role="3cqZAp">
                                      <node concept="1dyn4i" id="j7" role="3cqZAk">
                                        <property role="1zomUR" value="true" />
                                        <property role="1dyqJU" value="breakingNode" />
                                        <node concept="2ShNRf" id="j9" role="1dyrYi">
                                          <node concept="1pGfFk" id="jb" role="2ShVmc">
                                            <ref role="37wK5l" to="w1kc:~SNodePointer.&lt;init&gt;(java.lang.String,java.lang.String)" resolve="SNodePointer" />
                                            <node concept="Xl_RD" id="jd" role="37wK5m">
                                              <property role="Xl_RC" value="r:e4a7a6f9-a1de-4f08-9b98-b5f2b9ecf3a4(no.uio.mLab.decisions.references.laboratoryTest.constraints)" />
                                              <node concept="cd27G" id="jg" role="lGtFl">
                                                <node concept="3u3nmq" id="jh" role="cd27D">
                                                  <property role="3u3nmv" value="7282862830136195762" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="Xl_RD" id="je" role="37wK5m">
                                              <property role="Xl_RC" value="7282862830136195765" />
                                              <node concept="cd27G" id="ji" role="lGtFl">
                                                <node concept="3u3nmq" id="jj" role="cd27D">
                                                  <property role="3u3nmv" value="7282862830136195762" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="cd27G" id="jf" role="lGtFl">
                                              <node concept="3u3nmq" id="jk" role="cd27D">
                                                <property role="3u3nmv" value="7282862830136195762" />
                                              </node>
                                            </node>
                                          </node>
                                          <node concept="cd27G" id="jc" role="lGtFl">
                                            <node concept="3u3nmq" id="jl" role="cd27D">
                                              <property role="3u3nmv" value="7282862830136195762" />
                                            </node>
                                          </node>
                                        </node>
                                        <node concept="cd27G" id="ja" role="lGtFl">
                                          <node concept="3u3nmq" id="jm" role="cd27D">
                                            <property role="3u3nmv" value="7282862830136195762" />
                                          </node>
                                        </node>
                                      </node>
                                      <node concept="cd27G" id="j8" role="lGtFl">
                                        <node concept="3u3nmq" id="jn" role="cd27D">
                                          <property role="3u3nmv" value="7282862830136195762" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="cd27G" id="j6" role="lGtFl">
                                      <node concept="3u3nmq" id="jo" role="cd27D">
                                        <property role="3u3nmv" value="7282862830136195762" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="3uibUv" id="j0" role="3clF45">
                                    <ref role="3uigEE" to="mhbf:~SNodeReference" resolve="SNodeReference" />
                                    <node concept="cd27G" id="jp" role="lGtFl">
                                      <node concept="3u3nmq" id="jq" role="cd27D">
                                        <property role="3u3nmv" value="7282862830136195762" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="2AHcQZ" id="j1" role="2AJF6D">
                                    <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
                                    <node concept="cd27G" id="jr" role="lGtFl">
                                      <node concept="3u3nmq" id="js" role="cd27D">
                                        <property role="3u3nmv" value="7282862830136195762" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="cd27G" id="j2" role="lGtFl">
                                    <node concept="3u3nmq" id="jt" role="cd27D">
                                      <property role="3u3nmv" value="7282862830136195762" />
                                    </node>
                                  </node>
                                </node>
                                <node concept="3clFb_" id="iU" role="jymVt">
                                  <property role="IEkAT" value="false" />
                                  <property role="1EzhhJ" value="false" />
                                  <property role="TrG5h" value="createScope" />
                                  <property role="DiZV1" value="false" />
                                  <node concept="37vLTG" id="ju" role="3clF46">
                                    <property role="TrG5h" value="operationContext" />
                                    <property role="3TUv4t" value="true" />
                                    <node concept="3uibUv" id="j_" role="1tU5fm">
                                      <ref role="3uigEE" to="w1kc:~IOperationContext" resolve="IOperationContext" />
                                      <node concept="cd27G" id="jB" role="lGtFl">
                                        <node concept="3u3nmq" id="jC" role="cd27D">
                                          <property role="3u3nmv" value="7282862830136195762" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="cd27G" id="jA" role="lGtFl">
                                      <node concept="3u3nmq" id="jD" role="cd27D">
                                        <property role="3u3nmv" value="7282862830136195762" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="37vLTG" id="jv" role="3clF46">
                                    <property role="TrG5h" value="_context" />
                                    <property role="3TUv4t" value="true" />
                                    <node concept="3uibUv" id="jE" role="1tU5fm">
                                      <ref role="3uigEE" to="ze1i:~ReferenceConstraintsContext" resolve="ReferenceConstraintsContext" />
                                      <node concept="cd27G" id="jG" role="lGtFl">
                                        <node concept="3u3nmq" id="jH" role="cd27D">
                                          <property role="3u3nmv" value="7282862830136195762" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="cd27G" id="jF" role="lGtFl">
                                      <node concept="3u3nmq" id="jI" role="cd27D">
                                        <property role="3u3nmv" value="7282862830136195762" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="3Tm1VV" id="jw" role="1B3o_S">
                                    <node concept="cd27G" id="jJ" role="lGtFl">
                                      <node concept="3u3nmq" id="jK" role="cd27D">
                                        <property role="3u3nmv" value="7282862830136195762" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="3uibUv" id="jx" role="3clF45">
                                    <ref role="3uigEE" to="35tq:~Scope" resolve="Scope" />
                                    <node concept="cd27G" id="jL" role="lGtFl">
                                      <node concept="3u3nmq" id="jM" role="cd27D">
                                        <property role="3u3nmv" value="7282862830136195762" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="3clFbS" id="jy" role="3clF47">
                                    <node concept="9aQIb" id="jN" role="3cqZAp">
                                      <node concept="3clFbS" id="jP" role="9aQI4">
                                        <node concept="3cpWs8" id="jR" role="3cqZAp">
                                          <node concept="3cpWsn" id="jU" role="3cpWs9">
                                            <property role="TrG5h" value="scope" />
                                            <node concept="3uibUv" id="jW" role="1tU5fm">
                                              <ref role="3uigEE" to="35tq:~Scope" resolve="Scope" />
                                              <node concept="cd27G" id="jZ" role="lGtFl">
                                                <node concept="3u3nmq" id="k0" role="cd27D">
                                                  <property role="3u3nmv" value="7282862830136195762" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="2YIFZM" id="jX" role="33vP2m">
                                              <ref role="1Pybhc" to="35tq:~Scope" resolve="Scope" />
                                              <ref role="37wK5l" to="35tq:~Scope.getScope(org.jetbrains.mps.openapi.model.SNode,org.jetbrains.mps.openapi.language.SContainmentLink,int,org.jetbrains.mps.openapi.language.SAbstractConcept):jetbrains.mps.scope.Scope" resolve="getScope" />
                                              <node concept="2OqwBi" id="k1" role="37wK5m">
                                                <node concept="37vLTw" id="k6" role="2Oq$k0">
                                                  <ref role="3cqZAo" node="jv" resolve="_context" />
                                                  <node concept="cd27G" id="k9" role="lGtFl">
                                                    <node concept="3u3nmq" id="ka" role="cd27D">
                                                      <property role="3u3nmv" value="7282862830136195762" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="liA8E" id="k7" role="2OqNvi">
                                                  <ref role="37wK5l" to="ze1i:~ReferenceConstraintsContext.getContextNode():org.jetbrains.mps.openapi.model.SNode" resolve="getContextNode" />
                                                  <node concept="cd27G" id="kb" role="lGtFl">
                                                    <node concept="3u3nmq" id="kc" role="cd27D">
                                                      <property role="3u3nmv" value="7282862830136195762" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="cd27G" id="k8" role="lGtFl">
                                                  <node concept="3u3nmq" id="kd" role="cd27D">
                                                    <property role="3u3nmv" value="7282862830136195762" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="2OqwBi" id="k2" role="37wK5m">
                                                <node concept="liA8E" id="ke" role="2OqNvi">
                                                  <ref role="37wK5l" to="ze1i:~ReferenceConstraintsContext.getContainmentLink():org.jetbrains.mps.openapi.language.SContainmentLink" resolve="getContainmentLink" />
                                                  <node concept="cd27G" id="kh" role="lGtFl">
                                                    <node concept="3u3nmq" id="ki" role="cd27D">
                                                      <property role="3u3nmv" value="7282862830136195762" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="37vLTw" id="kf" role="2Oq$k0">
                                                  <ref role="3cqZAo" node="jv" resolve="_context" />
                                                  <node concept="cd27G" id="kj" role="lGtFl">
                                                    <node concept="3u3nmq" id="kk" role="cd27D">
                                                      <property role="3u3nmv" value="7282862830136195762" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="cd27G" id="kg" role="lGtFl">
                                                  <node concept="3u3nmq" id="kl" role="cd27D">
                                                    <property role="3u3nmv" value="7282862830136195762" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="2OqwBi" id="k3" role="37wK5m">
                                                <node concept="37vLTw" id="km" role="2Oq$k0">
                                                  <ref role="3cqZAo" node="jv" resolve="_context" />
                                                  <node concept="cd27G" id="kp" role="lGtFl">
                                                    <node concept="3u3nmq" id="kq" role="cd27D">
                                                      <property role="3u3nmv" value="7282862830136195762" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="liA8E" id="kn" role="2OqNvi">
                                                  <ref role="37wK5l" to="ze1i:~ReferenceConstraintsContext.getPosition():int" resolve="getPosition" />
                                                  <node concept="cd27G" id="kr" role="lGtFl">
                                                    <node concept="3u3nmq" id="ks" role="cd27D">
                                                      <property role="3u3nmv" value="7282862830136195762" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="cd27G" id="ko" role="lGtFl">
                                                  <node concept="3u3nmq" id="kt" role="cd27D">
                                                    <property role="3u3nmv" value="7282862830136195762" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="35c_gC" id="k4" role="37wK5m">
                                                <ref role="35c_gD" to="ruww:6khVixyavhY" resolve="SpecimenTypeVariable" />
                                                <node concept="cd27G" id="ku" role="lGtFl">
                                                  <node concept="3u3nmq" id="kv" role="cd27D">
                                                    <property role="3u3nmv" value="7282862830136195762" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="cd27G" id="k5" role="lGtFl">
                                                <node concept="3u3nmq" id="kw" role="cd27D">
                                                  <property role="3u3nmv" value="7282862830136195762" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="cd27G" id="jY" role="lGtFl">
                                              <node concept="3u3nmq" id="kx" role="cd27D">
                                                <property role="3u3nmv" value="7282862830136195762" />
                                              </node>
                                            </node>
                                          </node>
                                          <node concept="cd27G" id="jV" role="lGtFl">
                                            <node concept="3u3nmq" id="ky" role="cd27D">
                                              <property role="3u3nmv" value="7282862830136195762" />
                                            </node>
                                          </node>
                                        </node>
                                        <node concept="3cpWs6" id="jS" role="3cqZAp">
                                          <node concept="3K4zz7" id="kz" role="3cqZAk">
                                            <node concept="2ShNRf" id="k_" role="3K4E3e">
                                              <node concept="1pGfFk" id="kD" role="2ShVmc">
                                                <ref role="37wK5l" to="35tq:~EmptyScope.&lt;init&gt;()" resolve="EmptyScope" />
                                                <node concept="cd27G" id="kF" role="lGtFl">
                                                  <node concept="3u3nmq" id="kG" role="cd27D">
                                                    <property role="3u3nmv" value="7282862830136195762" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="cd27G" id="kE" role="lGtFl">
                                                <node concept="3u3nmq" id="kH" role="cd27D">
                                                  <property role="3u3nmv" value="7282862830136195762" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="37vLTw" id="kA" role="3K4GZi">
                                              <ref role="3cqZAo" node="jU" resolve="scope" />
                                              <node concept="cd27G" id="kI" role="lGtFl">
                                                <node concept="3u3nmq" id="kJ" role="cd27D">
                                                  <property role="3u3nmv" value="7282862830136195762" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="3clFbC" id="kB" role="3K4Cdx">
                                              <node concept="10Nm6u" id="kK" role="3uHU7w">
                                                <node concept="cd27G" id="kN" role="lGtFl">
                                                  <node concept="3u3nmq" id="kO" role="cd27D">
                                                    <property role="3u3nmv" value="7282862830136195762" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="37vLTw" id="kL" role="3uHU7B">
                                                <ref role="3cqZAo" node="jU" resolve="scope" />
                                                <node concept="cd27G" id="kP" role="lGtFl">
                                                  <node concept="3u3nmq" id="kQ" role="cd27D">
                                                    <property role="3u3nmv" value="7282862830136195762" />
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="cd27G" id="kM" role="lGtFl">
                                                <node concept="3u3nmq" id="kR" role="cd27D">
                                                  <property role="3u3nmv" value="7282862830136195762" />
                                                </node>
                                              </node>
                                            </node>
                                            <node concept="cd27G" id="kC" role="lGtFl">
                                              <node concept="3u3nmq" id="kS" role="cd27D">
                                                <property role="3u3nmv" value="7282862830136195762" />
                                              </node>
                                            </node>
                                          </node>
                                          <node concept="cd27G" id="k$" role="lGtFl">
                                            <node concept="3u3nmq" id="kT" role="cd27D">
                                              <property role="3u3nmv" value="7282862830136195762" />
                                            </node>
                                          </node>
                                        </node>
                                        <node concept="cd27G" id="jT" role="lGtFl">
                                          <node concept="3u3nmq" id="kU" role="cd27D">
                                            <property role="3u3nmv" value="7282862830136195762" />
                                          </node>
                                        </node>
                                      </node>
                                      <node concept="cd27G" id="jQ" role="lGtFl">
                                        <node concept="3u3nmq" id="kV" role="cd27D">
                                          <property role="3u3nmv" value="7282862830136195762" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="cd27G" id="jO" role="lGtFl">
                                      <node concept="3u3nmq" id="kW" role="cd27D">
                                        <property role="3u3nmv" value="7282862830136195762" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="2AHcQZ" id="jz" role="2AJF6D">
                                    <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
                                    <node concept="cd27G" id="kX" role="lGtFl">
                                      <node concept="3u3nmq" id="kY" role="cd27D">
                                        <property role="3u3nmv" value="7282862830136195762" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="cd27G" id="j$" role="lGtFl">
                                    <node concept="3u3nmq" id="kZ" role="cd27D">
                                      <property role="3u3nmv" value="7282862830136195762" />
                                    </node>
                                  </node>
                                </node>
                                <node concept="cd27G" id="iV" role="lGtFl">
                                  <node concept="3u3nmq" id="l0" role="cd27D">
                                    <property role="3u3nmv" value="7282862830136195762" />
                                  </node>
                                </node>
                              </node>
                              <node concept="cd27G" id="iR" role="lGtFl">
                                <node concept="3u3nmq" id="l1" role="cd27D">
                                  <property role="3u3nmv" value="7282862830136195762" />
                                </node>
                              </node>
                            </node>
                            <node concept="cd27G" id="iP" role="lGtFl">
                              <node concept="3u3nmq" id="l2" role="cd27D">
                                <property role="3u3nmv" value="7282862830136195762" />
                              </node>
                            </node>
                          </node>
                          <node concept="cd27G" id="iN" role="lGtFl">
                            <node concept="3u3nmq" id="l3" role="cd27D">
                              <property role="3u3nmv" value="7282862830136195762" />
                            </node>
                          </node>
                        </node>
                        <node concept="cd27G" id="iL" role="lGtFl">
                          <node concept="3u3nmq" id="l4" role="cd27D">
                            <property role="3u3nmv" value="7282862830136195762" />
                          </node>
                        </node>
                      </node>
                      <node concept="2AHcQZ" id="iC" role="2AJF6D">
                        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
                        <node concept="cd27G" id="l5" role="lGtFl">
                          <node concept="3u3nmq" id="l6" role="cd27D">
                            <property role="3u3nmv" value="7282862830136195762" />
                          </node>
                        </node>
                      </node>
                      <node concept="cd27G" id="iD" role="lGtFl">
                        <node concept="3u3nmq" id="l7" role="cd27D">
                          <property role="3u3nmv" value="7282862830136195762" />
                        </node>
                      </node>
                    </node>
                    <node concept="cd27G" id="hX" role="lGtFl">
                      <node concept="3u3nmq" id="l8" role="cd27D">
                        <property role="3u3nmv" value="7282862830136195762" />
                      </node>
                    </node>
                  </node>
                  <node concept="cd27G" id="hR" role="lGtFl">
                    <node concept="3u3nmq" id="l9" role="cd27D">
                      <property role="3u3nmv" value="7282862830136195762" />
                    </node>
                  </node>
                </node>
                <node concept="cd27G" id="hP" role="lGtFl">
                  <node concept="3u3nmq" id="la" role="cd27D">
                    <property role="3u3nmv" value="7282862830136195762" />
                  </node>
                </node>
              </node>
              <node concept="cd27G" id="hy" role="lGtFl">
                <node concept="3u3nmq" id="lb" role="cd27D">
                  <property role="3u3nmv" value="7282862830136195762" />
                </node>
              </node>
            </node>
            <node concept="cd27G" id="ht" role="lGtFl">
              <node concept="3u3nmq" id="lc" role="cd27D">
                <property role="3u3nmv" value="7282862830136195762" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="hq" role="lGtFl">
            <node concept="3u3nmq" id="ld" role="cd27D">
              <property role="3u3nmv" value="7282862830136195762" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="gX" role="3cqZAp">
          <node concept="37vLTw" id="le" role="3clFbG">
            <ref role="3cqZAo" node="gZ" resolve="references" />
            <node concept="cd27G" id="lg" role="lGtFl">
              <node concept="3u3nmq" id="lh" role="cd27D">
                <property role="3u3nmv" value="7282862830136195762" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="lf" role="lGtFl">
            <node concept="3u3nmq" id="li" role="cd27D">
              <property role="3u3nmv" value="7282862830136195762" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="gY" role="lGtFl">
          <node concept="3u3nmq" id="lj" role="cd27D">
            <property role="3u3nmv" value="7282862830136195762" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="gJ" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <node concept="cd27G" id="lk" role="lGtFl">
          <node concept="3u3nmq" id="ll" role="cd27D">
            <property role="3u3nmv" value="7282862830136195762" />
          </node>
        </node>
      </node>
      <node concept="cd27G" id="gK" role="lGtFl">
        <node concept="3u3nmq" id="lm" role="cd27D">
          <property role="3u3nmv" value="7282862830136195762" />
        </node>
      </node>
    </node>
    <node concept="cd27G" id="g8" role="lGtFl">
      <node concept="3u3nmq" id="ln" role="cd27D">
        <property role="3u3nmv" value="7282862830136195762" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="lo">
    <property role="TrG5h" value="ConstraintsAspectDescriptor" />
    <property role="3GE5qa" value="Constraints" />
    <node concept="3uibUv" id="lp" role="1zkMxy">
      <ref role="3uigEE" to="ze1i:~BaseConstraintsAspectDescriptor" resolve="BaseConstraintsAspectDescriptor" />
    </node>
    <node concept="3Tm1VV" id="lq" role="1B3o_S" />
    <node concept="3clFbW" id="lr" role="jymVt">
      <node concept="3cqZAl" id="lu" role="3clF45" />
      <node concept="3Tm1VV" id="lv" role="1B3o_S" />
      <node concept="3clFbS" id="lw" role="3clF47" />
    </node>
    <node concept="2tJIrI" id="ls" role="jymVt" />
    <node concept="3clFb_" id="lt" role="jymVt">
      <property role="IEkAT" value="false" />
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="getConstraints" />
      <property role="DiZV1" value="false" />
      <node concept="2AHcQZ" id="lx" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
      <node concept="3Tm1VV" id="ly" role="1B3o_S" />
      <node concept="3uibUv" id="lz" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConstraintsDescriptor" resolve="ConstraintsDescriptor" />
      </node>
      <node concept="37vLTG" id="l$" role="3clF46">
        <property role="TrG5h" value="concept" />
        <node concept="3bZ5Sz" id="lA" role="1tU5fm" />
      </node>
      <node concept="3clFbS" id="l_" role="3clF47">
        <node concept="1_3QMa" id="lB" role="3cqZAp">
          <node concept="37vLTw" id="lD" role="1_3QMn">
            <ref role="3cqZAo" node="l$" resolve="concept" />
          </node>
          <node concept="1pnPoh" id="lE" role="1_3QMm">
            <node concept="3clFbS" id="lL" role="1pnPq1">
              <node concept="3cpWs6" id="lN" role="3cqZAp">
                <node concept="1nCR9W" id="lO" role="3cqZAk">
                  <property role="1nD$Q0" value="no.uio.mLab.decisions.references.laboratoryTest.constraints.AspectLaboratoryTestWithTextResultReference_Constraints" />
                  <node concept="3uibUv" id="lP" role="2lIhxL">
                    <ref role="3uigEE" to="ze1i:~ConstraintsDescriptor" resolve="ConstraintsDescriptor" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3gn64h" id="lM" role="1pnPq6">
              <ref role="3gnhBz" to="ruww:4B5aqq2QgEt" resolve="AspectLaboratoryTestWithTextResultReference" />
            </node>
          </node>
          <node concept="1pnPoh" id="lF" role="1_3QMm">
            <node concept="3clFbS" id="lQ" role="1pnPq1">
              <node concept="3cpWs6" id="lS" role="3cqZAp">
                <node concept="1nCR9W" id="lT" role="3cqZAk">
                  <property role="1nD$Q0" value="no.uio.mLab.decisions.references.laboratoryTest.constraints.AspectLaboratoryTestWithNumberResultReference_Constraints" />
                  <node concept="3uibUv" id="lU" role="2lIhxL">
                    <ref role="3uigEE" to="ze1i:~ConstraintsDescriptor" resolve="ConstraintsDescriptor" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3gn64h" id="lR" role="1pnPq6">
              <ref role="3gnhBz" to="ruww:4B5aqq2QgEr" resolve="AspectLaboratoryTestWithNumberResultReference" />
            </node>
          </node>
          <node concept="1pnPoh" id="lG" role="1_3QMm">
            <node concept="3clFbS" id="lV" role="1pnPq1">
              <node concept="3cpWs6" id="lX" role="3cqZAp">
                <node concept="1nCR9W" id="lY" role="3cqZAk">
                  <property role="1nD$Q0" value="no.uio.mLab.decisions.references.laboratoryTest.constraints.AspectSpecimenTypeReference_Constraints" />
                  <node concept="3uibUv" id="lZ" role="2lIhxL">
                    <ref role="3uigEE" to="ze1i:~ConstraintsDescriptor" resolve="ConstraintsDescriptor" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3gn64h" id="lW" role="1pnPq6">
              <ref role="3gnhBz" to="ruww:6khVixyavhZ" resolve="AspectSpecimenTypeReference" />
            </node>
          </node>
          <node concept="1pnPoh" id="lH" role="1_3QMm">
            <node concept="3clFbS" id="m0" role="1pnPq1">
              <node concept="3cpWs6" id="m2" role="3cqZAp">
                <node concept="1nCR9W" id="m3" role="3cqZAk">
                  <property role="1nD$Q0" value="no.uio.mLab.decisions.references.laboratoryTest.constraints.AspectLaboratoryTestReference_Constraints" />
                  <node concept="3uibUv" id="m4" role="2lIhxL">
                    <ref role="3uigEE" to="ze1i:~ConstraintsDescriptor" resolve="ConstraintsDescriptor" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3gn64h" id="m1" role="1pnPq6">
              <ref role="3gnhBz" to="ruww:4QUW3efwB3a" resolve="AspectLaboratoryTestReference" />
            </node>
          </node>
          <node concept="1pnPoh" id="lI" role="1_3QMm">
            <node concept="3clFbS" id="m5" role="1pnPq1">
              <node concept="3cpWs6" id="m7" role="3cqZAp">
                <node concept="1nCR9W" id="m8" role="3cqZAk">
                  <property role="1nD$Q0" value="no.uio.mLab.decisions.references.laboratoryTest.constraints.LaboratoryTestWithTextResultPattern_Constraints" />
                  <node concept="3uibUv" id="m9" role="2lIhxL">
                    <ref role="3uigEE" to="ze1i:~ConstraintsDescriptor" resolve="ConstraintsDescriptor" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3gn64h" id="m6" role="1pnPq6">
              <ref role="3gnhBz" to="ruww:65epL7MjP7x" resolve="LaboratoryTestWithTextResultPattern" />
            </node>
          </node>
          <node concept="1pnPoh" id="lJ" role="1_3QMm">
            <node concept="3clFbS" id="ma" role="1pnPq1">
              <node concept="3cpWs6" id="mc" role="3cqZAp">
                <node concept="1nCR9W" id="md" role="3cqZAk">
                  <property role="1nD$Q0" value="no.uio.mLab.decisions.references.laboratoryTest.constraints.LaboratoryTestWithNumberResultPattern_Constraints" />
                  <node concept="3uibUv" id="me" role="2lIhxL">
                    <ref role="3uigEE" to="ze1i:~ConstraintsDescriptor" resolve="ConstraintsDescriptor" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3gn64h" id="mb" role="1pnPq6">
              <ref role="3gnhBz" to="ruww:65epL7MkNmK" resolve="LaboratoryTestWithNumberResultPattern" />
            </node>
          </node>
          <node concept="3clFbS" id="lK" role="1prKM_" />
        </node>
        <node concept="3cpWs6" id="lC" role="3cqZAp">
          <node concept="2ShNRf" id="mf" role="3cqZAk">
            <node concept="1pGfFk" id="mg" role="2ShVmc">
              <ref role="37wK5l" to="79pl:~BaseConstraintsDescriptor.&lt;init&gt;(org.jetbrains.mps.openapi.language.SAbstractConcept)" resolve="BaseConstraintsDescriptor" />
              <node concept="37vLTw" id="mh" role="37wK5m">
                <ref role="3cqZAo" node="l$" resolve="concept" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="39dXUE" id="mi" />
  <node concept="312cEu" id="mj">
    <property role="3GE5qa" value="aspect.patterns.laboratoryTest.withNumberResult" />
    <property role="TrG5h" value="LaboratoryTestWithNumberResultPattern_Constraints" />
    <node concept="3Tm1VV" id="mk" role="1B3o_S">
      <node concept="cd27G" id="mr" role="lGtFl">
        <node concept="3u3nmq" id="ms" role="cd27D">
          <property role="3u3nmv" value="7011654996640085515" />
        </node>
      </node>
    </node>
    <node concept="3uibUv" id="ml" role="1zkMxy">
      <ref role="3uigEE" to="79pl:~BaseConstraintsDescriptor" resolve="BaseConstraintsDescriptor" />
      <node concept="cd27G" id="mt" role="lGtFl">
        <node concept="3u3nmq" id="mu" role="cd27D">
          <property role="3u3nmv" value="7011654996640085515" />
        </node>
      </node>
    </node>
    <node concept="3clFbW" id="mm" role="jymVt">
      <node concept="3cqZAl" id="mv" role="3clF45">
        <node concept="cd27G" id="mz" role="lGtFl">
          <node concept="3u3nmq" id="m$" role="cd27D">
            <property role="3u3nmv" value="7011654996640085515" />
          </node>
        </node>
      </node>
      <node concept="3clFbS" id="mw" role="3clF47">
        <node concept="XkiVB" id="m_" role="3cqZAp">
          <ref role="37wK5l" to="79pl:~BaseConstraintsDescriptor.&lt;init&gt;(org.jetbrains.mps.openapi.language.SAbstractConcept)" resolve="BaseConstraintsDescriptor" />
          <node concept="2YIFZM" id="mB" role="37wK5m">
            <ref role="1Pybhc" to="2k9e:~MetaAdapterFactory" resolve="MetaAdapterFactory" />
            <ref role="37wK5l" to="2k9e:~MetaAdapterFactory.getConcept(long,long,long,java.lang.String):org.jetbrains.mps.openapi.language.SConcept" resolve="getConcept" />
            <node concept="1adDum" id="mD" role="37wK5m">
              <property role="1adDun" value="0x4a652d5536844d2dL" />
              <node concept="cd27G" id="mI" role="lGtFl">
                <node concept="3u3nmq" id="mJ" role="cd27D">
                  <property role="3u3nmv" value="7011654996640085515" />
                </node>
              </node>
            </node>
            <node concept="1adDum" id="mE" role="37wK5m">
              <property role="1adDun" value="0x98c92ef46f124c44L" />
              <node concept="cd27G" id="mK" role="lGtFl">
                <node concept="3u3nmq" id="mL" role="cd27D">
                  <property role="3u3nmv" value="7011654996640085515" />
                </node>
              </node>
            </node>
            <node concept="1adDum" id="mF" role="37wK5m">
              <property role="1adDun" value="0x614e6711f25335b0L" />
              <node concept="cd27G" id="mM" role="lGtFl">
                <node concept="3u3nmq" id="mN" role="cd27D">
                  <property role="3u3nmv" value="7011654996640085515" />
                </node>
              </node>
            </node>
            <node concept="Xl_RD" id="mG" role="37wK5m">
              <property role="Xl_RC" value="no.uio.mLab.decisions.references.laboratoryTest.structure.LaboratoryTestWithNumberResultPattern" />
              <node concept="cd27G" id="mO" role="lGtFl">
                <node concept="3u3nmq" id="mP" role="cd27D">
                  <property role="3u3nmv" value="7011654996640085515" />
                </node>
              </node>
            </node>
            <node concept="cd27G" id="mH" role="lGtFl">
              <node concept="3u3nmq" id="mQ" role="cd27D">
                <property role="3u3nmv" value="7011654996640085515" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="mC" role="lGtFl">
            <node concept="3u3nmq" id="mR" role="cd27D">
              <property role="3u3nmv" value="7011654996640085515" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="mA" role="lGtFl">
          <node concept="3u3nmq" id="mS" role="cd27D">
            <property role="3u3nmv" value="7011654996640085515" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="mx" role="1B3o_S">
        <node concept="cd27G" id="mT" role="lGtFl">
          <node concept="3u3nmq" id="mU" role="cd27D">
            <property role="3u3nmv" value="7011654996640085515" />
          </node>
        </node>
      </node>
      <node concept="cd27G" id="my" role="lGtFl">
        <node concept="3u3nmq" id="mV" role="cd27D">
          <property role="3u3nmv" value="7011654996640085515" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="mn" role="jymVt">
      <node concept="cd27G" id="mW" role="lGtFl">
        <node concept="3u3nmq" id="mX" role="cd27D">
          <property role="3u3nmv" value="7011654996640085515" />
        </node>
      </node>
    </node>
    <node concept="3clFb_" id="mo" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="calculateCanBeChildConstraint" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3Tmbuc" id="mY" role="1B3o_S">
        <node concept="cd27G" id="n3" role="lGtFl">
          <node concept="3u3nmq" id="n4" role="cd27D">
            <property role="3u3nmv" value="7011654996640085515" />
          </node>
        </node>
      </node>
      <node concept="3uibUv" id="mZ" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConstraintFunction" resolve="ConstraintFunction" />
        <node concept="3uibUv" id="n5" role="11_B2D">
          <ref role="3uigEE" to="ze1i:~ConstraintContext_CanBeChild" resolve="ConstraintContext_CanBeChild" />
          <node concept="cd27G" id="n8" role="lGtFl">
            <node concept="3u3nmq" id="n9" role="cd27D">
              <property role="3u3nmv" value="7011654996640085515" />
            </node>
          </node>
        </node>
        <node concept="3uibUv" id="n6" role="11_B2D">
          <ref role="3uigEE" to="wyt6:~Boolean" resolve="Boolean" />
          <node concept="cd27G" id="na" role="lGtFl">
            <node concept="3u3nmq" id="nb" role="cd27D">
              <property role="3u3nmv" value="7011654996640085515" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="n7" role="lGtFl">
          <node concept="3u3nmq" id="nc" role="cd27D">
            <property role="3u3nmv" value="7011654996640085515" />
          </node>
        </node>
      </node>
      <node concept="3clFbS" id="n0" role="3clF47">
        <node concept="3clFbF" id="nd" role="3cqZAp">
          <node concept="2ShNRf" id="nf" role="3clFbG">
            <node concept="YeOm9" id="nh" role="2ShVmc">
              <node concept="1Y3b0j" id="nj" role="YeSDq">
                <property role="2bfB8j" value="true" />
                <ref role="1Y3XeK" to="ze1i:~ConstraintFunction" resolve="ConstraintFunction" />
                <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
                <node concept="3Tm1VV" id="nl" role="1B3o_S">
                  <node concept="cd27G" id="nq" role="lGtFl">
                    <node concept="3u3nmq" id="nr" role="cd27D">
                      <property role="3u3nmv" value="7011654996640085515" />
                    </node>
                  </node>
                </node>
                <node concept="3clFb_" id="nm" role="jymVt">
                  <property role="1EzhhJ" value="false" />
                  <property role="TrG5h" value="invoke" />
                  <property role="DiZV1" value="false" />
                  <property role="od$2w" value="false" />
                  <node concept="3Tm1VV" id="ns" role="1B3o_S">
                    <node concept="cd27G" id="nz" role="lGtFl">
                      <node concept="3u3nmq" id="n$" role="cd27D">
                        <property role="3u3nmv" value="7011654996640085515" />
                      </node>
                    </node>
                  </node>
                  <node concept="2AHcQZ" id="nt" role="2AJF6D">
                    <ref role="2AI5Lk" to="mhfm:~NotNull" resolve="NotNull" />
                    <node concept="cd27G" id="n_" role="lGtFl">
                      <node concept="3u3nmq" id="nA" role="cd27D">
                        <property role="3u3nmv" value="7011654996640085515" />
                      </node>
                    </node>
                  </node>
                  <node concept="3uibUv" id="nu" role="3clF45">
                    <ref role="3uigEE" to="wyt6:~Boolean" resolve="Boolean" />
                    <node concept="cd27G" id="nB" role="lGtFl">
                      <node concept="3u3nmq" id="nC" role="cd27D">
                        <property role="3u3nmv" value="7011654996640085515" />
                      </node>
                    </node>
                  </node>
                  <node concept="37vLTG" id="nv" role="3clF46">
                    <property role="TrG5h" value="context" />
                    <node concept="3uibUv" id="nD" role="1tU5fm">
                      <ref role="3uigEE" to="ze1i:~ConstraintContext_CanBeChild" resolve="ConstraintContext_CanBeChild" />
                      <node concept="cd27G" id="nG" role="lGtFl">
                        <node concept="3u3nmq" id="nH" role="cd27D">
                          <property role="3u3nmv" value="7011654996640085515" />
                        </node>
                      </node>
                    </node>
                    <node concept="2AHcQZ" id="nE" role="2AJF6D">
                      <ref role="2AI5Lk" to="mhfm:~NotNull" resolve="NotNull" />
                      <node concept="cd27G" id="nI" role="lGtFl">
                        <node concept="3u3nmq" id="nJ" role="cd27D">
                          <property role="3u3nmv" value="7011654996640085515" />
                        </node>
                      </node>
                    </node>
                    <node concept="cd27G" id="nF" role="lGtFl">
                      <node concept="3u3nmq" id="nK" role="cd27D">
                        <property role="3u3nmv" value="7011654996640085515" />
                      </node>
                    </node>
                  </node>
                  <node concept="37vLTG" id="nw" role="3clF46">
                    <property role="TrG5h" value="checkingNodeContext" />
                    <node concept="3uibUv" id="nL" role="1tU5fm">
                      <ref role="3uigEE" to="ze1i:~CheckingNodeContext" resolve="CheckingNodeContext" />
                      <node concept="cd27G" id="nO" role="lGtFl">
                        <node concept="3u3nmq" id="nP" role="cd27D">
                          <property role="3u3nmv" value="7011654996640085515" />
                        </node>
                      </node>
                    </node>
                    <node concept="2AHcQZ" id="nM" role="2AJF6D">
                      <ref role="2AI5Lk" to="mhfm:~Nullable" resolve="Nullable" />
                      <node concept="cd27G" id="nQ" role="lGtFl">
                        <node concept="3u3nmq" id="nR" role="cd27D">
                          <property role="3u3nmv" value="7011654996640085515" />
                        </node>
                      </node>
                    </node>
                    <node concept="cd27G" id="nN" role="lGtFl">
                      <node concept="3u3nmq" id="nS" role="cd27D">
                        <property role="3u3nmv" value="7011654996640085515" />
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbS" id="nx" role="3clF47">
                    <node concept="3cpWs8" id="nT" role="3cqZAp">
                      <node concept="3cpWsn" id="nZ" role="3cpWs9">
                        <property role="TrG5h" value="result" />
                        <node concept="10P_77" id="o1" role="1tU5fm">
                          <node concept="cd27G" id="o4" role="lGtFl">
                            <node concept="3u3nmq" id="o5" role="cd27D">
                              <property role="3u3nmv" value="7011654996640085515" />
                            </node>
                          </node>
                        </node>
                        <node concept="1rXfSq" id="o2" role="33vP2m">
                          <ref role="37wK5l" node="mp" resolve="staticCanBeAChild" />
                          <node concept="2OqwBi" id="o6" role="37wK5m">
                            <node concept="37vLTw" id="ob" role="2Oq$k0">
                              <ref role="3cqZAo" node="nv" resolve="context" />
                              <node concept="cd27G" id="oe" role="lGtFl">
                                <node concept="3u3nmq" id="of" role="cd27D">
                                  <property role="3u3nmv" value="7011654996640085515" />
                                </node>
                              </node>
                            </node>
                            <node concept="liA8E" id="oc" role="2OqNvi">
                              <ref role="37wK5l" to="ze1i:~ConstraintContext_CanBeChild.getNode():org.jetbrains.mps.openapi.model.SNode" resolve="getNode" />
                              <node concept="cd27G" id="og" role="lGtFl">
                                <node concept="3u3nmq" id="oh" role="cd27D">
                                  <property role="3u3nmv" value="7011654996640085515" />
                                </node>
                              </node>
                            </node>
                            <node concept="cd27G" id="od" role="lGtFl">
                              <node concept="3u3nmq" id="oi" role="cd27D">
                                <property role="3u3nmv" value="7011654996640085515" />
                              </node>
                            </node>
                          </node>
                          <node concept="2OqwBi" id="o7" role="37wK5m">
                            <node concept="37vLTw" id="oj" role="2Oq$k0">
                              <ref role="3cqZAo" node="nv" resolve="context" />
                              <node concept="cd27G" id="om" role="lGtFl">
                                <node concept="3u3nmq" id="on" role="cd27D">
                                  <property role="3u3nmv" value="7011654996640085515" />
                                </node>
                              </node>
                            </node>
                            <node concept="liA8E" id="ok" role="2OqNvi">
                              <ref role="37wK5l" to="ze1i:~ConstraintContext_CanBeChild.getParentNode():org.jetbrains.mps.openapi.model.SNode" resolve="getParentNode" />
                              <node concept="cd27G" id="oo" role="lGtFl">
                                <node concept="3u3nmq" id="op" role="cd27D">
                                  <property role="3u3nmv" value="7011654996640085515" />
                                </node>
                              </node>
                            </node>
                            <node concept="cd27G" id="ol" role="lGtFl">
                              <node concept="3u3nmq" id="oq" role="cd27D">
                                <property role="3u3nmv" value="7011654996640085515" />
                              </node>
                            </node>
                          </node>
                          <node concept="2OqwBi" id="o8" role="37wK5m">
                            <node concept="37vLTw" id="or" role="2Oq$k0">
                              <ref role="3cqZAo" node="nv" resolve="context" />
                              <node concept="cd27G" id="ou" role="lGtFl">
                                <node concept="3u3nmq" id="ov" role="cd27D">
                                  <property role="3u3nmv" value="7011654996640085515" />
                                </node>
                              </node>
                            </node>
                            <node concept="liA8E" id="os" role="2OqNvi">
                              <ref role="37wK5l" to="ze1i:~ConstraintContext_CanBeChild.getConcept():org.jetbrains.mps.openapi.language.SAbstractConcept" resolve="getConcept" />
                              <node concept="cd27G" id="ow" role="lGtFl">
                                <node concept="3u3nmq" id="ox" role="cd27D">
                                  <property role="3u3nmv" value="7011654996640085515" />
                                </node>
                              </node>
                            </node>
                            <node concept="cd27G" id="ot" role="lGtFl">
                              <node concept="3u3nmq" id="oy" role="cd27D">
                                <property role="3u3nmv" value="7011654996640085515" />
                              </node>
                            </node>
                          </node>
                          <node concept="2OqwBi" id="o9" role="37wK5m">
                            <node concept="37vLTw" id="oz" role="2Oq$k0">
                              <ref role="3cqZAo" node="nv" resolve="context" />
                              <node concept="cd27G" id="oA" role="lGtFl">
                                <node concept="3u3nmq" id="oB" role="cd27D">
                                  <property role="3u3nmv" value="7011654996640085515" />
                                </node>
                              </node>
                            </node>
                            <node concept="liA8E" id="o$" role="2OqNvi">
                              <ref role="37wK5l" to="ze1i:~ConstraintContext_CanBeChild.getLink():org.jetbrains.mps.openapi.language.SContainmentLink" resolve="getLink" />
                              <node concept="cd27G" id="oC" role="lGtFl">
                                <node concept="3u3nmq" id="oD" role="cd27D">
                                  <property role="3u3nmv" value="7011654996640085515" />
                                </node>
                              </node>
                            </node>
                            <node concept="cd27G" id="o_" role="lGtFl">
                              <node concept="3u3nmq" id="oE" role="cd27D">
                                <property role="3u3nmv" value="7011654996640085515" />
                              </node>
                            </node>
                          </node>
                          <node concept="cd27G" id="oa" role="lGtFl">
                            <node concept="3u3nmq" id="oF" role="cd27D">
                              <property role="3u3nmv" value="7011654996640085515" />
                            </node>
                          </node>
                        </node>
                        <node concept="cd27G" id="o3" role="lGtFl">
                          <node concept="3u3nmq" id="oG" role="cd27D">
                            <property role="3u3nmv" value="7011654996640085515" />
                          </node>
                        </node>
                      </node>
                      <node concept="cd27G" id="o0" role="lGtFl">
                        <node concept="3u3nmq" id="oH" role="cd27D">
                          <property role="3u3nmv" value="7011654996640085515" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbH" id="nU" role="3cqZAp">
                      <node concept="cd27G" id="oI" role="lGtFl">
                        <node concept="3u3nmq" id="oJ" role="cd27D">
                          <property role="3u3nmv" value="7011654996640085515" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbJ" id="nV" role="3cqZAp">
                      <node concept="3clFbS" id="oK" role="3clFbx">
                        <node concept="3clFbF" id="oN" role="3cqZAp">
                          <node concept="2OqwBi" id="oP" role="3clFbG">
                            <node concept="37vLTw" id="oR" role="2Oq$k0">
                              <ref role="3cqZAo" node="nw" resolve="checkingNodeContext" />
                              <node concept="cd27G" id="oU" role="lGtFl">
                                <node concept="3u3nmq" id="oV" role="cd27D">
                                  <property role="3u3nmv" value="7011654996640085515" />
                                </node>
                              </node>
                            </node>
                            <node concept="liA8E" id="oS" role="2OqNvi">
                              <ref role="37wK5l" to="ze1i:~CheckingNodeContext.setBreakingNode(org.jetbrains.mps.openapi.model.SNodeReference):void" resolve="setBreakingNode" />
                              <node concept="1dyn4i" id="oW" role="37wK5m">
                                <property role="1dyqJU" value="canBeChildBreakingPoint" />
                                <node concept="2ShNRf" id="oY" role="1dyrYi">
                                  <node concept="1pGfFk" id="p0" role="2ShVmc">
                                    <ref role="37wK5l" to="w1kc:~SNodePointer.&lt;init&gt;(java.lang.String,java.lang.String)" resolve="SNodePointer" />
                                    <node concept="Xl_RD" id="p2" role="37wK5m">
                                      <property role="Xl_RC" value="r:e4a7a6f9-a1de-4f08-9b98-b5f2b9ecf3a4(no.uio.mLab.decisions.references.laboratoryTest.constraints)" />
                                      <node concept="cd27G" id="p5" role="lGtFl">
                                        <node concept="3u3nmq" id="p6" role="cd27D">
                                          <property role="3u3nmv" value="7011654996640085515" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="Xl_RD" id="p3" role="37wK5m">
                                      <property role="Xl_RC" value="7011654996640085516" />
                                      <node concept="cd27G" id="p7" role="lGtFl">
                                        <node concept="3u3nmq" id="p8" role="cd27D">
                                          <property role="3u3nmv" value="7011654996640085515" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="cd27G" id="p4" role="lGtFl">
                                      <node concept="3u3nmq" id="p9" role="cd27D">
                                        <property role="3u3nmv" value="7011654996640085515" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="cd27G" id="p1" role="lGtFl">
                                    <node concept="3u3nmq" id="pa" role="cd27D">
                                      <property role="3u3nmv" value="7011654996640085515" />
                                    </node>
                                  </node>
                                </node>
                                <node concept="cd27G" id="oZ" role="lGtFl">
                                  <node concept="3u3nmq" id="pb" role="cd27D">
                                    <property role="3u3nmv" value="7011654996640085515" />
                                  </node>
                                </node>
                              </node>
                              <node concept="cd27G" id="oX" role="lGtFl">
                                <node concept="3u3nmq" id="pc" role="cd27D">
                                  <property role="3u3nmv" value="7011654996640085515" />
                                </node>
                              </node>
                            </node>
                            <node concept="cd27G" id="oT" role="lGtFl">
                              <node concept="3u3nmq" id="pd" role="cd27D">
                                <property role="3u3nmv" value="7011654996640085515" />
                              </node>
                            </node>
                          </node>
                          <node concept="cd27G" id="oQ" role="lGtFl">
                            <node concept="3u3nmq" id="pe" role="cd27D">
                              <property role="3u3nmv" value="7011654996640085515" />
                            </node>
                          </node>
                        </node>
                        <node concept="cd27G" id="oO" role="lGtFl">
                          <node concept="3u3nmq" id="pf" role="cd27D">
                            <property role="3u3nmv" value="7011654996640085515" />
                          </node>
                        </node>
                      </node>
                      <node concept="1Wc70l" id="oL" role="3clFbw">
                        <node concept="3y3z36" id="pg" role="3uHU7w">
                          <node concept="10Nm6u" id="pj" role="3uHU7w">
                            <node concept="cd27G" id="pm" role="lGtFl">
                              <node concept="3u3nmq" id="pn" role="cd27D">
                                <property role="3u3nmv" value="7011654996640085515" />
                              </node>
                            </node>
                          </node>
                          <node concept="37vLTw" id="pk" role="3uHU7B">
                            <ref role="3cqZAo" node="nw" resolve="checkingNodeContext" />
                            <node concept="cd27G" id="po" role="lGtFl">
                              <node concept="3u3nmq" id="pp" role="cd27D">
                                <property role="3u3nmv" value="7011654996640085515" />
                              </node>
                            </node>
                          </node>
                          <node concept="cd27G" id="pl" role="lGtFl">
                            <node concept="3u3nmq" id="pq" role="cd27D">
                              <property role="3u3nmv" value="7011654996640085515" />
                            </node>
                          </node>
                        </node>
                        <node concept="3fqX7Q" id="ph" role="3uHU7B">
                          <node concept="37vLTw" id="pr" role="3fr31v">
                            <ref role="3cqZAo" node="nZ" resolve="result" />
                            <node concept="cd27G" id="pt" role="lGtFl">
                              <node concept="3u3nmq" id="pu" role="cd27D">
                                <property role="3u3nmv" value="7011654996640085515" />
                              </node>
                            </node>
                          </node>
                          <node concept="cd27G" id="ps" role="lGtFl">
                            <node concept="3u3nmq" id="pv" role="cd27D">
                              <property role="3u3nmv" value="7011654996640085515" />
                            </node>
                          </node>
                        </node>
                        <node concept="cd27G" id="pi" role="lGtFl">
                          <node concept="3u3nmq" id="pw" role="cd27D">
                            <property role="3u3nmv" value="7011654996640085515" />
                          </node>
                        </node>
                      </node>
                      <node concept="cd27G" id="oM" role="lGtFl">
                        <node concept="3u3nmq" id="px" role="cd27D">
                          <property role="3u3nmv" value="7011654996640085515" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbH" id="nW" role="3cqZAp">
                      <node concept="cd27G" id="py" role="lGtFl">
                        <node concept="3u3nmq" id="pz" role="cd27D">
                          <property role="3u3nmv" value="7011654996640085515" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbF" id="nX" role="3cqZAp">
                      <node concept="37vLTw" id="p$" role="3clFbG">
                        <ref role="3cqZAo" node="nZ" resolve="result" />
                        <node concept="cd27G" id="pA" role="lGtFl">
                          <node concept="3u3nmq" id="pB" role="cd27D">
                            <property role="3u3nmv" value="7011654996640085515" />
                          </node>
                        </node>
                      </node>
                      <node concept="cd27G" id="p_" role="lGtFl">
                        <node concept="3u3nmq" id="pC" role="cd27D">
                          <property role="3u3nmv" value="7011654996640085515" />
                        </node>
                      </node>
                    </node>
                    <node concept="cd27G" id="nY" role="lGtFl">
                      <node concept="3u3nmq" id="pD" role="cd27D">
                        <property role="3u3nmv" value="7011654996640085515" />
                      </node>
                    </node>
                  </node>
                  <node concept="cd27G" id="ny" role="lGtFl">
                    <node concept="3u3nmq" id="pE" role="cd27D">
                      <property role="3u3nmv" value="7011654996640085515" />
                    </node>
                  </node>
                </node>
                <node concept="3uibUv" id="nn" role="2Ghqu4">
                  <ref role="3uigEE" to="ze1i:~ConstraintContext_CanBeChild" resolve="ConstraintContext_CanBeChild" />
                  <node concept="cd27G" id="pF" role="lGtFl">
                    <node concept="3u3nmq" id="pG" role="cd27D">
                      <property role="3u3nmv" value="7011654996640085515" />
                    </node>
                  </node>
                </node>
                <node concept="3uibUv" id="no" role="2Ghqu4">
                  <ref role="3uigEE" to="wyt6:~Boolean" resolve="Boolean" />
                  <node concept="cd27G" id="pH" role="lGtFl">
                    <node concept="3u3nmq" id="pI" role="cd27D">
                      <property role="3u3nmv" value="7011654996640085515" />
                    </node>
                  </node>
                </node>
                <node concept="cd27G" id="np" role="lGtFl">
                  <node concept="3u3nmq" id="pJ" role="cd27D">
                    <property role="3u3nmv" value="7011654996640085515" />
                  </node>
                </node>
              </node>
              <node concept="cd27G" id="nk" role="lGtFl">
                <node concept="3u3nmq" id="pK" role="cd27D">
                  <property role="3u3nmv" value="7011654996640085515" />
                </node>
              </node>
            </node>
            <node concept="cd27G" id="ni" role="lGtFl">
              <node concept="3u3nmq" id="pL" role="cd27D">
                <property role="3u3nmv" value="7011654996640085515" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="ng" role="lGtFl">
            <node concept="3u3nmq" id="pM" role="cd27D">
              <property role="3u3nmv" value="7011654996640085515" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="ne" role="lGtFl">
          <node concept="3u3nmq" id="pN" role="cd27D">
            <property role="3u3nmv" value="7011654996640085515" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="n1" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <node concept="cd27G" id="pO" role="lGtFl">
          <node concept="3u3nmq" id="pP" role="cd27D">
            <property role="3u3nmv" value="7011654996640085515" />
          </node>
        </node>
      </node>
      <node concept="cd27G" id="n2" role="lGtFl">
        <node concept="3u3nmq" id="pQ" role="cd27D">
          <property role="3u3nmv" value="7011654996640085515" />
        </node>
      </node>
    </node>
    <node concept="2YIFZL" id="mp" role="jymVt">
      <property role="TrG5h" value="staticCanBeAChild" />
      <node concept="10P_77" id="pR" role="3clF45">
        <node concept="cd27G" id="pZ" role="lGtFl">
          <node concept="3u3nmq" id="q0" role="cd27D">
            <property role="3u3nmv" value="7011654996640085515" />
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="pS" role="1B3o_S">
        <node concept="cd27G" id="q1" role="lGtFl">
          <node concept="3u3nmq" id="q2" role="cd27D">
            <property role="3u3nmv" value="7011654996640085515" />
          </node>
        </node>
      </node>
      <node concept="3clFbS" id="pT" role="3clF47">
        <node concept="3clFbF" id="q3" role="3cqZAp">
          <node concept="2OqwBi" id="q5" role="3clFbG">
            <node concept="37vLTw" id="q7" role="2Oq$k0">
              <ref role="3cqZAo" node="pV" resolve="parentNode" />
              <node concept="cd27G" id="qa" role="lGtFl">
                <node concept="3u3nmq" id="qb" role="cd27D">
                  <property role="3u3nmv" value="7011654996640085983" />
                </node>
              </node>
            </node>
            <node concept="1mIQ4w" id="q8" role="2OqNvi">
              <node concept="chp4Y" id="qc" role="cj9EA">
                <ref role="cht4Q" to="ruww:4B5aqq2QmL5" resolve="LaboratoryTestWithNumberResultVariable" />
                <node concept="cd27G" id="qe" role="lGtFl">
                  <node concept="3u3nmq" id="qf" role="cd27D">
                    <property role="3u3nmv" value="7011654996640088355" />
                  </node>
                </node>
              </node>
              <node concept="cd27G" id="qd" role="lGtFl">
                <node concept="3u3nmq" id="qg" role="cd27D">
                  <property role="3u3nmv" value="7011654996640087542" />
                </node>
              </node>
            </node>
            <node concept="cd27G" id="q9" role="lGtFl">
              <node concept="3u3nmq" id="qh" role="cd27D">
                <property role="3u3nmv" value="7011654996640086732" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="q6" role="lGtFl">
            <node concept="3u3nmq" id="qi" role="cd27D">
              <property role="3u3nmv" value="7011654996640085984" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="q4" role="lGtFl">
          <node concept="3u3nmq" id="qj" role="cd27D">
            <property role="3u3nmv" value="7011654996640085517" />
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="pU" role="3clF46">
        <property role="TrG5h" value="node" />
        <node concept="3uibUv" id="qk" role="1tU5fm">
          <ref role="3uigEE" to="mhbf:~SNode" resolve="SNode" />
          <node concept="cd27G" id="qm" role="lGtFl">
            <node concept="3u3nmq" id="qn" role="cd27D">
              <property role="3u3nmv" value="7011654996640085515" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="ql" role="lGtFl">
          <node concept="3u3nmq" id="qo" role="cd27D">
            <property role="3u3nmv" value="7011654996640085515" />
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="pV" role="3clF46">
        <property role="TrG5h" value="parentNode" />
        <node concept="3uibUv" id="qp" role="1tU5fm">
          <ref role="3uigEE" to="mhbf:~SNode" resolve="SNode" />
          <node concept="cd27G" id="qr" role="lGtFl">
            <node concept="3u3nmq" id="qs" role="cd27D">
              <property role="3u3nmv" value="7011654996640085515" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="qq" role="lGtFl">
          <node concept="3u3nmq" id="qt" role="cd27D">
            <property role="3u3nmv" value="7011654996640085515" />
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="pW" role="3clF46">
        <property role="TrG5h" value="childConcept" />
        <node concept="3uibUv" id="qu" role="1tU5fm">
          <ref role="3uigEE" to="c17a:~SAbstractConcept" resolve="SAbstractConcept" />
          <node concept="cd27G" id="qw" role="lGtFl">
            <node concept="3u3nmq" id="qx" role="cd27D">
              <property role="3u3nmv" value="7011654996640085515" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="qv" role="lGtFl">
          <node concept="3u3nmq" id="qy" role="cd27D">
            <property role="3u3nmv" value="7011654996640085515" />
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="pX" role="3clF46">
        <property role="TrG5h" value="link" />
        <node concept="3uibUv" id="qz" role="1tU5fm">
          <ref role="3uigEE" to="c17a:~SContainmentLink" resolve="SContainmentLink" />
          <node concept="cd27G" id="q_" role="lGtFl">
            <node concept="3u3nmq" id="qA" role="cd27D">
              <property role="3u3nmv" value="7011654996640085515" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="q$" role="lGtFl">
          <node concept="3u3nmq" id="qB" role="cd27D">
            <property role="3u3nmv" value="7011654996640085515" />
          </node>
        </node>
      </node>
      <node concept="cd27G" id="pY" role="lGtFl">
        <node concept="3u3nmq" id="qC" role="cd27D">
          <property role="3u3nmv" value="7011654996640085515" />
        </node>
      </node>
    </node>
    <node concept="cd27G" id="mq" role="lGtFl">
      <node concept="3u3nmq" id="qD" role="cd27D">
        <property role="3u3nmv" value="7011654996640085515" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="qE">
    <property role="3GE5qa" value="aspect.patterns.laboratoryTest.withTextResult" />
    <property role="TrG5h" value="LaboratoryTestWithTextResultPattern_Constraints" />
    <node concept="3Tm1VV" id="qF" role="1B3o_S">
      <node concept="cd27G" id="qM" role="lGtFl">
        <node concept="3u3nmq" id="qN" role="cd27D">
          <property role="3u3nmv" value="7011654996639902320" />
        </node>
      </node>
    </node>
    <node concept="3uibUv" id="qG" role="1zkMxy">
      <ref role="3uigEE" to="79pl:~BaseConstraintsDescriptor" resolve="BaseConstraintsDescriptor" />
      <node concept="cd27G" id="qO" role="lGtFl">
        <node concept="3u3nmq" id="qP" role="cd27D">
          <property role="3u3nmv" value="7011654996639902320" />
        </node>
      </node>
    </node>
    <node concept="3clFbW" id="qH" role="jymVt">
      <node concept="3cqZAl" id="qQ" role="3clF45">
        <node concept="cd27G" id="qU" role="lGtFl">
          <node concept="3u3nmq" id="qV" role="cd27D">
            <property role="3u3nmv" value="7011654996639902320" />
          </node>
        </node>
      </node>
      <node concept="3clFbS" id="qR" role="3clF47">
        <node concept="XkiVB" id="qW" role="3cqZAp">
          <ref role="37wK5l" to="79pl:~BaseConstraintsDescriptor.&lt;init&gt;(org.jetbrains.mps.openapi.language.SAbstractConcept)" resolve="BaseConstraintsDescriptor" />
          <node concept="2YIFZM" id="qY" role="37wK5m">
            <ref role="1Pybhc" to="2k9e:~MetaAdapterFactory" resolve="MetaAdapterFactory" />
            <ref role="37wK5l" to="2k9e:~MetaAdapterFactory.getConcept(long,long,long,java.lang.String):org.jetbrains.mps.openapi.language.SConcept" resolve="getConcept" />
            <node concept="1adDum" id="r0" role="37wK5m">
              <property role="1adDun" value="0x4a652d5536844d2dL" />
              <node concept="cd27G" id="r5" role="lGtFl">
                <node concept="3u3nmq" id="r6" role="cd27D">
                  <property role="3u3nmv" value="7011654996639902320" />
                </node>
              </node>
            </node>
            <node concept="1adDum" id="r1" role="37wK5m">
              <property role="1adDun" value="0x98c92ef46f124c44L" />
              <node concept="cd27G" id="r7" role="lGtFl">
                <node concept="3u3nmq" id="r8" role="cd27D">
                  <property role="3u3nmv" value="7011654996639902320" />
                </node>
              </node>
            </node>
            <node concept="1adDum" id="r2" role="37wK5m">
              <property role="1adDun" value="0x614e6711f24f51e1L" />
              <node concept="cd27G" id="r9" role="lGtFl">
                <node concept="3u3nmq" id="ra" role="cd27D">
                  <property role="3u3nmv" value="7011654996639902320" />
                </node>
              </node>
            </node>
            <node concept="Xl_RD" id="r3" role="37wK5m">
              <property role="Xl_RC" value="no.uio.mLab.decisions.references.laboratoryTest.structure.LaboratoryTestWithTextResultPattern" />
              <node concept="cd27G" id="rb" role="lGtFl">
                <node concept="3u3nmq" id="rc" role="cd27D">
                  <property role="3u3nmv" value="7011654996639902320" />
                </node>
              </node>
            </node>
            <node concept="cd27G" id="r4" role="lGtFl">
              <node concept="3u3nmq" id="rd" role="cd27D">
                <property role="3u3nmv" value="7011654996639902320" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="qZ" role="lGtFl">
            <node concept="3u3nmq" id="re" role="cd27D">
              <property role="3u3nmv" value="7011654996639902320" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="qX" role="lGtFl">
          <node concept="3u3nmq" id="rf" role="cd27D">
            <property role="3u3nmv" value="7011654996639902320" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="qS" role="1B3o_S">
        <node concept="cd27G" id="rg" role="lGtFl">
          <node concept="3u3nmq" id="rh" role="cd27D">
            <property role="3u3nmv" value="7011654996639902320" />
          </node>
        </node>
      </node>
      <node concept="cd27G" id="qT" role="lGtFl">
        <node concept="3u3nmq" id="ri" role="cd27D">
          <property role="3u3nmv" value="7011654996639902320" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="qI" role="jymVt">
      <node concept="cd27G" id="rj" role="lGtFl">
        <node concept="3u3nmq" id="rk" role="cd27D">
          <property role="3u3nmv" value="7011654996639902320" />
        </node>
      </node>
    </node>
    <node concept="3clFb_" id="qJ" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="calculateCanBeChildConstraint" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3Tmbuc" id="rl" role="1B3o_S">
        <node concept="cd27G" id="rq" role="lGtFl">
          <node concept="3u3nmq" id="rr" role="cd27D">
            <property role="3u3nmv" value="7011654996639902320" />
          </node>
        </node>
      </node>
      <node concept="3uibUv" id="rm" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConstraintFunction" resolve="ConstraintFunction" />
        <node concept="3uibUv" id="rs" role="11_B2D">
          <ref role="3uigEE" to="ze1i:~ConstraintContext_CanBeChild" resolve="ConstraintContext_CanBeChild" />
          <node concept="cd27G" id="rv" role="lGtFl">
            <node concept="3u3nmq" id="rw" role="cd27D">
              <property role="3u3nmv" value="7011654996639902320" />
            </node>
          </node>
        </node>
        <node concept="3uibUv" id="rt" role="11_B2D">
          <ref role="3uigEE" to="wyt6:~Boolean" resolve="Boolean" />
          <node concept="cd27G" id="rx" role="lGtFl">
            <node concept="3u3nmq" id="ry" role="cd27D">
              <property role="3u3nmv" value="7011654996639902320" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="ru" role="lGtFl">
          <node concept="3u3nmq" id="rz" role="cd27D">
            <property role="3u3nmv" value="7011654996639902320" />
          </node>
        </node>
      </node>
      <node concept="3clFbS" id="rn" role="3clF47">
        <node concept="3clFbF" id="r$" role="3cqZAp">
          <node concept="2ShNRf" id="rA" role="3clFbG">
            <node concept="YeOm9" id="rC" role="2ShVmc">
              <node concept="1Y3b0j" id="rE" role="YeSDq">
                <property role="2bfB8j" value="true" />
                <ref role="1Y3XeK" to="ze1i:~ConstraintFunction" resolve="ConstraintFunction" />
                <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
                <node concept="3Tm1VV" id="rG" role="1B3o_S">
                  <node concept="cd27G" id="rL" role="lGtFl">
                    <node concept="3u3nmq" id="rM" role="cd27D">
                      <property role="3u3nmv" value="7011654996639902320" />
                    </node>
                  </node>
                </node>
                <node concept="3clFb_" id="rH" role="jymVt">
                  <property role="1EzhhJ" value="false" />
                  <property role="TrG5h" value="invoke" />
                  <property role="DiZV1" value="false" />
                  <property role="od$2w" value="false" />
                  <node concept="3Tm1VV" id="rN" role="1B3o_S">
                    <node concept="cd27G" id="rU" role="lGtFl">
                      <node concept="3u3nmq" id="rV" role="cd27D">
                        <property role="3u3nmv" value="7011654996639902320" />
                      </node>
                    </node>
                  </node>
                  <node concept="2AHcQZ" id="rO" role="2AJF6D">
                    <ref role="2AI5Lk" to="mhfm:~NotNull" resolve="NotNull" />
                    <node concept="cd27G" id="rW" role="lGtFl">
                      <node concept="3u3nmq" id="rX" role="cd27D">
                        <property role="3u3nmv" value="7011654996639902320" />
                      </node>
                    </node>
                  </node>
                  <node concept="3uibUv" id="rP" role="3clF45">
                    <ref role="3uigEE" to="wyt6:~Boolean" resolve="Boolean" />
                    <node concept="cd27G" id="rY" role="lGtFl">
                      <node concept="3u3nmq" id="rZ" role="cd27D">
                        <property role="3u3nmv" value="7011654996639902320" />
                      </node>
                    </node>
                  </node>
                  <node concept="37vLTG" id="rQ" role="3clF46">
                    <property role="TrG5h" value="context" />
                    <node concept="3uibUv" id="s0" role="1tU5fm">
                      <ref role="3uigEE" to="ze1i:~ConstraintContext_CanBeChild" resolve="ConstraintContext_CanBeChild" />
                      <node concept="cd27G" id="s3" role="lGtFl">
                        <node concept="3u3nmq" id="s4" role="cd27D">
                          <property role="3u3nmv" value="7011654996639902320" />
                        </node>
                      </node>
                    </node>
                    <node concept="2AHcQZ" id="s1" role="2AJF6D">
                      <ref role="2AI5Lk" to="mhfm:~NotNull" resolve="NotNull" />
                      <node concept="cd27G" id="s5" role="lGtFl">
                        <node concept="3u3nmq" id="s6" role="cd27D">
                          <property role="3u3nmv" value="7011654996639902320" />
                        </node>
                      </node>
                    </node>
                    <node concept="cd27G" id="s2" role="lGtFl">
                      <node concept="3u3nmq" id="s7" role="cd27D">
                        <property role="3u3nmv" value="7011654996639902320" />
                      </node>
                    </node>
                  </node>
                  <node concept="37vLTG" id="rR" role="3clF46">
                    <property role="TrG5h" value="checkingNodeContext" />
                    <node concept="3uibUv" id="s8" role="1tU5fm">
                      <ref role="3uigEE" to="ze1i:~CheckingNodeContext" resolve="CheckingNodeContext" />
                      <node concept="cd27G" id="sb" role="lGtFl">
                        <node concept="3u3nmq" id="sc" role="cd27D">
                          <property role="3u3nmv" value="7011654996639902320" />
                        </node>
                      </node>
                    </node>
                    <node concept="2AHcQZ" id="s9" role="2AJF6D">
                      <ref role="2AI5Lk" to="mhfm:~Nullable" resolve="Nullable" />
                      <node concept="cd27G" id="sd" role="lGtFl">
                        <node concept="3u3nmq" id="se" role="cd27D">
                          <property role="3u3nmv" value="7011654996639902320" />
                        </node>
                      </node>
                    </node>
                    <node concept="cd27G" id="sa" role="lGtFl">
                      <node concept="3u3nmq" id="sf" role="cd27D">
                        <property role="3u3nmv" value="7011654996639902320" />
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbS" id="rS" role="3clF47">
                    <node concept="3cpWs8" id="sg" role="3cqZAp">
                      <node concept="3cpWsn" id="sm" role="3cpWs9">
                        <property role="TrG5h" value="result" />
                        <node concept="10P_77" id="so" role="1tU5fm">
                          <node concept="cd27G" id="sr" role="lGtFl">
                            <node concept="3u3nmq" id="ss" role="cd27D">
                              <property role="3u3nmv" value="7011654996639902320" />
                            </node>
                          </node>
                        </node>
                        <node concept="1rXfSq" id="sp" role="33vP2m">
                          <ref role="37wK5l" node="qK" resolve="staticCanBeAChild" />
                          <node concept="2OqwBi" id="st" role="37wK5m">
                            <node concept="37vLTw" id="sy" role="2Oq$k0">
                              <ref role="3cqZAo" node="rQ" resolve="context" />
                              <node concept="cd27G" id="s_" role="lGtFl">
                                <node concept="3u3nmq" id="sA" role="cd27D">
                                  <property role="3u3nmv" value="7011654996639902320" />
                                </node>
                              </node>
                            </node>
                            <node concept="liA8E" id="sz" role="2OqNvi">
                              <ref role="37wK5l" to="ze1i:~ConstraintContext_CanBeChild.getNode():org.jetbrains.mps.openapi.model.SNode" resolve="getNode" />
                              <node concept="cd27G" id="sB" role="lGtFl">
                                <node concept="3u3nmq" id="sC" role="cd27D">
                                  <property role="3u3nmv" value="7011654996639902320" />
                                </node>
                              </node>
                            </node>
                            <node concept="cd27G" id="s$" role="lGtFl">
                              <node concept="3u3nmq" id="sD" role="cd27D">
                                <property role="3u3nmv" value="7011654996639902320" />
                              </node>
                            </node>
                          </node>
                          <node concept="2OqwBi" id="su" role="37wK5m">
                            <node concept="37vLTw" id="sE" role="2Oq$k0">
                              <ref role="3cqZAo" node="rQ" resolve="context" />
                              <node concept="cd27G" id="sH" role="lGtFl">
                                <node concept="3u3nmq" id="sI" role="cd27D">
                                  <property role="3u3nmv" value="7011654996639902320" />
                                </node>
                              </node>
                            </node>
                            <node concept="liA8E" id="sF" role="2OqNvi">
                              <ref role="37wK5l" to="ze1i:~ConstraintContext_CanBeChild.getParentNode():org.jetbrains.mps.openapi.model.SNode" resolve="getParentNode" />
                              <node concept="cd27G" id="sJ" role="lGtFl">
                                <node concept="3u3nmq" id="sK" role="cd27D">
                                  <property role="3u3nmv" value="7011654996639902320" />
                                </node>
                              </node>
                            </node>
                            <node concept="cd27G" id="sG" role="lGtFl">
                              <node concept="3u3nmq" id="sL" role="cd27D">
                                <property role="3u3nmv" value="7011654996639902320" />
                              </node>
                            </node>
                          </node>
                          <node concept="2OqwBi" id="sv" role="37wK5m">
                            <node concept="37vLTw" id="sM" role="2Oq$k0">
                              <ref role="3cqZAo" node="rQ" resolve="context" />
                              <node concept="cd27G" id="sP" role="lGtFl">
                                <node concept="3u3nmq" id="sQ" role="cd27D">
                                  <property role="3u3nmv" value="7011654996639902320" />
                                </node>
                              </node>
                            </node>
                            <node concept="liA8E" id="sN" role="2OqNvi">
                              <ref role="37wK5l" to="ze1i:~ConstraintContext_CanBeChild.getConcept():org.jetbrains.mps.openapi.language.SAbstractConcept" resolve="getConcept" />
                              <node concept="cd27G" id="sR" role="lGtFl">
                                <node concept="3u3nmq" id="sS" role="cd27D">
                                  <property role="3u3nmv" value="7011654996639902320" />
                                </node>
                              </node>
                            </node>
                            <node concept="cd27G" id="sO" role="lGtFl">
                              <node concept="3u3nmq" id="sT" role="cd27D">
                                <property role="3u3nmv" value="7011654996639902320" />
                              </node>
                            </node>
                          </node>
                          <node concept="2OqwBi" id="sw" role="37wK5m">
                            <node concept="37vLTw" id="sU" role="2Oq$k0">
                              <ref role="3cqZAo" node="rQ" resolve="context" />
                              <node concept="cd27G" id="sX" role="lGtFl">
                                <node concept="3u3nmq" id="sY" role="cd27D">
                                  <property role="3u3nmv" value="7011654996639902320" />
                                </node>
                              </node>
                            </node>
                            <node concept="liA8E" id="sV" role="2OqNvi">
                              <ref role="37wK5l" to="ze1i:~ConstraintContext_CanBeChild.getLink():org.jetbrains.mps.openapi.language.SContainmentLink" resolve="getLink" />
                              <node concept="cd27G" id="sZ" role="lGtFl">
                                <node concept="3u3nmq" id="t0" role="cd27D">
                                  <property role="3u3nmv" value="7011654996639902320" />
                                </node>
                              </node>
                            </node>
                            <node concept="cd27G" id="sW" role="lGtFl">
                              <node concept="3u3nmq" id="t1" role="cd27D">
                                <property role="3u3nmv" value="7011654996639902320" />
                              </node>
                            </node>
                          </node>
                          <node concept="cd27G" id="sx" role="lGtFl">
                            <node concept="3u3nmq" id="t2" role="cd27D">
                              <property role="3u3nmv" value="7011654996639902320" />
                            </node>
                          </node>
                        </node>
                        <node concept="cd27G" id="sq" role="lGtFl">
                          <node concept="3u3nmq" id="t3" role="cd27D">
                            <property role="3u3nmv" value="7011654996639902320" />
                          </node>
                        </node>
                      </node>
                      <node concept="cd27G" id="sn" role="lGtFl">
                        <node concept="3u3nmq" id="t4" role="cd27D">
                          <property role="3u3nmv" value="7011654996639902320" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbH" id="sh" role="3cqZAp">
                      <node concept="cd27G" id="t5" role="lGtFl">
                        <node concept="3u3nmq" id="t6" role="cd27D">
                          <property role="3u3nmv" value="7011654996639902320" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbJ" id="si" role="3cqZAp">
                      <node concept="3clFbS" id="t7" role="3clFbx">
                        <node concept="3clFbF" id="ta" role="3cqZAp">
                          <node concept="2OqwBi" id="tc" role="3clFbG">
                            <node concept="37vLTw" id="te" role="2Oq$k0">
                              <ref role="3cqZAo" node="rR" resolve="checkingNodeContext" />
                              <node concept="cd27G" id="th" role="lGtFl">
                                <node concept="3u3nmq" id="ti" role="cd27D">
                                  <property role="3u3nmv" value="7011654996639902320" />
                                </node>
                              </node>
                            </node>
                            <node concept="liA8E" id="tf" role="2OqNvi">
                              <ref role="37wK5l" to="ze1i:~CheckingNodeContext.setBreakingNode(org.jetbrains.mps.openapi.model.SNodeReference):void" resolve="setBreakingNode" />
                              <node concept="1dyn4i" id="tj" role="37wK5m">
                                <property role="1dyqJU" value="canBeChildBreakingPoint" />
                                <node concept="2ShNRf" id="tl" role="1dyrYi">
                                  <node concept="1pGfFk" id="tn" role="2ShVmc">
                                    <ref role="37wK5l" to="w1kc:~SNodePointer.&lt;init&gt;(java.lang.String,java.lang.String)" resolve="SNodePointer" />
                                    <node concept="Xl_RD" id="tp" role="37wK5m">
                                      <property role="Xl_RC" value="r:e4a7a6f9-a1de-4f08-9b98-b5f2b9ecf3a4(no.uio.mLab.decisions.references.laboratoryTest.constraints)" />
                                      <node concept="cd27G" id="ts" role="lGtFl">
                                        <node concept="3u3nmq" id="tt" role="cd27D">
                                          <property role="3u3nmv" value="7011654996639902320" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="Xl_RD" id="tq" role="37wK5m">
                                      <property role="Xl_RC" value="7011654996639902321" />
                                      <node concept="cd27G" id="tu" role="lGtFl">
                                        <node concept="3u3nmq" id="tv" role="cd27D">
                                          <property role="3u3nmv" value="7011654996639902320" />
                                        </node>
                                      </node>
                                    </node>
                                    <node concept="cd27G" id="tr" role="lGtFl">
                                      <node concept="3u3nmq" id="tw" role="cd27D">
                                        <property role="3u3nmv" value="7011654996639902320" />
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="cd27G" id="to" role="lGtFl">
                                    <node concept="3u3nmq" id="tx" role="cd27D">
                                      <property role="3u3nmv" value="7011654996639902320" />
                                    </node>
                                  </node>
                                </node>
                                <node concept="cd27G" id="tm" role="lGtFl">
                                  <node concept="3u3nmq" id="ty" role="cd27D">
                                    <property role="3u3nmv" value="7011654996639902320" />
                                  </node>
                                </node>
                              </node>
                              <node concept="cd27G" id="tk" role="lGtFl">
                                <node concept="3u3nmq" id="tz" role="cd27D">
                                  <property role="3u3nmv" value="7011654996639902320" />
                                </node>
                              </node>
                            </node>
                            <node concept="cd27G" id="tg" role="lGtFl">
                              <node concept="3u3nmq" id="t$" role="cd27D">
                                <property role="3u3nmv" value="7011654996639902320" />
                              </node>
                            </node>
                          </node>
                          <node concept="cd27G" id="td" role="lGtFl">
                            <node concept="3u3nmq" id="t_" role="cd27D">
                              <property role="3u3nmv" value="7011654996639902320" />
                            </node>
                          </node>
                        </node>
                        <node concept="cd27G" id="tb" role="lGtFl">
                          <node concept="3u3nmq" id="tA" role="cd27D">
                            <property role="3u3nmv" value="7011654996639902320" />
                          </node>
                        </node>
                      </node>
                      <node concept="1Wc70l" id="t8" role="3clFbw">
                        <node concept="3y3z36" id="tB" role="3uHU7w">
                          <node concept="10Nm6u" id="tE" role="3uHU7w">
                            <node concept="cd27G" id="tH" role="lGtFl">
                              <node concept="3u3nmq" id="tI" role="cd27D">
                                <property role="3u3nmv" value="7011654996639902320" />
                              </node>
                            </node>
                          </node>
                          <node concept="37vLTw" id="tF" role="3uHU7B">
                            <ref role="3cqZAo" node="rR" resolve="checkingNodeContext" />
                            <node concept="cd27G" id="tJ" role="lGtFl">
                              <node concept="3u3nmq" id="tK" role="cd27D">
                                <property role="3u3nmv" value="7011654996639902320" />
                              </node>
                            </node>
                          </node>
                          <node concept="cd27G" id="tG" role="lGtFl">
                            <node concept="3u3nmq" id="tL" role="cd27D">
                              <property role="3u3nmv" value="7011654996639902320" />
                            </node>
                          </node>
                        </node>
                        <node concept="3fqX7Q" id="tC" role="3uHU7B">
                          <node concept="37vLTw" id="tM" role="3fr31v">
                            <ref role="3cqZAo" node="sm" resolve="result" />
                            <node concept="cd27G" id="tO" role="lGtFl">
                              <node concept="3u3nmq" id="tP" role="cd27D">
                                <property role="3u3nmv" value="7011654996639902320" />
                              </node>
                            </node>
                          </node>
                          <node concept="cd27G" id="tN" role="lGtFl">
                            <node concept="3u3nmq" id="tQ" role="cd27D">
                              <property role="3u3nmv" value="7011654996639902320" />
                            </node>
                          </node>
                        </node>
                        <node concept="cd27G" id="tD" role="lGtFl">
                          <node concept="3u3nmq" id="tR" role="cd27D">
                            <property role="3u3nmv" value="7011654996639902320" />
                          </node>
                        </node>
                      </node>
                      <node concept="cd27G" id="t9" role="lGtFl">
                        <node concept="3u3nmq" id="tS" role="cd27D">
                          <property role="3u3nmv" value="7011654996639902320" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbH" id="sj" role="3cqZAp">
                      <node concept="cd27G" id="tT" role="lGtFl">
                        <node concept="3u3nmq" id="tU" role="cd27D">
                          <property role="3u3nmv" value="7011654996639902320" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbF" id="sk" role="3cqZAp">
                      <node concept="37vLTw" id="tV" role="3clFbG">
                        <ref role="3cqZAo" node="sm" resolve="result" />
                        <node concept="cd27G" id="tX" role="lGtFl">
                          <node concept="3u3nmq" id="tY" role="cd27D">
                            <property role="3u3nmv" value="7011654996639902320" />
                          </node>
                        </node>
                      </node>
                      <node concept="cd27G" id="tW" role="lGtFl">
                        <node concept="3u3nmq" id="tZ" role="cd27D">
                          <property role="3u3nmv" value="7011654996639902320" />
                        </node>
                      </node>
                    </node>
                    <node concept="cd27G" id="sl" role="lGtFl">
                      <node concept="3u3nmq" id="u0" role="cd27D">
                        <property role="3u3nmv" value="7011654996639902320" />
                      </node>
                    </node>
                  </node>
                  <node concept="cd27G" id="rT" role="lGtFl">
                    <node concept="3u3nmq" id="u1" role="cd27D">
                      <property role="3u3nmv" value="7011654996639902320" />
                    </node>
                  </node>
                </node>
                <node concept="3uibUv" id="rI" role="2Ghqu4">
                  <ref role="3uigEE" to="ze1i:~ConstraintContext_CanBeChild" resolve="ConstraintContext_CanBeChild" />
                  <node concept="cd27G" id="u2" role="lGtFl">
                    <node concept="3u3nmq" id="u3" role="cd27D">
                      <property role="3u3nmv" value="7011654996639902320" />
                    </node>
                  </node>
                </node>
                <node concept="3uibUv" id="rJ" role="2Ghqu4">
                  <ref role="3uigEE" to="wyt6:~Boolean" resolve="Boolean" />
                  <node concept="cd27G" id="u4" role="lGtFl">
                    <node concept="3u3nmq" id="u5" role="cd27D">
                      <property role="3u3nmv" value="7011654996639902320" />
                    </node>
                  </node>
                </node>
                <node concept="cd27G" id="rK" role="lGtFl">
                  <node concept="3u3nmq" id="u6" role="cd27D">
                    <property role="3u3nmv" value="7011654996639902320" />
                  </node>
                </node>
              </node>
              <node concept="cd27G" id="rF" role="lGtFl">
                <node concept="3u3nmq" id="u7" role="cd27D">
                  <property role="3u3nmv" value="7011654996639902320" />
                </node>
              </node>
            </node>
            <node concept="cd27G" id="rD" role="lGtFl">
              <node concept="3u3nmq" id="u8" role="cd27D">
                <property role="3u3nmv" value="7011654996639902320" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="rB" role="lGtFl">
            <node concept="3u3nmq" id="u9" role="cd27D">
              <property role="3u3nmv" value="7011654996639902320" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="r_" role="lGtFl">
          <node concept="3u3nmq" id="ua" role="cd27D">
            <property role="3u3nmv" value="7011654996639902320" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="ro" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
        <node concept="cd27G" id="ub" role="lGtFl">
          <node concept="3u3nmq" id="uc" role="cd27D">
            <property role="3u3nmv" value="7011654996639902320" />
          </node>
        </node>
      </node>
      <node concept="cd27G" id="rp" role="lGtFl">
        <node concept="3u3nmq" id="ud" role="cd27D">
          <property role="3u3nmv" value="7011654996639902320" />
        </node>
      </node>
    </node>
    <node concept="2YIFZL" id="qK" role="jymVt">
      <property role="TrG5h" value="staticCanBeAChild" />
      <node concept="10P_77" id="ue" role="3clF45">
        <node concept="cd27G" id="um" role="lGtFl">
          <node concept="3u3nmq" id="un" role="cd27D">
            <property role="3u3nmv" value="7011654996639902320" />
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="uf" role="1B3o_S">
        <node concept="cd27G" id="uo" role="lGtFl">
          <node concept="3u3nmq" id="up" role="cd27D">
            <property role="3u3nmv" value="7011654996639902320" />
          </node>
        </node>
      </node>
      <node concept="3clFbS" id="ug" role="3clF47">
        <node concept="3clFbF" id="uq" role="3cqZAp">
          <node concept="2OqwBi" id="us" role="3clFbG">
            <node concept="37vLTw" id="uu" role="2Oq$k0">
              <ref role="3cqZAo" node="ui" resolve="parentNode" />
              <node concept="cd27G" id="ux" role="lGtFl">
                <node concept="3u3nmq" id="uy" role="cd27D">
                  <property role="3u3nmv" value="7011654996639902788" />
                </node>
              </node>
            </node>
            <node concept="1mIQ4w" id="uv" role="2OqNvi">
              <node concept="chp4Y" id="uz" role="cj9EA">
                <ref role="cht4Q" to="ruww:4B5aqq2QmL6" resolve="LaboratoryTestWithTextResultVariable" />
                <node concept="cd27G" id="u_" role="lGtFl">
                  <node concept="3u3nmq" id="uA" role="cd27D">
                    <property role="3u3nmv" value="7011654996639906773" />
                  </node>
                </node>
              </node>
              <node concept="cd27G" id="u$" role="lGtFl">
                <node concept="3u3nmq" id="uB" role="cd27D">
                  <property role="3u3nmv" value="7011654996639905960" />
                </node>
              </node>
            </node>
            <node concept="cd27G" id="uw" role="lGtFl">
              <node concept="3u3nmq" id="uC" role="cd27D">
                <property role="3u3nmv" value="7011654996639903537" />
              </node>
            </node>
          </node>
          <node concept="cd27G" id="ut" role="lGtFl">
            <node concept="3u3nmq" id="uD" role="cd27D">
              <property role="3u3nmv" value="7011654996639902789" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="ur" role="lGtFl">
          <node concept="3u3nmq" id="uE" role="cd27D">
            <property role="3u3nmv" value="7011654996639902322" />
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="uh" role="3clF46">
        <property role="TrG5h" value="node" />
        <node concept="3uibUv" id="uF" role="1tU5fm">
          <ref role="3uigEE" to="mhbf:~SNode" resolve="SNode" />
          <node concept="cd27G" id="uH" role="lGtFl">
            <node concept="3u3nmq" id="uI" role="cd27D">
              <property role="3u3nmv" value="7011654996639902320" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="uG" role="lGtFl">
          <node concept="3u3nmq" id="uJ" role="cd27D">
            <property role="3u3nmv" value="7011654996639902320" />
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="ui" role="3clF46">
        <property role="TrG5h" value="parentNode" />
        <node concept="3uibUv" id="uK" role="1tU5fm">
          <ref role="3uigEE" to="mhbf:~SNode" resolve="SNode" />
          <node concept="cd27G" id="uM" role="lGtFl">
            <node concept="3u3nmq" id="uN" role="cd27D">
              <property role="3u3nmv" value="7011654996639902320" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="uL" role="lGtFl">
          <node concept="3u3nmq" id="uO" role="cd27D">
            <property role="3u3nmv" value="7011654996639902320" />
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="uj" role="3clF46">
        <property role="TrG5h" value="childConcept" />
        <node concept="3uibUv" id="uP" role="1tU5fm">
          <ref role="3uigEE" to="c17a:~SAbstractConcept" resolve="SAbstractConcept" />
          <node concept="cd27G" id="uR" role="lGtFl">
            <node concept="3u3nmq" id="uS" role="cd27D">
              <property role="3u3nmv" value="7011654996639902320" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="uQ" role="lGtFl">
          <node concept="3u3nmq" id="uT" role="cd27D">
            <property role="3u3nmv" value="7011654996639902320" />
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="uk" role="3clF46">
        <property role="TrG5h" value="link" />
        <node concept="3uibUv" id="uU" role="1tU5fm">
          <ref role="3uigEE" to="c17a:~SContainmentLink" resolve="SContainmentLink" />
          <node concept="cd27G" id="uW" role="lGtFl">
            <node concept="3u3nmq" id="uX" role="cd27D">
              <property role="3u3nmv" value="7011654996639902320" />
            </node>
          </node>
        </node>
        <node concept="cd27G" id="uV" role="lGtFl">
          <node concept="3u3nmq" id="uY" role="cd27D">
            <property role="3u3nmv" value="7011654996639902320" />
          </node>
        </node>
      </node>
      <node concept="cd27G" id="ul" role="lGtFl">
        <node concept="3u3nmq" id="uZ" role="cd27D">
          <property role="3u3nmv" value="7011654996639902320" />
        </node>
      </node>
    </node>
    <node concept="cd27G" id="qL" role="lGtFl">
      <node concept="3u3nmq" id="v0" role="cd27D">
        <property role="3u3nmv" value="7011654996639902320" />
      </node>
    </node>
  </node>
</model>

