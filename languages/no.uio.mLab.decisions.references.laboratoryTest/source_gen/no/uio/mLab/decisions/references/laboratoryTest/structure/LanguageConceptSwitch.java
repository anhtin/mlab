package no.uio.mLab.decisions.references.laboratoryTest.structure;

/*Generated by MPS */

import jetbrains.mps.lang.smodel.LanguageConceptIndex;
import jetbrains.mps.lang.smodel.LanguageConceptIndexBuilder;
import jetbrains.mps.smodel.adapter.ids.SConceptId;
import org.jetbrains.mps.openapi.language.SAbstractConcept;

public final class LanguageConceptSwitch {
  private final LanguageConceptIndex myIndex;
  public static final int AspectLaboratoryTestReference = 0;
  public static final int AspectLaboratoryTestWithNumberResultReference = 1;
  public static final int AspectLaboratoryTestWithTextResultReference = 2;
  public static final int AspectSpecimenTypeReference = 3;
  public static final int EntityLaboratoryTestReference = 4;
  public static final int EntityLaboratoryTestWithNumberResultReference = 5;
  public static final int EntityLaboratoryTestWithTextResultReference = 6;
  public static final int EntitySpecimenTypeReference = 7;
  public static final int ITranslatableLaboratoryTestReferenceConcept = 8;
  public static final int LaboratoryTestPattern = 9;
  public static final int LaboratoryTestReference = 10;
  public static final int LaboratoryTestVariable = 11;
  public static final int LaboratoryTestWithNumberResultPattern = 12;
  public static final int LaboratoryTestWithNumberResultReference = 13;
  public static final int LaboratoryTestWithNumberResultVariable = 14;
  public static final int LaboratoryTestWithTextResultPattern = 15;
  public static final int LaboratoryTestWithTextResultReference = 16;
  public static final int LaboratoryTestWithTextResultVariable = 17;
  public static final int SelectedLaboratoryTestPattern = 18;
  public static final int SelectedLaboratoryTestWithNumberPattern = 19;
  public static final int SelectedLaboratoryTestWithTextPattern = 20;
  public static final int SpecimenTypePattern = 21;
  public static final int SpecimenTypeReference = 22;
  public static final int SpecimenTypeVariable = 23;
  public static final int WildcardLaboratoryTestPattern = 24;
  public static final int WildcardLaboratoryTestWithNumberPattern = 25;
  public static final int WildcardLaboratoryTestWithTextPattern = 26;
  public static final int WildcardSpecimenTypePattern = 27;

  public LanguageConceptSwitch() {
    LanguageConceptIndexBuilder builder = new LanguageConceptIndexBuilder(0x4a652d5536844d2dL, 0x98c92ef46f124c44L);
    builder.put(0x4dbaf0338f8270caL, AspectLaboratoryTestReference);
    builder.put(0x49c529a682d90a9bL, AspectLaboratoryTestWithNumberResultReference);
    builder.put(0x49c529a682d90a9dL, AspectLaboratoryTestWithTextResultReference);
    builder.put(0x6511ed286229f47fL, AspectSpecimenTypeReference);
    builder.put(0x1b881273c8529730L, EntityLaboratoryTestReference);
    builder.put(0x1b881273c852974dL, EntityLaboratoryTestWithNumberResultReference);
    builder.put(0x1b881273c8529750L, EntityLaboratoryTestWithTextResultReference);
    builder.put(0x6511ed286229eb8eL, EntitySpecimenTypeReference);
    builder.put(0x4dbaf0338d7a6471L, ITranslatableLaboratoryTestReferenceConcept);
    builder.put(0x6c7943d5b36669a2L, LaboratoryTestPattern);
    builder.put(0x4dbaf0338f8270c0L, LaboratoryTestReference);
    builder.put(0x4dbaf0338da7b8b0L, LaboratoryTestVariable);
    builder.put(0x614e6711f25335b0L, LaboratoryTestWithNumberResultPattern);
    builder.put(0x4ec3b323fdb62189L, LaboratoryTestWithNumberResultReference);
    builder.put(0x49c529a682d96c45L, LaboratoryTestWithNumberResultVariable);
    builder.put(0x614e6711f24f51e1L, LaboratoryTestWithTextResultPattern);
    builder.put(0x4ec3b323fdb62841L, LaboratoryTestWithTextResultReference);
    builder.put(0x49c529a682d96c46L, LaboratoryTestWithTextResultVariable);
    builder.put(0x6c7943d5b3f5c133L, SelectedLaboratoryTestPattern);
    builder.put(0x614e6711f25335b1L, SelectedLaboratoryTestWithNumberPattern);
    builder.put(0x614e6711f24f51e2L, SelectedLaboratoryTestWithTextPattern);
    builder.put(0x6c7943d5b3d8855cL, SpecimenTypePattern);
    builder.put(0x6511ed286229eb8dL, SpecimenTypeReference);
    builder.put(0x6511ed286229f47eL, SpecimenTypeVariable);
    builder.put(0x6c7943d5b2d7a65dL, WildcardLaboratoryTestPattern);
    builder.put(0x614e6711f25335b3L, WildcardLaboratoryTestWithNumberPattern);
    builder.put(0x614e6711f24f51e4L, WildcardLaboratoryTestWithTextPattern);
    builder.put(0x6c7943d5b3d97f16L, WildcardSpecimenTypePattern);
    myIndex = builder.seal();
  }

  /*package*/ int index(SConceptId cid) {
    return myIndex.index(cid);
  }

  public int index(SAbstractConcept concept) {
    return myIndex.index(concept);
  }
}
