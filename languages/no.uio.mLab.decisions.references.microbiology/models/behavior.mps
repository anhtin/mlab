<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:217a4f58-3a08-4926-9576-bdee28b3e305(no.uio.mLab.decisions.references.microbiology.behavior)">
  <persistence version="9" />
  <languages>
    <use id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior" version="1" />
    <use id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage" version="6" />
    <use id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel" version="11" />
    <use id="3a13115c-633c-4c5c-bbcc-75c4219e9555" name="jetbrains.mps.lang.quotation" version="0" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="1yj" ref="r:0c887d67-81ab-420f-9a35-a9f6bf96c975(no.uio.mLab.shared.behavior)" />
    <import index="wb6c" ref="r:19d15037-83c1-4ae2-b66a-763828cb8279(no.uio.mLab.decisions.core.behavior)" />
    <import index="ok0m" ref="r:4db7f216-3df2-435e-8be1-a0ab7e4a26b5(no.uio.mLab.decisions.references.microbiology.structure)" />
    <import index="qzk2" ref="r:111a6c30-c76c-4d86-a426-b4a1815fe195(no.uio.mLab.entities.microbiology.structure)" />
    <import index="kkto" ref="r:01f3534b-874e-42dc-a9b5-7540591f45bb(no.uio.mLab.entities.core.structure)" />
  </imports>
  <registry>
    <language id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior">
      <concept id="1225194240794" name="jetbrains.mps.lang.behavior.structure.ConceptBehavior" flags="ng" index="13h7C7">
        <reference id="1225194240799" name="concept" index="13h7C2" />
        <child id="1225194240805" name="method" index="13h7CS" />
        <child id="1225194240801" name="constructor" index="13h7CW" />
      </concept>
      <concept id="1225194413805" name="jetbrains.mps.lang.behavior.structure.ConceptConstructorDeclaration" flags="in" index="13hLZK" />
      <concept id="1225194472830" name="jetbrains.mps.lang.behavior.structure.ConceptMethodDeclaration" flags="ng" index="13i0hz">
        <property id="5864038008284099149" name="isStatic" index="2Ki8OM" />
        <property id="1225194472832" name="isVirtual" index="13i0it" />
        <property id="1225194472834" name="isAbstract" index="13i0iv" />
        <reference id="1225194472831" name="overriddenMethod" index="13i0hy" />
      </concept>
      <concept id="1225194691553" name="jetbrains.mps.lang.behavior.structure.ThisNodeExpression" flags="nn" index="13iPFW" />
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1070534644030" name="jetbrains.mps.baseLanguage.structure.BooleanType" flags="in" index="10P_77" />
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123137" name="jetbrains.mps.baseLanguage.structure.BooleanConstant" flags="nn" index="3clFbT">
        <property id="1068580123138" name="value" index="3clFbU" />
      </concept>
      <concept id="1068581517677" name="jetbrains.mps.baseLanguage.structure.VoidType" flags="in" index="3cqZAl" />
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
    </language>
    <language id="3a13115c-633c-4c5c-bbcc-75c4219e9555" name="jetbrains.mps.lang.quotation">
      <concept id="5455284157994012186" name="jetbrains.mps.lang.quotation.structure.NodeBuilderInitLink" flags="ng" index="2pIpSj">
        <reference id="5455284157994012188" name="link" index="2pIpSl" />
      </concept>
      <concept id="5455284157993911097" name="jetbrains.mps.lang.quotation.structure.NodeBuilderInitPart" flags="ng" index="2pJxcK">
        <child id="5455284157993911094" name="expression" index="2pJxcZ" />
      </concept>
      <concept id="5455284157993863837" name="jetbrains.mps.lang.quotation.structure.NodeBuilder" flags="nn" index="2pJPEk">
        <child id="5455284157993863838" name="quotedNode" index="2pJPEn" />
      </concept>
      <concept id="5455284157993863840" name="jetbrains.mps.lang.quotation.structure.NodeBuilderNode" flags="nn" index="2pJPED">
        <reference id="5455284157993910961" name="concept" index="2pJxaS" />
        <child id="5455284157993911099" name="values" index="2pJxcM" />
      </concept>
      <concept id="8182547171709752110" name="jetbrains.mps.lang.quotation.structure.NodeBuilderExpression" flags="nn" index="36biLy">
        <child id="8182547171709752112" name="expression" index="36biLW" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="2396822768958367367" name="jetbrains.mps.lang.smodel.structure.AbstractTypeCastExpression" flags="nn" index="$5XWr">
        <child id="6733348108486823193" name="leftExpression" index="1m5AlR" />
        <child id="3906496115198199033" name="conceptArgument" index="3oSUPX" />
      </concept>
      <concept id="2644386474300074836" name="jetbrains.mps.lang.smodel.structure.ConceptIdRefExpression" flags="nn" index="35c_gC">
        <reference id="2644386474300074837" name="conceptDeclaration" index="35c_gD" />
      </concept>
      <concept id="6677504323281689838" name="jetbrains.mps.lang.smodel.structure.SConceptType" flags="in" index="3bZ5Sz">
        <reference id="6677504323281689839" name="conceptDeclaraton" index="3bZ5Sy" />
      </concept>
      <concept id="1140131837776" name="jetbrains.mps.lang.smodel.structure.Node_ReplaceWithAnotherOperation" flags="nn" index="1P9Npp">
        <child id="1140131861877" name="replacementNode" index="1P9ThW" />
      </concept>
      <concept id="1140137987495" name="jetbrains.mps.lang.smodel.structure.SNodeTypeCastExpression" flags="nn" index="1PxgMI" />
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="13h7C7" id="6khVixyal_j">
    <property role="3GE5qa" value="aspect.variables" />
    <ref role="13h7C2" to="ok0m:6khVixyair6" resolve="CultureTestVariable" />
    <node concept="13hLZK" id="6khVixyal_k" role="13h7CW">
      <node concept="3clFbS" id="6khVixyal_l" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6LTgXmNW5dv" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="6LTgXmNW5dw" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNW5d_" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNW5dE" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNW5lN" role="3clFbG">
            <property role="Xl_RC" value="culture test" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmNW5dA" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmNW5dF" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="6LTgXmNW5dG" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNW5dL" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNW5dQ" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNW5mS" role="3clFbG">
            <property role="Xl_RC" value="" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmNW5dM" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixyal_u" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getReferenceDescription" />
      <property role="2Ki8OM" value="false" />
      <ref role="13i0hy" to="1yj:4V3GMfXuP$X" resolve="getReferenceDescription" />
      <node concept="3Tm1VV" id="6khVixyal_v" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixyal_E" role="3clF47">
        <node concept="3clFbF" id="6khVixyalG1" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixyalG0" role="3clFbG">
            <property role="Xl_RC" value="culture test" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixyal_F" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="6khVixyaEal">
    <property role="3GE5qa" value="aspect.variables" />
    <ref role="13h7C2" to="ok0m:6khVixyapzB" resolve="PathogenVariable" />
    <node concept="13hLZK" id="6khVixyaEam" role="13h7CW">
      <node concept="3clFbS" id="6khVixyaEan" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6LTgXmNW53Z" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="6LTgXmNW540" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNW545" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNW54a" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNW5cj" role="3clFbG">
            <property role="Xl_RC" value="pathogen" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmNW546" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmNW54b" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="6LTgXmNW54c" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNW54h" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNW54m" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNW5cV" role="3clFbG">
            <property role="Xl_RC" value="" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmNW54i" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixyaEaw" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getReferenceDescription" />
      <property role="2Ki8OM" value="false" />
      <ref role="13i0hy" to="1yj:4V3GMfXuP$X" resolve="getReferenceDescription" />
      <node concept="3Tm1VV" id="6khVixyaEax" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixyaEaG" role="3clF47">
        <node concept="3clFbF" id="6khVixyaEfy" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixyaEfx" role="3clFbG">
            <property role="Xl_RC" value="pathogen" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixyaEaH" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="6LTgXmNVtVj">
    <property role="3GE5qa" value="aspect.patterns.antimicrobial" />
    <ref role="13h7C2" to="ok0m:6LTgXmNVtUT" resolve="WilcardAntimicrobialPattern" />
    <node concept="13hLZK" id="6LTgXmNVtVk" role="13h7CW">
      <node concept="3clFbS" id="6LTgXmNVtVl" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6LTgXmNVtVI" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="6LTgXmNVtVJ" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNVtVO" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNVuds" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNVudr" role="3clFbG">
            <property role="Xl_RC" value="*" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmNVtVP" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmNVtVU" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="6LTgXmNVtVV" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNVtW0" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNVtW5" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNVue6" role="3clFbG">
            <property role="Xl_RC" value="any microbial agent" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmNVtW1" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmNVtW6" role="13h7CS">
      <property role="TrG5h" value="hasPattern" />
      <property role="13i0it" value="false" />
      <property role="13i0iv" value="false" />
      <ref role="13i0hy" to="wb6c:6LTgXmNRv0n" resolve="hasPattern" />
      <node concept="3Tm1VV" id="6LTgXmNVtW7" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNVtWc" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNVtWh" role="3cqZAp">
          <node concept="3clFbT" id="6LTgXmNVuj3" role="3clFbG">
            <property role="3clFbU" value="true" />
          </node>
        </node>
      </node>
      <node concept="10P_77" id="6LTgXmNVtWd" role="3clF45" />
    </node>
    <node concept="13i0hz" id="2FjKBCOhjPg" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getMatchEntityConcept" />
      <ref role="13i0hy" to="wb6c:65epL7MnLJr" resolve="getMatchEntityConcept" />
      <node concept="3Tm1VV" id="2FjKBCOhjPh" role="1B3o_S" />
      <node concept="3clFbS" id="2FjKBCOhjPm" role="3clF47">
        <node concept="3clFbF" id="2FjKBCOhjPr" role="3cqZAp">
          <node concept="35c_gC" id="2FjKBCOhjZy" role="3clFbG">
            <ref role="35c_gD" to="qzk2:6khVixyGBqF" resolve="Antimicrobial" />
          </node>
        </node>
      </node>
      <node concept="3bZ5Sz" id="2FjKBCOhjPn" role="3clF45">
        <ref role="3bZ5Sy" to="kkto:4QUW3edDqMR" resolve="Entity" />
      </node>
    </node>
  </node>
  <node concept="13h7C7" id="6LTgXmNVCwN">
    <property role="3GE5qa" value="aspect.variables" />
    <ref role="13h7C2" to="ok0m:6khVixyGCLg" resolve="AntimicrobialVariable" />
    <node concept="13hLZK" id="6LTgXmNVCwO" role="13h7CW">
      <node concept="3clFbS" id="6LTgXmNVCwP" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6LTgXmNVCwY" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="6LTgXmNVCwZ" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNVCx4" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNVCx9" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNVC_o" role="3clFbG">
            <property role="Xl_RC" value="antimicrobial agent" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmNVCx5" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmNVCxa" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="6LTgXmNVCxb" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNVCxg" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNVCxl" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNVCAO" role="3clFbG">
            <property role="Xl_RC" value="" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmNVCxh" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmNVCBF" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getReferenceDescription" />
      <property role="2Ki8OM" value="false" />
      <ref role="13i0hy" to="1yj:4V3GMfXuP$X" resolve="getReferenceDescription" />
      <node concept="3Tm1VV" id="6LTgXmNVCBG" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNVCBR" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNVCLa" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNVCL9" role="3clFbG">
            <property role="Xl_RC" value="antimicrobial agent" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmNVCBS" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="6LTgXmNVRLQ">
    <property role="3GE5qa" value="aspect.patterns.cultureTest" />
    <ref role="13h7C2" to="ok0m:6LTgXmNVRKW" resolve="WildcardCultureTestPattern" />
    <node concept="13hLZK" id="6LTgXmNVRLR" role="13h7CW">
      <node concept="3clFbS" id="6LTgXmNVRLS" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6LTgXmNVRM1" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="6LTgXmNVRM2" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNVRM7" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNVS1W" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNVS1V" role="3clFbG">
            <property role="Xl_RC" value="*" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmNVRM8" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmNVRMd" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="6LTgXmNVRMe" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNVRMj" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNVRMo" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNVS2A" role="3clFbG">
            <property role="Xl_RC" value="any culture test" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmNVRMk" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmNVRMp" role="13h7CS">
      <property role="TrG5h" value="hasPattern" />
      <property role="13i0it" value="false" />
      <property role="13i0iv" value="false" />
      <ref role="13i0hy" to="wb6c:6LTgXmNRv0n" resolve="hasPattern" />
      <node concept="3Tm1VV" id="6LTgXmNVRMq" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNVRMv" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNVRM$" role="3cqZAp">
          <node concept="3clFbT" id="6LTgXmNVS3h" role="3clFbG">
            <property role="3clFbU" value="true" />
          </node>
        </node>
      </node>
      <node concept="10P_77" id="6LTgXmNVRMw" role="3clF45" />
    </node>
    <node concept="13i0hz" id="2FjKBCOhkKX" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getMatchEntityConcept" />
      <ref role="13i0hy" to="wb6c:65epL7MnLJr" resolve="getMatchEntityConcept" />
      <node concept="3Tm1VV" id="2FjKBCOhkKY" role="1B3o_S" />
      <node concept="3clFbS" id="2FjKBCOhkL3" role="3clF47">
        <node concept="3clFbF" id="2FjKBCOhkUo" role="3cqZAp">
          <node concept="35c_gC" id="2FjKBCOhkUj" role="3clFbG">
            <ref role="35c_gD" to="qzk2:6khVixyahUI" resolve="CultureTest" />
          </node>
        </node>
      </node>
      <node concept="3bZ5Sz" id="2FjKBCOhkL4" role="3clF45">
        <ref role="3bZ5Sy" to="kkto:4QUW3edDqMR" resolve="Entity" />
      </node>
    </node>
  </node>
  <node concept="13h7C7" id="6LTgXmNWe0K">
    <property role="3GE5qa" value="aspect.patterns.pathogen" />
    <ref role="13h7C2" to="ok0m:6LTgXmNW6Ec" resolve="WildcardPathogenPattern" />
    <node concept="13hLZK" id="6LTgXmNWe0L" role="13h7CW">
      <node concept="3clFbS" id="6LTgXmNWe0M" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6LTgXmNWe0V" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="6LTgXmNWe0W" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNWe11" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNWegQ" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNWegP" role="3clFbG">
            <property role="Xl_RC" value="*" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmNWe12" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmNWe17" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="6LTgXmNWe18" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNWe1d" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNWe1i" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNWehw" role="3clFbG">
            <property role="Xl_RC" value="any pathogen" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6LTgXmNWe1e" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmNWe1j" role="13h7CS">
      <property role="TrG5h" value="hasPattern" />
      <property role="13i0it" value="false" />
      <property role="13i0iv" value="false" />
      <ref role="13i0hy" to="wb6c:6LTgXmNRv0n" resolve="hasPattern" />
      <node concept="3Tm1VV" id="6LTgXmNWe1k" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNWe1p" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNWe1u" role="3cqZAp">
          <node concept="3clFbT" id="6LTgXmNWeil" role="3clFbG">
            <property role="3clFbU" value="true" />
          </node>
        </node>
      </node>
      <node concept="10P_77" id="6LTgXmNWe1q" role="3clF45" />
    </node>
    <node concept="13i0hz" id="2FjKBCOhlcM" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getMatchEntityConcept" />
      <ref role="13i0hy" to="wb6c:65epL7MnLJr" resolve="getMatchEntityConcept" />
      <node concept="3Tm1VV" id="2FjKBCOhlcN" role="1B3o_S" />
      <node concept="3clFbS" id="2FjKBCOhlcO" role="3clF47">
        <node concept="3clFbF" id="2FjKBCOhlcP" role="3cqZAp">
          <node concept="35c_gC" id="2FjKBCOhlcQ" role="3clFbG">
            <ref role="35c_gD" to="qzk2:6khVixy0xXs" resolve="Pathogen" />
          </node>
        </node>
      </node>
      <node concept="3bZ5Sz" id="2FjKBCOhlcR" role="3clF45">
        <ref role="3bZ5Sy" to="kkto:4QUW3edDqMR" resolve="Entity" />
      </node>
    </node>
  </node>
  <node concept="13h7C7" id="1I84Bf8ktb5">
    <property role="3GE5qa" value="aspect.references" />
    <ref role="13h7C2" to="ok0m:6khVixyGCLh" resolve="AspectAntimicrobialReference" />
    <node concept="13hLZK" id="1I84Bf8ktb6" role="13h7CW">
      <node concept="3clFbS" id="1I84Bf8ktb7" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="1I84Bf8ktbg" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="replaceWith" />
      <ref role="13i0hy" to="wb6c:1I84Bf8ilTw" resolve="replaceWith" />
      <node concept="3Tm1VV" id="1I84Bf8ktbh" role="1B3o_S" />
      <node concept="3clFbS" id="1I84Bf8ktbr" role="3clF47">
        <node concept="3clFbF" id="1I84Bf8ktim" role="3cqZAp">
          <node concept="2OqwBi" id="1I84Bf8ktuS" role="3clFbG">
            <node concept="13iPFW" id="1I84Bf8ktil" role="2Oq$k0" />
            <node concept="1P9Npp" id="1I84Bf8ktMs" role="2OqNvi">
              <node concept="2pJPEk" id="1I84Bf8ktOE" role="1P9ThW">
                <node concept="2pJPED" id="1I84Bf8ktSq" role="2pJPEn">
                  <ref role="2pJxaS" to="ok0m:6khVixyGBAp" resolve="EntityAntimicrobialReference" />
                  <node concept="2pIpSj" id="1I84Bf8ktTs" role="2pJxcM">
                    <ref role="2pIpSl" to="ok0m:6khVixyGBAq" resolve="target" />
                    <node concept="36biLy" id="1I84Bf8ktXe" role="2pJxcZ">
                      <node concept="1PxgMI" id="1I84Bf8kusl" role="36biLW">
                        <node concept="chp4Y" id="1I84Bf8kusS" role="3oSUPX">
                          <ref role="cht4Q" to="qzk2:6khVixyGBqF" resolve="Antimicrobial" />
                        </node>
                        <node concept="37vLTw" id="1I84Bf8ktXx" role="1m5AlR">
                          <ref role="3cqZAo" node="1I84Bf8ktbs" resolve="node" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="1I84Bf8ktbs" role="3clF46">
        <property role="TrG5h" value="node" />
        <node concept="3Tqbb2" id="1I84Bf8ktbt" role="1tU5fm" />
      </node>
      <node concept="3cqZAl" id="1I84Bf8ktbu" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="1I84Bf8k_Oj">
    <property role="3GE5qa" value="aspect.references" />
    <ref role="13h7C2" to="ok0m:6khVixyai6r" resolve="AspectCultureTestReference" />
    <node concept="13hLZK" id="1I84Bf8k_Ok" role="13h7CW">
      <node concept="3clFbS" id="1I84Bf8k_Ol" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="1I84Bf8k_Ou" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="replaceWith" />
      <ref role="13i0hy" to="wb6c:1I84Bf8ilTw" resolve="replaceWith" />
      <node concept="3Tm1VV" id="1I84Bf8k_Ov" role="1B3o_S" />
      <node concept="3clFbS" id="1I84Bf8k_OD" role="3clF47">
        <node concept="3clFbF" id="1I84Bf8k_VF" role="3cqZAp">
          <node concept="2OqwBi" id="1I84Bf8kA9B" role="3clFbG">
            <node concept="13iPFW" id="1I84Bf8k_V_" role="2Oq$k0" />
            <node concept="1P9Npp" id="1I84Bf8kAwt" role="2OqNvi">
              <node concept="2pJPEk" id="1I84Bf8kAyF" role="1P9ThW">
                <node concept="2pJPED" id="1I84Bf8kA$T" role="2pJPEn">
                  <ref role="2pJxaS" to="ok0m:6khVixyai6q" resolve="EntityCultureTestReference" />
                  <node concept="2pIpSj" id="1I84Bf8kA_V" role="2pJxcM">
                    <ref role="2pIpSl" to="ok0m:6khVixyai6s" resolve="target" />
                    <node concept="36biLy" id="1I84Bf8kAB1" role="2pJxcZ">
                      <node concept="1PxgMI" id="1I84Bf8kB2V" role="36biLW">
                        <node concept="chp4Y" id="1I84Bf8kB3u" role="3oSUPX">
                          <ref role="cht4Q" to="qzk2:6khVixyahUI" resolve="CultureTest" />
                        </node>
                        <node concept="37vLTw" id="1I84Bf8kAEh" role="1m5AlR">
                          <ref role="3cqZAo" node="1I84Bf8k_OE" resolve="node" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="1I84Bf8k_OE" role="3clF46">
        <property role="TrG5h" value="node" />
        <node concept="3Tqbb2" id="1I84Bf8k_OF" role="1tU5fm" />
      </node>
      <node concept="3cqZAl" id="1I84Bf8k_OG" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="1I84Bf8kMRF">
    <property role="3GE5qa" value="aspect.references" />
    <ref role="13h7C2" to="ok0m:6khVixyapzC" resolve="AspectPathogenReference" />
    <node concept="13hLZK" id="1I84Bf8kMRG" role="13h7CW">
      <node concept="3clFbS" id="1I84Bf8kMRH" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="1I84Bf8kMRQ" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="replaceWith" />
      <ref role="13i0hy" to="wb6c:1I84Bf8ilTw" resolve="replaceWith" />
      <node concept="3Tm1VV" id="1I84Bf8kMRR" role="1B3o_S" />
      <node concept="3clFbS" id="1I84Bf8kMS1" role="3clF47">
        <node concept="3clFbF" id="1I84Bf8kMXv" role="3cqZAp">
          <node concept="2OqwBi" id="1I84Bf8kNa1" role="3clFbG">
            <node concept="13iPFW" id="1I84Bf8kMXu" role="2Oq$k0" />
            <node concept="1P9Npp" id="1I84Bf8kNt_" role="2OqNvi">
              <node concept="2pJPEk" id="1I84Bf8kNvN" role="1P9ThW">
                <node concept="2pJPED" id="1I84Bf8kNBM" role="2pJPEn">
                  <ref role="2pJxaS" to="ok0m:6khVixyapz_" resolve="EntityPathogenReference" />
                  <node concept="2pIpSj" id="1I84Bf8kNCO" role="2pJxcM">
                    <ref role="2pIpSl" to="ok0m:6khVixyapzA" resolve="target" />
                    <node concept="36biLy" id="1I84Bf8kNGA" role="2pJxcZ">
                      <node concept="1PxgMI" id="1I84Bf8kNRH" role="36biLW">
                        <node concept="chp4Y" id="1I84Bf8kNSg" role="3oSUPX">
                          <ref role="cht4Q" to="qzk2:6khVixy0xXs" resolve="Pathogen" />
                        </node>
                        <node concept="37vLTw" id="1I84Bf8kNGT" role="1m5AlR">
                          <ref role="3cqZAo" node="1I84Bf8kMS2" resolve="node" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="1I84Bf8kMS2" role="3clF46">
        <property role="TrG5h" value="node" />
        <node concept="3Tqbb2" id="1I84Bf8kMS3" role="1tU5fm" />
      </node>
      <node concept="3cqZAl" id="1I84Bf8kMS4" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="6LTgXmNXm9s">
    <property role="3GE5qa" value="aspects.variables" />
    <ref role="13h7C2" to="ok0m:6khVixylRXC" resolve="SecondaryMicrobiologyWorkVariable" />
    <node concept="13hLZK" id="6LTgXmNXm9t" role="13h7CW">
      <node concept="3clFbS" id="6LTgXmNXm9u" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6LTgXmNXmsC" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="6LTgXmNXmsD" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNXmsI" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNXmFq" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNXmFp" role="3clFbG">
            <property role="Xl_RC" value="secondary microbiology work" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6PcFLt2kxW1" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmNXmsO" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="6LTgXmNXmsP" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNXmsU" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNXmsZ" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNXmHv" role="3clFbG">
            <property role="Xl_RC" value="" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6PcFLt2kxWg" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmNXmt0" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getReferenceDescription" />
      <property role="2Ki8OM" value="false" />
      <ref role="13i0hy" to="1yj:4V3GMfXuP$X" resolve="getReferenceDescription" />
      <node concept="3Tm1VV" id="6LTgXmNXmt1" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNXmtc" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNXmth" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNXmHU" role="3clFbG">
            <property role="Xl_RC" value="secondary microbiology work" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6PcFLt2kxWv" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="1I84Bf8lbVJ">
    <property role="3GE5qa" value="aspects.references" />
    <ref role="13h7C2" to="ok0m:6khVixylRzg" resolve="AspectSecondaryMicrobiologyWorkReference" />
    <node concept="13hLZK" id="1I84Bf8lbVK" role="13h7CW">
      <node concept="3clFbS" id="1I84Bf8lbVL" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="1I84Bf8lbVU" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="replaceWith" />
      <ref role="13i0hy" to="wb6c:1I84Bf8ilTw" resolve="replaceWith" />
      <node concept="3Tm1VV" id="1I84Bf8lbVV" role="1B3o_S" />
      <node concept="3clFbS" id="1I84Bf8lbW5" role="3clF47">
        <node concept="3clFbF" id="1I84Bf8lc1F" role="3cqZAp">
          <node concept="2OqwBi" id="1I84Bf8lced" role="3clFbG">
            <node concept="13iPFW" id="1I84Bf8lc1E" role="2Oq$k0" />
            <node concept="1P9Npp" id="1I84Bf8lcV3" role="2OqNvi">
              <node concept="2pJPEk" id="1I84Bf8lcXh" role="1P9ThW">
                <node concept="2pJPED" id="1I84Bf8lcZv" role="2pJPEn">
                  <ref role="2pJxaS" to="ok0m:6khVixylO14" resolve="EntitySecondaryMicrobiologyWorkReference" />
                  <node concept="2pIpSj" id="1I84Bf8ld0x" role="2pJxcM">
                    <ref role="2pIpSl" to="ok0m:6khVixylO16" resolve="target" />
                    <node concept="36biLy" id="1I84Bf8ld4j" role="2pJxcZ">
                      <node concept="1PxgMI" id="1I84Bf8ldfq" role="36biLW">
                        <node concept="chp4Y" id="1I84Bf8ldfX" role="3oSUPX">
                          <ref role="cht4Q" to="qzk2:6khVixylEWp" resolve="SecondaryMicrobiologyWork" />
                        </node>
                        <node concept="37vLTw" id="1I84Bf8ld4A" role="1m5AlR">
                          <ref role="3cqZAo" node="1I84Bf8lbW6" resolve="node" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="1I84Bf8lbW6" role="3clF46">
        <property role="TrG5h" value="node" />
        <node concept="3Tqbb2" id="1I84Bf8lbW7" role="1tU5fm" />
      </node>
      <node concept="3cqZAl" id="1I84Bf8lbW8" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="6LTgXmNX4mn">
    <property role="3GE5qa" value="aspects.patterns.secondaryWork" />
    <ref role="13h7C2" to="ok0m:6LTgXmNX4l3" resolve="WildcardSecondaryMicrobiologyWorkPattern" />
    <node concept="13hLZK" id="6LTgXmNX4mo" role="13h7CW">
      <node concept="3clFbS" id="6LTgXmNX4mp" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6LTgXmNX4my" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="6LTgXmNX4mz" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNX4mC" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNX4At" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNX4As" role="3clFbG">
            <property role="Xl_RC" value="*" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6PcFLt2kAmZ" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmNX4mI" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="6LTgXmNX4mJ" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNX4mO" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNX4mT" role="3cqZAp">
          <node concept="Xl_RD" id="6LTgXmNX4BI" role="3clFbG">
            <property role="Xl_RC" value="any microbiology work" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6PcFLt2kAne" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6LTgXmNX4mU" role="13h7CS">
      <property role="TrG5h" value="hasPattern" />
      <property role="13i0it" value="false" />
      <property role="13i0iv" value="false" />
      <ref role="13i0hy" to="wb6c:6LTgXmNRv0n" resolve="hasPattern" />
      <node concept="3Tm1VV" id="6LTgXmNX4mV" role="1B3o_S" />
      <node concept="3clFbS" id="6LTgXmNX4n0" role="3clF47">
        <node concept="3clFbF" id="6LTgXmNX4n5" role="3cqZAp">
          <node concept="3clFbT" id="6LTgXmNX4HM" role="3clFbG">
            <property role="3clFbU" value="true" />
          </node>
        </node>
      </node>
      <node concept="10P_77" id="6PcFLt2kAnt" role="3clF45" />
    </node>
    <node concept="13i0hz" id="2FjKBCOhm8l" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getMatchEntityConcept" />
      <ref role="13i0hy" to="wb6c:65epL7MnLJr" resolve="getMatchEntityConcept" />
      <node concept="3Tm1VV" id="2FjKBCOhm8m" role="1B3o_S" />
      <node concept="3clFbS" id="2FjKBCOhm8n" role="3clF47">
        <node concept="3clFbF" id="2FjKBCOhm8o" role="3cqZAp">
          <node concept="35c_gC" id="2FjKBCOhm8p" role="3clFbG">
            <ref role="35c_gD" to="qzk2:6khVixylEWp" resolve="SecondaryMicrobiologyWork" />
          </node>
        </node>
      </node>
      <node concept="3bZ5Sz" id="2FjKBCOhmDQ" role="3clF45">
        <ref role="3bZ5Sy" to="kkto:4QUW3edDqMR" resolve="Entity" />
      </node>
    </node>
  </node>
</model>

