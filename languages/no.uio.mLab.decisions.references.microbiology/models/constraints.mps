<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:e1396e31-9c77-4ad1-a040-ffad9996579b(no.uio.mLab.decisions.references.microbiology.constraints)">
  <persistence version="9" />
  <languages>
    <use id="3f4bc5f5-c6c1-4a28-8b10-c83066ffa4a1" name="jetbrains.mps.lang.constraints" version="4" />
    <devkit ref="00000000-0000-4000-0000-5604ebd4f22c(jetbrains.mps.devkit.aspect.constraints)" />
  </languages>
  <imports>
    <import index="ok0m" ref="r:4db7f216-3df2-435e-8be1-a0ab7e4a26b5(no.uio.mLab.decisions.references.microbiology.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="3f4bc5f5-c6c1-4a28-8b10-c83066ffa4a1" name="jetbrains.mps.lang.constraints">
      <concept id="8401916545537438642" name="jetbrains.mps.lang.constraints.structure.InheritedNodeScopeFactory" flags="ng" index="1dDu$B">
        <reference id="8401916545537438643" name="kind" index="1dDu$A" />
      </concept>
      <concept id="1213093968558" name="jetbrains.mps.lang.constraints.structure.ConceptConstraints" flags="ng" index="1M2fIO">
        <reference id="1213093996982" name="concept" index="1M2myG" />
        <child id="1213100494875" name="referent" index="1Mr941" />
      </concept>
      <concept id="1148687176410" name="jetbrains.mps.lang.constraints.structure.NodeReferentConstraint" flags="ng" index="1N5Pfh">
        <reference id="1148687202698" name="applicableLink" index="1N5Vy1" />
        <child id="1148687345559" name="searchScopeFactory" index="1N6uqs" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
    </language>
  </registry>
  <node concept="1M2fIO" id="6khVixyairz">
    <property role="3GE5qa" value="aspect.references" />
    <ref role="1M2myG" to="ok0m:6khVixyai6r" resolve="AspectCultureTestReference" />
    <node concept="1N5Pfh" id="6khVixyair$" role="1Mr941">
      <ref role="1N5Vy1" to="ok0m:6khVixyair8" resolve="target" />
      <node concept="1dDu$B" id="6khVixyairA" role="1N6uqs">
        <ref role="1dDu$A" to="ok0m:6khVixyair6" resolve="CultureTestVariable" />
      </node>
    </node>
  </node>
  <node concept="1M2fIO" id="6khVixyarW9">
    <property role="3GE5qa" value="aspect.references" />
    <ref role="1M2myG" to="ok0m:6khVixyapzC" resolve="AspectPathogenReference" />
    <node concept="1N5Pfh" id="6khVixyarWa" role="1Mr941">
      <ref role="1N5Vy1" to="ok0m:6khVixyapzD" resolve="target" />
      <node concept="1dDu$B" id="6khVixyarWc" role="1N6uqs">
        <ref role="1dDu$A" to="ok0m:6khVixyapzB" resolve="PathogenVariable" />
      </node>
    </node>
  </node>
  <node concept="1M2fIO" id="6khVixyGDT3">
    <property role="3GE5qa" value="aspect.references" />
    <ref role="1M2myG" to="ok0m:6khVixyGCLh" resolve="AspectAntimicrobialReference" />
    <node concept="1N5Pfh" id="6khVixyGDT4" role="1Mr941">
      <ref role="1N5Vy1" to="ok0m:6khVixyGCLi" resolve="target" />
      <node concept="1dDu$B" id="6khVixyGDT6" role="1N6uqs">
        <ref role="1dDu$A" to="ok0m:6khVixyGCLg" resolve="AntimicrobialVariable" />
      </node>
    </node>
  </node>
  <node concept="1M2fIO" id="6khVixylRYw">
    <property role="3GE5qa" value="aspects.references" />
    <ref role="1M2myG" to="ok0m:6khVixylRzg" resolve="AspectSecondaryMicrobiologyWorkReference" />
    <node concept="1N5Pfh" id="6khVixylRYx" role="1Mr941">
      <ref role="1N5Vy1" to="ok0m:6khVixylRY5" resolve="target" />
      <node concept="1dDu$B" id="6khVixylRYy" role="1N6uqs">
        <ref role="1dDu$A" to="ok0m:6khVixylRXC" resolve="SecondaryMicrobiologyWorkVariable" />
      </node>
    </node>
  </node>
</model>

