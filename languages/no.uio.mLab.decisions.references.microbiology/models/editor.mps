<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:5d1c9cea-7b82-4f6f-a2bd-87d8c1ca39c8(no.uio.mLab.decisions.references.microbiology.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="11" />
    <use id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions" version="4" />
    <use id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage" version="6" />
    <use id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel" version="11" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="i7z6" ref="r:515ff38d-92f9-43f3-aa4c-c5bec29efe22(no.uio.mLab.decisions.core.editor)" />
    <import index="1yj" ref="r:0c887d67-81ab-420f-9a35-a9f6bf96c975(no.uio.mLab.shared.behavior)" />
    <import index="ok0m" ref="r:4db7f216-3df2-435e-8be1-a0ab7e4a26b5(no.uio.mLab.decisions.references.microbiology.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="2000375450116454183" name="jetbrains.mps.lang.editor.structure.ISubstituteMenu" flags="ng" index="22mbnS">
        <child id="414384289274416996" name="parts" index="3ft7WO" />
      </concept>
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi" />
      <concept id="6089045305654894366" name="jetbrains.mps.lang.editor.structure.SubstituteMenuReference_Default" flags="ng" index="2kknPJ" />
      <concept id="1177327570013" name="jetbrains.mps.lang.editor.structure.QueryFunction_SubstituteMenu_Substitute" flags="in" index="ucgPf" />
      <concept id="8478191136883534237" name="jetbrains.mps.lang.editor.structure.IExtensibleSubstituteMenuPart" flags="ng" index="upBLQ">
        <child id="8478191136883534238" name="features" index="upBLP" />
      </concept>
      <concept id="1177335944525" name="jetbrains.mps.lang.editor.structure.QueryFunction_SubstituteMenu_SubstituteString" flags="in" index="uGdhv" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="1078938745671" name="jetbrains.mps.lang.editor.structure.EditorComponentDeclaration" flags="ig" index="PKFIW">
        <child id="7033942394258392116" name="overridenEditorComponent" index="1PM95z" />
      </concept>
      <concept id="615427434521884870" name="jetbrains.mps.lang.editor.structure.SubstituteMenuPart_Subconcepts" flags="ng" index="2VfDsV" />
      <concept id="1630016958697718209" name="jetbrains.mps.lang.editor.structure.IMenuReference_Default" flags="ng" index="2Z_bC8">
        <reference id="1630016958698373342" name="concept" index="2ZyFGn" />
      </concept>
      <concept id="1630016958697344083" name="jetbrains.mps.lang.editor.structure.IMenu_Concept" flags="ng" index="2ZABuq">
        <reference id="6591946374543067572" name="conceptDeclaration" index="aqKnT" />
      </concept>
      <concept id="8998492695583125082" name="jetbrains.mps.lang.editor.structure.SubstituteFeature_MatchingText" flags="ng" index="16NfWO">
        <child id="8998492695583129244" name="query" index="16NeZM" />
      </concept>
      <concept id="8998492695583129971" name="jetbrains.mps.lang.editor.structure.SubstituteFeature_DescriptionText" flags="ng" index="16NL0t">
        <child id="8998492695583129972" name="query" index="16NL0q" />
      </concept>
      <concept id="7342352913006985483" name="jetbrains.mps.lang.editor.structure.SubstituteMenuPart_Action" flags="ng" index="3eGOop">
        <child id="8612453216082699922" name="substituteHandler" index="3aKz83" />
      </concept>
      <concept id="3308396621974580100" name="jetbrains.mps.lang.editor.structure.SubstituteMenu_Default" flags="ng" index="3p36aQ" />
      <concept id="730181322658904464" name="jetbrains.mps.lang.editor.structure.SubstituteMenuPart_IncludeMenu" flags="ng" index="1s_PAr">
        <child id="730181322658904467" name="menuReference" index="1s_PAo" />
      </concept>
      <concept id="5425882385312046132" name="jetbrains.mps.lang.editor.structure.QueryFunctionParameter_SubstituteMenu_CurrentTargetNode" flags="nn" index="1yR$tW" />
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <property id="1139852716018" name="noTargetText" index="1$x2rV" />
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389577006" name="jetbrains.mps.lang.editor.structure.CellModel_Constant" flags="sn" stub="3610246225209162225" index="3F0ifn">
        <property id="1073389577007" name="text" index="3F0ifm" />
      </concept>
      <concept id="1073389882823" name="jetbrains.mps.lang.editor.structure.CellModel_RefNode" flags="sg" stub="730538219795960754" index="3F1sOY" />
      <concept id="7033942394256351208" name="jetbrains.mps.lang.editor.structure.EditorComponentDeclarationReference" flags="ng" index="1PE4EZ">
        <reference id="7033942394256351817" name="editorComponent" index="1PE7su" />
      </concept>
      <concept id="4307758654696938365" name="jetbrains.mps.lang.editor.structure.QueryFunction_SubstituteMenu_RefPresentation" flags="ig" index="1WAQ3h" />
      <concept id="4307758654696952957" name="jetbrains.mps.lang.editor.structure.QueryFunctionParameter_SubstituteMenu_ReferencedNode" flags="ng" index="1WAUZh" />
      <concept id="8428109087107030357" name="jetbrains.mps.lang.editor.structure.SubstituteMenuPart_ReferenceScope" flags="ng" index="3XHNnq">
        <reference id="8428109087107339113" name="reference" index="3XGfJA" />
        <child id="4307758654694907855" name="descriptionTextFunction" index="1WZ6hz" />
      </concept>
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
      </concept>
    </language>
    <language id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions">
      <concept id="5979988948250981289" name="jetbrains.mps.lang.actions.structure.SNodeCreatorAndInitializer" flags="nn" index="2fJWfE">
        <child id="3757480014665187678" name="prototype" index="1wAG5O" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="2644386474300074836" name="jetbrains.mps.lang.smodel.structure.ConceptIdRefExpression" flags="nn" index="35c_gC">
        <reference id="2644386474300074837" name="conceptDeclaration" index="35c_gD" />
      </concept>
      <concept id="1180636770613" name="jetbrains.mps.lang.smodel.structure.SNodeCreator" flags="nn" index="3zrR0B">
        <child id="1180636770616" name="createdType" index="3zrR0E" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="3p36aQ" id="6khVixyaiid">
    <property role="3GE5qa" value="shared.references.cultureTest" />
    <ref role="aqKnT" to="ok0m:6khVixyahUl" resolve="CultureTestReference" />
    <node concept="1s_PAr" id="6khVixyaiie" role="3ft7WO">
      <node concept="2kknPJ" id="6khVixyaiig" role="1s_PAo">
        <ref role="2ZyFGn" to="ok0m:6khVixyai6r" resolve="AspectCultureTestReference" />
      </node>
    </node>
    <node concept="1s_PAr" id="6khVixyaiim" role="3ft7WO">
      <node concept="2kknPJ" id="6khVixyaiir" role="1s_PAo">
        <ref role="2ZyFGn" to="ok0m:6khVixyai6q" resolve="EntityCultureTestReference" />
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="6khVixyais1">
    <property role="3GE5qa" value="aspect.references" />
    <ref role="aqKnT" to="ok0m:6khVixyai6r" resolve="AspectCultureTestReference" />
    <node concept="3XHNnq" id="6khVixyais2" role="3ft7WO">
      <ref role="3XGfJA" to="ok0m:6khVixyair8" resolve="target" />
      <node concept="1WAQ3h" id="6khVixyaiDK" role="1WZ6hz">
        <node concept="3clFbS" id="6khVixyaiDL" role="2VODD2">
          <node concept="3clFbF" id="6khVixyaiMm" role="3cqZAp">
            <node concept="2OqwBi" id="6khVixyak0u" role="3clFbG">
              <node concept="1WAUZh" id="6khVixyajJu" role="2Oq$k0" />
              <node concept="2qgKlT" id="6khVixyakqf" role="2OqNvi">
                <ref role="37wK5l" to="1yj:4V3GMfXuP$X" resolve="getReferenceDescription" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="6khVixyalQN">
    <property role="3GE5qa" value="base" />
    <ref role="aqKnT" to="ok0m:6khVixyai6q" resolve="EntityCultureTestReference" />
    <node concept="3XHNnq" id="6khVixyalQO" role="3ft7WO">
      <ref role="3XGfJA" to="ok0m:6khVixyai6s" resolve="target" />
      <node concept="1WAQ3h" id="6khVixyalQQ" role="1WZ6hz">
        <node concept="3clFbS" id="6khVixyalQR" role="2VODD2">
          <node concept="3clFbF" id="6khVixyalZs" role="3cqZAp">
            <node concept="2OqwBi" id="6khVixyamq0" role="3clFbG">
              <node concept="1WAUZh" id="6khVixyalZr" role="2Oq$k0" />
              <node concept="2qgKlT" id="6khVixyamTw" role="2OqNvi">
                <ref role="37wK5l" to="1yj:4V3GMfXuP$X" resolve="getReferenceDescription" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="6khVixyap$4">
    <property role="3GE5qa" value="base" />
    <ref role="aqKnT" to="ok0m:6khVixyapz_" resolve="EntityPathogenReference" />
    <node concept="3XHNnq" id="6khVixyap$5" role="3ft7WO">
      <ref role="3XGfJA" to="ok0m:6khVixyapzA" resolve="target" />
      <node concept="1WAQ3h" id="6khVixyap$7" role="1WZ6hz">
        <node concept="3clFbS" id="6khVixyap$8" role="2VODD2">
          <node concept="3clFbF" id="6khVixyapGH" role="3cqZAp">
            <node concept="2OqwBi" id="6khVixyaq3P" role="3clFbG">
              <node concept="1WAUZh" id="6khVixyapGG" role="2Oq$k0" />
              <node concept="2qgKlT" id="6khVixyaqxC" role="2OqNvi">
                <ref role="37wK5l" to="1yj:4V3GMfXuP$X" resolve="getReferenceDescription" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="6khVixyaqMo">
    <property role="3GE5qa" value="aspect.references" />
    <ref role="aqKnT" to="ok0m:6khVixyapzC" resolve="AspectPathogenReference" />
    <node concept="3XHNnq" id="6khVixyaqMp" role="3ft7WO">
      <ref role="3XGfJA" to="ok0m:6khVixyapzD" resolve="target" />
      <node concept="1WAQ3h" id="6khVixyaqMr" role="1WZ6hz">
        <node concept="3clFbS" id="6khVixyaqMs" role="2VODD2">
          <node concept="3clFbF" id="6khVixyaqV1" role="3cqZAp">
            <node concept="2OqwBi" id="6khVixyargn" role="3clFbG">
              <node concept="1WAUZh" id="6khVixyaqV0" role="2Oq$k0" />
              <node concept="2qgKlT" id="6khVixyarE8" role="2OqNvi">
                <ref role="37wK5l" to="1yj:4V3GMfXuP$X" resolve="getReferenceDescription" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="6khVixyatwm">
    <property role="3GE5qa" value="shared.references.pathogen" />
    <ref role="aqKnT" to="ok0m:6khVixyapzE" resolve="PathogenReference" />
    <node concept="1s_PAr" id="6khVixyatwn" role="3ft7WO">
      <node concept="2kknPJ" id="6khVixyatwp" role="1s_PAo">
        <ref role="2ZyFGn" to="ok0m:6khVixyapzC" resolve="AspectPathogenReference" />
      </node>
    </node>
    <node concept="1s_PAr" id="6khVixyatwv" role="3ft7WO">
      <node concept="2kknPJ" id="6khVixyatw$" role="1s_PAo">
        <ref role="2ZyFGn" to="ok0m:6khVixyapz_" resolve="EntityPathogenReference" />
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="6khVixyGBAO">
    <property role="3GE5qa" value="base" />
    <ref role="aqKnT" to="ok0m:6khVixyGBAp" resolve="EntityAntimicrobialReference" />
    <node concept="3XHNnq" id="6khVixyGBAP" role="3ft7WO">
      <ref role="3XGfJA" to="ok0m:6khVixyGBAq" resolve="target" />
      <node concept="1WAQ3h" id="6khVixyGBAR" role="1WZ6hz">
        <node concept="3clFbS" id="6khVixyGBAS" role="2VODD2">
          <node concept="3clFbF" id="6khVixyGBJt" role="3cqZAp">
            <node concept="2OqwBi" id="6khVixyGC5G" role="3clFbG">
              <node concept="1WAUZh" id="6khVixyGBJs" role="2Oq$k0" />
              <node concept="2qgKlT" id="6khVixyGCxu" role="2OqNvi">
                <ref role="37wK5l" to="1yj:4V3GMfXuP$X" resolve="getReferenceDescription" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="6khVixyGCLG">
    <property role="3GE5qa" value="aspect.references" />
    <ref role="aqKnT" to="ok0m:6khVixyGCLh" resolve="AspectAntimicrobialReference" />
    <node concept="3XHNnq" id="6khVixyGCLH" role="3ft7WO">
      <ref role="3XGfJA" to="ok0m:6khVixyGCLi" resolve="target" />
      <node concept="1WAQ3h" id="6khVixyGCLJ" role="1WZ6hz">
        <node concept="3clFbS" id="6khVixyGCLK" role="2VODD2">
          <node concept="3clFbF" id="6khVixyGCUl" role="3cqZAp">
            <node concept="2OqwBi" id="6khVixyGDfF" role="3clFbG">
              <node concept="1WAUZh" id="6khVixyGCUk" role="2Oq$k0" />
              <node concept="2qgKlT" id="6khVixyGDDs" role="2OqNvi">
                <ref role="37wK5l" to="1yj:4V3GMfXuP$X" resolve="getReferenceDescription" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="6khVixyGGdF">
    <property role="3GE5qa" value="shared.references" />
    <ref role="aqKnT" to="ok0m:6khVixyGBAo" resolve="AntimicrobialReference" />
    <node concept="1s_PAr" id="6khVixyGGdG" role="3ft7WO">
      <node concept="2kknPJ" id="6khVixyGGdI" role="1s_PAo">
        <ref role="2ZyFGn" to="ok0m:6khVixyGCLh" resolve="AspectAntimicrobialReference" />
      </node>
    </node>
    <node concept="1s_PAr" id="6khVixyGGdU" role="3ft7WO">
      <node concept="2kknPJ" id="6khVixyGGdZ" role="1s_PAo">
        <ref role="2ZyFGn" to="ok0m:6khVixyGBAp" resolve="EntityAntimicrobialReference" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="6LTgXmNVxiE">
    <property role="3GE5qa" value="aspect.patterns.antimicrobial" />
    <ref role="1XX52x" to="ok0m:6LTgXmNVtUT" resolve="WilcardAntimicrobialPattern" />
    <node concept="3F0ifn" id="6LTgXmNVxiG" role="2wV5jI">
      <property role="3F0ifm" value="*" />
    </node>
  </node>
  <node concept="3p36aQ" id="6LTgXmNVxj8">
    <property role="3GE5qa" value="aspect.patterns.antimicrobial" />
    <ref role="aqKnT" to="ok0m:6LTgXmNVtUT" resolve="WilcardAntimicrobialPattern" />
    <node concept="3eGOop" id="6LTgXmNVxjd" role="3ft7WO">
      <node concept="ucgPf" id="6LTgXmNVxje" role="3aKz83">
        <node concept="3clFbS" id="6LTgXmNVxjf" role="2VODD2">
          <node concept="3clFbF" id="6LTgXmNVxnU" role="3cqZAp">
            <node concept="2ShNRf" id="6LTgXmNVxnS" role="3clFbG">
              <node concept="2fJWfE" id="6LTgXmNVxQg" role="2ShVmc">
                <node concept="3Tqbb2" id="6LTgXmNVxQi" role="3zrR0E">
                  <ref role="ehGHo" to="ok0m:6LTgXmNVtUT" resolve="WilcardAntimicrobialPattern" />
                </node>
                <node concept="1yR$tW" id="6LTgXmNVxZN" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="6LTgXmNVy4E" role="upBLP">
        <node concept="uGdhv" id="6LTgXmNVy9z" role="16NeZM">
          <node concept="3clFbS" id="6LTgXmNVy9_" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmNVyib" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmNVySw" role="3clFbG">
                <node concept="35c_gC" id="6LTgXmNVyia" role="2Oq$k0">
                  <ref role="35c_gD" to="ok0m:6LTgXmNVtUT" resolve="WilcardAntimicrobialPattern" />
                </node>
                <node concept="2qgKlT" id="6LTgXmNVzmV" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="6LTgXmNVzDF" role="upBLP">
        <node concept="uGdhv" id="6LTgXmNVzIL" role="16NL0q">
          <node concept="3clFbS" id="6LTgXmNVzIN" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmNVzRp" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmNV$wr" role="3clFbG">
                <node concept="35c_gC" id="6LTgXmNVzRo" role="2Oq$k0">
                  <ref role="35c_gD" to="ok0m:6LTgXmNVtUT" resolve="WilcardAntimicrobialPattern" />
                </node>
                <node concept="2qgKlT" id="6LTgXmNV$YQ" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="6LTgXmNVDyC">
    <property role="3GE5qa" value="aspect.variables" />
    <ref role="aqKnT" to="ok0m:6khVixyGCLg" resolve="AntimicrobialVariable" />
    <node concept="3eGOop" id="6LTgXmNVDyD" role="3ft7WO">
      <node concept="ucgPf" id="6LTgXmNVDyE" role="3aKz83">
        <node concept="3clFbS" id="6LTgXmNVDyF" role="2VODD2">
          <node concept="3clFbF" id="6LTgXmNVDBA" role="3cqZAp">
            <node concept="2ShNRf" id="6LTgXmNVDB$" role="3clFbG">
              <node concept="2fJWfE" id="6LTgXmNVDJB" role="2ShVmc">
                <node concept="3Tqbb2" id="6LTgXmNVDJD" role="3zrR0E">
                  <ref role="ehGHo" to="ok0m:6khVixyGCLg" resolve="AntimicrobialVariable" />
                </node>
                <node concept="1yR$tW" id="6LTgXmNVDTE" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="6LTgXmNVDYL" role="upBLP">
        <node concept="uGdhv" id="6LTgXmNVE3U" role="16NeZM">
          <node concept="3clFbS" id="6LTgXmNVE3W" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmNVEcy" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmNVEGx" role="3clFbG">
                <node concept="35c_gC" id="6LTgXmNVEcx" role="2Oq$k0">
                  <ref role="35c_gD" to="ok0m:6khVixyGCLg" resolve="AntimicrobialVariable" />
                </node>
                <node concept="2qgKlT" id="6LTgXmNVFbY" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="6LTgXmNVFv5" role="upBLP">
        <node concept="uGdhv" id="6LTgXmNVF$r" role="16NL0q">
          <node concept="3clFbS" id="6LTgXmNVF$t" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmNVFH3" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmNVGd2" role="3clFbG">
                <node concept="35c_gC" id="6LTgXmNVFH2" role="2Oq$k0">
                  <ref role="35c_gD" to="ok0m:6khVixyGCLg" resolve="AntimicrobialVariable" />
                </node>
                <node concept="2qgKlT" id="6LTgXmNVGGv" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="PKFIW" id="6LTgXmNVJzS">
    <property role="3GE5qa" value="aspect.variables" />
    <property role="TrG5h" value="Pattern_AntimicrobialVariable_EditorComponent" />
    <ref role="1XX52x" to="ok0m:6khVixyGCLg" resolve="AntimicrobialVariable" />
    <node concept="3F1sOY" id="6LTgXmNVJzW" role="2wV5jI">
      <property role="1$x2rV" value="&lt;&lt; ... &gt;&gt;" />
      <ref role="1NtTu8" to="ok0m:6LTgXmNVM7F" resolve="pattern" />
    </node>
    <node concept="1PE4EZ" id="6LTgXmNVJzU" role="1PM95z">
      <ref role="1PE7su" to="i7z6:6LTgXmNpPu9" resolve="Pattern_AspectVariable_EditorComponent" />
    </node>
  </node>
  <node concept="3p36aQ" id="6LTgXmNVP8H">
    <property role="3GE5qa" value="aspect.patterns.antimicrobial" />
    <ref role="aqKnT" to="ok0m:6LTgXmNVtU4" resolve="AntimicrobialPattern" />
    <node concept="2VfDsV" id="6LTgXmNVP8I" role="3ft7WO" />
  </node>
  <node concept="3p36aQ" id="6LTgXmNVRKT">
    <property role="3GE5qa" value="aspect.patterns.cultureTest" />
    <ref role="aqKnT" to="ok0m:6LTgXmNVxig" resolve="CultureTestPattern" />
    <node concept="2VfDsV" id="6LTgXmNVRKU" role="3ft7WO" />
  </node>
  <node concept="24kQdi" id="6LTgXmNVRLo">
    <property role="3GE5qa" value="aspect.patterns.cultureTest" />
    <ref role="1XX52x" to="ok0m:6LTgXmNVRKW" resolve="WildcardCultureTestPattern" />
    <node concept="3F0ifn" id="6LTgXmNVRLq" role="2wV5jI">
      <property role="3F0ifm" value="*" />
    </node>
  </node>
  <node concept="3p36aQ" id="6LTgXmNVYa2">
    <property role="3GE5qa" value="aspect.patterns.cultureTest" />
    <ref role="aqKnT" to="ok0m:6LTgXmNVRKW" resolve="WildcardCultureTestPattern" />
    <node concept="3eGOop" id="6LTgXmNVYa3" role="3ft7WO">
      <node concept="ucgPf" id="6LTgXmNVYa4" role="3aKz83">
        <node concept="3clFbS" id="6LTgXmNVYa5" role="2VODD2">
          <node concept="3clFbF" id="6LTgXmNVYeK" role="3cqZAp">
            <node concept="2ShNRf" id="6LTgXmNVYeI" role="3clFbG">
              <node concept="2fJWfE" id="6LTgXmNVYm$" role="2ShVmc">
                <node concept="3Tqbb2" id="6LTgXmNVYmA" role="3zrR0E">
                  <ref role="ehGHo" to="ok0m:6LTgXmNVRKW" resolve="WildcardCultureTestPattern" />
                </node>
                <node concept="1yR$tW" id="6LTgXmNVYw7" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="6LTgXmNVY$Y" role="upBLP">
        <node concept="uGdhv" id="6LTgXmNVYDR" role="16NeZM">
          <node concept="3clFbS" id="6LTgXmNVYDT" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmNVYMv" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmNVZi9" role="3clFbG">
                <node concept="35c_gC" id="6LTgXmNVYMu" role="2Oq$k0">
                  <ref role="35c_gD" to="ok0m:6LTgXmNVRKW" resolve="WildcardCultureTestPattern" />
                </node>
                <node concept="2qgKlT" id="6LTgXmNVZKB" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="6LTgXmNW0uN" role="upBLP">
        <node concept="uGdhv" id="6LTgXmNW0zT" role="16NL0q">
          <node concept="3clFbS" id="6LTgXmNW0zV" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmNW0Gx" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmNW1cb" role="3clFbG">
                <node concept="35c_gC" id="6LTgXmNW0Gw" role="2Oq$k0">
                  <ref role="35c_gD" to="ok0m:6LTgXmNVRKW" resolve="WildcardCultureTestPattern" />
                </node>
                <node concept="2qgKlT" id="6LTgXmNW1EA" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="6LTgXmNW6EA">
    <property role="3GE5qa" value="aspect.patterns.pathogen" />
    <ref role="aqKnT" to="ok0m:6LTgXmNW6Ec" resolve="WildcardPathogenPattern" />
    <node concept="3eGOop" id="6LTgXmNW6EB" role="3ft7WO">
      <node concept="ucgPf" id="6LTgXmNW6EC" role="3aKz83">
        <node concept="3clFbS" id="6LTgXmNW6ED" role="2VODD2">
          <node concept="3clFbF" id="6LTgXmNW6Jk" role="3cqZAp">
            <node concept="2ShNRf" id="6LTgXmNW6Ji" role="3clFbG">
              <node concept="2fJWfE" id="6LTgXmNW6R8" role="2ShVmc">
                <node concept="3Tqbb2" id="6LTgXmNW6Ra" role="3zrR0E">
                  <ref role="ehGHo" to="ok0m:6LTgXmNW6Ec" resolve="WildcardPathogenPattern" />
                </node>
                <node concept="1yR$tW" id="6LTgXmNW70F" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="6LTgXmNW75y" role="upBLP">
        <node concept="uGdhv" id="6LTgXmNW7ar" role="16NeZM">
          <node concept="3clFbS" id="6LTgXmNW7at" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmNW7j3" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmNW7To" role="3clFbG">
                <node concept="35c_gC" id="6LTgXmNW7j2" role="2Oq$k0">
                  <ref role="35c_gD" to="ok0m:6LTgXmNW6Ec" resolve="WildcardPathogenPattern" />
                </node>
                <node concept="2qgKlT" id="6LTgXmNW8nN" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="6LTgXmNW8Ez" role="upBLP">
        <node concept="uGdhv" id="6LTgXmNW8JD" role="16NL0q">
          <node concept="3clFbS" id="6LTgXmNW8JF" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmNW8Sh" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmNW9nV" role="3clFbG">
                <node concept="35c_gC" id="6LTgXmNW8Sg" role="2Oq$k0">
                  <ref role="35c_gD" to="ok0m:6LTgXmNW6Ec" resolve="WildcardPathogenPattern" />
                </node>
                <node concept="2qgKlT" id="6LTgXmNW9Qp" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="6LTgXmNWe0i">
    <property role="3GE5qa" value="aspect.patterns.pathogen" />
    <ref role="1XX52x" to="ok0m:6LTgXmNW6Ec" resolve="WildcardPathogenPattern" />
    <node concept="3F0ifn" id="6LTgXmNWe0k" role="2wV5jI">
      <property role="3F0ifm" value="*" />
    </node>
  </node>
  <node concept="PKFIW" id="6LTgXmNWli7">
    <property role="3GE5qa" value="aspect.variables" />
    <property role="TrG5h" value="Pattern_CultureTestVariable_EditorComponent" />
    <ref role="1XX52x" to="ok0m:6khVixyair6" resolve="CultureTestVariable" />
    <node concept="3F1sOY" id="6LTgXmNWlib" role="2wV5jI">
      <property role="1$x2rV" value="&lt;&lt; ... &gt;&gt;" />
      <ref role="1NtTu8" to="ok0m:6LTgXmNWlhE" resolve="pattern" />
    </node>
    <node concept="1PE4EZ" id="6LTgXmNWli9" role="1PM95z">
      <ref role="1PE7su" to="i7z6:6LTgXmNpPu9" resolve="Pattern_AspectVariable_EditorComponent" />
    </node>
  </node>
  <node concept="PKFIW" id="6LTgXmNWpbD">
    <property role="3GE5qa" value="aspect.variables" />
    <property role="TrG5h" value="Pattern_PathogenVariable_EditorComponent" />
    <ref role="1XX52x" to="ok0m:6khVixyapzB" resolve="PathogenVariable" />
    <node concept="3F1sOY" id="6LTgXmNWpbF" role="2wV5jI">
      <property role="1$x2rV" value="&lt;&lt; ... &gt;&gt;" />
      <ref role="1NtTu8" to="ok0m:6LTgXmNWpaZ" resolve="pattern" />
    </node>
    <node concept="1PE4EZ" id="6LTgXmNWDjm" role="1PM95z">
      <ref role="1PE7su" to="i7z6:6LTgXmNpPu9" resolve="Pattern_AspectVariable_EditorComponent" />
    </node>
  </node>
  <node concept="3p36aQ" id="6LTgXmNWtc$">
    <property role="3GE5qa" value="aspect.variables" />
    <ref role="aqKnT" to="ok0m:6khVixyapzB" resolve="PathogenVariable" />
    <node concept="3eGOop" id="6LTgXmNWtc_" role="3ft7WO">
      <node concept="ucgPf" id="6LTgXmNWtcA" role="3aKz83">
        <node concept="3clFbS" id="6LTgXmNWtcB" role="2VODD2">
          <node concept="3clFbF" id="6LTgXmNWthy" role="3cqZAp">
            <node concept="2ShNRf" id="6LTgXmNWthw" role="3clFbG">
              <node concept="2fJWfE" id="6LTgXmNWtpz" role="2ShVmc">
                <node concept="3Tqbb2" id="6LTgXmNWtp_" role="3zrR0E">
                  <ref role="ehGHo" to="ok0m:6khVixyapzB" resolve="PathogenVariable" />
                </node>
                <node concept="1yR$tW" id="6LTgXmNWtzA" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="6LTgXmNWtCH" role="upBLP">
        <node concept="uGdhv" id="6LTgXmNWtHQ" role="16NeZM">
          <node concept="3clFbS" id="6LTgXmNWtHS" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmNWtQu" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmNWutg" role="3clFbG">
                <node concept="35c_gC" id="6LTgXmNWtQt" role="2Oq$k0">
                  <ref role="35c_gD" to="ok0m:6khVixyapzB" resolve="PathogenVariable" />
                </node>
                <node concept="2qgKlT" id="6LTgXmNWuWH" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="6LTgXmNWvfO" role="upBLP">
        <node concept="uGdhv" id="6LTgXmNWvla" role="16NL0q">
          <node concept="3clFbS" id="6LTgXmNWvlc" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmNWvtM" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmNWvXL" role="3clFbG">
                <node concept="35c_gC" id="6LTgXmNWvtL" role="2Oq$k0">
                  <ref role="35c_gD" to="ok0m:6khVixyapzB" resolve="PathogenVariable" />
                </node>
                <node concept="2qgKlT" id="6LTgXmNWwte" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="6LTgXmNWwFs">
    <property role="3GE5qa" value="aspect.variables" />
    <ref role="aqKnT" to="ok0m:6khVixyair6" resolve="CultureTestVariable" />
    <node concept="3eGOop" id="6LTgXmNWwFt" role="3ft7WO">
      <node concept="ucgPf" id="6LTgXmNWwFu" role="3aKz83">
        <node concept="3clFbS" id="6LTgXmNWwFv" role="2VODD2">
          <node concept="3clFbF" id="6LTgXmNWwKq" role="3cqZAp">
            <node concept="2ShNRf" id="6LTgXmNWwKo" role="3clFbG">
              <node concept="2fJWfE" id="6LTgXmNWwSr" role="2ShVmc">
                <node concept="3Tqbb2" id="6LTgXmNWwSt" role="3zrR0E">
                  <ref role="ehGHo" to="ok0m:6khVixyair6" resolve="CultureTestVariable" />
                </node>
                <node concept="1yR$tW" id="6LTgXmNWx2u" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="6LTgXmNWx7_" role="upBLP">
        <node concept="uGdhv" id="6LTgXmNWxcI" role="16NeZM">
          <node concept="3clFbS" id="6LTgXmNWxcK" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmNWxlm" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmNWxW8" role="3clFbG">
                <node concept="35c_gC" id="6LTgXmNWxll" role="2Oq$k0">
                  <ref role="35c_gD" to="ok0m:6khVixyair6" resolve="CultureTestVariable" />
                </node>
                <node concept="2qgKlT" id="6LTgXmNWyr_" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="6LTgXmNWyIG" role="upBLP">
        <node concept="uGdhv" id="6LTgXmNWyO2" role="16NL0q">
          <node concept="3clFbS" id="6LTgXmNWyO4" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmNWzdK" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmNWzKs" role="3clFbG">
                <node concept="35c_gC" id="6LTgXmNWzdJ" role="2Oq$k0">
                  <ref role="35c_gD" to="ok0m:6khVixyair6" resolve="CultureTestVariable" />
                </node>
                <node concept="2qgKlT" id="6LTgXmNW$fW" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="6LTgXmNWHt1">
    <property role="3GE5qa" value="aspect.patterns.pathogen" />
    <ref role="aqKnT" to="ok0m:6LTgXmNW6Eb" resolve="PathogenPattern" />
    <node concept="2VfDsV" id="6LTgXmNWHt2" role="3ft7WO" />
  </node>
  <node concept="3p36aQ" id="6khVixylWcl">
    <property role="3GE5qa" value="shared" />
    <ref role="aqKnT" to="ok0m:6khVixylO15" resolve="SecondaryMicrobiologyWorkReference" />
    <node concept="2VfDsV" id="6PcFLt2kAGX" role="3ft7WO" />
  </node>
  <node concept="3p36aQ" id="6khVixylO1G">
    <property role="3GE5qa" value="base.references" />
    <ref role="aqKnT" to="ok0m:6khVixylO14" resolve="EntitySecondaryMicrobiologyWorkReference" />
    <node concept="3XHNnq" id="6khVixylO1H" role="3ft7WO">
      <ref role="3XGfJA" to="ok0m:6khVixylO16" resolve="target" />
      <node concept="1WAQ3h" id="6khVixylO1I" role="1WZ6hz">
        <node concept="3clFbS" id="6khVixylO1J" role="2VODD2">
          <node concept="3clFbF" id="6khVixylOrs" role="3cqZAp">
            <node concept="2OqwBi" id="6khVixylQPc" role="3clFbG">
              <node concept="1WAUZh" id="6khVixylQyr" role="2Oq$k0" />
              <node concept="2qgKlT" id="6PcFLt2kBwJ" role="2OqNvi">
                <ref role="37wK5l" to="1yj:4V3GMfXuP$X" resolve="getReferenceDescription" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="6LTgXmNXf83">
    <property role="3GE5qa" value="aspects.variables" />
    <ref role="aqKnT" to="ok0m:6khVixylRXC" resolve="SecondaryMicrobiologyWorkVariable" />
    <node concept="3eGOop" id="6LTgXmNXf84" role="3ft7WO">
      <node concept="ucgPf" id="6LTgXmNXf85" role="3aKz83">
        <node concept="3clFbS" id="6LTgXmNXf86" role="2VODD2">
          <node concept="3clFbF" id="6LTgXmNXfd1" role="3cqZAp">
            <node concept="2ShNRf" id="6LTgXmNXfcZ" role="3clFbG">
              <node concept="2fJWfE" id="6LTgXmNXfl5" role="2ShVmc">
                <node concept="3Tqbb2" id="6LTgXmNXfl7" role="3zrR0E">
                  <ref role="ehGHo" to="ok0m:6khVixylRXC" resolve="SecondaryMicrobiologyWorkVariable" />
                </node>
                <node concept="1yR$tW" id="6LTgXmNXfv8" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="6LTgXmNXf$f" role="upBLP">
        <node concept="uGdhv" id="6LTgXmNXfDo" role="16NeZM">
          <node concept="3clFbS" id="6LTgXmNXfDq" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmNXfM0" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmNXgrv" role="3clFbG">
                <node concept="35c_gC" id="6LTgXmNXfLZ" role="2Oq$k0">
                  <ref role="35c_gD" to="ok0m:6khVixylRXC" resolve="SecondaryMicrobiologyWorkVariable" />
                </node>
                <node concept="2qgKlT" id="6PcFLt2kDYW" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="6LTgXmNXhe3" role="upBLP">
        <node concept="uGdhv" id="6LTgXmNXhjp" role="16NL0q">
          <node concept="3clFbS" id="6LTgXmNXhjr" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmNXhs1" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmNXhW0" role="3clFbG">
                <node concept="35c_gC" id="6LTgXmNXhs0" role="2Oq$k0">
                  <ref role="35c_gD" to="ok0m:6khVixylRXC" resolve="SecondaryMicrobiologyWorkVariable" />
                </node>
                <node concept="2qgKlT" id="6PcFLt2kEwT" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="PKFIW" id="6LTgXmNXnXt">
    <property role="3GE5qa" value="aspects.variables" />
    <property role="TrG5h" value="Pattern_SecondaryMicrobiologyWorkVariable_EditorComponent" />
    <ref role="1XX52x" to="ok0m:6khVixylRXC" resolve="SecondaryMicrobiologyWorkVariable" />
    <node concept="3F1sOY" id="6LTgXmNXoh5" role="2wV5jI">
      <property role="1$x2rV" value="&lt;&lt; ... &gt;&gt;" />
      <ref role="1NtTu8" to="ok0m:6LTgXmNXogZ" resolve="pattern" />
    </node>
    <node concept="1PE4EZ" id="6LTgXmNXoh3" role="1PM95z">
      <ref role="1PE7su" to="i7z6:6LTgXmNpPu9" resolve="Pattern_AspectVariable_EditorComponent" />
    </node>
  </node>
  <node concept="3p36aQ" id="6khVixylRzE">
    <property role="3GE5qa" value="aspects.references" />
    <ref role="aqKnT" to="ok0m:6khVixylRzg" resolve="AspectSecondaryMicrobiologyWorkReference" />
    <node concept="3XHNnq" id="6khVixylRzF" role="3ft7WO">
      <ref role="3XGfJA" to="ok0m:6khVixylRY5" resolve="target" />
      <node concept="1WAQ3h" id="6khVixylRzG" role="1WZ6hz">
        <node concept="3clFbS" id="6khVixylRzH" role="2VODD2">
          <node concept="3clFbF" id="6khVixylRzI" role="3cqZAp">
            <node concept="2OqwBi" id="6khVixylRzJ" role="3clFbG">
              <node concept="1WAUZh" id="6khVixylT8U" role="2Oq$k0" />
              <node concept="2qgKlT" id="6PcFLt2kCtW" role="2OqNvi">
                <ref role="37wK5l" to="1yj:4V3GMfXuP$X" resolve="getReferenceDescription" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="6LTgXmNX4lt">
    <property role="3GE5qa" value="aspects.patterns.secondaryWork" />
    <ref role="aqKnT" to="ok0m:6LTgXmNX4l2" resolve="SecondaryMicrobiologyWorkPattern" />
    <node concept="2VfDsV" id="6LTgXmNX4lu" role="3ft7WO" />
  </node>
  <node concept="24kQdi" id="6LTgXmNX4lT">
    <property role="3GE5qa" value="aspects.patterns.secondaryWork" />
    <ref role="1XX52x" to="ok0m:6LTgXmNX4l3" resolve="WildcardSecondaryMicrobiologyWorkPattern" />
    <node concept="3F0ifn" id="6LTgXmNX4lV" role="2wV5jI">
      <property role="3F0ifm" value="*" />
    </node>
  </node>
  <node concept="3p36aQ" id="6LTgXmNX6Eo">
    <property role="3GE5qa" value="aspects.patterns.secondaryWork" />
    <ref role="aqKnT" to="ok0m:6LTgXmNX4l3" resolve="WildcardSecondaryMicrobiologyWorkPattern" />
    <node concept="3eGOop" id="6LTgXmNX6Ep" role="3ft7WO">
      <node concept="ucgPf" id="6LTgXmNX6Eq" role="3aKz83">
        <node concept="3clFbS" id="6LTgXmNX6Er" role="2VODD2">
          <node concept="3clFbF" id="6LTgXmNX6J6" role="3cqZAp">
            <node concept="2ShNRf" id="6LTgXmNX6J4" role="3clFbG">
              <node concept="2fJWfE" id="6LTgXmNX6QR" role="2ShVmc">
                <node concept="3Tqbb2" id="6LTgXmNX6QT" role="3zrR0E">
                  <ref role="ehGHo" to="ok0m:6LTgXmNX4l3" resolve="WildcardSecondaryMicrobiologyWorkPattern" />
                </node>
                <node concept="1yR$tW" id="6LTgXmNX72_" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="6LTgXmNX77s" role="upBLP">
        <node concept="uGdhv" id="6LTgXmNX7cl" role="16NeZM">
          <node concept="3clFbS" id="6LTgXmNX7cn" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmNX7kX" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmNX7Vi" role="3clFbG">
                <node concept="35c_gC" id="6LTgXmNX7kW" role="2Oq$k0">
                  <ref role="35c_gD" to="ok0m:6LTgXmNX4l3" resolve="WildcardSecondaryMicrobiologyWorkPattern" />
                </node>
                <node concept="2qgKlT" id="6PcFLt2kGhR" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="6LTgXmNX8Gw" role="upBLP">
        <node concept="uGdhv" id="6LTgXmNX8LA" role="16NL0q">
          <node concept="3clFbS" id="6LTgXmNX8LC" role="2VODD2">
            <node concept="3clFbF" id="6LTgXmNX8Ue" role="3cqZAp">
              <node concept="2OqwBi" id="6LTgXmNX9wz" role="3clFbG">
                <node concept="35c_gC" id="6LTgXmNX8Ud" role="2Oq$k0">
                  <ref role="35c_gD" to="ok0m:6LTgXmNX4l3" resolve="WildcardSecondaryMicrobiologyWorkPattern" />
                </node>
                <node concept="2qgKlT" id="6PcFLt2kGML" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

