<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:4db7f216-3df2-435e-8be1-a0ab7e4a26b5(no.uio.mLab.decisions.references.microbiology.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="6" />
    <devkit ref="78434eb8-b0e5-444b-850d-e7c4ad2da9ab(jetbrains.mps.devkit.aspect.structure)" />
  </languages>
  <imports>
    <import index="ruww" ref="r:d7787915-5b9a-4713-a6ab-6345c5e1ec03(no.uio.mLab.decisions.references.laboratoryTest.structure)" />
    <import index="qzk2" ref="r:111a6c30-c76c-4d86-a426-b4a1815fe195(no.uio.mLab.entities.microbiology.structure)" />
    <import index="7f9y" ref="r:790ac37e-42d3-4dd3-977c-e8fe9cc56a45(no.uio.mLab.decisions.core.structure)" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599698500" name="specializedLink" index="20ksaX" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="6khVixyahUl">
    <property role="EcuMT" value="7282862830136139413" />
    <property role="TrG5h" value="CultureTestReference" />
    <property role="3GE5qa" value="shared.references.cultureTest" />
    <ref role="1TJDcQ" to="ruww:4QUW3efwB30" resolve="LaboratoryTestReference" />
  </node>
  <node concept="1TIwiD" id="6khVixyai6q">
    <property role="EcuMT" value="7282862830136140186" />
    <property role="TrG5h" value="EntityCultureTestReference" />
    <property role="3GE5qa" value="base" />
    <ref role="1TJDcQ" node="6khVixyahUl" resolve="CultureTestReference" />
    <node concept="1TJgyj" id="6khVixyai6s" role="1TKVEi">
      <property role="IQ2ns" value="7282862830136140188" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="target" />
      <ref role="20lvS9" to="qzk2:6khVixyahUI" resolve="CultureTest" />
      <ref role="20ksaX" to="7f9y:4QUW3efv2jv" resolve="target" />
    </node>
    <node concept="PrWs8" id="6LTgXmMD2ek" role="PzmwI">
      <ref role="PrY4T" to="7f9y:6LTgXmMvpq9" resolve="IEntityReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="6khVixyai6r">
    <property role="EcuMT" value="7282862830136140187" />
    <property role="TrG5h" value="AspectCultureTestReference" />
    <property role="3GE5qa" value="aspect.references" />
    <ref role="1TJDcQ" node="6khVixyahUl" resolve="CultureTestReference" />
    <node concept="1TJgyj" id="6khVixyair8" role="1TKVEi">
      <property role="IQ2ns" value="7282862830136141512" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="target" />
      <ref role="20lvS9" node="6khVixyair6" resolve="CultureTestVariable" />
      <ref role="20ksaX" to="7f9y:6LTgXmMAS1z" resolve="target" />
    </node>
    <node concept="PrWs8" id="6LTgXmMD2et" role="PzmwI">
      <ref role="PrY4T" to="7f9y:6LTgXmMAn$t" resolve="IAspectVariableReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="6khVixyair6">
    <property role="EcuMT" value="7282862830136141510" />
    <property role="3GE5qa" value="aspect.variables" />
    <property role="TrG5h" value="CultureTestVariable" />
    <ref role="1TJDcQ" to="7f9y:4QUW3edDXqS" resolve="AspectVariable" />
    <node concept="1TJgyj" id="6LTgXmNWlhE" role="1TKVEi">
      <property role="IQ2ns" value="7816353213404238954" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="pattern" />
      <ref role="20lvS9" node="6LTgXmNVxig" resolve="CultureTestPattern" />
      <ref role="20ksaX" to="7f9y:6LTgXmNnjBG" resolve="pattern" />
    </node>
  </node>
  <node concept="1TIwiD" id="6khVixyapz_">
    <property role="EcuMT" value="7282862830136170725" />
    <property role="TrG5h" value="EntityPathogenReference" />
    <property role="3GE5qa" value="base" />
    <ref role="1TJDcQ" node="6khVixyapzE" resolve="PathogenReference" />
    <node concept="1TJgyj" id="6khVixyapzA" role="1TKVEi">
      <property role="IQ2ns" value="7282862830136170726" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="target" />
      <ref role="20ksaX" to="7f9y:4QUW3efv2jv" resolve="target" />
      <ref role="20lvS9" to="qzk2:6khVixy0xXs" resolve="Pathogen" />
    </node>
    <node concept="PrWs8" id="6LTgXmMD2en" role="PzmwI">
      <ref role="PrY4T" to="7f9y:6LTgXmMvpq9" resolve="IEntityReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="6khVixyapzB">
    <property role="EcuMT" value="7282862830136170727" />
    <property role="3GE5qa" value="aspect.variables" />
    <property role="TrG5h" value="PathogenVariable" />
    <ref role="1TJDcQ" to="7f9y:4QUW3edDXqS" resolve="AspectVariable" />
    <node concept="1TJgyj" id="6LTgXmNWpaZ" role="1TKVEi">
      <property role="IQ2ns" value="7816353213404254911" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="pattern" />
      <ref role="20lvS9" node="6LTgXmNW6Eb" resolve="PathogenPattern" />
      <ref role="20ksaX" to="7f9y:6LTgXmNnjBG" resolve="pattern" />
    </node>
  </node>
  <node concept="1TIwiD" id="6khVixyapzC">
    <property role="EcuMT" value="7282862830136170728" />
    <property role="TrG5h" value="AspectPathogenReference" />
    <property role="3GE5qa" value="aspect.references" />
    <ref role="1TJDcQ" node="6khVixyapzE" resolve="PathogenReference" />
    <node concept="1TJgyj" id="6khVixyapzD" role="1TKVEi">
      <property role="IQ2ns" value="7282862830136170729" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="target" />
      <ref role="20lvS9" node="6khVixyapzB" resolve="PathogenVariable" />
      <ref role="20ksaX" to="7f9y:6LTgXmMAS1z" resolve="target" />
    </node>
    <node concept="PrWs8" id="6LTgXmMD2ew" role="PzmwI">
      <ref role="PrY4T" to="7f9y:6LTgXmMAn$t" resolve="IAspectVariableReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="6khVixyapzE">
    <property role="EcuMT" value="7282862830136170730" />
    <property role="TrG5h" value="PathogenReference" />
    <property role="3GE5qa" value="shared.references.pathogen" />
    <ref role="1TJDcQ" to="7f9y:4QUW3efv2iG" resolve="Reference" />
  </node>
  <node concept="1TIwiD" id="6khVixyGBAo">
    <property role="EcuMT" value="7282862830145141144" />
    <property role="3GE5qa" value="shared.references" />
    <property role="TrG5h" value="AntimicrobialReference" />
    <ref role="1TJDcQ" to="7f9y:4QUW3efv2iG" resolve="Reference" />
  </node>
  <node concept="1TIwiD" id="6khVixyGBAp">
    <property role="EcuMT" value="7282862830145141145" />
    <property role="TrG5h" value="EntityAntimicrobialReference" />
    <property role="3GE5qa" value="base" />
    <ref role="1TJDcQ" node="6khVixyGBAo" resolve="AntimicrobialReference" />
    <node concept="1TJgyj" id="6khVixyGBAq" role="1TKVEi">
      <property role="IQ2ns" value="7282862830145141146" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="target" />
      <ref role="20ksaX" to="7f9y:4QUW3efv2jv" resolve="target" />
      <ref role="20lvS9" to="qzk2:6khVixyGBqF" resolve="Antimicrobial" />
    </node>
    <node concept="PrWs8" id="6LTgXmMD2eh" role="PzmwI">
      <ref role="PrY4T" to="7f9y:6LTgXmMvpq9" resolve="IEntityReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="6khVixyGCLg">
    <property role="EcuMT" value="7282862830145145936" />
    <property role="3GE5qa" value="aspect.variables" />
    <property role="TrG5h" value="AntimicrobialVariable" />
    <ref role="1TJDcQ" to="7f9y:4QUW3edDXqS" resolve="AspectVariable" />
    <node concept="1TJgyj" id="6LTgXmNVM7F" role="1TKVEi">
      <property role="IQ2ns" value="7816353213404094955" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="pattern" />
      <ref role="20lvS9" node="6LTgXmNVtU4" resolve="AntimicrobialPattern" />
      <ref role="20ksaX" to="7f9y:6LTgXmNnjBG" resolve="pattern" />
    </node>
  </node>
  <node concept="1TIwiD" id="6khVixyGCLh">
    <property role="EcuMT" value="7282862830145145937" />
    <property role="TrG5h" value="AspectAntimicrobialReference" />
    <property role="3GE5qa" value="aspect.references" />
    <ref role="1TJDcQ" node="6khVixyGBAo" resolve="AntimicrobialReference" />
    <node concept="1TJgyj" id="6khVixyGCLi" role="1TKVEi">
      <property role="IQ2ns" value="7282862830145145938" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="target" />
      <ref role="20lvS9" node="6khVixyGCLg" resolve="AntimicrobialVariable" />
      <ref role="20ksaX" to="7f9y:6LTgXmMAS1z" resolve="target" />
    </node>
    <node concept="PrWs8" id="6LTgXmMD2eq" role="PzmwI">
      <ref role="PrY4T" to="7f9y:6LTgXmMAn$t" resolve="IAspectVariableReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="6LTgXmNVtU4">
    <property role="EcuMT" value="7816353213404012164" />
    <property role="3GE5qa" value="aspect.patterns.antimicrobial" />
    <property role="TrG5h" value="AntimicrobialPattern" />
    <ref role="1TJDcQ" to="7f9y:6LTgXmNrfh_" resolve="AspectVariablePattern" />
  </node>
  <node concept="1TIwiD" id="6LTgXmNVtUT">
    <property role="EcuMT" value="7816353213404012217" />
    <property role="3GE5qa" value="aspect.patterns.antimicrobial" />
    <property role="TrG5h" value="WilcardAntimicrobialPattern" />
    <ref role="1TJDcQ" node="6LTgXmNVtU4" resolve="AntimicrobialPattern" />
    <node concept="PrWs8" id="2FjKBCOhjOv" role="PzmwI">
      <ref role="PrY4T" to="7f9y:65epL7Ml5Dl" resolve="IWildcardPattern" />
    </node>
  </node>
  <node concept="1TIwiD" id="6LTgXmNVxig">
    <property role="EcuMT" value="7816353213404026000" />
    <property role="3GE5qa" value="aspect.patterns.cultureTest" />
    <property role="TrG5h" value="CultureTestPattern" />
    <ref role="1TJDcQ" to="7f9y:6LTgXmNrfh_" resolve="AspectVariablePattern" />
  </node>
  <node concept="1TIwiD" id="6LTgXmNVRKW">
    <property role="EcuMT" value="7816353213404118076" />
    <property role="3GE5qa" value="aspect.patterns.cultureTest" />
    <property role="TrG5h" value="WildcardCultureTestPattern" />
    <ref role="1TJDcQ" node="6LTgXmNVxig" resolve="CultureTestPattern" />
    <node concept="PrWs8" id="2FjKBCOhk_F" role="PzmwI">
      <ref role="PrY4T" to="7f9y:65epL7Ml5Dl" resolve="IWildcardPattern" />
    </node>
  </node>
  <node concept="1TIwiD" id="6LTgXmNW6Eb">
    <property role="EcuMT" value="7816353213404179083" />
    <property role="3GE5qa" value="aspect.patterns.pathogen" />
    <property role="TrG5h" value="PathogenPattern" />
    <ref role="1TJDcQ" to="7f9y:6LTgXmNrfh_" resolve="AspectVariablePattern" />
  </node>
  <node concept="1TIwiD" id="6LTgXmNW6Ec">
    <property role="EcuMT" value="7816353213404179084" />
    <property role="3GE5qa" value="aspect.patterns.pathogen" />
    <property role="TrG5h" value="WildcardPathogenPattern" />
    <ref role="1TJDcQ" node="6LTgXmNW6Eb" resolve="PathogenPattern" />
    <node concept="PrWs8" id="2FjKBCOhlUR" role="PzmwI">
      <ref role="PrY4T" to="7f9y:65epL7Ml5Dl" resolve="IWildcardPattern" />
    </node>
  </node>
  <node concept="1TIwiD" id="6khVixylO15">
    <property role="EcuMT" value="7282862830139162693" />
    <property role="TrG5h" value="SecondaryMicrobiologyWorkReference" />
    <property role="3GE5qa" value="shared" />
    <ref role="1TJDcQ" to="7f9y:4QUW3efv2iG" resolve="Reference" />
  </node>
  <node concept="1TIwiD" id="6khVixylO14">
    <property role="EcuMT" value="7282862830139162692" />
    <property role="3GE5qa" value="base.references" />
    <property role="TrG5h" value="EntitySecondaryMicrobiologyWorkReference" />
    <ref role="1TJDcQ" node="6khVixylO15" resolve="SecondaryMicrobiologyWorkReference" />
    <node concept="1TJgyj" id="6khVixylO16" role="1TKVEi">
      <property role="IQ2ns" value="7282862830139162694" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="target" />
      <ref role="20ksaX" to="7f9y:4QUW3efv2jv" resolve="target" />
      <ref role="20lvS9" to="qzk2:6khVixylEWp" resolve="SecondaryMicrobiologyWork" />
    </node>
    <node concept="PrWs8" id="6LTgXmMDI4W" role="PzmwI">
      <ref role="PrY4T" to="7f9y:6LTgXmMvpq9" resolve="IEntityReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="6khVixylRXC">
    <property role="EcuMT" value="7282862830139178856" />
    <property role="3GE5qa" value="aspects.variables" />
    <property role="TrG5h" value="SecondaryMicrobiologyWorkVariable" />
    <ref role="1TJDcQ" to="7f9y:4QUW3edDXqS" resolve="AspectVariable" />
    <node concept="1TJgyj" id="6LTgXmNXogZ" role="1TKVEi">
      <property role="IQ2ns" value="7816353213404513343" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="pattern" />
      <ref role="20lvS9" node="6LTgXmNX4l2" resolve="SecondaryMicrobiologyWorkPattern" />
      <ref role="20ksaX" to="7f9y:6LTgXmNnjBG" resolve="pattern" />
    </node>
  </node>
  <node concept="1TIwiD" id="6khVixylRzg">
    <property role="EcuMT" value="7282862830139177168" />
    <property role="3GE5qa" value="aspects.references" />
    <property role="TrG5h" value="AspectSecondaryMicrobiologyWorkReference" />
    <ref role="1TJDcQ" node="6khVixylO15" resolve="SecondaryMicrobiologyWorkReference" />
    <node concept="1TJgyj" id="6khVixylRY5" role="1TKVEi">
      <property role="IQ2ns" value="7282862830139178885" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="target" />
      <ref role="20lvS9" node="6khVixylRXC" resolve="SecondaryMicrobiologyWorkVariable" />
      <ref role="20ksaX" to="7f9y:6LTgXmMAS1z" resolve="target" />
    </node>
    <node concept="PrWs8" id="6LTgXmNR6co" role="PzmwI">
      <ref role="PrY4T" to="7f9y:6LTgXmMAn$t" resolve="IAspectVariableReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="6LTgXmNX4l2">
    <property role="EcuMT" value="7816353213404431682" />
    <property role="3GE5qa" value="aspects.patterns.secondaryWork" />
    <property role="TrG5h" value="SecondaryMicrobiologyWorkPattern" />
    <ref role="1TJDcQ" to="7f9y:6LTgXmNrfh_" resolve="AspectVariablePattern" />
  </node>
  <node concept="1TIwiD" id="6LTgXmNX4l3">
    <property role="EcuMT" value="7816353213404431683" />
    <property role="3GE5qa" value="aspects.patterns.secondaryWork" />
    <property role="TrG5h" value="WildcardSecondaryMicrobiologyWorkPattern" />
    <ref role="1TJDcQ" node="6LTgXmNX4l2" resolve="SecondaryMicrobiologyWorkPattern" />
    <node concept="PrWs8" id="2FjKBCOhmuJ" role="PzmwI">
      <ref role="PrY4T" to="7f9y:65epL7Ml5Dl" resolve="IWildcardPattern" />
    </node>
  </node>
</model>

