<?xml version="1.0" encoding="UTF-8"?>
<solution name="no.uio.mLab.decisions.references.microbiology.translations" uuid="6ffb4f00-a991-465b-b999-465bc3332cb3" moduleVersion="0" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
  </models>
  <sourcePath />
  <languageVersions />
  <dependencyVersions>
    <module reference="6ffb4f00-a991-465b-b999-465bc3332cb3(no.uio.mLab.decisions.references.microbiology.translations)" version="0" />
  </dependencyVersions>
</solution>

