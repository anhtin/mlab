<?xml version="1.0" encoding="UTF-8"?>
<solution name="no.uio.mLab.decisions.references.microbiology.runtime" uuid="8dec6cdc-3fcc-4fca-a2be-78326eb2c783" moduleVersion="0" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
  </models>
  <sourcePath />
  <languageVersions />
  <dependencyVersions>
    <module reference="8dec6cdc-3fcc-4fca-a2be-78326eb2c783(no.uio.mLab.decisions.references.microbiology.runtime)" version="0" />
  </dependencyVersions>
</solution>

