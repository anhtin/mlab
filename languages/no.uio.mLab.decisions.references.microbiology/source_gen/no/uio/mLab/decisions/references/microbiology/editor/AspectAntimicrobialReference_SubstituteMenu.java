package no.uio.mLab.decisions.references.microbiology.editor;

/*Generated by MPS */

import jetbrains.mps.nodeEditor.menus.substitute.SubstituteMenuBase;
import org.jetbrains.annotations.NotNull;
import java.util.List;
import jetbrains.mps.lang.editor.menus.MenuPart;
import jetbrains.mps.openapi.editor.menus.substitute.SubstituteMenuItem;
import jetbrains.mps.openapi.editor.menus.substitute.SubstituteMenuContext;
import java.util.ArrayList;
import jetbrains.mps.lang.editor.menus.substitute.ConstraintsFilteringSubstituteMenuPartDecorator;
import jetbrains.mps.smodel.adapter.structure.MetaAdapterFactory;
import jetbrains.mps.lang.editor.menus.EditorMenuDescriptorBase;
import jetbrains.mps.smodel.SNodePointer;
import jetbrains.mps.lang.editor.menus.substitute.ReferenceScopeSubstituteMenuPart;
import jetbrains.mps.lang.editor.menus.substitute.ReferenceScopeSubstituteMenuItem;
import org.jetbrains.mps.openapi.model.SNode;
import jetbrains.mps.openapi.editor.menus.EditorMenuTraceInfo;
import org.jetbrains.mps.openapi.language.SConcept;
import org.jetbrains.mps.openapi.language.SReferenceLink;
import no.uio.mLab.shared.behavior.IReferrableConcept__BehaviorDescriptor;

public class AspectAntimicrobialReference_SubstituteMenu extends SubstituteMenuBase {
  @NotNull
  @Override
  protected List<MenuPart<SubstituteMenuItem, SubstituteMenuContext>> getParts(final SubstituteMenuContext _context) {
    List<MenuPart<SubstituteMenuItem, SubstituteMenuContext>> result = new ArrayList<MenuPart<SubstituteMenuItem, SubstituteMenuContext>>();
    result.add(new ConstraintsFilteringSubstituteMenuPartDecorator(new AspectAntimicrobialReference_SubstituteMenu.SMP_ReferenceScope_8h58sk_a(), MetaAdapterFactory.getConcept(0x64b829cb114b4b36L, 0x813b6c9497a3e006L, 0x6511ed2862b28c51L, "no.uio.mLab.decisions.references.microbiology.structure.AspectAntimicrobialReference")));
    return result;
  }

  @NotNull
  @Override
  public List<SubstituteMenuItem> createMenuItems(@NotNull SubstituteMenuContext context) {
    context.getEditorMenuTrace().pushTraceInfo();
    context.getEditorMenuTrace().setDescriptor(new EditorMenuDescriptorBase("default substitute menu for " + "AspectAntimicrobialReference", new SNodePointer("r:5d1c9cea-7b82-4f6f-a2bd-87d8c1ca39c8(no.uio.mLab.decisions.references.microbiology.editor)", "7282862830145145964")));
    try {
      return super.createMenuItems(context);
    } finally {
      context.getEditorMenuTrace().popTraceInfo();
    }
  }


  public class SMP_ReferenceScope_8h58sk_a extends ReferenceScopeSubstituteMenuPart {

    public SMP_ReferenceScope_8h58sk_a() {
      super(MetaAdapterFactory.getConcept(0x64b829cb114b4b36L, 0x813b6c9497a3e006L, 0x6511ed2862b28c51L, "no.uio.mLab.decisions.references.microbiology.structure.AspectAntimicrobialReference"), MetaAdapterFactory.getReferenceLink(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x6c7943d5b299791dL, 0x6c7943d5b29b8063L, "target"));
    }
    @NotNull
    @Override
    public List<SubstituteMenuItem> createItems(SubstituteMenuContext context) {
      context.getEditorMenuTrace().pushTraceInfo();
      context.getEditorMenuTrace().setDescriptor(new EditorMenuDescriptorBase("reference scope substitute menu part", new SNodePointer("r:5d1c9cea-7b82-4f6f-a2bd-87d8c1ca39c8(no.uio.mLab.decisions.references.microbiology.editor)", "7282862830145145965")));
      try {
        return super.createItems(context);
      } finally {
        context.getEditorMenuTrace().popTraceInfo();
      }
    }

    @Override
    @NotNull
    protected ReferenceScopeSubstituteMenuItem createItem(SubstituteMenuContext context, SNode referencedNode) {
      return new AspectAntimicrobialReference_SubstituteMenu.SMP_ReferenceScope_8h58sk_a.Item(context, referencedNode, getConcept(), getReferenceLink());
    }
    private class Item extends ReferenceScopeSubstituteMenuItem {
      private final SubstituteMenuContext _context;
      private final SNode referencedNode;
      private EditorMenuTraceInfo myTraceInfo;

      private Item(SubstituteMenuContext context, SNode refNode, SConcept concept, SReferenceLink referenceLink) {
        super(concept, context.getParentNode(), context.getCurrentTargetNode(), refNode, referenceLink, context.getEditorContext());
        _context = context;
        referencedNode = refNode;
        myTraceInfo = context.getEditorMenuTrace().getTraceInfo();
      }
      @Override
      public String getDescriptionText(String pattern) {
        return (String) IReferrableConcept__BehaviorDescriptor.getReferenceDescription_id4V3GMfXuP$X.invoke(referencedNode);
      }

      @Override
      public EditorMenuTraceInfo getTraceInfo() {
        return myTraceInfo;
      }
    }
  }
}
