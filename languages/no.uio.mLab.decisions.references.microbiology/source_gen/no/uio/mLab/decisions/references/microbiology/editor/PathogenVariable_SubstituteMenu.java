package no.uio.mLab.decisions.references.microbiology.editor;

/*Generated by MPS */

import jetbrains.mps.nodeEditor.menus.substitute.SubstituteMenuBase;
import org.jetbrains.annotations.NotNull;
import java.util.List;
import jetbrains.mps.lang.editor.menus.MenuPart;
import jetbrains.mps.openapi.editor.menus.substitute.SubstituteMenuItem;
import jetbrains.mps.openapi.editor.menus.substitute.SubstituteMenuContext;
import java.util.ArrayList;
import jetbrains.mps.lang.editor.menus.substitute.ConstraintsFilteringSubstituteMenuPartDecorator;
import jetbrains.mps.smodel.adapter.structure.MetaAdapterFactory;
import jetbrains.mps.lang.editor.menus.EditorMenuDescriptorBase;
import jetbrains.mps.smodel.SNodePointer;
import jetbrains.mps.lang.editor.menus.substitute.SingleItemSubstituteMenuPart;
import org.jetbrains.annotations.Nullable;
import org.apache.log4j.Logger;
import jetbrains.mps.lang.editor.menus.substitute.DefaultSubstituteMenuItem;
import jetbrains.mps.openapi.editor.menus.EditorMenuTraceInfo;
import org.jetbrains.mps.openapi.model.SNode;
import jetbrains.mps.smodel.action.SNodeFactoryOperations;
import no.uio.mLab.shared.behavior.ITranslatableConcept__BehaviorDescriptor;
import jetbrains.mps.lang.smodel.generator.smodelAdapter.SNodeOperations;

public class PathogenVariable_SubstituteMenu extends SubstituteMenuBase {
  @NotNull
  @Override
  protected List<MenuPart<SubstituteMenuItem, SubstituteMenuContext>> getParts(final SubstituteMenuContext _context) {
    List<MenuPart<SubstituteMenuItem, SubstituteMenuContext>> result = new ArrayList<MenuPart<SubstituteMenuItem, SubstituteMenuContext>>();
    result.add(new ConstraintsFilteringSubstituteMenuPartDecorator(new PathogenVariable_SubstituteMenu.SMP_Action_1p6srf_a(), MetaAdapterFactory.getConcept(0x64b829cb114b4b36L, 0x813b6c9497a3e006L, 0x6511ed28622998e7L, "no.uio.mLab.decisions.references.microbiology.structure.PathogenVariable")));
    return result;
  }

  @NotNull
  @Override
  public List<SubstituteMenuItem> createMenuItems(@NotNull SubstituteMenuContext context) {
    context.getEditorMenuTrace().pushTraceInfo();
    context.getEditorMenuTrace().setDescriptor(new EditorMenuDescriptorBase("default substitute menu for " + "PathogenVariable", new SNodePointer("r:5d1c9cea-7b82-4f6f-a2bd-87d8c1ca39c8(no.uio.mLab.decisions.references.microbiology.editor)", "7816353213404271396")));
    try {
      return super.createMenuItems(context);
    } finally {
      context.getEditorMenuTrace().popTraceInfo();
    }
  }


  private class SMP_Action_1p6srf_a extends SingleItemSubstituteMenuPart {

    @Nullable
    @Override
    protected SubstituteMenuItem createItem(SubstituteMenuContext _context) {
      PathogenVariable_SubstituteMenu.SMP_Action_1p6srf_a.Item item = new PathogenVariable_SubstituteMenu.SMP_Action_1p6srf_a.Item(_context);
      String description;
      try {
        description = "Substitute item: " + item.getMatchingText("");
      } catch (Throwable t) {
        Logger.getLogger(getClass()).error("Exception while executing getMatchingText() of the item " + item, t);
        return null;
      }

      _context.getEditorMenuTrace().pushTraceInfo();
      try {
        _context.getEditorMenuTrace().setDescriptor(new EditorMenuDescriptorBase(description, new SNodePointer("r:5d1c9cea-7b82-4f6f-a2bd-87d8c1ca39c8(no.uio.mLab.decisions.references.microbiology.editor)", "7816353213404271397")));
        item.setTraceInfo(_context.getEditorMenuTrace().getTraceInfo());
      } finally {
        _context.getEditorMenuTrace().popTraceInfo();
      }

      return item;
    }
    private class Item extends DefaultSubstituteMenuItem {
      private final SubstituteMenuContext _context;
      private EditorMenuTraceInfo myTraceInfo;
      public Item(SubstituteMenuContext context) {
        super(MetaAdapterFactory.getConcept(0x64b829cb114b4b36L, 0x813b6c9497a3e006L, 0x6511ed28622998e7L, "no.uio.mLab.decisions.references.microbiology.structure.PathogenVariable"), context.getParentNode(), context.getCurrentTargetNode(), context.getEditorContext());
        _context = context;
      }

      private void setTraceInfo(EditorMenuTraceInfo traceInfo) {
        myTraceInfo = traceInfo;
      }

      @Nullable
      @Override
      public SNode createNode(@NotNull String pattern) {
        return SNodeFactoryOperations.createNewNode(SNodeFactoryOperations.asInstanceConcept(MetaAdapterFactory.getConcept(0x64b829cb114b4b36L, 0x813b6c9497a3e006L, 0x6511ed28622998e7L, "no.uio.mLab.decisions.references.microbiology.structure.PathogenVariable")), _context.getCurrentTargetNode());
      }

      @Override
      public EditorMenuTraceInfo getTraceInfo() {
        return myTraceInfo;
      }
      @Nullable
      @Override
      public String getMatchingText(@NotNull String pattern) {
        return (String) ITranslatableConcept__BehaviorDescriptor.getDisplayAlias_id5Wfdz$0vc2$.invoke(SNodeOperations.asSConcept(MetaAdapterFactory.getConcept(0x64b829cb114b4b36L, 0x813b6c9497a3e006L, 0x6511ed28622998e7L, "no.uio.mLab.decisions.references.microbiology.structure.PathogenVariable")));
      }
      @Nullable
      @Override
      public String getDescriptionText(@NotNull String pattern) {
        return (String) ITranslatableConcept__BehaviorDescriptor.getDisplayDescription_id5Wfdz$0vc3v.invoke(SNodeOperations.asSConcept(MetaAdapterFactory.getConcept(0x64b829cb114b4b36L, 0x813b6c9497a3e006L, 0x6511ed28622998e7L, "no.uio.mLab.decisions.references.microbiology.structure.PathogenVariable")));
      }
    }
  }
}
