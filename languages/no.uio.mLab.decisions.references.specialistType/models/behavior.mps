<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:8824e0eb-4fe8-4214-be3e-48459ee5742a(no.uio.mLab.decisions.references.specialistType.behavior)">
  <persistence version="9" />
  <languages>
    <use id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior" version="1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="wb6c" ref="r:19d15037-83c1-4ae2-b66a-763828cb8279(no.uio.mLab.decisions.core.behavior)" />
    <import index="6n4l" ref="r:c9ba589e-03d5-4471-b76f-564a5e0e9230(no.uio.mLab.decisions.references.specialistType.translations)" />
    <import index="hpcx" ref="r:2530bb31-0779-45e3-bc3f-01507d6b2623(no.uio.mLab.decisions.references.specialistType.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior">
      <concept id="6496299201655527393" name="jetbrains.mps.lang.behavior.structure.LocalBehaviorMethodCall" flags="nn" index="BsUDl" />
      <concept id="1225194240794" name="jetbrains.mps.lang.behavior.structure.ConceptBehavior" flags="ng" index="13h7C7">
        <reference id="1225194240799" name="concept" index="13h7C2" />
        <child id="1225194240805" name="method" index="13h7CS" />
        <child id="1225194240801" name="constructor" index="13h7CW" />
      </concept>
      <concept id="1225194413805" name="jetbrains.mps.lang.behavior.structure.ConceptConstructorDeclaration" flags="in" index="13hLZK" />
      <concept id="1225194472830" name="jetbrains.mps.lang.behavior.structure.ConceptMethodDeclaration" flags="ng" index="13i0hz">
        <property id="5864038008284099149" name="isStatic" index="2Ki8OM" />
        <property id="1225194472832" name="isVirtual" index="13i0it" />
        <property id="1225194472834" name="isAbstract" index="13i0iv" />
        <reference id="1225194472831" name="overriddenMethod" index="13i0hy" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
      </concept>
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="13h7C7" id="4QUW3efWYSf">
    <ref role="13h7C2" to="hpcx:4QUW3efwBDd" resolve="EntitySpecialistTypeReference" />
    <node concept="13hLZK" id="4QUW3efWYSg" role="13h7CW">
      <node concept="3clFbS" id="4QUW3efWYSh" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="4QUW3efWYSq" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getMissingTargetError" />
      <ref role="13i0hy" to="wb6c:4QUW3edDA0E" resolve="getMissingTargetError" />
      <node concept="3Tm1VV" id="4QUW3efWYSr" role="1B3o_S" />
      <node concept="3clFbS" id="4QUW3efWYSy" role="3clF47">
        <node concept="3clFbF" id="4QUW3efWYSB" role="3cqZAp">
          <node concept="2OqwBi" id="4QUW3efX061" role="3clFbG">
            <node concept="BsUDl" id="4QUW3efX1M_" role="2Oq$k0">
              <ref role="37wK5l" node="4QUW3efWZl2" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="4QUW3efX0bQ" role="2OqNvi">
              <ref role="37wK5l" to="6n4l:4QUW3efWZ6v" resolve="getSpecialistTypeReferenceMissingSpecialistTypeError" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="4QUW3efWYSz" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="4QUW3efWZkR">
    <ref role="13h7C2" to="hpcx:4QUW3efWZ4C" resolve="ITranslatableSpecialistTypeConcept" />
    <node concept="13i0hz" id="4QUW3efWZl2" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="true" />
      <property role="TrG5h" value="getDisplayTranslations" />
      <property role="2Ki8OM" value="true" />
      <node concept="3Tm1VV" id="4QUW3efWZl3" role="1B3o_S" />
      <node concept="3uibUv" id="4QUW3efWZlB" role="3clF45">
        <ref role="3uigEE" to="6n4l:4zMac8rUNtP" resolve="ISpecialistTypeReferenceTranslations" />
      </node>
      <node concept="3clFbS" id="4QUW3efWZl5" role="3clF47">
        <node concept="3clFbF" id="4QUW3efWZmk" role="3cqZAp">
          <node concept="10M0yZ" id="4QUW3efWZmK" role="3clFbG">
            <ref role="3cqZAo" to="6n4l:4zMac8rVBeK" resolve="displayTranslations" />
            <ref role="1PxDUh" to="6n4l:4zMac8rUNsN" resolve="SpecialistTypeReferenceTranslationProvider" />
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="4QUW3efWZnl" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="true" />
      <property role="TrG5h" value="getGenerationTranslations" />
      <property role="2Ki8OM" value="true" />
      <node concept="3Tm1VV" id="4QUW3efWZnm" role="1B3o_S" />
      <node concept="3uibUv" id="4QUW3efWZnn" role="3clF45">
        <ref role="3uigEE" to="6n4l:4zMac8rUNtP" resolve="ISpecialistTypeReferenceTranslations" />
      </node>
      <node concept="3clFbS" id="4QUW3efWZno" role="3clF47">
        <node concept="3clFbF" id="4QUW3efWZnp" role="3cqZAp">
          <node concept="10M0yZ" id="4QUW3efWZqm" role="3clFbG">
            <ref role="3cqZAo" to="6n4l:1Hxyv4DUmfq" resolve="generationTranslations" />
            <ref role="1PxDUh" to="6n4l:4zMac8rUNsN" resolve="SpecialistTypeReferenceTranslationProvider" />
          </node>
        </node>
      </node>
    </node>
    <node concept="13hLZK" id="4QUW3efWZkS" role="13h7CW">
      <node concept="3clFbS" id="4QUW3efWZkT" role="2VODD2" />
    </node>
  </node>
</model>

