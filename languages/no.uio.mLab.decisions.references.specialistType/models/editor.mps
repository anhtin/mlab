<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:1753506e-4a31-4f44-a9da-d7cdb7129c1f(no.uio.mLab.decisions.references.specialistType.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="11" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="uubs" ref="r:481ae0fe-9100-4bb8-9488-731d6cfeba53(no.uio.mLab.shared.editor)" />
    <import index="hpcx" ref="r:2530bb31-0779-45e3-bc3f-01507d6b2623(no.uio.mLab.decisions.references.specialistType.structure)" implicit="true" />
    <import index="kkto" ref="r:01f3534b-874e-42dc-a9b5-7540591f45bb(no.uio.mLab.entities.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="2000375450116454183" name="jetbrains.mps.lang.editor.structure.ISubstituteMenu" flags="ng" index="22mbnS">
        <child id="414384289274416996" name="parts" index="3ft7WO" />
      </concept>
      <concept id="1630016958697344083" name="jetbrains.mps.lang.editor.structure.IMenu_Concept" flags="ng" index="2ZABuq">
        <reference id="6591946374543067572" name="conceptDeclaration" index="aqKnT" />
      </concept>
      <concept id="3308396621974580100" name="jetbrains.mps.lang.editor.structure.SubstituteMenu_Default" flags="ng" index="3p36aQ" />
      <concept id="7991336459489871999" name="jetbrains.mps.lang.editor.structure.IOutputConceptSubstituteMenuPart" flags="ng" index="3EoQpk">
        <reference id="7991336459489872009" name="outputConcept" index="3EoQqy" />
      </concept>
      <concept id="4307758654696938365" name="jetbrains.mps.lang.editor.structure.QueryFunction_SubstituteMenu_RefPresentation" flags="ig" index="1WAQ3h" />
      <concept id="4307758654696952957" name="jetbrains.mps.lang.editor.structure.QueryFunctionParameter_SubstituteMenu_ReferencedNode" flags="ng" index="1WAUZh" />
      <concept id="8428109087107030357" name="jetbrains.mps.lang.editor.structure.SubstituteMenuPart_ReferenceScope" flags="ng" index="3XHNnq">
        <reference id="8428109087107339113" name="reference" index="3XGfJA" />
        <child id="4307758654694907855" name="descriptionTextFunction" index="1WZ6hz" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
    </language>
  </registry>
  <node concept="3p36aQ" id="4QUW3edDWDN">
    <property role="3GE5qa" value="aspects.variables" />
    <ref role="aqKnT" to="hpcx:4QUW3edDU1n" resolve="SpecialistTypeVariable" />
  </node>
  <node concept="3p36aQ" id="4QUW3efWYHR">
    <ref role="aqKnT" to="hpcx:4QUW3efwBDa" resolve="SpecialistTypeReference" />
    <node concept="3XHNnq" id="4QUW3efWYHS" role="3ft7WO">
      <ref role="3EoQqy" to="hpcx:4QUW3efwBDd" resolve="EntitySpecialistTypeReference" />
      <ref role="3XGfJA" to="hpcx:4QUW3efwBDj" resolve="target" />
    </node>
    <node concept="3XHNnq" id="4QUW3efWYHX" role="3ft7WO">
      <ref role="3EoQqy" to="hpcx:4QUW3efwBDe" resolve="AspectSpecialistTypeReference" />
      <ref role="3XGfJA" to="hpcx:4QUW3efwBDf" resolve="target" />
    </node>
  </node>
  <node concept="3p36aQ" id="4V3GMfXOim_">
    <property role="3GE5qa" value="base.references" />
    <ref role="aqKnT" to="hpcx:4QUW3efwBDd" resolve="EntitySpecialistTypeReference" />
    <node concept="3XHNnq" id="4V3GMfXOimA" role="3ft7WO">
      <ref role="3XGfJA" to="hpcx:4QUW3efwBDj" resolve="target" />
      <node concept="1WAQ3h" id="4V3GMfXOimC" role="1WZ6hz">
        <node concept="3clFbS" id="4V3GMfXOimD" role="2VODD2">
          <node concept="3clFbF" id="4V3GMfXOive" role="3cqZAp">
            <node concept="2OqwBi" id="4V3GMfXOiO$" role="3clFbG">
              <node concept="1WAUZh" id="4V3GMfXOivd" role="2Oq$k0" />
              <node concept="3TrcHB" id="4V3GMfXOjel" role="2OqNvi">
                <ref role="3TsBF5" to="kkto:4QUW3edDU1q" resolve="description" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

