<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:2530bb31-0779-45e3-bc3f-01507d6b2623(no.uio.mLab.decisions.references.specialistType.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="6" />
    <devkit ref="78434eb8-b0e5-444b-850d-e7c4ad2da9ab(jetbrains.mps.devkit.aspect.structure)" />
  </languages>
  <imports>
    <import index="7f9y" ref="r:790ac37e-42d3-4dd3-977c-e8fe9cc56a45(no.uio.mLab.decisions.core.structure)" />
    <import index="bop4" ref="r:cbb3825a-4c97-4746-89c7-6d2bd4f55eff(no.uio.mLab.entities.specialistType.structure)" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <property id="4628067390765956807" name="final" index="R5$K2" />
        <property id="4628067390765956802" name="abstract" index="R5$K7" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
      </concept>
      <concept id="1169125989551" name="jetbrains.mps.lang.structure.structure.InterfaceConceptDeclaration" flags="ig" index="PlHQZ" />
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599698500" name="specializedLink" index="20ksaX" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="4QUW3edDU1n">
    <property role="EcuMT" value="5601053190799204439" />
    <property role="3GE5qa" value="aspects.variables" />
    <property role="TrG5h" value="SpecialistTypeVariable" />
    <property role="R5$K7" value="false" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" to="7f9y:4QUW3edDXqS" resolve="AspectVariable" />
  </node>
  <node concept="1TIwiD" id="4QUW3efwBDa">
    <property role="EcuMT" value="5601053190830324298" />
    <property role="TrG5h" value="SpecialistTypeReference" />
    <property role="R5$K7" value="false" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" to="7f9y:4QUW3efv2iG" resolve="Reference" />
    <node concept="PrWs8" id="4QUW3efWZVa" role="PzmwI">
      <ref role="PrY4T" node="4QUW3efWZ4C" resolve="ITranslatableSpecialistTypeConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="4QUW3efwBDd">
    <property role="EcuMT" value="5601053190830324301" />
    <property role="3GE5qa" value="base.references" />
    <property role="TrG5h" value="EntitySpecialistTypeReference" />
    <ref role="1TJDcQ" node="4QUW3efwBDa" resolve="SpecialistTypeReference" />
    <node concept="1TJgyj" id="4QUW3efwBDj" role="1TKVEi">
      <property role="IQ2ns" value="5601053190830324307" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="target" />
      <ref role="20lvS9" to="bop4:1Hxyv4DS4$7" resolve="SpecialistType" />
      <ref role="20ksaX" to="7f9y:4QUW3efv2jv" resolve="target" />
    </node>
    <node concept="PrWs8" id="6LTgXmMD2K0" role="PzmwI">
      <ref role="PrY4T" to="7f9y:6LTgXmMvpq9" resolve="IEntityReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="4QUW3efwBDe">
    <property role="EcuMT" value="5601053190830324302" />
    <property role="3GE5qa" value="aspects.references" />
    <property role="TrG5h" value="AspectSpecialistTypeReference" />
    <ref role="1TJDcQ" node="4QUW3efwBDa" resolve="SpecialistTypeReference" />
    <node concept="1TJgyj" id="4QUW3efwBDf" role="1TKVEi">
      <property role="IQ2ns" value="5601053190830324303" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="target" />
      <ref role="20lvS9" node="4QUW3edDU1n" resolve="SpecialistTypeVariable" />
      <ref role="20ksaX" to="7f9y:6LTgXmMAS1z" resolve="target" />
    </node>
    <node concept="PrWs8" id="6LTgXmMD2JX" role="PzmwI">
      <ref role="PrY4T" to="7f9y:6LTgXmMAn$t" resolve="IAspectVariableReference" />
    </node>
  </node>
  <node concept="PlHQZ" id="4QUW3efWZ4C">
    <property role="EcuMT" value="5601053190837760296" />
    <property role="TrG5h" value="ITranslatableSpecialistTypeConcept" />
  </node>
</model>

