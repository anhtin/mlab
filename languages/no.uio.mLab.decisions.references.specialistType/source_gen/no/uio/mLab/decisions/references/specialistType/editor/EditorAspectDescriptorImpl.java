package no.uio.mLab.decisions.references.specialistType.editor;

/*Generated by MPS */

import jetbrains.mps.nodeEditor.EditorAspectDescriptorBase;
import org.jetbrains.annotations.NotNull;
import java.util.Collection;
import jetbrains.mps.openapi.editor.descriptor.SubstituteMenu;
import org.jetbrains.mps.openapi.language.SAbstractConcept;
import java.util.Collections;
import jetbrains.mps.lang.smodel.ConceptSwitchIndex;
import jetbrains.mps.lang.smodel.ConceptSwitchIndexBuilder;
import jetbrains.mps.smodel.adapter.ids.MetaIdFactory;

public class EditorAspectDescriptorImpl extends EditorAspectDescriptorBase {


  @NotNull
  @Override
  public Collection<SubstituteMenu> getDeclaredDefaultSubstituteMenus(SAbstractConcept concept) {
    SAbstractConcept cncpt = concept;
    switch (conceptIndex.index(cncpt)) {
      case 0:
        return Collections.<SubstituteMenu>singletonList(new EntitySpecialistTypeReference_SubstituteMenu());
      case 1:
        return Collections.<SubstituteMenu>singletonList(new SpecialistTypeReference_SubstituteMenu());
      case 2:
        return Collections.<SubstituteMenu>singletonList(new SpecialistTypeVariable_SubstituteMenu());
      default:
    }
    return Collections.<SubstituteMenu>emptyList();
  }

  private static final ConceptSwitchIndex conceptIndex = new ConceptSwitchIndexBuilder().put(MetaIdFactory.conceptId(0xe664af0ccb5427dL, 0x94ac6f90ac2b6424L, 0x4dbaf0338f827a4dL), MetaIdFactory.conceptId(0xe664af0ccb5427dL, 0x94ac6f90ac2b6424L, 0x4dbaf0338f827a4aL), MetaIdFactory.conceptId(0xe664af0ccb5427dL, 0x94ac6f90ac2b6424L, 0x4dbaf0338da7a057L)).seal();
}
