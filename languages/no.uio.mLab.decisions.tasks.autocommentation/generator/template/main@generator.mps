<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:997dbebf-18e1-473c-b624-9eda24cc4937(main@generator)">
  <persistence version="9" />
  <languages>
    <devkit ref="a2eb3a43-fcc2-4200-80dc-c60110c4862d(jetbrains.mps.devkit.templates)" />
  </languages>
  <imports>
    <import index="jn6m" ref="r:8d706f6f-a342-4d4f-96d8-7be452515eda(no.uio.mLab.decisions.tasks.autocommentation.structure)" />
    <import index="raik" ref="r:80119bb0-41d8-4e59-a1ce-200704faa812(no.uio.mLab.decisions.tasks.autocommentation.runtime)" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="2820489544401957797" name="jetbrains.mps.baseLanguage.structure.DefaultClassCreator" flags="nn" index="HV5vD">
        <reference id="2820489544401957798" name="classifier" index="HV5vE" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1212685548494" name="jetbrains.mps.baseLanguage.structure.ClassCreator" flags="nn" index="1pGfFk" />
    </language>
    <language id="b401a680-8325-4110-8fd3-84331ff25bef" name="jetbrains.mps.lang.generator">
      <concept id="1114706874351" name="jetbrains.mps.lang.generator.structure.CopySrcNodeMacro" flags="ln" index="29HgVG">
        <child id="1168024447342" name="sourceNodeQuery" index="3NFExx" />
      </concept>
      <concept id="1095416546421" name="jetbrains.mps.lang.generator.structure.MappingConfiguration" flags="ig" index="bUwia">
        <child id="1167328349397" name="reductionMappingRule" index="3acgRq" />
        <child id="1195502100749" name="preMappingScript" index="1puA0r" />
      </concept>
      <concept id="1177093525992" name="jetbrains.mps.lang.generator.structure.InlineTemplate_RuleConsequence" flags="lg" index="gft3U">
        <child id="1177093586806" name="templateNode" index="gfFT$" />
      </concept>
      <concept id="1167169188348" name="jetbrains.mps.lang.generator.structure.TemplateFunctionParameter_sourceNode" flags="nn" index="30H73N" />
      <concept id="1167169308231" name="jetbrains.mps.lang.generator.structure.BaseMappingRule" flags="ng" index="30H$t8">
        <reference id="1167169349424" name="applicableConcept" index="30HIoZ" />
      </concept>
      <concept id="1167327847730" name="jetbrains.mps.lang.generator.structure.Reduction_MappingRule" flags="lg" index="3aamgX">
        <child id="1169672767469" name="ruleConsequence" index="1lVwrX" />
      </concept>
      <concept id="1195499912406" name="jetbrains.mps.lang.generator.structure.MappingScript" flags="lg" index="1pmfR0">
        <property id="1195595592106" name="scriptKind" index="1v3f2W" />
        <child id="1195501105008" name="codeBlock" index="1pqMTA" />
      </concept>
      <concept id="1195500722856" name="jetbrains.mps.lang.generator.structure.MappingScript_CodeBlock" flags="in" index="1pplIY" />
      <concept id="1195502151594" name="jetbrains.mps.lang.generator.structure.MappingScriptReference" flags="lg" index="1puMqW">
        <reference id="1195502167610" name="mappingScript" index="1puQsG" />
      </concept>
      <concept id="1168024337012" name="jetbrains.mps.lang.generator.structure.SourceSubstituteMacro_SourceNodeQuery" flags="in" index="3NFfHV" />
    </language>
    <language id="760a0a8c-eabb-4521-8bfd-65db761a9ba3" name="jetbrains.mps.baseLanguage.logging">
      <concept id="6332851714983831325" name="jetbrains.mps.baseLanguage.logging.structure.MsgStatement" flags="ng" index="2xdQw9">
        <property id="6332851714983843871" name="severity" index="2xdLsb" />
        <child id="5721587534047265374" name="message" index="9lYJi" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="bUwia" id="1mAGFBLE7p$">
    <property role="TrG5h" value="taskAutocommentationMain" />
    <node concept="1puMqW" id="7lYCqhudsHa" role="1puA0r">
      <ref role="1puQsG" node="7lYCqhudsHe" resolve="taskAutocommentationScript" />
    </node>
    <node concept="3aamgX" id="2FjKBCOLsc$" role="3acgRq">
      <ref role="30HIoZ" to="jn6m:2FjKBCOKOcn" resolve="HasInterpretativeComment" />
      <node concept="gft3U" id="2FjKBCOLsdj" role="1lVwrX">
        <node concept="2ShNRf" id="2FjKBCOLsdp" role="gfFT$">
          <node concept="1pGfFk" id="2FjKBCOLsgy" role="2ShVmc">
            <ref role="37wK5l" to="raik:2FjKBCOLdck" resolve="HasInterpretativeComment" />
            <node concept="2ShNRf" id="2FjKBCOLsgH" role="37wK5m">
              <node concept="HV5vD" id="2FjKBCOLslN" role="2ShVmc">
                <ref role="HV5vE" to="raik:4QUW3ee0Hqr" resolve="InterpretativeComment" />
              </node>
              <node concept="29HgVG" id="2FjKBCOLsm8" role="lGtFl">
                <node concept="3NFfHV" id="2FjKBCOLsm9" role="3NFExx">
                  <node concept="3clFbS" id="2FjKBCOLsma" role="2VODD2">
                    <node concept="3clFbF" id="2FjKBCOLsmg" role="3cqZAp">
                      <node concept="2OqwBi" id="2FjKBCOLsmb" role="3clFbG">
                        <node concept="3TrEf2" id="2FjKBCOLsme" role="2OqNvi">
                          <ref role="3Tt5mk" to="jn6m:2FjKBCOKOco" resolve="comment" />
                        </node>
                        <node concept="30H73N" id="2FjKBCOLsmf" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="1mAGFBL_DGQ" role="3acgRq">
      <ref role="30HIoZ" to="jn6m:1mAGFBLedHi" resolve="AddCommentForRequestor" />
      <node concept="gft3U" id="1mAGFBL_DGU" role="1lVwrX">
        <node concept="2ShNRf" id="1mAGFBL_DK3" role="gfFT$">
          <node concept="1pGfFk" id="1mAGFBL_DQi" role="2ShVmc">
            <ref role="37wK5l" to="raik:1mAGFBL$_fg" resolve="AddCommentForRequestor" />
            <node concept="2ShNRf" id="4QUW3ee0Idt" role="37wK5m">
              <node concept="HV5vD" id="4QUW3ee0Iiq" role="2ShVmc">
                <ref role="HV5vE" to="raik:4QUW3ee0Hqr" resolve="InterpretativeComment" />
              </node>
              <node concept="29HgVG" id="4QUW3ee0IiH" role="lGtFl">
                <node concept="3NFfHV" id="4QUW3ee0IiI" role="3NFExx">
                  <node concept="3clFbS" id="4QUW3ee0IiJ" role="2VODD2">
                    <node concept="3clFbF" id="4QUW3ee0IiP" role="3cqZAp">
                      <node concept="2OqwBi" id="4QUW3ee0IiK" role="3clFbG">
                        <node concept="3TrEf2" id="4QUW3ee0IiN" role="2OqNvi">
                          <ref role="3Tt5mk" to="jn6m:1mAGFBLen0G" resolve="comment" />
                        </node>
                        <node concept="30H73N" id="4QUW3ee0IiO" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="PDjyzk_LLJ" role="3acgRq">
      <ref role="30HIoZ" to="jn6m:PDjyzk_uY0" resolve="TextInterpretativeCommentForRequestor" />
      <node concept="gft3U" id="PDjyzk_LMg" role="1lVwrX">
        <node concept="2ShNRf" id="PDjyzk_LMm" role="gfFT$">
          <node concept="1pGfFk" id="PDjyzk_LPt" role="2ShVmc">
            <ref role="37wK5l" to="raik:PDjyzk_Kqw" resolve="TextInterpretativeCommentForRequestor" />
            <node concept="Xl_RD" id="PDjyzk_LPA" role="37wK5m">
              <property role="Xl_RC" value="text comment" />
              <node concept="29HgVG" id="PDjyzk_LQ0" role="lGtFl">
                <node concept="3NFfHV" id="PDjyzk_LQ1" role="3NFExx">
                  <node concept="3clFbS" id="PDjyzk_LQ2" role="2VODD2">
                    <node concept="3clFbF" id="PDjyzk_LQ8" role="3cqZAp">
                      <node concept="2OqwBi" id="PDjyzk_LQ3" role="3clFbG">
                        <node concept="3TrEf2" id="PDjyzk_LQ6" role="2OqNvi">
                          <ref role="3Tt5mk" to="jn6m:PDjyzk_uYx" resolve="text" />
                        </node>
                        <node concept="30H73N" id="PDjyzk_LQ7" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="1mAGFBL_Xgz" role="3acgRq">
      <ref role="30HIoZ" to="jn6m:1mAGFBKnHGH" resolve="NotifySpecialist" />
      <node concept="gft3U" id="1mAGFBLA0c4" role="1lVwrX">
        <node concept="2ShNRf" id="1mAGFBLA0c8" role="gfFT$">
          <node concept="1pGfFk" id="1mAGFBLA0j0" role="2ShVmc">
            <ref role="37wK5l" to="raik:1mAGFBL_YUT" resolve="NotifySpecialist" />
            <node concept="Xl_RD" id="4QUW3efwSsj" role="37wK5m">
              <property role="Xl_RC" value="" />
              <node concept="29HgVG" id="4QUW3efwSsy" role="lGtFl">
                <node concept="3NFfHV" id="4QUW3efwSsz" role="3NFExx">
                  <node concept="3clFbS" id="4QUW3efwSs$" role="2VODD2">
                    <node concept="3clFbF" id="4QUW3efwSsE" role="3cqZAp">
                      <node concept="2OqwBi" id="4QUW3efwSs_" role="3clFbG">
                        <node concept="3TrEf2" id="4QUW3efwSsC" role="2OqNvi">
                          <ref role="3Tt5mk" to="jn6m:4QUW3efwFhq" resolve="specialistReference" />
                        </node>
                        <node concept="30H73N" id="4QUW3efwSsD" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="1pmfR0" id="7lYCqhudsHe">
    <property role="TrG5h" value="taskAutocommentationScript" />
    <property role="1v3f2W" value="pre_processing" />
    <node concept="1pplIY" id="7lYCqhudsHf" role="1pqMTA">
      <node concept="3clFbS" id="7lYCqhudsHg" role="2VODD2">
        <node concept="2xdQw9" id="7lYCqhudsHr" role="3cqZAp">
          <property role="2xdLsb" value="debug" />
          <node concept="Xl_RD" id="7lYCqhudsHt" role="9lYJi">
            <property role="Xl_RC" value="tasks.autocommentation" />
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

