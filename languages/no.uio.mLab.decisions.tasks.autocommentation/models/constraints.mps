<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:eec9392b-9131-4731-825b-b4cd0a12f71a(no.uio.mLab.decisions.tasks.autocommentation.constraints)">
  <persistence version="9" />
  <languages>
    <use id="3f4bc5f5-c6c1-4a28-8b10-c83066ffa4a1" name="jetbrains.mps.lang.constraints" version="4" />
    <devkit ref="00000000-0000-4000-0000-5604ebd4f22c(jetbrains.mps.devkit.aspect.constraints)" />
  </languages>
  <imports>
    <import index="jn6m" ref="r:8d706f6f-a342-4d4f-96d8-7be452515eda(no.uio.mLab.decisions.tasks.autocommentation.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="3f4bc5f5-c6c1-4a28-8b10-c83066ffa4a1" name="jetbrains.mps.lang.constraints">
      <concept id="8401916545537438642" name="jetbrains.mps.lang.constraints.structure.InheritedNodeScopeFactory" flags="ng" index="1dDu$B">
        <reference id="8401916545537438643" name="kind" index="1dDu$A" />
      </concept>
      <concept id="1213093968558" name="jetbrains.mps.lang.constraints.structure.ConceptConstraints" flags="ng" index="1M2fIO">
        <reference id="1213093996982" name="concept" index="1M2myG" />
        <child id="1213100494875" name="referent" index="1Mr941" />
      </concept>
      <concept id="1148687176410" name="jetbrains.mps.lang.constraints.structure.NodeReferentConstraint" flags="ng" index="1N5Pfh">
        <reference id="1148687202698" name="applicableLink" index="1N5Vy1" />
        <child id="1148687345559" name="searchScopeFactory" index="1N6uqs" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
    </language>
  </registry>
  <node concept="1M2fIO" id="2FjKBCPOuRH">
    <property role="3GE5qa" value="aspect.references" />
    <ref role="1M2myG" to="jn6m:2FjKBCPNbwZ" resolve="AspectInterpretativeCommentReference" />
    <node concept="1N5Pfh" id="2FjKBCPOuRI" role="1Mr941">
      <ref role="1N5Vy1" to="jn6m:2FjKBCPOuRg" resolve="target" />
      <node concept="1dDu$B" id="2FjKBCPOuRK" role="1N6uqs">
        <ref role="1dDu$A" to="jn6m:2FjKBCPNbwY" resolve="InterpretativeCommentVariable" />
      </node>
    </node>
  </node>
  <node concept="1M2fIO" id="2FjKBCPOvKy">
    <property role="3GE5qa" value="aspect.references" />
    <ref role="1M2myG" to="jn6m:2FjKBCPOvmx" resolve="AspectInterpretativeCommentForRequestorReference" />
    <node concept="1N5Pfh" id="2FjKBCPOvKz" role="1Mr941">
      <ref role="1N5Vy1" to="jn6m:2FjKBCPOvmz" resolve="target" />
      <node concept="1dDu$B" id="2FjKBCPOvK_" role="1N6uqs">
        <ref role="1dDu$A" to="jn6m:2FjKBCPNbwY" resolve="InterpretativeCommentVariable" />
      </node>
    </node>
  </node>
</model>

