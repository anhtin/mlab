<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:00aa8c92-ad64-4f4b-9901-076e5158f704(no.uio.mLab.decisions.tasks.autocommentation.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="11" />
    <use id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions" version="4" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="i7z6" ref="r:515ff38d-92f9-43f3-aa4c-c5bec29efe22(no.uio.mLab.decisions.core.editor)" />
    <import index="jn6m" ref="r:8d706f6f-a342-4d4f-96d8-7be452515eda(no.uio.mLab.decisions.tasks.autocommentation.structure)" implicit="true" />
    <import index="1yj" ref="r:0c887d67-81ab-420f-9a35-a9f6bf96c975(no.uio.mLab.shared.behavior)" implicit="true" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" implicit="true" />
    <import index="7f9y" ref="r:790ac37e-42d3-4dd3-977c-e8fe9cc56a45(no.uio.mLab.decisions.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="2000375450116454183" name="jetbrains.mps.lang.editor.structure.ISubstituteMenu" flags="ng" index="22mbnS">
        <child id="414384289274416996" name="parts" index="3ft7WO" />
      </concept>
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi" />
      <concept id="1106270549637" name="jetbrains.mps.lang.editor.structure.CellLayout_Horizontal" flags="nn" index="2iRfu4" />
      <concept id="1177327274449" name="jetbrains.mps.lang.editor.structure.QueryFunctionParameter_pattern" flags="nn" index="ub8z3" />
      <concept id="1177327570013" name="jetbrains.mps.lang.editor.structure.QueryFunction_SubstituteMenu_Substitute" flags="in" index="ucgPf" />
      <concept id="8478191136883534237" name="jetbrains.mps.lang.editor.structure.IExtensibleSubstituteMenuPart" flags="ng" index="upBLQ">
        <child id="8478191136883534238" name="features" index="upBLP" />
      </concept>
      <concept id="1177335944525" name="jetbrains.mps.lang.editor.structure.QueryFunction_SubstituteMenu_SubstituteString" flags="in" index="uGdhv" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="8383079901754291618" name="jetbrains.mps.lang.editor.structure.CellModel_NextEditor" flags="ng" index="B$lHz" />
      <concept id="1078938745671" name="jetbrains.mps.lang.editor.structure.EditorComponentDeclaration" flags="ig" index="PKFIW">
        <child id="7033942394258392116" name="overridenEditorComponent" index="1PM95z" />
      </concept>
      <concept id="615427434521884870" name="jetbrains.mps.lang.editor.structure.SubstituteMenuPart_Subconcepts" flags="ng" index="2VfDsV" />
      <concept id="1630016958697344083" name="jetbrains.mps.lang.editor.structure.IMenu_Concept" flags="ng" index="2ZABuq">
        <reference id="6591946374543067572" name="conceptDeclaration" index="aqKnT" />
      </concept>
      <concept id="8998492695583109601" name="jetbrains.mps.lang.editor.structure.QueryFunction_SubstituteMenu_CanSubstitute" flags="ig" index="16Na2f" />
      <concept id="8998492695583125082" name="jetbrains.mps.lang.editor.structure.SubstituteFeature_MatchingText" flags="ng" index="16NfWO">
        <child id="8998492695583129244" name="query" index="16NeZM" />
      </concept>
      <concept id="8998492695583129971" name="jetbrains.mps.lang.editor.structure.SubstituteFeature_DescriptionText" flags="ng" index="16NL0t">
        <child id="8998492695583129972" name="query" index="16NL0q" />
      </concept>
      <concept id="8998492695583129991" name="jetbrains.mps.lang.editor.structure.SubstituteFeature_CanSubstitute" flags="ng" index="16NL3D">
        <child id="8998492695583129992" name="query" index="16NL3A" />
      </concept>
      <concept id="7342352913006985483" name="jetbrains.mps.lang.editor.structure.SubstituteMenuPart_Action" flags="ng" index="3eGOop">
        <child id="8612453216082699922" name="substituteHandler" index="3aKz83" />
      </concept>
      <concept id="3308396621974580100" name="jetbrains.mps.lang.editor.structure.SubstituteMenu_Default" flags="ng" index="3p36aQ" />
      <concept id="5425882385312046132" name="jetbrains.mps.lang.editor.structure.QueryFunctionParameter_SubstituteMenu_CurrentTargetNode" flags="nn" index="1yR$tW" />
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <property id="1139852716018" name="noTargetText" index="1$x2rV" />
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389446423" name="jetbrains.mps.lang.editor.structure.CellModel_Collection" flags="sn" stub="3013115976261988961" index="3EZMnI">
        <child id="1106270802874" name="cellLayout" index="2iSdaV" />
        <child id="1073389446424" name="childCellModel" index="3EZMnx" />
      </concept>
      <concept id="1073389882823" name="jetbrains.mps.lang.editor.structure.CellModel_RefNode" flags="sg" stub="730538219795960754" index="3F1sOY" />
      <concept id="7033942394256351208" name="jetbrains.mps.lang.editor.structure.EditorComponentDeclarationReference" flags="ng" index="1PE4EZ">
        <reference id="7033942394256351817" name="editorComponent" index="1PE7su" />
      </concept>
      <concept id="4307758654696938365" name="jetbrains.mps.lang.editor.structure.QueryFunction_SubstituteMenu_RefPresentation" flags="ig" index="1WAQ3h" />
      <concept id="4307758654696952957" name="jetbrains.mps.lang.editor.structure.QueryFunctionParameter_SubstituteMenu_ReferencedNode" flags="ng" index="1WAUZh" />
      <concept id="8428109087107030357" name="jetbrains.mps.lang.editor.structure.SubstituteMenuPart_ReferenceScope" flags="ng" index="3XHNnq">
        <reference id="8428109087107339113" name="reference" index="3XGfJA" />
        <child id="4307758654694907855" name="descriptionTextFunction" index="1WZ6hz" />
      </concept>
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1080223426719" name="jetbrains.mps.baseLanguage.structure.OrExpression" flags="nn" index="22lmx$" />
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="1153417849900" name="jetbrains.mps.baseLanguage.structure.GreaterThanOrEqualsExpression" flags="nn" index="2d3UOw" />
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1070534370425" name="jetbrains.mps.baseLanguage.structure.IntegerType" flags="in" index="10Oyi0" />
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="1225271369338" name="jetbrains.mps.baseLanguage.structure.IsEmptyOperation" flags="nn" index="17RlXB" />
      <concept id="1225271408483" name="jetbrains.mps.baseLanguage.structure.IsNotEmptyOperation" flags="nn" index="17RvpY" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242869" name="jetbrains.mps.baseLanguage.structure.MinusExpression" flags="nn" index="3cpWsd" />
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1163668896201" name="jetbrains.mps.baseLanguage.structure.TernaryOperatorExpression" flags="nn" index="3K4zz7">
        <child id="1163668914799" name="condition" index="3K4Cdx" />
        <child id="1163668922816" name="ifTrue" index="3K4E3e" />
        <child id="1163668934364" name="ifFalse" index="3K4GZi" />
      </concept>
      <concept id="1080120340718" name="jetbrains.mps.baseLanguage.structure.AndExpression" flags="nn" index="1Wc70l" />
    </language>
    <language id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions">
      <concept id="5979988948250981289" name="jetbrains.mps.lang.actions.structure.SNodeCreatorAndInitializer" flags="nn" index="2fJWfE">
        <child id="3757480014665187678" name="prototype" index="1wAG5O" />
      </concept>
      <concept id="767145758118872830" name="jetbrains.mps.lang.actions.structure.NF_Link_SetNewChildOperation" flags="nn" index="2DeJnY" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="2644386474300074836" name="jetbrains.mps.lang.smodel.structure.ConceptIdRefExpression" flags="nn" index="35c_gC">
        <reference id="2644386474300074837" name="conceptDeclaration" index="35c_gD" />
      </concept>
      <concept id="1180636770613" name="jetbrains.mps.lang.smodel.structure.SNodeCreator" flags="nn" index="3zrR0B">
        <child id="1180636770616" name="createdType" index="3zrR0E" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="24kQdi" id="1mAGFBLexVS">
    <property role="3GE5qa" value="base.actions.addComment" />
    <ref role="1XX52x" to="jn6m:1mAGFBLedHi" resolve="AddCommentForRequestor" />
    <node concept="3EZMnI" id="1mAGFBLexVU" role="2wV5jI">
      <node concept="2iRfu4" id="1mAGFBLexVV" role="2iSdaV" />
      <node concept="B$lHz" id="1mAGFBLexW0" role="3EZMnx" />
      <node concept="3F1sOY" id="1mAGFBLexW6" role="3EZMnx">
        <property role="1$x2rV" value="&lt;&lt; ... &gt;&gt;" />
        <ref role="1NtTu8" to="jn6m:1mAGFBLen0G" resolve="comment" />
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="1mAGFBLeeeW">
    <property role="3GE5qa" value="base.actions.addComment" />
    <ref role="aqKnT" to="jn6m:1mAGFBLedHi" resolve="AddCommentForRequestor" />
    <node concept="3eGOop" id="1mAGFBLeeeX" role="3ft7WO">
      <node concept="ucgPf" id="1mAGFBLeeeY" role="3aKz83">
        <node concept="3clFbS" id="1mAGFBLeeeZ" role="2VODD2">
          <node concept="3clFbF" id="1mAGFBLEhAK" role="3cqZAp">
            <node concept="2ShNRf" id="1mAGFBLeej8" role="3clFbG">
              <node concept="2fJWfE" id="1mAGFBLeequ" role="2ShVmc">
                <node concept="3Tqbb2" id="1mAGFBLeeqw" role="3zrR0E">
                  <ref role="ehGHo" to="jn6m:1mAGFBLedHi" resolve="AddCommentForRequestor" />
                </node>
                <node concept="1yR$tW" id="1mAGFBLee$W" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="1mAGFBLeeDj" role="upBLP">
        <node concept="uGdhv" id="1mAGFBLeeQG" role="16NeZM">
          <node concept="3clFbS" id="1mAGFBLeeQI" role="2VODD2">
            <node concept="3clFbF" id="1mAGFBLeeZk" role="3cqZAp">
              <node concept="2OqwBi" id="1mAGFBLefBs" role="3clFbG">
                <node concept="35c_gC" id="1mAGFBLeeZj" role="2Oq$k0">
                  <ref role="35c_gD" to="jn6m:1mAGFBLedHi" resolve="AddCommentForRequestor" />
                </node>
                <node concept="2qgKlT" id="1mAGFBLF1UE" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="1mAGFBLeglV" role="upBLP">
        <node concept="uGdhv" id="1mAGFBLegqx" role="16NL0q">
          <node concept="3clFbS" id="1mAGFBLegqz" role="2VODD2">
            <node concept="3clFbF" id="1mAGFBLegz9" role="3cqZAp">
              <node concept="2OqwBi" id="1mAGFBLeh8$" role="3clFbG">
                <node concept="35c_gC" id="1mAGFBLegz8" role="2Oq$k0">
                  <ref role="35c_gD" to="jn6m:1mAGFBLedHi" resolve="AddCommentForRequestor" />
                </node>
                <node concept="2qgKlT" id="1mAGFBLF13M" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="1mAGFBLd8rW">
    <property role="3GE5qa" value="base.actions" />
    <ref role="1XX52x" to="jn6m:1mAGFBKnHGH" resolve="NotifySpecialist" />
    <node concept="3EZMnI" id="1mAGFBLdeCy" role="2wV5jI">
      <node concept="2iRfu4" id="1mAGFBLdeCz" role="2iSdaV" />
      <node concept="B$lHz" id="1mAGFBLdeCF" role="3EZMnx" />
      <node concept="3F1sOY" id="4QUW3efwFhw" role="3EZMnx">
        <ref role="1NtTu8" to="jn6m:4QUW3efwFhq" resolve="specialistReference" />
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="1mAGFBLa$jQ">
    <property role="3GE5qa" value="base.actions" />
    <ref role="aqKnT" to="jn6m:1mAGFBKnHGH" resolve="NotifySpecialist" />
    <node concept="3eGOop" id="1mAGFBLa$jR" role="3ft7WO">
      <node concept="ucgPf" id="1mAGFBLa$jS" role="3aKz83">
        <node concept="3clFbS" id="1mAGFBLa$jT" role="2VODD2">
          <node concept="3clFbF" id="1mAGFBLa$o4" role="3cqZAp">
            <node concept="2ShNRf" id="1mAGFBLa$o2" role="3clFbG">
              <node concept="2fJWfE" id="1mAGFBLa$vo" role="2ShVmc">
                <node concept="3Tqbb2" id="1mAGFBLa$vq" role="3zrR0E">
                  <ref role="ehGHo" to="jn6m:1mAGFBKnHGH" resolve="NotifySpecialist" />
                </node>
                <node concept="1yR$tW" id="1mAGFBLa$DQ" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="1mAGFBLa_j9" role="upBLP">
        <node concept="uGdhv" id="1mAGFBLa_ny" role="16NeZM">
          <node concept="3clFbS" id="1mAGFBLa_n$" role="2VODD2">
            <node concept="3clFbF" id="1mAGFBLa_wa" role="3cqZAp">
              <node concept="2OqwBi" id="1mAGFBLaA8i" role="3clFbG">
                <node concept="35c_gC" id="1mAGFBLa_w9" role="2Oq$k0">
                  <ref role="35c_gD" to="jn6m:1mAGFBKnHGH" resolve="NotifySpecialist" />
                </node>
                <node concept="2qgKlT" id="1mAGFBLaA$J" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="1mAGFBLaAQQ" role="upBLP">
        <node concept="uGdhv" id="1mAGFBLaAVs" role="16NL0q">
          <node concept="3clFbS" id="1mAGFBLaAVu" role="2VODD2">
            <node concept="3clFbF" id="1mAGFBLaB44" role="3cqZAp">
              <node concept="2OqwBi" id="1mAGFBLaBGc" role="3clFbG">
                <node concept="35c_gC" id="1mAGFBLaB43" role="2Oq$k0">
                  <ref role="35c_gD" to="jn6m:1mAGFBKnHGH" resolve="NotifySpecialist" />
                </node>
                <node concept="2qgKlT" id="1mAGFBLF2Zl" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="PDjyzk_uYs">
    <property role="3GE5qa" value="base.actions.addComment.parameters" />
    <ref role="1XX52x" to="jn6m:PDjyzk_uY0" resolve="TextInterpretativeCommentForRequestor" />
    <node concept="3F1sOY" id="PDjyzk_uY_" role="2wV5jI">
      <property role="1$x2rV" value="..." />
      <ref role="1NtTu8" to="jn6m:PDjyzk_uYx" resolve="text" />
    </node>
  </node>
  <node concept="3p36aQ" id="PDjyzk_BcY">
    <property role="3GE5qa" value="base.actions.addComment.parameters" />
    <ref role="aqKnT" to="jn6m:PDjyzk_uY0" resolve="TextInterpretativeCommentForRequestor" />
    <node concept="3eGOop" id="PDjyzk_BcZ" role="3ft7WO">
      <node concept="16NL3D" id="5jVYnMGVy3J" role="upBLP">
        <node concept="16Na2f" id="5jVYnMGVy3K" role="16NL3A">
          <node concept="3clFbS" id="5jVYnMGVy3L" role="2VODD2">
            <node concept="3clFbF" id="1mAGFBKnt1x" role="3cqZAp">
              <node concept="22lmx$" id="7AAKH6gdy9R" role="3clFbG">
                <node concept="2OqwBi" id="7AAKH6gdyFd" role="3uHU7w">
                  <node concept="ub8z3" id="7AAKH6gdyhM" role="2Oq$k0" />
                  <node concept="liA8E" id="7AAKH6gdz9B" role="2OqNvi">
                    <ref role="37wK5l" to="wyt6:~String.startsWith(java.lang.String):boolean" resolve="startsWith" />
                    <node concept="Xl_RD" id="7AAKH6gdzhX" role="37wK5m">
                      <property role="Xl_RC" value="\&quot;" />
                    </node>
                  </node>
                </node>
                <node concept="2OqwBi" id="7AAKH6gdwKx" role="3uHU7B">
                  <node concept="ub8z3" id="7AAKH6gdwjG" role="2Oq$k0" />
                  <node concept="17RlXB" id="7AAKH6gdxtU" role="2OqNvi" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="ucgPf" id="PDjyzk_Bd0" role="3aKz83">
        <node concept="3clFbS" id="PDjyzk_Bd1" role="2VODD2">
          <node concept="3cpWs8" id="5jVYnMGVBjx" role="3cqZAp">
            <node concept="3cpWsn" id="5jVYnMGVBj$" role="3cpWs9">
              <property role="TrG5h" value="node" />
              <node concept="3Tqbb2" id="5jVYnMGVBjv" role="1tU5fm">
                <ref role="ehGHo" to="jn6m:PDjyzk_uY0" resolve="TextInterpretativeCommentForRequestor" />
              </node>
              <node concept="2ShNRf" id="PDjyzk_BhU" role="33vP2m">
                <node concept="2fJWfE" id="PDjyzk_Bq0" role="2ShVmc">
                  <node concept="3Tqbb2" id="PDjyzk_Bq2" role="3zrR0E">
                    <ref role="ehGHo" to="jn6m:PDjyzk_uY0" resolve="TextInterpretativeCommentForRequestor" />
                  </node>
                  <node concept="1yR$tW" id="PDjyzk_BAm" role="1wAG5O" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="PDjyzk_BhW" role="3cqZAp">
            <node concept="2OqwBi" id="5jVYnMGVCUb" role="3clFbG">
              <node concept="2OqwBi" id="5jVYnMGVC4B" role="2Oq$k0">
                <node concept="37vLTw" id="5jVYnMGVBRF" role="2Oq$k0">
                  <ref role="3cqZAo" node="5jVYnMGVBj$" resolve="node" />
                </node>
                <node concept="3TrEf2" id="5jVYnMGVCqN" role="2OqNvi">
                  <ref role="3Tt5mk" to="jn6m:PDjyzk_uYx" resolve="text" />
                </node>
              </node>
              <node concept="2DeJnY" id="5jVYnMGVDj1" role="2OqNvi" />
            </node>
          </node>
          <node concept="3clFbJ" id="7AAKH6gdKYq" role="3cqZAp">
            <node concept="3clFbS" id="7AAKH6gdKYs" role="3clFbx">
              <node concept="3cpWs8" id="7AAKH6gdzW6" role="3cqZAp">
                <node concept="3cpWsn" id="7AAKH6gdzW9" role="3cpWs9">
                  <property role="TrG5h" value="start" />
                  <node concept="10Oyi0" id="7AAKH6gdzW4" role="1tU5fm" />
                  <node concept="3K4zz7" id="7AAKH6gd_Ib" role="33vP2m">
                    <node concept="3cmrfG" id="7AAKH6gd_Ok" role="3K4E3e">
                      <property role="3cmrfH" value="1" />
                    </node>
                    <node concept="3cmrfG" id="7AAKH6gd_Tq" role="3K4GZi">
                      <property role="3cmrfH" value="0" />
                    </node>
                    <node concept="2OqwBi" id="7AAKH6gd$y9" role="3K4Cdx">
                      <node concept="ub8z3" id="7AAKH6gd$al" role="2Oq$k0" />
                      <node concept="liA8E" id="7AAKH6gd$TV" role="2OqNvi">
                        <ref role="37wK5l" to="wyt6:~String.startsWith(java.lang.String):boolean" resolve="startsWith" />
                        <node concept="Xl_RD" id="7AAKH6gd_2S" role="37wK5m">
                          <property role="Xl_RC" value="\&quot;" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3cpWs8" id="7AAKH6gdA4j" role="3cqZAp">
                <node concept="3cpWsn" id="7AAKH6gdA4m" role="3cpWs9">
                  <property role="TrG5h" value="end" />
                  <node concept="10Oyi0" id="7AAKH6gdA4h" role="1tU5fm" />
                  <node concept="3K4zz7" id="7AAKH6gdBYL" role="33vP2m">
                    <node concept="3cpWsd" id="7AAKH6gdDAu" role="3K4E3e">
                      <node concept="3cmrfG" id="7AAKH6gdDA$" role="3uHU7w">
                        <property role="3cmrfH" value="1" />
                      </node>
                      <node concept="2OqwBi" id="7AAKH6gdCss" role="3uHU7B">
                        <node concept="ub8z3" id="7AAKH6gdC5s" role="2Oq$k0" />
                        <node concept="liA8E" id="7AAKH6gdCPj" role="2OqNvi">
                          <ref role="37wK5l" to="wyt6:~String.length():int" resolve="length" />
                        </node>
                      </node>
                    </node>
                    <node concept="2OqwBi" id="7AAKH6gdFSE" role="3K4GZi">
                      <node concept="ub8z3" id="7AAKH6gdDMq" role="2Oq$k0" />
                      <node concept="liA8E" id="7AAKH6gdGDE" role="2OqNvi">
                        <ref role="37wK5l" to="wyt6:~String.length():int" resolve="length" />
                      </node>
                    </node>
                    <node concept="1Wc70l" id="7AAKH6ge4Nl" role="3K4Cdx">
                      <node concept="2d3UOw" id="7AAKH6ge7Hg" role="3uHU7B">
                        <node concept="3cmrfG" id="7AAKH6ge80C" role="3uHU7w">
                          <property role="3cmrfH" value="2" />
                        </node>
                        <node concept="2OqwBi" id="7AAKH6ge5I4" role="3uHU7B">
                          <node concept="ub8z3" id="7AAKH6ge56n" role="2Oq$k0" />
                          <node concept="liA8E" id="7AAKH6ge6At" role="2OqNvi">
                            <ref role="37wK5l" to="wyt6:~String.length():int" resolve="length" />
                          </node>
                        </node>
                      </node>
                      <node concept="2OqwBi" id="7AAKH6gdAJw" role="3uHU7w">
                        <node concept="ub8z3" id="7AAKH6gdAkL" role="2Oq$k0" />
                        <node concept="liA8E" id="7AAKH6gdB7Y" role="2OqNvi">
                          <ref role="37wK5l" to="wyt6:~String.endsWith(java.lang.String):boolean" resolve="endsWith" />
                          <node concept="Xl_RD" id="7AAKH6gdBht" role="37wK5m">
                            <property role="Xl_RC" value="\&quot;" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="7AAKH6g982Q" role="3cqZAp">
                <node concept="37vLTI" id="7AAKH6g99iP" role="3clFbG">
                  <node concept="2OqwBi" id="7AAKH6gdH6w" role="37vLTx">
                    <node concept="ub8z3" id="7AAKH6g99LG" role="2Oq$k0" />
                    <node concept="liA8E" id="7AAKH6gdHFp" role="2OqNvi">
                      <ref role="37wK5l" to="wyt6:~String.substring(int,int):java.lang.String" resolve="substring" />
                      <node concept="37vLTw" id="7AAKH6gdJbq" role="37wK5m">
                        <ref role="3cqZAo" node="7AAKH6gdzW9" resolve="start" />
                      </node>
                      <node concept="37vLTw" id="7AAKH6gdKoQ" role="37wK5m">
                        <ref role="3cqZAo" node="7AAKH6gdA4m" resolve="end" />
                      </node>
                    </node>
                  </node>
                  <node concept="2OqwBi" id="5jVYnMGVNDM" role="37vLTJ">
                    <node concept="2OqwBi" id="5jVYnMGVMwb" role="2Oq$k0">
                      <node concept="37vLTw" id="5jVYnMGVM4A" role="2Oq$k0">
                        <ref role="3cqZAo" node="5jVYnMGVBj$" resolve="node" />
                      </node>
                      <node concept="3TrEf2" id="5jVYnMGVN2J" role="2OqNvi">
                        <ref role="3Tt5mk" to="jn6m:PDjyzk_uYx" resolve="text" />
                      </node>
                    </node>
                    <node concept="3TrcHB" id="5jVYnMGVOcA" role="2OqNvi">
                      <ref role="3TsBF5" to="7f9y:5Wfdz$0v7yL" resolve="value" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="2OqwBi" id="7AAKH6gdLT7" role="3clFbw">
              <node concept="ub8z3" id="7AAKH6gdLhw" role="2Oq$k0" />
              <node concept="17RvpY" id="7AAKH6gdMLq" role="2OqNvi" />
            </node>
          </node>
          <node concept="3clFbF" id="5jVYnMGVDFB" role="3cqZAp">
            <node concept="37vLTw" id="5jVYnMGVDF_" role="3clFbG">
              <ref role="3cqZAo" node="5jVYnMGVBj$" resolve="node" />
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="PDjyzk_BFt" role="upBLP">
        <node concept="uGdhv" id="PDjyzk_BKA" role="16NeZM">
          <node concept="3clFbS" id="PDjyzk_BKC" role="2VODD2">
            <node concept="3clFbJ" id="5jVYnMGVx1E" role="3cqZAp">
              <node concept="3clFbS" id="5jVYnMGVx1G" role="3clFbx">
                <node concept="3cpWs6" id="5jVYnMGVzKM" role="3cqZAp">
                  <node concept="2OqwBi" id="PDjyzk_CyH" role="3cqZAk">
                    <node concept="35c_gC" id="PDjyzk_BTd" role="2Oq$k0">
                      <ref role="35c_gD" to="jn6m:PDjyzk_uY0" resolve="TextInterpretativeCommentForRequestor" />
                    </node>
                    <node concept="2qgKlT" id="PDjyzk_D2c" role="2OqNvi">
                      <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="5jVYnMGVyWN" role="3clFbw">
                <node concept="ub8z3" id="5jVYnMGVypD" role="2Oq$k0" />
                <node concept="17RlXB" id="5jVYnMGVztO" role="2OqNvi" />
              </node>
            </node>
            <node concept="3clFbF" id="PDjyzk_BTe" role="3cqZAp">
              <node concept="ub8z3" id="5jVYnMGV$BZ" role="3clFbG" />
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="PDjyzk_Dli" role="upBLP">
        <node concept="uGdhv" id="PDjyzk_DqC" role="16NL0q">
          <node concept="3clFbS" id="PDjyzk_DqE" role="2VODD2">
            <node concept="3clFbF" id="PDjyzk_Dzg" role="3cqZAp">
              <node concept="2OqwBi" id="PDjyzk_EcJ" role="3clFbG">
                <node concept="35c_gC" id="PDjyzk_Dzf" role="2Oq$k0">
                  <ref role="35c_gD" to="jn6m:PDjyzk_uY0" resolve="TextInterpretativeCommentForRequestor" />
                </node>
                <node concept="2qgKlT" id="PDjyzk_EGe" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="2FjKBCOKOcN">
    <property role="3GE5qa" value="base.dataValues" />
    <ref role="1XX52x" to="jn6m:2FjKBCOKOcn" resolve="HasInterpretativeComment" />
    <node concept="3EZMnI" id="2FjKBCOKOcP" role="2wV5jI">
      <node concept="2iRfu4" id="2FjKBCOKOcQ" role="2iSdaV" />
      <node concept="B$lHz" id="2FjKBCOL8u_" role="3EZMnx" />
      <node concept="3F1sOY" id="2FjKBCOKOd0" role="3EZMnx">
        <ref role="1NtTu8" to="jn6m:2FjKBCOKOco" resolve="comment" />
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="2FjKBCOL1Yi">
    <property role="3GE5qa" value="base.dataValues" />
    <ref role="aqKnT" to="jn6m:2FjKBCOKOcn" resolve="HasInterpretativeComment" />
    <node concept="3eGOop" id="2FjKBCOL1Yj" role="3ft7WO">
      <node concept="ucgPf" id="2FjKBCOL1Yk" role="3aKz83">
        <node concept="3clFbS" id="2FjKBCOL1Yl" role="2VODD2">
          <node concept="3clFbF" id="2FjKBCOL23M" role="3cqZAp">
            <node concept="2ShNRf" id="2FjKBCOL23K" role="3clFbG">
              <node concept="2fJWfE" id="2FjKBCOL2cl" role="2ShVmc">
                <node concept="3Tqbb2" id="2FjKBCOL2cn" role="3zrR0E">
                  <ref role="ehGHo" to="jn6m:2FjKBCOKOcn" resolve="HasInterpretativeComment" />
                </node>
                <node concept="1yR$tW" id="2FjKBCOL2ns" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="2FjKBCOL2t5" role="upBLP">
        <node concept="uGdhv" id="2FjKBCOL2yK" role="16NeZM">
          <node concept="3clFbS" id="2FjKBCOL2yM" role="2VODD2">
            <node concept="3clFbF" id="2FjKBCOL2Fo" role="3cqZAp">
              <node concept="2OqwBi" id="2FjKBCOL3c8" role="3clFbG">
                <node concept="35c_gC" id="2FjKBCOL2Fn" role="2Oq$k0">
                  <ref role="35c_gD" to="jn6m:2FjKBCOKOcn" resolve="HasInterpretativeComment" />
                </node>
                <node concept="2qgKlT" id="2FjKBCOL3I1" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="2FjKBCOL41V" role="upBLP">
        <node concept="uGdhv" id="2FjKBCOL47N" role="16NL0q">
          <node concept="3clFbS" id="2FjKBCOL47P" role="2VODD2">
            <node concept="3clFbF" id="2FjKBCOL4gr" role="3cqZAp">
              <node concept="2OqwBi" id="2FjKBCOL4Lb" role="3clFbG">
                <node concept="35c_gC" id="2FjKBCOL4gq" role="2Oq$k0">
                  <ref role="35c_gD" to="jn6m:2FjKBCOKOcn" resolve="HasInterpretativeComment" />
                </node>
                <node concept="2qgKlT" id="2FjKBCOL5j4" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="2FjKBCPNbBi">
    <property role="3GE5qa" value="aspect.variables" />
    <ref role="aqKnT" to="jn6m:2FjKBCPNbwY" resolve="InterpretativeCommentVariable" />
    <node concept="3eGOop" id="2FjKBCPNbBj" role="3ft7WO">
      <node concept="ucgPf" id="2FjKBCPNbBk" role="3aKz83">
        <node concept="3clFbS" id="2FjKBCPNbBl" role="2VODD2">
          <node concept="3clFbF" id="2FjKBCPNbGg" role="3cqZAp">
            <node concept="2ShNRf" id="2FjKBCPNbGe" role="3clFbG">
              <node concept="2fJWfE" id="2FjKBCPNbOh" role="2ShVmc">
                <node concept="3Tqbb2" id="2FjKBCPNbOj" role="3zrR0E">
                  <ref role="ehGHo" to="jn6m:2FjKBCPNbwY" resolve="InterpretativeCommentVariable" />
                </node>
                <node concept="1yR$tW" id="2FjKBCPNbYk" role="1wAG5O" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="2FjKBCPNc3r" role="upBLP">
        <node concept="uGdhv" id="2FjKBCPNc8$" role="16NeZM">
          <node concept="3clFbS" id="2FjKBCPNc8A" role="2VODD2">
            <node concept="3clFbF" id="2FjKBCPNchc" role="3cqZAp">
              <node concept="2OqwBi" id="2FjKBCPNcLb" role="3clFbG">
                <node concept="35c_gC" id="2FjKBCPNchb" role="2Oq$k0">
                  <ref role="35c_gD" to="jn6m:2FjKBCPNbwY" resolve="InterpretativeCommentVariable" />
                </node>
                <node concept="2qgKlT" id="2FjKBCPNdgC" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="2FjKBCPNdzJ" role="upBLP">
        <node concept="uGdhv" id="2FjKBCPNdD5" role="16NL0q">
          <node concept="3clFbS" id="2FjKBCPNdD7" role="2VODD2">
            <node concept="3clFbF" id="2FjKBCPNdLH" role="3cqZAp">
              <node concept="2OqwBi" id="2FjKBCPNehF" role="3clFbG">
                <node concept="35c_gC" id="2FjKBCPNdLG" role="2Oq$k0">
                  <ref role="35c_gD" to="jn6m:2FjKBCPNbwY" resolve="InterpretativeCommentVariable" />
                </node>
                <node concept="2qgKlT" id="2FjKBCPNeLb" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="2FjKBCPNeZN">
    <property role="3GE5qa" value="aspect.patterns" />
    <ref role="aqKnT" to="jn6m:2FjKBCPNeZp" resolve="InterpretativeCommentPattern" />
    <node concept="2VfDsV" id="2FjKBCPNeZO" role="3ft7WO" />
  </node>
  <node concept="PKFIW" id="2FjKBCPNf0j">
    <property role="3GE5qa" value="aspect.variables" />
    <property role="TrG5h" value="Pattern_InterpretativeComment_EditorComponent" />
    <ref role="1XX52x" to="jn6m:2FjKBCPNbwY" resolve="InterpretativeCommentVariable" />
    <node concept="3F1sOY" id="2FjKBCPNf0p" role="2wV5jI">
      <property role="1$x2rV" value="&lt;&lt; ... &gt;&gt;" />
      <ref role="1NtTu8" to="jn6m:2FjKBCPNeZQ" resolve="pattern" />
    </node>
    <node concept="1PE4EZ" id="2FjKBCPNf0n" role="1PM95z">
      <ref role="1PE7su" to="i7z6:6LTgXmNpPu9" resolve="Pattern_AspectVariable_EditorComponent" />
    </node>
  </node>
  <node concept="3p36aQ" id="2FjKBCPOm67">
    <property role="3GE5qa" value="aspect.patterns" />
    <ref role="aqKnT" to="jn6m:2FjKBCPNjKf" resolve="WildcardInterpretativeCommentPattern" />
    <node concept="3eGOop" id="2FjKBCPOm68" role="3ft7WO">
      <node concept="ucgPf" id="2FjKBCPOm69" role="3aKz83">
        <node concept="3clFbS" id="2FjKBCPOm6a" role="2VODD2">
          <node concept="3clFbF" id="2FjKBCPOmbp" role="3cqZAp">
            <node concept="2ShNRf" id="2FjKBCPOmbn" role="3clFbG">
              <node concept="2fJWfE" id="2FjKBCPOmjI" role="2ShVmc">
                <node concept="3Tqbb2" id="2FjKBCPOmjK" role="3zrR0E">
                  <ref role="ehGHo" to="jn6m:2FjKBCPNjKf" resolve="WildcardInterpretativeCommentPattern" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="2FjKBCPOmup" role="upBLP">
        <node concept="uGdhv" id="2FjKBCPOmzN" role="16NeZM">
          <node concept="3clFbS" id="2FjKBCPOmzP" role="2VODD2">
            <node concept="3clFbF" id="2FjKBCPOmGr" role="3cqZAp">
              <node concept="2OqwBi" id="2FjKBCPOncX" role="3clFbG">
                <node concept="35c_gC" id="2FjKBCPOmGq" role="2Oq$k0">
                  <ref role="35c_gD" to="jn6m:2FjKBCPNjKf" resolve="WildcardInterpretativeCommentPattern" />
                </node>
                <node concept="2qgKlT" id="2FjKBCPOnI9" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="2FjKBCPOo1I" role="upBLP">
        <node concept="uGdhv" id="2FjKBCPOo7l" role="16NL0q">
          <node concept="3clFbS" id="2FjKBCPOo7n" role="2VODD2">
            <node concept="3clFbF" id="2FjKBCPOofX" role="3cqZAp">
              <node concept="2OqwBi" id="2FjKBCPOoKv" role="3clFbG">
                <node concept="35c_gC" id="2FjKBCPOofW" role="2Oq$k0">
                  <ref role="35c_gD" to="jn6m:2FjKBCPNjKf" resolve="WildcardInterpretativeCommentPattern" />
                </node>
                <node concept="2qgKlT" id="2FjKBCPOphF" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="2FjKBCPOwsC">
    <property role="3GE5qa" value="aspect.references" />
    <ref role="aqKnT" to="jn6m:2FjKBCPOvmx" resolve="AspectInterpretativeCommentForRequestorReference" />
    <node concept="3XHNnq" id="2FjKBCPOwsD" role="3ft7WO">
      <ref role="3XGfJA" to="jn6m:2FjKBCPOvmz" resolve="target" />
      <node concept="1WAQ3h" id="2FjKBCPOwsF" role="1WZ6hz">
        <node concept="3clFbS" id="2FjKBCPOwsG" role="2VODD2">
          <node concept="3clFbF" id="2FjKBCPOw_h" role="3cqZAp">
            <node concept="2OqwBi" id="2FjKBCPOwUB" role="3clFbG">
              <node concept="1WAUZh" id="2FjKBCPOw_g" role="2Oq$k0" />
              <node concept="2qgKlT" id="2FjKBCPOxko" role="2OqNvi">
                <ref role="37wK5l" to="1yj:4V3GMfXuP$X" resolve="getReferenceDescription" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

