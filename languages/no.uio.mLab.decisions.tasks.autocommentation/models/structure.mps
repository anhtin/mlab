<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:8d706f6f-a342-4d4f-96d8-7be452515eda(no.uio.mLab.decisions.tasks.autocommentation.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="6" />
    <devkit ref="78434eb8-b0e5-444b-850d-e7c4ad2da9ab(jetbrains.mps.devkit.aspect.structure)" />
  </languages>
  <imports>
    <import index="7f9y" ref="r:790ac37e-42d3-4dd3-977c-e8fe9cc56a45(no.uio.mLab.decisions.core.structure)" />
    <import index="vbok" ref="r:6cc208d3-a370-49ca-a6ac-ded97805a924(no.uio.mLab.shared.structure)" />
    <import index="hpcx" ref="r:2530bb31-0779-45e3-bc3f-01507d6b2623(no.uio.mLab.decisions.references.specialistType.structure)" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <property id="4628067390765956807" name="final" index="R5$K2" />
        <property id="4628067390765956802" name="abstract" index="R5$K7" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
      </concept>
      <concept id="1169125989551" name="jetbrains.mps.lang.structure.structure.InterfaceConceptDeclaration" flags="ig" index="PlHQZ">
        <child id="1169127546356" name="extends" index="PrDN$" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599698500" name="specializedLink" index="20ksaX" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="1mAGFBLedHi">
    <property role="EcuMT" value="1560130832616184658" />
    <property role="3GE5qa" value="base.actions.addComment" />
    <property role="TrG5h" value="AddCommentForRequestor" />
    <ref role="1TJDcQ" to="7f9y:1mAGFBKnGHq" resolve="Action" />
    <node concept="1TJgyj" id="1mAGFBLen0G" role="1TKVEi">
      <property role="IQ2ns" value="1560130832616222764" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="comment" />
      <ref role="20lvS9" node="4QUW3ee0Jac" resolve="InterpretativeCommentForRequestor" />
    </node>
    <node concept="PrWs8" id="1Hxyv4EVCP8" role="PzmwI">
      <ref role="PrY4T" node="1Hxyv4EVCOV" resolve="ITranslatableAutoCommentationConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="1mAGFBKnHGH">
    <property role="EcuMT" value="1560130832601897773" />
    <property role="TrG5h" value="NotifySpecialist" />
    <property role="3GE5qa" value="base.actions" />
    <ref role="1TJDcQ" to="7f9y:1mAGFBKnGHq" resolve="Action" />
    <node concept="PrWs8" id="1Hxyv4EVDkf" role="PzmwI">
      <ref role="PrY4T" node="1Hxyv4EVCOV" resolve="ITranslatableAutoCommentationConcept" />
    </node>
    <node concept="1TJgyj" id="4QUW3efwFhq" role="1TKVEi">
      <property role="IQ2ns" value="5601053190830339162" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="specialistReference" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="hpcx:4QUW3efwBDa" resolve="SpecialistTypeReference" />
    </node>
  </node>
  <node concept="PlHQZ" id="1Hxyv4EVCOV">
    <property role="EcuMT" value="1973009780665388347" />
    <property role="TrG5h" value="ITranslatableAutoCommentationConcept" />
    <node concept="PrWs8" id="1Hxyv4EVCP6" role="PrDN$">
      <ref role="PrY4T" to="vbok:5Wfdz$0vc1Z" resolve="ITranslatableConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="4QUW3ee0Het">
    <property role="EcuMT" value="5601053190805181341" />
    <property role="3GE5qa" value="base.actions.addComment.parameters" />
    <property role="TrG5h" value="InterpretativeComment" />
    <property role="R5$K7" value="true" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" to="7f9y:2FjKBCOFjHC" resolve="ActionParameter" />
    <node concept="PrWs8" id="4QUW3ee10jg" role="PzmwI">
      <ref role="PrY4T" node="1Hxyv4EVCOV" resolve="ITranslatableAutoCommentationConcept" />
    </node>
    <node concept="PrWs8" id="1I84Bf7$U4Z" role="PzmwI">
      <ref role="PrY4T" to="7f9y:6LTgXmMs$_4" resolve="IBasePattern" />
    </node>
  </node>
  <node concept="1TIwiD" id="4QUW3ee0Jac">
    <property role="EcuMT" value="5601053190805189260" />
    <property role="3GE5qa" value="base.actions.addComment.parameters" />
    <property role="TrG5h" value="InterpretativeCommentForRequestor" />
    <property role="R5$K7" value="true" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" node="4QUW3ee0Het" resolve="InterpretativeComment" />
  </node>
  <node concept="1TIwiD" id="PDjyzk_uY0">
    <property role="EcuMT" value="966389532319936384" />
    <property role="3GE5qa" value="base.actions.addComment.parameters" />
    <property role="TrG5h" value="TextInterpretativeCommentForRequestor" />
    <ref role="1TJDcQ" node="4QUW3ee0Jac" resolve="InterpretativeCommentForRequestor" />
    <node concept="1TJgyj" id="PDjyzk_uYx" role="1TKVEi">
      <property role="IQ2ns" value="966389532319936417" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="text" />
      <ref role="20lvS9" to="7f9y:5Wfdz$0v7yK" resolve="TextLiteral" />
    </node>
  </node>
  <node concept="1TIwiD" id="2FjKBCOKOcn">
    <property role="EcuMT" value="3086023999805932311" />
    <property role="3GE5qa" value="base.dataValues" />
    <property role="TrG5h" value="HasInterpretativeComment" />
    <ref role="1TJDcQ" to="7f9y:1mAGFBKuqjA" resolve="BooleanDataValue" />
    <node concept="1TJgyj" id="2FjKBCOKOco" role="1TKVEi">
      <property role="IQ2ns" value="3086023999805932312" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="comment" />
      <ref role="20lvS9" node="4QUW3ee0Het" resolve="InterpretativeComment" />
    </node>
    <node concept="PrWs8" id="2FjKBCOKVxU" role="PzmwI">
      <ref role="PrY4T" node="1Hxyv4EVCOV" resolve="ITranslatableAutoCommentationConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="2FjKBCPNbwY">
    <property role="EcuMT" value="3086023999823329342" />
    <property role="3GE5qa" value="aspect.variables" />
    <property role="TrG5h" value="InterpretativeCommentVariable" />
    <ref role="1TJDcQ" to="7f9y:4QUW3edDXqS" resolve="AspectVariable" />
    <node concept="1TJgyj" id="2FjKBCPNeZQ" role="1TKVEi">
      <property role="IQ2ns" value="3086023999823343606" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="pattern" />
      <ref role="20lvS9" node="2FjKBCPNeZp" resolve="InterpretativeCommentPattern" />
      <ref role="20ksaX" to="7f9y:6LTgXmNnjBG" resolve="pattern" />
    </node>
  </node>
  <node concept="1TIwiD" id="2FjKBCPNbwZ">
    <property role="EcuMT" value="3086023999823329343" />
    <property role="3GE5qa" value="aspect.references" />
    <property role="TrG5h" value="AspectInterpretativeCommentReference" />
    <ref role="1TJDcQ" node="4QUW3ee0Het" resolve="InterpretativeComment" />
    <node concept="PrWs8" id="2FjKBCPNbx0" role="PzmwI">
      <ref role="PrY4T" to="7f9y:6LTgXmMAn$t" resolve="IAspectVariableReference" />
    </node>
    <node concept="1TJgyj" id="2FjKBCPOuRg" role="1TKVEi">
      <property role="IQ2ns" value="3086023999823670736" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="target" />
      <ref role="20lvS9" node="2FjKBCPNbwY" resolve="InterpretativeCommentVariable" />
      <ref role="20ksaX" to="7f9y:6LTgXmMAS1z" resolve="target" />
    </node>
  </node>
  <node concept="1TIwiD" id="2FjKBCPNeZp">
    <property role="EcuMT" value="3086023999823343577" />
    <property role="3GE5qa" value="aspect.patterns" />
    <property role="TrG5h" value="InterpretativeCommentPattern" />
    <ref role="1TJDcQ" to="7f9y:6LTgXmNrfh_" resolve="AspectVariablePattern" />
  </node>
  <node concept="1TIwiD" id="2FjKBCPNjKf">
    <property role="EcuMT" value="3086023999823363087" />
    <property role="3GE5qa" value="aspect.patterns" />
    <property role="TrG5h" value="WildcardInterpretativeCommentPattern" />
    <ref role="1TJDcQ" node="2FjKBCPNeZp" resolve="InterpretativeCommentPattern" />
    <node concept="PrWs8" id="2FjKBCPNjKg" role="PzmwI">
      <ref role="PrY4T" to="7f9y:65epL7Ml5Dl" resolve="IWildcardPattern" />
    </node>
  </node>
  <node concept="1TIwiD" id="2FjKBCPOvmx">
    <property role="EcuMT" value="3086023999823672737" />
    <property role="3GE5qa" value="aspect.references" />
    <property role="TrG5h" value="AspectInterpretativeCommentForRequestorReference" />
    <ref role="1TJDcQ" node="4QUW3ee0Jac" resolve="InterpretativeCommentForRequestor" />
    <node concept="PrWs8" id="2FjKBCPOvmy" role="PzmwI">
      <ref role="PrY4T" to="7f9y:6LTgXmMAn$t" resolve="IAspectVariableReference" />
    </node>
    <node concept="1TJgyj" id="2FjKBCPOvmz" role="1TKVEi">
      <property role="IQ2ns" value="3086023999823672739" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="target" />
      <ref role="20lvS9" node="2FjKBCPNbwY" resolve="InterpretativeCommentVariable" />
      <ref role="20ksaX" to="7f9y:6LTgXmMAS1z" resolve="target" />
    </node>
  </node>
</model>

