<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:31cdb065-98c9-4ea1-a600-4ca41da57471(no.uio.mLab.decisions.tasks.autocommentation.typesystem)">
  <persistence version="9" />
  <languages>
    <use id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem" version="1" />
    <devkit ref="00000000-0000-4000-0000-1de82b3a4936(jetbrains.mps.devkit.aspect.typesystem)" />
  </languages>
  <imports>
    <import index="jn6m" ref="r:8d706f6f-a342-4d4f-96d8-7be452515eda(no.uio.mLab.decisions.tasks.autocommentation.structure)" implicit="true" />
    <import index="qs4s" ref="r:21f6a30a-8f3c-4251-80f3-8c6b604f12dc(no.uio.mLab.decisions.tasks.autocommentation.behavior)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
      </concept>
    </language>
    <language id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem">
      <concept id="1175517767210" name="jetbrains.mps.lang.typesystem.structure.ReportErrorStatement" flags="nn" index="2MkqsV">
        <child id="1175517851849" name="errorString" index="2MkJ7o" />
      </concept>
      <concept id="1227096774658" name="jetbrains.mps.lang.typesystem.structure.MessageStatement" flags="ng" index="2OEH$v">
        <child id="1227096802790" name="nodeToReport" index="2OEOjV" />
      </concept>
      <concept id="1195213580585" name="jetbrains.mps.lang.typesystem.structure.AbstractCheckingRule" flags="ig" index="18hYwZ">
        <child id="1195213635060" name="body" index="18ibNy" />
      </concept>
      <concept id="1195214364922" name="jetbrains.mps.lang.typesystem.structure.NonTypesystemRule" flags="ig" index="18kY7G" />
      <concept id="1174642788531" name="jetbrains.mps.lang.typesystem.structure.ConceptReference" flags="ig" index="1YaCAy">
        <reference id="1174642800329" name="concept" index="1YaFvo" />
      </concept>
      <concept id="1174648085619" name="jetbrains.mps.lang.typesystem.structure.AbstractRule" flags="ng" index="1YuPPy">
        <child id="1174648101952" name="applicableNode" index="1YuTPh" />
      </concept>
      <concept id="1174650418652" name="jetbrains.mps.lang.typesystem.structure.ApplicableNodeReference" flags="nn" index="1YBJjd">
        <reference id="1174650432090" name="applicableNode" index="1YBMHb" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="7453996997717780434" name="jetbrains.mps.lang.smodel.structure.Node_GetSConceptOperation" flags="nn" index="2yIwOk" />
      <concept id="1171999116870" name="jetbrains.mps.lang.smodel.structure.Node_IsNullOperation" flags="nn" index="3w_OXm" />
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="18kY7G" id="1mAGFBLeprJ">
    <property role="TrG5h" value="check_AddComment" />
    <property role="3GE5qa" value="base.actions.addComment" />
    <node concept="3clFbS" id="1mAGFBLeprK" role="18ibNy">
      <node concept="3clFbJ" id="1mAGFBLeprQ" role="3cqZAp">
        <node concept="2OqwBi" id="1mAGFBLeq2E" role="3clFbw">
          <node concept="2OqwBi" id="1mAGFBLepAl" role="2Oq$k0">
            <node concept="1YBJjd" id="1mAGFBLeps2" role="2Oq$k0">
              <ref role="1YBMHb" node="1mAGFBLeprM" resolve="addCommentForRequestor" />
            </node>
            <node concept="3TrEf2" id="1mAGFBLFTKX" role="2OqNvi">
              <ref role="3Tt5mk" to="jn6m:1mAGFBLen0G" resolve="comment" />
            </node>
          </node>
          <node concept="3w_OXm" id="1mAGFBLeqdm" role="2OqNvi" />
        </node>
        <node concept="3clFbS" id="1mAGFBLeprS" role="3clFbx">
          <node concept="2MkqsV" id="1mAGFBLeqfk" role="3cqZAp">
            <node concept="2OqwBi" id="1mAGFBLera9" role="2MkJ7o">
              <node concept="2OqwBi" id="1mAGFBLeqpQ" role="2Oq$k0">
                <node concept="1YBJjd" id="1mAGFBLeqfw" role="2Oq$k0">
                  <ref role="1YBMHb" node="1mAGFBLeprM" resolve="addCommentForRequestor" />
                </node>
                <node concept="2yIwOk" id="1mAGFBLeqBP" role="2OqNvi" />
              </node>
              <node concept="2qgKlT" id="1mAGFBLFU3d" role="2OqNvi">
                <ref role="37wK5l" to="qs4s:1mAGFBLen1J" resolve="getMissingComment" />
              </node>
            </node>
            <node concept="1YBJjd" id="1mAGFBLerwZ" role="2OEOjV">
              <ref role="1YBMHb" node="1mAGFBLeprM" resolve="addCommentForRequestor" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="1mAGFBLeprM" role="1YuTPh">
      <property role="TrG5h" value="addCommentForRequestor" />
      <ref role="1YaFvo" to="jn6m:1mAGFBLedHi" resolve="AddCommentForRequestor" />
    </node>
  </node>
</model>

