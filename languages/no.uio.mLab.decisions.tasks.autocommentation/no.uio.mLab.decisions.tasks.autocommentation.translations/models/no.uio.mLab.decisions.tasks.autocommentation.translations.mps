<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:9f6c7de2-64a9-454f-9a9b-110883f340fd(no.uio.mLab.decisions.tasks.autocommentation.translations)">
  <persistence version="9" />
  <languages>
    <use id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage" version="6" />
  </languages>
  <imports>
    <import index="9igz" ref="r:0ba33904-c124-449b-bde4-640f55a71953(no.uio.mLab.shared.translations)" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1465982738277781862" name="jetbrains.mps.baseLanguage.structure.PlaceholderMember" flags="ng" index="2tJIrI" />
      <concept id="1188207840427" name="jetbrains.mps.baseLanguage.structure.AnnotationInstance" flags="nn" index="2AHcQZ">
        <reference id="1188208074048" name="annotation" index="2AI5Lk" />
      </concept>
      <concept id="1188208481402" name="jetbrains.mps.baseLanguage.structure.HasAnnotation" flags="ng" index="2AJDlI">
        <child id="1188208488637" name="annotation" index="2AJF6D" />
      </concept>
      <concept id="2820489544401957797" name="jetbrains.mps.baseLanguage.structure.DefaultClassCreator" flags="nn" index="HV5vD">
        <reference id="2820489544401957798" name="classifier" index="HV5vE" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1070462154015" name="jetbrains.mps.baseLanguage.structure.StaticFieldDeclaration" flags="ig" index="Wx3nA">
        <property id="6468716278899126575" name="isVolatile" index="2dlcS1" />
        <property id="6468716278899125786" name="isTransient" index="2dld4O" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1081236700938" name="jetbrains.mps.baseLanguage.structure.StaticMethodDeclaration" flags="ig" index="2YIFZL" />
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu">
        <child id="1095933932569" name="implementedInterface" index="EKbjA" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <property id="1176718929932" name="isFinal" index="3TUv4t" />
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <property id="4276006055363816570" name="isSynchronized" index="od$2w" />
        <property id="1181808852946" name="isFinal" index="DiZV1" />
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123165" name="jetbrains.mps.baseLanguage.structure.InstanceMethodDeclaration" flags="ig" index="3clFb_">
        <property id="1178608670077" name="isAbstract" index="1EzhhJ" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1107461130800" name="jetbrains.mps.baseLanguage.structure.Classifier" flags="ng" index="3pOWGL">
        <child id="5375687026011219971" name="member" index="jymVt" unordered="true" />
      </concept>
      <concept id="7812454656619025416" name="jetbrains.mps.baseLanguage.structure.MethodDeclaration" flags="ng" index="1rXfSm">
        <property id="8355037393041754995" name="isNative" index="2aFKle" />
      </concept>
      <concept id="7812454656619025412" name="jetbrains.mps.baseLanguage.structure.LocalMethodCall" flags="nn" index="1rXfSq" />
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1107796713796" name="jetbrains.mps.baseLanguage.structure.Interface" flags="ig" index="3HP615" />
      <concept id="1163670490218" name="jetbrains.mps.baseLanguage.structure.SwitchStatement" flags="nn" index="3KaCP$">
        <child id="1163670592366" name="defaultBlock" index="3Kb1Dw" />
        <child id="1163670766145" name="expression" index="3KbGdf" />
        <child id="1163670772911" name="case" index="3KbHQx" />
      </concept>
      <concept id="1163670641947" name="jetbrains.mps.baseLanguage.structure.SwitchCase" flags="ng" index="3KbdKl">
        <child id="1163670677455" name="expression" index="3Kbmr1" />
        <child id="1163670683720" name="body" index="3Kbo56" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
      <concept id="1146644623116" name="jetbrains.mps.baseLanguage.structure.PrivateVisibility" flags="nn" index="3Tm6S6" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="312cEu" id="4zMac8rUO1O">
    <property role="TrG5h" value="NoAutoCommentationTranslations" />
    <property role="3GE5qa" value="localization" />
    <node concept="3clFb_" id="2FjKBCOKTdk" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getHasInterpretativeCommentAlias" />
      <node concept="3Tm1VV" id="2FjKBCOKTdm" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCOKTdn" role="3clF45" />
      <node concept="3clFbS" id="2FjKBCOKTdo" role="3clF47">
        <node concept="3clFbF" id="2FjKBCOKUH0" role="3cqZAp">
          <node concept="Xl_RD" id="2FjKBCOKUGZ" role="3clFbG">
            <property role="Xl_RC" value="har kommentar" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="2FjKBCOKTdp" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="2FjKBCOKTdq" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getHasInterpretativeCommentDescription" />
      <node concept="3Tm1VV" id="2FjKBCOKTds" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCOKTdt" role="3clF45" />
      <node concept="3clFbS" id="2FjKBCOKTdu" role="3clF47">
        <node concept="3clFbF" id="2FjKBCOKUIH" role="3cqZAp">
          <node concept="Xl_RD" id="2FjKBCOKUIG" role="3clFbG">
            <property role="Xl_RC" value="sjekk om kommentar er lagt til" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="2FjKBCOKTdv" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="2FjKBCOKUy3" role="jymVt" />
    <node concept="3clFb_" id="5ZQBr_XMocX" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getAddCommentForRequestorAlias" />
      <node concept="3Tm1VV" id="5ZQBr_XMocZ" role="1B3o_S" />
      <node concept="17QB3L" id="5ZQBr_XMod0" role="3clF45" />
      <node concept="3clFbS" id="5ZQBr_XMod1" role="3clF47">
        <node concept="3clFbF" id="5ZQBr_XMorp" role="3cqZAp">
          <node concept="Xl_RD" id="5ZQBr_XMoro" role="3clFbG">
            <property role="Xl_RC" value="legg til kommentar" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="5ZQBr_XMod2" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="5ZQBr_XMod3" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getAddCommentForRequestorDescription" />
      <node concept="3Tm1VV" id="5ZQBr_XMod5" role="1B3o_S" />
      <node concept="17QB3L" id="5ZQBr_XMod6" role="3clF45" />
      <node concept="3clFbS" id="5ZQBr_XMod7" role="3clF47">
        <node concept="3clFbF" id="5ZQBr_XMosv" role="3cqZAp">
          <node concept="Xl_RD" id="5ZQBr_XMosu" role="3clFbG">
            <property role="Xl_RC" value="legg ved en kommentar til rekvirent" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="5ZQBr_XMod8" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="1mAGFBLenn9" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getAddCommentForRequestorMissingCommentError" />
      <node concept="3Tm1VV" id="1mAGFBLennb" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBLennc" role="3clF45" />
      <node concept="3clFbS" id="1mAGFBLennd" role="3clF47">
        <node concept="3clFbF" id="1mAGFBLenxC" role="3cqZAp">
          <node concept="Xl_RD" id="1mAGFBLenxB" role="3clFbG">
            <property role="Xl_RC" value="mangler kommentar" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="1mAGFBLenne" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="PDjyzk_xmD" role="jymVt" />
    <node concept="3clFb_" id="PDjyzk_xti" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getTextInterpretativeCommentAlias" />
      <node concept="3Tm1VV" id="PDjyzk_xtk" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzk_xtl" role="3clF45" />
      <node concept="3clFbS" id="PDjyzk_xtm" role="3clF47">
        <node concept="3clFbF" id="PDjyzk_xBT" role="3cqZAp">
          <node concept="Xl_RD" id="PDjyzk_xBS" role="3clFbG">
            <property role="Xl_RC" value="tekstkommentar" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzk_xtn" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzk_xto" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getTextInterpretativeCommentDescription" />
      <node concept="3Tm1VV" id="PDjyzk_xtq" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzk_xtr" role="3clF45" />
      <node concept="3clFbS" id="PDjyzk_xts" role="3clF47">
        <node concept="3clFbF" id="PDjyzk_xCZ" role="3cqZAp">
          <node concept="Xl_RD" id="PDjyzk_xCY" role="3clFbG">
            <property role="Xl_RC" value="fritekst kommentar" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzk_xtt" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="17XAtu7ZOJZ" role="jymVt" />
    <node concept="3clFb_" id="17XAtu7ZNzG" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getNotifySpecialistAlias" />
      <node concept="3Tm1VV" id="17XAtu7ZNzI" role="1B3o_S" />
      <node concept="17QB3L" id="17XAtu7ZNzJ" role="3clF45" />
      <node concept="3clFbS" id="17XAtu7ZNzK" role="3clF47">
        <node concept="3clFbF" id="17XAtu7ZPLq" role="3cqZAp">
          <node concept="Xl_RD" id="17XAtu7ZPLp" role="3clFbG">
            <property role="Xl_RC" value="varsle lege" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="17XAtu7ZNzL" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="17XAtu7ZNzM" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getNotifySpecialistDesciption" />
      <node concept="3Tm1VV" id="17XAtu7ZNzO" role="1B3o_S" />
      <node concept="17QB3L" id="17XAtu7ZNzP" role="3clF45" />
      <node concept="3clFbS" id="17XAtu7ZNzQ" role="3clF47">
        <node concept="3clFbF" id="17XAtu7ZPMy" role="3cqZAp">
          <node concept="Xl_RD" id="17XAtu7ZPPQ" role="3clFbG">
            <property role="Xl_RC" value="be lege om å tolke analysesvar" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="17XAtu7ZNzR" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="1mAGFBLd8XY" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getNotifySpecialistMissingSpecialistTypeError" />
      <node concept="3Tm1VV" id="1mAGFBLd8Y0" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBLd8Y1" role="3clF45" />
      <node concept="3clFbS" id="1mAGFBLd8Y2" role="3clF47">
        <node concept="3clFbF" id="1mAGFBLd97d" role="3cqZAp">
          <node concept="Xl_RD" id="1mAGFBLd97c" role="3clFbG">
            <property role="Xl_RC" value="mangler konsulentype" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="1mAGFBLd8Y3" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3Tm1VV" id="4zMac8rUO1P" role="1B3o_S" />
    <node concept="3uibUv" id="4zMac8rUO2g" role="EKbjA">
      <ref role="3uigEE" node="4zMac8rUNtP" resolve="IAutoCommentationTranslations" />
    </node>
  </node>
  <node concept="312cEu" id="4zMac8rUO0_">
    <property role="TrG5h" value="EnAutoCommentationTranslations" />
    <property role="3GE5qa" value="localization" />
    <node concept="3clFb_" id="2FjKBCOKSCI" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getHasInterpretativeCommentAlias" />
      <node concept="3Tm1VV" id="2FjKBCOKSCK" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCOKSCL" role="3clF45" />
      <node concept="3clFbS" id="2FjKBCOKSCM" role="3clF47">
        <node concept="3clFbF" id="2FjKBCOKT0_" role="3cqZAp">
          <node concept="Xl_RD" id="2FjKBCOKT0$" role="3clFbG">
            <property role="Xl_RC" value="has interpretative comment" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="2FjKBCOKSCN" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="2FjKBCOKSCO" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getHasInterpretativeCommentDescription" />
      <node concept="3Tm1VV" id="2FjKBCOKSCQ" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCOKSCR" role="3clF45" />
      <node concept="3clFbS" id="2FjKBCOKSCS" role="3clF47">
        <node concept="3clFbF" id="2FjKBCOKT1S" role="3cqZAp">
          <node concept="Xl_RD" id="2FjKBCOKT1R" role="3clFbG">
            <property role="Xl_RC" value="check if comment has been added" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="2FjKBCOKSCT" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="2FjKBCOKSvV" role="jymVt" />
    <node concept="3clFb_" id="17XAtu8bhKf" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getAddCommentForRequestorAlias" />
      <node concept="3Tm1VV" id="17XAtu8bhKh" role="1B3o_S" />
      <node concept="17QB3L" id="17XAtu8bhKi" role="3clF45" />
      <node concept="3clFbS" id="17XAtu8bhKj" role="3clF47">
        <node concept="3clFbF" id="17XAtu8bjpB" role="3cqZAp">
          <node concept="Xl_RD" id="17XAtu8bjpA" role="3clFbG">
            <property role="Xl_RC" value="add comment" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="17XAtu8bhKk" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="17XAtu8bhKl" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getAddCommentForRequestorDescription" />
      <node concept="3Tm1VV" id="17XAtu8bhKn" role="1B3o_S" />
      <node concept="17QB3L" id="17XAtu8bhKo" role="3clF45" />
      <node concept="3clFbS" id="17XAtu8bhKp" role="3clF47">
        <node concept="3clFbF" id="17XAtu8bjrd" role="3cqZAp">
          <node concept="Xl_RD" id="17XAtu8bjrc" role="3clFbG">
            <property role="Xl_RC" value="write comment for prescriber" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="17XAtu8bhKq" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="1mAGFBLenP2" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getAddCommentForRequestorMissingCommentError" />
      <node concept="3Tm1VV" id="1mAGFBLenP4" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBLenP5" role="3clF45" />
      <node concept="3clFbS" id="1mAGFBLenP6" role="3clF47">
        <node concept="3clFbF" id="1mAGFBLeo8m" role="3cqZAp">
          <node concept="Xl_RD" id="1mAGFBLeo8l" role="3clFbG">
            <property role="Xl_RC" value="missing comment" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="1mAGFBLenP7" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="PDjyzk_wTS" role="jymVt" />
    <node concept="3clFb_" id="PDjyzk_x0x" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getTextInterpretativeCommentAlias" />
      <node concept="3Tm1VV" id="PDjyzk_x0z" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzk_x0$" role="3clF45" />
      <node concept="3clFbS" id="PDjyzk_x0_" role="3clF47">
        <node concept="3clFbF" id="PDjyzk_xb8" role="3cqZAp">
          <node concept="Xl_RD" id="PDjyzk_xb7" role="3clFbG">
            <property role="Xl_RC" value="text comment" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzk_x0A" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="PDjyzk_x0B" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getTextInterpretativeCommentDescription" />
      <node concept="3Tm1VV" id="PDjyzk_x0D" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzk_x0E" role="3clF45" />
      <node concept="3clFbS" id="PDjyzk_x0F" role="3clF47">
        <node concept="3clFbF" id="PDjyzk_xbO" role="3cqZAp">
          <node concept="Xl_RD" id="PDjyzk_xbN" role="3clFbG">
            <property role="Xl_RC" value="arbitrary text comment" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="PDjyzk_x0G" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="17XAtu8bjsk" role="jymVt" />
    <node concept="3clFb_" id="17XAtu8bhKr" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getNotifySpecialistAlias" />
      <node concept="3Tm1VV" id="17XAtu8bhKt" role="1B3o_S" />
      <node concept="17QB3L" id="17XAtu8bhKu" role="3clF45" />
      <node concept="3clFbS" id="17XAtu8bhKv" role="3clF47">
        <node concept="3clFbF" id="17XAtu8bkFV" role="3cqZAp">
          <node concept="Xl_RD" id="17XAtu8bkFU" role="3clFbG">
            <property role="Xl_RC" value="notify specialist" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="17XAtu8bhKw" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="17XAtu8bhKx" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getNotifySpecialistDesciption" />
      <node concept="3Tm1VV" id="17XAtu8bhKz" role="1B3o_S" />
      <node concept="17QB3L" id="17XAtu8bhK$" role="3clF45" />
      <node concept="3clFbS" id="17XAtu8bhK_" role="3clF47">
        <node concept="3clFbF" id="17XAtu8bkIJ" role="3cqZAp">
          <node concept="Xl_RD" id="17XAtu8bkII" role="3clFbG">
            <property role="Xl_RC" value="notify a specialist to interpret test result" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="17XAtu8bhKA" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="1mAGFBLd8G8" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getNotifySpecialistMissingSpecialistTypeError" />
      <node concept="3Tm1VV" id="1mAGFBLd8Ga" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBLd8Gb" role="3clF45" />
      <node concept="3clFbS" id="1mAGFBLd8Gc" role="3clF47">
        <node concept="3clFbF" id="1mAGFBLd8Pn" role="3cqZAp">
          <node concept="Xl_RD" id="1mAGFBLd8Pm" role="3clFbG">
            <property role="Xl_RC" value="missing specialist type" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="1mAGFBLd8Gd" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3Tm1VV" id="4zMac8rUO0A" role="1B3o_S" />
    <node concept="3uibUv" id="4zMac8rUO1F" role="EKbjA">
      <ref role="3uigEE" node="4zMac8rUNtP" resolve="IAutoCommentationTranslations" />
    </node>
  </node>
  <node concept="312cEu" id="4zMac8rUNsN">
    <property role="TrG5h" value="AutoCommentationTranslationProvider" />
    <property role="3GE5qa" value="internationalization" />
    <node concept="Wx3nA" id="4zMac8rVBeK" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="TrG5h" value="displayTranslations" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm1VV" id="4zMac8rVBei" role="1B3o_S" />
      <node concept="3uibUv" id="4zMac8rVBeC" role="1tU5fm">
        <ref role="3uigEE" node="4zMac8rUNtP" resolve="IAutoCommentationTranslations" />
      </node>
      <node concept="1rXfSq" id="4zMac8rVBfh" role="33vP2m">
        <ref role="37wK5l" node="4zMac8rUNuE" resolve="init" />
        <node concept="10M0yZ" id="ofcHXfXsNF" role="37wK5m">
          <ref role="3cqZAo" to="9igz:5fXsrCVmzVm" resolve="displayLanguage" />
          <ref role="1PxDUh" to="9igz:5fXsrCVmxTN" resolve="TranslationConfiguration" />
        </node>
      </node>
    </node>
    <node concept="Wx3nA" id="1Hxyv4EVCxs" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="TrG5h" value="generationTranslations" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm1VV" id="1Hxyv4EVCxt" role="1B3o_S" />
      <node concept="3uibUv" id="1Hxyv4EVCxu" role="1tU5fm">
        <ref role="3uigEE" node="4zMac8rUNtP" resolve="IAutoCommentationTranslations" />
      </node>
      <node concept="1rXfSq" id="1Hxyv4EVCxv" role="33vP2m">
        <ref role="37wK5l" node="4zMac8rUNuE" resolve="init" />
        <node concept="10M0yZ" id="ofcHXfXsON" role="37wK5m">
          <ref role="3cqZAo" to="9igz:5fXsrCVmzVN" resolve="generationLanguage" />
          <ref role="1PxDUh" to="9igz:5fXsrCVmxTN" resolve="TranslationConfiguration" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="4zMac8rUNtp" role="jymVt" />
    <node concept="2YIFZL" id="4zMac8rUNuE" role="jymVt">
      <property role="TrG5h" value="init" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="4zMac8rUNuH" role="3clF47">
        <node concept="3KaCP$" id="4zMac8rUNv7" role="3cqZAp">
          <node concept="37vLTw" id="1Hxyv4EVExx" role="3KbGdf">
            <ref role="3cqZAo" node="1Hxyv4EVEq8" resolve="language" />
          </node>
          <node concept="3KbdKl" id="4zMac8rUNXu" role="3KbHQx">
            <node concept="Xl_RD" id="4zMac8rUNXU" role="3Kbmr1">
              <property role="Xl_RC" value="no" />
            </node>
            <node concept="3clFbS" id="4zMac8rUNXw" role="3Kbo56">
              <node concept="3cpWs6" id="4zMac8rUNYt" role="3cqZAp">
                <node concept="2ShNRf" id="4zMac8rUO3y" role="3cqZAk">
                  <node concept="HV5vD" id="4zMac8rUP5z" role="2ShVmc">
                    <ref role="HV5vE" node="4zMac8rUO1O" resolve="NoAutoCommentationTranslations" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="4zMac8rUNYX" role="3Kb1Dw">
            <node concept="3cpWs6" id="4zMac8rUNZC" role="3cqZAp">
              <node concept="2ShNRf" id="4zMac8rUP6U" role="3cqZAk">
                <node concept="HV5vD" id="4zMac8rUPap" role="2ShVmc">
                  <ref role="HV5vE" node="4zMac8rUO0_" resolve="EnAutoCommentationTranslations" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="4zMac8rW809" role="1B3o_S" />
      <node concept="3uibUv" id="4zMac8rUNuz" role="3clF45">
        <ref role="3uigEE" node="4zMac8rUNtP" resolve="IAutoCommentationTranslations" />
      </node>
      <node concept="37vLTG" id="1Hxyv4EVEq8" role="3clF46">
        <property role="TrG5h" value="language" />
        <node concept="17QB3L" id="1Hxyv4EVEq7" role="1tU5fm" />
      </node>
    </node>
    <node concept="3Tm1VV" id="4zMac8rUNsO" role="1B3o_S" />
  </node>
  <node concept="3HP615" id="4zMac8rUNtP">
    <property role="TrG5h" value="IAutoCommentationTranslations" />
    <property role="3GE5qa" value="internationalization" />
    <node concept="3clFb_" id="2FjKBCOKScz" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getHasInterpretativeCommentAlias" />
      <node concept="3clFbS" id="2FjKBCOKScA" role="3clF47" />
      <node concept="3Tm1VV" id="2FjKBCOKScB" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCOKSbo" role="3clF45" />
    </node>
    <node concept="3clFb_" id="2FjKBCOKSib" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getHasInterpretativeCommentDescription" />
      <node concept="3clFbS" id="2FjKBCOKSie" role="3clF47" />
      <node concept="3Tm1VV" id="2FjKBCOKSif" role="1B3o_S" />
      <node concept="17QB3L" id="2FjKBCOKSgR" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="2FjKBCOKSfA" role="jymVt" />
    <node concept="3clFb_" id="5ZQBr_XMo4l" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getAddCommentForRequestorAlias" />
      <node concept="3clFbS" id="5ZQBr_XMo4o" role="3clF47" />
      <node concept="3Tm1VV" id="5ZQBr_XMo4p" role="1B3o_S" />
      <node concept="17QB3L" id="5ZQBr_XMo4c" role="3clF45" />
    </node>
    <node concept="3clFb_" id="5ZQBr_XMo5N" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getAddCommentForRequestorDescription" />
      <node concept="3clFbS" id="5ZQBr_XMo5Q" role="3clF47" />
      <node concept="3Tm1VV" id="5ZQBr_XMo5R" role="1B3o_S" />
      <node concept="17QB3L" id="5ZQBr_XMo5y" role="3clF45" />
    </node>
    <node concept="3clFb_" id="1mAGFBLen9Q" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getAddCommentForRequestorMissingCommentError" />
      <node concept="3clFbS" id="1mAGFBLen9T" role="3clF47" />
      <node concept="3Tm1VV" id="1mAGFBLen9U" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBLen8N" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="PDjyzk_wDV" role="jymVt" />
    <node concept="3clFb_" id="PDjyzk_wG9" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getTextInterpretativeCommentAlias" />
      <node concept="3clFbS" id="PDjyzk_wGc" role="3clF47" />
      <node concept="3Tm1VV" id="PDjyzk_wGd" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzk_wFe" role="3clF45" />
    </node>
    <node concept="3clFb_" id="PDjyzk_wK7" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getTextInterpretativeCommentDescription" />
      <node concept="3clFbS" id="PDjyzk_wKa" role="3clF47" />
      <node concept="3Tm1VV" id="PDjyzk_wKb" role="1B3o_S" />
      <node concept="17QB3L" id="PDjyzk_wJ4" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="17XAtu7ZLgl" role="jymVt" />
    <node concept="3clFb_" id="17XAtu7ZLHc" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getNotifySpecialistAlias" />
      <node concept="3clFbS" id="17XAtu7ZLHf" role="3clF47" />
      <node concept="3Tm1VV" id="17XAtu7ZLHg" role="1B3o_S" />
      <node concept="17QB3L" id="17XAtu7ZLyp" role="3clF45" />
    </node>
    <node concept="3clFb_" id="17XAtu7ZMvd" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getNotifySpecialistDesciption" />
      <node concept="3clFbS" id="17XAtu7ZMvg" role="3clF47" />
      <node concept="3Tm1VV" id="17XAtu7ZMvh" role="1B3o_S" />
      <node concept="17QB3L" id="17XAtu7ZMke" role="3clF45" />
    </node>
    <node concept="3clFb_" id="1mAGFBLd8vK" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getNotifySpecialistMissingSpecialistTypeError" />
      <node concept="3clFbS" id="1mAGFBLd8vN" role="3clF47" />
      <node concept="3Tm1VV" id="1mAGFBLd8vO" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBLd8uP" role="3clF45" />
    </node>
    <node concept="3Tm1VV" id="4zMac8rUNtQ" role="1B3o_S" />
  </node>
</model>

