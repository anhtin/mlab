package no.uio.mLab.decisions.tasks.autocommentation.behavior;

/*Generated by MPS */

import jetbrains.mps.core.aspects.behaviour.BaseBHDescriptor;
import org.jetbrains.mps.openapi.language.SAbstractConcept;
import jetbrains.mps.smodel.adapter.structure.MetaAdapterFactory;
import jetbrains.mps.core.aspects.behaviour.api.BehaviorRegistry;
import jetbrains.mps.smodel.language.ConceptRegistry;
import jetbrains.mps.core.aspects.behaviour.api.SMethod;
import jetbrains.mps.core.aspects.behaviour.SMethodBuilder;
import jetbrains.mps.core.aspects.behaviour.SJavaCompoundTypeImpl;
import jetbrains.mps.core.aspects.behaviour.SModifiersImpl;
import jetbrains.mps.core.aspects.behaviour.AccessPrivileges;
import org.jetbrains.mps.openapi.language.SConcept;
import java.util.List;
import java.util.Arrays;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.mps.openapi.model.SNode;
import jetbrains.mps.core.aspects.behaviour.api.SConstructor;
import org.jetbrains.annotations.Nullable;
import jetbrains.mps.core.aspects.behaviour.api.BHMethodNotFoundException;

public final class WildcardInterpretativeCommentPattern__BehaviorDescriptor extends BaseBHDescriptor {
  private static final SAbstractConcept CONCEPT = MetaAdapterFactory.getConcept(0xebe710b88f464b5eL, 0x8fb1221de61f9e7cL, 0x2ad3c27a35cd3c0fL, "no.uio.mLab.decisions.tasks.autocommentation.structure.WildcardInterpretativeCommentPattern");
  private static final BehaviorRegistry REGISTRY = ConceptRegistry.getInstance().getBehaviorRegistry();

  public static final SMethod<String> getDisplayDescription_id5Wfdz$0vc3v = new SMethodBuilder<String>(new SJavaCompoundTypeImpl(String.class)).name("getDisplayDescription").modifiers(SModifiersImpl.create(9, AccessPrivileges.PUBLIC)).concept(CONCEPT).id("5Wfdz$0vc3v").registry(REGISTRY).build();
  public static final SMethod<Boolean> hasPattern_id6LTgXmNRv0n = new SMethodBuilder<Boolean>(new SJavaCompoundTypeImpl(Boolean.TYPE)).name("hasPattern").modifiers(SModifiersImpl.create(8, AccessPrivileges.PUBLIC)).concept(CONCEPT).id("6LTgXmNRv0n").registry(REGISTRY).build();
  public static final SMethod<SConcept> getMatchActionParameter_id2FjKBCPNnnK = new SMethodBuilder<SConcept>(new SJavaCompoundTypeImpl((Class<SConcept>) ((Class) Object.class))).name("getMatchActionParameter").modifiers(SModifiersImpl.create(8, AccessPrivileges.PUBLIC)).concept(CONCEPT).id("2FjKBCPNnnK").registry(REGISTRY).build();

  private static final List<SMethod<?>> BH_METHODS = Arrays.<SMethod<?>>asList(getDisplayDescription_id5Wfdz$0vc3v, hasPattern_id6LTgXmNRv0n, getMatchActionParameter_id2FjKBCPNnnK);

  private static void ___init___(@NotNull SNode __thisNode__) {
  }

  /*package*/ static String getDisplayDescription_id5Wfdz$0vc3v(@NotNull SAbstractConcept __thisConcept__) {
    return "any interpretative comment";
  }
  /*package*/ static boolean hasPattern_id6LTgXmNRv0n(@NotNull SNode __thisNode__) {
    return true;
  }
  /*package*/ static SConcept getMatchActionParameter_id2FjKBCPNnnK(@NotNull SNode __thisNode__) {
    return MetaAdapterFactory.getConcept(0xebe710b88f464b5eL, 0x8fb1221de61f9e7cL, 0x4dbaf0338e02d39dL, "no.uio.mLab.decisions.tasks.autocommentation.structure.InterpretativeComment");
  }

  /*package*/ WildcardInterpretativeCommentPattern__BehaviorDescriptor() {
    super(REGISTRY);
  }

  @Override
  protected void initNode(@NotNull SNode node, @NotNull SConstructor constructor, @Nullable Object[] parameters) {
    ___init___(node);
  }

  @Override
  protected <T> T invokeSpecial0(@NotNull SNode node, @NotNull SMethod<T> method, @Nullable Object[] parameters) {
    int methodIndex = BH_METHODS.indexOf(method);
    if (methodIndex < 0) {
      throw new BHMethodNotFoundException(this, method);
    }
    switch (methodIndex) {
      case 1:
        return (T) ((Boolean) hasPattern_id6LTgXmNRv0n(node));
      case 2:
        return (T) ((SConcept) getMatchActionParameter_id2FjKBCPNnnK(node));
      default:
        throw new BHMethodNotFoundException(this, method);
    }
  }

  @Override
  protected <T> T invokeSpecial0(@NotNull SAbstractConcept concept, @NotNull SMethod<T> method, @Nullable Object[] parameters) {
    int methodIndex = BH_METHODS.indexOf(method);
    if (methodIndex < 0) {
      throw new BHMethodNotFoundException(this, method);
    }
    switch (methodIndex) {
      case 0:
        return (T) ((String) getDisplayDescription_id5Wfdz$0vc3v(concept));
      default:
        throw new BHMethodNotFoundException(this, method);
    }
  }

  @NotNull
  @Override
  public List<SMethod<?>> getDeclaredMethods() {
    return BH_METHODS;
  }

  @NotNull
  @Override
  public SAbstractConcept getConcept() {
    return CONCEPT;
  }
}
