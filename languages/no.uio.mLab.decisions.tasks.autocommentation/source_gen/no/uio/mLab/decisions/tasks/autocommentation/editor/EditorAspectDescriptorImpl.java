package no.uio.mLab.decisions.tasks.autocommentation.editor;

/*Generated by MPS */

import jetbrains.mps.nodeEditor.EditorAspectDescriptorBase;
import org.jetbrains.annotations.NotNull;
import java.util.Collection;
import jetbrains.mps.openapi.editor.descriptor.ConceptEditor;
import org.jetbrains.mps.openapi.language.SAbstractConcept;
import java.util.Collections;
import jetbrains.mps.openapi.editor.descriptor.ConceptEditorComponent;
import jetbrains.mps.openapi.editor.descriptor.SubstituteMenu;
import jetbrains.mps.lang.smodel.ConceptSwitchIndex;
import jetbrains.mps.lang.smodel.ConceptSwitchIndexBuilder;
import jetbrains.mps.smodel.adapter.ids.MetaIdFactory;

public class EditorAspectDescriptorImpl extends EditorAspectDescriptorBase {
  @NotNull
  public Collection<ConceptEditor> getDeclaredEditors(SAbstractConcept concept) {
    SAbstractConcept cncpt = ((SAbstractConcept) concept);
    switch (conceptIndex.index(cncpt)) {
      case 0:
        return Collections.<ConceptEditor>singletonList(new AddCommentForRequestor_Editor());
      case 1:
        return Collections.<ConceptEditor>singletonList(new HasInterpretativeComment_Editor());
      case 2:
        return Collections.<ConceptEditor>singletonList(new NotifySpecialist_Editor());
      case 3:
        return Collections.<ConceptEditor>singletonList(new TextInterpretativeCommentForRequestor_Editor());
      default:
    }
    return Collections.<ConceptEditor>emptyList();
  }

  @NotNull
  public Collection<ConceptEditorComponent> getDeclaredEditorComponents(SAbstractConcept concept, String editorComponentId) {
    SAbstractConcept cncpt = ((SAbstractConcept) concept);
    switch (conceptIndex1.index(cncpt)) {
      case 0:
        if (true) {
          if ("no.uio.mLab.decisions.core.editor.Pattern_AspectVariable_EditorComponent".equals(editorComponentId)) {
            return Collections.<ConceptEditorComponent>singletonList(new Pattern_InterpretativeComment_EditorComponent());
          }
        }
        break;
      default:
    }
    return Collections.<ConceptEditorComponent>emptyList();
  }

  @NotNull
  @Override
  public Collection<SubstituteMenu> getDeclaredDefaultSubstituteMenus(SAbstractConcept concept) {
    SAbstractConcept cncpt = concept;
    switch (conceptIndex2.index(cncpt)) {
      case 0:
        return Collections.<SubstituteMenu>singletonList(new AddCommentForRequestor_SubstituteMenu());
      case 1:
        return Collections.<SubstituteMenu>singletonList(new AspectInterpretativeCommentForRequestorReference_SubstituteMenu());
      case 2:
        return Collections.<SubstituteMenu>singletonList(new HasInterpretativeComment_SubstituteMenu());
      case 3:
        return Collections.<SubstituteMenu>singletonList(new InterpretativeCommentPattern_SubstituteMenu());
      case 4:
        return Collections.<SubstituteMenu>singletonList(new InterpretativeCommentVariable_SubstituteMenu());
      case 5:
        return Collections.<SubstituteMenu>singletonList(new NotifySpecialist_SubstituteMenu());
      case 6:
        return Collections.<SubstituteMenu>singletonList(new TextInterpretativeCommentForRequestor_SubstituteMenu());
      case 7:
        return Collections.<SubstituteMenu>singletonList(new WildcardInterpretativeCommentPattern_SubstituteMenu());
      default:
    }
    return Collections.<SubstituteMenu>emptyList();
  }

  private static final ConceptSwitchIndex conceptIndex = new ConceptSwitchIndexBuilder().put(MetaIdFactory.conceptId(0xebe710b88f464b5eL, 0x8fb1221de61f9e7cL, 0x15a6b2b9f138db52L), MetaIdFactory.conceptId(0xebe710b88f464b5eL, 0x8fb1221de61f9e7cL, 0x2ad3c27a34c34317L), MetaIdFactory.conceptId(0xebe710b88f464b5eL, 0x8fb1221de61f9e7cL, 0x15a6b2b9f05edb2dL), MetaIdFactory.conceptId(0xebe710b88f464b5eL, 0x8fb1221de61f9e7cL, 0xd694e28d495ef80L)).seal();
  private static final ConceptSwitchIndex conceptIndex1 = new ConceptSwitchIndexBuilder().put(MetaIdFactory.conceptId(0xebe710b88f464b5eL, 0x8fb1221de61f9e7cL, 0x2ad3c27a35ccb83eL)).seal();
  private static final ConceptSwitchIndex conceptIndex2 = new ConceptSwitchIndexBuilder().put(MetaIdFactory.conceptId(0xebe710b88f464b5eL, 0x8fb1221de61f9e7cL, 0x15a6b2b9f138db52L), MetaIdFactory.conceptId(0xebe710b88f464b5eL, 0x8fb1221de61f9e7cL, 0x2ad3c27a35d1f5a1L), MetaIdFactory.conceptId(0xebe710b88f464b5eL, 0x8fb1221de61f9e7cL, 0x2ad3c27a34c34317L), MetaIdFactory.conceptId(0xebe710b88f464b5eL, 0x8fb1221de61f9e7cL, 0x2ad3c27a35ccefd9L), MetaIdFactory.conceptId(0xebe710b88f464b5eL, 0x8fb1221de61f9e7cL, 0x2ad3c27a35ccb83eL), MetaIdFactory.conceptId(0xebe710b88f464b5eL, 0x8fb1221de61f9e7cL, 0x15a6b2b9f05edb2dL), MetaIdFactory.conceptId(0xebe710b88f464b5eL, 0x8fb1221de61f9e7cL, 0xd694e28d495ef80L), MetaIdFactory.conceptId(0xebe710b88f464b5eL, 0x8fb1221de61f9e7cL, 0x2ad3c27a35cd3c0fL)).seal();
}
