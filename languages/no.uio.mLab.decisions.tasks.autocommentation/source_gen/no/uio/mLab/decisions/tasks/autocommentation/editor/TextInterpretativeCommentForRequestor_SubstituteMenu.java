package no.uio.mLab.decisions.tasks.autocommentation.editor;

/*Generated by MPS */

import jetbrains.mps.nodeEditor.menus.substitute.SubstituteMenuBase;
import org.jetbrains.annotations.NotNull;
import java.util.List;
import jetbrains.mps.lang.editor.menus.MenuPart;
import jetbrains.mps.openapi.editor.menus.substitute.SubstituteMenuItem;
import jetbrains.mps.openapi.editor.menus.substitute.SubstituteMenuContext;
import java.util.ArrayList;
import jetbrains.mps.lang.editor.menus.substitute.ConstraintsFilteringSubstituteMenuPartDecorator;
import jetbrains.mps.smodel.adapter.structure.MetaAdapterFactory;
import jetbrains.mps.lang.editor.menus.EditorMenuDescriptorBase;
import jetbrains.mps.smodel.SNodePointer;
import jetbrains.mps.lang.editor.menus.substitute.SingleItemSubstituteMenuPart;
import org.jetbrains.annotations.Nullable;
import org.apache.log4j.Logger;
import jetbrains.mps.lang.editor.menus.substitute.DefaultSubstituteMenuItem;
import jetbrains.mps.openapi.editor.menus.EditorMenuTraceInfo;
import org.jetbrains.mps.openapi.model.SNode;
import jetbrains.mps.smodel.action.SNodeFactoryOperations;
import jetbrains.mps.lang.smodel.generator.smodelAdapter.SPropertyOperations;
import jetbrains.mps.lang.smodel.generator.smodelAdapter.SLinkOperations;
import no.uio.mLab.shared.behavior.ITranslatableConcept__BehaviorDescriptor;
import jetbrains.mps.lang.smodel.generator.smodelAdapter.SNodeOperations;

public class TextInterpretativeCommentForRequestor_SubstituteMenu extends SubstituteMenuBase {
  @NotNull
  @Override
  protected List<MenuPart<SubstituteMenuItem, SubstituteMenuContext>> getParts(final SubstituteMenuContext _context) {
    List<MenuPart<SubstituteMenuItem, SubstituteMenuContext>> result = new ArrayList<MenuPart<SubstituteMenuItem, SubstituteMenuContext>>();
    result.add(new ConstraintsFilteringSubstituteMenuPartDecorator(new TextInterpretativeCommentForRequestor_SubstituteMenu.SMP_Action_vfsrqi_a(), MetaAdapterFactory.getConcept(0xebe710b88f464b5eL, 0x8fb1221de61f9e7cL, 0xd694e28d495ef80L, "no.uio.mLab.decisions.tasks.autocommentation.structure.TextInterpretativeCommentForRequestor")));
    return result;
  }

  @NotNull
  @Override
  public List<SubstituteMenuItem> createMenuItems(@NotNull SubstituteMenuContext context) {
    context.getEditorMenuTrace().pushTraceInfo();
    context.getEditorMenuTrace().setDescriptor(new EditorMenuDescriptorBase("default substitute menu for " + "TextInterpretativeCommentForRequestor", new SNodePointer("r:00aa8c92-ad64-4f4b-9901-076e5158f704(no.uio.mLab.decisions.tasks.autocommentation.editor)", "966389532319970110")));
    try {
      return super.createMenuItems(context);
    } finally {
      context.getEditorMenuTrace().popTraceInfo();
    }
  }


  private class SMP_Action_vfsrqi_a extends SingleItemSubstituteMenuPart {

    @Nullable
    @Override
    protected SubstituteMenuItem createItem(SubstituteMenuContext _context) {
      TextInterpretativeCommentForRequestor_SubstituteMenu.SMP_Action_vfsrqi_a.Item item = new TextInterpretativeCommentForRequestor_SubstituteMenu.SMP_Action_vfsrqi_a.Item(_context);
      String description;
      try {
        description = "Substitute item: " + item.getMatchingText("");
      } catch (Throwable t) {
        Logger.getLogger(getClass()).error("Exception while executing getMatchingText() of the item " + item, t);
        return null;
      }

      _context.getEditorMenuTrace().pushTraceInfo();
      try {
        _context.getEditorMenuTrace().setDescriptor(new EditorMenuDescriptorBase(description, new SNodePointer("r:00aa8c92-ad64-4f4b-9901-076e5158f704(no.uio.mLab.decisions.tasks.autocommentation.editor)", "966389532319970111")));
        item.setTraceInfo(_context.getEditorMenuTrace().getTraceInfo());
      } finally {
        _context.getEditorMenuTrace().popTraceInfo();
      }

      return item;
    }
    private class Item extends DefaultSubstituteMenuItem {
      private final SubstituteMenuContext _context;
      private EditorMenuTraceInfo myTraceInfo;
      public Item(SubstituteMenuContext context) {
        super(MetaAdapterFactory.getConcept(0xebe710b88f464b5eL, 0x8fb1221de61f9e7cL, 0xd694e28d495ef80L, "no.uio.mLab.decisions.tasks.autocommentation.structure.TextInterpretativeCommentForRequestor"), context.getParentNode(), context.getCurrentTargetNode(), context.getEditorContext());
        _context = context;
      }

      private void setTraceInfo(EditorMenuTraceInfo traceInfo) {
        myTraceInfo = traceInfo;
      }

      @Nullable
      @Override
      public SNode createNode(@NotNull String pattern) {
        SNode node = SNodeFactoryOperations.createNewNode(SNodeFactoryOperations.asInstanceConcept(MetaAdapterFactory.getConcept(0xebe710b88f464b5eL, 0x8fb1221de61f9e7cL, 0xd694e28d495ef80L, "no.uio.mLab.decisions.tasks.autocommentation.structure.TextInterpretativeCommentForRequestor")), _context.getCurrentTargetNode());
        SNodeFactoryOperations.setNewChild(node, MetaAdapterFactory.getContainmentLink(0xebe710b88f464b5eL, 0x8fb1221de61f9e7cL, 0xd694e28d495ef80L, 0xd694e28d495efa1L, "text"), SNodeFactoryOperations.asInstanceConcept(MetaAdapterFactory.getConcept(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x5f0f3639007c78b0L, "no.uio.mLab.decisions.core.structure.TextLiteral")));
        if ((pattern != null && pattern.length() > 0)) {
          int start = (pattern.startsWith("\"") ? 1 : 0);
          int end = (pattern.length() >= 2 && pattern.endsWith("\"") ? pattern.length() - 1 : pattern.length());
          SPropertyOperations.assign(SLinkOperations.getTarget(node, MetaAdapterFactory.getContainmentLink(0xebe710b88f464b5eL, 0x8fb1221de61f9e7cL, 0xd694e28d495ef80L, 0xd694e28d495efa1L, "text")), MetaAdapterFactory.getProperty(0xc610a4ebc47c4f95L, 0xb56811236971769cL, 0x5f0f3639007c78b0L, 0x5f0f3639007c78b1L, "value"), pattern.substring(start, end));
        }
        return node;
      }

      @Override
      public EditorMenuTraceInfo getTraceInfo() {
        return myTraceInfo;
      }
      @Override
      public boolean canExecute(@NotNull String pattern) {
        return canExecute_internal(pattern, false);
      }
      @Override
      public boolean canExecuteStrictly(@NotNull String pattern) {
        return canExecute_internal(pattern, true);
      }
      public boolean canExecute_internal(@NotNull String pattern, boolean strictly) {
        return (pattern == null || pattern.length() == 0) || pattern.startsWith("\"");
      }
      @Nullable
      @Override
      public String getMatchingText(@NotNull String pattern) {
        if ((pattern == null || pattern.length() == 0)) {
          return ITranslatableConcept__BehaviorDescriptor.getDisplayAlias_id5Wfdz$0vc2$.invoke(SNodeOperations.asSConcept(MetaAdapterFactory.getConcept(0xebe710b88f464b5eL, 0x8fb1221de61f9e7cL, 0xd694e28d495ef80L, "no.uio.mLab.decisions.tasks.autocommentation.structure.TextInterpretativeCommentForRequestor")));
        }
        return pattern;
      }
      @Nullable
      @Override
      public String getDescriptionText(@NotNull String pattern) {
        return (String) ITranslatableConcept__BehaviorDescriptor.getDisplayDescription_id5Wfdz$0vc3v.invoke(SNodeOperations.asSConcept(MetaAdapterFactory.getConcept(0xebe710b88f464b5eL, 0x8fb1221de61f9e7cL, 0xd694e28d495ef80L, "no.uio.mLab.decisions.tasks.autocommentation.structure.TextInterpretativeCommentForRequestor")));
      }
    }
  }
}
