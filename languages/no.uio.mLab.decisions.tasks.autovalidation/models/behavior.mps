<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:9a8b90c2-e2cf-49ec-8752-8d47c482d64e(no.uio.mLab.decisions.tasks.autovalidation.behavior)">
  <persistence version="9" />
  <languages>
    <use id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior" version="1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="c5hk" ref="r:9fddaf0a-3e76-4553-975c-e5dee43f6d4f(no.uio.mLab.decisions.tasks.autovalidation.runtime)" />
    <import index="1yj" ref="r:0c887d67-81ab-420f-9a35-a9f6bf96c975(no.uio.mLab.shared.behavior)" />
    <import index="wb6c" ref="r:19d15037-83c1-4ae2-b66a-763828cb8279(no.uio.mLab.decisions.core.behavior)" />
    <import index="tt7x" ref="r:d018e1a4-c777-4723-b40c-3142fe00cae8(no.uio.mLab.decisions.tasks.autovalidation.translations)" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" />
    <import index="wnv9" ref="r:b9f6513c-4c87-4df4-8a31-edae615f1db3(no.uio.mLab.decisions.tasks.autovalidation.structure)" />
    <import index="7f9y" ref="r:790ac37e-42d3-4dd3-977c-e8fe9cc56a45(no.uio.mLab.decisions.core.structure)" />
    <import index="hyw5" ref="r:b77da972-4430-4cfb-8552-374583961329(no.uio.mLab.decisions.core.utils)" />
  </imports>
  <registry>
    <language id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior">
      <concept id="6496299201655527393" name="jetbrains.mps.lang.behavior.structure.LocalBehaviorMethodCall" flags="nn" index="BsUDl" />
      <concept id="1225194240794" name="jetbrains.mps.lang.behavior.structure.ConceptBehavior" flags="ng" index="13h7C7">
        <reference id="1225194240799" name="concept" index="13h7C2" />
        <child id="1225194240805" name="method" index="13h7CS" />
        <child id="1225194240801" name="constructor" index="13h7CW" />
      </concept>
      <concept id="1225194413805" name="jetbrains.mps.lang.behavior.structure.ConceptConstructorDeclaration" flags="in" index="13hLZK" />
      <concept id="1225194472830" name="jetbrains.mps.lang.behavior.structure.ConceptMethodDeclaration" flags="ng" index="13i0hz">
        <property id="5864038008284099149" name="isStatic" index="2Ki8OM" />
        <property id="1225194472832" name="isVirtual" index="13i0it" />
        <property id="1225194472834" name="isAbstract" index="13i0iv" />
        <reference id="1225194472831" name="overriddenMethod" index="13i0hy" />
      </concept>
      <concept id="1225194691553" name="jetbrains.mps.lang.behavior.structure.ThisNodeExpression" flags="nn" index="13iPFW" />
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="4836112446988635817" name="jetbrains.mps.baseLanguage.structure.UndefinedType" flags="in" index="2jxLKc" />
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="5497648299878491908" name="jetbrains.mps.baseLanguage.structure.BaseVariableReference" flags="nn" index="1M0zk4">
        <reference id="5497648299878491909" name="baseVariableDeclaration" index="1M0zk5" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="1883223317721008708" name="jetbrains.mps.lang.smodel.structure.IfInstanceOfStatement" flags="nn" index="Jncv_">
        <reference id="1883223317721008712" name="nodeConcept" index="JncvD" />
        <child id="1883223317721008709" name="body" index="Jncv$" />
        <child id="1883223317721008711" name="variable" index="JncvA" />
        <child id="1883223317721008710" name="nodeExpression" index="JncvB" />
      </concept>
      <concept id="1883223317721008713" name="jetbrains.mps.lang.smodel.structure.IfInstanceOfVariable" flags="ng" index="JncvC" />
      <concept id="1883223317721107059" name="jetbrains.mps.lang.smodel.structure.IfInstanceOfVarReference" flags="nn" index="Jnkvi" />
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2" />
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="13h7C7" id="1mAGFBLEUb5">
    <property role="3GE5qa" value="actions" />
    <ref role="13h7C2" to="wnv9:1mAGFBLbXAm" resolve="NotifyTechnologist" />
    <node concept="13i0hz" id="1mAGFBLdlRb" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="1mAGFBLdlRc" role="1B3o_S" />
      <node concept="3clFbS" id="1mAGFBLdlRh" role="3clF47">
        <node concept="3clFbF" id="1mAGFBLdlZk" role="3cqZAp">
          <node concept="2OqwBi" id="1mAGFBLdm6S" role="3clFbG">
            <node concept="BsUDl" id="1Hxyv4EVFDO" role="2Oq$k0">
              <ref role="37wK5l" node="1Hxyv4EVFrL" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="1mAGFBLdmcA" role="2OqNvi">
              <ref role="37wK5l" to="tt7x:1mAGFBLaw4j" resolve="getNotifyTechnologistAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="1mAGFBLdlRi" role="3clF45" />
    </node>
    <node concept="13i0hz" id="1mAGFBLdlRn" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="1mAGFBLdlRo" role="1B3o_S" />
      <node concept="3clFbS" id="1mAGFBLdlRt" role="3clF47">
        <node concept="3clFbF" id="1mAGFBLdmdw" role="3cqZAp">
          <node concept="2OqwBi" id="1mAGFBLdmkR" role="3clFbG">
            <node concept="BsUDl" id="1Hxyv4EVFJ5" role="2Oq$k0">
              <ref role="37wK5l" node="1Hxyv4EVFrL" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="1mAGFBLdmqH" role="2OqNvi">
              <ref role="37wK5l" to="tt7x:1mAGFBLaw7h" resolve="getNotifyTechnologistDescription" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="1mAGFBLdlRu" role="3clF45" />
    </node>
    <node concept="13i0hz" id="499Gn2DH3ot" role="13h7CS">
      <property role="TrG5h" value="toDisplayString" />
      <property role="2Ki8OM" value="false" />
      <property role="13i0it" value="false" />
      <property role="13i0iv" value="false" />
      <ref role="13i0hy" to="1yj:499Gn2DGTs1" resolve="toDisplayString" />
      <node concept="3clFbS" id="499Gn2DH3oD" role="3clF47">
        <node concept="3clFbF" id="499Gn2DH3w6" role="3cqZAp">
          <node concept="2YIFZM" id="499Gn2DH3wq" role="3clFbG">
            <ref role="37wK5l" to="wyt6:~String.format(java.lang.String,java.lang.Object...):java.lang.String" resolve="format" />
            <ref role="1Pybhc" to="wyt6:~String" resolve="String" />
            <node concept="Xl_RD" id="499Gn2DH3xu" role="37wK5m">
              <property role="Xl_RC" value="%s %s" />
            </node>
            <node concept="BsUDl" id="499Gn2DH3EO" role="37wK5m">
              <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
            </node>
            <node concept="2OqwBi" id="4QUW3efwYXW" role="37wK5m">
              <node concept="2OqwBi" id="4QUW3efwYb4" role="2Oq$k0">
                <node concept="13iPFW" id="4QUW3efwXUG" role="2Oq$k0" />
                <node concept="3TrEf2" id="4QUW3efwYt_" role="2OqNvi">
                  <ref role="3Tt5mk" to="wnv9:4QUW3efwXkw" resolve="testReference" />
                </node>
              </node>
              <node concept="2qgKlT" id="4QUW3efwZqk" role="2OqNvi">
                <ref role="37wK5l" to="1yj:499Gn2DGTs1" resolve="toDisplayString" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="499Gn2DROTY" role="3clF45" />
      <node concept="3Tm1VV" id="499Gn2DROTZ" role="1B3o_S" />
    </node>
    <node concept="13i0hz" id="499Gn2DH3oJ" role="13h7CS">
      <property role="TrG5h" value="toGenerationString" />
      <property role="2Ki8OM" value="false" />
      <property role="13i0it" value="false" />
      <property role="13i0iv" value="false" />
      <ref role="13i0hy" to="1yj:499Gn2DGU_t" resolve="toGenerationString" />
      <node concept="3clFbS" id="499Gn2DH3oV" role="3clF47">
        <node concept="3clFbF" id="499Gn2DH5cz" role="3cqZAp">
          <node concept="2YIFZM" id="499Gn2DH5c_" role="3clFbG">
            <ref role="1Pybhc" to="wyt6:~String" resolve="String" />
            <ref role="37wK5l" to="wyt6:~String.format(java.lang.String,java.lang.Object...):java.lang.String" resolve="format" />
            <node concept="Xl_RD" id="499Gn2DH5cA" role="37wK5m">
              <property role="Xl_RC" value="%s %s" />
            </node>
            <node concept="2OqwBi" id="499Gn2DH5WT" role="37wK5m">
              <node concept="BsUDl" id="499Gn2DH5Iz" role="2Oq$k0">
                <ref role="37wK5l" node="1Hxyv4EVFvk" resolve="getGenerationTranslations" />
              </node>
              <node concept="liA8E" id="499Gn2DH6fe" role="2OqNvi">
                <ref role="37wK5l" to="tt7x:1mAGFBLaw4j" resolve="getNotifyTechnologistAlias" />
              </node>
            </node>
            <node concept="2OqwBi" id="4QUW3efx19f" role="37wK5m">
              <node concept="2OqwBi" id="4QUW3efx0lX" role="2Oq$k0">
                <node concept="13iPFW" id="4QUW3efx05n" role="2Oq$k0" />
                <node concept="3TrEf2" id="4QUW3efx0CG" role="2OqNvi">
                  <ref role="3Tt5mk" to="wnv9:4QUW3efwXkw" resolve="testReference" />
                </node>
              </node>
              <node concept="2qgKlT" id="4QUW3efx1_P" role="2OqNvi">
                <ref role="37wK5l" to="1yj:499Gn2DGU_t" resolve="toGenerationString" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="499Gn2DRP2W" role="3clF45" />
      <node concept="3Tm1VV" id="499Gn2DRP2X" role="1B3o_S" />
    </node>
    <node concept="13i0hz" id="65epL7MjI9o" role="13h7CS">
      <property role="TrG5h" value="matches" />
      <property role="13i0it" value="false" />
      <property role="13i0iv" value="false" />
      <ref role="13i0hy" to="wb6c:6LTgXmMs$_D" resolve="matches" />
      <node concept="3Tm1VV" id="65epL7MjI9p" role="1B3o_S" />
      <node concept="3clFbS" id="65epL7MjI9A" role="3clF47">
        <node concept="Jncv_" id="65epL7MjIxc" role="3cqZAp">
          <ref role="JncvD" to="wnv9:1mAGFBLbXAm" resolve="NotifyTechnologist" />
          <node concept="37vLTw" id="65epL7MjIxD" role="JncvB">
            <ref role="3cqZAo" node="65epL7MjI9B" resolve="node" />
          </node>
          <node concept="3clFbS" id="65epL7MjIxe" role="Jncv$">
            <node concept="3cpWs6" id="65epL7MjIyy" role="3cqZAp">
              <node concept="2OqwBi" id="65epL7MjJvl" role="3cqZAk">
                <node concept="2OqwBi" id="65epL7MjIKC" role="2Oq$k0">
                  <node concept="13iPFW" id="65epL7MjIz0" role="2Oq$k0" />
                  <node concept="3TrEf2" id="65epL7MjJ0W" role="2OqNvi">
                    <ref role="3Tt5mk" to="wnv9:4QUW3efwXkw" resolve="testReference" />
                  </node>
                </node>
                <node concept="2qgKlT" id="65epL7MjJQ8" role="2OqNvi">
                  <ref role="37wK5l" to="wb6c:6LTgXmMs$_D" resolve="matches" />
                  <node concept="2OqwBi" id="65epL7MjKpD" role="37wK5m">
                    <node concept="Jnkvi" id="65epL7MjK3h" role="2Oq$k0">
                      <ref role="1M0zk5" node="65epL7MjIxf" resolve="action" />
                    </node>
                    <node concept="3TrEf2" id="65epL7MjKOP" role="2OqNvi">
                      <ref role="3Tt5mk" to="wnv9:4QUW3efwXkw" resolve="testReference" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="JncvC" id="65epL7MjIxf" role="JncvA">
            <property role="TrG5h" value="action" />
            <node concept="2jxLKc" id="65epL7MjIxg" role="1tU5fm" />
          </node>
        </node>
        <node concept="3clFbF" id="65epL7MjL9U" role="3cqZAp">
          <node concept="2YIFZM" id="65epL7MjLjO" role="3clFbG">
            <ref role="37wK5l" to="hyw5:1I84Bf7XEgz" resolve="createMismatch" />
            <ref role="1Pybhc" to="hyw5:6LTgXmO6cZw" resolve="JoinPointMatch" />
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="65epL7MjI9B" role="3clF46">
        <property role="TrG5h" value="node" />
        <node concept="3Tqbb2" id="65epL7MjI9C" role="1tU5fm" />
      </node>
      <node concept="3uibUv" id="65epL7MjI9D" role="3clF45">
        <ref role="3uigEE" to="hyw5:6LTgXmO6cZw" resolve="JoinPointMatch" />
      </node>
    </node>
    <node concept="13hLZK" id="1mAGFBLEUb6" role="13h7CW">
      <node concept="3clFbS" id="1mAGFBLEUb7" role="2VODD2" />
    </node>
  </node>
  <node concept="13h7C7" id="1Hxyv4EVFrs">
    <ref role="13h7C2" to="wnv9:1Hxyv4EVFqQ" resolve="ITranslatableAutoValidationConcept" />
    <node concept="13i0hz" id="1Hxyv4EVFrL" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getDisplayTranslations" />
      <node concept="3Tm1VV" id="1Hxyv4EVFrM" role="1B3o_S" />
      <node concept="3uibUv" id="2$xY$aF7CXG" role="3clF45">
        <ref role="3uigEE" to="tt7x:4zMac8rUNtP" resolve="IAutoValidationTranslations" />
      </node>
      <node concept="3clFbS" id="1Hxyv4EVFrO" role="3clF47">
        <node concept="3clFbF" id="1Hxyv4EVFuc" role="3cqZAp">
          <node concept="10M0yZ" id="2$xY$aF7CYw" role="3clFbG">
            <ref role="3cqZAo" to="tt7x:4zMac8rVBeK" resolve="displayTranslations" />
            <ref role="1PxDUh" to="tt7x:4zMac8rUNsN" resolve="AutoValidationTranslationProvider" />
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="1Hxyv4EVFvk" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getGenerationTranslations" />
      <node concept="3Tm1VV" id="1Hxyv4EVFvl" role="1B3o_S" />
      <node concept="3uibUv" id="2$xY$aF7CZ5" role="3clF45">
        <ref role="3uigEE" to="tt7x:4zMac8rUNtP" resolve="IAutoValidationTranslations" />
      </node>
      <node concept="3clFbS" id="1Hxyv4EVFvn" role="3clF47">
        <node concept="3clFbF" id="1Hxyv4EVFvo" role="3cqZAp">
          <node concept="10M0yZ" id="2$xY$aF7CZE" role="3clFbG">
            <ref role="3cqZAo" to="tt7x:1Hxyv4EVEjd" resolve="generationTranslations" />
            <ref role="1PxDUh" to="tt7x:4zMac8rUNsN" resolve="AutoValidationTranslationProvider" />
          </node>
        </node>
      </node>
    </node>
    <node concept="13hLZK" id="1Hxyv4EVFrt" role="13h7CW">
      <node concept="3clFbS" id="1Hxyv4EVFru" role="2VODD2" />
    </node>
  </node>
</model>

