<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:03377333-9075-4497-89eb-4d7ba6f6d4b5(no.uio.mLab.decisions.tasks.autovalidation.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="11" />
    <use id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions" version="4" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="wnv9" ref="r:b9f6513c-4c87-4df4-8a31-edae615f1db3(no.uio.mLab.decisions.tasks.autovalidation.structure)" implicit="true" />
    <import index="1yj" ref="r:0c887d67-81ab-420f-9a35-a9f6bf96c975(no.uio.mLab.shared.behavior)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="2000375450116454183" name="jetbrains.mps.lang.editor.structure.ISubstituteMenu" flags="ng" index="22mbnS">
        <child id="414384289274416996" name="parts" index="3ft7WO" />
      </concept>
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi" />
      <concept id="1106270549637" name="jetbrains.mps.lang.editor.structure.CellLayout_Horizontal" flags="nn" index="2iRfu4" />
      <concept id="1177327570013" name="jetbrains.mps.lang.editor.structure.QueryFunction_SubstituteMenu_Substitute" flags="in" index="ucgPf" />
      <concept id="8478191136883534237" name="jetbrains.mps.lang.editor.structure.IExtensibleSubstituteMenuPart" flags="ng" index="upBLQ">
        <child id="8478191136883534238" name="features" index="upBLP" />
      </concept>
      <concept id="1177335944525" name="jetbrains.mps.lang.editor.structure.QueryFunction_SubstituteMenu_SubstituteString" flags="in" index="uGdhv" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="8383079901754291618" name="jetbrains.mps.lang.editor.structure.CellModel_NextEditor" flags="ng" index="B$lHz" />
      <concept id="1630016958697344083" name="jetbrains.mps.lang.editor.structure.IMenu_Concept" flags="ng" index="2ZABuq">
        <reference id="6591946374543067572" name="conceptDeclaration" index="aqKnT" />
      </concept>
      <concept id="8998492695583125082" name="jetbrains.mps.lang.editor.structure.SubstituteFeature_MatchingText" flags="ng" index="16NfWO">
        <child id="8998492695583129244" name="query" index="16NeZM" />
      </concept>
      <concept id="8998492695583129971" name="jetbrains.mps.lang.editor.structure.SubstituteFeature_DescriptionText" flags="ng" index="16NL0t">
        <child id="8998492695583129972" name="query" index="16NL0q" />
      </concept>
      <concept id="7342352913006985483" name="jetbrains.mps.lang.editor.structure.SubstituteMenuPart_Action" flags="ng" index="3eGOop">
        <child id="8612453216082699922" name="substituteHandler" index="3aKz83" />
      </concept>
      <concept id="3308396621974580100" name="jetbrains.mps.lang.editor.structure.SubstituteMenu_Default" flags="ng" index="3p36aQ" />
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389446423" name="jetbrains.mps.lang.editor.structure.CellModel_Collection" flags="sn" stub="3013115976261988961" index="3EZMnI">
        <child id="1106270802874" name="cellLayout" index="2iSdaV" />
        <child id="1073389446424" name="childCellModel" index="3EZMnx" />
      </concept>
      <concept id="1073389882823" name="jetbrains.mps.lang.editor.structure.CellModel_RefNode" flags="sg" stub="730538219795960754" index="3F1sOY" />
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
      </concept>
    </language>
    <language id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions">
      <concept id="5979988948250981289" name="jetbrains.mps.lang.actions.structure.SNodeCreatorAndInitializer" flags="nn" index="2fJWfE" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="2644386474300074836" name="jetbrains.mps.lang.smodel.structure.ConceptIdRefExpression" flags="nn" index="35c_gC">
        <reference id="2644386474300074837" name="conceptDeclaration" index="35c_gD" />
      </concept>
      <concept id="1180636770613" name="jetbrains.mps.lang.smodel.structure.SNodeCreator" flags="nn" index="3zrR0B">
        <child id="1180636770616" name="createdType" index="3zrR0E" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
    </language>
  </registry>
  <node concept="24kQdi" id="1mAGFBLbXAM">
    <property role="3GE5qa" value="actions" />
    <ref role="1XX52x" to="wnv9:1mAGFBLbXAm" resolve="NotifyTechnologist" />
    <node concept="3EZMnI" id="1mAGFBLbXAO" role="2wV5jI">
      <node concept="2iRfu4" id="1mAGFBLbXAP" role="2iSdaV" />
      <node concept="B$lHz" id="1mAGFBLbXAU" role="3EZMnx" />
      <node concept="3F1sOY" id="4QUW3efwXkA" role="3EZMnx">
        <ref role="1NtTu8" to="wnv9:4QUW3efwXkw" resolve="testReference" />
      </node>
    </node>
  </node>
  <node concept="3p36aQ" id="1mAGFBLdiqA">
    <property role="3GE5qa" value="actions" />
    <ref role="aqKnT" to="wnv9:1mAGFBLbXAm" resolve="NotifyTechnologist" />
    <node concept="3eGOop" id="1mAGFBLdiqB" role="3ft7WO">
      <node concept="ucgPf" id="1mAGFBLdiqC" role="3aKz83">
        <node concept="3clFbS" id="1mAGFBLdiqD" role="2VODD2">
          <node concept="3clFbF" id="1mAGFBLdiv4" role="3cqZAp">
            <node concept="2ShNRf" id="1mAGFBLdiv2" role="3clFbG">
              <node concept="2fJWfE" id="1mAGFBLdiAC" role="2ShVmc">
                <node concept="3Tqbb2" id="1mAGFBLdiAE" role="3zrR0E">
                  <ref role="ehGHo" to="wnv9:1mAGFBLbXAm" resolve="NotifyTechnologist" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NfWO" id="1mAGFBLdiJF" role="upBLP">
        <node concept="uGdhv" id="1mAGFBLdiOh" role="16NeZM">
          <node concept="3clFbS" id="1mAGFBLdiOj" role="2VODD2">
            <node concept="3clFbF" id="1mAGFBLdiWT" role="3cqZAp">
              <node concept="2OqwBi" id="1mAGFBLdj_u" role="3clFbG">
                <node concept="35c_gC" id="1mAGFBLdiWS" role="2Oq$k0">
                  <ref role="35c_gD" to="wnv9:1mAGFBLbXAm" resolve="NotifyTechnologist" />
                </node>
                <node concept="2qgKlT" id="1mAGFBLdk2Z" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="16NL0t" id="1mAGFBLdklq" role="upBLP">
        <node concept="uGdhv" id="1mAGFBLdkqd" role="16NL0q">
          <node concept="3clFbS" id="1mAGFBLdkqf" role="2VODD2">
            <node concept="3clFbF" id="1mAGFBLdkyP" role="3cqZAp">
              <node concept="2OqwBi" id="1mAGFBLdlbq" role="3clFbG">
                <node concept="35c_gC" id="1mAGFBLdkyO" role="2Oq$k0">
                  <ref role="35c_gD" to="wnv9:1mAGFBLbXAm" resolve="NotifyTechnologist" />
                </node>
                <node concept="2qgKlT" id="1mAGFBLdlCV" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

