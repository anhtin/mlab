<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:7e8a9f85-2aa2-44c2-b3df-3b0f51d323a7(no.uio.mLab.decisions.tasks.autovalidation.typesystem)">
  <persistence version="9" />
  <languages>
    <use id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem" version="1" />
    <devkit ref="00000000-0000-4000-0000-1de82b3a4936(jetbrains.mps.devkit.aspect.typesystem)" />
  </languages>
  <imports>
    <import index="c5hk" ref="r:9fddaf0a-3e76-4553-975c-e5dee43f6d4f(no.uio.mLab.decisions.tasks.autovalidation.runtime)" />
  </imports>
  <registry />
</model>

