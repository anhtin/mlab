<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:9fddaf0a-3e76-4553-975c-e5dee43f6d4f(no.uio.mLab.decisions.tasks.autovalidation.runtime)">
  <persistence version="9" />
  <languages>
    <use id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage" version="6" />
  </languages>
  <imports>
    <import index="8r9s" ref="r:0e143259-7e64-4fad-906c-02a97b75504d(no.uio.mLab.decisions.core.runtime)" />
    <import index="mygi" ref="r:d014d2e6-828c-4ff5-a027-d2fa848d3c1b(no.uio.mLab.decisions.references.laboratoryTest.runtime)" />
    <import index="33ny" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.util(JDK/)" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1080223426719" name="jetbrains.mps.baseLanguage.structure.OrExpression" flags="nn" index="22lmx$" />
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1465982738277781862" name="jetbrains.mps.baseLanguage.structure.PlaceholderMember" flags="ng" index="2tJIrI" />
      <concept id="1188207840427" name="jetbrains.mps.baseLanguage.structure.AnnotationInstance" flags="nn" index="2AHcQZ">
        <reference id="1188208074048" name="annotation" index="2AI5Lk" />
      </concept>
      <concept id="1188208481402" name="jetbrains.mps.baseLanguage.structure.HasAnnotation" flags="ng" index="2AJDlI">
        <child id="1188208488637" name="annotation" index="2AJF6D" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1197029447546" name="jetbrains.mps.baseLanguage.structure.FieldReferenceOperation" flags="nn" index="2OwXpG">
        <reference id="1197029500499" name="fieldDeclaration" index="2Oxat5" />
      </concept>
      <concept id="1070475354124" name="jetbrains.mps.baseLanguage.structure.ThisExpression" flags="nn" index="Xjq3P" />
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1070534058343" name="jetbrains.mps.baseLanguage.structure.NullLiteral" flags="nn" index="10Nm6u" />
      <concept id="1070534370425" name="jetbrains.mps.baseLanguage.structure.IntegerType" flags="in" index="10Oyi0" />
      <concept id="1070534644030" name="jetbrains.mps.baseLanguage.structure.BooleanType" flags="in" index="10P_77" />
      <concept id="1070534934090" name="jetbrains.mps.baseLanguage.structure.CastExpression" flags="nn" index="10QFUN">
        <child id="1070534934091" name="type" index="10QFUM" />
        <child id="1070534934092" name="expression" index="10QFUP" />
      </concept>
      <concept id="1068390468200" name="jetbrains.mps.baseLanguage.structure.FieldDeclaration" flags="ig" index="312cEg">
        <property id="8606350594693632173" name="isTransient" index="eg7rD" />
        <property id="1240249534625" name="isVolatile" index="34CwA1" />
      </concept>
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu">
        <child id="1095933932569" name="implementedInterface" index="EKbjA" />
        <child id="1165602531693" name="superclass" index="1zkMxy" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <property id="1176718929932" name="isFinal" index="3TUv4t" />
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <property id="4276006055363816570" name="isSynchronized" index="od$2w" />
        <property id="1181808852946" name="isFinal" index="DiZV1" />
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123165" name="jetbrains.mps.baseLanguage.structure.InstanceMethodDeclaration" flags="ig" index="3clFb_">
        <property id="1178608670077" name="isAbstract" index="1EzhhJ" />
      </concept>
      <concept id="1068580123152" name="jetbrains.mps.baseLanguage.structure.EqualsExpression" flags="nn" index="3clFbC" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123137" name="jetbrains.mps.baseLanguage.structure.BooleanConstant" flags="nn" index="3clFbT">
        <property id="1068580123138" name="value" index="3clFbU" />
      </concept>
      <concept id="1068580123140" name="jetbrains.mps.baseLanguage.structure.ConstructorDeclaration" flags="ig" index="3clFbW" />
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1068581517677" name="jetbrains.mps.baseLanguage.structure.VoidType" flags="in" index="3cqZAl" />
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1107461130800" name="jetbrains.mps.baseLanguage.structure.Classifier" flags="ng" index="3pOWGL">
        <child id="5375687026011219971" name="member" index="jymVt" unordered="true" />
      </concept>
      <concept id="7812454656619025416" name="jetbrains.mps.baseLanguage.structure.MethodDeclaration" flags="ng" index="1rXfSm">
        <property id="8355037393041754995" name="isNative" index="2aFKle" />
      </concept>
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1073239437375" name="jetbrains.mps.baseLanguage.structure.NotEqualsExpression" flags="nn" index="3y3z36" />
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="312cEu" id="1mAGFBL_Xh3">
    <property role="3GE5qa" value="actions" />
    <property role="TrG5h" value="NotifyTechnologist" />
    <node concept="312cEg" id="1mAGFBLEFwI" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="test" />
      <property role="3TUv4t" value="false" />
      <node concept="17QB3L" id="1mAGFBLEFw$" role="1tU5fm" />
      <node concept="3Tm1VV" id="1mAGFBLEFwX" role="1B3o_S" />
    </node>
    <node concept="2tJIrI" id="1mAGFBLEFxd" role="jymVt" />
    <node concept="3clFbW" id="1mAGFBLEFxy" role="jymVt">
      <node concept="3cqZAl" id="1mAGFBLEFx$" role="3clF45" />
      <node concept="3Tm1VV" id="1mAGFBLEFx_" role="1B3o_S" />
      <node concept="3clFbS" id="1mAGFBLEFxA" role="3clF47">
        <node concept="3clFbF" id="1mAGFBLEU_M" role="3cqZAp">
          <node concept="37vLTI" id="1mAGFBLEUSZ" role="3clFbG">
            <node concept="37vLTw" id="1mAGFBLEUUq" role="37vLTx">
              <ref role="3cqZAo" node="1mAGFBLEUIG" resolve="test" />
            </node>
            <node concept="2OqwBi" id="1mAGFBLEUC_" role="37vLTJ">
              <node concept="Xjq3P" id="1mAGFBLEU_L" role="2Oq$k0" />
              <node concept="2OwXpG" id="1mAGFBLEUGu" role="2OqNvi">
                <ref role="2Oxat5" node="1mAGFBLEFwI" resolve="test" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="1mAGFBLEUIG" role="3clF46">
        <property role="TrG5h" value="test" />
        <node concept="17QB3L" id="1mAGFBLEUIF" role="1tU5fm" />
      </node>
    </node>
    <node concept="2tJIrI" id="7lYCqhv50Ah" role="jymVt" />
    <node concept="3clFb_" id="7lYCqhv4LBo" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="hashCode" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3Tm1VV" id="7lYCqhv4LBp" role="1B3o_S" />
      <node concept="10Oyi0" id="7lYCqhv4LBr" role="3clF45" />
      <node concept="3clFbS" id="7lYCqhv4LBs" role="3clF47">
        <node concept="3clFbF" id="7lYCqhv4Mlk" role="3cqZAp">
          <node concept="2YIFZM" id="7lYCqhv4MBO" role="3clFbG">
            <ref role="1Pybhc" to="33ny:~Objects" resolve="Objects" />
            <ref role="37wK5l" to="33ny:~Objects.hash(java.lang.Object...):int" resolve="hash" />
            <node concept="2OqwBi" id="7lYCqhv4MIr" role="37wK5m">
              <node concept="Xjq3P" id="7lYCqhv4MCo" role="2Oq$k0" />
              <node concept="liA8E" id="7lYCqhv4MUb" role="2OqNvi">
                <ref role="37wK5l" to="wyt6:~Object.getClass():java.lang.Class" resolve="getClass" />
              </node>
            </node>
            <node concept="37vLTw" id="7lYCqhv50LT" role="37wK5m">
              <ref role="3cqZAo" node="1mAGFBLEFwI" resolve="test" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="7lYCqhv4LBt" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="7lYCqhv4M2C" role="jymVt" />
    <node concept="3clFb_" id="7lYCqhv4LBw" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="equals" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3Tm1VV" id="7lYCqhv4LBx" role="1B3o_S" />
      <node concept="10P_77" id="7lYCqhv4LBz" role="3clF45" />
      <node concept="37vLTG" id="7lYCqhv4LB$" role="3clF46">
        <property role="TrG5h" value="object" />
        <node concept="3uibUv" id="7lYCqhv4LB_" role="1tU5fm">
          <ref role="3uigEE" to="wyt6:~Object" resolve="Object" />
        </node>
      </node>
      <node concept="3clFbS" id="7lYCqhv4LBA" role="3clF47">
        <node concept="3clFbJ" id="4B5aqq5OJ30" role="3cqZAp">
          <node concept="3clFbS" id="4B5aqq5OJ31" role="3clFbx">
            <node concept="3cpWs6" id="4B5aqq5OJ32" role="3cqZAp">
              <node concept="3clFbT" id="4B5aqq5OJ33" role="3cqZAk">
                <property role="3clFbU" value="true" />
              </node>
            </node>
          </node>
          <node concept="3clFbC" id="4B5aqq5OJ34" role="3clFbw">
            <node concept="Xjq3P" id="4B5aqq5OJ35" role="3uHU7B" />
            <node concept="37vLTw" id="4B5aqq5OJ36" role="3uHU7w">
              <ref role="3cqZAo" node="7lYCqhv4LB$" resolve="object" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="4B5aqq5OJ37" role="3cqZAp">
          <node concept="3clFbS" id="4B5aqq5OJ38" role="3clFbx">
            <node concept="3cpWs6" id="4B5aqq5OJ39" role="3cqZAp">
              <node concept="3clFbT" id="4B5aqq5OJ3a" role="3cqZAk">
                <property role="3clFbU" value="false" />
              </node>
            </node>
          </node>
          <node concept="22lmx$" id="4B5aqq5OJW5" role="3clFbw">
            <node concept="3clFbC" id="4B5aqq5OKlz" role="3uHU7B">
              <node concept="10Nm6u" id="4B5aqq5OKtL" role="3uHU7w" />
              <node concept="37vLTw" id="4B5aqq5OKbr" role="3uHU7B">
                <ref role="3cqZAo" node="7lYCqhv4LB$" resolve="object" />
              </node>
            </node>
            <node concept="3y3z36" id="4B5aqq5OJ3b" role="3uHU7w">
              <node concept="2OqwBi" id="4B5aqq5OJ3c" role="3uHU7B">
                <node concept="Xjq3P" id="4B5aqq5OJ3d" role="2Oq$k0" />
                <node concept="liA8E" id="4B5aqq5OJ3e" role="2OqNvi">
                  <ref role="37wK5l" to="wyt6:~Object.getClass():java.lang.Class" resolve="getClass" />
                </node>
              </node>
              <node concept="2OqwBi" id="4B5aqq5OJ3f" role="3uHU7w">
                <node concept="37vLTw" id="4B5aqq5OJ3g" role="2Oq$k0">
                  <ref role="3cqZAo" node="7lYCqhv4LB$" resolve="object" />
                </node>
                <node concept="liA8E" id="4B5aqq5OJ3h" role="2OqNvi">
                  <ref role="37wK5l" to="wyt6:~Object.getClass():java.lang.Class" resolve="getClass" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="7lYCqhv4Ogi" role="3cqZAp">
          <node concept="3cpWsn" id="7lYCqhv4Ogj" role="3cpWs9">
            <property role="TrG5h" value="other" />
            <node concept="3uibUv" id="7lYCqhv51bN" role="1tU5fm">
              <ref role="3uigEE" node="1mAGFBL_Xh3" resolve="NotifyTechnologist" />
            </node>
            <node concept="10QFUN" id="7lYCqhv4Tin" role="33vP2m">
              <node concept="3uibUv" id="7lYCqhv5112" role="10QFUM">
                <ref role="3uigEE" node="1mAGFBL_Xh3" resolve="NotifyTechnologist" />
              </node>
              <node concept="37vLTw" id="7lYCqhv4Ovp" role="10QFUP">
                <ref role="3cqZAo" node="7lYCqhv4LB$" resolve="object" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="7lYCqhv4ONh" role="3cqZAp">
          <node concept="2YIFZM" id="4B5aqq5Q56J" role="3cqZAk">
            <ref role="37wK5l" to="33ny:~Objects.equals(java.lang.Object,java.lang.Object):boolean" resolve="equals" />
            <ref role="1Pybhc" to="33ny:~Objects" resolve="Objects" />
            <node concept="2OqwBi" id="4B5aqq5Q5t6" role="37wK5m">
              <node concept="Xjq3P" id="4B5aqq5Q5iL" role="2Oq$k0" />
              <node concept="2OwXpG" id="4B5aqq5Q5GB" role="2OqNvi">
                <ref role="2Oxat5" node="1mAGFBLEFwI" resolve="test" />
              </node>
            </node>
            <node concept="2OqwBi" id="4B5aqq5Q61L" role="37wK5m">
              <node concept="37vLTw" id="4B5aqq5Q5T9" role="2Oq$k0">
                <ref role="3cqZAo" node="7lYCqhv4Ogj" resolve="other" />
              </node>
              <node concept="2OwXpG" id="4B5aqq5Q6hz" role="2OqNvi">
                <ref role="2Oxat5" node="1mAGFBLEFwI" resolve="test" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="7lYCqhv4LBB" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="1mAGFBLEUZt" role="jymVt" />
    <node concept="3Tm1VV" id="1mAGFBL_Xh4" role="1B3o_S" />
    <node concept="3uibUv" id="1mAGFBL_Xi3" role="1zkMxy">
      <ref role="3uigEE" to="8r9s:1mAGFBLiCZV" resolve="Action" />
    </node>
    <node concept="3uibUv" id="7lYCqhuXya2" role="EKbjA">
      <ref role="3uigEE" to="mygi:1mAGFBLEFyK" resolve="ILaboratoryTestReferrer" />
    </node>
    <node concept="3clFb_" id="1mAGFBLEUzY" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getLaboratoryTestName" />
      <node concept="3Tm1VV" id="1mAGFBLEU$0" role="1B3o_S" />
      <node concept="17QB3L" id="1mAGFBLEU$1" role="3clF45" />
      <node concept="3clFbS" id="1mAGFBLEU$2" role="3clF47">
        <node concept="3cpWs6" id="1mAGFBLEUWW" role="3cqZAp">
          <node concept="2OqwBi" id="1mAGFBLEVei" role="3cqZAk">
            <node concept="Xjq3P" id="1mAGFBLEV7v" role="2Oq$k0" />
            <node concept="2OwXpG" id="1mAGFBLEVj7" role="2OqNvi">
              <ref role="2Oxat5" node="1mAGFBLEFwI" resolve="test" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="1mAGFBLEU$3" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
  </node>
</model>

