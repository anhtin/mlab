<?xml version="1.0" encoding="UTF-8"?>
<solution name="no.uio.mLab.decisions.tasks.autovalidation.runtime" uuid="0f0a8fb0-9593-41c4-900c-15c594642394" moduleVersion="0" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
  </models>
  <sourcePath />
  <dependencies>
    <dependency reexport="true">47bf15b2-e4c6-41f8-af94-4c062e947b5f(no.uio.mLab.decisions.core.runtime)</dependency>
    <dependency reexport="true">c4c71b02-3736-455d-8937-9cdbab70a659(no.uio.mLab.entities.laboratoryTest.runtime)</dependency>
    <dependency reexport="true">cd9f1852-e6bc-481e-9ffd-bfcf93751d73(no.uio.mLab.decisions.references.laboratoryTest.runtime)</dependency>
    <dependency reexport="false">6354ebe7-c22a-4a0f-ac54-50b52ab9b065(JDK)</dependency>
  </dependencies>
  <languageVersions>
    <language slang="l:f3061a53-9226-4cc5-a443-f952ceaf5816:jetbrains.mps.baseLanguage" version="6" />
    <language slang="l:ceab5195-25ea-4f22-9b92-103b95ca8c0c:jetbrains.mps.lang.core" version="1" />
    <language slang="l:9ded098b-ad6a-4657-bfd9-48636cfe8bc3:jetbrains.mps.lang.traceable" version="0" />
  </languageVersions>
  <dependencyVersions>
    <module reference="6354ebe7-c22a-4a0f-ac54-50b52ab9b065(JDK)" version="0" />
    <module reference="47bf15b2-e4c6-41f8-af94-4c062e947b5f(no.uio.mLab.decisions.core.runtime)" version="0" />
    <module reference="cd9f1852-e6bc-481e-9ffd-bfcf93751d73(no.uio.mLab.decisions.references.laboratoryTest.runtime)" version="0" />
    <module reference="0f0a8fb0-9593-41c4-900c-15c594642394(no.uio.mLab.decisions.tasks.autovalidation.runtime)" version="0" />
    <module reference="c4c71b02-3736-455d-8937-9cdbab70a659(no.uio.mLab.entities.laboratoryTest.runtime)" version="0" />
  </dependencyVersions>
</solution>

