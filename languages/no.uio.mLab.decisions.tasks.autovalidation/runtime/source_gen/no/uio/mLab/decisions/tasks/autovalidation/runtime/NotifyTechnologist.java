package no.uio.mLab.decisions.tasks.autovalidation.runtime;

/*Generated by MPS */

import no.uio.mLab.decisions.core.runtime.Action;
import no.uio.mLab.decisions.references.laboratoryTest.runtime.ILaboratoryTestReferrer;
import java.util.Objects;

public class NotifyTechnologist extends Action implements ILaboratoryTestReferrer {
  public String test;

  public NotifyTechnologist(String test) {
    this.test = test;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.getClass(), test);
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
      return true;
    }
    if (object == null || this.getClass() != object.getClass()) {
      return false;
    }
    NotifyTechnologist other = (NotifyTechnologist) object;
    return Objects.equals(this.test, other.test);
  }

  @Override
  public String getLaboratoryTestName() {
    return this.test;
  }
}
