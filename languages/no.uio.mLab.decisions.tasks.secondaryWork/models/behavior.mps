<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:fa599934-e8a3-4d84-bc7d-815d9dd482c4(no.uio.mLab.decisions.tasks.secondaryWork.behavior)">
  <persistence version="9" />
  <languages>
    <use id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior" version="1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="pwpt" ref="r:bdf710cc-da96-4882-8743-04e8cf4eb647(no.uio.mLab.decisions.tasks.secondaryWork.translations)" />
    <import index="1yj" ref="r:0c887d67-81ab-420f-9a35-a9f6bf96c975(no.uio.mLab.shared.behavior)" />
    <import index="vbok" ref="r:6cc208d3-a370-49ca-a6ac-ded97805a924(no.uio.mLab.shared.structure)" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" />
    <import index="54w7" ref="r:cbf54237-c566-461d-bab3-000e22c54a23(no.uio.mLab.decisions.tasks.secondaryWork.structure)" />
    <import index="wb6c" ref="r:19d15037-83c1-4ae2-b66a-763828cb8279(no.uio.mLab.decisions.core.behavior)" />
    <import index="7f9y" ref="r:790ac37e-42d3-4dd3-977c-e8fe9cc56a45(no.uio.mLab.decisions.core.structure)" />
    <import index="hyw5" ref="r:b77da972-4430-4cfb-8552-374583961329(no.uio.mLab.decisions.core.utils)" />
  </imports>
  <registry>
    <language id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior">
      <concept id="6496299201655527393" name="jetbrains.mps.lang.behavior.structure.LocalBehaviorMethodCall" flags="nn" index="BsUDl" />
      <concept id="1225194240794" name="jetbrains.mps.lang.behavior.structure.ConceptBehavior" flags="ng" index="13h7C7">
        <reference id="1225194240799" name="concept" index="13h7C2" />
        <child id="1225194240805" name="method" index="13h7CS" />
        <child id="1225194240801" name="constructor" index="13h7CW" />
      </concept>
      <concept id="1225194413805" name="jetbrains.mps.lang.behavior.structure.ConceptConstructorDeclaration" flags="in" index="13hLZK" />
      <concept id="1225194472830" name="jetbrains.mps.lang.behavior.structure.ConceptMethodDeclaration" flags="ng" index="13i0hz">
        <property id="5864038008284099149" name="isStatic" index="2Ki8OM" />
        <property id="1225194472832" name="isVirtual" index="13i0it" />
        <property id="1225194472834" name="isAbstract" index="13i0iv" />
        <reference id="1225194472831" name="overriddenMethod" index="13i0hy" />
      </concept>
      <concept id="1225194691553" name="jetbrains.mps.lang.behavior.structure.ThisNodeExpression" flags="nn" index="13iPFW" />
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="4836112446988635817" name="jetbrains.mps.baseLanguage.structure.UndefinedType" flags="in" index="2jxLKc" />
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="5497648299878491908" name="jetbrains.mps.baseLanguage.structure.BaseVariableReference" flags="nn" index="1M0zk4">
        <reference id="5497648299878491909" name="baseVariableDeclaration" index="1M0zk5" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="1883223317721008708" name="jetbrains.mps.lang.smodel.structure.IfInstanceOfStatement" flags="nn" index="Jncv_">
        <reference id="1883223317721008712" name="nodeConcept" index="JncvD" />
        <child id="1883223317721008709" name="body" index="Jncv$" />
        <child id="1883223317721008711" name="variable" index="JncvA" />
        <child id="1883223317721008710" name="nodeExpression" index="JncvB" />
      </concept>
      <concept id="1883223317721008713" name="jetbrains.mps.lang.smodel.structure.IfInstanceOfVariable" flags="ng" index="JncvC" />
      <concept id="1883223317721107059" name="jetbrains.mps.lang.smodel.structure.IfInstanceOfVarReference" flags="nn" index="Jnkvi" />
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2" />
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="13h7C7" id="6khVixylBl9">
    <property role="3GE5qa" value="shared" />
    <ref role="13h7C2" to="54w7:6khVixylBkJ" resolve="ITranslatableSecondaryWorkTaskConcept" />
    <node concept="13i0hz" id="6khVixylBlk" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayTranslations" />
      <property role="2Ki8OM" value="true" />
      <node concept="3Tm1VV" id="6khVixylBll" role="1B3o_S" />
      <node concept="3uibUv" id="6khVixylCqX" role="3clF45">
        <ref role="3uigEE" to="pwpt:5ZQBr_XMo3W" resolve="ISecondaryWorkTaskTranslations" />
      </node>
      <node concept="3clFbS" id="6khVixylBln" role="3clF47">
        <node concept="3clFbF" id="6khVixylCrx" role="3cqZAp">
          <node concept="10M0yZ" id="6khVixylCrU" role="3clFbG">
            <ref role="3cqZAo" to="pwpt:5ZQBr_XMprO" resolve="displayTranslations" />
            <ref role="1PxDUh" to="pwpt:5ZQBr_XMo3g" resolve="SecondaryWorkTaskTranslationProvider" />
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="6khVixylCsu" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getGenerationTranslations" />
      <property role="2Ki8OM" value="true" />
      <node concept="3Tm1VV" id="6khVixylCsv" role="1B3o_S" />
      <node concept="3uibUv" id="6khVixylCsw" role="3clF45">
        <ref role="3uigEE" to="pwpt:5ZQBr_XMo3W" resolve="ISecondaryWorkTaskTranslations" />
      </node>
      <node concept="3clFbS" id="6khVixylCsx" role="3clF47">
        <node concept="3clFbF" id="6khVixylCsy" role="3cqZAp">
          <node concept="10M0yZ" id="6khVixylCuz" role="3clFbG">
            <ref role="3cqZAo" to="pwpt:1Hxyv4DUGoA" resolve="generationTranslations" />
            <ref role="1PxDUh" to="pwpt:5ZQBr_XMo3g" resolve="SecondaryWorkTaskTranslationProvider" />
          </node>
        </node>
      </node>
    </node>
    <node concept="13hLZK" id="6khVixylBla" role="13h7CW">
      <node concept="3clFbS" id="6khVixylBlb" role="2VODD2" />
    </node>
  </node>
  <node concept="13h7C7" id="6khVixylCIY">
    <ref role="13h7C2" to="54w7:6khVixylCId" resolve="ScheduleSecondaryMicrobiologyWork" />
    <node concept="13hLZK" id="6khVixylCIZ" role="13h7CW">
      <node concept="3clFbS" id="6khVixylCJ0" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6khVixylCJ9" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="6khVixylCJa" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixylCJf" role="3clF47">
        <node concept="3clFbF" id="6khVixylCJk" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixylCZI" role="3clFbG">
            <node concept="BsUDl" id="6khVixylCSW" role="2Oq$k0">
              <ref role="37wK5l" node="6khVixylBlk" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixylD5o" role="2OqNvi">
              <ref role="37wK5l" to="pwpt:6khVixylBOS" resolve="getScheduleSecondaryWorkAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixylCJg" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixylCJl" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="6khVixylCJm" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixylCJr" role="3clF47">
        <node concept="3clFbF" id="6khVixylD6i" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixylDdk" role="3clFbG">
            <node concept="BsUDl" id="6khVixylD6h" role="2Oq$k0">
              <ref role="37wK5l" node="6khVixylBlk" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixylDj6" role="2OqNvi">
              <ref role="37wK5l" to="pwpt:6khVixylBPx" resolve="getScheduleSecondaryWorkDescription" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixylCJs" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixylCJx" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getGenerationAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:4QUW3efxqUv" resolve="getGenerationAlias" />
      <node concept="3Tm1VV" id="6khVixylCJy" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixylCJB" role="3clF47">
        <node concept="3clFbF" id="6khVixylDk0" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixylDqU" role="3clFbG">
            <node concept="BsUDl" id="6khVixylDjZ" role="2Oq$k0">
              <ref role="37wK5l" node="6khVixylCsu" resolve="getGenerationTranslations" />
            </node>
            <node concept="liA8E" id="6khVixylDwA" role="2OqNvi">
              <ref role="37wK5l" to="pwpt:6khVixylBOS" resolve="getScheduleSecondaryWorkAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixylCJC" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixylCJH" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="toDisplayString" />
      <ref role="13i0hy" to="1yj:499Gn2DGTs1" resolve="toDisplayString" />
      <node concept="3Tm1VV" id="6khVixylCJI" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixylCJT" role="3clF47">
        <node concept="3clFbF" id="6khVixylDxC" role="3cqZAp">
          <node concept="2YIFZM" id="6khVixylDxU" role="3clFbG">
            <ref role="37wK5l" to="wyt6:~String.format(java.lang.String,java.lang.Object...):java.lang.String" resolve="format" />
            <ref role="1Pybhc" to="wyt6:~String" resolve="String" />
            <node concept="Xl_RD" id="6khVixylDyX" role="37wK5m">
              <property role="Xl_RC" value="%s %s" />
            </node>
            <node concept="BsUDl" id="6khVixylDGg" role="37wK5m">
              <ref role="37wK5l" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
            </node>
            <node concept="2OqwBi" id="6khVixyIj_X" role="37wK5m">
              <node concept="2OqwBi" id="6khVixyIiUx" role="2Oq$k0">
                <node concept="13iPFW" id="6khVixyIiGV" role="2Oq$k0" />
                <node concept="3TrEf2" id="6khVixyIjaV" role="2OqNvi">
                  <ref role="3Tt5mk" to="54w7:6khVixylVxR" resolve="workReference" />
                </node>
              </node>
              <node concept="2qgKlT" id="6khVixyIjXe" role="2OqNvi">
                <ref role="37wK5l" to="1yj:499Gn2DGTs1" resolve="toDisplayString" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixylCJU" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixylCJZ" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="toGenerationString" />
      <ref role="13i0hy" to="1yj:499Gn2DGU_t" resolve="toGenerationString" />
      <node concept="3Tm1VV" id="6khVixylCK0" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixylCKb" role="3clF47">
        <node concept="3clFbF" id="6khVixylDS1" role="3cqZAp">
          <node concept="2YIFZM" id="6khVixylDSh" role="3clFbG">
            <ref role="1Pybhc" to="wyt6:~String" resolve="String" />
            <ref role="37wK5l" to="wyt6:~String.format(java.lang.String,java.lang.Object...):java.lang.String" resolve="format" />
            <node concept="Xl_RD" id="6khVixylDTl" role="37wK5m">
              <property role="Xl_RC" value="%s %s" />
            </node>
            <node concept="BsUDl" id="6khVixylE94" role="37wK5m">
              <ref role="37wK5l" to="1yj:4QUW3efxqUv" resolve="getGenerationAlias" />
            </node>
            <node concept="2OqwBi" id="6khVixyIkZc" role="37wK5m">
              <node concept="2OqwBi" id="6khVixyIkjS" role="2Oq$k0">
                <node concept="13iPFW" id="6khVixyIk6i" role="2Oq$k0" />
                <node concept="3TrEf2" id="6khVixyIk$i" role="2OqNvi">
                  <ref role="3Tt5mk" to="54w7:6khVixylVxR" resolve="workReference" />
                </node>
              </node>
              <node concept="2qgKlT" id="6khVixyIlmt" role="2OqNvi">
                <ref role="37wK5l" to="1yj:499Gn2DGU_t" resolve="toGenerationString" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixylCKc" role="3clF45" />
    </node>
    <node concept="13i0hz" id="65epL7MjBUk" role="13h7CS">
      <property role="TrG5h" value="matches" />
      <property role="13i0it" value="false" />
      <property role="13i0iv" value="false" />
      <ref role="13i0hy" to="wb6c:6LTgXmMs$_D" resolve="matches" />
      <node concept="3Tm1VV" id="65epL7MjBUl" role="1B3o_S" />
      <node concept="3clFbS" id="65epL7MjBUy" role="3clF47">
        <node concept="Jncv_" id="65epL7MjCh$" role="3cqZAp">
          <ref role="JncvD" to="54w7:6khVixylCId" resolve="ScheduleSecondaryMicrobiologyWork" />
          <node concept="37vLTw" id="65epL7MjCi1" role="JncvB">
            <ref role="3cqZAo" node="65epL7MjBUz" resolve="node" />
          </node>
          <node concept="3clFbS" id="65epL7MjChA" role="Jncv$">
            <node concept="3cpWs6" id="65epL7MjCju" role="3cqZAp">
              <node concept="2OqwBi" id="65epL7MjDfF" role="3cqZAk">
                <node concept="2OqwBi" id="65epL7MjCyr" role="2Oq$k0">
                  <node concept="13iPFW" id="65epL7MjCkU" role="2Oq$k0" />
                  <node concept="3TrEf2" id="65epL7MjCMt" role="2OqNvi">
                    <ref role="3Tt5mk" to="54w7:6khVixylVxR" resolve="workReference" />
                  </node>
                </node>
                <node concept="2qgKlT" id="65epL7MjD$B" role="2OqNvi">
                  <ref role="37wK5l" to="wb6c:6LTgXmMs$_D" resolve="matches" />
                  <node concept="2OqwBi" id="65epL7MjE6n" role="37wK5m">
                    <node concept="Jnkvi" id="65epL7MjDKM" role="2Oq$k0">
                      <ref role="1M0zk5" node="65epL7MjChB" resolve="action" />
                    </node>
                    <node concept="3TrEf2" id="65epL7MjEw_" role="2OqNvi">
                      <ref role="3Tt5mk" to="54w7:6khVixylVxR" resolve="workReference" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="JncvC" id="65epL7MjChB" role="JncvA">
            <property role="TrG5h" value="action" />
            <node concept="2jxLKc" id="65epL7MjChC" role="1tU5fm" />
          </node>
        </node>
        <node concept="3clFbF" id="65epL7MjEOi" role="3cqZAp">
          <node concept="2YIFZM" id="65epL7MjF3D" role="3clFbG">
            <ref role="37wK5l" to="hyw5:1I84Bf7XEgz" resolve="createMismatch" />
            <ref role="1Pybhc" to="hyw5:6LTgXmO6cZw" resolve="JoinPointMatch" />
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="65epL7MjBUz" role="3clF46">
        <property role="TrG5h" value="node" />
        <node concept="3Tqbb2" id="65epL7MjBU$" role="1tU5fm" />
      </node>
      <node concept="3uibUv" id="65epL7MjBU_" role="3clF45">
        <ref role="3uigEE" to="hyw5:6LTgXmO6cZw" resolve="JoinPointMatch" />
      </node>
    </node>
  </node>
</model>

