<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:cbf54237-c566-461d-bab3-000e22c54a23(no.uio.mLab.decisions.tasks.secondaryWork.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="6" />
    <devkit ref="78434eb8-b0e5-444b-850d-e7c4ad2da9ab(jetbrains.mps.devkit.aspect.structure)" />
  </languages>
  <imports>
    <import index="7f9y" ref="r:790ac37e-42d3-4dd3-977c-e8fe9cc56a45(no.uio.mLab.decisions.core.structure)" />
    <import index="ok0m" ref="r:4db7f216-3df2-435e-8be1-a0ab7e4a26b5(no.uio.mLab.decisions.references.microbiology.structure)" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
      </concept>
      <concept id="1169125989551" name="jetbrains.mps.lang.structure.structure.InterfaceConceptDeclaration" flags="ig" index="PlHQZ" />
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="PlHQZ" id="6khVixylBkJ">
    <property role="EcuMT" value="7282862830139110703" />
    <property role="TrG5h" value="ITranslatableSecondaryWorkTaskConcept" />
    <property role="3GE5qa" value="shared" />
  </node>
  <node concept="1TIwiD" id="6khVixylCId">
    <property role="EcuMT" value="7282862830139116429" />
    <property role="TrG5h" value="ScheduleSecondaryMicrobiologyWork" />
    <ref role="1TJDcQ" to="7f9y:1mAGFBKnGHq" resolve="Action" />
    <node concept="PrWs8" id="6khVixylCIp" role="PzmwI">
      <ref role="PrY4T" node="6khVixylBkJ" resolve="ITranslatableSecondaryWorkTaskConcept" />
    </node>
    <node concept="1TJgyj" id="6khVixylVxR" role="1TKVEi">
      <property role="IQ2ns" value="7282862830139193463" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="workReference" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="ok0m:6khVixylO15" resolve="SecondaryMicrobiologyWorkReference" />
    </node>
  </node>
</model>

