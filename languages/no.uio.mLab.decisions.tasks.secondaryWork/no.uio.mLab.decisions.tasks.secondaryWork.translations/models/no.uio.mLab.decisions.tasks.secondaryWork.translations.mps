<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:bdf710cc-da96-4882-8743-04e8cf4eb647(no.uio.mLab.decisions.tasks.secondaryWork.translations)">
  <persistence version="9" />
  <languages>
    <use id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage" version="6" />
  </languages>
  <imports>
    <import index="9igz" ref="r:0ba33904-c124-449b-bde4-640f55a71953(no.uio.mLab.shared.translations)" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1465982738277781862" name="jetbrains.mps.baseLanguage.structure.PlaceholderMember" flags="ng" index="2tJIrI" />
      <concept id="1188207840427" name="jetbrains.mps.baseLanguage.structure.AnnotationInstance" flags="nn" index="2AHcQZ">
        <reference id="1188208074048" name="annotation" index="2AI5Lk" />
      </concept>
      <concept id="1188208481402" name="jetbrains.mps.baseLanguage.structure.HasAnnotation" flags="ng" index="2AJDlI">
        <child id="1188208488637" name="annotation" index="2AJF6D" />
      </concept>
      <concept id="2820489544401957797" name="jetbrains.mps.baseLanguage.structure.DefaultClassCreator" flags="nn" index="HV5vD">
        <reference id="2820489544401957798" name="classifier" index="HV5vE" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1070462154015" name="jetbrains.mps.baseLanguage.structure.StaticFieldDeclaration" flags="ig" index="Wx3nA">
        <property id="6468716278899126575" name="isVolatile" index="2dlcS1" />
        <property id="6468716278899125786" name="isTransient" index="2dld4O" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1081236700938" name="jetbrains.mps.baseLanguage.structure.StaticMethodDeclaration" flags="ig" index="2YIFZL" />
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu">
        <property id="1221565133444" name="isFinal" index="1EXbeo" />
        <child id="1095933932569" name="implementedInterface" index="EKbjA" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <property id="1176718929932" name="isFinal" index="3TUv4t" />
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <property id="4276006055363816570" name="isSynchronized" index="od$2w" />
        <property id="1181808852946" name="isFinal" index="DiZV1" />
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123165" name="jetbrains.mps.baseLanguage.structure.InstanceMethodDeclaration" flags="ig" index="3clFb_">
        <property id="1178608670077" name="isAbstract" index="1EzhhJ" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1107461130800" name="jetbrains.mps.baseLanguage.structure.Classifier" flags="ng" index="3pOWGL">
        <child id="5375687026011219971" name="member" index="jymVt" unordered="true" />
      </concept>
      <concept id="7812454656619025416" name="jetbrains.mps.baseLanguage.structure.MethodDeclaration" flags="ng" index="1rXfSm">
        <property id="8355037393041754995" name="isNative" index="2aFKle" />
      </concept>
      <concept id="7812454656619025412" name="jetbrains.mps.baseLanguage.structure.LocalMethodCall" flags="nn" index="1rXfSq" />
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1107796713796" name="jetbrains.mps.baseLanguage.structure.Interface" flags="ig" index="3HP615" />
      <concept id="1163670490218" name="jetbrains.mps.baseLanguage.structure.SwitchStatement" flags="nn" index="3KaCP$">
        <child id="1163670592366" name="defaultBlock" index="3Kb1Dw" />
        <child id="1163670766145" name="expression" index="3KbGdf" />
        <child id="1163670772911" name="case" index="3KbHQx" />
      </concept>
      <concept id="1163670641947" name="jetbrains.mps.baseLanguage.structure.SwitchCase" flags="ng" index="3KbdKl">
        <child id="1163670677455" name="expression" index="3Kbmr1" />
        <child id="1163670683720" name="body" index="3Kbo56" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
      <concept id="1146644623116" name="jetbrains.mps.baseLanguage.structure.PrivateVisibility" flags="nn" index="3Tm6S6" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="312cEu" id="5ZQBr_XMo3g">
    <property role="TrG5h" value="SecondaryWorkTaskTranslationProvider" />
    <property role="1EXbeo" value="true" />
    <property role="3GE5qa" value="internationalization" />
    <node concept="Wx3nA" id="5ZQBr_XMprO" role="jymVt">
      <property role="TrG5h" value="displayTranslations" />
      <property role="2dlcS1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="2dld4O" value="false" />
      <node concept="3uibUv" id="5ZQBr_XMprQ" role="1tU5fm">
        <ref role="3uigEE" node="5ZQBr_XMo3W" resolve="ISecondaryWorkTaskTranslations" />
      </node>
      <node concept="3Tm1VV" id="5ZQBr_XMprR" role="1B3o_S" />
      <node concept="1rXfSq" id="5Wfdz$0v$Nz" role="33vP2m">
        <ref role="37wK5l" node="5Wfdz$0v$KX" resolve="init" />
        <node concept="10M0yZ" id="ofcHXfRkX0" role="37wK5m">
          <ref role="1PxDUh" to="9igz:5fXsrCVmxTN" resolve="TranslationConfiguration" />
          <ref role="3cqZAo" to="9igz:5fXsrCVmzVm" resolve="displayLanguage" />
        </node>
      </node>
    </node>
    <node concept="Wx3nA" id="1Hxyv4DUGoA" role="jymVt">
      <property role="TrG5h" value="generationTranslations" />
      <property role="2dlcS1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="2dld4O" value="false" />
      <node concept="3uibUv" id="1Hxyv4DUGoB" role="1tU5fm">
        <ref role="3uigEE" node="5ZQBr_XMo3W" resolve="ISecondaryWorkTaskTranslations" />
      </node>
      <node concept="3Tm1VV" id="1Hxyv4DUGoC" role="1B3o_S" />
      <node concept="1rXfSq" id="1Hxyv4DUGoD" role="33vP2m">
        <ref role="37wK5l" node="5Wfdz$0v$KX" resolve="init" />
        <node concept="10M0yZ" id="ofcHXfRkXM" role="37wK5m">
          <ref role="1PxDUh" to="9igz:5fXsrCVmxTN" resolve="TranslationConfiguration" />
          <ref role="3cqZAo" to="9igz:5fXsrCVmzVN" resolve="generationLanguage" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="5fXsrCVpf1R" role="jymVt" />
    <node concept="2YIFZL" id="5Wfdz$0v$KX" role="jymVt">
      <property role="TrG5h" value="init" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3clFbS" id="5fXsrCVpf2J" role="3clF47">
        <node concept="3KaCP$" id="5fXsrCVpf3u" role="3cqZAp">
          <node concept="3KbdKl" id="5fXsrCVpfye" role="3KbHQx">
            <node concept="Xl_RD" id="5fXsrCVpfyH" role="3Kbmr1">
              <property role="Xl_RC" value="no" />
            </node>
            <node concept="3clFbS" id="5fXsrCVpfyg" role="3Kbo56">
              <node concept="3cpWs6" id="5fXsrCVpfzh" role="3cqZAp">
                <node concept="2ShNRf" id="5fXsrCVpf_6" role="3cqZAk">
                  <node concept="HV5vD" id="5fXsrCVpfEG" role="2ShVmc">
                    <ref role="HV5vE" node="5ZQBr_XMobA" resolve="NoSecondaryWorkTaskTranslations" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="5fXsrCVpfFN" role="3Kb1Dw">
            <node concept="3cpWs6" id="5fXsrCVpfHd" role="3cqZAp">
              <node concept="2ShNRf" id="5fXsrCVpfJo" role="3cqZAk">
                <node concept="HV5vD" id="5fXsrCVpfPK" role="2ShVmc">
                  <ref role="HV5vE" node="17XAtu8bhIo" resolve="EnSecondaryWorkTaskTranslations" />
                </node>
              </node>
            </node>
          </node>
          <node concept="37vLTw" id="1Hxyv4DUGmq" role="3KbGdf">
            <ref role="3cqZAo" node="1Hxyv4DUGgF" resolve="language" />
          </node>
        </node>
      </node>
      <node concept="3uibUv" id="5fXsrCVpf2z" role="3clF45">
        <ref role="3uigEE" node="5ZQBr_XMo3W" resolve="ISecondaryWorkTaskTranslations" />
      </node>
      <node concept="3Tm6S6" id="5fXsrCVpf2i" role="1B3o_S" />
      <node concept="37vLTG" id="1Hxyv4DUGgF" role="3clF46">
        <property role="TrG5h" value="language" />
        <node concept="17QB3L" id="1Hxyv4DUGgE" role="1tU5fm" />
      </node>
    </node>
    <node concept="3Tm1VV" id="5ZQBr_XMo3h" role="1B3o_S" />
  </node>
  <node concept="3HP615" id="5ZQBr_XMo3W">
    <property role="TrG5h" value="ISecondaryWorkTaskTranslations" />
    <property role="3GE5qa" value="internationalization" />
    <node concept="3clFb_" id="6khVixylBOS" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getScheduleSecondaryWorkAlias" />
      <node concept="3clFbS" id="6khVixylBOV" role="3clF47" />
      <node concept="3Tm1VV" id="6khVixylBOW" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixylBOL" role="3clF45" />
    </node>
    <node concept="3clFb_" id="6khVixylBPx" role="jymVt">
      <property role="1EzhhJ" value="true" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getScheduleSecondaryWorkDescription" />
      <node concept="3clFbS" id="6khVixylBP$" role="3clF47" />
      <node concept="3Tm1VV" id="6khVixylBP_" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixylBPm" role="3clF45" />
    </node>
    <node concept="3Tm1VV" id="5ZQBr_XMo3X" role="1B3o_S" />
  </node>
  <node concept="312cEu" id="17XAtu8bhIo">
    <property role="TrG5h" value="EnSecondaryWorkTaskTranslations" />
    <property role="3GE5qa" value="localizations" />
    <node concept="3Tm1VV" id="17XAtu8bhIp" role="1B3o_S" />
    <node concept="3uibUv" id="17XAtu8bhJg" role="EKbjA">
      <ref role="3uigEE" node="5ZQBr_XMo3W" resolve="ISecondaryWorkTaskTranslations" />
    </node>
    <node concept="3clFb_" id="6khVixylBSK" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getScheduleSecondaryWorkAlias" />
      <node concept="3Tm1VV" id="6khVixylBSM" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixylBSN" role="3clF45" />
      <node concept="3clFbS" id="6khVixylBSO" role="3clF47">
        <node concept="3clFbF" id="6khVixylBVD" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixylBVC" role="3clFbG">
            <property role="Xl_RC" value="schedule secondary microbiology work" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixylBSP" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="6khVixylBSQ" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getScheduleSecondaryWorkDescription" />
      <node concept="3Tm1VV" id="6khVixylBSS" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixylBST" role="3clF45" />
      <node concept="3clFbS" id="6khVixylBSU" role="3clF47">
        <node concept="3clFbF" id="6khVixylBXI" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixylBXH" role="3clFbG">
            <property role="Xl_RC" value="" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixylBSV" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="5ZQBr_XMobA">
    <property role="TrG5h" value="NoSecondaryWorkTaskTranslations" />
    <property role="3GE5qa" value="localizations" />
    <node concept="3Tm1VV" id="5ZQBr_XMobB" role="1B3o_S" />
    <node concept="3uibUv" id="5ZQBr_XMocM" role="EKbjA">
      <ref role="3uigEE" node="5ZQBr_XMo3W" resolve="ISecondaryWorkTaskTranslations" />
    </node>
    <node concept="3clFb_" id="6khVixylBYz" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getScheduleSecondaryWorkAlias" />
      <node concept="3Tm1VV" id="6khVixylBY_" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixylBYA" role="3clF45" />
      <node concept="3clFbS" id="6khVixylBYB" role="3clF47">
        <node concept="3clFbF" id="6khVixylC1s" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixylC1r" role="3clFbG">
            <property role="Xl_RC" value="bestill mikrobiologi sekundærarbeid" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixylBYC" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="3clFb_" id="6khVixylBYD" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="getScheduleSecondaryWorkDescription" />
      <node concept="3Tm1VV" id="6khVixylBYF" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixylBYG" role="3clF45" />
      <node concept="3clFbS" id="6khVixylBYH" role="3clF47">
        <node concept="3clFbF" id="6khVixylC5z" role="3cqZAp">
          <node concept="Xl_RD" id="6khVixylC5y" role="3clFbG">
            <property role="Xl_RC" value="" />
          </node>
        </node>
      </node>
      <node concept="2AHcQZ" id="6khVixylBYI" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
  </node>
</model>

