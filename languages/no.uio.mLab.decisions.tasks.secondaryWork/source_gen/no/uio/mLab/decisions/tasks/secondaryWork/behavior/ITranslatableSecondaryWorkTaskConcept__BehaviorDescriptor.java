package no.uio.mLab.decisions.tasks.secondaryWork.behavior;

/*Generated by MPS */

import jetbrains.mps.core.aspects.behaviour.BaseBHDescriptor;
import org.jetbrains.mps.openapi.language.SAbstractConcept;
import jetbrains.mps.smodel.adapter.structure.MetaAdapterFactory;
import jetbrains.mps.core.aspects.behaviour.api.BehaviorRegistry;
import jetbrains.mps.smodel.language.ConceptRegistry;
import jetbrains.mps.core.aspects.behaviour.api.SMethod;
import no.uio.mLab.decisions.tasks.secondaryWork.translations.ISecondaryWorkTaskTranslations;
import jetbrains.mps.core.aspects.behaviour.SMethodBuilder;
import jetbrains.mps.core.aspects.behaviour.SJavaCompoundTypeImpl;
import jetbrains.mps.core.aspects.behaviour.SModifiersImpl;
import jetbrains.mps.core.aspects.behaviour.AccessPrivileges;
import java.util.List;
import java.util.Arrays;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.mps.openapi.model.SNode;
import no.uio.mLab.decisions.tasks.secondaryWork.translations.SecondaryWorkTaskTranslationProvider;
import jetbrains.mps.core.aspects.behaviour.api.SConstructor;
import org.jetbrains.annotations.Nullable;
import jetbrains.mps.core.aspects.behaviour.api.BHMethodNotFoundException;

public final class ITranslatableSecondaryWorkTaskConcept__BehaviorDescriptor extends BaseBHDescriptor {
  private static final SAbstractConcept CONCEPT = MetaAdapterFactory.getInterfaceConcept(0x1b13d5a180e447ffL, 0xb7734b4b3c9489d3L, 0x6511ed286256752fL, "no.uio.mLab.decisions.tasks.secondaryWork.structure.ITranslatableSecondaryWorkTaskConcept");
  private static final BehaviorRegistry REGISTRY = ConceptRegistry.getInstance().getBehaviorRegistry();

  public static final SMethod<ISecondaryWorkTaskTranslations> getDisplayTranslations_id6khVixylBlk = new SMethodBuilder<ISecondaryWorkTaskTranslations>(new SJavaCompoundTypeImpl(ISecondaryWorkTaskTranslations.class)).name("getDisplayTranslations").modifiers(SModifiersImpl.create(1, AccessPrivileges.PUBLIC)).concept(CONCEPT).id("6khVixylBlk").registry(REGISTRY).build();
  public static final SMethod<ISecondaryWorkTaskTranslations> getGenerationTranslations_id6khVixylCsu = new SMethodBuilder<ISecondaryWorkTaskTranslations>(new SJavaCompoundTypeImpl(ISecondaryWorkTaskTranslations.class)).name("getGenerationTranslations").modifiers(SModifiersImpl.create(1, AccessPrivileges.PUBLIC)).concept(CONCEPT).id("6khVixylCsu").registry(REGISTRY).build();

  private static final List<SMethod<?>> BH_METHODS = Arrays.<SMethod<?>>asList(getDisplayTranslations_id6khVixylBlk, getGenerationTranslations_id6khVixylCsu);

  private static void ___init___(@NotNull SNode __thisNode__) {
  }

  /*package*/ static ISecondaryWorkTaskTranslations getDisplayTranslations_id6khVixylBlk(@NotNull SAbstractConcept __thisConcept__) {
    return SecondaryWorkTaskTranslationProvider.displayTranslations;
  }
  /*package*/ static ISecondaryWorkTaskTranslations getGenerationTranslations_id6khVixylCsu(@NotNull SAbstractConcept __thisConcept__) {
    return SecondaryWorkTaskTranslationProvider.generationTranslations;
  }

  /*package*/ ITranslatableSecondaryWorkTaskConcept__BehaviorDescriptor() {
    super(REGISTRY);
  }

  @Override
  protected void initNode(@NotNull SNode node, @NotNull SConstructor constructor, @Nullable Object[] parameters) {
    ___init___(node);
  }

  @Override
  protected <T> T invokeSpecial0(@NotNull SNode node, @NotNull SMethod<T> method, @Nullable Object[] parameters) {
    int methodIndex = BH_METHODS.indexOf(method);
    if (methodIndex < 0) {
      throw new BHMethodNotFoundException(this, method);
    }
    switch (methodIndex) {
      default:
        throw new BHMethodNotFoundException(this, method);
    }
  }

  @Override
  protected <T> T invokeSpecial0(@NotNull SAbstractConcept concept, @NotNull SMethod<T> method, @Nullable Object[] parameters) {
    int methodIndex = BH_METHODS.indexOf(method);
    if (methodIndex < 0) {
      throw new BHMethodNotFoundException(this, method);
    }
    switch (methodIndex) {
      case 0:
        return (T) ((ISecondaryWorkTaskTranslations) getDisplayTranslations_id6khVixylBlk(concept));
      case 1:
        return (T) ((ISecondaryWorkTaskTranslations) getGenerationTranslations_id6khVixylCsu(concept));
      default:
        throw new BHMethodNotFoundException(this, method);
    }
  }

  @NotNull
  @Override
  public List<SMethod<?>> getDeclaredMethods() {
    return BH_METHODS;
  }

  @NotNull
  @Override
  public SAbstractConcept getConcept() {
    return CONCEPT;
  }
}
