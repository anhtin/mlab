package no.uio.mLab.decisions.tasks.secondaryWork.structure;

/*Generated by MPS */

import jetbrains.mps.lang.smodel.LanguageConceptIndex;
import jetbrains.mps.lang.smodel.LanguageConceptIndexBuilder;
import jetbrains.mps.smodel.adapter.ids.SConceptId;
import org.jetbrains.mps.openapi.language.SAbstractConcept;

public final class LanguageConceptSwitch {
  private final LanguageConceptIndex myIndex;
  public static final int ITranslatableSecondaryWorkTaskConcept = 0;
  public static final int ScheduleSecondaryMicrobiologyWork = 1;

  public LanguageConceptSwitch() {
    LanguageConceptIndexBuilder builder = new LanguageConceptIndexBuilder(0x1b13d5a180e447ffL, 0xb7734b4b3c9489d3L);
    builder.put(0x6511ed286256752fL, ITranslatableSecondaryWorkTaskConcept);
    builder.put(0x6511ed2862568b8dL, ScheduleSecondaryMicrobiologyWork);
    myIndex = builder.seal();
  }

  /*package*/ int index(SConceptId cid) {
    return myIndex.index(cid);
  }

  public int index(SAbstractConcept concept) {
    return myIndex.index(concept);
  }
}
