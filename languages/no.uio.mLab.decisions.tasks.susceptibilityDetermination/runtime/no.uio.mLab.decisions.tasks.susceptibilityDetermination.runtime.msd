<?xml version="1.0" encoding="UTF-8"?>
<solution name="no.uio.mLab.decisions.tasks.susceptibilityDetermination.runtime" uuid="e24f1ca6-1598-45b1-93b8-a442b4bf8837" moduleVersion="0" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
  </models>
  <sourcePath />
  <languageVersions />
  <dependencyVersions>
    <module reference="e24f1ca6-1598-45b1-93b8-a442b4bf8837(no.uio.mLab.decisions.tasks.susceptibilityDetermination.runtime)" version="0" />
  </dependencyVersions>
</solution>

