<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:ac703613-99db-4556-9c85-bfc0d1c1eea4(no.uio.mLab.entities.biochemistry.behavior)">
  <persistence version="9" />
  <languages>
    <use id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior" version="1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="6dfx" ref="r:4492d096-097c-4532-b592-a000ef58e529(no.uio.mLab.entities.laboratoryTest.behavior)" />
    <import index="1yj" ref="r:0c887d67-81ab-420f-9a35-a9f6bf96c975(no.uio.mLab.shared.behavior)" />
    <import index="aqtt" ref="r:4712535c-9b1a-427f-a086-1861c129a613(no.uio.mLab.entities.biochemistry.translations)" />
    <import index="4v7t" ref="r:3abf6acc-a4c5-4ee7-8b22-79c4941d18e6(no.uio.mLab.entities.biochemistry.structure)" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" />
    <import index="vbok" ref="r:6cc208d3-a370-49ca-a6ac-ded97805a924(no.uio.mLab.shared.structure)" />
  </imports>
  <registry>
    <language id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior">
      <concept id="6496299201655527393" name="jetbrains.mps.lang.behavior.structure.LocalBehaviorMethodCall" flags="nn" index="BsUDl" />
      <concept id="1225194240794" name="jetbrains.mps.lang.behavior.structure.ConceptBehavior" flags="ng" index="13h7C7">
        <reference id="1225194240799" name="concept" index="13h7C2" />
        <child id="1225194240805" name="method" index="13h7CS" />
        <child id="1225194240801" name="constructor" index="13h7CW" />
      </concept>
      <concept id="1225194413805" name="jetbrains.mps.lang.behavior.structure.ConceptConstructorDeclaration" flags="in" index="13hLZK" />
      <concept id="1225194472830" name="jetbrains.mps.lang.behavior.structure.ConceptMethodDeclaration" flags="ng" index="13i0hz">
        <property id="5864038008284099149" name="isStatic" index="2Ki8OM" />
        <property id="1225194472832" name="isVirtual" index="13i0it" />
        <property id="1225194472834" name="isAbstract" index="13i0iv" />
        <reference id="1225194472831" name="overriddenMethod" index="13i0hy" />
      </concept>
      <concept id="1225194691553" name="jetbrains.mps.lang.behavior.structure.ThisNodeExpression" flags="nn" index="13iPFW" />
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
      </concept>
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="13h7C7" id="499Gn2D9FBp">
    <ref role="13h7C2" to="4v7t:499Gn2D9F$O" resolve="ITranslatableBiochemistryEntitiesConcept" />
    <node concept="13i0hz" id="499Gn2D9FB$" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getDisplayTranslations" />
      <node concept="3Tm1VV" id="499Gn2D9FB_" role="1B3o_S" />
      <node concept="3uibUv" id="ofcHXfYUY2" role="3clF45">
        <ref role="3uigEE" to="aqtt:4zMac8rUNtP" resolve="IBiochemistryEntitiesTranslations" />
      </node>
      <node concept="3clFbS" id="499Gn2D9FBB" role="3clF47">
        <node concept="3clFbF" id="499Gn2D9FD1" role="3cqZAp">
          <node concept="10M0yZ" id="ofcHXfYUYQ" role="3clFbG">
            <ref role="3cqZAo" to="aqtt:4zMac8rVBeK" resolve="displayTranslations" />
            <ref role="1PxDUh" to="aqtt:4zMac8rUNsN" resolve="BiochemistryEntitiesTranslationProvider" />
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="499Gn2D9FEe" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getGenerationTranslations" />
      <node concept="3Tm1VV" id="499Gn2D9FEf" role="1B3o_S" />
      <node concept="3uibUv" id="ofcHXfYUZr" role="3clF45">
        <ref role="3uigEE" to="aqtt:4zMac8rUNtP" resolve="IBiochemistryEntitiesTranslations" />
      </node>
      <node concept="3clFbS" id="499Gn2D9FEh" role="3clF47">
        <node concept="3clFbF" id="499Gn2D9FEi" role="3cqZAp">
          <node concept="10M0yZ" id="ofcHXfYV00" role="3clFbG">
            <ref role="3cqZAo" to="aqtt:1Hxyv4DUmfq" resolve="generationTranslations" />
            <ref role="1PxDUh" to="aqtt:4zMac8rUNsN" resolve="BiochemistryEntitiesTranslationProvider" />
          </node>
        </node>
      </node>
    </node>
    <node concept="13hLZK" id="499Gn2D9FBq" role="13h7CW">
      <node concept="3clFbS" id="499Gn2D9FBr" role="2VODD2" />
    </node>
  </node>
  <node concept="13h7C7" id="499Gn2DH13P">
    <property role="3GE5qa" value="" />
    <ref role="13h7C2" to="4v7t:1mAGFBLdp3F" resolve="BiochemistrySpecialistType" />
    <node concept="13hLZK" id="499Gn2DH13Q" role="13h7CW">
      <node concept="3clFbS" id="499Gn2DH13R" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6khVixy1s1K" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="6khVixy1s1L" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixy1s1S" role="3clF47">
        <node concept="3clFbF" id="6khVixy1stC" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixy1s$$" role="3clFbG">
            <node concept="BsUDl" id="6khVixy1stB" role="2Oq$k0">
              <ref role="37wK5l" node="499Gn2D9FB$" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixy1sEl" role="2OqNvi">
              <ref role="37wK5l" to="aqtt:6khVixy1gIO" resolve="getSpecialistTypeAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixy1s1T" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixy1s1Y" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="6khVixy1s1Z" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixy1s24" role="3clF47">
        <node concept="3clFbF" id="6khVixy1s29" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixy1sF7" role="3clFbG">
            <node concept="BsUDl" id="6khVixy1sF8" role="2Oq$k0">
              <ref role="37wK5l" node="499Gn2D9FB$" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixy1sF9" role="2OqNvi">
              <ref role="37wK5l" to="aqtt:6khVixy1gKW" resolve="getSpecialistTypeDescription" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixy1s25" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="7lYCqhuj$2v">
    <property role="3GE5qa" value="" />
    <ref role="13h7C2" to="4v7t:5Wfdz$0qWIu" resolve="BiochemistryTest" />
    <node concept="13hLZK" id="7lYCqhuj$2w" role="13h7CW">
      <node concept="3clFbS" id="7lYCqhuj$2x" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6khVixy1ihe" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="6khVixy1ihf" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixy1ihm" role="3clF47">
        <node concept="3clFbF" id="6khVixy1ihr" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixy1jBK" role="3clFbG">
            <node concept="BsUDl" id="6khVixy1jwP" role="2Oq$k0">
              <ref role="37wK5l" node="499Gn2D9FB$" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixy1jHx" role="2OqNvi">
              <ref role="37wK5l" to="aqtt:6khVixy1gGG" resolve="getBiochemistryTestAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixy1ihn" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixy1ihE" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="6khVixy1ihF" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixy1ihK" role="3clF47">
        <node concept="3clFbF" id="6khVixy1ihP" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixy1jJY" role="3clFbG">
            <node concept="BsUDl" id="6khVixy1jJZ" role="2Oq$k0">
              <ref role="37wK5l" node="499Gn2D9FB$" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixy1jK0" role="2OqNvi">
              <ref role="37wK5l" to="aqtt:6khVixy1gH$" resolve="getBiochemistryTestDescription" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixy1ihL" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixy1oDR" role="13h7CS">
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getUnitLabel" />
      <node concept="3Tm1VV" id="6khVixy1oDS" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixy1oHL" role="3clF45" />
      <node concept="3clFbS" id="6khVixy1oDU" role="3clF47">
        <node concept="3clFbF" id="6khVixy1oI4" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixy1kka" role="3clFbG">
            <node concept="BsUDl" id="6khVixy1kkb" role="2Oq$k0">
              <ref role="37wK5l" node="499Gn2D9FB$" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixy1kkc" role="2OqNvi">
              <ref role="37wK5l" to="aqtt:6khVixy1gMZ" resolve="getBiochemistryTestUnitLabel" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="7lYCqhuj$2E" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayUnit" />
      <ref role="13i0hy" to="1yj:7lYCqhujn6u" resolve="getDisplayUnit" />
      <node concept="3Tm1VV" id="7lYCqhuj$2F" role="1B3o_S" />
      <node concept="3clFbS" id="7lYCqhuj$2Q" role="3clF47">
        <node concept="3clFbF" id="7lYCqhuj$6Y" role="3cqZAp">
          <node concept="2OqwBi" id="7lYCqhuj$m5" role="3clFbG">
            <node concept="13iPFW" id="7lYCqhuj$6X" role="2Oq$k0" />
            <node concept="3TrcHB" id="7lYCqhuj$Dn" role="2OqNvi">
              <ref role="3TsBF5" to="4v7t:7lYCqhuj$24" resolve="unit" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="7lYCqhuj$2R" role="3clF45" />
    </node>
    <node concept="13i0hz" id="4B5aqq6xwwr" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getGenerationUnit" />
      <ref role="13i0hy" to="1yj:4B5aqq6oFwi" resolve="getGenerationUnit" />
      <node concept="3Tm1VV" id="4B5aqq6xwws" role="1B3o_S" />
      <node concept="3clFbS" id="4B5aqq6xwwB" role="3clF47">
        <node concept="3clFbF" id="4B5aqq6xwCz" role="3cqZAp">
          <node concept="2OqwBi" id="4B5aqq6xwTt" role="3clFbG">
            <node concept="13iPFW" id="4B5aqq6xwCu" role="2Oq$k0" />
            <node concept="3TrcHB" id="4B5aqq6xxg1" role="2OqNvi">
              <ref role="3TsBF5" to="4v7t:7lYCqhuj$24" resolve="unit" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="4B5aqq6xwwC" role="3clF45" />
    </node>
  </node>
</model>

