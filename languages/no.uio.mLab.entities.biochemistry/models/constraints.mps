<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:0821f11f-7411-4efa-8623-626aabdd57c5(no.uio.mLab.entities.biochemistry.constraints)">
  <persistence version="9" />
  <languages>
    <use id="3f4bc5f5-c6c1-4a28-8b10-c83066ffa4a1" name="jetbrains.mps.lang.constraints" version="4" />
    <devkit ref="00000000-0000-4000-0000-5604ebd4f22c(jetbrains.mps.devkit.aspect.constraints)" />
  </languages>
  <imports />
  <registry />
</model>

