<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:34b5238d-c4b4-4bfa-b73c-d92c11382985(no.uio.mLab.entities.biochemistry.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="11" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="4v7t" ref="r:3abf6acc-a4c5-4ee7-8b22-79c4941d18e6(no.uio.mLab.entities.biochemistry.structure)" implicit="true" />
    <import index="uubs" ref="r:481ae0fe-9100-4bb8-9488-731d6cfeba53(no.uio.mLab.shared.editor)" implicit="true" />
    <import index="z6e9" ref="r:ac703613-99db-4556-9c85-bfc0d1c1eea4(no.uio.mLab.entities.biochemistry.behavior)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi" />
      <concept id="1106270549637" name="jetbrains.mps.lang.editor.structure.CellLayout_Horizontal" flags="nn" index="2iRfu4" />
      <concept id="1106270571710" name="jetbrains.mps.lang.editor.structure.CellLayout_Vertical" flags="nn" index="2iRkQZ" />
      <concept id="1142886811589" name="jetbrains.mps.lang.editor.structure.ConceptFunctionParameter_node" flags="nn" index="pncrf" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="8383079901754291618" name="jetbrains.mps.lang.editor.structure.CellModel_NextEditor" flags="ng" index="B$lHz" />
      <concept id="8313721352726366579" name="jetbrains.mps.lang.editor.structure.CellModel_Empty" flags="ng" index="35HoNQ" />
      <concept id="1381004262292414836" name="jetbrains.mps.lang.editor.structure.ICellStyle" flags="ng" index="1k5N5V">
        <reference id="1381004262292426837" name="parentStyleClass" index="1k5W1q" />
      </concept>
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <property id="1139852716018" name="noTargetText" index="1$x2rV" />
        <property id="1140114345053" name="allowEmptyText" index="1O74Pk" />
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389446423" name="jetbrains.mps.lang.editor.structure.CellModel_Collection" flags="sn" stub="3013115976261988961" index="3EZMnI">
        <child id="1106270802874" name="cellLayout" index="2iSdaV" />
        <child id="1073389446424" name="childCellModel" index="3EZMnx" />
      </concept>
      <concept id="1073389658414" name="jetbrains.mps.lang.editor.structure.CellModel_Property" flags="sg" stub="730538219796134133" index="3F0A7n" />
      <concept id="1225898583838" name="jetbrains.mps.lang.editor.structure.ReadOnlyModelAccessor" flags="ng" index="1HfYo3">
        <child id="1225898971709" name="getter" index="1Hhtcw" />
      </concept>
      <concept id="1225900081164" name="jetbrains.mps.lang.editor.structure.CellModel_ReadOnlyModelAccessor" flags="sg" stub="3708815482283559694" index="1HlG4h">
        <child id="1225900141900" name="modelAccessor" index="1HlULh" />
      </concept>
      <concept id="1176717841777" name="jetbrains.mps.lang.editor.structure.QueryFunction_ModelAccess_Getter" flags="in" index="3TQlhw" />
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="7453996997717780434" name="jetbrains.mps.lang.smodel.structure.Node_GetSConceptOperation" flags="nn" index="2yIwOk" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
    </language>
  </registry>
  <node concept="24kQdi" id="1mAGFBLG3zj">
    <property role="3GE5qa" value="" />
    <ref role="1XX52x" to="4v7t:5Wfdz$0qWIu" resolve="BiochemistryTest" />
    <node concept="3EZMnI" id="5Wfdz$0r8yZ" role="2wV5jI">
      <node concept="2iRkQZ" id="5Wfdz$0r8z0" role="2iSdaV" />
      <node concept="B$lHz" id="6khVixy1guV" role="3EZMnx" />
      <node concept="35HoNQ" id="4B5aqq8yhPR" role="3EZMnx" />
      <node concept="3EZMnI" id="5Wfdz$0r8$d" role="3EZMnx">
        <node concept="2iRfu4" id="5Wfdz$0r8$e" role="2iSdaV" />
        <node concept="1HlG4h" id="6khVixy1i5a" role="3EZMnx">
          <ref role="1k5W1q" to="uubs:2z0vFKsMLG5" resolve="Keyword" />
          <node concept="1HfYo3" id="6khVixy1i5c" role="1HlULh">
            <node concept="3TQlhw" id="6khVixy1i5e" role="1Hhtcw">
              <node concept="3clFbS" id="6khVixy1i5g" role="2VODD2">
                <node concept="3clFbF" id="6khVixy1kzH" role="3cqZAp">
                  <node concept="2OqwBi" id="6khVixy1mjv" role="3clFbG">
                    <node concept="2OqwBi" id="6khVixy1kSO" role="2Oq$k0">
                      <node concept="pncrf" id="6khVixy1kzG" role="2Oq$k0" />
                      <node concept="2yIwOk" id="6khVixy1lw5" role="2OqNvi" />
                    </node>
                    <node concept="2qgKlT" id="6khVixy1pni" role="2OqNvi">
                      <ref role="37wK5l" to="z6e9:6khVixy1oDR" resolve="getUnitLabel" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3F0A7n" id="5Wfdz$0r8$2" role="3EZMnx">
          <property role="1$x2rV" value="..." />
          <property role="1O74Pk" value="true" />
          <ref role="1NtTu8" to="4v7t:7lYCqhuj$24" resolve="unit" />
        </node>
      </node>
    </node>
  </node>
</model>

