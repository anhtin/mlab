<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:3abf6acc-a4c5-4ee7-8b22-79c4941d18e6(no.uio.mLab.entities.biochemistry.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="6" />
    <devkit ref="78434eb8-b0e5-444b-850d-e7c4ad2da9ab(jetbrains.mps.devkit.aspect.structure)" />
  </languages>
  <imports>
    <import index="xvtf" ref="r:506bae70-6209-41ca-8d8b-9690b3810379(no.uio.mLab.entities.laboratoryTest.structure)" />
    <import index="bop4" ref="r:cbb3825a-4c97-4746-89c7-6d2bd4f55eff(no.uio.mLab.entities.specialistType.structure)" />
    <import index="vbok" ref="r:6cc208d3-a370-49ca-a6ac-ded97805a924(no.uio.mLab.shared.structure)" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <property id="4628067390765956807" name="final" index="R5$K2" />
        <property id="4628067390765956802" name="abstract" index="R5$K7" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
        <child id="1071489727084" name="propertyDeclaration" index="1TKVEl" />
      </concept>
      <concept id="1169125989551" name="jetbrains.mps.lang.structure.structure.InterfaceConceptDeclaration" flags="ig" index="PlHQZ" />
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <property id="1096454100552" name="rootable" index="19KtqR" />
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288299" name="jetbrains.mps.lang.structure.structure.PropertyDeclaration" flags="ig" index="1TJgyi">
        <property id="241647608299431129" name="propertyId" index="IQ2nx" />
        <reference id="1082985295845" name="dataType" index="AX2Wp" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599698500" name="specializedLink" index="20ksaX" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="5Wfdz$0qWIu">
    <property role="EcuMT" value="6849753176701914014" />
    <property role="TrG5h" value="BiochemistryTest" />
    <property role="R5$K7" value="false" />
    <property role="R5$K2" value="false" />
    <property role="19KtqR" value="true" />
    <property role="3GE5qa" value="" />
    <ref role="1TJDcQ" to="xvtf:1mAGFBLav_o" resolve="LaboratoryTest" />
    <node concept="1TJgyj" id="4QUW3edDrEa" role="1TKVEi">
      <property role="IQ2ns" value="5601053190799080074" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="deprecatedBy" />
      <ref role="20ksaX" to="xvtf:499Gn2D9EqT" resolve="deprecatedBy" />
      <ref role="20lvS9" node="5Wfdz$0qWIu" resolve="BiochemistryTest" />
    </node>
    <node concept="PrWs8" id="7lYCqhujpdr" role="PzmwI">
      <ref role="PrY4T" to="vbok:7lYCqhujn5u" resolve="IHasUnit" />
    </node>
    <node concept="PrWs8" id="6khVixy1jlK" role="PzmwI">
      <ref role="PrY4T" node="499Gn2D9F$O" resolve="ITranslatableBiochemistryEntitiesConcept" />
    </node>
    <node concept="1TJgyi" id="7lYCqhuj$24" role="1TKVEl">
      <property role="IQ2nx" value="8466382076832006276" />
      <property role="TrG5h" value="unit" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="1mAGFBLdp3F">
    <property role="EcuMT" value="1560130832615969003" />
    <property role="TrG5h" value="BiochemistrySpecialistType" />
    <property role="R5$K7" value="false" />
    <property role="R5$K2" value="false" />
    <property role="19KtqR" value="true" />
    <property role="3GE5qa" value="" />
    <ref role="1TJDcQ" to="bop4:1Hxyv4DS4$7" resolve="SpecialistType" />
    <node concept="1TJgyj" id="4QUW3edDvLp" role="1TKVEi">
      <property role="IQ2ns" value="5601053190799096921" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="deprecatedBy" />
      <ref role="20lvS9" node="1mAGFBLdp3F" resolve="BiochemistrySpecialistType" />
      <ref role="20ksaX" to="bop4:4QUW3edDvrX" resolve="deprecatedBy" />
    </node>
    <node concept="PrWs8" id="6khVixy1sle" role="PzmwI">
      <ref role="PrY4T" node="499Gn2D9F$O" resolve="ITranslatableBiochemistryEntitiesConcept" />
    </node>
  </node>
  <node concept="PlHQZ" id="499Gn2D9F$O">
    <property role="EcuMT" value="4776543977235462452" />
    <property role="TrG5h" value="ITranslatableBiochemistryEntitiesConcept" />
  </node>
</model>

