package no.uio.mLab.entities.biochemistry.translations;

/*Generated by MPS */


public interface IBiochemistryEntitiesTranslations {
  String getBiochemistryTestAlias();
  String getBiochemistryTestDescription();
  String getBiochemistryTestUnitLabel();

  String getSpecialistTypeAlias();
  String getSpecialistTypeDescription();
}
