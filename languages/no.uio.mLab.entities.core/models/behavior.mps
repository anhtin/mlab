<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:1fd56a16-a059-4e83-aeb8-466c3ef2af93(no.uio.mLab.entities.core.behavior)">
  <persistence version="9" />
  <languages>
    <use id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior" version="1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="1yj" ref="r:0c887d67-81ab-420f-9a35-a9f6bf96c975(no.uio.mLab.shared.behavior)" />
    <import index="vbok" ref="r:6cc208d3-a370-49ca-a6ac-ded97805a924(no.uio.mLab.shared.structure)" />
    <import index="pitv" ref="r:16cfa3f1-e64c-49ff-a344-e90e90a00e93(no.uio.mLab.entities.core.translations)" />
    <import index="kkto" ref="r:01f3534b-874e-42dc-a9b5-7540591f45bb(no.uio.mLab.entities.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior">
      <concept id="6496299201655527393" name="jetbrains.mps.lang.behavior.structure.LocalBehaviorMethodCall" flags="nn" index="BsUDl" />
      <concept id="1225194240794" name="jetbrains.mps.lang.behavior.structure.ConceptBehavior" flags="ng" index="13h7C7">
        <reference id="1225194240799" name="concept" index="13h7C2" />
        <child id="1225194240805" name="method" index="13h7CS" />
        <child id="1225194240801" name="constructor" index="13h7CW" />
      </concept>
      <concept id="1225194413805" name="jetbrains.mps.lang.behavior.structure.ConceptConstructorDeclaration" flags="in" index="13hLZK" />
      <concept id="1225194472830" name="jetbrains.mps.lang.behavior.structure.ConceptMethodDeclaration" flags="ng" index="13i0hz">
        <property id="5864038008284099149" name="isStatic" index="2Ki8OM" />
        <property id="1225194472832" name="isVirtual" index="13i0it" />
        <property id="1225194472834" name="isAbstract" index="13i0iv" />
        <reference id="1225194472831" name="overriddenMethod" index="13i0hy" />
      </concept>
      <concept id="1225194691553" name="jetbrains.mps.lang.behavior.structure.ThisNodeExpression" flags="nn" index="13iPFW" />
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
      </concept>
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="13h7C7" id="4V3GMfXv92M">
    <property role="3GE5qa" value="" />
    <ref role="13h7C2" to="kkto:4QUW3edDqMR" resolve="Entity" />
    <node concept="13i0hz" id="6khVixy0Hme" role="13h7CS">
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getCodeLabel" />
      <node concept="3Tm1VV" id="6khVixy0Hmf" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixy0Hmg" role="3clF45" />
      <node concept="3clFbS" id="6khVixy0Hmh" role="3clF47">
        <node concept="3clFbF" id="6khVixy0Hmi" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixy0Hmj" role="3clFbG">
            <node concept="BsUDl" id="6khVixy0Hmk" role="2Oq$k0">
              <ref role="37wK5l" node="6khVixy0E7l" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixy0Hml" role="2OqNvi">
              <ref role="37wK5l" to="pitv:6khVixy0Ggz" resolve="getEntityCodeLabel" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="6khVixy0Dgg" role="13h7CS">
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getDescriptionLabel" />
      <node concept="3Tm1VV" id="6khVixy0Dgh" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixy0Djf" role="3clF45" />
      <node concept="3clFbS" id="6khVixy0Dgj" role="3clF47">
        <node concept="3clFbF" id="6khVixy0Eg8" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixy0EmW" role="3clFbG">
            <node concept="BsUDl" id="6khVixy0Eg7" role="2Oq$k0">
              <ref role="37wK5l" node="6khVixy0E7l" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixy0EsH" role="2OqNvi">
              <ref role="37wK5l" to="pitv:6khVixy0D$3" resolve="getEntityDescriptionLabel" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="6khVixy0Etn" role="13h7CS">
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getDeprecatedByLabel" />
      <node concept="3Tm1VV" id="6khVixy0Eto" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixy0Etp" role="3clF45" />
      <node concept="3clFbS" id="6khVixy0Etq" role="3clF47">
        <node concept="3clFbF" id="6khVixy0Etr" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixy0Ets" role="3clFbG">
            <node concept="BsUDl" id="6khVixy0Ett" role="2Oq$k0">
              <ref role="37wK5l" node="6khVixy0E7l" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixy0Etu" role="2OqNvi">
              <ref role="37wK5l" to="pitv:6khVixy0D$V" resolve="getEntityDeprecatedByLabel" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="6khVixyaf0e" role="13h7CS">
      <property role="TrG5h" value="getMissingNameError" />
      <property role="2Ki8OM" value="true" />
      <node concept="3Tm1VV" id="6khVixyaf0f" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixyaf0g" role="3clF45" />
      <node concept="3clFbS" id="6khVixyaf0h" role="3clF47">
        <node concept="3clFbF" id="6khVixyaf0i" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixyaf0j" role="3clFbG">
            <node concept="BsUDl" id="6khVixyaf0k" role="2Oq$k0">
              <ref role="37wK5l" node="6khVixy0E7l" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixyaf0l" role="2OqNvi">
              <ref role="37wK5l" to="pitv:6khVixyaaKc" resolve="getEntityMissingNameError" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="13hLZK" id="4V3GMfXv92N" role="13h7CW">
      <node concept="3clFbS" id="4V3GMfXv92O" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6khVixy0WHA" role="13h7CS">
      <property role="TrG5h" value="getMissingCodeError" />
      <property role="2Ki8OM" value="true" />
      <node concept="3Tm1VV" id="6khVixy0WHB" role="1B3o_S" />
      <node concept="17QB3L" id="6khVixy0WKZ" role="3clF45" />
      <node concept="3clFbS" id="6khVixy0WHD" role="3clF47">
        <node concept="3clFbF" id="6khVixy0WSq" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixy0WZe" role="3clFbG">
            <node concept="BsUDl" id="6khVixy0WSp" role="2Oq$k0">
              <ref role="37wK5l" node="6khVixy0E7l" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixy0X5n" role="2OqNvi">
              <ref role="37wK5l" to="pitv:6khVixy0VIH" resolve="getEntityMissingCodeError" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="4V3GMfXv97O" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getReferenceDescription" />
      <property role="2Ki8OM" value="false" />
      <ref role="13i0hy" to="1yj:4V3GMfXuP$X" resolve="getReferenceDescription" />
      <node concept="3Tm1VV" id="4V3GMfXv97P" role="1B3o_S" />
      <node concept="3clFbS" id="4V3GMfXv97Y" role="3clF47">
        <node concept="3clFbF" id="4V3GMfXv9cj" role="3cqZAp">
          <node concept="2OqwBi" id="4V3GMfXv9mB" role="3clFbG">
            <node concept="13iPFW" id="4V3GMfXv9ci" role="2Oq$k0" />
            <node concept="3TrcHB" id="4V3GMfXv9wB" role="2OqNvi">
              <ref role="3TsBF5" to="kkto:4QUW3edDU1q" resolve="description" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="4V3GMfXv97Z" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="6khVixy0E7a">
    <ref role="13h7C2" to="kkto:6khVixy0E6I" resolve="ICoreEntityTranslatableConcept" />
    <node concept="13i0hz" id="6khVixy0E7l" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getDisplayTranslations" />
      <node concept="3Tm1VV" id="6khVixy0E7m" role="1B3o_S" />
      <node concept="3uibUv" id="6khVixy0E8d" role="3clF45">
        <ref role="3uigEE" to="pitv:4zMac8rUNtP" resolve="ICoreEntitiesTranslations" />
      </node>
      <node concept="3clFbS" id="6khVixy0E7o" role="3clF47">
        <node concept="3clFbF" id="6khVixy0E8X" role="3cqZAp">
          <node concept="10M0yZ" id="6khVixy0E9n" role="3clFbG">
            <ref role="3cqZAo" to="pitv:4zMac8rVBeK" resolve="displayTranslations" />
            <ref role="1PxDUh" to="pitv:4zMac8rUNsN" resolve="CoreEntitiesTranslationProvider" />
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="6khVixy0E9W" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getGenerationTranslations" />
      <node concept="3Tm1VV" id="6khVixy0E9X" role="1B3o_S" />
      <node concept="3uibUv" id="6khVixy0E9Y" role="3clF45">
        <ref role="3uigEE" to="pitv:4zMac8rUNtP" resolve="ICoreEntitiesTranslations" />
      </node>
      <node concept="3clFbS" id="6khVixy0E9Z" role="3clF47">
        <node concept="3clFbF" id="6khVixy0Ea0" role="3cqZAp">
          <node concept="10M0yZ" id="6khVixy0Eb2" role="3clFbG">
            <ref role="3cqZAo" to="pitv:1Hxyv4DUmfq" resolve="generationTranslations" />
            <ref role="1PxDUh" to="pitv:4zMac8rUNsN" resolve="CoreEntitiesTranslationProvider" />
          </node>
        </node>
      </node>
    </node>
    <node concept="13hLZK" id="6khVixy0E7b" role="13h7CW">
      <node concept="3clFbS" id="6khVixy0E7c" role="2VODD2" />
    </node>
  </node>
</model>

