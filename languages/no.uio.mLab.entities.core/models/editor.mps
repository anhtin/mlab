<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:6badafb7-7beb-45af-8649-984adc980044(no.uio.mLab.entities.core.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="11" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="kkto" ref="r:01f3534b-874e-42dc-a9b5-7540591f45bb(no.uio.mLab.entities.core.structure)" implicit="true" />
    <import index="uubs" ref="r:481ae0fe-9100-4bb8-9488-731d6cfeba53(no.uio.mLab.shared.editor)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
    <import index="860a" ref="r:1fd56a16-a059-4e83-aeb8-466c3ef2af93(no.uio.mLab.entities.core.behavior)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi" />
      <concept id="1106270549637" name="jetbrains.mps.lang.editor.structure.CellLayout_Horizontal" flags="nn" index="2iRfu4" />
      <concept id="1106270571710" name="jetbrains.mps.lang.editor.structure.CellLayout_Vertical" flags="nn" index="2iRkQZ" />
      <concept id="1142886811589" name="jetbrains.mps.lang.editor.structure.ConceptFunctionParameter_node" flags="nn" index="pncrf" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="1078939183254" name="jetbrains.mps.lang.editor.structure.CellModel_Component" flags="sg" stub="3162947552742194261" index="PMmxH">
        <reference id="1078939183255" name="editorComponent" index="PMmxG" />
      </concept>
      <concept id="1088013125922" name="jetbrains.mps.lang.editor.structure.CellModel_RefCell" flags="sg" stub="730538219795941030" index="1iCGBv">
        <child id="1088186146602" name="editorComponent" index="1sWHZn" />
      </concept>
      <concept id="1381004262292414836" name="jetbrains.mps.lang.editor.structure.ICellStyle" flags="ng" index="1k5N5V">
        <reference id="1381004262292426837" name="parentStyleClass" index="1k5W1q" />
      </concept>
      <concept id="1088185857835" name="jetbrains.mps.lang.editor.structure.InlineEditorComponent" flags="ig" index="1sVBvm" />
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <property id="1139852716018" name="noTargetText" index="1$x2rV" />
        <property id="1140017977771" name="readOnly" index="1Intyy" />
        <property id="1140114345053" name="allowEmptyText" index="1O74Pk" />
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389446423" name="jetbrains.mps.lang.editor.structure.CellModel_Collection" flags="sn" stub="3013115976261988961" index="3EZMnI">
        <child id="1106270802874" name="cellLayout" index="2iSdaV" />
        <child id="1073389446424" name="childCellModel" index="3EZMnx" />
      </concept>
      <concept id="1073389658414" name="jetbrains.mps.lang.editor.structure.CellModel_Property" flags="sg" stub="730538219796134133" index="3F0A7n" />
      <concept id="1225898583838" name="jetbrains.mps.lang.editor.structure.ReadOnlyModelAccessor" flags="ng" index="1HfYo3">
        <child id="1225898971709" name="getter" index="1Hhtcw" />
      </concept>
      <concept id="1225900081164" name="jetbrains.mps.lang.editor.structure.CellModel_ReadOnlyModelAccessor" flags="sg" stub="3708815482283559694" index="1HlG4h">
        <child id="1225900141900" name="modelAccessor" index="1HlULh" />
      </concept>
      <concept id="1176717841777" name="jetbrains.mps.lang.editor.structure.QueryFunction_ModelAccess_Getter" flags="in" index="3TQlhw" />
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="7453996997717780434" name="jetbrains.mps.lang.smodel.structure.Node_GetSConceptOperation" flags="nn" index="2yIwOk" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
    </language>
  </registry>
  <node concept="24kQdi" id="6khVixy0AYK">
    <property role="3GE5qa" value="" />
    <ref role="1XX52x" to="kkto:4QUW3edDqMR" resolve="Entity" />
    <node concept="3EZMnI" id="6khVixy0AYS" role="2wV5jI">
      <node concept="2iRkQZ" id="6khVixy0AYT" role="2iSdaV" />
      <node concept="3EZMnI" id="6khVixy0AYM" role="3EZMnx">
        <node concept="2iRfu4" id="6khVixy0AYN" role="2iSdaV" />
        <node concept="PMmxH" id="6khVixy0Bfs" role="3EZMnx">
          <ref role="PMmxG" to="uubs:5ZQBr_XMEtE" resolve="TranslatedAlias" />
          <ref role="1k5W1q" to="uubs:2z0vFKsMLG5" resolve="Keyword" />
        </node>
        <node concept="3F0A7n" id="6khVixy0Bfx" role="3EZMnx">
          <property role="1$x2rV" value="..." />
          <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
        </node>
      </node>
      <node concept="3EZMnI" id="6khVixy0FQ$" role="3EZMnx">
        <node concept="2iRfu4" id="6khVixy0FQ_" role="2iSdaV" />
        <node concept="1HlG4h" id="6khVixy0FQA" role="3EZMnx">
          <ref role="1k5W1q" to="uubs:2z0vFKsMLG5" resolve="Keyword" />
          <node concept="1HfYo3" id="6khVixy0FQB" role="1HlULh">
            <node concept="3TQlhw" id="6khVixy0FQC" role="1Hhtcw">
              <node concept="3clFbS" id="6khVixy0FQD" role="2VODD2">
                <node concept="3clFbF" id="6khVixy0FQE" role="3cqZAp">
                  <node concept="2OqwBi" id="6khVixy0FQF" role="3clFbG">
                    <node concept="2OqwBi" id="6khVixy0FQG" role="2Oq$k0">
                      <node concept="pncrf" id="6khVixy0FQH" role="2Oq$k0" />
                      <node concept="2yIwOk" id="6khVixy0FQI" role="2OqNvi" />
                    </node>
                    <node concept="2qgKlT" id="6khVixy0HZV" role="2OqNvi">
                      <ref role="37wK5l" to="860a:6khVixy0Hme" resolve="getCodeLabel" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3F0A7n" id="6khVixy0FQK" role="3EZMnx">
          <property role="1$x2rV" value="..." />
          <ref role="1NtTu8" to="kkto:6khVixy0AQY" resolve="code" />
        </node>
      </node>
      <node concept="3EZMnI" id="6khVixy0BfF" role="3EZMnx">
        <node concept="2iRfu4" id="6khVixy0BfG" role="2iSdaV" />
        <node concept="1HlG4h" id="6khVixy0BfP" role="3EZMnx">
          <ref role="1k5W1q" to="uubs:2z0vFKsMLG5" resolve="Keyword" />
          <node concept="1HfYo3" id="6khVixy0BfR" role="1HlULh">
            <node concept="3TQlhw" id="6khVixy0BfT" role="1Hhtcw">
              <node concept="3clFbS" id="6khVixy0BfV" role="2VODD2">
                <node concept="3clFbF" id="6khVixy0BoC" role="3cqZAp">
                  <node concept="2OqwBi" id="6khVixy0CGQ" role="3clFbG">
                    <node concept="2OqwBi" id="6khVixy0BCP" role="2Oq$k0">
                      <node concept="pncrf" id="6khVixy0BoB" role="2Oq$k0" />
                      <node concept="2yIwOk" id="6khVixy0C5g" role="2OqNvi" />
                    </node>
                    <node concept="2qgKlT" id="6khVixy0IHZ" role="2OqNvi">
                      <ref role="37wK5l" to="860a:6khVixy0Dgg" resolve="getDescriptionLabel" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3F0A7n" id="6khVixy0EP5" role="3EZMnx">
          <property role="1$x2rV" value="..." />
          <property role="1O74Pk" value="true" />
          <ref role="1NtTu8" to="kkto:4QUW3edDU1q" resolve="description" />
        </node>
      </node>
      <node concept="3EZMnI" id="6khVixy0Fy_" role="3EZMnx">
        <node concept="2iRfu4" id="6khVixy0FyA" role="2iSdaV" />
        <node concept="1HlG4h" id="6khVixy0FyB" role="3EZMnx">
          <ref role="1k5W1q" to="uubs:2z0vFKsMLG5" resolve="Keyword" />
          <node concept="1HfYo3" id="6khVixy0FyC" role="1HlULh">
            <node concept="3TQlhw" id="6khVixy0FyD" role="1Hhtcw">
              <node concept="3clFbS" id="6khVixy0FyE" role="2VODD2">
                <node concept="3clFbF" id="6khVixy0FyF" role="3cqZAp">
                  <node concept="2OqwBi" id="6khVixy0FyG" role="3clFbG">
                    <node concept="2OqwBi" id="6khVixy0FyH" role="2Oq$k0">
                      <node concept="pncrf" id="6khVixy0FyI" role="2Oq$k0" />
                      <node concept="2yIwOk" id="6khVixy0FyJ" role="2OqNvi" />
                    </node>
                    <node concept="2qgKlT" id="6khVixy0Js3" role="2OqNvi">
                      <ref role="37wK5l" to="860a:6khVixy0Etn" resolve="getDeprecatedByLabel" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1iCGBv" id="6khVixy0Ks8" role="3EZMnx">
          <property role="1$x2rV" value="&lt;&lt; ... &gt;&gt;" />
          <ref role="1NtTu8" to="kkto:4QUW3edDrpg" resolve="deprecatedBy" />
          <node concept="1sVBvm" id="6khVixy0Ksa" role="1sWHZn">
            <node concept="3F0A7n" id="6khVixy0KD9" role="2wV5jI">
              <property role="1Intyy" value="true" />
              <property role="1$x2rV" value="&lt;&lt; ... &gt;&gt;" />
              <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

