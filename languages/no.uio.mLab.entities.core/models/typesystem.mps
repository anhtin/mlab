<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:3acc04cf-4702-4bac-83fc-15401f123152(no.uio.mLab.entities.core.typesystem)">
  <persistence version="9" />
  <languages>
    <use id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem" version="1" />
    <devkit ref="00000000-0000-4000-0000-1de82b3a4936(jetbrains.mps.devkit.aspect.typesystem)" />
  </languages>
  <imports>
    <import index="860a" ref="r:1fd56a16-a059-4e83-aeb8-466c3ef2af93(no.uio.mLab.entities.core.behavior)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
    <import index="kkto" ref="r:01f3534b-874e-42dc-a9b5-7540591f45bb(no.uio.mLab.entities.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1225271369338" name="jetbrains.mps.baseLanguage.structure.IsEmptyOperation" flags="nn" index="17RlXB" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
      </concept>
    </language>
    <language id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem">
      <concept id="1175517767210" name="jetbrains.mps.lang.typesystem.structure.ReportErrorStatement" flags="nn" index="2MkqsV">
        <child id="1175517851849" name="errorString" index="2MkJ7o" />
      </concept>
      <concept id="1227096498176" name="jetbrains.mps.lang.typesystem.structure.PropertyMessageTarget" flags="ng" index="2ODE4t">
        <reference id="1227096521710" name="propertyDeclaration" index="2ODJFN" />
      </concept>
      <concept id="1227096774658" name="jetbrains.mps.lang.typesystem.structure.MessageStatement" flags="ng" index="2OEH$v">
        <child id="1227096802790" name="nodeToReport" index="2OEOjV" />
        <child id="1227096836496" name="messageTarget" index="2OEWyd" />
      </concept>
      <concept id="1195213580585" name="jetbrains.mps.lang.typesystem.structure.AbstractCheckingRule" flags="ig" index="18hYwZ">
        <child id="1195213635060" name="body" index="18ibNy" />
      </concept>
      <concept id="1195214364922" name="jetbrains.mps.lang.typesystem.structure.NonTypesystemRule" flags="ig" index="18kY7G" />
      <concept id="1174642788531" name="jetbrains.mps.lang.typesystem.structure.ConceptReference" flags="ig" index="1YaCAy">
        <reference id="1174642800329" name="concept" index="1YaFvo" />
      </concept>
      <concept id="1174648085619" name="jetbrains.mps.lang.typesystem.structure.AbstractRule" flags="ng" index="1YuPPy">
        <child id="1174648101952" name="applicableNode" index="1YuTPh" />
      </concept>
      <concept id="1174650418652" name="jetbrains.mps.lang.typesystem.structure.ApplicableNodeReference" flags="nn" index="1YBJjd">
        <reference id="1174650432090" name="applicableNode" index="1YBMHb" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="7453996997717780434" name="jetbrains.mps.lang.smodel.structure.Node_GetSConceptOperation" flags="nn" index="2yIwOk" />
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="18kY7G" id="6khVixy0TVF">
    <property role="TrG5h" value="check_Entity" />
    <property role="3GE5qa" value="" />
    <node concept="3clFbS" id="6khVixy0TVG" role="18ibNy">
      <node concept="3clFbJ" id="6khVixyacOv" role="3cqZAp">
        <node concept="3clFbS" id="6khVixyacOx" role="3clFbx">
          <node concept="2MkqsV" id="6khVixyaels" role="3cqZAp">
            <node concept="2OqwBi" id="6khVixyafGd" role="2MkJ7o">
              <node concept="2OqwBi" id="6khVixyaeyu" role="2Oq$k0">
                <node concept="1YBJjd" id="6khVixyaelF" role="2Oq$k0">
                  <ref role="1YBMHb" node="6khVixy0TVI" resolve="entity" />
                </node>
                <node concept="2yIwOk" id="6khVixyaeSp" role="2OqNvi" />
              </node>
              <node concept="2qgKlT" id="6khVixyag56" role="2OqNvi">
                <ref role="37wK5l" to="860a:6khVixyaf0e" resolve="getMissingNameError" />
              </node>
            </node>
            <node concept="1YBJjd" id="6khVixyag90" role="2OEOjV">
              <ref role="1YBMHb" node="6khVixy0TVI" resolve="entity" />
            </node>
            <node concept="2ODE4t" id="6khVixyageY" role="2OEWyd">
              <ref role="2ODJFN" to="tpck:h0TrG11" resolve="name" />
            </node>
          </node>
        </node>
        <node concept="2OqwBi" id="6khVixyadZd" role="3clFbw">
          <node concept="2OqwBi" id="6khVixyad5K" role="2Oq$k0">
            <node concept="1YBJjd" id="6khVixyacT0" role="2Oq$k0">
              <ref role="1YBMHb" node="6khVixy0TVI" resolve="entity" />
            </node>
            <node concept="3TrcHB" id="6khVixyadrq" role="2OqNvi">
              <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
            </node>
          </node>
          <node concept="17RlXB" id="6khVixyaele" role="2OqNvi" />
        </node>
      </node>
      <node concept="3clFbJ" id="6khVixy0TVM" role="3cqZAp">
        <node concept="2OqwBi" id="6khVixy0UVh" role="3clFbw">
          <node concept="2OqwBi" id="6khVixy0U8I" role="2Oq$k0">
            <node concept="1YBJjd" id="6khVixy0TVY" role="2Oq$k0">
              <ref role="1YBMHb" node="6khVixy0TVI" resolve="entity" />
            </node>
            <node concept="3TrcHB" id="6khVixy0Unu" role="2OqNvi">
              <ref role="3TsBF5" to="kkto:6khVixy0AQY" resolve="code" />
            </node>
          </node>
          <node concept="17RlXB" id="6khVixy0VcK" role="2OqNvi" />
        </node>
        <node concept="3clFbS" id="6khVixy0TVO" role="3clFbx">
          <node concept="2MkqsV" id="6khVixy0X8S" role="3cqZAp">
            <node concept="2OqwBi" id="6khVixy0Y8C" role="2MkJ7o">
              <node concept="2OqwBi" id="6khVixy0XlR" role="2Oq$k0">
                <node concept="1YBJjd" id="6khVixy0X94" role="2Oq$k0">
                  <ref role="1YBMHb" node="6khVixy0TVI" resolve="entity" />
                </node>
                <node concept="2yIwOk" id="6khVixy0XBg" role="2OqNvi" />
              </node>
              <node concept="2qgKlT" id="6khVixy0YsZ" role="2OqNvi">
                <ref role="37wK5l" to="860a:6khVixy0WHA" resolve="getMissingCodeError" />
              </node>
            </node>
            <node concept="1YBJjd" id="6khVixy0YwT" role="2OEOjV">
              <ref role="1YBMHb" node="6khVixy0TVI" resolve="entity" />
            </node>
            <node concept="2ODE4t" id="6khVixy0Zfv" role="2OEWyd">
              <ref role="2ODJFN" to="kkto:6khVixy0AQY" resolve="code" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="6khVixy0TVI" role="1YuTPh">
      <property role="TrG5h" value="entity" />
      <ref role="1YaFvo" to="kkto:4QUW3edDqMR" resolve="Entity" />
    </node>
  </node>
</model>

