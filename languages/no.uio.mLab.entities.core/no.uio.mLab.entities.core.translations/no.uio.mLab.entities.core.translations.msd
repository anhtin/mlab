<?xml version="1.0" encoding="UTF-8"?>
<solution name="no.uio.mLab.entities.core.translations" uuid="e375b23a-63f7-4ac0-8e48-28b0ce172a49" moduleVersion="0" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
  </models>
  <sourcePath />
  <dependencies>
    <dependency reexport="false">3d1e5220-5be7-4cad-97be-258d1d3e62dd(no.uio.mLab.shared.translations)</dependency>
  </dependencies>
  <languageVersions>
    <language slang="l:f3061a53-9226-4cc5-a443-f952ceaf5816:jetbrains.mps.baseLanguage" version="6" />
    <language slang="l:ceab5195-25ea-4f22-9b92-103b95ca8c0c:jetbrains.mps.lang.core" version="1" />
    <language slang="l:9ded098b-ad6a-4657-bfd9-48636cfe8bc3:jetbrains.mps.lang.traceable" version="0" />
  </languageVersions>
  <dependencyVersions>
    <module reference="e375b23a-63f7-4ac0-8e48-28b0ce172a49(no.uio.mLab.entities.core.translations)" version="0" />
    <module reference="3d1e5220-5be7-4cad-97be-258d1d3e62dd(no.uio.mLab.shared.translations)" version="0" />
  </dependencyVersions>
</solution>

