package no.uio.mLab.entities.core.structure;

/*Generated by MPS */

import jetbrains.mps.smodel.runtime.ConceptPresentationAspectBase;
import jetbrains.mps.smodel.runtime.ConceptPresentation;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.mps.openapi.language.SAbstractConcept;
import jetbrains.mps.smodel.runtime.ConceptPresentationBuilder;

public class ConceptPresentationAspectImpl extends ConceptPresentationAspectBase {
  private ConceptPresentation props_Entity;
  private ConceptPresentation props_ICoreEntityTranslatableConcept;

  @Override
  @Nullable
  public ConceptPresentation getDescriptor(SAbstractConcept c) {
    StructureAspectDescriptor structureDescriptor = (StructureAspectDescriptor) myLanguageRuntime.getAspect(jetbrains.mps.smodel.runtime.StructureAspectDescriptor.class);
    switch (structureDescriptor.internalIndex(c)) {
      case LanguageConceptSwitch.Entity:
        if (props_Entity == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          props_Entity = cpb.create();
        }
        return props_Entity;
      case LanguageConceptSwitch.ICoreEntityTranslatableConcept:
        if (props_ICoreEntityTranslatableConcept == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          props_ICoreEntityTranslatableConcept = cpb.create();
        }
        return props_ICoreEntityTranslatableConcept;
    }
    return null;
  }
}
