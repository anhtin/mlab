package no.uio.mLab.entities.core.structure;

/*Generated by MPS */

import jetbrains.mps.smodel.runtime.BaseStructureAspectDescriptor;
import jetbrains.mps.smodel.runtime.ConceptDescriptor;
import java.util.Collection;
import java.util.Arrays;
import org.jetbrains.annotations.Nullable;
import jetbrains.mps.smodel.adapter.ids.SConceptId;
import org.jetbrains.mps.openapi.language.SAbstractConcept;
import jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2;

public class StructureAspectDescriptor extends BaseStructureAspectDescriptor {
  /*package*/ final ConceptDescriptor myConceptEntity = createDescriptorForEntity();
  /*package*/ final ConceptDescriptor myConceptICoreEntityTranslatableConcept = createDescriptorForICoreEntityTranslatableConcept();
  private final LanguageConceptSwitch myIndexSwitch;

  public StructureAspectDescriptor() {
    myIndexSwitch = new LanguageConceptSwitch();
  }

  @Override
  public Collection<ConceptDescriptor> getDescriptors() {
    return Arrays.asList(myConceptEntity, myConceptICoreEntityTranslatableConcept);
  }

  @Override
  @Nullable
  public ConceptDescriptor getDescriptor(SConceptId id) {
    switch (myIndexSwitch.index(id)) {
      case LanguageConceptSwitch.Entity:
        return myConceptEntity;
      case LanguageConceptSwitch.ICoreEntityTranslatableConcept:
        return myConceptICoreEntityTranslatableConcept;
      default:
        return null;
    }
  }

  /*package*/ int internalIndex(SAbstractConcept c) {
    return myIndexSwitch.index(c);
  }

  private static ConceptDescriptor createDescriptorForEntity() {
    ConceptDescriptorBuilder2 b = new ConceptDescriptorBuilder2("no.uio.mLab.entities.core", "Entity", 0x33c0f773be2948e3L, 0x958740dc4b3c54f0L, 0x4dbaf0338da5acb7L);
    b.class_(false, true, false);
    b.parent(0x6adcef385cdc48dcL, 0xb225be76276615fdL, 0x4dbaf0338fb1e303L);
    b.parent(0x33c0f773be2948e3L, 0x958740dc4b3c54f0L, 0x6511ed286202a1aeL);
    b.origin("r:01f3534b-874e-42dc-a9b5-7540591f45bb(no.uio.mLab.entities.core.structure)/5601053190799076535");
    b.version(2);
    b.prop("code", 0x6511ed2862026dbeL, "7282862830133603774");
    b.prop("description", 0x4dbaf0338da7a05aL, "5601053190799204442");
    b.associate("deprecatedBy", 0x4dbaf0338da5b650L).target(0x33c0f773be2948e3L, 0x958740dc4b3c54f0L, 0x4dbaf0338da5acb7L).optional(true).origin("5601053190799078992").done();
    return b.create();
  }
  private static ConceptDescriptor createDescriptorForICoreEntityTranslatableConcept() {
    ConceptDescriptorBuilder2 b = new ConceptDescriptorBuilder2("no.uio.mLab.entities.core", "ICoreEntityTranslatableConcept", 0x33c0f773be2948e3L, 0x958740dc4b3c54f0L, 0x6511ed286202a1aeL);
    b.interface_();
    b.parent(0x6adcef385cdc48dcL, 0xb225be76276615fdL, 0x5f0f3639007cc07fL);
    b.origin("r:01f3534b-874e-42dc-a9b5-7540591f45bb(no.uio.mLab.entities.core.structure)/7282862830133617070");
    b.version(2);
    return b.create();
  }
}
