<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:4492d096-097c-4532-b592-a000ef58e529(no.uio.mLab.entities.laboratoryTest.behavior)">
  <persistence version="9" />
  <languages>
    <use id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior" version="1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="1yj" ref="r:0c887d67-81ab-420f-9a35-a9f6bf96c975(no.uio.mLab.shared.behavior)" />
    <import index="vbok" ref="r:6cc208d3-a370-49ca-a6ac-ded97805a924(no.uio.mLab.shared.structure)" />
    <import index="5xsk" ref="r:ff06c7ae-d9d2-4a9d-a6a5-d29c7fa1a7fb(no.uio.mLab.entities.laboratoryTest.runtime)" />
    <import index="8rk2" ref="r:d0aa2e84-544f-4299-9ec7-be2d67d5b5b2(no.uio.mLab.entities.laboratoryTest.translations)" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" />
    <import index="xvtf" ref="r:506bae70-6209-41ca-8d8b-9690b3810379(no.uio.mLab.entities.laboratoryTest.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior">
      <concept id="6496299201655527393" name="jetbrains.mps.lang.behavior.structure.LocalBehaviorMethodCall" flags="nn" index="BsUDl" />
      <concept id="1225194240794" name="jetbrains.mps.lang.behavior.structure.ConceptBehavior" flags="ng" index="13h7C7">
        <reference id="1225194240799" name="concept" index="13h7C2" />
        <child id="1225194240805" name="method" index="13h7CS" />
        <child id="1225194240801" name="constructor" index="13h7CW" />
      </concept>
      <concept id="1225194413805" name="jetbrains.mps.lang.behavior.structure.ConceptConstructorDeclaration" flags="in" index="13hLZK" />
      <concept id="1225194472830" name="jetbrains.mps.lang.behavior.structure.ConceptMethodDeclaration" flags="ng" index="13i0hz">
        <property id="5864038008284099149" name="isStatic" index="2Ki8OM" />
        <property id="1225194472832" name="isVirtual" index="13i0it" />
        <property id="1225194472834" name="isAbstract" index="13i0iv" />
        <reference id="1225194472831" name="overriddenMethod" index="13i0hy" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
      </concept>
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="13h7C7" id="ofcHXgb1db">
    <ref role="13h7C2" to="xvtf:ofcHXgb16J" resolve="ITranslatableLaboratoryTestConcept" />
    <node concept="13i0hz" id="ofcHXgb1du" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayTranslations" />
      <property role="2Ki8OM" value="true" />
      <node concept="3Tm1VV" id="ofcHXgb1dv" role="1B3o_S" />
      <node concept="3uibUv" id="ofcHXgb1jD" role="3clF45">
        <ref role="3uigEE" to="8rk2:4zMac8rUNtP" resolve="ILaboratoryTestTranslations" />
      </node>
      <node concept="3clFbS" id="ofcHXgb1dx" role="3clF47">
        <node concept="3clFbF" id="ofcHXgb1fk" role="3cqZAp">
          <node concept="10M0yZ" id="ofcHXgb1fK" role="3clFbG">
            <ref role="3cqZAo" to="8rk2:4zMac8rVBeK" resolve="displayTranslations" />
            <ref role="1PxDUh" to="8rk2:4zMac8rUNsN" resolve="LaboratoryTestTranslationProvider" />
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="ofcHXgb1hD" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getGenerationTranslations" />
      <property role="2Ki8OM" value="true" />
      <node concept="3Tm1VV" id="ofcHXgb1hE" role="1B3o_S" />
      <node concept="3uibUv" id="ofcHXgb1lA" role="3clF45">
        <ref role="3uigEE" to="8rk2:4zMac8rUNtP" resolve="ILaboratoryTestTranslations" />
      </node>
      <node concept="3clFbS" id="ofcHXgb1hG" role="3clF47">
        <node concept="3clFbF" id="ofcHXgb1hH" role="3cqZAp">
          <node concept="10M0yZ" id="ofcHXgb1nP" role="3clFbG">
            <ref role="3cqZAo" to="8rk2:1Hxyv4DUmfq" resolve="generationTranslations" />
            <ref role="1PxDUh" to="8rk2:4zMac8rUNsN" resolve="LaboratoryTestTranslationProvider" />
          </node>
        </node>
      </node>
    </node>
    <node concept="13hLZK" id="ofcHXgb1dc" role="13h7CW">
      <node concept="3clFbS" id="ofcHXgb1dd" role="2VODD2" />
    </node>
  </node>
  <node concept="13h7C7" id="4QUW3efUlDh">
    <property role="3GE5qa" value="" />
    <ref role="13h7C2" to="xvtf:1mAGFBLav_o" resolve="LaboratoryTest" />
    <node concept="13hLZK" id="4QUW3efUlDi" role="13h7CW">
      <node concept="3clFbS" id="4QUW3efUlDj" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="4QUW3efUlDs" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="4QUW3efUlDt" role="1B3o_S" />
      <node concept="3clFbS" id="4QUW3efUlDy" role="3clF47">
        <node concept="3clFbF" id="4QUW3efUlDB" role="3cqZAp">
          <node concept="2OqwBi" id="4QUW3efUmeI" role="3clFbG">
            <node concept="BsUDl" id="4QUW3efUm7D" role="2Oq$k0">
              <ref role="37wK5l" node="ofcHXgb1du" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="4QUW3efUmk_" role="2OqNvi">
              <ref role="37wK5l" to="8rk2:4QUW3efUlLR" resolve="getLaboratoryTestAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="4QUW3efUlDz" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixya6cd" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="6khVixya6ce" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixya6cj" role="3clF47">
        <node concept="3clFbF" id="6khVixya6co" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixya6mB" role="3clFbG">
            <node concept="BsUDl" id="6khVixya6fO" role="2Oq$k0">
              <ref role="37wK5l" node="ofcHXgb1du" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixya6sw" role="2OqNvi">
              <ref role="37wK5l" to="8rk2:6khVixya4YN" resolve="getLaboratoryTestDescription" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixya6ck" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="6khVixya6bl">
    <ref role="13h7C2" to="xvtf:6khVixya4DS" resolve="SpecimenType" />
    <node concept="13hLZK" id="6khVixya6bm" role="13h7CW">
      <node concept="3clFbS" id="6khVixya6bn" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6khVixya6ti" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="6khVixya6tj" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixya6to" role="3clF47">
        <node concept="3clFbF" id="6khVixya6tt" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixya6Gx" role="3clFbG">
            <node concept="BsUDl" id="6khVixya6_I" role="2Oq$k0">
              <ref role="37wK5l" node="ofcHXgb1du" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixya6Mi" role="2OqNvi">
              <ref role="37wK5l" to="8rk2:6khVixya50z" resolve="getSpecimenTypeAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixya6tp" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixya6tu" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="6khVixya6tv" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixya6t$" role="3clF47">
        <node concept="3clFbF" id="6khVixya6tD" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixya6N4" role="3clFbG">
            <node concept="BsUDl" id="6khVixya6N5" role="2Oq$k0">
              <ref role="37wK5l" node="ofcHXgb1du" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixya6N6" role="2OqNvi">
              <ref role="37wK5l" to="8rk2:6khVixya526" resolve="getSpecimenTypeDescription" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixya6t_" role="3clF45" />
    </node>
  </node>
</model>

