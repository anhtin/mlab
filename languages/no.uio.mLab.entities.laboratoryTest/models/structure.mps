<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:506bae70-6209-41ca-8d8b-9690b3810379(no.uio.mLab.entities.laboratoryTest.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="6" />
    <devkit ref="78434eb8-b0e5-444b-850d-e7c4ad2da9ab(jetbrains.mps.devkit.aspect.structure)" />
  </languages>
  <imports>
    <import index="vbok" ref="r:6cc208d3-a370-49ca-a6ac-ded97805a924(no.uio.mLab.shared.structure)" />
    <import index="kkto" ref="r:01f3534b-874e-42dc-a9b5-7540591f45bb(no.uio.mLab.entities.core.structure)" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <property id="4628067390765956807" name="final" index="R5$K2" />
        <property id="4628067390765956802" name="abstract" index="R5$K7" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
      </concept>
      <concept id="1169125989551" name="jetbrains.mps.lang.structure.structure.InterfaceConceptDeclaration" flags="ig" index="PlHQZ">
        <child id="1169127546356" name="extends" index="PrDN$" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <property id="1096454100552" name="rootable" index="19KtqR" />
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599698500" name="specializedLink" index="20ksaX" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="1mAGFBLav_o">
    <property role="EcuMT" value="1560130832615209304" />
    <property role="TrG5h" value="LaboratoryTest" />
    <property role="R5$K7" value="true" />
    <property role="R5$K2" value="false" />
    <property role="3GE5qa" value="" />
    <ref role="1TJDcQ" to="kkto:4QUW3edDqMR" resolve="Entity" />
    <node concept="1TJgyj" id="499Gn2D9EqT" role="1TKVEi">
      <property role="IQ2ns" value="4776543977235457721" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="deprecatedBy" />
      <ref role="20lvS9" node="1mAGFBLav_o" resolve="LaboratoryTest" />
      <ref role="20ksaX" to="kkto:4QUW3edDrpg" resolve="deprecatedBy" />
    </node>
    <node concept="PrWs8" id="4QUW3efUm4q" role="PzmwI">
      <ref role="PrY4T" node="ofcHXgb16J" resolve="ITranslatableLaboratoryTestConcept" />
    </node>
  </node>
  <node concept="PlHQZ" id="ofcHXgb16J">
    <property role="EcuMT" value="436623623582388655" />
    <property role="TrG5h" value="ITranslatableLaboratoryTestConcept" />
    <node concept="PrWs8" id="ofcHXgb16K" role="PrDN$">
      <ref role="PrY4T" to="vbok:5Wfdz$0vc1Z" resolve="ITranslatableConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="6khVixya4DS">
    <property role="EcuMT" value="7282862830136085112" />
    <property role="TrG5h" value="SpecimenType" />
    <property role="19KtqR" value="true" />
    <ref role="1TJDcQ" to="kkto:4QUW3edDqMR" resolve="Entity" />
    <node concept="PrWs8" id="6khVixya6bw" role="PzmwI">
      <ref role="PrY4T" node="ofcHXgb16J" resolve="ITranslatableLaboratoryTestConcept" />
    </node>
  </node>
</model>

