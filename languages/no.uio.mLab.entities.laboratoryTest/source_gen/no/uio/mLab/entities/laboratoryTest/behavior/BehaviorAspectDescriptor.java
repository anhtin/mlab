package no.uio.mLab.entities.laboratoryTest.behavior;

/*Generated by MPS */

import jetbrains.mps.core.aspects.behaviour.BaseBehaviorAspectDescriptor;
import jetbrains.mps.core.aspects.behaviour.api.BHDescriptor;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.mps.openapi.language.SAbstractConcept;
import jetbrains.mps.lang.smodel.ConceptSwitchIndex;
import jetbrains.mps.lang.smodel.ConceptSwitchIndexBuilder;
import jetbrains.mps.smodel.adapter.ids.MetaIdFactory;

public final class BehaviorAspectDescriptor extends BaseBehaviorAspectDescriptor {
  private final BHDescriptor myITranslatableLaboratoryTestConcept__BehaviorDescriptor = new ITranslatableLaboratoryTestConcept__BehaviorDescriptor();
  private final BHDescriptor myLaboratoryTest__BehaviorDescriptor = new LaboratoryTest__BehaviorDescriptor();
  private final BHDescriptor mySpecimenType__BehaviorDescriptor = new SpecimenType__BehaviorDescriptor();

  public BehaviorAspectDescriptor() {
  }

  @Nullable
  public BHDescriptor getDescriptor(@NotNull SAbstractConcept concept) {
    SAbstractConcept cncpt = concept;
    switch (conceptIndex.index(cncpt)) {
      case 0:
        return myITranslatableLaboratoryTestConcept__BehaviorDescriptor;
      case 1:
        return myLaboratoryTest__BehaviorDescriptor;
      case 2:
        return mySpecimenType__BehaviorDescriptor;
      default:
    }
    return null;
  }
  private static final ConceptSwitchIndex conceptIndex = new ConceptSwitchIndexBuilder().put(MetaIdFactory.conceptId(0xcae652fa7f6e4122L, 0x8bd50b26c8b4eec8L, 0x60f32df502c11afL), MetaIdFactory.conceptId(0xcae652fa7f6e4122L, 0x8bd50b26c8b4eec8L, 0x15a6b2b9f129f958L), MetaIdFactory.conceptId(0xcae652fa7f6e4122L, 0x8bd50b26c8b4eec8L, 0x6511ed2862284a78L)).seal();
}
