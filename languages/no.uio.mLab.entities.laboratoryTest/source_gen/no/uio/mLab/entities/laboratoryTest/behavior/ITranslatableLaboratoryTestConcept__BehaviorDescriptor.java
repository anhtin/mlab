package no.uio.mLab.entities.laboratoryTest.behavior;

/*Generated by MPS */

import jetbrains.mps.core.aspects.behaviour.BaseBHDescriptor;
import org.jetbrains.mps.openapi.language.SAbstractConcept;
import jetbrains.mps.smodel.adapter.structure.MetaAdapterFactory;
import jetbrains.mps.core.aspects.behaviour.api.BehaviorRegistry;
import jetbrains.mps.smodel.language.ConceptRegistry;
import jetbrains.mps.core.aspects.behaviour.api.SMethod;
import no.uio.mLab.entities.laboratoryTest.translations.ILaboratoryTestTranslations;
import jetbrains.mps.core.aspects.behaviour.SMethodBuilder;
import jetbrains.mps.core.aspects.behaviour.SJavaCompoundTypeImpl;
import jetbrains.mps.core.aspects.behaviour.SModifiersImpl;
import jetbrains.mps.core.aspects.behaviour.AccessPrivileges;
import java.util.List;
import java.util.Arrays;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.mps.openapi.model.SNode;
import no.uio.mLab.entities.laboratoryTest.translations.LaboratoryTestTranslationProvider;
import jetbrains.mps.core.aspects.behaviour.api.SConstructor;
import org.jetbrains.annotations.Nullable;
import jetbrains.mps.core.aspects.behaviour.api.BHMethodNotFoundException;

public final class ITranslatableLaboratoryTestConcept__BehaviorDescriptor extends BaseBHDescriptor {
  private static final SAbstractConcept CONCEPT = MetaAdapterFactory.getInterfaceConcept(0xcae652fa7f6e4122L, 0x8bd50b26c8b4eec8L, 0x60f32df502c11afL, "no.uio.mLab.entities.laboratoryTest.structure.ITranslatableLaboratoryTestConcept");
  private static final BehaviorRegistry REGISTRY = ConceptRegistry.getInstance().getBehaviorRegistry();

  public static final SMethod<ILaboratoryTestTranslations> getDisplayTranslations_idofcHXgb1du = new SMethodBuilder<ILaboratoryTestTranslations>(new SJavaCompoundTypeImpl(ILaboratoryTestTranslations.class)).name("getDisplayTranslations").modifiers(SModifiersImpl.create(1, AccessPrivileges.PUBLIC)).concept(CONCEPT).id("ofcHXgb1du").registry(REGISTRY).build();
  public static final SMethod<ILaboratoryTestTranslations> getGenerationTranslations_idofcHXgb1hD = new SMethodBuilder<ILaboratoryTestTranslations>(new SJavaCompoundTypeImpl(ILaboratoryTestTranslations.class)).name("getGenerationTranslations").modifiers(SModifiersImpl.create(1, AccessPrivileges.PUBLIC)).concept(CONCEPT).id("ofcHXgb1hD").registry(REGISTRY).build();

  private static final List<SMethod<?>> BH_METHODS = Arrays.<SMethod<?>>asList(getDisplayTranslations_idofcHXgb1du, getGenerationTranslations_idofcHXgb1hD);

  private static void ___init___(@NotNull SNode __thisNode__) {
  }

  /*package*/ static ILaboratoryTestTranslations getDisplayTranslations_idofcHXgb1du(@NotNull SAbstractConcept __thisConcept__) {
    return LaboratoryTestTranslationProvider.displayTranslations;
  }
  /*package*/ static ILaboratoryTestTranslations getGenerationTranslations_idofcHXgb1hD(@NotNull SAbstractConcept __thisConcept__) {
    return LaboratoryTestTranslationProvider.generationTranslations;
  }

  /*package*/ ITranslatableLaboratoryTestConcept__BehaviorDescriptor() {
    super(REGISTRY);
  }

  @Override
  protected void initNode(@NotNull SNode node, @NotNull SConstructor constructor, @Nullable Object[] parameters) {
    ___init___(node);
  }

  @Override
  protected <T> T invokeSpecial0(@NotNull SNode node, @NotNull SMethod<T> method, @Nullable Object[] parameters) {
    int methodIndex = BH_METHODS.indexOf(method);
    if (methodIndex < 0) {
      throw new BHMethodNotFoundException(this, method);
    }
    switch (methodIndex) {
      default:
        throw new BHMethodNotFoundException(this, method);
    }
  }

  @Override
  protected <T> T invokeSpecial0(@NotNull SAbstractConcept concept, @NotNull SMethod<T> method, @Nullable Object[] parameters) {
    int methodIndex = BH_METHODS.indexOf(method);
    if (methodIndex < 0) {
      throw new BHMethodNotFoundException(this, method);
    }
    switch (methodIndex) {
      case 0:
        return (T) ((ILaboratoryTestTranslations) getDisplayTranslations_idofcHXgb1du(concept));
      case 1:
        return (T) ((ILaboratoryTestTranslations) getGenerationTranslations_idofcHXgb1hD(concept));
      default:
        throw new BHMethodNotFoundException(this, method);
    }
  }

  @NotNull
  @Override
  public List<SMethod<?>> getDeclaredMethods() {
    return BH_METHODS;
  }

  @NotNull
  @Override
  public SAbstractConcept getConcept() {
    return CONCEPT;
  }
}
