<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:5fbfb075-ca3e-47eb-965a-d943cc2f1fae(no.uio.mLab.entities.microbiology.behavior)">
  <persistence version="9" />
  <languages>
    <use id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior" version="1" />
    <use id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage" version="6" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="1yj" ref="r:0c887d67-81ab-420f-9a35-a9f6bf96c975(no.uio.mLab.shared.behavior)" />
    <import index="oxqx" ref="r:b1883212-72ef-401b-98f5-0fd849ef0508(no.uio.mLab.entities.microbiology.translations)" />
    <import index="qzk2" ref="r:111a6c30-c76c-4d86-a426-b4a1815fe195(no.uio.mLab.entities.microbiology.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior">
      <concept id="6496299201655527393" name="jetbrains.mps.lang.behavior.structure.LocalBehaviorMethodCall" flags="nn" index="BsUDl" />
      <concept id="1225194240794" name="jetbrains.mps.lang.behavior.structure.ConceptBehavior" flags="ng" index="13h7C7">
        <reference id="1225194240799" name="concept" index="13h7C2" />
        <child id="1225194240805" name="method" index="13h7CS" />
        <child id="1225194240801" name="constructor" index="13h7CW" />
      </concept>
      <concept id="1225194413805" name="jetbrains.mps.lang.behavior.structure.ConceptConstructorDeclaration" flags="in" index="13hLZK" />
      <concept id="1225194472830" name="jetbrains.mps.lang.behavior.structure.ConceptMethodDeclaration" flags="ng" index="13i0hz">
        <property id="5864038008284099149" name="isStatic" index="2Ki8OM" />
        <property id="1225194472832" name="isVirtual" index="13i0it" />
        <property id="1225194472834" name="isAbstract" index="13i0iv" />
        <reference id="1225194472831" name="overriddenMethod" index="13i0hy" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
      </concept>
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="13h7C7" id="6khVixy0Rxe">
    <ref role="13h7C2" to="qzk2:6khVixy0xXs" resolve="Pathogen" />
    <node concept="13hLZK" id="6khVixy0Rxf" role="13h7CW">
      <node concept="3clFbS" id="6khVixy0Rxg" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6khVixy0Rxp" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="6khVixy0Rxq" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixy0Rxv" role="3clF47">
        <node concept="3clFbF" id="6khVixy0Rx$" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixy0STP" role="3clFbG">
            <node concept="BsUDl" id="6khVixy0SMU" role="2Oq$k0">
              <ref role="37wK5l" node="6khVixy0SyI" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixy0SZA" role="2OqNvi">
              <ref role="37wK5l" to="oxqx:6khVixy0RIl" resolve="getPathogenAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixy0Rxw" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixy0Rx_" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="6khVixy0RxA" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixy0RxF" role="3clF47">
        <node concept="3clFbF" id="6khVixy0RxK" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixy0T0o" role="3clFbG">
            <node concept="BsUDl" id="6khVixy0T0p" role="2Oq$k0">
              <ref role="37wK5l" node="6khVixy0SyI" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixy0T0q" role="2OqNvi">
              <ref role="37wK5l" to="oxqx:6khVixy0RJ9" resolve="getPathogenDescription" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixy0RxG" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="6khVixy0Syz">
    <property role="3GE5qa" value="" />
    <ref role="13h7C2" to="qzk2:6khVixy0Sy7" resolve="IMicrobiologyEntitiesTranslatableConcept" />
    <node concept="13i0hz" id="6khVixy0SyI" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getDisplayTranslations" />
      <node concept="3Tm1VV" id="6khVixy0SyJ" role="1B3o_S" />
      <node concept="3uibUv" id="6khVixy0SzD" role="3clF45">
        <ref role="3uigEE" to="oxqx:4zMac8rUNtP" resolve="IMicrobiologyEntitiesTranslations" />
      </node>
      <node concept="3clFbS" id="6khVixy0SyL" role="3clF47">
        <node concept="3clFbF" id="6khVixy0S$e" role="3cqZAp">
          <node concept="10M0yZ" id="6khVixy0S$E" role="3clFbG">
            <ref role="3cqZAo" to="oxqx:4zMac8rVBeK" resolve="displayTranslations" />
            <ref role="1PxDUh" to="oxqx:4zMac8rUNsN" resolve="MicrobiologyEntitiesTranslationProvider" />
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="6khVixy0S_f" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getGenerationTranslations" />
      <node concept="3Tm1VV" id="6khVixy0S_g" role="1B3o_S" />
      <node concept="3uibUv" id="6khVixy0S_h" role="3clF45">
        <ref role="3uigEE" to="oxqx:4zMac8rUNtP" resolve="IMicrobiologyEntitiesTranslations" />
      </node>
      <node concept="3clFbS" id="6khVixy0S_i" role="3clF47">
        <node concept="3clFbF" id="6khVixy0S_j" role="3cqZAp">
          <node concept="10M0yZ" id="6khVixy0SBw" role="3clFbG">
            <ref role="3cqZAo" to="oxqx:1Hxyv4DUmfq" resolve="generationTranslations" />
            <ref role="1PxDUh" to="oxqx:4zMac8rUNsN" resolve="MicrobiologyEntitiesTranslationProvider" />
          </node>
        </node>
      </node>
    </node>
    <node concept="13hLZK" id="6khVixy0Sy$" role="13h7CW">
      <node concept="3clFbS" id="6khVixy0Sy_" role="2VODD2" />
    </node>
  </node>
  <node concept="13h7C7" id="6khVixydc5A">
    <ref role="13h7C2" to="qzk2:6khVixyahUI" resolve="CultureTest" />
    <node concept="13hLZK" id="6khVixydc5B" role="13h7CW">
      <node concept="3clFbS" id="6khVixydc5C" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6khVixydc5L" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="6khVixydc5M" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixydc5T" role="3clF47">
        <node concept="3clFbF" id="6khVixydcf1" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixydclX" role="3clFbG">
            <node concept="BsUDl" id="6khVixydcf0" role="2Oq$k0">
              <ref role="37wK5l" node="6khVixy0SyI" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixydec4" role="2OqNvi">
              <ref role="37wK5l" to="oxqx:6khVixydcxh" resolve="getCultureTestAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixydc5U" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixydc5Z" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="6khVixydc60" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixydc67" role="3clF47">
        <node concept="3clFbF" id="6khVixydelp" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixydest" role="3clFbG">
            <node concept="BsUDl" id="6khVixydelo" role="2Oq$k0">
              <ref role="37wK5l" node="6khVixy0SyI" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixydeyk" role="2OqNvi">
              <ref role="37wK5l" to="oxqx:6khVixydczp" resolve="getCultureTestDescription" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixydc68" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="6khVixyHu32">
    <ref role="13h7C2" to="qzk2:6khVixyGBqF" resolve="Antimicrobial" />
    <node concept="13hLZK" id="6khVixyHu33" role="13h7CW">
      <node concept="3clFbS" id="6khVixyHu34" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6khVixyHu3d" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="6khVixyHu3e" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixyHu3j" role="3clF47">
        <node concept="3clFbF" id="6khVixyHuax" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixyHuhl" role="3clFbG">
            <node concept="BsUDl" id="6khVixyHuaw" role="2Oq$k0">
              <ref role="37wK5l" node="6khVixy0SyI" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixyHun4" role="2OqNvi">
              <ref role="37wK5l" to="oxqx:6khVixyHsTu" resolve="getAntimicrobialAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixyHu3k" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixyHu3p" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="6khVixyHu3q" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixyHu3v" role="3clF47">
        <node concept="3clFbF" id="6khVixyHunR" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixyHuuV" role="3clFbG">
            <node concept="BsUDl" id="6khVixyHunQ" role="2Oq$k0">
              <ref role="37wK5l" node="6khVixy0SyI" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixyHu$O" role="2OqNvi">
              <ref role="37wK5l" to="oxqx:6khVixyHsZp" resolve="getAntimicrobialDescription" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixyHu3w" role="3clF45" />
    </node>
  </node>
  <node concept="13h7C7" id="6khVixylEWN">
    <ref role="13h7C2" to="qzk2:6khVixylEWp" resolve="SecondaryMicrobiologyWork" />
    <node concept="13hLZK" id="6khVixylEWO" role="13h7CW">
      <node concept="3clFbS" id="6khVixylEWP" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6khVixylG12" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc2$" resolve="getDisplayAlias" />
      <node concept="3Tm1VV" id="6khVixylG13" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixylG18" role="3clF47">
        <node concept="3clFbF" id="6khVixylG8p" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixylGfd" role="3clFbG">
            <node concept="BsUDl" id="6PcFLt2kqsL" role="2Oq$k0">
              <ref role="37wK5l" node="6khVixy0SyI" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixylGkY" role="2OqNvi">
              <ref role="37wK5l" to="oxqx:6khVixylEY_" resolve="getSecondaryMicrobiologyWorkAlias" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6PcFLt2kqsh" role="3clF45" />
    </node>
    <node concept="13i0hz" id="6khVixylG1e" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <ref role="13i0hy" to="1yj:5Wfdz$0vc3v" resolve="getDisplayDescription" />
      <node concept="3Tm1VV" id="6khVixylG1f" role="1B3o_S" />
      <node concept="3clFbS" id="6khVixylG1k" role="3clF47">
        <node concept="3clFbF" id="6khVixylGlT" role="3cqZAp">
          <node concept="2OqwBi" id="6khVixylGsX" role="3clFbG">
            <node concept="BsUDl" id="6PcFLt2kqtq" role="2Oq$k0">
              <ref role="37wK5l" node="6khVixy0SyI" resolve="getDisplayTranslations" />
            </node>
            <node concept="liA8E" id="6khVixylGyO" role="2OqNvi">
              <ref role="37wK5l" to="oxqx:6khVixylF0w" resolve="getSecondaryMicrobiologyWorkDescription" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="6khVixylG1l" role="3clF45" />
    </node>
  </node>
</model>

