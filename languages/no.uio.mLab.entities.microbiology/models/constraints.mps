<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:e3c7df7c-5ad9-4e38-9abb-4d5ca4a27156(no.uio.mLab.entities.microbiology.constraints)">
  <persistence version="9" />
  <languages>
    <use id="3f4bc5f5-c6c1-4a28-8b10-c83066ffa4a1" name="jetbrains.mps.lang.constraints" version="4" />
    <devkit ref="00000000-0000-4000-0000-5604ebd4f22c(jetbrains.mps.devkit.aspect.constraints)" />
  </languages>
  <imports />
  <registry />
</model>

