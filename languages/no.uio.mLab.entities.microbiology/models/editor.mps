<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:17679ce1-f5ba-44b7-88e2-dbc81077c39a(no.uio.mLab.entities.microbiology.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="11" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports />
  <registry />
</model>

