<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:111a6c30-c76c-4d86-a426-b4a1815fe195(no.uio.mLab.entities.microbiology.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="6" />
    <devkit ref="78434eb8-b0e5-444b-850d-e7c4ad2da9ab(jetbrains.mps.devkit.aspect.structure)" />
  </languages>
  <imports>
    <import index="kkto" ref="r:01f3534b-874e-42dc-a9b5-7540591f45bb(no.uio.mLab.entities.core.structure)" />
    <import index="vbok" ref="r:6cc208d3-a370-49ca-a6ac-ded97805a924(no.uio.mLab.shared.structure)" />
    <import index="xvtf" ref="r:506bae70-6209-41ca-8d8b-9690b3810379(no.uio.mLab.entities.laboratoryTest.structure)" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
      </concept>
      <concept id="1169125989551" name="jetbrains.mps.lang.structure.structure.InterfaceConceptDeclaration" flags="ig" index="PlHQZ">
        <child id="1169127546356" name="extends" index="PrDN$" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <property id="1096454100552" name="rootable" index="19KtqR" />
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599698500" name="specializedLink" index="20ksaX" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="6khVixy0xXs">
    <property role="EcuMT" value="7282862830133583708" />
    <property role="TrG5h" value="Pathogen" />
    <property role="19KtqR" value="true" />
    <ref role="1TJDcQ" to="kkto:4QUW3edDqMR" resolve="Entity" />
    <node concept="1TJgyj" id="6khVixy0AK9" role="1TKVEi">
      <property role="IQ2ns" value="7282862830133603337" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="deprecatedBy" />
      <ref role="20ksaX" to="kkto:4QUW3edDrpg" resolve="deprecatedBy" />
      <ref role="20lvS9" node="6khVixy0xXs" resolve="Pathogen" />
    </node>
    <node concept="PrWs8" id="6khVixy0SDy" role="PzmwI">
      <ref role="PrY4T" node="6khVixy0Sy7" resolve="IMicrobiologyEntitiesTranslatableConcept" />
    </node>
  </node>
  <node concept="PlHQZ" id="6khVixy0Sy7">
    <property role="EcuMT" value="7282862830133676167" />
    <property role="TrG5h" value="IMicrobiologyEntitiesTranslatableConcept" />
    <property role="3GE5qa" value="" />
    <node concept="PrWs8" id="6khVixy0Sy8" role="PrDN$">
      <ref role="PrY4T" to="vbok:5Wfdz$0vc1Z" resolve="ITranslatableConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="6khVixyahUI">
    <property role="EcuMT" value="7282862830136139438" />
    <property role="TrG5h" value="CultureTest" />
    <property role="19KtqR" value="true" />
    <ref role="1TJDcQ" to="xvtf:1mAGFBLav_o" resolve="LaboratoryTest" />
    <node concept="PrWs8" id="6khVixydcaT" role="PzmwI">
      <ref role="PrY4T" node="6khVixy0Sy7" resolve="IMicrobiologyEntitiesTranslatableConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="6khVixyGBqF">
    <property role="EcuMT" value="7282862830145140395" />
    <property role="TrG5h" value="Antimicrobial" />
    <property role="19KtqR" value="true" />
    <ref role="1TJDcQ" to="kkto:4QUW3edDqMR" resolve="Entity" />
    <node concept="PrWs8" id="6khVixyHu7l" role="PzmwI">
      <ref role="PrY4T" node="6khVixy0Sy7" resolve="IMicrobiologyEntitiesTranslatableConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="6khVixylEWp">
    <property role="EcuMT" value="7282862830139125529" />
    <property role="TrG5h" value="SecondaryMicrobiologyWork" />
    <property role="19KtqR" value="true" />
    <ref role="1TJDcQ" to="kkto:4QUW3edDqMR" resolve="Entity" />
    <node concept="PrWs8" id="6PcFLt2kqew" role="PzmwI">
      <ref role="PrY4T" node="6khVixy0Sy7" resolve="IMicrobiologyEntitiesTranslatableConcept" />
    </node>
  </node>
</model>

