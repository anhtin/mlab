<?xml version="1.0" encoding="UTF-8"?>
<solution name="no.uio.mLab.entities.microbiology.runtime" uuid="7db9e422-98cf-49d8-81dd-7afe7853ce0a" moduleVersion="0" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
  </models>
  <sourcePath />
  <languageVersions />
  <dependencyVersions>
    <module reference="7db9e422-98cf-49d8-81dd-7afe7853ce0a(no.uio.mLab.entities.microbiology.runtime)" version="0" />
  </dependencyVersions>
</solution>

