package no.uio.mLab.entities.specialistType.runtime;

/*Generated by MPS */


public class EnSpecialistTypeTranslations implements ISpecialistTypeTranslations {
  @Override
  public String getSpecialistTypeAlias() {
    return "specialist type";
  }
}
