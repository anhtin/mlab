package no.uio.mLab.entities.specialistType.runtime;

/*Generated by MPS */


public class NoLaboratoryTestTranslations implements ISpecialistTypeTranslations {
  @Override
  public String getSpecialistTypeAlias() {
    return "konsulenttype";
  }
}
