<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:0c887d67-81ab-420f-9a35-a9f6bf96c975(no.uio.mLab.shared.behavior)">
  <persistence version="9" />
  <languages>
    <use id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior" version="1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="vbok" ref="r:6cc208d3-a370-49ca-a6ac-ded97805a924(no.uio.mLab.shared.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior">
      <concept id="6496299201655527393" name="jetbrains.mps.lang.behavior.structure.LocalBehaviorMethodCall" flags="nn" index="BsUDl" />
      <concept id="1225194240794" name="jetbrains.mps.lang.behavior.structure.ConceptBehavior" flags="ng" index="13h7C7">
        <reference id="1225194240799" name="concept" index="13h7C2" />
        <child id="1225194240805" name="method" index="13h7CS" />
        <child id="1225194240801" name="constructor" index="13h7CW" />
      </concept>
      <concept id="1225194413805" name="jetbrains.mps.lang.behavior.structure.ConceptConstructorDeclaration" flags="in" index="13hLZK" />
      <concept id="1225194472830" name="jetbrains.mps.lang.behavior.structure.ConceptMethodDeclaration" flags="ng" index="13i0hz">
        <property id="5864038008284099149" name="isStatic" index="2Ki8OM" />
        <property id="1225194472832" name="isVirtual" index="13i0it" />
        <property id="1225194472834" name="isAbstract" index="13i0iv" />
      </concept>
      <concept id="1225194691553" name="jetbrains.mps.lang.behavior.structure.ThisNodeExpression" flags="nn" index="13iPFW" />
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1173175405605" name="jetbrains.mps.baseLanguage.structure.ArrayAccessExpression" flags="nn" index="AH0OO">
        <child id="1173175577737" name="index" index="AHEQo" />
        <child id="1173175590490" name="array" index="AHHXb" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1070534760951" name="jetbrains.mps.baseLanguage.structure.ArrayType" flags="in" index="10Q1$e">
        <child id="1070534760952" name="componentType" index="10Q1$1" />
      </concept>
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242875" name="jetbrains.mps.baseLanguage.structure.PlusExpression" flags="nn" index="3cpWs3" />
      <concept id="1154542696413" name="jetbrains.mps.baseLanguage.structure.ArrayCreatorWithInitializer" flags="nn" index="3g6Rrh">
        <child id="1154542793668" name="componentType" index="3g7fb8" />
        <child id="1154542803372" name="initValue" index="3g7hyw" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="7453996997717780434" name="jetbrains.mps.lang.smodel.structure.Node_GetSConceptOperation" flags="nn" index="2yIwOk" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="13h7C7" id="5Wfdz$0vc2p">
    <ref role="13h7C2" to="vbok:5Wfdz$0vc1Z" resolve="ITranslatableConcept" />
    <node concept="13i0hz" id="5Wfdz$0vc2$" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="true" />
      <property role="TrG5h" value="getDisplayAlias" />
      <property role="2Ki8OM" value="true" />
      <node concept="3Tm1VV" id="5Wfdz$0vc2_" role="1B3o_S" />
      <node concept="17QB3L" id="5Wfdz$0vc2O" role="3clF45" />
      <node concept="3clFbS" id="5Wfdz$0vc2B" role="3clF47">
        <node concept="3clFbF" id="5Wfdz$0vc5K" role="3cqZAp">
          <node concept="Xl_RD" id="5Wfdz$0vcs5" role="3clFbG">
            <property role="Xl_RC" value="[missing display alias]" />
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="5Wfdz$0vc3v" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="true" />
      <property role="TrG5h" value="getDisplayDescription" />
      <property role="2Ki8OM" value="true" />
      <node concept="3Tm1VV" id="5Wfdz$0vc3w" role="1B3o_S" />
      <node concept="17QB3L" id="5Wfdz$0vc3N" role="3clF45" />
      <node concept="3clFbS" id="5Wfdz$0vc3y" role="3clF47">
        <node concept="3clFbF" id="5Wfdz$0vct4" role="3cqZAp">
          <node concept="Xl_RD" id="5Wfdz$0vct3" role="3clFbG">
            <property role="Xl_RC" value="[missing display description]" />
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="4QUW3efxqUv" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="true" />
      <property role="TrG5h" value="getGenerationAlias" />
      <property role="2Ki8OM" value="true" />
      <node concept="3Tm1VV" id="4QUW3efxqUw" role="1B3o_S" />
      <node concept="17QB3L" id="4QUW3efxqUV" role="3clF45" />
      <node concept="3clFbS" id="4QUW3efxqUy" role="3clF47">
        <node concept="3clFbF" id="4QUW3efxqVR" role="3cqZAp">
          <node concept="Xl_RD" id="4QUW3efxqVQ" role="3clFbG">
            <property role="Xl_RC" value="[missing generation alias]" />
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="4B5aqq46Wbc" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="true" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getFirstDisplayKeyword" />
      <node concept="3Tm1VV" id="4B5aqq46Wbd" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq46Wci" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq46Wbf" role="3clF47">
        <node concept="3clFbF" id="4B5aqq46WcY" role="3cqZAp">
          <node concept="AH0OO" id="4B5aqq46WDC" role="3clFbG">
            <node concept="3cmrfG" id="4B5aqq46WED" role="AHEQo">
              <property role="3cmrfH" value="0" />
            </node>
            <node concept="BsUDl" id="4B5aqq46WcX" role="AHHXb">
              <ref role="37wK5l" node="4B5aqq3ZFNV" resolve="getDisplayKeywords" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="4B5aqq3ZFNV" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="true" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getDisplayKeywords" />
      <node concept="3Tm1VV" id="4B5aqq3ZFNW" role="1B3o_S" />
      <node concept="10Q1$e" id="4B5aqq3ZFOT" role="3clF45">
        <node concept="17QB3L" id="4B5aqq3ZFOH" role="10Q1$1" />
      </node>
      <node concept="3clFbS" id="4B5aqq3ZFNY" role="3clF47">
        <node concept="3clFbF" id="4B5aqq3ZFPZ" role="3cqZAp">
          <node concept="2ShNRf" id="4B5aqq3ZU7f" role="3clFbG">
            <node concept="3g6Rrh" id="4B5aqq3ZUkJ" role="2ShVmc">
              <node concept="17QB3L" id="4B5aqq3ZUcq" role="3g7fb8" />
              <node concept="BsUDl" id="4B5aqq3ZUrX" role="3g7hyw">
                <ref role="37wK5l" node="5Wfdz$0vc2$" resolve="getDisplayAlias" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="4B5aqq3ZFQj" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="true" />
      <property role="2Ki8OM" value="true" />
      <property role="TrG5h" value="getGenerationKeywords" />
      <node concept="3Tm1VV" id="4B5aqq3ZFQk" role="1B3o_S" />
      <node concept="10Q1$e" id="4B5aqq3ZFRg" role="3clF45">
        <node concept="17QB3L" id="4B5aqq3ZFRc" role="10Q1$1" />
      </node>
      <node concept="3clFbS" id="4B5aqq3ZFQm" role="3clF47">
        <node concept="3clFbF" id="4B5aqq3ZFS6" role="3cqZAp">
          <node concept="2ShNRf" id="4B5aqq3ZUzh" role="3clFbG">
            <node concept="3g6Rrh" id="4B5aqq3ZUzi" role="2ShVmc">
              <node concept="17QB3L" id="4B5aqq3ZUzj" role="3g7fb8" />
              <node concept="BsUDl" id="4B5aqq3ZU$c" role="3g7hyw">
                <ref role="37wK5l" node="4QUW3efxqUv" resolve="getGenerationAlias" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="13hLZK" id="5Wfdz$0vc2q" role="13h7CW">
      <node concept="3clFbS" id="5Wfdz$0vc2r" role="2VODD2" />
    </node>
  </node>
  <node concept="13h7C7" id="499Gn2DGOKf">
    <ref role="13h7C2" to="vbok:499Gn2DGOCc" resolve="IShowableConcept" />
    <node concept="13i0hz" id="499Gn2DGTs1" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="true" />
      <property role="TrG5h" value="toDisplayString" />
      <node concept="3Tm1VV" id="499Gn2DGTs2" role="1B3o_S" />
      <node concept="17QB3L" id="499Gn2DGTsh" role="3clF45" />
      <node concept="3clFbS" id="499Gn2DGTs4" role="3clF47">
        <node concept="3clFbF" id="499Gn2DGTtX" role="3cqZAp">
          <node concept="3cpWs3" id="499Gn2DGUmM" role="3clFbG">
            <node concept="Xl_RD" id="499Gn2DGUrL" role="3uHU7w">
              <property role="Xl_RC" value="]" />
            </node>
            <node concept="3cpWs3" id="499Gn2DGTKP" role="3uHU7B">
              <node concept="Xl_RD" id="499Gn2DGTtW" role="3uHU7B">
                <property role="Xl_RC" value="[missing toDisplayString from " />
              </node>
              <node concept="2OqwBi" id="499Gn2DGTW$" role="3uHU7w">
                <node concept="13iPFW" id="499Gn2DGTLb" role="2Oq$k0" />
                <node concept="2yIwOk" id="499Gn2DGU3l" role="2OqNvi" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="499Gn2DGU_t" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="true" />
      <property role="TrG5h" value="toGenerationString" />
      <node concept="3Tm1VV" id="499Gn2DGU_u" role="1B3o_S" />
      <node concept="17QB3L" id="499Gn2DGU_v" role="3clF45" />
      <node concept="3clFbS" id="499Gn2DGU_w" role="3clF47">
        <node concept="3clFbF" id="499Gn2DGU_x" role="3cqZAp">
          <node concept="3cpWs3" id="499Gn2DGU_y" role="3clFbG">
            <node concept="Xl_RD" id="499Gn2DGU_z" role="3uHU7w">
              <property role="Xl_RC" value="]" />
            </node>
            <node concept="3cpWs3" id="499Gn2DGU_$" role="3uHU7B">
              <node concept="Xl_RD" id="499Gn2DGU__" role="3uHU7B">
                <property role="Xl_RC" value="[missing toGenerationString from " />
              </node>
              <node concept="2OqwBi" id="499Gn2DGU_A" role="3uHU7w">
                <node concept="13iPFW" id="499Gn2DGU_B" role="2Oq$k0" />
                <node concept="2yIwOk" id="499Gn2DGU_C" role="2OqNvi" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="13hLZK" id="499Gn2DGOKg" role="13h7CW">
      <node concept="3clFbS" id="499Gn2DGOKh" role="2VODD2" />
    </node>
  </node>
  <node concept="13h7C7" id="4V3GMfXuP$M">
    <ref role="13h7C2" to="vbok:4QUW3efGuc3" resolve="IReferrableConcept" />
    <node concept="13i0hz" id="4V3GMfXuP$X" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="true" />
      <property role="TrG5h" value="getReferenceDescription" />
      <property role="2Ki8OM" value="false" />
      <node concept="3Tm1VV" id="4V3GMfXuP$Y" role="1B3o_S" />
      <node concept="17QB3L" id="4V3GMfXuP_d" role="3clF45" />
      <node concept="3clFbS" id="4V3GMfXuP_0" role="3clF47">
        <node concept="3clFbF" id="4V3GMfXuPA1" role="3cqZAp">
          <node concept="3cpWs3" id="4V3GMfXuQd_" role="3clFbG">
            <node concept="Xl_RD" id="4V3GMfXuQig" role="3uHU7w">
              <property role="Xl_RC" value="]" />
            </node>
            <node concept="3cpWs3" id="4V3GMfXuPST" role="3uHU7B">
              <node concept="Xl_RD" id="4V3GMfXuPA0" role="3uHU7B">
                <property role="Xl_RC" value="[missing getReferenceDescription method for " />
              </node>
              <node concept="2OqwBi" id="4V3GMfXva4K" role="3uHU7w">
                <node concept="13iPFW" id="4V3GMfXv1X8" role="2Oq$k0" />
                <node concept="2yIwOk" id="4V3GMfXvad8" role="2OqNvi" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="13hLZK" id="4V3GMfXuP$N" role="13h7CW">
      <node concept="3clFbS" id="4V3GMfXuP$O" role="2VODD2" />
    </node>
  </node>
  <node concept="13h7C7" id="7lYCqhujn6j">
    <ref role="13h7C2" to="vbok:7lYCqhujn5u" resolve="IHasUnit" />
    <node concept="13i0hz" id="7lYCqhujn6u" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="true" />
      <property role="TrG5h" value="getDisplayUnit" />
      <node concept="3Tm1VV" id="7lYCqhujn6v" role="1B3o_S" />
      <node concept="17QB3L" id="7lYCqhujn6I" role="3clF45" />
      <node concept="3clFbS" id="7lYCqhujn6x" role="3clF47">
        <node concept="3clFbF" id="7lYCqhujn7i" role="3cqZAp">
          <node concept="3cpWs3" id="7lYCqhujogx" role="3clFbG">
            <node concept="Xl_RD" id="7lYCqhujol$" role="3uHU7w">
              <property role="Xl_RC" value="]" />
            </node>
            <node concept="3cpWs3" id="7lYCqhujnpz" role="3uHU7B">
              <node concept="Xl_RD" id="7lYCqhujn7h" role="3uHU7B">
                <property role="Xl_RC" value="[missing getDisplayUnit method for " />
              </node>
              <node concept="2OqwBi" id="7lYCqhujnDq" role="3uHU7w">
                <node concept="13iPFW" id="7lYCqhujnpT" role="2Oq$k0" />
                <node concept="2yIwOk" id="7lYCqhujnQu" role="2OqNvi" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="4B5aqq6oFwi" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="true" />
      <property role="TrG5h" value="getGenerationUnit" />
      <node concept="3Tm1VV" id="4B5aqq6oFwj" role="1B3o_S" />
      <node concept="17QB3L" id="4B5aqq6oFwk" role="3clF45" />
      <node concept="3clFbS" id="4B5aqq6oFwl" role="3clF47">
        <node concept="3clFbF" id="4B5aqq6oFwm" role="3cqZAp">
          <node concept="3cpWs3" id="4B5aqq6oFwn" role="3clFbG">
            <node concept="Xl_RD" id="4B5aqq6oFwo" role="3uHU7w">
              <property role="Xl_RC" value="]" />
            </node>
            <node concept="3cpWs3" id="4B5aqq6oFwp" role="3uHU7B">
              <node concept="Xl_RD" id="4B5aqq6oFwq" role="3uHU7B">
                <property role="Xl_RC" value="[missing getGenerationUnit method for " />
              </node>
              <node concept="2OqwBi" id="4B5aqq6oFwr" role="3uHU7w">
                <node concept="13iPFW" id="4B5aqq6oFws" role="2Oq$k0" />
                <node concept="2yIwOk" id="4B5aqq6oFwt" role="2OqNvi" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="13hLZK" id="7lYCqhujn6k" role="13h7CW">
      <node concept="3clFbS" id="7lYCqhujn6l" role="2VODD2" />
    </node>
  </node>
</model>

