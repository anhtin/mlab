<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:481ae0fe-9100-4bb8-9488-731d6cfeba53(no.uio.mLab.shared.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="11" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="vbok" ref="r:6cc208d3-a370-49ca-a6ac-ded97805a924(no.uio.mLab.shared.structure)" implicit="true" />
    <import index="1yj" ref="r:0c887d67-81ab-420f-9a35-a9f6bf96c975(no.uio.mLab.shared.behavior)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="1402906326895675325" name="jetbrains.mps.lang.editor.structure.CellActionMap_FunctionParm_selectedNode" flags="nn" index="0IXxy" />
      <concept id="2491174914159318432" name="jetbrains.mps.lang.editor.structure.DominatesRecord" flags="lg" index="2lhJJ2" />
      <concept id="1142886811589" name="jetbrains.mps.lang.editor.structure.ConceptFunctionParameter_node" flags="nn" index="pncrf" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="8329266386016608055" name="jetbrains.mps.lang.editor.structure.ApproveDelete_Operation" flags="ng" index="2xy62i">
        <child id="8329266386016685951" name="editorContext" index="2xHN3q" />
      </concept>
      <concept id="1078938745671" name="jetbrains.mps.lang.editor.structure.EditorComponentDeclaration" flags="ig" index="PKFIW" />
      <concept id="1186402211651" name="jetbrains.mps.lang.editor.structure.StyleSheet" flags="ng" index="V5hpn">
        <child id="1186402402630" name="styleClass" index="V601i" />
      </concept>
      <concept id="1186403694788" name="jetbrains.mps.lang.editor.structure.ColorStyleClassItem" flags="ln" index="VaVBg">
        <property id="1186403713874" name="color" index="Vb096" />
        <child id="1186403803051" name="query" index="VblUZ" />
      </concept>
      <concept id="1186403751766" name="jetbrains.mps.lang.editor.structure.FontStyleStyleClassItem" flags="ln" index="Vb9p2">
        <property id="1186403771423" name="style" index="Vbekb" />
      </concept>
      <concept id="1186404549998" name="jetbrains.mps.lang.editor.structure.ForegroundColorStyleClassItem" flags="ln" index="VechU" />
      <concept id="1186414536763" name="jetbrains.mps.lang.editor.structure.BooleanStyleSheetItem" flags="ln" index="VOi$J">
        <property id="1186414551515" name="flag" index="VOm3f" />
      </concept>
      <concept id="1186414976055" name="jetbrains.mps.lang.editor.structure.DrawBorderStyleClassItem" flags="ln" index="VPXOz" />
      <concept id="1186414999511" name="jetbrains.mps.lang.editor.structure.UnderlinedStyleClassItem" flags="ln" index="VQ3r3">
        <property id="1214316229833" name="underlined" index="2USNnj" />
      </concept>
      <concept id="1233758997495" name="jetbrains.mps.lang.editor.structure.PunctuationLeftStyleClassItem" flags="ln" index="11L4FC" />
      <concept id="1233759184865" name="jetbrains.mps.lang.editor.structure.PunctuationRightStyleClassItem" flags="ln" index="11LMrY" />
      <concept id="3383245079137382180" name="jetbrains.mps.lang.editor.structure.StyleClass" flags="ig" index="14StLt">
        <child id="3383245079137422296" name="dominates" index="14Sbyx" />
      </concept>
      <concept id="1139535219966" name="jetbrains.mps.lang.editor.structure.CellActionMapDeclaration" flags="ig" index="1h_SRR">
        <reference id="1139535219968" name="applicableConcept" index="1h_SK9" />
        <child id="1139535219969" name="item" index="1h_SK8" />
      </concept>
      <concept id="1139535280617" name="jetbrains.mps.lang.editor.structure.CellActionMapItem" flags="lg" index="1hA7zw">
        <property id="1139535298778" name="actionId" index="1hAc7j" />
        <child id="1139535280620" name="executeFunction" index="1hA7z_" />
      </concept>
      <concept id="1139535439104" name="jetbrains.mps.lang.editor.structure.CellActionMap_ExecuteFunction" flags="in" index="1hAIg9" />
      <concept id="1225456267680" name="jetbrains.mps.lang.editor.structure.RGBColor" flags="ng" index="1iSF2X">
        <property id="1225456424731" name="value" index="1iTho6" />
      </concept>
      <concept id="1236262245656" name="jetbrains.mps.lang.editor.structure.MatchingLabelStyleClassItem" flags="ln" index="3mYdg7">
        <property id="1238091709220" name="labelName" index="1413C4" />
      </concept>
      <concept id="9122903797312246523" name="jetbrains.mps.lang.editor.structure.StyleReference" flags="ng" index="1wgc9g">
        <reference id="9122903797312247166" name="style" index="1wgcnl" />
      </concept>
      <concept id="1073389214265" name="jetbrains.mps.lang.editor.structure.EditorCellModel" flags="ng" index="3EYTF0">
        <reference id="1139959269582" name="actionMap" index="1ERwB7" />
      </concept>
      <concept id="1219418625346" name="jetbrains.mps.lang.editor.structure.IStyleContainer" flags="ng" index="3F0Thp">
        <child id="1219418656006" name="styleItem" index="3F10Kt" />
      </concept>
      <concept id="1225898583838" name="jetbrains.mps.lang.editor.structure.ReadOnlyModelAccessor" flags="ng" index="1HfYo3">
        <child id="1225898971709" name="getter" index="1Hhtcw" />
      </concept>
      <concept id="1225900081164" name="jetbrains.mps.lang.editor.structure.CellModel_ReadOnlyModelAccessor" flags="sg" stub="3708815482283559694" index="1HlG4h">
        <child id="1225900141900" name="modelAccessor" index="1HlULh" />
      </concept>
      <concept id="1161622981231" name="jetbrains.mps.lang.editor.structure.ConceptFunctionParameter_editorContext" flags="nn" index="1Q80Hx" />
      <concept id="1176717841777" name="jetbrains.mps.lang.editor.structure.QueryFunction_ModelAccess_Getter" flags="in" index="3TQlhw" />
      <concept id="1950447826681509042" name="jetbrains.mps.lang.editor.structure.ApplyStyleClass" flags="lg" index="3Xmtl4">
        <child id="1950447826683828796" name="target" index="3XvnJa" />
      </concept>
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6" />
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="7453996997717780434" name="jetbrains.mps.lang.smodel.structure.Node_GetSConceptOperation" flags="nn" index="2yIwOk" />
      <concept id="1228341669568" name="jetbrains.mps.lang.smodel.structure.Node_DetachOperation" flags="nn" index="3YRAZt" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
      <concept id="709746936026466394" name="jetbrains.mps.lang.core.structure.ChildAttribute" flags="ng" index="3VBwX9">
        <property id="709746936026609031" name="linkId" index="3V$3ak" />
        <property id="709746936026609029" name="linkRole" index="3V$3am" />
      </concept>
      <concept id="4452961908202556907" name="jetbrains.mps.lang.core.structure.BaseCommentAttribute" flags="ng" index="1X3_iC">
        <child id="3078666699043039389" name="commentedNode" index="8Wnug" />
      </concept>
    </language>
  </registry>
  <node concept="PKFIW" id="5ZQBr_XMEtE">
    <property role="3GE5qa" value="" />
    <property role="TrG5h" value="TranslatedAlias" />
    <ref role="1XX52x" to="vbok:5Wfdz$0vc1Z" resolve="ITranslatableConcept" />
    <node concept="1HlG4h" id="5ZQBr_XMEtG" role="2wV5jI">
      <ref role="1ERwB7" node="1mAGFBJU9EC" resolve="DeleteSelf_ActionMap" />
      <node concept="1HfYo3" id="5ZQBr_XMEtI" role="1HlULh">
        <node concept="3TQlhw" id="5ZQBr_XMEtK" role="1Hhtcw">
          <node concept="3clFbS" id="5ZQBr_XMEtM" role="2VODD2">
            <node concept="3clFbF" id="4B5aqq45VwG" role="3cqZAp">
              <node concept="2OqwBi" id="4B5aqq46ZIi" role="3clFbG">
                <node concept="2OqwBi" id="4B5aqq46YYb" role="2Oq$k0">
                  <node concept="pncrf" id="4B5aqq46YKq" role="2Oq$k0" />
                  <node concept="2yIwOk" id="4B5aqq46Zl7" role="2OqNvi" />
                </node>
                <node concept="2qgKlT" id="4B5aqq4708d" role="2OqNvi">
                  <ref role="37wK5l" to="1yj:4B5aqq46Wbc" resolve="getFirstDisplayKeyword" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="V5hpn" id="2z0vFKsMLG2">
    <property role="TrG5h" value="mLab_Stylesheet" />
    <property role="3GE5qa" value="" />
    <node concept="14StLt" id="2XLt5KUuXkg" role="V601i">
      <property role="TrG5h" value="ActionKeyword" />
      <node concept="VechU" id="2XLt5KUuXl4" role="3F10Kt">
        <property role="Vb096" value="DARK_BLUE" />
      </node>
      <node concept="Vb9p2" id="2XLt5KUuXl5" role="3F10Kt">
        <property role="Vbekb" value="BOLD" />
      </node>
    </node>
    <node concept="14StLt" id="4B5aqq376uT" role="V601i">
      <property role="TrG5h" value="AspectKeyword" />
      <node concept="VechU" id="4B5aqq38suo" role="3F10Kt">
        <property role="Vb096" value="black" />
      </node>
      <node concept="Vb9p2" id="4B5aqq376vS" role="3F10Kt">
        <property role="Vbekb" value="BOLD" />
      </node>
    </node>
    <node concept="14StLt" id="CxH2rE0tig" role="V601i">
      <property role="TrG5h" value="Bordered" />
      <node concept="VPXOz" id="CxH2rE0tiM" role="3F10Kt">
        <property role="VOm3f" value="true" />
      </node>
    </node>
    <node concept="14StLt" id="499Gn2DynVy" role="V601i">
      <property role="TrG5h" value="ConditionName" />
      <node concept="VechU" id="499Gn2DyonH" role="3F10Kt">
        <property role="Vb096" value="black" />
      </node>
      <node concept="Vb9p2" id="499Gn2DyonN" role="3F10Kt">
        <property role="Vbekb" value="BOLD" />
      </node>
    </node>
    <node concept="14StLt" id="2XLt5KU4_eA" role="V601i">
      <property role="TrG5h" value="ConditionKeyword" />
      <node concept="VechU" id="2XLt5KU4_jI" role="3F10Kt">
        <property role="Vb096" value="DARK_BLUE" />
      </node>
      <node concept="Vb9p2" id="2XLt5KU4_jO" role="3F10Kt">
        <property role="Vbekb" value="BOLD" />
      </node>
    </node>
    <node concept="14StLt" id="2XLt5KUuxu$" role="V601i">
      <property role="TrG5h" value="ConditionNotKeyword" />
      <node concept="VechU" id="2XLt5KUuxvl" role="3F10Kt">
        <property role="Vb096" value="DARK_MAGENTA" />
        <node concept="1iSF2X" id="2XLt5KUuxvm" role="VblUZ">
          <property role="1iTho6" value="ad3a3a" />
        </node>
      </node>
      <node concept="Vb9p2" id="2XLt5KUuxvn" role="3F10Kt">
        <property role="Vbekb" value="BOLD" />
      </node>
    </node>
    <node concept="14StLt" id="2XLt5KV6Hr$" role="V601i">
      <property role="TrG5h" value="ConstraintKeyword" />
      <node concept="VechU" id="2XLt5KV6Hsr" role="3F10Kt">
        <property role="Vb096" value="DARK_BLUE" />
      </node>
      <node concept="Vb9p2" id="2XLt5KV6Hss" role="3F10Kt">
        <property role="Vbekb" value="BOLD" />
      </node>
    </node>
    <node concept="14StLt" id="2XLt5KUltJN" role="V601i">
      <property role="TrG5h" value="DataValueKeyword" />
      <node concept="VechU" id="2XLt5KUltKx" role="3F10Kt">
        <property role="Vb096" value="DARK_BLUE" />
      </node>
      <node concept="Vb9p2" id="2XLt5KUltKz" role="3F10Kt">
        <property role="Vbekb" value="BOLD" />
      </node>
    </node>
    <node concept="14StLt" id="2z0vFKsF8Jf" role="V601i">
      <property role="TrG5h" value="Header" />
      <node concept="Vb9p2" id="2z0vFKsFt$4" role="3F10Kt">
        <property role="Vbekb" value="BOLD" />
      </node>
      <node concept="VechU" id="2z0vFKsF8Jn" role="3F10Kt">
        <property role="Vb096" value="DARK_GREEN" />
      </node>
      <node concept="VQ3r3" id="2z0vFKsF8J_" role="3F10Kt">
        <property role="2USNnj" value="2" />
      </node>
    </node>
    <node concept="14StLt" id="5ZQBr_XMuMQ" role="V601i">
      <property role="TrG5h" value="Identifier" />
      <node concept="VechU" id="5ZQBr_XMuMR" role="3F10Kt">
        <property role="Vb096" value="black" />
      </node>
      <node concept="Vb9p2" id="6khVixyGyXv" role="3F10Kt">
        <property role="Vbekb" value="ITALIC" />
      </node>
      <node concept="1X3_iC" id="6khVixyGyhp" role="lGtFl">
        <property role="3V$3am" value="styleItem" />
        <property role="3V$3ak" value="18bc6592-03a6-4e29-a83a-7ff23bde13ba/1219418625346/1219418656006" />
        <node concept="Vb9p2" id="5ZQBr_XMuMS" role="8Wnug">
          <property role="Vbekb" value="BOLD" />
        </node>
      </node>
    </node>
    <node concept="14StLt" id="2z0vFKsMLG5" role="V601i">
      <property role="TrG5h" value="Keyword" />
      <node concept="VechU" id="2z0vFKsMLG8" role="3F10Kt">
        <property role="Vb096" value="DARK_BLUE" />
      </node>
      <node concept="Vb9p2" id="2z0vFKsMLGd" role="3F10Kt">
        <property role="Vbekb" value="BOLD" />
      </node>
    </node>
    <node concept="14StLt" id="5ZQBr_XMuMW" role="V601i">
      <property role="TrG5h" value="Label" />
      <node concept="VechU" id="5ZQBr_XMuMX" role="3F10Kt">
        <property role="Vb096" value="DARK_MAGENTA" />
      </node>
      <node concept="Vb9p2" id="301PNdp0GJK" role="3F10Kt" />
    </node>
    <node concept="14StLt" id="17XAtu7YBTa" role="V601i">
      <property role="TrG5h" value="NumberLiteral" />
      <node concept="VechU" id="17XAtu7YCio" role="3F10Kt">
        <property role="Vb096" value="blue" />
      </node>
    </node>
    <node concept="14StLt" id="5ZQBr_WQpo$" role="V601i">
      <property role="TrG5h" value="StringLiteral" />
      <node concept="VechU" id="5ZQBr_WQpoU" role="3F10Kt">
        <property role="Vb096" value="DARK_GREEN" />
      </node>
    </node>
    <node concept="14StLt" id="6khVixykMkg" role="V601i">
      <property role="TrG5h" value="Literal" />
      <node concept="VechU" id="6khVixykMla" role="3F10Kt">
        <property role="Vb096" value="blue" />
      </node>
    </node>
    <node concept="14StLt" id="17XAtu7SK6X" role="V601i">
      <property role="TrG5h" value="LeftBracket" />
      <node concept="11LMrY" id="17XAtu7SK7t" role="3F10Kt">
        <property role="VOm3f" value="true" />
      </node>
      <node concept="3mYdg7" id="17XAtu7SK7u" role="3F10Kt">
        <property role="1413C4" value="bracket" />
      </node>
    </node>
    <node concept="14StLt" id="17XAtu7SK88" role="V601i">
      <property role="TrG5h" value="RightBracket" />
      <node concept="11L4FC" id="17XAtu7SK8O" role="3F10Kt">
        <property role="VOm3f" value="true" />
      </node>
      <node concept="3mYdg7" id="17XAtu7SK8J" role="3F10Kt">
        <property role="1413C4" value="bracket" />
      </node>
    </node>
    <node concept="14StLt" id="17XAtu7W6_l" role="V601i">
      <property role="TrG5h" value="LeftQuote" />
      <node concept="3Xmtl4" id="17XAtu7W6Cx" role="3F10Kt">
        <node concept="1wgc9g" id="17XAtu7W6CH" role="3XvnJa">
          <ref role="1wgcnl" node="5ZQBr_WQpo$" resolve="StringLiteral" />
        </node>
      </node>
      <node concept="11LMrY" id="17XAtu7W6A1" role="3F10Kt">
        <property role="VOm3f" value="true" />
      </node>
    </node>
    <node concept="14StLt" id="17XAtu7W6Bt" role="V601i">
      <property role="TrG5h" value="RightQuote" />
      <node concept="3Xmtl4" id="17XAtu7W6CS" role="3F10Kt">
        <node concept="1wgc9g" id="17XAtu7W6D4" role="3XvnJa">
          <ref role="1wgcnl" node="5ZQBr_WQpo$" resolve="StringLiteral" />
        </node>
      </node>
      <node concept="11L4FC" id="17XAtu7W6Ck" role="3F10Kt">
        <property role="VOm3f" value="true" />
      </node>
    </node>
    <node concept="14StLt" id="1Hxyv4EJjSm" role="V601i">
      <property role="TrG5h" value="Underlight" />
      <node concept="VechU" id="1Hxyv4EJjSY" role="3F10Kt">
        <property role="Vb096" value="gray" />
      </node>
      <node concept="2lhJJ2" id="1Hxyv4EJjSU" role="14Sbyx" />
    </node>
  </node>
  <node concept="1h_SRR" id="1mAGFBJU9EC">
    <property role="TrG5h" value="DeleteSelf_ActionMap" />
    <ref role="1h_SK9" to="vbok:5Wfdz$0vc1Z" resolve="ITranslatableConcept" />
    <node concept="1hA7zw" id="1mAGFBJU9ED" role="1h_SK8">
      <property role="1hAc7j" value="backspace_action_id" />
      <node concept="1hAIg9" id="1mAGFBJU9EE" role="1hA7z_">
        <node concept="3clFbS" id="1mAGFBJU9EF" role="2VODD2">
          <node concept="3clFbJ" id="1mAGFBJU9ER" role="3cqZAp">
            <node concept="2OqwBi" id="1mAGFBJU9NO" role="3clFbw">
              <node concept="0IXxy" id="1mAGFBJU9Fb" role="2Oq$k0" />
              <node concept="2xy62i" id="1mAGFBJU9Wm" role="2OqNvi">
                <node concept="1Q80Hx" id="1mAGFBJU9WR" role="2xHN3q" />
              </node>
            </node>
            <node concept="3clFbS" id="1mAGFBJU9ET" role="3clFbx">
              <node concept="3cpWs6" id="1mAGFBJU9Xm" role="3cqZAp" />
            </node>
          </node>
          <node concept="3clFbF" id="1mAGFBJU9Y1" role="3cqZAp">
            <node concept="2OqwBi" id="1mAGFBJUa57" role="3clFbG">
              <node concept="0IXxy" id="1mAGFBJU9XZ" role="2Oq$k0" />
              <node concept="3YRAZt" id="1mAGFBJUadP" role="2OqNvi" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

