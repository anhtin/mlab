<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:6cc208d3-a370-49ca-a6ac-ded97805a924(no.uio.mLab.shared.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="6" />
    <devkit ref="78434eb8-b0e5-444b-850d-e7c4ad2da9ab(jetbrains.mps.devkit.aspect.structure)" />
  </languages>
  <imports>
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
      </concept>
      <concept id="1169125989551" name="jetbrains.mps.lang.structure.structure.InterfaceConceptDeclaration" flags="ig" index="PlHQZ">
        <child id="1169127546356" name="extends" index="PrDN$" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="PlHQZ" id="5Wfdz$0vc1Z">
    <property role="EcuMT" value="6849753176703025279" />
    <property role="TrG5h" value="ITranslatableConcept" />
    <node concept="PrWs8" id="499Gn2DGOCd" role="PrDN$">
      <ref role="PrY4T" node="499Gn2DGOCc" resolve="IShowableConcept" />
    </node>
  </node>
  <node concept="PlHQZ" id="499Gn2DGOCc">
    <property role="EcuMT" value="4776543977244674572" />
    <property role="TrG5h" value="IShowableConcept" />
  </node>
  <node concept="PlHQZ" id="4QUW3efGuc3">
    <property role="EcuMT" value="5601053190833431299" />
    <property role="TrG5h" value="IReferrableConcept" />
    <node concept="PrWs8" id="4V3GMfXBfbF" role="PrDN$">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="PrWs8" id="4V3GMfXBew5" role="PrDN$">
      <ref role="PrY4T" node="5Wfdz$0vc1Z" resolve="ITranslatableConcept" />
    </node>
  </node>
  <node concept="PlHQZ" id="7lYCqhujn5u">
    <property role="EcuMT" value="8466382076831953246" />
    <property role="TrG5h" value="IHasUnit" />
  </node>
</model>

