<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:0ba33904-c124-449b-bde4-640f55a71953(no.uio.mLab.shared.translations)">
  <persistence version="9" />
  <languages>
    <use id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage" version="6" />
  </languages>
  <imports />
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1070462154015" name="jetbrains.mps.baseLanguage.structure.StaticFieldDeclaration" flags="ig" index="Wx3nA">
        <property id="6468716278899126575" name="isVolatile" index="2dlcS1" />
        <property id="6468716278899125786" name="isTransient" index="2dld4O" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu">
        <property id="1221565133444" name="isFinal" index="1EXbeo" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <property id="1176718929932" name="isFinal" index="3TUv4t" />
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1107461130800" name="jetbrains.mps.baseLanguage.structure.Classifier" flags="ng" index="3pOWGL">
        <child id="5375687026011219971" name="member" index="jymVt" unordered="true" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="312cEu" id="5fXsrCVmxTN">
    <property role="TrG5h" value="TranslationConfiguration" />
    <property role="1EXbeo" value="false" />
    <node concept="Wx3nA" id="5fXsrCVmzVm" role="jymVt">
      <property role="TrG5h" value="displayLanguage" />
      <property role="2dlcS1" value="false" />
      <property role="3TUv4t" value="false" />
      <property role="2dld4O" value="false" />
      <node concept="17QB3L" id="5fXsrCVmzVp" role="1tU5fm" />
      <node concept="Xl_RD" id="5fXsrCVmzVq" role="33vP2m">
        <property role="Xl_RC" value="en" />
      </node>
      <node concept="3Tm1VV" id="5fXsrCVmzVo" role="1B3o_S" />
    </node>
    <node concept="Wx3nA" id="5fXsrCVmzVN" role="jymVt">
      <property role="TrG5h" value="generationLanguage" />
      <property role="2dlcS1" value="false" />
      <property role="3TUv4t" value="false" />
      <property role="2dld4O" value="false" />
      <node concept="17QB3L" id="5fXsrCVmzVQ" role="1tU5fm" />
      <node concept="Xl_RD" id="5fXsrCVmzVR" role="33vP2m">
        <property role="Xl_RC" value="en" />
      </node>
      <node concept="3Tm1VV" id="5fXsrCVmzVP" role="1B3o_S" />
    </node>
    <node concept="3Tm1VV" id="5fXsrCVmxTO" role="1B3o_S" />
  </node>
</model>

