<?xml version="1.0" encoding="UTF-8"?>
<solution name="no.uio.mLab.shared.runtime" uuid="39b6c822-5e7e-4849-bc08-d30db203f584" moduleVersion="0" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
  </models>
  <sourcePath />
  <languageVersions>
    <language slang="l:f3061a53-9226-4cc5-a443-f952ceaf5816:jetbrains.mps.baseLanguage" version="6" />
    <language slang="l:ceab5195-25ea-4f22-9b92-103b95ca8c0c:jetbrains.mps.lang.core" version="1" />
    <language slang="l:9ded098b-ad6a-4657-bfd9-48636cfe8bc3:jetbrains.mps.lang.traceable" version="0" />
  </languageVersions>
  <dependencyVersions>
    <module reference="39b6c822-5e7e-4849-bc08-d30db203f584(no.uio.mLab.shared.runtime)" version="0" />
  </dependencyVersions>
</solution>

