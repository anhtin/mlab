<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:57fee833-4ddf-489d-93f3-21c9e2e96777(no.furst.mLab.build)">
  <persistence version="9" />
  <languages>
    <use id="798100da-4f0a-421a-b991-71f8c50ce5d2" name="jetbrains.mps.build" version="0" />
    <use id="0cf935df-4699-4e9c-a132-fa109541cba3" name="jetbrains.mps.build.mps" version="5" />
  </languages>
  <imports>
    <import index="ffeo" ref="r:874d959d-e3b4-4d04-b931-ca849af130dd(jetbrains.mps.ide.build)" />
    <import index="lv7u" ref="r:7c3361f6-3c2c-4cdf-8d3e-794e424e582d(no.uio.mLab.build)" />
    <import index="al5i" ref="r:742f344d-4dc4-4862-992c-4bc94b094870(com.mbeddr.mpsutil.dev.build)" />
  </imports>
  <registry>
    <language id="798100da-4f0a-421a-b991-71f8c50ce5d2" name="jetbrains.mps.build">
      <concept id="5481553824944787378" name="jetbrains.mps.build.structure.BuildSourceProjectRelativePath" flags="ng" index="55IIr" />
      <concept id="2755237150521975431" name="jetbrains.mps.build.structure.BuildVariableMacroInitWithString" flags="ng" index="aVJcg">
        <child id="2755237150521975437" name="value" index="aVJcq" />
      </concept>
      <concept id="244868996532454372" name="jetbrains.mps.build.structure.BuildVariableMacroInitWithDate" flags="ng" index="hHN3E">
        <property id="244868996532454384" name="pattern" index="hHN3Y" />
      </concept>
      <concept id="7321017245476976379" name="jetbrains.mps.build.structure.BuildRelativePath" flags="ng" index="iG8Mu">
        <child id="7321017245477039051" name="compositePart" index="iGT6I" />
      </concept>
      <concept id="3767587139141066978" name="jetbrains.mps.build.structure.BuildVariableMacro" flags="ng" index="2kB4xC">
        <child id="2755237150521975432" name="initialValue" index="aVJcv" />
      </concept>
      <concept id="4993211115183325728" name="jetbrains.mps.build.structure.BuildProjectDependency" flags="ng" index="2sgV4H">
        <reference id="5617550519002745380" name="script" index="1l3spb" />
        <child id="4129895186893471026" name="artifacts" index="2JcizS" />
      </concept>
      <concept id="4380385936562003279" name="jetbrains.mps.build.structure.BuildString" flags="ng" index="NbPM2">
        <child id="4903714810883783243" name="parts" index="3MwsjC" />
      </concept>
      <concept id="8618885170173601777" name="jetbrains.mps.build.structure.BuildCompositePath" flags="nn" index="2Ry0Ak">
        <property id="8618885170173601779" name="head" index="2Ry0Am" />
        <child id="8618885170173601778" name="tail" index="2Ry0An" />
      </concept>
      <concept id="2591537044435828004" name="jetbrains.mps.build.structure.BuildLayout_CompileOutputOf" flags="ng" index="Saw0i">
        <reference id="2591537044435828006" name="module" index="Saw0g" />
      </concept>
      <concept id="6647099934206700647" name="jetbrains.mps.build.structure.BuildJavaPlugin" flags="ng" index="10PD9b" />
      <concept id="7389400916848050074" name="jetbrains.mps.build.structure.BuildLayout_Jar" flags="ng" index="3981dx" />
      <concept id="7389400916848050060" name="jetbrains.mps.build.structure.BuildLayout_NamedContainer" flags="ng" index="3981dR">
        <child id="4380385936562148502" name="containerName" index="Nbhlr" />
      </concept>
      <concept id="7389400916848036984" name="jetbrains.mps.build.structure.BuildLayout_Folder" flags="ng" index="398223" />
      <concept id="7389400916848136194" name="jetbrains.mps.build.structure.BuildFolderMacro" flags="ng" index="398rNT">
        <child id="7389400916848144618" name="defaultPath" index="398pKh" />
      </concept>
      <concept id="7389400916848153117" name="jetbrains.mps.build.structure.BuildSourceMacroRelativePath" flags="ng" index="398BVA">
        <reference id="7389400916848153130" name="macro" index="398BVh" />
      </concept>
      <concept id="5617550519002745364" name="jetbrains.mps.build.structure.BuildLayout" flags="ng" index="1l3spV" />
      <concept id="5617550519002745363" name="jetbrains.mps.build.structure.BuildProject" flags="ng" index="1l3spW">
        <property id="4915877860348071612" name="fileName" index="turDy" />
        <property id="5204048710541015587" name="internalBaseDirectory" index="2DA0ip" />
        <child id="6647099934206700656" name="plugins" index="10PD9s" />
        <child id="7389400916848080626" name="parts" index="3989C9" />
        <child id="5617550519002745381" name="dependencies" index="1l3spa" />
        <child id="5617550519002745378" name="macros" index="1l3spd" />
        <child id="5617550519002745372" name="layout" index="1l3spN" />
      </concept>
      <concept id="4701820937132344003" name="jetbrains.mps.build.structure.BuildLayout_Container" flags="ng" index="1y1bJS">
        <child id="7389400916848037006" name="children" index="39821P" />
      </concept>
      <concept id="841011766566059607" name="jetbrains.mps.build.structure.BuildStringNotEmpty" flags="ng" index="3_J27D" />
      <concept id="4903714810883702019" name="jetbrains.mps.build.structure.BuildTextStringPart" flags="ng" index="3Mxwew">
        <property id="4903714810883755350" name="text" index="3MwjfP" />
      </concept>
      <concept id="4903714810883702017" name="jetbrains.mps.build.structure.BuildVarRefStringPart" flags="ng" index="3Mxwey">
        <reference id="4903714810883702018" name="macro" index="3Mxwex" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="0cf935df-4699-4e9c-a132-fa109541cba3" name="jetbrains.mps.build.mps">
      <concept id="6592112598314586625" name="jetbrains.mps.build.mps.structure.BuildMps_IdeaPluginGroup" flags="ng" index="m$f5U">
        <reference id="6592112598314586626" name="group" index="m$f5T" />
      </concept>
      <concept id="6592112598314498932" name="jetbrains.mps.build.mps.structure.BuildMps_IdeaPlugin" flags="ng" index="m$_wf">
        <property id="6592112598314498927" name="id" index="m$_wk" />
        <child id="6592112598314498931" name="version" index="m$_w8" />
        <child id="6592112598314499050" name="content" index="m$_yh" />
        <child id="6592112598314499028" name="dependencies" index="m$_yJ" />
        <child id="6592112598314499021" name="name" index="m$_yQ" />
        <child id="6592112598314855574" name="containerName" index="m_cZH" />
      </concept>
      <concept id="6592112598314499027" name="jetbrains.mps.build.mps.structure.BuildMps_IdeaPluginDependency" flags="ng" index="m$_yC">
        <reference id="6592112598314499066" name="target" index="m$_y1" />
      </concept>
      <concept id="6592112598314795900" name="jetbrains.mps.build.mps.structure.BuildMpsLayout_PluginDescriptor" flags="ng" index="m_q07">
        <reference id="6592112598314795901" name="plugin" index="m_q06" />
      </concept>
      <concept id="1500819558095907805" name="jetbrains.mps.build.mps.structure.BuildMps_Group" flags="ng" index="2G$12M">
        <child id="1500819558095907806" name="modules" index="2G$12L" />
      </concept>
      <concept id="1265949165890536423" name="jetbrains.mps.build.mps.structure.BuildMpsLayout_ModuleJars" flags="ng" index="L2wRC">
        <reference id="1265949165890536425" name="module" index="L2wRA" />
      </concept>
      <concept id="868032131020265945" name="jetbrains.mps.build.mps.structure.BuildMPSPlugin" flags="ng" index="3b7kt6" />
      <concept id="5253498789149381388" name="jetbrains.mps.build.mps.structure.BuildMps_Module" flags="ng" index="3bQrTs">
        <property id="1500819558096356884" name="doNotCompile" index="2GAjPV" />
        <child id="5253498789149547704" name="dependencies" index="3bR37C" />
      </concept>
      <concept id="5253498789149585690" name="jetbrains.mps.build.mps.structure.BuildMps_ModuleDependencyOnModule" flags="ng" index="3bR9La">
        <property id="5253498789149547713" name="reexport" index="3bR36h" />
        <reference id="5253498789149547705" name="module" index="3bR37D" />
      </concept>
      <concept id="5507251971038816436" name="jetbrains.mps.build.mps.structure.BuildMps_Generator" flags="ng" index="1yeLz9" />
      <concept id="4278635856200794926" name="jetbrains.mps.build.mps.structure.BuildMps_ModuleDependencyExtendLanguage" flags="ng" index="1Busua">
        <reference id="4278635856200794928" name="language" index="1Busuk" />
      </concept>
      <concept id="3189788309731981027" name="jetbrains.mps.build.mps.structure.BuildMps_ModuleSolutionRuntime" flags="ng" index="1E0d5M">
        <reference id="3189788309731981028" name="solution" index="1E0d5P" />
      </concept>
      <concept id="3189788309731840247" name="jetbrains.mps.build.mps.structure.BuildMps_Solution" flags="ng" index="1E1JtA" />
      <concept id="3189788309731840248" name="jetbrains.mps.build.mps.structure.BuildMps_Language" flags="ng" index="1E1JtD">
        <child id="3189788309731917348" name="runtime" index="1E1XAP" />
        <child id="9200313594498201639" name="generator" index="1TViLv" />
      </concept>
      <concept id="322010710375794190" name="jetbrains.mps.build.mps.structure.BuildMps_DevKit" flags="ng" index="3LEwk6">
        <child id="322010710375805250" name="extends" index="3LEz9a" />
        <child id="322010710375832962" name="exports" index="3LEDUa" />
      </concept>
      <concept id="322010710375805242" name="jetbrains.mps.build.mps.structure.BuildMps_DevKitRef" flags="ng" index="3LEz8M">
        <reference id="322010710375805243" name="devkit" index="3LEz8N" />
      </concept>
      <concept id="322010710375832938" name="jetbrains.mps.build.mps.structure.BuildMps_DevKitExportLanguage" flags="ng" index="3LEDTy">
        <reference id="322010710375832947" name="language" index="3LEDTV" />
      </concept>
      <concept id="322010710375871467" name="jetbrains.mps.build.mps.structure.BuildMps_AbstractModule" flags="ng" index="3LEN3z">
        <property id="8369506495128725901" name="compact" index="BnDLt" />
        <property id="322010710375892619" name="uuid" index="3LESm3" />
        <child id="322010710375956261" name="path" index="3LF7KH" />
      </concept>
      <concept id="7259033139236285166" name="jetbrains.mps.build.mps.structure.BuildMps_ExtractedModuleDependency" flags="nn" index="1SiIV0">
        <child id="7259033139236285167" name="dependency" index="1SiIV1" />
      </concept>
    </language>
  </registry>
  <node concept="1l3spW" id="ofcHXg4JVC">
    <property role="TrG5h" value="no.furst.mLab" />
    <property role="2DA0ip" value="../../" />
    <property role="turDy" value="build_furst_mLab_plugin.xml" />
    <node concept="2kB4xC" id="ofcHXg4JVD" role="1l3spd">
      <property role="TrG5h" value="date" />
      <node concept="hHN3E" id="ofcHXg4JVE" role="aVJcv">
        <property role="hHN3Y" value="yyyyMMdd" />
      </node>
    </node>
    <node concept="2kB4xC" id="ofcHXg4JVF" role="1l3spd">
      <property role="TrG5h" value="build.number" />
      <node concept="aVJcg" id="ofcHXg4JVG" role="aVJcv">
        <node concept="NbPM2" id="ofcHXg4JVH" role="aVJcq">
          <node concept="3Mxwew" id="ofcHXg4JVI" role="3MwsjC">
            <property role="3MwjfP" value="mLabEngineFurstExtensions-182.SNAPSHOT" />
          </node>
        </node>
      </node>
    </node>
    <node concept="10PD9b" id="ofcHXg4JVJ" role="10PD9s" />
    <node concept="3b7kt6" id="ofcHXg4JVK" role="10PD9s" />
    <node concept="398rNT" id="ofcHXg4JVL" role="1l3spd">
      <property role="TrG5h" value="mps_home" />
      <node concept="55IIr" id="ofcHXg4JVM" role="398pKh">
        <node concept="2Ry0Ak" id="ofcHXg4JVN" role="iGT6I">
          <property role="2Ry0Am" value=".." />
          <node concept="2Ry0Ak" id="ofcHXg4JVO" role="2Ry0An">
            <property role="2Ry0Am" value=".." />
            <node concept="2Ry0Ak" id="ofcHXg4JVP" role="2Ry0An">
              <property role="2Ry0Am" value=".." />
              <node concept="2Ry0Ak" id="ofcHXg4JVQ" role="2Ry0An">
                <property role="2Ry0Am" value=".." />
                <node concept="2Ry0Ak" id="ofcHXg4JVR" role="2Ry0An">
                  <property role="2Ry0Am" value="Applications" />
                  <node concept="2Ry0Ak" id="ofcHXg4JVS" role="2Ry0An">
                    <property role="2Ry0Am" value="MPS 2018.2.app" />
                    <node concept="2Ry0Ak" id="6zy3YvEuC93" role="2Ry0An">
                      <property role="2Ry0Am" value="Contents" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2sgV4H" id="ofcHXg4JVU" role="1l3spa">
      <ref role="1l3spb" to="ffeo:3IKDaVZmzS6" resolve="mps" />
      <node concept="398BVA" id="ofcHXg4JVV" role="2JcizS">
        <ref role="398BVh" node="ofcHXg4JVL" resolve="mps_home" />
      </node>
    </node>
    <node concept="2sgV4H" id="ofcHXg4JVW" role="1l3spa">
      <ref role="1l3spb" to="lv7u:2$xY$aEDy6N" resolve="no.uio.mLab" />
    </node>
    <node concept="2sgV4H" id="2$xY$aEDywJ" role="1l3spa">
      <ref role="1l3spb" to="al5i:3AVJcIMlF8l" resolve="com.mbeddr.platform" />
      <node concept="398BVA" id="2$xY$aEDyzC" role="2JcizS">
        <ref role="398BVh" node="ofcHXg4JVL" resolve="mps_home" />
        <node concept="2Ry0Ak" id="5NEhRcN3UJB" role="iGT6I">
          <property role="2Ry0Am" value="plugins" />
        </node>
      </node>
    </node>
    <node concept="1l3spV" id="ofcHXg4JVX" role="1l3spN">
      <node concept="398223" id="ofcHXg4JVY" role="39821P">
        <node concept="3_J27D" id="ofcHXg4JVZ" role="Nbhlr">
          <node concept="3Mxwew" id="ofcHXg4JW0" role="3MwsjC">
            <property role="3MwjfP" value="no.furst.mLab" />
          </node>
        </node>
        <node concept="398223" id="ofcHXg4JW1" role="39821P">
          <node concept="398223" id="ofcHXg4JW2" role="39821P">
            <node concept="m_q07" id="ofcHXg4JW3" role="39821P">
              <ref role="m_q06" node="ofcHXg4JWt" resolve="no.furst.mLab.bundle" />
            </node>
            <node concept="3_J27D" id="ofcHXg4JW4" role="Nbhlr">
              <node concept="3Mxwew" id="ofcHXg4JW5" role="3MwsjC">
                <property role="3MwjfP" value="META-INF" />
              </node>
            </node>
          </node>
          <node concept="3_J27D" id="ofcHXg4JW6" role="Nbhlr">
            <node concept="3Mxwew" id="ofcHXg4JW7" role="3MwsjC">
              <property role="3MwjfP" value="bundle" />
            </node>
          </node>
          <node concept="398223" id="ofcHXg4JW8" role="39821P">
            <node concept="L2wRC" id="ofcHXg4JW9" role="39821P">
              <ref role="L2wRA" node="ofcHXg4JWC" resolve="no.furst.mLab.entities" />
            </node>
            <node concept="L2wRC" id="ofcHXg4JWa" role="39821P">
              <ref role="L2wRA" node="ofcHXg4JWI" resolve="no.furst.mLab.entities.runtime" />
            </node>
            <node concept="L2wRC" id="6khVixy0oMn" role="39821P">
              <ref role="L2wRA" node="6khVixy0oBm" resolve="no.furst.mLab.entities.translations" />
            </node>
            <node concept="L2wRC" id="ofcHXg4JWb" role="39821P">
              <ref role="L2wRA" node="ofcHXg4JWO" resolve="no.furst.mLab.decisions.tasks.autocommentation" />
            </node>
            <node concept="L2wRC" id="ofcHXg4JWc" role="39821P">
              <ref role="L2wRA" node="ofcHXg4JXd" resolve="no.furst.mLab.decisions.tasks.autocommentation.runtime" />
            </node>
            <node concept="L2wRC" id="ofcHXg4JWd" role="39821P">
              <ref role="L2wRA" node="ofcHXg4JXp" resolve="no.furst.mLab.decisions.tasks.autocommentation.translations" />
            </node>
            <node concept="L2wRC" id="6_a_4w5ojxj" role="39821P">
              <ref role="L2wRA" node="6_a_4w5ojOm" resolve="no.furst.mLab.decisions.references" />
            </node>
            <node concept="L2wRC" id="6_a_4w5ojVB" role="39821P">
              <ref role="L2wRA" node="6_a_4w5ojzf" resolve="no.furst.mLab.decisions.references.runtime" />
            </node>
            <node concept="L2wRC" id="6_a_4w5ojYP" role="39821P">
              <ref role="L2wRA" node="6_a_4w5ojF4" resolve="no.furst.mLab.decisions.references.translations" />
            </node>
            <node concept="L2wRC" id="5jVYnMGXNhO" role="39821P">
              <ref role="L2wRA" node="5jVYnMGXMGZ" resolve="no.furst.mLab.devkits.autocommentation" />
            </node>
            <node concept="3_J27D" id="ofcHXg4JWe" role="Nbhlr">
              <node concept="3Mxwew" id="ofcHXg4JWf" role="3MwsjC">
                <property role="3MwjfP" value="languages" />
              </node>
            </node>
          </node>
        </node>
        <node concept="398223" id="ofcHXg4JWl" role="39821P">
          <node concept="3_J27D" id="ofcHXg4JWm" role="Nbhlr">
            <node concept="3Mxwew" id="ofcHXg4JWn" role="3MwsjC">
              <property role="3MwjfP" value="engine" />
            </node>
          </node>
          <node concept="3981dx" id="ofcHXg4JWo" role="39821P">
            <node concept="3_J27D" id="ofcHXg4JWp" role="Nbhlr">
              <node concept="3Mxwey" id="ofcHXg4JWq" role="3MwsjC">
                <ref role="3Mxwex" node="ofcHXg4JVF" resolve="build.number" />
              </node>
              <node concept="3Mxwew" id="ofcHXg4JWr" role="3MwsjC">
                <property role="3MwjfP" value=".jar" />
              </node>
            </node>
            <node concept="Saw0i" id="ofcHXg4JWs" role="39821P">
              <ref role="Saw0g" node="ofcHXg4JXd" resolve="no.furst.mLab.decisions.tasks.autocommentation.runtime" />
            </node>
            <node concept="Saw0i" id="5_LESNW4ea$" role="39821P">
              <ref role="Saw0g" node="6_a_4w5ojzf" resolve="no.furst.mLab.decisions.references.runtime" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="m$_wf" id="ofcHXg4JWt" role="3989C9">
      <property role="m$_wk" value="no.furst.mLab.bundle" />
      <node concept="3_J27D" id="ofcHXg4JWu" role="m$_yQ">
        <node concept="3Mxwew" id="ofcHXg4JWv" role="3MwsjC">
          <property role="3MwjfP" value="no.furst.mLab.bundle" />
        </node>
      </node>
      <node concept="3_J27D" id="ofcHXg4JWw" role="m$_w8">
        <node concept="3Mxwew" id="ofcHXg4JWx" role="3MwsjC">
          <property role="3MwjfP" value="1.0" />
        </node>
      </node>
      <node concept="m$f5U" id="ofcHXg4JWy" role="m$_yh">
        <ref role="m$f5T" node="ofcHXg4JWB" resolve="no.furst.mLab" />
      </node>
      <node concept="m$_yC" id="ofcHXg4JWz" role="m$_yJ">
        <ref role="m$_y1" to="ffeo:4k71ibbKLe8" resolve="jetbrains.mps.core" />
      </node>
      <node concept="m$_yC" id="6zy3YvEuC9x" role="m$_yJ">
        <ref role="m$_y1" to="lv7u:2$xY$aEDy9X" resolve="no.uio.mLab.bundle" />
      </node>
      <node concept="3_J27D" id="ofcHXg4JW_" role="m_cZH">
        <node concept="3Mxwew" id="ofcHXg4JWA" role="3MwsjC">
          <property role="3MwjfP" value="no.furst.mLab.bundle" />
        </node>
      </node>
    </node>
    <node concept="2G$12M" id="ofcHXg4JWB" role="3989C9">
      <property role="TrG5h" value="no.furst.mLab" />
      <node concept="1E1JtA" id="ofcHXg4JWI" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.furst.mLab.entities.runtime" />
        <property role="3LESm3" value="0988caba-dfaa-4725-bb14-8b2e0238bda0" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="ofcHXg4JWJ" role="3LF7KH">
          <node concept="2Ry0Ak" id="ofcHXg4JWK" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="ofcHXg4JWL" role="2Ry0An">
              <property role="2Ry0Am" value="no.furst.mLab.entities" />
              <node concept="2Ry0Ak" id="ofcHXg4JWM" role="2Ry0An">
                <property role="2Ry0Am" value="runtime" />
                <node concept="2Ry0Ak" id="ofcHXg4JWN" role="2Ry0An">
                  <property role="2Ry0Am" value="no.furst.mLab.entities.runtime.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="6khVixy0o$6" role="3bR37C">
          <node concept="3bR9La" id="6khVixy0o$7" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="lv7u:ofcHXfYTWj" resolve="no.uio.mLab.shared.translations" />
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="6khVixy0oBm" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.furst.mLab.entities.translations" />
        <property role="3LESm3" value="44062e09-808c-42d1-a1f9-aeecbffb07e1" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="6khVixy0oBn" role="3LF7KH">
          <node concept="2Ry0Ak" id="6khVixy0oBo" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="6khVixy0oBp" role="2Ry0An">
              <property role="2Ry0Am" value="no.furst.mLab.entities" />
              <node concept="2Ry0Ak" id="6khVixy0oBq" role="2Ry0An">
                <property role="2Ry0Am" value="no.furst.mLab.entities.translations" />
                <node concept="2Ry0Ak" id="6khVixy0oFh" role="2Ry0An">
                  <property role="2Ry0Am" value="no.furst.mLab.entities.translations.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="6khVixy0oBs" role="3bR37C">
          <node concept="3bR9La" id="6khVixy0oBt" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="lv7u:ofcHXfYTWj" resolve="no.uio.mLab.shared.translations" />
          </node>
        </node>
        <node concept="1SiIV0" id="6khVixy0oGq" role="3bR37C">
          <node concept="3bR9La" id="6khVixy0oGr" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="ffeo:mXGwHwhVPj" resolve="JDK" />
          </node>
        </node>
      </node>
      <node concept="1E1JtD" id="ofcHXg4JWC" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.furst.mLab.entities" />
        <property role="3LESm3" value="bdbd2c14-a335-48a1-809b-6b32de6d4748" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="ofcHXg4JWD" role="3LF7KH">
          <node concept="2Ry0Ak" id="ofcHXg4JWE" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="ofcHXg4JWF" role="2Ry0An">
              <property role="2Ry0Am" value="no.furst.mLab.entities" />
              <node concept="2Ry0Ak" id="ofcHXg4JWG" role="2Ry0An">
                <property role="2Ry0Am" value="no.furst.mLab.entities.mpl" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1E0d5M" id="ofcHXg4JWH" role="1E1XAP">
          <ref role="1E0d5P" node="ofcHXg4JWI" resolve="no.furst.mLab.entities.runtime" />
        </node>
        <node concept="1SiIV0" id="2TpFF5_375q" role="3bR37C">
          <node concept="1Busua" id="2TpFF5_375r" role="1SiIV1">
            <ref role="1Busuk" to="lv7u:4QUW3ee1aEc" resolve="no.uio.mLab.entities.core" />
          </node>
        </node>
        <node concept="1SiIV0" id="6khVixy0o$d" role="3bR37C">
          <node concept="3bR9La" id="6khVixy0o$e" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="lv7u:2$xY$aEDy9q" resolve="no.uio.mLab.shared" />
          </node>
        </node>
        <node concept="1E0d5M" id="6khVixy0oGx" role="1E1XAP">
          <ref role="1E0d5P" node="6khVixy0oBm" resolve="no.furst.mLab.entities.translations" />
        </node>
        <node concept="1SiIV0" id="6khVixy0oIQ" role="3bR37C">
          <node concept="3bR9La" id="6khVixy0oIR" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="6khVixy0oBm" resolve="no.furst.mLab.entities.translations" />
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="6_a_4w5ojzf" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.furst.mLab.decisions.references.runtime" />
        <property role="3LESm3" value="ec5992d0-1694-4cb6-bd65-ec4931518cf6" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="6_a_4w5ojzi" role="3LF7KH">
          <node concept="2Ry0Ak" id="6_a_4w5oj_7" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="6_a_4w5ojA4" role="2Ry0An">
              <property role="2Ry0Am" value="no.furst.mLab.decisions.references" />
              <node concept="2Ry0Ak" id="6_a_4w5ojB1" role="2Ry0An">
                <property role="2Ry0Am" value="runtime" />
                <node concept="2Ry0Ak" id="6khVixym0F4" role="2Ry0An">
                  <property role="2Ry0Am" value="no.furst.mLab.decisions.references.runtime.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="6_a_4w5ojF4" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.furst.mLab.decisions.references.translations" />
        <property role="3LESm3" value="cc4bc78c-7acc-4821-acb0-2dd30729c35b" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="6_a_4w5ojF7" role="3LF7KH">
          <node concept="2Ry0Ak" id="6_a_4w5ojIa" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="6_a_4w5ojIK" role="2Ry0An">
              <property role="2Ry0Am" value="no.furst.mLab.decisions.references" />
              <node concept="2Ry0Ak" id="6_a_4w5ojJR" role="2Ry0An">
                <property role="2Ry0Am" value="no.furst.mLab.decisions.references.translations" />
                <node concept="2Ry0Ak" id="6khVixym0Hy" role="2Ry0An">
                  <property role="2Ry0Am" value="no.furst.mLab.decisions.references.translations.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="1E1JtD" id="6_a_4w5ojOm" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.furst.mLab.decisions.references" />
        <property role="3LESm3" value="8fc9d2a5-9169-4fbe-a02d-1c7f83e86b8e" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="6_a_4w5ojOp" role="3LF7KH">
          <node concept="2Ry0Ak" id="6_a_4w5ojQE" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="6_a_4w5ojRl" role="2Ry0An">
              <property role="2Ry0Am" value="no.furst.mLab.decisions.references" />
              <node concept="2Ry0Ak" id="6khVixym0Jc" role="2Ry0An">
                <property role="2Ry0Am" value="no.furst.mLab.decisions.references.mpl" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="6_a_4w5ojTx" role="3bR37C">
          <node concept="3bR9La" id="6_a_4w5ojTy" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="lv7u:2$xY$aEDy7b" resolve="no.uio.mLab.entities.biochemistry" />
          </node>
        </node>
        <node concept="1E0d5M" id="6_a_4w5ojTz" role="1E1XAP">
          <ref role="1E0d5P" node="6_a_4w5ojzf" resolve="no.furst.mLab.decisions.references.runtime" />
        </node>
        <node concept="1yeLz9" id="6_a_4w5ojTA" role="1TViLv">
          <property role="TrG5h" value="no.furst.mLab.decisions.references#01" />
          <property role="3LESm3" value="20de1f2f-0705-41a9-845c-1d805f065338" />
          <property role="2GAjPV" value="false" />
        </node>
        <node concept="1SiIV0" id="6khVixym0Kq" role="3bR37C">
          <node concept="3bR9La" id="6khVixym0Kr" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="ofcHXg4JWC" resolve="no.furst.mLab.entities" />
          </node>
        </node>
        <node concept="1SiIV0" id="4B5aqq2kVQK" role="3bR37C">
          <node concept="3bR9La" id="4B5aqq2kVQL" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="lv7u:2$xY$aEDy9q" resolve="no.uio.mLab.shared" />
          </node>
        </node>
        <node concept="1SiIV0" id="4B5aqq315Ad" role="3bR37C">
          <node concept="3bR9La" id="4B5aqq315Ae" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="lv7u:2$xY$aEDy7U" resolve="no.uio.mLab.decisions.core" />
          </node>
        </node>
        <node concept="1SiIV0" id="4B5aqq315Ah" role="3bR37C">
          <node concept="1Busua" id="4B5aqq315Ai" role="1SiIV1">
            <ref role="1Busuk" to="lv7u:4QUW3edwKrx" resolve="no.uio.mLab.decisions.references.laboratoryTest" />
          </node>
        </node>
        <node concept="1SiIV0" id="5uQLXCtv3$k" role="3bR37C">
          <node concept="3bR9La" id="5uQLXCtv3$l" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="ffeo:mXGwHwhVPj" resolve="JDK" />
          </node>
        </node>
        <node concept="1SiIV0" id="5uQLXCtv3$m" role="3bR37C">
          <node concept="3bR9La" id="5uQLXCtv3$n" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="lv7u:4QUW3edwLYq" resolve="no.uio.mLab.decisions.references.biochemistry" />
          </node>
        </node>
        <node concept="1SiIV0" id="2FjKBCQxZau" role="3bR37C">
          <node concept="3bR9La" id="2FjKBCQxZav" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="lv7u:4QUW3ee1aEc" resolve="no.uio.mLab.entities.core" />
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="ofcHXg4JXd" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.furst.mLab.decisions.tasks.autocommentation.runtime" />
        <property role="3LESm3" value="28fb07c3-2fbc-49a8-8c0b-84e5b85b0095" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="ofcHXg4JXe" role="3LF7KH">
          <node concept="2Ry0Ak" id="ofcHXg4JXf" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="ofcHXg4JXg" role="2Ry0An">
              <property role="2Ry0Am" value="no.furst.mLab.decisions.tasks.autocommentation" />
              <node concept="2Ry0Ak" id="ofcHXg4JXh" role="2Ry0An">
                <property role="2Ry0Am" value="runtime" />
                <node concept="2Ry0Ak" id="5_LESNW4dH1" role="2Ry0An">
                  <property role="2Ry0Am" value="no.furst.mLab.decisions.tasks.autocommentation.runtime.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="ofcHXg4JXj" role="3bR37C">
          <node concept="3bR9La" id="ofcHXg4JXk" role="1SiIV1">
            <property role="3bR36h" value="true" />
            <ref role="3bR37D" to="lv7u:2$xY$aEDy7O" resolve="no.uio.mLab.decisions.core.runtime" />
          </node>
        </node>
        <node concept="1SiIV0" id="4QUW3ee36bT" role="3bR37C">
          <node concept="3bR9La" id="4QUW3ee36bU" role="1SiIV1">
            <property role="3bR36h" value="true" />
            <ref role="3bR37D" to="lv7u:2$xY$aEDy7H" resolve="no.uio.mLab.decisions.tasks.autocommentation.runtime" />
          </node>
        </node>
        <node concept="1SiIV0" id="q8DvPRLvxR" role="3bR37C">
          <node concept="3bR9La" id="q8DvPRLvxS" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="ffeo:mXGwHwhVPj" resolve="JDK" />
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="ofcHXg4JXp" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.furst.mLab.decisions.tasks.autocommentation.translations" />
        <property role="3LESm3" value="30a9e9ae-4d79-4c13-8de2-2b78d91cbfbd" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="ofcHXg4JXq" role="3LF7KH">
          <node concept="2Ry0Ak" id="ofcHXg4JXr" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="ofcHXg4JXs" role="2Ry0An">
              <property role="2Ry0Am" value="no.furst.mLab.decisions.tasks.autocommentation" />
              <node concept="2Ry0Ak" id="ofcHXg4JXt" role="2Ry0An">
                <property role="2Ry0Am" value="no.furst.mLab.decisions.tasks.autocommentation.translations" />
                <node concept="2Ry0Ak" id="5_LESNW4dIn" role="2Ry0An">
                  <property role="2Ry0Am" value="no.furst.mLab.decisions.tasks.autocommentation.translations.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="ofcHXg4JXv" role="3bR37C">
          <node concept="3bR9La" id="ofcHXg4JXw" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="ffeo:mXGwHwhVPj" resolve="JDK" />
          </node>
        </node>
        <node concept="1SiIV0" id="ofcHXg4JXx" role="3bR37C">
          <node concept="3bR9La" id="ofcHXg4JXy" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="lv7u:ofcHXfYTWj" resolve="no.uio.mLab.shared.translations" />
          </node>
        </node>
      </node>
      <node concept="1E1JtD" id="ofcHXg4JWO" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.furst.mLab.decisions.tasks.autocommentation" />
        <property role="3LESm3" value="598e47df-5b3d-4e8c-94a1-69f953d4de82" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="ofcHXg4JWP" role="3LF7KH">
          <node concept="2Ry0Ak" id="ofcHXg4JWQ" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="ofcHXg4JWR" role="2Ry0An">
              <property role="2Ry0Am" value="no.furst.mLab.decisions.tasks.autocommentation" />
              <node concept="2Ry0Ak" id="5_LESNW4dJh" role="2Ry0An">
                <property role="2Ry0Am" value="no.furst.mLab.decisions.tasks.autocommentation.mpl" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1yeLz9" id="ofcHXg4JWT" role="1TViLv">
          <property role="TrG5h" value="no.furst.mLab.decisions.tasks.autocommentation#01" />
          <property role="3LESm3" value="6d9b02c5-e37f-497e-b022-7bab6cbf7b50" />
          <property role="2GAjPV" value="false" />
        </node>
        <node concept="1E0d5M" id="ofcHXg4K8K" role="1E1XAP">
          <ref role="1E0d5P" node="ofcHXg4JXd" resolve="no.furst.mLab.decisions.tasks.autocommentation.runtime" />
        </node>
        <node concept="1SiIV0" id="4QUW3ee36c5" role="3bR37C">
          <node concept="1Busua" id="4QUW3ee36c6" role="1SiIV1">
            <ref role="1Busuk" to="lv7u:2$xY$aEDy9e" resolve="no.uio.mLab.decisions.tasks.autocommentation" />
          </node>
        </node>
        <node concept="1SiIV0" id="q8DvPRLoRx" role="3bR37C">
          <node concept="3bR9La" id="q8DvPRLoRy" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="lv7u:2$xY$aEDy9q" resolve="no.uio.mLab.shared" />
          </node>
        </node>
        <node concept="1SiIV0" id="5_LESNW4dSw" role="3bR37C">
          <node concept="3bR9La" id="5_LESNW4dSx" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="ofcHXg4JXp" resolve="no.furst.mLab.decisions.tasks.autocommentation.translations" />
          </node>
        </node>
        <node concept="1SiIV0" id="4B5aqq2kVR5" role="3bR37C">
          <node concept="3bR9La" id="4B5aqq2kVR6" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="6_a_4w5ojOm" resolve="no.furst.mLab.decisions.references" />
          </node>
        </node>
        <node concept="1SiIV0" id="4B5aqq2kVR7" role="3bR37C">
          <node concept="3bR9La" id="4B5aqq2kVR8" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="ofcHXg4JWO" resolve="no.furst.mLab.decisions.tasks.autocommentation" />
          </node>
        </node>
        <node concept="1SiIV0" id="4B5aqq2kVR9" role="3bR37C">
          <node concept="3bR9La" id="4B5aqq2kVRa" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="lv7u:2$xY$aEDy7U" resolve="no.uio.mLab.decisions.core" />
          </node>
        </node>
        <node concept="1SiIV0" id="5uQLXCtv3$F" role="3bR37C">
          <node concept="3bR9La" id="5uQLXCtv3$G" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="lv7u:ofcHXfO28W" resolve="no.uio.mLab.decisions.core.utils" />
          </node>
        </node>
        <node concept="1SiIV0" id="2FjKBCQxZaN" role="3bR37C">
          <node concept="3bR9La" id="2FjKBCQxZaO" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="ffeo:mXGwHwhVPj" resolve="JDK" />
          </node>
        </node>
      </node>
      <node concept="3LEwk6" id="5jVYnMGXMGZ" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.furst.mLab.devkits.autocommentation" />
        <property role="3LESm3" value="da4aea3e-0607-4fcd-b168-a3e320d4476f" />
        <node concept="55IIr" id="5jVYnMGXMH2" role="3LF7KH">
          <node concept="2Ry0Ak" id="5jVYnMGXMNN" role="iGT6I">
            <property role="2Ry0Am" value="devkits" />
            <node concept="2Ry0Ak" id="5jVYnMGXMP1" role="2Ry0An">
              <property role="2Ry0Am" value="no.furst.mLab.devkits.autocommentation" />
              <node concept="2Ry0Ak" id="5jVYnMGXMRm" role="2Ry0An">
                <property role="2Ry0Am" value="no.furst.mLab.devkits.autocommentation.devkit" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3LEz8M" id="5jVYnMGXMTB" role="3LEz9a">
          <ref role="3LEz8N" to="lv7u:5jVYnMGX6wb" resolve="no.uio.mLab.devkits.autocommentation" />
        </node>
        <node concept="3LEDTy" id="5jVYnMGXMTC" role="3LEDUa">
          <ref role="3LEDTV" node="6_a_4w5ojOm" resolve="no.furst.mLab.decisions.references" />
        </node>
        <node concept="3LEDTy" id="5jVYnMGXMTD" role="3LEDUa">
          <ref role="3LEDTV" node="ofcHXg4JWO" resolve="no.furst.mLab.decisions.tasks.autocommentation" />
        </node>
      </node>
    </node>
  </node>
</model>

