<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:e36e7a5f-73d3-4888-aa7d-e5b07142aed6(no.furst.mLab.code)">
  <persistence version="9" />
  <languages>
    <use id="0cf935df-4699-4e9c-a132-fa109541cba3" name="jetbrains.mps.build.mps" version="5" />
    <use id="798100da-4f0a-421a-b991-71f8c50ce5d2" name="jetbrains.mps.build" version="0" />
  </languages>
  <imports>
    <import index="ffeo" ref="r:874d959d-e3b4-4d04-b931-ca849af130dd(jetbrains.mps.ide.build)" />
    <import index="lv7u" ref="r:7c3361f6-3c2c-4cdf-8d3e-794e424e582d(no.uio.mLab.build)" />
    <import index="4owg" ref="r:57fee833-4ddf-489d-93f3-21c9e2e96777(no.furst.mLab.build)" />
    <import index="al5i" ref="r:742f344d-4dc4-4862-992c-4bc94b094870(com.mbeddr.mpsutil.dev.build)" />
  </imports>
  <registry>
    <language id="798100da-4f0a-421a-b991-71f8c50ce5d2" name="jetbrains.mps.build">
      <concept id="5481553824944787378" name="jetbrains.mps.build.structure.BuildSourceProjectRelativePath" flags="ng" index="55IIr" />
      <concept id="2755237150521975431" name="jetbrains.mps.build.structure.BuildVariableMacroInitWithString" flags="ng" index="aVJcg">
        <child id="2755237150521975437" name="value" index="aVJcq" />
      </concept>
      <concept id="244868996532454372" name="jetbrains.mps.build.structure.BuildVariableMacroInitWithDate" flags="ng" index="hHN3E">
        <property id="244868996532454384" name="pattern" index="hHN3Y" />
      </concept>
      <concept id="7321017245476976379" name="jetbrains.mps.build.structure.BuildRelativePath" flags="ng" index="iG8Mu">
        <child id="7321017245477039051" name="compositePart" index="iGT6I" />
      </concept>
      <concept id="3767587139141066978" name="jetbrains.mps.build.structure.BuildVariableMacro" flags="ng" index="2kB4xC">
        <child id="2755237150521975432" name="initialValue" index="aVJcv" />
      </concept>
      <concept id="4993211115183325728" name="jetbrains.mps.build.structure.BuildProjectDependency" flags="ng" index="2sgV4H">
        <reference id="5617550519002745380" name="script" index="1l3spb" />
        <child id="4129895186893471026" name="artifacts" index="2JcizS" />
      </concept>
      <concept id="4380385936562003279" name="jetbrains.mps.build.structure.BuildString" flags="ng" index="NbPM2">
        <child id="4903714810883783243" name="parts" index="3MwsjC" />
      </concept>
      <concept id="8618885170173601777" name="jetbrains.mps.build.structure.BuildCompositePath" flags="nn" index="2Ry0Ak">
        <property id="8618885170173601779" name="head" index="2Ry0Am" />
        <child id="8618885170173601778" name="tail" index="2Ry0An" />
      </concept>
      <concept id="2591537044435828004" name="jetbrains.mps.build.structure.BuildLayout_CompileOutputOf" flags="ng" index="Saw0i">
        <reference id="2591537044435828006" name="module" index="Saw0g" />
      </concept>
      <concept id="6647099934206700647" name="jetbrains.mps.build.structure.BuildJavaPlugin" flags="ng" index="10PD9b" />
      <concept id="7389400916848050074" name="jetbrains.mps.build.structure.BuildLayout_Jar" flags="ng" index="3981dx" />
      <concept id="7389400916848050060" name="jetbrains.mps.build.structure.BuildLayout_NamedContainer" flags="ng" index="3981dR">
        <child id="4380385936562148502" name="containerName" index="Nbhlr" />
      </concept>
      <concept id="7389400916848136194" name="jetbrains.mps.build.structure.BuildFolderMacro" flags="ng" index="398rNT">
        <child id="7389400916848144618" name="defaultPath" index="398pKh" />
      </concept>
      <concept id="7389400916848153117" name="jetbrains.mps.build.structure.BuildSourceMacroRelativePath" flags="ng" index="398BVA">
        <reference id="7389400916848153130" name="macro" index="398BVh" />
      </concept>
      <concept id="5617550519002745364" name="jetbrains.mps.build.structure.BuildLayout" flags="ng" index="1l3spV" />
      <concept id="5617550519002745363" name="jetbrains.mps.build.structure.BuildProject" flags="ng" index="1l3spW">
        <property id="4915877860348071612" name="fileName" index="turDy" />
        <property id="5204048710541015587" name="internalBaseDirectory" index="2DA0ip" />
        <child id="6647099934206700656" name="plugins" index="10PD9s" />
        <child id="7389400916848080626" name="parts" index="3989C9" />
        <child id="5617550519002745381" name="dependencies" index="1l3spa" />
        <child id="5617550519002745378" name="macros" index="1l3spd" />
        <child id="5617550519002745372" name="layout" index="1l3spN" />
      </concept>
      <concept id="4701820937132344003" name="jetbrains.mps.build.structure.BuildLayout_Container" flags="ng" index="1y1bJS">
        <child id="7389400916848037006" name="children" index="39821P" />
      </concept>
      <concept id="841011766566059607" name="jetbrains.mps.build.structure.BuildStringNotEmpty" flags="ng" index="3_J27D" />
      <concept id="4903714810883702019" name="jetbrains.mps.build.structure.BuildTextStringPart" flags="ng" index="3Mxwew">
        <property id="4903714810883755350" name="text" index="3MwjfP" />
      </concept>
      <concept id="4903714810883702017" name="jetbrains.mps.build.structure.BuildVarRefStringPart" flags="ng" index="3Mxwey">
        <reference id="4903714810883702018" name="macro" index="3Mxwex" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="0cf935df-4699-4e9c-a132-fa109541cba3" name="jetbrains.mps.build.mps">
      <concept id="1500819558095907805" name="jetbrains.mps.build.mps.structure.BuildMps_Group" flags="ng" index="2G$12M">
        <child id="1500819558095907806" name="modules" index="2G$12L" />
      </concept>
      <concept id="868032131020265945" name="jetbrains.mps.build.mps.structure.BuildMPSPlugin" flags="ng" index="3b7kt6" />
      <concept id="5253498789149381388" name="jetbrains.mps.build.mps.structure.BuildMps_Module" flags="ng" index="3bQrTs">
        <property id="1500819558096356884" name="doNotCompile" index="2GAjPV" />
      </concept>
      <concept id="3189788309731840247" name="jetbrains.mps.build.mps.structure.BuildMps_Solution" flags="ng" index="1E1JtA" />
      <concept id="322010710375871467" name="jetbrains.mps.build.mps.structure.BuildMps_AbstractModule" flags="ng" index="3LEN3z">
        <property id="8369506495128725901" name="compact" index="BnDLt" />
        <property id="322010710375892619" name="uuid" index="3LESm3" />
        <child id="322010710375956261" name="path" index="3LF7KH" />
      </concept>
    </language>
  </registry>
  <node concept="1l3spW" id="6oczKrNJPEt">
    <property role="TrG5h" value="no.furst.mLab.regelverk" />
    <property role="2DA0ip" value="../../" />
    <property role="turDy" value="generate_rulebase.xml" />
    <node concept="10PD9b" id="6oczKrNJPEu" role="10PD9s" />
    <node concept="3b7kt6" id="6oczKrNJPEv" role="10PD9s" />
    <node concept="2kB4xC" id="6oczKrNJPEZ" role="1l3spd">
      <property role="TrG5h" value="date" />
      <node concept="hHN3E" id="6oczKrNJPF0" role="aVJcv">
        <property role="hHN3Y" value="yyyyMMdd" />
      </node>
    </node>
    <node concept="2kB4xC" id="6oczKrNJPF1" role="1l3spd">
      <property role="TrG5h" value="build.number" />
      <node concept="aVJcg" id="6oczKrNJPF2" role="aVJcv">
        <node concept="NbPM2" id="6oczKrNJPF3" role="aVJcq">
          <node concept="3Mxwew" id="6oczKrNJPF4" role="3MwsjC">
            <property role="3MwjfP" value="FurstRegelverk-182.SNAPSHOT" />
          </node>
        </node>
      </node>
    </node>
    <node concept="398rNT" id="6oczKrNJPF5" role="1l3spd">
      <property role="TrG5h" value="mps_home" />
      <node concept="55IIr" id="6oczKrNJPF6" role="398pKh">
        <node concept="2Ry0Ak" id="6oczKrNJPF7" role="iGT6I">
          <property role="2Ry0Am" value=".." />
          <node concept="2Ry0Ak" id="6oczKrNJPF8" role="2Ry0An">
            <property role="2Ry0Am" value=".." />
            <node concept="2Ry0Ak" id="6oczKrNJPF9" role="2Ry0An">
              <property role="2Ry0Am" value=".." />
              <node concept="2Ry0Ak" id="6oczKrNJPFa" role="2Ry0An">
                <property role="2Ry0Am" value=".." />
                <node concept="2Ry0Ak" id="6oczKrNJPFb" role="2Ry0An">
                  <property role="2Ry0Am" value="Applications" />
                  <node concept="2Ry0Ak" id="6oczKrNJPFc" role="2Ry0An">
                    <property role="2Ry0Am" value="MPS 2018.2.app" />
                    <node concept="2Ry0Ak" id="6oczKrNJPFd" role="2Ry0An">
                      <property role="2Ry0Am" value="Contents" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2sgV4H" id="6oczKrNJPTM" role="1l3spa">
      <ref role="1l3spb" to="ffeo:3IKDaVZmzS6" resolve="mps" />
      <node concept="398BVA" id="6oczKrNJPWT" role="2JcizS">
        <ref role="398BVh" node="6oczKrNJPF5" resolve="mps_home" />
      </node>
    </node>
    <node concept="2sgV4H" id="5jVYnMGVtIu" role="1l3spa">
      <ref role="1l3spb" to="lv7u:2$xY$aEDy6N" resolve="no.uio.mLab" />
    </node>
    <node concept="2sgV4H" id="ofcHXgg$ER" role="1l3spa">
      <ref role="1l3spb" to="4owg:ofcHXg4JVC" resolve="no.furst.mLab" />
    </node>
    <node concept="2sgV4H" id="2$xY$aEDywJ" role="1l3spa">
      <ref role="1l3spb" to="al5i:3AVJcIMlF8l" resolve="com.mbeddr.platform" />
      <node concept="398BVA" id="2$xY$aEDyzC" role="2JcizS">
        <ref role="398BVh" node="6oczKrNJPF5" resolve="mps_home" />
        <node concept="2Ry0Ak" id="5NEhRcN3UJB" role="iGT6I">
          <property role="2Ry0Am" value="plugins" />
        </node>
      </node>
    </node>
    <node concept="1l3spV" id="6oczKrNJPFk" role="1l3spN">
      <node concept="3981dx" id="6oczKrNJPM2" role="39821P">
        <node concept="3_J27D" id="6oczKrNJPM3" role="Nbhlr">
          <node concept="3Mxwey" id="6oczKrNJPMD" role="3MwsjC">
            <ref role="3Mxwex" node="6oczKrNJPF1" resolve="build.number" />
          </node>
          <node concept="3Mxwew" id="6oczKrNJPNc" role="3MwsjC">
            <property role="3MwjfP" value=".jar" />
          </node>
        </node>
        <node concept="Saw0i" id="6oczKrNJPOf" role="39821P">
          <ref role="Saw0g" node="6oczKrNJPH8" resolve="no.furst.mLab.regelsystem" />
        </node>
      </node>
    </node>
    <node concept="2G$12M" id="6oczKrNJPH7" role="3989C9">
      <property role="TrG5h" value="no.furst.mLab.regelsystem" />
      <node concept="1E1JtA" id="6oczKrNJPH8" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.furst.mLab.regelsystem" />
        <property role="3LESm3" value="38a4235e-83dd-4249-b71b-fd73b55abc20" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="6oczKrNJPH9" role="3LF7KH">
          <node concept="2Ry0Ak" id="6oczKrNJPHa" role="iGT6I">
            <property role="2Ry0Am" value="solutions" />
            <node concept="2Ry0Ak" id="6oczKrNJPHb" role="2Ry0An">
              <property role="2Ry0Am" value="no.furst.mLab.regelsystem" />
              <node concept="2Ry0Ak" id="2$xY$aEUbVX" role="2Ry0An">
                <property role="2Ry0Am" value="no.furst.mLab.regelsystem.msd" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

