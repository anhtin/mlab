<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:b12b40b4-f631-4cde-91ae-1d91715bb193(no.furst.mLab.ide)">
  <persistence version="9" />
  <languages>
    <use id="0cf935df-4699-4e9c-a132-fa109541cba3" name="jetbrains.mps.build.mps" version="5" />
    <use id="798100da-4f0a-421a-b991-71f8c50ce5d2" name="jetbrains.mps.build" version="0" />
  </languages>
  <imports>
    <import index="ffeo" ref="r:874d959d-e3b4-4d04-b931-ca849af130dd(jetbrains.mps.ide.build)" />
    <import index="4owg" ref="r:57fee833-4ddf-489d-93f3-21c9e2e96777(no.furst.mLab.build)" />
    <import index="lv7u" ref="r:7c3361f6-3c2c-4cdf-8d3e-794e424e582d(no.uio.mLab.build)" />
    <import index="al5i" ref="r:742f344d-4dc4-4862-992c-4bc94b094870(com.mbeddr.mpsutil.dev.build)" />
    <import index="90a9" ref="r:fb24ac52-5985-4947-bba9-25be6fd32c1a(de.itemis.mps.extensions.build)" />
  </imports>
  <registry>
    <language id="798100da-4f0a-421a-b991-71f8c50ce5d2" name="jetbrains.mps.build">
      <concept id="5481553824944787378" name="jetbrains.mps.build.structure.BuildSourceProjectRelativePath" flags="ng" index="55IIr" />
      <concept id="9126048691955220717" name="jetbrains.mps.build.structure.BuildLayout_File" flags="ng" index="28jJK3">
        <property id="9126048691955221291" name="filemode" index="28jJZ5" />
        <child id="9126048691955220774" name="parameters" index="28jJR8" />
        <child id="9126048691955220762" name="path" index="28jJRO" />
      </concept>
      <concept id="2755237150521975431" name="jetbrains.mps.build.structure.BuildVariableMacroInitWithString" flags="ng" index="aVJcg">
        <child id="2755237150521975437" name="value" index="aVJcq" />
      </concept>
      <concept id="244868996532454372" name="jetbrains.mps.build.structure.BuildVariableMacroInitWithDate" flags="ng" index="hHN3E">
        <property id="244868996532454384" name="pattern" index="hHN3Y" />
      </concept>
      <concept id="7321017245476976379" name="jetbrains.mps.build.structure.BuildRelativePath" flags="ng" index="iG8Mu">
        <child id="7321017245477039051" name="compositePart" index="iGT6I" />
      </concept>
      <concept id="3767587139141066978" name="jetbrains.mps.build.structure.BuildVariableMacro" flags="ng" index="2kB4xC">
        <child id="2755237150521975432" name="initialValue" index="aVJcv" />
      </concept>
      <concept id="4993211115183325728" name="jetbrains.mps.build.structure.BuildProjectDependency" flags="ng" index="2sgV4H">
        <reference id="5617550519002745380" name="script" index="1l3spb" />
        <child id="4129895186893471026" name="artifacts" index="2JcizS" />
      </concept>
      <concept id="7801138212747054656" name="jetbrains.mps.build.structure.BuildLayout_Filemode" flags="ng" index="yKbIv">
        <property id="7801138212747054660" name="filemode" index="yKbIr" />
      </concept>
      <concept id="2750015747481074431" name="jetbrains.mps.build.structure.BuildLayout_Files" flags="ng" index="2HvfSZ">
        <child id="2750015747481074432" name="path" index="2HvfZ0" />
        <child id="2750015747481074433" name="parameters" index="2HvfZ1" />
      </concept>
      <concept id="4380385936562003279" name="jetbrains.mps.build.structure.BuildString" flags="ng" index="NbPM2">
        <child id="4903714810883783243" name="parts" index="3MwsjC" />
      </concept>
      <concept id="8618885170173601777" name="jetbrains.mps.build.structure.BuildCompositePath" flags="nn" index="2Ry0Ak">
        <property id="8618885170173601779" name="head" index="2Ry0Am" />
        <child id="8618885170173601778" name="tail" index="2Ry0An" />
      </concept>
      <concept id="6647099934206700647" name="jetbrains.mps.build.structure.BuildJavaPlugin" flags="ng" index="10PD9b" />
      <concept id="9184644532457106504" name="jetbrains.mps.build.structure.BuildLayout_CopyFilterReplaceRegex" flags="ng" index="1688n2">
        <property id="9184644532457106505" name="pattern" index="1688n3" />
        <property id="9184644532457106508" name="flags" index="1688n6" />
        <child id="9184644532457106506" name="value" index="1688n0" />
      </concept>
      <concept id="7389400916848050074" name="jetbrains.mps.build.structure.BuildLayout_Jar" flags="ng" index="3981dx" />
      <concept id="7389400916848050071" name="jetbrains.mps.build.structure.BuildLayout_Zip" flags="ng" index="3981dG" />
      <concept id="7389400916848050060" name="jetbrains.mps.build.structure.BuildLayout_NamedContainer" flags="ng" index="3981dR">
        <child id="4380385936562148502" name="containerName" index="Nbhlr" />
      </concept>
      <concept id="7389400916848036984" name="jetbrains.mps.build.structure.BuildLayout_Folder" flags="ng" index="398223" />
      <concept id="7389400916848136194" name="jetbrains.mps.build.structure.BuildFolderMacro" flags="ng" index="398rNT">
        <child id="7389400916848144618" name="defaultPath" index="398pKh" />
      </concept>
      <concept id="7389400916848153117" name="jetbrains.mps.build.structure.BuildSourceMacroRelativePath" flags="ng" index="398BVA">
        <reference id="7389400916848153130" name="macro" index="398BVh" />
      </concept>
      <concept id="4198392933254416812" name="jetbrains.mps.build.structure.BuildLayout_CopyFilterFixCRLF" flags="ng" index="3co7Ac">
        <property id="4198392933254416822" name="eol" index="3co7Am" />
        <property id="4198392933254551900" name="removeEOF" index="3cpA_W" />
      </concept>
      <concept id="5617550519002745364" name="jetbrains.mps.build.structure.BuildLayout" flags="ng" index="1l3spV" />
      <concept id="5617550519002745363" name="jetbrains.mps.build.structure.BuildProject" flags="ng" index="1l3spW">
        <property id="4915877860348071612" name="fileName" index="turDy" />
        <property id="5204048710541015587" name="internalBaseDirectory" index="2DA0ip" />
        <child id="6647099934206700656" name="plugins" index="10PD9s" />
        <child id="7389400916848080626" name="parts" index="3989C9" />
        <child id="5617550519002745381" name="dependencies" index="1l3spa" />
        <child id="5617550519002745378" name="macros" index="1l3spd" />
        <child id="5617550519002745372" name="layout" index="1l3spN" />
      </concept>
      <concept id="8577651205286814211" name="jetbrains.mps.build.structure.BuildLayout_Tar" flags="ng" index="1tmT9g">
        <property id="1979010778009209128" name="compression" index="AB_bT" />
      </concept>
      <concept id="4701820937132344003" name="jetbrains.mps.build.structure.BuildLayout_Container" flags="ng" index="1y1bJS">
        <child id="7389400916848037006" name="children" index="39821P" />
      </concept>
      <concept id="5610619299013057363" name="jetbrains.mps.build.structure.BuildLayout_ImportContent" flags="ng" index="3ygNvl">
        <reference id="5610619299013057365" name="target" index="3ygNvj" />
        <child id="6789562173791401562" name="selectors" index="1juEy9" />
      </concept>
      <concept id="7753544965996647428" name="jetbrains.mps.build.structure.BuildLayout_FilesOf" flags="ng" index="1zDrgl">
        <reference id="7753544965996647430" name="element" index="1zDrgn" />
      </concept>
      <concept id="841011766565753074" name="jetbrains.mps.build.structure.BuildLayout_Import" flags="ng" index="3_I8Xc">
        <reference id="841011766565753076" name="target" index="3_I8Xa" />
      </concept>
      <concept id="841011766566059607" name="jetbrains.mps.build.structure.BuildStringNotEmpty" flags="ng" index="3_J27D" />
      <concept id="5248329904288051111" name="jetbrains.mps.build.structure.BuildFileExcludeSelector" flags="ng" index="3LWZYq">
        <property id="5248329904288051112" name="pattern" index="3LWZYl" />
      </concept>
      <concept id="5248329904288051100" name="jetbrains.mps.build.structure.BuildFileIncludeSelector" flags="ng" index="3LWZYx">
        <property id="5248329904288051101" name="pattern" index="3LWZYw" />
      </concept>
      <concept id="4903714810883702019" name="jetbrains.mps.build.structure.BuildTextStringPart" flags="ng" index="3Mxwew">
        <property id="4903714810883755350" name="text" index="3MwjfP" />
      </concept>
      <concept id="4903714810883702017" name="jetbrains.mps.build.structure.BuildVarRefStringPart" flags="ng" index="3Mxwey">
        <reference id="4903714810883702018" name="macro" index="3Mxwex" />
      </concept>
      <concept id="202934866059043946" name="jetbrains.mps.build.structure.BuildLayout_EchoProperties" flags="ng" index="1TblL5">
        <child id="202934866059043948" name="fileName" index="1TblL3" />
        <child id="202934866059043962" name="entries" index="1TblLl" />
      </concept>
      <concept id="202934866059043959" name="jetbrains.mps.build.structure.BuildLayout_EchoPropertyEntry" flags="ng" index="1TblLo">
        <property id="202934866059043960" name="key" index="1TblLn" />
        <child id="202934866059043961" name="value" index="1TblLm" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
      <concept id="709746936026466394" name="jetbrains.mps.lang.core.structure.ChildAttribute" flags="ng" index="3VBwX9">
        <property id="709746936026609031" name="linkId" index="3V$3ak" />
        <property id="709746936026609029" name="linkRole" index="3V$3am" />
      </concept>
      <concept id="4452961908202556907" name="jetbrains.mps.lang.core.structure.BaseCommentAttribute" flags="ng" index="1X3_iC">
        <child id="3078666699043039389" name="commentedNode" index="8Wnug" />
      </concept>
    </language>
    <language id="0cf935df-4699-4e9c-a132-fa109541cba3" name="jetbrains.mps.build.mps">
      <concept id="6592112598314498932" name="jetbrains.mps.build.mps.structure.BuildMps_IdeaPlugin" flags="ng" index="m$_wf">
        <property id="6592112598314498927" name="id" index="m$_wk" />
        <child id="6592112598314498931" name="version" index="m$_w8" />
        <child id="6592112598314499028" name="dependencies" index="m$_yJ" />
        <child id="6592112598314499021" name="name" index="m$_yQ" />
        <child id="6592112598314855574" name="containerName" index="m_cZH" />
      </concept>
      <concept id="6592112598314499027" name="jetbrains.mps.build.mps.structure.BuildMps_IdeaPluginDependency" flags="ng" index="m$_yC">
        <reference id="6592112598314499066" name="target" index="m$_y1" />
      </concept>
      <concept id="868032131020265945" name="jetbrains.mps.build.mps.structure.BuildMPSPlugin" flags="ng" index="3b7kt6" />
      <concept id="5253498789149381388" name="jetbrains.mps.build.mps.structure.BuildMps_Module" flags="ng" index="3bQrTs">
        <property id="1500819558096356884" name="doNotCompile" index="2GAjPV" />
      </concept>
      <concept id="7753544965996377997" name="jetbrains.mps.build.mps.structure.BuildMps_Branding" flags="ng" index="1zClus">
        <property id="3497141547781541445" name="minor" index="2OjLBK" />
        <property id="3497141547781541444" name="major" index="2OjLBL" />
        <child id="6108265972537182997" name="aboutScreen" index="2EqU2s" />
        <child id="6108265972537182996" name="splashScreen" index="2EqU2t" />
        <child id="6108265972537229337" name="buildNumber" index="2EteIg" />
        <child id="6108265972537229339" name="icon16" index="2EteIi" />
        <child id="6108265972537229338" name="icon32" index="2EteIj" />
        <child id="6108265972537229340" name="icon32opaque" index="2EteIl" />
        <child id="6108265972537372847" name="shortName" index="2EtHGA" />
        <child id="6108265972537372848" name="fullName" index="2EtHGT" />
        <child id="8795525031433238889" name="textColor" index="HFo83" />
        <child id="3497141547781549827" name="codename" index="2OjNyQ" />
        <child id="1462305029084462472" name="buildDate" index="R$TG_" />
        <child id="772379520210716142" name="welcomeLogo" index="3vi$VU" />
      </concept>
      <concept id="3189788309731840247" name="jetbrains.mps.build.mps.structure.BuildMps_Solution" flags="ng" index="1E1JtA" />
      <concept id="322010710375871467" name="jetbrains.mps.build.mps.structure.BuildMps_AbstractModule" flags="ng" index="3LEN3z">
        <property id="8369506495128725901" name="compact" index="BnDLt" />
        <property id="322010710375892619" name="uuid" index="3LESm3" />
        <child id="322010710375956261" name="path" index="3LF7KH" />
      </concept>
    </language>
  </registry>
  <node concept="1l3spW" id="6oczKrNHpUb">
    <property role="TrG5h" value="no.furst.mLab.ide" />
    <property role="2DA0ip" value="../../" />
    <property role="turDy" value="build_furst_ide_general.xml" />
    <node concept="2sgV4H" id="6oczKrOm1Om" role="1l3spa">
      <ref role="1l3spb" to="ffeo:3IKDaVZmzS6" resolve="mps" />
      <node concept="398BVA" id="6oczKrOm8Pi" role="2JcizS">
        <ref role="398BVh" node="6oczKrNHpUk" resolve="mps_home" />
      </node>
    </node>
    <node concept="10PD9b" id="6oczKrNHpUc" role="10PD9s" />
    <node concept="3b7kt6" id="6oczKrNHpUd" role="10PD9s" />
    <node concept="1zClus" id="6oczKrNHpUq" role="3989C9">
      <property role="2OjLBK" value="0" />
      <property role="TrG5h" value="MPS" />
      <property role="2OjLBL" value="1" />
      <node concept="55IIr" id="6oczKrNHpUr" role="3vi$VU">
        <node concept="2Ry0Ak" id="6oczKrNHpUs" role="iGT6I">
          <property role="2Ry0Am" value="icons" />
          <node concept="2Ry0Ak" id="6oczKrNHpUt" role="2Ry0An">
            <property role="2Ry0Am" value="logo.png" />
          </node>
        </node>
      </node>
      <node concept="3_J27D" id="6oczKrNHpUu" role="2EteIg">
        <node concept="3Mxwey" id="6oczKrNHpUv" role="3MwsjC">
          <ref role="3Mxwex" node="6oczKrNHpUg" resolve="build.number" />
        </node>
      </node>
      <node concept="55IIr" id="6oczKrNHpUw" role="2EteIi">
        <node concept="2Ry0Ak" id="6oczKrNHpUx" role="iGT6I">
          <property role="2Ry0Am" value="icons" />
          <node concept="2Ry0Ak" id="6oczKrNHpUy" role="2Ry0An">
            <property role="2Ry0Am" value="MPS16.png" />
          </node>
        </node>
      </node>
      <node concept="3_J27D" id="6oczKrNHpUz" role="2EtHGA">
        <node concept="3Mxwew" id="6oczKrNHpU$" role="3MwsjC">
          <property role="3MwjfP" value="mLabFurst" />
        </node>
      </node>
      <node concept="3_J27D" id="6oczKrNHpU_" role="2EtHGT">
        <node concept="3Mxwew" id="6oczKrNHpUA" role="3MwsjC">
          <property role="3MwjfP" value="mLabFurst" />
        </node>
      </node>
      <node concept="NbPM2" id="6oczKrNHpUB" role="2OjNyQ">
        <node concept="3Mxwew" id="6oczKrNHpUC" role="3MwsjC">
          <property role="3MwjfP" value="mLabFurst" />
        </node>
      </node>
      <node concept="3_J27D" id="6oczKrNHpUD" role="HFo83">
        <node concept="3Mxwew" id="6oczKrNHpUE" role="3MwsjC">
          <property role="3MwjfP" value="002387" />
        </node>
      </node>
      <node concept="55IIr" id="6oczKrNHpUF" role="2EteIj">
        <node concept="2Ry0Ak" id="6oczKrNHpUG" role="iGT6I">
          <property role="2Ry0Am" value="icons" />
          <node concept="2Ry0Ak" id="6oczKrNHpUH" role="2Ry0An">
            <property role="2Ry0Am" value="MPS32.png" />
          </node>
        </node>
      </node>
      <node concept="3_J27D" id="6oczKrNHpUI" role="R$TG_">
        <node concept="3Mxwey" id="6oczKrNHpUJ" role="3MwsjC">
          <ref role="3Mxwex" node="6oczKrNHpUe" resolve="date" />
        </node>
      </node>
      <node concept="55IIr" id="6oczKrNHpUK" role="2EteIl">
        <node concept="2Ry0Ak" id="6oczKrNHpUL" role="iGT6I">
          <property role="2Ry0Am" value="icons" />
          <node concept="2Ry0Ak" id="6oczKrNHpUM" role="2Ry0An">
            <property role="2Ry0Am" value="MPS32.png" />
          </node>
        </node>
      </node>
      <node concept="55IIr" id="6oczKrNHpUN" role="2EqU2t">
        <node concept="2Ry0Ak" id="6oczKrNHpUO" role="iGT6I">
          <property role="2Ry0Am" value="icons" />
          <node concept="2Ry0Ak" id="6oczKrNHpUP" role="2Ry0An">
            <property role="2Ry0Am" value="splash.png" />
          </node>
        </node>
      </node>
      <node concept="55IIr" id="6oczKrNHpUQ" role="2EqU2s">
        <node concept="2Ry0Ak" id="6oczKrNHpUR" role="iGT6I">
          <property role="2Ry0Am" value="icons" />
          <node concept="2Ry0Ak" id="6oczKrNHpUS" role="2Ry0An">
            <property role="2Ry0Am" value="about.png" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2kB4xC" id="6oczKrNHpUe" role="1l3spd">
      <property role="TrG5h" value="date" />
      <node concept="hHN3E" id="6oczKrNHpUf" role="aVJcv">
        <property role="hHN3Y" value="yyyyMMdd" />
      </node>
    </node>
    <node concept="2kB4xC" id="6oczKrNHpUg" role="1l3spd">
      <property role="TrG5h" value="build.number" />
      <node concept="aVJcg" id="6oczKrNHpUh" role="aVJcv">
        <node concept="NbPM2" id="6oczKrNHpUi" role="aVJcq">
          <node concept="3Mxwew" id="6oczKrNHpUj" role="3MwsjC">
            <property role="3MwjfP" value="mLabFurst-182.SNAPSHOT" />
          </node>
        </node>
      </node>
    </node>
    <node concept="398rNT" id="6oczKrNHpUk" role="1l3spd">
      <property role="TrG5h" value="mps_home" />
      <node concept="55IIr" id="6oczKrNHqT7" role="398pKh">
        <node concept="2Ry0Ak" id="6oczKrNHqZk" role="iGT6I">
          <property role="2Ry0Am" value=".." />
          <node concept="2Ry0Ak" id="6oczKrNHrhP" role="2Ry0An">
            <property role="2Ry0Am" value=".." />
            <node concept="2Ry0Ak" id="6oczKrNHrhS" role="2Ry0An">
              <property role="2Ry0Am" value=".." />
              <node concept="2Ry0Ak" id="6oczKrNHrhV" role="2Ry0An">
                <property role="2Ry0Am" value=".." />
                <node concept="2Ry0Ak" id="6oczKrNHrl5" role="2Ry0An">
                  <property role="2Ry0Am" value="Applications" />
                  <node concept="2Ry0Ak" id="6oczKrNHrof" role="2Ry0An">
                    <property role="2Ry0Am" value="MPS 2018.2.app" />
                    <node concept="2Ry0Ak" id="6oczKrNHrrp" role="2Ry0An">
                      <property role="2Ry0Am" value="Contents" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2sgV4H" id="6oczKrNHpUl" role="1l3spa">
      <ref role="1l3spb" to="ffeo:1diLdO26mQ6" resolve="mpsStandalone" />
      <node concept="398BVA" id="6oczKrNHpUm" role="2JcizS">
        <ref role="398BVh" node="6oczKrNHpUk" resolve="mps_home" />
      </node>
    </node>
    <node concept="2sgV4H" id="6oczKrNHpUn" role="1l3spa">
      <ref role="1l3spb" to="ffeo:5rNMDvYzelV" resolve="mpsMakePlugin" />
      <node concept="398BVA" id="6oczKrNHpUo" role="2JcizS">
        <ref role="398BVh" node="6oczKrNHpUk" resolve="mps_home" />
        <node concept="2Ry0Ak" id="6oczKrNHpUp" role="iGT6I">
          <property role="2Ry0Am" value="plugins" />
        </node>
      </node>
    </node>
    <node concept="2sgV4H" id="7pRzn8SVbVF" role="1l3spa">
      <ref role="1l3spb" to="lv7u:2$xY$aEDy6N" resolve="no.uio.mLab" />
    </node>
    <node concept="2sgV4H" id="ofcHXgg$F5" role="1l3spa">
      <ref role="1l3spb" to="4owg:ofcHXg4JVC" resolve="no.furst.mLab" />
    </node>
    <node concept="2sgV4H" id="2$xY$aEDywJ" role="1l3spa">
      <ref role="1l3spb" to="al5i:3AVJcIMlF8l" resolve="com.mbeddr.platform" />
      <node concept="398BVA" id="2$xY$aEDyzC" role="2JcizS">
        <ref role="398BVh" node="6oczKrNHpUk" resolve="mps_home" />
        <node concept="2Ry0Ak" id="5NEhRcN3UJB" role="iGT6I">
          <property role="2Ry0Am" value="plugins" />
        </node>
      </node>
    </node>
    <node concept="1l3spV" id="6oczKrNHpYo" role="1l3spN">
      <node concept="3_I8Xc" id="6oczKrNHpYw" role="39821P">
        <ref role="3_I8Xa" to="ffeo:1diLdO26H79" resolve="languages" />
      </node>
      <node concept="3_I8Xc" id="6oczKrNHpYx" role="39821P">
        <ref role="3_I8Xa" to="ffeo:1aRUp2KiMC$" resolve="license" />
      </node>
      <node concept="398223" id="6oczKrNHpYy" role="39821P">
        <node concept="3_J27D" id="6oczKrNHpYz" role="Nbhlr">
          <node concept="3Mxwew" id="6oczKrNHpY$" role="3MwsjC">
            <property role="3MwjfP" value="bin" />
          </node>
        </node>
        <node concept="3ygNvl" id="6oczKrNHpY_" role="39821P">
          <ref role="3ygNvj" to="ffeo:3cxBkkDa4_O" resolve="bin" />
          <node concept="3LWZYx" id="6oczKrNHpYA" role="1juEy9">
            <property role="3LWZYw" value="log.xml" />
          </node>
          <node concept="3LWZYx" id="6oczKrNHpYB" role="1juEy9">
            <property role="3LWZYw" value="log4j.dtd" />
          </node>
        </node>
        <node concept="28jJK3" id="6oczKrNHpYC" role="39821P">
          <node concept="1688n2" id="6oczKrNHpYD" role="28jJR8">
            <property role="1688n6" value="g" />
            <property role="1688n3" value="\.MPS(\w+)" />
            <node concept="NbPM2" id="6oczKrNHpYE" role="1688n0">
              <node concept="3Mxwew" id="6oczKrNHpYF" role="3MwsjC">
                <property role="3MwjfP" value="\." />
              </node>
              <node concept="3Mxwey" id="6oczKrNHpYG" role="3MwsjC">
                <ref role="3Mxwex" node="6oczKrNHpUg" resolve="build.number" />
              </node>
            </node>
          </node>
          <node concept="398BVA" id="6oczKrNHpYs" role="28jJRO">
            <ref role="398BVh" node="6oczKrNHpUk" resolve="mps_home" />
            <node concept="2Ry0Ak" id="6oczKrNHpYt" role="iGT6I">
              <property role="2Ry0Am" value="bin" />
              <node concept="2Ry0Ak" id="6oczKrNHpYu" role="2Ry0An">
                <property role="2Ry0Am" value="idea.properties" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="398223" id="6oczKrNHpYH" role="39821P">
        <node concept="3_J27D" id="6oczKrNHpYI" role="Nbhlr">
          <node concept="3Mxwew" id="6oczKrNHpYJ" role="3MwsjC">
            <property role="3MwjfP" value="lib" />
          </node>
        </node>
        <node concept="3ygNvl" id="6oczKrNHpYK" role="39821P">
          <ref role="3ygNvj" to="ffeo:1diLdO26H7f" resolve="lib" />
          <node concept="3LWZYq" id="6oczKrNHpYL" role="1juEy9">
            <property role="3LWZYl" value="MPS-src.zip" />
          </node>
          <node concept="3LWZYq" id="6oczKrNHpYM" role="1juEy9">
            <property role="3LWZYl" value="branding.jar" />
          </node>
        </node>
        <node concept="3981dx" id="6oczKrNHpYN" role="39821P">
          <node concept="3_J27D" id="6oczKrNHpYO" role="Nbhlr">
            <node concept="3Mxwew" id="6oczKrNHpYP" role="3MwsjC">
              <property role="3MwjfP" value="branding.jar" />
            </node>
          </node>
          <node concept="1zDrgl" id="6oczKrNHpYQ" role="39821P">
            <ref role="1zDrgn" node="6oczKrNHpUq" resolve="mLabFurst 1.0" />
          </node>
        </node>
      </node>
      <node concept="398223" id="6oczKrNHpYR" role="39821P">
        <node concept="3_I8Xc" id="6oczKrNHpYS" role="39821P">
          <ref role="3_I8Xa" to="ffeo:1diLdO26H7T" resolve="cvsIntegration" />
        </node>
        <node concept="3_I8Xc" id="6oczKrNHpYT" role="39821P">
          <ref role="3_I8Xa" to="ffeo:3nGzrDEfcNJ" resolve="svn4idea" />
        </node>
        <node concept="3_I8Xc" id="6oczKrNHpYU" role="39821P">
          <ref role="3_I8Xa" to="ffeo:I6XuqH2zYV" resolve="git4idea" />
        </node>
        <node concept="3_I8Xc" id="6oczKrNHpYV" role="39821P">
          <ref role="3_I8Xa" to="ffeo:ymnOULBdbM" resolve="mps-core" />
        </node>
        <node concept="3_I8Xc" id="6oczKrOm1Up" role="39821P">
          <ref role="3_I8Xa" to="ffeo:5xhjlkpPhA8" resolve="http-support" />
        </node>
        <node concept="1X3_iC" id="7pRzn8STNVA" role="lGtFl">
          <property role="3V$3am" value="children" />
          <property role="3V$3ak" value="798100da-4f0a-421a-b991-71f8c50ce5d2/4701820937132344003/7389400916848037006" />
          <node concept="3ygNvl" id="VnHf4nzJS3" role="8Wnug">
            <ref role="3ygNvj" to="al5i:3AVJcIMlF9x" />
          </node>
        </node>
        <node concept="3_I8Xc" id="2$xY$aEJv3c" role="39821P">
          <ref role="3_I8Xa" to="al5i:64SK4bcO$6K" resolve="com.mbeddr.mpsutil.plantuml" />
        </node>
        <node concept="3_I8Xc" id="1xUfawlDfDx" role="39821P">
          <ref role="3_I8Xa" to="90a9:6860Y5A0_cI" resolve="de.itemis.mps.utils" />
        </node>
        <node concept="3_I8Xc" id="7pRzn8SVbWA" role="39821P">
          <ref role="3_I8Xa" to="lv7u:2$xY$aENvLN" resolve="no.uio.mLab" />
        </node>
        <node concept="3_I8Xc" id="7pRzn8SVbTp" role="39821P">
          <ref role="3_I8Xa" to="4owg:ofcHXg4JVY" resolve="no.furst.mLab" />
        </node>
        <node concept="3_J27D" id="6oczKrNHpYX" role="Nbhlr">
          <node concept="3Mxwew" id="6oczKrNHpYY" role="3MwsjC">
            <property role="3MwjfP" value="plugins" />
          </node>
        </node>
      </node>
      <node concept="1TblL5" id="6oczKrNHpYZ" role="39821P">
        <node concept="3_J27D" id="6oczKrNHpZ0" role="1TblL3">
          <node concept="3Mxwew" id="6oczKrNHpZ1" role="3MwsjC">
            <property role="3MwjfP" value="build.number" />
          </node>
        </node>
        <node concept="1TblLo" id="6oczKrNHpZ2" role="1TblLl">
          <property role="1TblLn" value="build.number" />
          <node concept="NbPM2" id="6oczKrNHpZ3" role="1TblLm">
            <node concept="3Mxwey" id="6oczKrNHpZ4" role="3MwsjC">
              <ref role="3Mxwex" node="6oczKrNHpUg" resolve="build.number" />
            </node>
          </node>
        </node>
        <node concept="1TblLo" id="6oczKrNHpZ5" role="1TblLl">
          <property role="1TblLn" value="date" />
          <node concept="NbPM2" id="6oczKrNHpZ6" role="1TblLm">
            <node concept="3Mxwey" id="6oczKrNHpZ7" role="3MwsjC">
              <ref role="3Mxwex" node="6oczKrNHpUe" resolve="date" />
            </node>
          </node>
        </node>
        <node concept="1TblLo" id="6oczKrNHpZ8" role="1TblLl">
          <property role="1TblLn" value="version" />
          <node concept="NbPM2" id="6oczKrNHpZ9" role="1TblLm">
            <node concept="3Mxwew" id="6oczKrNHpZa" role="3MwsjC">
              <property role="3MwjfP" value="1.0" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="m$_wf" id="6oczKrNHpYf" role="3989C9">
      <property role="m$_wk" value="mLabFurst" />
      <node concept="3_J27D" id="6oczKrNHpYg" role="m$_yQ">
        <node concept="3Mxwew" id="6oczKrNHpYh" role="3MwsjC">
          <property role="3MwjfP" value="mLabFurst" />
        </node>
      </node>
      <node concept="3_J27D" id="6oczKrNHpYi" role="m$_w8">
        <node concept="3Mxwew" id="6oczKrNHpYj" role="3MwsjC">
          <property role="3MwjfP" value="1.0" />
        </node>
      </node>
      <node concept="m$_yC" id="6oczKrNHpYl" role="m$_yJ">
        <ref role="m$_y1" to="ffeo:4k71ibbKLe8" resolve="jetbrains.mps.core" />
      </node>
      <node concept="m$_yC" id="ofcHXgg$GK" role="m$_yJ">
        <ref role="m$_y1" to="4owg:ofcHXg4JWt" resolve="no.furst.mLab.bundle" />
      </node>
      <node concept="3_J27D" id="6oczKrNHpYm" role="m_cZH">
        <node concept="3Mxwew" id="6oczKrNHpYn" role="3MwsjC">
          <property role="3MwjfP" value="mLabFurst" />
        </node>
      </node>
    </node>
    <node concept="1E1JtA" id="4MuIwsv1kUd" role="3989C9">
      <property role="BnDLt" value="true" />
      <property role="TrG5h" value="no.furst.mLab.regelsystem" />
      <property role="3LESm3" value="38a4235e-83dd-4249-b71b-fd73b55abc20" />
      <property role="2GAjPV" value="false" />
      <node concept="55IIr" id="4MuIwsv1kWe" role="3LF7KH">
        <node concept="2Ry0Ak" id="4MuIwsv1kWf" role="iGT6I">
          <property role="2Ry0Am" value="solutions" />
          <node concept="2Ry0Ak" id="4MuIwsv1kWg" role="2Ry0An">
            <property role="2Ry0Am" value="no.furst.mLab.regelsystem" />
            <node concept="2Ry0Ak" id="4MuIwsv1kWh" role="2Ry0An">
              <property role="2Ry0Am" value="no.furst.mLab.regelsystem.msd" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="1l3spW" id="6oczKrNHq2E">
    <property role="TrG5h" value="no.furst.mLab.distribution" />
    <property role="turDy" value="build_furst_distribution.xml" />
    <property role="2DA0ip" value="../../" />
    <node concept="2sgV4H" id="6oczKrNHq2F" role="1l3spa">
      <ref role="1l3spb" node="6oczKrNHpUb" resolve="no.furst.mLab.ide" />
    </node>
    <node concept="1l3spV" id="6oczKrNHq2G" role="1l3spN">
      <node concept="1tmT9g" id="6oczKrNHq3q" role="39821P">
        <property role="AB_bT" value="gzip" />
        <node concept="398223" id="6oczKrNHq3r" role="39821P">
          <node concept="3ygNvl" id="6oczKrNHq3s" role="39821P">
            <ref role="3ygNvj" node="6oczKrNHpYo" />
          </node>
          <node concept="398223" id="6oczKrNHq3t" role="39821P">
            <node concept="28jJK3" id="6oczKrNHq3u" role="39821P">
              <property role="28jJZ5" value="755" />
              <node concept="398BVA" id="6oczKrNHq2S" role="28jJRO">
                <ref role="398BVh" node="6oczKrNHq2H" resolve="mps_home" />
                <node concept="2Ry0Ak" id="6oczKrNHq2T" role="iGT6I">
                  <property role="2Ry0Am" value="bin" />
                  <node concept="2Ry0Ak" id="6oczKrNHq2U" role="2Ry0An">
                    <property role="2Ry0Am" value="linux" />
                    <node concept="2Ry0Ak" id="6oczKrNHq2V" role="2Ry0An">
                      <property role="2Ry0Am" value="fsnotifier" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="28jJK3" id="6oczKrNHq3v" role="39821P">
              <property role="28jJZ5" value="755" />
              <node concept="398BVA" id="6oczKrNHq30" role="28jJRO">
                <ref role="398BVh" node="6oczKrNHq2H" resolve="mps_home" />
                <node concept="2Ry0Ak" id="6oczKrNHq31" role="iGT6I">
                  <property role="2Ry0Am" value="bin" />
                  <node concept="2Ry0Ak" id="6oczKrNHq32" role="2Ry0An">
                    <property role="2Ry0Am" value="linux" />
                    <node concept="2Ry0Ak" id="6oczKrNHq33" role="2Ry0An">
                      <property role="2Ry0Am" value="fsnotifier64" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="28jJK3" id="6oczKrNHq3w" role="39821P">
              <node concept="3co7Ac" id="6oczKrNHq3x" role="28jJR8">
                <property role="3co7Am" value="lf" />
                <property role="3cpA_W" value="true" />
              </node>
              <node concept="398BVA" id="6oczKrNHq37" role="28jJRO">
                <ref role="398BVh" node="6oczKrNHq2H" resolve="mps_home" />
                <node concept="2Ry0Ak" id="6oczKrNHq38" role="iGT6I">
                  <property role="2Ry0Am" value="bin" />
                  <node concept="2Ry0Ak" id="6oczKrNHq39" role="2Ry0An">
                    <property role="2Ry0Am" value="mps.vmoptions" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="28jJK3" id="6oczKrNHq3y" role="39821P">
              <node concept="3co7Ac" id="6oczKrNHq3z" role="28jJR8">
                <property role="3co7Am" value="lf" />
                <property role="3cpA_W" value="true" />
              </node>
              <node concept="398BVA" id="6oczKrNHq3d" role="28jJRO">
                <ref role="398BVh" node="6oczKrNHq2H" resolve="mps_home" />
                <node concept="2Ry0Ak" id="6oczKrNHq3e" role="iGT6I">
                  <property role="2Ry0Am" value="bin" />
                  <node concept="2Ry0Ak" id="6oczKrNHq3f" role="2Ry0An">
                    <property role="2Ry0Am" value="mps64.vmoptions" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="2HvfSZ" id="6oczKrNHq3$" role="39821P">
              <node concept="3LWZYq" id="6oczKrNHq3_" role="2HvfZ1">
                <property role="3LWZYl" value="**/fsnotifier" />
              </node>
              <node concept="3LWZYq" id="6oczKrNHq3A" role="2HvfZ1">
                <property role="3LWZYl" value="**/fsnotifier64" />
              </node>
              <node concept="398BVA" id="6oczKrNHq3j" role="2HvfZ0">
                <ref role="398BVh" node="6oczKrNHq2H" resolve="mps_home" />
                <node concept="2Ry0Ak" id="6oczKrNHq3k" role="iGT6I">
                  <property role="2Ry0Am" value="bin" />
                  <node concept="2Ry0Ak" id="6oczKrNHq3l" role="2Ry0An">
                    <property role="2Ry0Am" value="linux" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3_J27D" id="6oczKrNHq3B" role="Nbhlr">
              <node concept="3Mxwew" id="6oczKrNHq3C" role="3MwsjC">
                <property role="3MwjfP" value="bin" />
              </node>
            </node>
          </node>
          <node concept="28jJK3" id="6oczKrNHq3D" role="39821P">
            <property role="28jJZ5" value="755" />
            <node concept="3co7Ac" id="6oczKrNHq3E" role="28jJR8">
              <property role="3co7Am" value="lf" />
              <property role="3cpA_W" value="true" />
            </node>
            <node concept="398BVA" id="6oczKrNHq3o" role="28jJRO">
              <ref role="398BVh" node="6oczKrNHq2H" resolve="mps_home" />
              <node concept="2Ry0Ak" id="6oczKrNHq3p" role="iGT6I">
                <property role="2Ry0Am" value="mps.sh" />
              </node>
            </node>
          </node>
          <node concept="3_J27D" id="6oczKrNHq3F" role="Nbhlr">
            <node concept="3Mxwew" id="6oczKrNHq3G" role="3MwsjC">
              <property role="3MwjfP" value="mLabFurst " />
            </node>
            <node concept="3Mxwey" id="6oczKrNHq3H" role="3MwsjC">
              <ref role="3Mxwex" node="6oczKrNHq2I" resolve="version" />
            </node>
          </node>
        </node>
        <node concept="3_J27D" id="6oczKrNHq3I" role="Nbhlr">
          <node concept="3Mxwey" id="6oczKrNHq3J" role="3MwsjC">
            <ref role="3Mxwex" node="6oczKrNHpUg" resolve="build.number" />
          </node>
          <node concept="3Mxwew" id="6oczKrNHq3K" role="3MwsjC">
            <property role="3MwjfP" value="-linux.tar.gz" />
          </node>
        </node>
      </node>
      <node concept="3981dG" id="6oczKrNHq6f" role="39821P">
        <node concept="398223" id="6oczKrNHq6g" role="39821P">
          <node concept="3ygNvl" id="6oczKrNHq6h" role="39821P">
            <ref role="3ygNvj" node="6oczKrNHpYo" />
          </node>
          <node concept="398223" id="6oczKrNHq6i" role="39821P">
            <node concept="3_J27D" id="6oczKrNHq6j" role="Nbhlr">
              <node concept="3Mxwew" id="6oczKrNHq6k" role="3MwsjC">
                <property role="3MwjfP" value="bin" />
              </node>
            </node>
            <node concept="28jJK3" id="6oczKrNHq6l" role="39821P">
              <node concept="398BVA" id="6oczKrNHq3O" role="28jJRO">
                <ref role="398BVh" node="6oczKrNHq2H" resolve="mps_home" />
                <node concept="2Ry0Ak" id="6oczKrNHq3P" role="iGT6I">
                  <property role="2Ry0Am" value="bin" />
                  <node concept="2Ry0Ak" id="6oczKrNHq3Q" role="2Ry0An">
                    <property role="2Ry0Am" value="mps.vmoptions" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="28jJK3" id="6oczKrNHq6m" role="39821P">
              <node concept="398BVA" id="6oczKrNHq3U" role="28jJRO">
                <ref role="398BVh" node="6oczKrNHq2H" resolve="mps_home" />
                <node concept="2Ry0Ak" id="6oczKrNHq3V" role="iGT6I">
                  <property role="2Ry0Am" value="bin" />
                  <node concept="2Ry0Ak" id="6oczKrNHq3W" role="2Ry0An">
                    <property role="2Ry0Am" value="mps.exe.vmoptions" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="28jJK3" id="6oczKrNHq6n" role="39821P">
              <node concept="398BVA" id="6oczKrNHq40" role="28jJRO">
                <ref role="398BVh" node="6oczKrNHq2H" resolve="mps_home" />
                <node concept="2Ry0Ak" id="6oczKrNHq41" role="iGT6I">
                  <property role="2Ry0Am" value="bin" />
                  <node concept="2Ry0Ak" id="6oczKrNHq42" role="2Ry0An">
                    <property role="2Ry0Am" value="mps64.vmoptions" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="28jJK3" id="6oczKrNHq6o" role="39821P">
              <node concept="398BVA" id="6oczKrNHq46" role="28jJRO">
                <ref role="398BVh" node="6oczKrNHq2H" resolve="mps_home" />
                <node concept="2Ry0Ak" id="6oczKrNHq47" role="iGT6I">
                  <property role="2Ry0Am" value="bin" />
                  <node concept="2Ry0Ak" id="6oczKrNHq48" role="2Ry0An">
                    <property role="2Ry0Am" value="mps64.exe.vmoptions" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="398223" id="6oczKrNHq6p" role="39821P">
              <node concept="3_J27D" id="6oczKrNHq6q" role="Nbhlr">
                <node concept="3Mxwew" id="6oczKrNHq6r" role="3MwsjC">
                  <property role="3MwjfP" value="win" />
                </node>
              </node>
              <node concept="2HvfSZ" id="6oczKrNHq6s" role="39821P">
                <node concept="3LWZYq" id="6oczKrNHq6t" role="2HvfZ1">
                  <property role="3LWZYl" value="**/*.exe" />
                </node>
                <node concept="398BVA" id="6oczKrNHq4c" role="2HvfZ0">
                  <ref role="398BVh" node="6oczKrNHq2H" resolve="mps_home" />
                  <node concept="2Ry0Ak" id="6oczKrNHq4d" role="iGT6I">
                    <property role="2Ry0Am" value="bin" />
                    <node concept="2Ry0Ak" id="6oczKrNHq4e" role="2Ry0An">
                      <property role="2Ry0Am" value="win" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="yKbIv" id="6oczKrNHq6u" role="39821P">
                <property role="yKbIr" value="755" />
                <node concept="2HvfSZ" id="6oczKrNHq6v" role="39821P">
                  <node concept="3LWZYx" id="6oczKrNHq6w" role="2HvfZ1">
                    <property role="3LWZYw" value="**/*.exe" />
                  </node>
                  <node concept="398BVA" id="6oczKrNHq4i" role="2HvfZ0">
                    <ref role="398BVh" node="6oczKrNHq2H" resolve="mps_home" />
                    <node concept="2Ry0Ak" id="6oczKrNHq4j" role="iGT6I">
                      <property role="2Ry0Am" value="bin" />
                      <node concept="2Ry0Ak" id="6oczKrNHq4k" role="2Ry0An">
                        <property role="2Ry0Am" value="win" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="398223" id="6oczKrNHq6x" role="39821P">
              <node concept="2HvfSZ" id="6oczKrNHq6y" role="39821P">
                <node concept="3LWZYq" id="6oczKrNHq6z" role="2HvfZ1">
                  <property role="3LWZYl" value="**/fsnotifier" />
                </node>
                <node concept="3LWZYq" id="6oczKrNHq6$" role="2HvfZ1">
                  <property role="3LWZYl" value="**/fsnotifier64" />
                </node>
                <node concept="398BVA" id="6oczKrNHq4o" role="2HvfZ0">
                  <ref role="398BVh" node="6oczKrNHq2H" resolve="mps_home" />
                  <node concept="2Ry0Ak" id="6oczKrNHq4p" role="iGT6I">
                    <property role="2Ry0Am" value="bin" />
                    <node concept="2Ry0Ak" id="6oczKrNHq4q" role="2Ry0An">
                      <property role="2Ry0Am" value="linux" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="28jJK3" id="6oczKrNHq6_" role="39821P">
                <property role="28jJZ5" value="755" />
                <node concept="398BVA" id="6oczKrNHq4v" role="28jJRO">
                  <ref role="398BVh" node="6oczKrNHq2H" resolve="mps_home" />
                  <node concept="2Ry0Ak" id="6oczKrNHq4w" role="iGT6I">
                    <property role="2Ry0Am" value="bin" />
                    <node concept="2Ry0Ak" id="6oczKrNHq4x" role="2Ry0An">
                      <property role="2Ry0Am" value="linux" />
                      <node concept="2Ry0Ak" id="6oczKrNHq4y" role="2Ry0An">
                        <property role="2Ry0Am" value="fsnotifier" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="28jJK3" id="6oczKrNHq6A" role="39821P">
                <property role="28jJZ5" value="755" />
                <node concept="398BVA" id="6oczKrNHq4B" role="28jJRO">
                  <ref role="398BVh" node="6oczKrNHq2H" resolve="mps_home" />
                  <node concept="2Ry0Ak" id="6oczKrNHq4C" role="iGT6I">
                    <property role="2Ry0Am" value="bin" />
                    <node concept="2Ry0Ak" id="6oczKrNHq4D" role="2Ry0An">
                      <property role="2Ry0Am" value="linux" />
                      <node concept="2Ry0Ak" id="6oczKrNHq4E" role="2Ry0An">
                        <property role="2Ry0Am" value="fsnotifier64" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3_J27D" id="6oczKrNHq6B" role="Nbhlr">
                <node concept="3Mxwew" id="6oczKrNHq6C" role="3MwsjC">
                  <property role="3MwjfP" value="linux" />
                </node>
              </node>
            </node>
            <node concept="398223" id="6oczKrNHq6I" role="39821P">
              <node concept="28jJK3" id="6oczKrNHq6J" role="39821P">
                <node concept="398BVA" id="6oczKrNHq4P" role="28jJRO">
                  <ref role="398BVh" node="6oczKrNHq2H" resolve="mps_home" />
                  <node concept="2Ry0Ak" id="6oczKrNHq4Q" role="iGT6I">
                    <property role="2Ry0Am" value="bin" />
                    <node concept="2Ry0Ak" id="6oczKrNHq4R" role="2Ry0An">
                      <property role="2Ry0Am" value="mac" />
                      <node concept="2Ry0Ak" id="6oczKrNHq4S" role="2Ry0An">
                        <property role="2Ry0Am" value="libbreakgen.jnilib" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="28jJK3" id="6oczKrNHq6K" role="39821P">
                <node concept="398BVA" id="6oczKrNHq4X" role="28jJRO">
                  <ref role="398BVh" node="6oczKrNHq2H" resolve="mps_home" />
                  <node concept="2Ry0Ak" id="6oczKrNHq4Y" role="iGT6I">
                    <property role="2Ry0Am" value="bin" />
                    <node concept="2Ry0Ak" id="6oczKrNHq4Z" role="2Ry0An">
                      <property role="2Ry0Am" value="mac" />
                      <node concept="2Ry0Ak" id="6oczKrNHq50" role="2Ry0An">
                        <property role="2Ry0Am" value="libbreakgen64.jnilib" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="28jJK3" id="6oczKrNHq6L" role="39821P">
                <property role="28jJZ5" value="755" />
                <node concept="398BVA" id="6oczKrNHq55" role="28jJRO">
                  <ref role="398BVh" node="6oczKrNHq2H" resolve="mps_home" />
                  <node concept="2Ry0Ak" id="6oczKrNHq56" role="iGT6I">
                    <property role="2Ry0Am" value="bin" />
                    <node concept="2Ry0Ak" id="6oczKrNHq57" role="2Ry0An">
                      <property role="2Ry0Am" value="mac" />
                      <node concept="2Ry0Ak" id="6oczKrNHq58" role="2Ry0An">
                        <property role="2Ry0Am" value="restarter" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="28jJK3" id="6oczKrNHq6M" role="39821P">
                <property role="28jJZ5" value="755" />
                <node concept="398BVA" id="6oczKrNHq5d" role="28jJRO">
                  <ref role="398BVh" node="6oczKrNHq2H" resolve="mps_home" />
                  <node concept="2Ry0Ak" id="6oczKrNHq5e" role="iGT6I">
                    <property role="2Ry0Am" value="bin" />
                    <node concept="2Ry0Ak" id="6oczKrNHq5f" role="2Ry0An">
                      <property role="2Ry0Am" value="mac" />
                      <node concept="2Ry0Ak" id="6oczKrNHq5g" role="2Ry0An">
                        <property role="2Ry0Am" value="fsnotifier" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="28jJK3" id="6oczKrNHq6N" role="39821P">
                <property role="28jJZ5" value="755" />
                <node concept="398BVA" id="6oczKrNHq5l" role="28jJRO">
                  <ref role="398BVh" node="6oczKrNHq2H" resolve="mps_home" />
                  <node concept="2Ry0Ak" id="6oczKrNHq5m" role="iGT6I">
                    <property role="2Ry0Am" value="bin" />
                    <node concept="2Ry0Ak" id="6oczKrNHq5n" role="2Ry0An">
                      <property role="2Ry0Am" value="mac" />
                      <node concept="2Ry0Ak" id="6oczKrNHq5o" role="2Ry0An">
                        <property role="2Ry0Am" value="printenv.py" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="398223" id="6oczKrNHq6O" role="39821P">
                <node concept="3_J27D" id="6oczKrNHq6P" role="Nbhlr">
                  <node concept="3Mxwew" id="6oczKrNHq6Q" role="3MwsjC">
                    <property role="3MwjfP" value="Contents" />
                  </node>
                </node>
                <node concept="398223" id="6oczKrNHq6R" role="39821P">
                  <node concept="3_J27D" id="6oczKrNHq6S" role="Nbhlr">
                    <node concept="3Mxwew" id="6oczKrNHq6T" role="3MwsjC">
                      <property role="3MwjfP" value="Resources" />
                    </node>
                  </node>
                  <node concept="28jJK3" id="6oczKrNHq6U" role="39821P">
                    <node concept="398BVA" id="6oczKrNHq5v" role="28jJRO">
                      <ref role="398BVh" node="6oczKrNHq2H" resolve="mps_home" />
                      <node concept="2Ry0Ak" id="6oczKrNHq5w" role="iGT6I">
                        <property role="2Ry0Am" value="bin" />
                        <node concept="2Ry0Ak" id="6oczKrNHq5x" role="2Ry0An">
                          <property role="2Ry0Am" value="mac" />
                          <node concept="2Ry0Ak" id="6oczKrNHq5y" role="2Ry0An">
                            <property role="2Ry0Am" value="Contents" />
                            <node concept="2Ry0Ak" id="6oczKrNHq5z" role="2Ry0An">
                              <property role="2Ry0Am" value="Resources" />
                              <node concept="2Ry0Ak" id="6oczKrNHq5$" role="2Ry0An">
                                <property role="2Ry0Am" value="mps.icns" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="398223" id="6oczKrNHq6V" role="39821P">
                  <node concept="3_J27D" id="6oczKrNHq6W" role="Nbhlr">
                    <node concept="3Mxwew" id="6oczKrNHq6X" role="3MwsjC">
                      <property role="3MwjfP" value="MacOS" />
                    </node>
                  </node>
                  <node concept="28jJK3" id="6oczKrNHq6Y" role="39821P">
                    <property role="28jJZ5" value="755" />
                    <node concept="398BVA" id="6oczKrNHq5F" role="28jJRO">
                      <ref role="398BVh" node="6oczKrNHq2H" resolve="mps_home" />
                      <node concept="2Ry0Ak" id="6oczKrNHq5G" role="iGT6I">
                        <property role="2Ry0Am" value="bin" />
                        <node concept="2Ry0Ak" id="6oczKrNHq5H" role="2Ry0An">
                          <property role="2Ry0Am" value="mac" />
                          <node concept="2Ry0Ak" id="6oczKrNHq5I" role="2Ry0An">
                            <property role="2Ry0Am" value="Contents" />
                            <node concept="2Ry0Ak" id="6oczKrNHq5J" role="2Ry0An">
                              <property role="2Ry0Am" value="MacOS" />
                              <node concept="2Ry0Ak" id="6oczKrNHq5K" role="2Ry0An">
                                <property role="2Ry0Am" value="mps" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="28jJK3" id="6oczKrNHq6Z" role="39821P">
                    <property role="28jJZ5" value="644" />
                    <node concept="398BVA" id="6oczKrNHq5R" role="28jJRO">
                      <ref role="398BVh" node="6oczKrNHq2H" resolve="mps_home" />
                      <node concept="2Ry0Ak" id="6oczKrNHq5S" role="iGT6I">
                        <property role="2Ry0Am" value="bin" />
                        <node concept="2Ry0Ak" id="6oczKrNHq5T" role="2Ry0An">
                          <property role="2Ry0Am" value="mac" />
                          <node concept="2Ry0Ak" id="6oczKrNHq5U" role="2Ry0An">
                            <property role="2Ry0Am" value="Contents" />
                            <node concept="2Ry0Ak" id="6oczKrNHq5V" role="2Ry0An">
                              <property role="2Ry0Am" value="MacOS" />
                              <node concept="2Ry0Ak" id="6oczKrNHq5W" role="2Ry0An">
                                <property role="2Ry0Am" value="idea_appLauncher" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="28jJK3" id="6oczKrNHq70" role="39821P">
                  <node concept="398BVA" id="6oczKrNHq62" role="28jJRO">
                    <ref role="398BVh" node="6oczKrNHq2H" resolve="mps_home" />
                    <node concept="2Ry0Ak" id="6oczKrNHq63" role="iGT6I">
                      <property role="2Ry0Am" value="bin" />
                      <node concept="2Ry0Ak" id="6oczKrNHq64" role="2Ry0An">
                        <property role="2Ry0Am" value="mac" />
                        <node concept="2Ry0Ak" id="6oczKrNHq65" role="2Ry0An">
                          <property role="2Ry0Am" value="Contents" />
                          <node concept="2Ry0Ak" id="6oczKrNHq66" role="2Ry0An">
                            <property role="2Ry0Am" value="Info.plist" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3_J27D" id="6oczKrNHq71" role="Nbhlr">
                <node concept="3Mxwew" id="6oczKrNHq72" role="3MwsjC">
                  <property role="3MwjfP" value="mac" />
                </node>
              </node>
            </node>
          </node>
          <node concept="28jJK3" id="6oczKrNHq73" role="39821P">
            <property role="28jJZ5" value="755" />
            <node concept="398BVA" id="6oczKrNHq69" role="28jJRO">
              <ref role="398BVh" node="6oczKrNHq2H" resolve="mps_home" />
              <node concept="2Ry0Ak" id="6oczKrNHq6a" role="iGT6I">
                <property role="2Ry0Am" value="mps.sh" />
              </node>
            </node>
          </node>
          <node concept="28jJK3" id="6oczKrNHq74" role="39821P">
            <property role="28jJZ5" value="755" />
            <node concept="398BVA" id="6oczKrNHq6d" role="28jJRO">
              <ref role="398BVh" node="6oczKrNHq2H" resolve="mps_home" />
              <node concept="2Ry0Ak" id="6oczKrNHq6e" role="iGT6I">
                <property role="2Ry0Am" value="mps.bat" />
              </node>
            </node>
          </node>
          <node concept="3_J27D" id="6oczKrNHq75" role="Nbhlr">
            <node concept="3Mxwew" id="6oczKrNHq76" role="3MwsjC">
              <property role="3MwjfP" value="mLabFurst " />
            </node>
            <node concept="3Mxwey" id="6oczKrNHq77" role="3MwsjC">
              <ref role="3Mxwex" node="6oczKrNHq2I" resolve="version" />
            </node>
          </node>
        </node>
        <node concept="3_J27D" id="6oczKrNHq78" role="Nbhlr">
          <node concept="3Mxwey" id="6oczKrNHq79" role="3MwsjC">
            <ref role="3Mxwex" node="6oczKrNHpUg" resolve="build.number" />
          </node>
          <node concept="3Mxwew" id="6oczKrNHq7a" role="3MwsjC">
            <property role="3MwjfP" value=".zip" />
          </node>
        </node>
      </node>
      <node concept="3981dG" id="6oczKrNHq8L" role="39821P">
        <node concept="3_J27D" id="6oczKrNHq8M" role="Nbhlr">
          <node concept="3Mxwey" id="6oczKrNHq8N" role="3MwsjC">
            <ref role="3Mxwex" node="6oczKrNHpUg" resolve="build.number" />
          </node>
          <node concept="3Mxwew" id="6oczKrNHq8O" role="3MwsjC">
            <property role="3MwjfP" value="-macos.zip" />
          </node>
        </node>
        <node concept="398223" id="6oczKrNHq8P" role="39821P">
          <node concept="398223" id="6oczKrNHq8Q" role="39821P">
            <node concept="3ygNvl" id="6oczKrNHq8R" role="39821P">
              <ref role="3ygNvj" node="6oczKrNHpYo" />
            </node>
            <node concept="3_J27D" id="6oczKrNHq8S" role="Nbhlr">
              <node concept="3Mxwew" id="6oczKrNHq8T" role="3MwsjC">
                <property role="3MwjfP" value="Contents" />
              </node>
            </node>
            <node concept="398223" id="6oczKrNHq8U" role="39821P">
              <node concept="3_J27D" id="6oczKrNHq8V" role="Nbhlr">
                <node concept="3Mxwew" id="6oczKrNHq8W" role="3MwsjC">
                  <property role="3MwjfP" value="Resources" />
                </node>
              </node>
              <node concept="28jJK3" id="6oczKrNHq8X" role="39821P">
                <node concept="398BVA" id="6oczKrNHq7h" role="28jJRO">
                  <ref role="398BVh" node="6oczKrNHq2H" resolve="mps_home" />
                  <node concept="2Ry0Ak" id="6oczKrNHq7i" role="iGT6I">
                    <property role="2Ry0Am" value="bin" />
                    <node concept="2Ry0Ak" id="6oczKrNHq7j" role="2Ry0An">
                      <property role="2Ry0Am" value="mac" />
                      <node concept="2Ry0Ak" id="6oczKrNHq7k" role="2Ry0An">
                        <property role="2Ry0Am" value="Contents" />
                        <node concept="2Ry0Ak" id="6oczKrNHq7l" role="2Ry0An">
                          <property role="2Ry0Am" value="Resources" />
                          <node concept="2Ry0Ak" id="6oczKrNHq7m" role="2Ry0An">
                            <property role="2Ry0Am" value="mps.icns" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="398223" id="6oczKrNHq8Y" role="39821P">
              <node concept="28jJK3" id="6oczKrNHq8Z" role="39821P">
                <property role="28jJZ5" value="755" />
                <node concept="398BVA" id="6oczKrNHq7t" role="28jJRO">
                  <ref role="398BVh" node="6oczKrNHq2H" resolve="mps_home" />
                  <node concept="2Ry0Ak" id="6oczKrNHq7u" role="iGT6I">
                    <property role="2Ry0Am" value="bin" />
                    <node concept="2Ry0Ak" id="6oczKrNHq7v" role="2Ry0An">
                      <property role="2Ry0Am" value="mac" />
                      <node concept="2Ry0Ak" id="6oczKrNHq7w" role="2Ry0An">
                        <property role="2Ry0Am" value="Contents" />
                        <node concept="2Ry0Ak" id="6oczKrNHq7x" role="2Ry0An">
                          <property role="2Ry0Am" value="MacOS" />
                          <node concept="2Ry0Ak" id="6oczKrNHq7y" role="2Ry0An">
                            <property role="2Ry0Am" value="mps" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3_J27D" id="6oczKrNHq90" role="Nbhlr">
                <node concept="3Mxwew" id="6oczKrNHq91" role="3MwsjC">
                  <property role="3MwjfP" value="MacOS" />
                </node>
              </node>
              <node concept="28jJK3" id="6oczKrNHq92" role="39821P">
                <property role="28jJZ5" value="644" />
                <node concept="398BVA" id="6oczKrNHq7D" role="28jJRO">
                  <ref role="398BVh" node="6oczKrNHq2H" resolve="mps_home" />
                  <node concept="2Ry0Ak" id="6oczKrNHq7E" role="iGT6I">
                    <property role="2Ry0Am" value="bin" />
                    <node concept="2Ry0Ak" id="6oczKrNHq7F" role="2Ry0An">
                      <property role="2Ry0Am" value="mac" />
                      <node concept="2Ry0Ak" id="6oczKrNHq7G" role="2Ry0An">
                        <property role="2Ry0Am" value="Contents" />
                        <node concept="2Ry0Ak" id="6oczKrNHq7H" role="2Ry0An">
                          <property role="2Ry0Am" value="MacOS" />
                          <node concept="2Ry0Ak" id="6oczKrNHq7I" role="2Ry0An">
                            <property role="2Ry0Am" value="idea_appLauncher" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="28jJK3" id="6oczKrNHq93" role="39821P">
              <node concept="398BVA" id="6oczKrNHq7O" role="28jJRO">
                <ref role="398BVh" node="6oczKrNHq2H" resolve="mps_home" />
                <node concept="2Ry0Ak" id="6oczKrNHq7P" role="iGT6I">
                  <property role="2Ry0Am" value="bin" />
                  <node concept="2Ry0Ak" id="6oczKrNHq7Q" role="2Ry0An">
                    <property role="2Ry0Am" value="mac" />
                    <node concept="2Ry0Ak" id="6oczKrNHq7R" role="2Ry0An">
                      <property role="2Ry0Am" value="Contents" />
                      <node concept="2Ry0Ak" id="6oczKrNHq7S" role="2Ry0An">
                        <property role="2Ry0Am" value="Info.plist" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="398223" id="6oczKrNHq94" role="39821P">
              <node concept="3_J27D" id="6oczKrNHq95" role="Nbhlr">
                <node concept="3Mxwew" id="6oczKrNHq96" role="3MwsjC">
                  <property role="3MwjfP" value="bin" />
                </node>
              </node>
              <node concept="28jJK3" id="6oczKrNHq97" role="39821P">
                <node concept="398BVA" id="6oczKrNHq7X" role="28jJRO">
                  <ref role="398BVh" node="6oczKrNHq2H" resolve="mps_home" />
                  <node concept="2Ry0Ak" id="6oczKrNHq7Y" role="iGT6I">
                    <property role="2Ry0Am" value="bin" />
                    <node concept="2Ry0Ak" id="6oczKrNHq7Z" role="2Ry0An">
                      <property role="2Ry0Am" value="mac" />
                      <node concept="2Ry0Ak" id="6oczKrNHq80" role="2Ry0An">
                        <property role="2Ry0Am" value="libbreakgen.jnilib" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="28jJK3" id="6oczKrNHq98" role="39821P">
                <node concept="398BVA" id="6oczKrNHq85" role="28jJRO">
                  <ref role="398BVh" node="6oczKrNHq2H" resolve="mps_home" />
                  <node concept="2Ry0Ak" id="6oczKrNHq86" role="iGT6I">
                    <property role="2Ry0Am" value="bin" />
                    <node concept="2Ry0Ak" id="6oczKrNHq87" role="2Ry0An">
                      <property role="2Ry0Am" value="mac" />
                      <node concept="2Ry0Ak" id="6oczKrNHq88" role="2Ry0An">
                        <property role="2Ry0Am" value="libbreakgen64.jnilib" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="28jJK3" id="6oczKrNHq99" role="39821P">
                <property role="28jJZ5" value="755" />
                <node concept="398BVA" id="6oczKrNHq8d" role="28jJRO">
                  <ref role="398BVh" node="6oczKrNHq2H" resolve="mps_home" />
                  <node concept="2Ry0Ak" id="6oczKrNHq8e" role="iGT6I">
                    <property role="2Ry0Am" value="bin" />
                    <node concept="2Ry0Ak" id="6oczKrNHq8f" role="2Ry0An">
                      <property role="2Ry0Am" value="mac" />
                      <node concept="2Ry0Ak" id="6oczKrNHq8g" role="2Ry0An">
                        <property role="2Ry0Am" value="restarter" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="28jJK3" id="6oczKrNHq9a" role="39821P">
                <property role="28jJZ5" value="755" />
                <node concept="398BVA" id="6oczKrNHq8l" role="28jJRO">
                  <ref role="398BVh" node="6oczKrNHq2H" resolve="mps_home" />
                  <node concept="2Ry0Ak" id="6oczKrNHq8m" role="iGT6I">
                    <property role="2Ry0Am" value="bin" />
                    <node concept="2Ry0Ak" id="6oczKrNHq8n" role="2Ry0An">
                      <property role="2Ry0Am" value="mac" />
                      <node concept="2Ry0Ak" id="6oczKrNHq8o" role="2Ry0An">
                        <property role="2Ry0Am" value="fsnotifier" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="28jJK3" id="6oczKrNHq9b" role="39821P">
                <node concept="3co7Ac" id="6oczKrNHq9c" role="28jJR8">
                  <property role="3co7Am" value="lf" />
                  <property role="3cpA_W" value="true" />
                </node>
                <node concept="398BVA" id="6oczKrNHq8s" role="28jJRO">
                  <ref role="398BVh" node="6oczKrNHq2H" resolve="mps_home" />
                  <node concept="2Ry0Ak" id="6oczKrNHq8t" role="iGT6I">
                    <property role="2Ry0Am" value="bin" />
                    <node concept="2Ry0Ak" id="6oczKrNHq8u" role="2Ry0An">
                      <property role="2Ry0Am" value="mps.vmoptions" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="28jJK3" id="6oczKrNHq9d" role="39821P">
                <node concept="3co7Ac" id="6oczKrNHq9e" role="28jJR8">
                  <property role="3co7Am" value="lf" />
                  <property role="3cpA_W" value="true" />
                </node>
                <node concept="398BVA" id="6oczKrNHq8y" role="28jJRO">
                  <ref role="398BVh" node="6oczKrNHq2H" resolve="mps_home" />
                  <node concept="2Ry0Ak" id="6oczKrNHq8z" role="iGT6I">
                    <property role="2Ry0Am" value="bin" />
                    <node concept="2Ry0Ak" id="6oczKrNHq8$" role="2Ry0An">
                      <property role="2Ry0Am" value="mps64.vmoptions" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="28jJK3" id="6oczKrNHq9f" role="39821P">
                <property role="28jJZ5" value="755" />
                <node concept="3co7Ac" id="6oczKrNHq9g" role="28jJR8">
                  <property role="3co7Am" value="lf" />
                  <property role="3cpA_W" value="true" />
                </node>
                <node concept="398BVA" id="6oczKrNHq8D" role="28jJRO">
                  <ref role="398BVh" node="6oczKrNHq2H" resolve="mps_home" />
                  <node concept="2Ry0Ak" id="6oczKrNHq8E" role="iGT6I">
                    <property role="2Ry0Am" value="bin" />
                    <node concept="2Ry0Ak" id="6oczKrNHq8F" role="2Ry0An">
                      <property role="2Ry0Am" value="mac" />
                      <node concept="2Ry0Ak" id="6oczKrNHq8G" role="2Ry0An">
                        <property role="2Ry0Am" value="printenv.py" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="28jJK3" id="6oczKrNHq9h" role="39821P">
              <property role="28jJZ5" value="755" />
              <node concept="3co7Ac" id="6oczKrNHq9i" role="28jJR8">
                <property role="3co7Am" value="lf" />
                <property role="3cpA_W" value="true" />
              </node>
              <node concept="398BVA" id="6oczKrNHq8J" role="28jJRO">
                <ref role="398BVh" node="6oczKrNHq2H" resolve="mps_home" />
                <node concept="2Ry0Ak" id="6oczKrNHq8K" role="iGT6I">
                  <property role="2Ry0Am" value="mps.sh" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3_J27D" id="6oczKrNHq9j" role="Nbhlr">
            <node concept="3Mxwew" id="6oczKrNHq9k" role="3MwsjC">
              <property role="3MwjfP" value="mLabFurst " />
            </node>
            <node concept="3Mxwey" id="6oczKrNHq9l" role="3MwsjC">
              <ref role="3Mxwex" node="6oczKrNHq2I" resolve="version" />
            </node>
            <node concept="3Mxwew" id="6oczKrNHq9m" role="3MwsjC">
              <property role="3MwjfP" value=".app" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="398rNT" id="6oczKrNHq2H" role="1l3spd">
      <property role="TrG5h" value="mps_home" />
    </node>
    <node concept="2kB4xC" id="6oczKrNHq2I" role="1l3spd">
      <property role="TrG5h" value="version" />
      <node concept="aVJcg" id="6oczKrNHq2J" role="aVJcv">
        <node concept="NbPM2" id="6oczKrNHq2K" role="aVJcq">
          <node concept="3Mxwew" id="6oczKrNHq2L" role="3MwsjC">
            <property role="3MwjfP" value="2018.2" />
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

