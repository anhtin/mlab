<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:4a15446b-18f6-473d-9989-8199dfed8381(no.furst.mLab.regelsystem.entities)">
  <persistence version="9" />
  <languages>
    <use id="284ce324-ec91-4245-a186-fefe6b9a1ea8" name="no.uio.mLab.entities.biochemistry" version="0" />
    <use id="bdbd2c14-a335-48a1-809b-6b32de6d4748" name="no.furst.mLab.entities" version="0" />
    <use id="7de0968d-bd8f-4776-b76d-8d0cb1ebf77b" name="no.uio.mLab.entities.microbiology" version="0" />
    <use id="cae652fa-7f6e-4122-8bd5-0b26c8b4eec8" name="no.uio.mLab.entities.laboratoryTest" version="0" />
  </languages>
  <imports />
  <registry>
    <language id="284ce324-ec91-4245-a186-fefe6b9a1ea8" name="no.uio.mLab.entities.biochemistry">
      <concept id="6849753176701914014" name="no.uio.mLab.entities.biochemistry.structure.BiochemistryTest" flags="ng" index="1vpfQB">
        <property id="8466382076832006276" name="unit" index="Fce4w" />
      </concept>
      <concept id="1560130832615969003" name="no.uio.mLab.entities.biochemistry.structure.BiochemistrySpecialistType" flags="ng" index="3$$M9i" />
    </language>
    <language id="bdbd2c14-a335-48a1-809b-6b32de6d4748" name="no.furst.mLab.entities">
      <concept id="1560130832624016893" name="no.furst.mLab.entities.structure.Autokommentar" flags="ng" index="3$5IX4" />
    </language>
    <language id="7de0968d-bd8f-4776-b76d-8d0cb1ebf77b" name="no.uio.mLab.entities.microbiology">
      <concept id="7282862830133583708" name="no.uio.mLab.entities.microbiology.structure.Pathogen" flags="ng" index="74Z9O" />
      <concept id="7282862830136139438" name="no.uio.mLab.entities.microbiology.structure.CultureTest" flags="ng" index="7efe6" />
      <concept id="7282862830139125529" name="no.uio.mLab.entities.microbiology.structure.SecondaryMicrobiologyWork" flags="ng" index="7hO8L" />
      <concept id="7282862830145140395" name="no.uio.mLab.entities.microbiology.structure.Antimicrobial" flags="ng" index="7CTI3" />
    </language>
    <language id="33c0f773-be29-48e3-9587-40dc4b3c54f0" name="no.uio.mLab.entities.core">
      <concept id="5601053190799076535" name="no.uio.mLab.entities.core.structure.Entity" flags="ng" index="307PM4">
        <property id="7282862830133603774" name="code" index="74S2m" />
        <property id="5601053190799204442" name="description" index="307l1D" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="cae652fa-7f6e-4122-8bd5-0b26c8b4eec8" name="no.uio.mLab.entities.laboratoryTest">
      <concept id="7282862830136085112" name="no.uio.mLab.entities.laboratoryTest.structure.SpecimenType" flags="ng" index="7eqtg" />
    </language>
  </registry>
  <node concept="1vpfQB" id="1mAGFBLaKtq">
    <property role="TrG5h" value="A58 PEth" />
    <property role="3GE5qa" value="biochemistry.tests.alcohol" />
    <property role="307l1D" value="B-Fosfatidyletanol (PEth) 16:0/18:1" />
    <property role="Fce4w" value="µmol/L" />
    <property role="74S2m" value="A58" />
  </node>
  <node concept="1vpfQB" id="1mAGFBLaKtr">
    <property role="TrG5h" value="658 CDT" />
    <property role="3GE5qa" value="biochemistry.tests.alcohol" />
    <property role="Fce4w" value="%" />
    <property role="74S2m" value="658" />
    <property role="307l1D" value="P-CDT" />
  </node>
  <node concept="1vpfQB" id="1mAGFBLaKts">
    <property role="TrG5h" value="567 S-Etanol" />
    <property role="3GE5qa" value="biochemistry.tests.alcohol" />
    <property role="Fce4w" value="‰" />
    <property role="74S2m" value="568" />
  </node>
  <node concept="3$5IX4" id="1mAGFBLG8R6">
    <property role="3GE5qa" value="biochemistry.codedComments" />
    <property role="TrG5h" value="AA5851" />
    <property role="307l1D" value="K51" />
    <property role="74S2m" value="AA5851" />
  </node>
  <node concept="3$5IX4" id="1mAGFBLG8R7">
    <property role="3GE5qa" value="biochemistry.codedComments" />
    <property role="TrG5h" value="A65811" />
    <property role="307l1D" value="K11" />
    <property role="74S2m" value="A65811" />
  </node>
  <node concept="3$5IX4" id="1mAGFBLG8R8">
    <property role="3GE5qa" value="biochemistry.codedComments" />
    <property role="TrG5h" value="A65812" />
    <property role="307l1D" value="K12" />
    <property role="74S2m" value="A65812" />
  </node>
  <node concept="3$5IX4" id="1mAGFBLG8R9">
    <property role="3GE5qa" value="biochemistry.codedComments" />
    <property role="TrG5h" value="A65813" />
    <property role="307l1D" value="K13" />
    <property role="74S2m" value="A65813" />
  </node>
  <node concept="3$$M9i" id="1mAGFBLdDxo">
    <property role="3GE5qa" value="specialities.biochemistry" />
    <property role="TrG5h" value="CDT" />
    <property role="74S2m" value="CDT" />
  </node>
  <node concept="3$5IX4" id="2XLt5KX3xG8">
    <property role="3GE5qa" value="biochemistry.codedComments" />
    <property role="TrG5h" value="AA5801" />
    <property role="307l1D" value="K01" />
    <property role="74S2m" value="AA5801" />
  </node>
  <node concept="74Z9O" id="6khVixy0$rs">
    <property role="3GE5qa" value="microbiology.pathogens" />
    <property role="TrG5h" value="Staphylococcus aureus" />
    <property role="74S2m" value="PA00" />
  </node>
  <node concept="7eqtg" id="6khVixya4XV">
    <property role="TrG5h" value="Wound Swab" />
    <property role="74S2m" value="L11" />
    <property role="3GE5qa" value="specimenTypes" />
  </node>
  <node concept="7efe6" id="6khVixydbyJ">
    <property role="3GE5qa" value="microbiology.cultureTests" />
    <property role="TrG5h" value="Us-Aerob" />
    <property role="74S2m" value="U01" />
    <property role="307l1D" value="Vanlig dyrkning, generell dyrkning" />
  </node>
  <node concept="7efe6" id="6khVixydf1d">
    <property role="3GE5qa" value="microbiology.cultureTests" />
    <property role="TrG5h" value="Us-Anaerob" />
    <property role="74S2m" value="U02" />
  </node>
  <node concept="7efe6" id="6khVixydf1h">
    <property role="3GE5qa" value="microbiology.cultureTests" />
    <property role="74S2m" value="P92" />
    <property role="TrG5h" value="P92 DERMFYTT" />
  </node>
  <node concept="7efe6" id="6khVixydf1i">
    <property role="3GE5qa" value="microbiology.cultureTests" />
    <property role="TrG5h" value="P91 DERMFYTT" />
    <property role="74S2m" value="P91" />
  </node>
  <node concept="7efe6" id="6khVixydf1j">
    <property role="3GE5qa" value="microbiology.cultureTests" />
    <property role="TrG5h" value="P90 DERMFYTT" />
    <property role="74S2m" value="P90" />
  </node>
  <node concept="7eqtg" id="6khVixyiqiN">
    <property role="3GE5qa" value="specimenTypes" />
    <property role="TrG5h" value="Feces" />
    <property role="74S2m" value="L??" />
  </node>
  <node concept="7eqtg" id="6khVixyGx_f">
    <property role="3GE5qa" value="specimenTypes" />
    <property role="TrG5h" value="Nasofarynx" />
    <property role="74S2m" value="L04" />
  </node>
  <node concept="7eqtg" id="6khVixyGx_g">
    <property role="3GE5qa" value="specimenTypes" />
    <property role="TrG5h" value="Urine" />
    <property role="74S2m" value="M01" />
  </node>
  <node concept="7eqtg" id="6khVixyGx_h">
    <property role="TrG5h" value="Uricult" />
    <property role="74S2m" value="M02" />
    <property role="3GE5qa" value="specimenTypes" />
  </node>
  <node concept="7eqtg" id="6khVixyGx_i">
    <property role="TrG5h" value="Ear Swab" />
    <property role="74S2m" value="L02" />
    <property role="3GE5qa" value="specimenTypes" />
  </node>
  <node concept="7CTI3" id="6khVixyHsQz">
    <property role="3GE5qa" value="microbiology.antimicrobials" />
    <property role="TrG5h" value="Erythromycin" />
    <property role="74S2m" value="AM00" />
  </node>
  <node concept="7CTI3" id="6khVixyHvbg">
    <property role="3GE5qa" value="microbiology.antimicrobials" />
    <property role="TrG5h" value="Trimetoprim-sulfametoxazol" />
    <property role="74S2m" value="AM01" />
  </node>
  <node concept="7CTI3" id="6khVixyHvbh">
    <property role="3GE5qa" value="microbiology.antimicrobials" />
    <property role="TrG5h" value="Amoksicillin" />
    <property role="74S2m" value="AM02" />
  </node>
  <node concept="7CTI3" id="6khVixyHvbi">
    <property role="3GE5qa" value="microbiology.antimicrobials" />
    <property role="TrG5h" value="Amox/K Clav" />
    <property role="74S2m" value="AM03" />
  </node>
  <node concept="7CTI3" id="6khVixyHvbj">
    <property role="3GE5qa" value="microbiology.antimicrobials" />
    <property role="TrG5h" value="Tetracyclin" />
    <property role="74S2m" value="AM04" />
  </node>
  <node concept="7CTI3" id="6khVixyHvbk">
    <property role="3GE5qa" value="microbiology.antimicrobials" />
    <property role="TrG5h" value="Ciprofloxacin" />
    <property role="74S2m" value="AM05" />
  </node>
  <node concept="7CTI3" id="6khVixyHvbl">
    <property role="3GE5qa" value="microbiology.antimicrobials" />
    <property role="TrG5h" value="Cefotaxim" />
    <property role="74S2m" value="AM07" />
  </node>
  <node concept="74Z9O" id="6khVixyHvMd">
    <property role="3GE5qa" value="microbiology.pathogens" />
    <property role="TrG5h" value="Staphylococcus lugdunensis" />
    <property role="74S2m" value="PA01" />
  </node>
  <node concept="74Z9O" id="6khVixyHvMe">
    <property role="3GE5qa" value="microbiology.pathogens" />
    <property role="TrG5h" value="Haemophilus influenzae" />
    <property role="74S2m" value="PA02" />
  </node>
  <node concept="74Z9O" id="6khVixyHvMf">
    <property role="3GE5qa" value="microbiology.pathogens" />
    <property role="TrG5h" value="Enterococcus faecalis" />
    <property role="74S2m" value="PA03" />
  </node>
  <node concept="74Z9O" id="6khVixyHvMg">
    <property role="3GE5qa" value="microbiology.pathogens" />
    <property role="TrG5h" value="Aerococcus urinae" />
    <property role="74S2m" value="PA04" />
  </node>
  <node concept="7eqtg" id="6khVixyKcjP">
    <property role="TrG5h" value="Eye Swab" />
    <property role="74S2m" value="L01" />
    <property role="3GE5qa" value="specimenTypes" />
  </node>
  <node concept="1vpfQB" id="PDjyzkxhHo">
    <property role="3GE5qa" value="biochemistry.tests.calprotectin" />
    <property role="TrG5h" value="484 Kalprot" />
    <property role="74S2m" value="484" />
    <property role="307l1D" value="F-Kalprotektin" />
    <property role="Fce4w" value="mg/kg" />
  </node>
  <node concept="3$5IX4" id="PDjyzkxhHp">
    <property role="3GE5qa" value="biochemistry.codedComments" />
    <property role="TrG5h" value="A48401" />
    <property role="74S2m" value="A48401" />
    <property role="307l1D" value="Lav sannsynlighet for inflammasjon av betydning eller aktiv inflammatorisk tarmsykdom" />
  </node>
  <node concept="3$5IX4" id="PDjyzkxhHq">
    <property role="3GE5qa" value="biochemistry.codedComments" />
    <property role="TrG5h" value="A48402" />
    <property role="74S2m" value="A48401" />
    <property role="307l1D" value="Lettere inflammasjon kan foreligge, forenlig med gastroenteritt eller inflammatorisk tarmsykdom." />
  </node>
  <node concept="3$5IX4" id="PDjyzkxhHr">
    <property role="3GE5qa" value="biochemistry.codedComments" />
    <property role="TrG5h" value="A48403" />
    <property role="74S2m" value="A48401" />
    <property role="307l1D" value="Lettere inflammasjon kan foreligge. Ved klinisk mistanke om udiagnostisert inflammatorisk tarmsykdom kan kontrollprøve innen tre måneder vurderes. Ved kjent inflammaotrisk tarmsykdom er risikoen for klinisk tilbakefall økt og ny kontrollprøve etter tre til seks måneder kan vurderes." />
  </node>
  <node concept="3$5IX4" id="PDjyzkxhHs">
    <property role="3GE5qa" value="biochemistry.codedComments" />
    <property role="TrG5h" value="A48404" />
    <property role="74S2m" value="A48401" />
    <property role="307l1D" value="Inflammasjon foreligger. Ved klinisk mistanke om udiagnostisert inflammatorisk tarmsykdom bør pasienten følges opp. Ved kjent inflammatorisk tarmsykdom kan aktuell terapi ha utilstrekkelig effekt." />
  </node>
  <node concept="1vpfQB" id="PDjyzk_uXx">
    <property role="3GE5qa" value="biochemistry.tests.coeliac" />
    <property role="TrG5h" value="041 fS-Glukose" />
    <property role="74S2m" value="041" />
    <property role="Fce4w" value="mmol/L" />
    <property role="307l1D" value="P-Glukose, fastende" />
  </node>
  <node concept="1vpfQB" id="PDjyzk_uXy">
    <property role="3GE5qa" value="biochemistry.tests.coeliac" />
    <property role="TrG5h" value="042 Pt-Glukosebelastning" />
    <property role="74S2m" value="042" />
    <property role="Fce4w" value="mmol/L" />
  </node>
  <node concept="1vpfQB" id="PDjyzk_uXz">
    <property role="3GE5qa" value="biochemistry.tests.coeliac" />
    <property role="TrG5h" value="341 S-Glukose ikke fast" />
    <property role="74S2m" value="341" />
    <property role="Fce4w" value="mmol/L" />
    <property role="307l1D" value="P-Glukose" />
  </node>
  <node concept="1vpfQB" id="PDjyzk_uX$">
    <property role="3GE5qa" value="biochemistry.tests.coeliac" />
    <property role="TrG5h" value="331 Pt-Glukosebelastning, utvidet" />
    <property role="74S2m" value="331" />
    <property role="Fce4w" value="mmol/L" />
  </node>
  <node concept="1vpfQB" id="PDjyzk_uX_">
    <property role="3GE5qa" value="biochemistry.tests.coeliac" />
    <property role="TrG5h" value="145 Belastning" />
    <property role="74S2m" value="145" />
    <property role="Fce4w" value="mmol/L" />
  </node>
  <node concept="1vpfQB" id="PDjyzk_uXA">
    <property role="3GE5qa" value="biochemistry.tests.coeliac" />
    <property role="TrG5h" value="141 S-Glukose 2t" />
    <property role="74S2m" value="141" />
    <property role="Fce4w" value="mmol/L" />
  </node>
  <node concept="1vpfQB" id="PDjyzk_uXB">
    <property role="3GE5qa" value="biochemistry.tests.coeliac" />
    <property role="TrG5h" value="332 S-Glukose 1t" />
    <property role="74S2m" value="332" />
    <property role="Fce4w" value="mmol/L" />
  </node>
  <node concept="1vpfQB" id="PDjyzk_uXC">
    <property role="3GE5qa" value="biochemistry.tests.coeliac" />
    <property role="TrG5h" value="846 S-Glukose 3t" />
    <property role="74S2m" value="846" />
    <property role="Fce4w" value="mmol/L" />
  </node>
  <node concept="1vpfQB" id="PDjyzk_uXD">
    <property role="3GE5qa" value="biochemistry.tests.coeliac" />
    <property role="TrG5h" value="847 S-Glukose 4t" />
    <property role="74S2m" value="847" />
    <property role="Fce4w" value="mmol/L" />
  </node>
  <node concept="1vpfQB" id="PDjyzk_uXE">
    <property role="3GE5qa" value="biochemistry.tests.coeliac" />
    <property role="TrG5h" value="855 S-Glukose 5t" />
    <property role="74S2m" value="855" />
    <property role="Fce4w" value="mmol/L" />
  </node>
  <node concept="1vpfQB" id="PDjyzk_uXF">
    <property role="3GE5qa" value="biochemistry.tests.coeliac" />
    <property role="TrG5h" value="336 Sentrifugert her" />
    <property role="74S2m" value="336" />
  </node>
  <node concept="1vpfQB" id="PDjyzk_uXG">
    <property role="3GE5qa" value="biochemistry.tests.coeliac" />
    <property role="TrG5h" value="986 Sentrifugering ikke ok" />
    <property role="74S2m" value="986" />
  </node>
  <node concept="1vpfQB" id="5jVYnMGVvXF">
    <property role="3GE5qa" value="biochemistry.tests.diarrhea" />
    <property role="TrG5h" value="N66" />
    <property role="74S2m" value="N66" />
  </node>
  <node concept="1vpfQB" id="5jVYnMGVvXG">
    <property role="3GE5qa" value="biochemistry.tests.diarrhea" />
    <property role="TrG5h" value="N73" />
    <property role="74S2m" value="N73" />
  </node>
  <node concept="1vpfQB" id="5jVYnMGVvXH">
    <property role="3GE5qa" value="biochemistry.tests.diarrhea" />
    <property role="TrG5h" value="N63" />
    <property role="74S2m" value="N63" />
  </node>
  <node concept="1vpfQB" id="5jVYnMGVw9H">
    <property role="3GE5qa" value="biochemistry.tests.diarrhea" />
    <property role="TrG5h" value="N71" />
    <property role="74S2m" value="N71" />
  </node>
  <node concept="3$$M9i" id="5jVYnMGVSvP">
    <property role="3GE5qa" value="specialities.biochemistry" />
    <property role="TrG5h" value="DIARRHEA" />
    <property role="74S2m" value="DIARE" />
  </node>
  <node concept="7hO8L" id="6khVixyGx_e">
    <property role="3GE5qa" value="microbiology.secondaryWork" />
    <property role="TrG5h" value="Tellur test" />
    <property role="74S2m" value="TE" />
  </node>
  <node concept="7hO8L" id="6khVixyGx_c">
    <property role="3GE5qa" value="microbiology.secondaryWork" />
    <property role="TrG5h" value="Staphylococcus agglutination" />
    <property role="74S2m" value="SX" />
  </node>
  <node concept="7hO8L" id="6khVixyGx_d">
    <property role="3GE5qa" value="microbiology.secondaryWork" />
    <property role="TrG5h" value="XV test" />
    <property role="74S2m" value="XV" />
  </node>
  <node concept="7hO8L" id="6khVixylHtD">
    <property role="3GE5qa" value="microbiology.secondaryWork" />
    <property role="TrG5h" value="Send to ref. laboratory" />
    <property role="74S2m" value="REF" />
  </node>
  <node concept="7CTI3" id="2FjKBCR26E2">
    <property role="3GE5qa" value="microbiology.antimicrobials" />
    <property role="TrG5h" value="Vancomycin" />
    <property role="74S2m" value="AM08" />
  </node>
  <node concept="7CTI3" id="2FjKBCR26E3">
    <property role="3GE5qa" value="microbiology.antimicrobials" />
    <property role="TrG5h" value="Ceftazidim" />
    <property role="74S2m" value="AM09" />
  </node>
  <node concept="7CTI3" id="2FjKBCR26E4">
    <property role="3GE5qa" value="microbiology.antimicrobials" />
    <property role="TrG5h" value="Cefoxitin" />
    <property role="74S2m" value="AM10" />
  </node>
  <node concept="7hO8L" id="2FjKBCR26E5">
    <property role="3GE5qa" value="microbiology.secondaryWork" />
    <property role="TrG5h" value="e-test ESBLₘ" />
    <property role="74S2m" value="ESBSLM" />
  </node>
  <node concept="7hO8L" id="2FjKBCR26E6">
    <property role="3GE5qa" value="microbiology.secondaryWork" />
    <property role="TrG5h" value="e-test ESBLₐ" />
    <property role="74S2m" value="ESBSLA" />
  </node>
  <node concept="7hO8L" id="2FjKBCR26E7">
    <property role="3GE5qa" value="microbiology.secondaryWork" />
    <property role="TrG5h" value="Vanomycin test A" />
    <property role="74S2m" value="VANA" />
  </node>
  <node concept="7hO8L" id="2FjKBCR26E8">
    <property role="3GE5qa" value="microbiology.secondaryWork" />
    <property role="TrG5h" value="Vanomycin test B" />
    <property role="74S2m" value="VANB" />
  </node>
  <node concept="7hO8L" id="2FjKBCR26E9">
    <property role="3GE5qa" value="microbiology.secondaryWork" />
    <property role="TrG5h" value="Mec" />
    <property role="74S2m" value="MEC" />
  </node>
  <node concept="7hO8L" id="2FjKBCR26Ea">
    <property role="3GE5qa" value="microbiology.secondaryWork" />
    <property role="TrG5h" value="Nuc" />
    <property role="74S2m" value="NUC" />
  </node>
  <node concept="74Z9O" id="2FjKBCR2Cx_">
    <property role="3GE5qa" value="microbiology.pathogens" />
    <property role="TrG5h" value="Escherichia coli" />
    <property role="74S2m" value="PA11" />
  </node>
  <node concept="74Z9O" id="2FjKBCR2CxK">
    <property role="3GE5qa" value="microbiology.pathogens" />
    <property role="TrG5h" value="Enterococcus faecium" />
    <property role="74S2m" value="PA012" />
  </node>
</model>

