<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:8d0b2415-f96b-488e-8165-0d18775b4b97(no.furst.mLab.regelsystem.rules)">
  <persistence version="9" />
  <languages>
    <use id="c610a4eb-c47c-4f95-b568-11236971769c" name="no.uio.mLab.decisions.core" version="0" />
    <use id="6b1bb23e-8155-434e-8611-255c4637394b" name="no.uio.mLab.decisions.data.microbiology" version="0" />
    <use id="f130674d-23fb-4e9b-a200-548653f5fd0b" name="no.uio.mLab.decisions.data.patient" version="0" />
    <use id="bdfbcabc-8ac0-41dc-aae3-092d151214a5" name="no.uio.mLab.decisions.tasks.autovalidation" version="0" />
    <use id="ebe710b8-8f46-4b5e-8fb1-221de61f9e7c" name="no.uio.mLab.decisions.tasks.autocommentation" version="0" />
    <use id="598e47df-5b3d-4e8c-94a1-69f953d4de82" name="no.furst.mLab.decisions.tasks.autocommentation" version="0" />
    <use id="76146113-297e-4cbe-99cf-714ae175bb83" name="no.uio.mLab.decisions.references.biochemistry" version="0" />
    <use id="60f87c04-0cd8-4174-8d1a-4a254a9e2f58" name="no.uio.mLab.decisions.data.laboratoryTest.textualResult" version="0" />
    <use id="93a27952-3baa-41ca-9e72-e2a9cf1b3f77" name="no.uio.mLab.decisions.data.laboratoryTest" version="0" />
    <use id="72a150d4-97f8-489b-aded-022e7dabd8b9" name="no.uio.mLab.decisions.data.laboratoryTest.numberResult" version="0" />
    <use id="8fc9d2a5-9169-4fbe-a02d-1c7f83e86b8e" name="no.furst.mLab.decisions.references" version="0" />
    <use id="64b829cb-114b-4b36-813b-6c9497a3e006" name="no.uio.mLab.decisions.references.microbiology" version="0" />
    <use id="1b13d5a1-80e4-47ff-b773-4b4b3c9489d3" name="no.uio.mLab.decisions.tasks.secondaryWork" version="0" />
    <use id="4a652d55-3684-4d2d-98c9-2ef46f124c44" name="no.uio.mLab.decisions.references.laboratoryTest" version="0" />
  </languages>
  <imports>
    <import index="1iku" ref="r:4a15446b-18f6-473d-9989-8199dfed8381(no.furst.mLab.regelsystem.entities)" />
  </imports>
  <registry>
    <language id="4a652d55-3684-4d2d-98c9-2ef46f124c44" name="no.uio.mLab.decisions.references.laboratoryTest">
      <concept id="7282862830136191886" name="no.uio.mLab.decisions.references.laboratoryTest.structure.EntitySpecimenTypeReference" flags="ng" index="7e0qA" />
      <concept id="7011654996639830500" name="no.uio.mLab.decisions.references.laboratoryTest.structure.WildcardLaboratoryTestWithTextPattern" flags="ng" index="2j0jrP" />
      <concept id="5601053190799210672" name="no.uio.mLab.decisions.references.laboratoryTest.structure.LaboratoryTestVariable" flags="ng" index="307ky3" />
      <concept id="5601053190830321866" name="no.uio.mLab.decisions.references.laboratoryTest.structure.AspectLaboratoryTestReference" flags="ng" index="32e83T" />
      <concept id="5315700730317310621" name="no.uio.mLab.decisions.references.laboratoryTest.structure.AspectLaboratoryTestWithTextResultReference" flags="ng" index="3b72cV" />
      <concept id="5315700730317335622" name="no.uio.mLab.decisions.references.laboratoryTest.structure.LaboratoryTestWithTextResultVariable" flags="ng" index="3b74nw" />
      <concept id="7816353213385778781" name="no.uio.mLab.decisions.references.laboratoryTest.structure.WildcardLaboratoryTestPattern" flags="ng" index="3uFIm8" />
    </language>
    <language id="ebe710b8-8f46-4b5e-8fb1-221de61f9e7c" name="no.uio.mLab.decisions.tasks.autocommentation">
      <concept id="3086023999805932311" name="no.uio.mLab.decisions.tasks.autocommentation.structure.HasInterpretativeComment" flags="ng" index="u485H">
        <child id="3086023999805932312" name="comment" index="u485y" />
      </concept>
      <concept id="3086023999823672737" name="no.uio.mLab.decisions.tasks.autocommentation.structure.AspectInterpretativeCommentForRequestorReference" flags="ng" index="v0zvr" />
      <concept id="3086023999823363087" name="no.uio.mLab.decisions.tasks.autocommentation.structure.WildcardInterpretativeCommentPattern" flags="ng" index="v7JTP" />
      <concept id="3086023999823329342" name="no.uio.mLab.decisions.tasks.autocommentation.structure.InterpretativeCommentVariable" flags="ng" index="v7RD4" />
      <concept id="966389532319936384" name="no.uio.mLab.decisions.tasks.autocommentation.structure.TextInterpretativeCommentForRequestor" flags="ng" index="3pZa6C">
        <child id="966389532319936417" name="text" index="3pZa69" />
      </concept>
      <concept id="1560130832616184658" name="no.uio.mLab.decisions.tasks.autocommentation.structure.AddCommentForRequestor" flags="ng" index="3$BABF">
        <child id="1560130832616222764" name="comment" index="3$BWal" />
      </concept>
      <concept id="1560130832601897773" name="no.uio.mLab.decisions.tasks.autocommentation.structure.NotifySpecialist" flags="ng" index="3_Y6Ak">
        <child id="5601053190830339162" name="specialistReference" index="32e4hD" />
      </concept>
    </language>
    <language id="1b13d5a1-80e4-47ff-b773-4b4b3c9489d3" name="no.uio.mLab.decisions.tasks.secondaryWork">
      <concept id="7282862830139116429" name="no.uio.mLab.decisions.tasks.secondaryWork.structure.ScheduleSecondaryMicrobiologyWork" flags="ng" index="7hQq_">
        <child id="7282862830139193463" name="workReference" index="7h_lv" />
      </concept>
    </language>
    <language id="76146113-297e-4cbe-99cf-714ae175bb83" name="no.uio.mLab.decisions.references.biochemistry">
      <concept id="5675576922574079249" name="no.uio.mLab.decisions.references.biochemistry.structure.EntityBiochemistryTestWithNumberResultReference" flags="ng" index="2NFnoM" />
      <concept id="5601053190830327690" name="no.uio.mLab.decisions.references.biochemistry.structure.EntityBiochemistryTestReference" flags="ng" index="32e7uT" />
    </language>
    <language id="6b1bb23e-8155-434e-8611-255c4637394b" name="no.uio.mLab.decisions.data.microbiology">
      <concept id="7282862830136807995" name="no.uio.mLab.decisions.data.microbiology.structure.CultureFindingsIncludePathogen" flags="ng" index="78FWj">
        <child id="7282862830136816559" name="pathogenReference" index="78DU7" />
      </concept>
      <concept id="7282862830136242518" name="no.uio.mLab.decisions.data.microbiology.structure.FindingsFromAnyCultureTest" flags="ng" index="7ePLY">
        <child id="7282862830136409295" name="specimenTypeReference" index="7fd7B" />
      </concept>
      <concept id="7282862830138357897" name="no.uio.mLab.decisions.data.microbiology.structure.AntimicrobialSusceptibilityOfPathogenFromAnyCultureTestAndSpecimenType" flags="ng" index="7mLmx">
        <child id="7282862830138357898" name="pathogenReference" index="7mLmy" />
        <child id="7282862830145167372" name="antimicrobialReference" index="7CKO$" />
      </concept>
      <concept id="7282862830138631727" name="no.uio.mLab.decisions.data.microbiology.structure.ResistantAntimicrobialSensitivity" flags="ng" index="7nGG7" />
      <concept id="7282862830138631728" name="no.uio.mLab.decisions.data.microbiology.structure.SensitiveAntimicrobialSensitivity" flags="ng" index="7nGGo" />
      <concept id="7282862830138602360" name="no.uio.mLab.decisions.data.microbiology.structure.AntimicrobialSensitivityEqualTo" flags="ng" index="7nPTg">
        <child id="7282862830138636780" name="sensitivity" index="7nHj4" />
      </concept>
    </language>
    <language id="60f87c04-0cd8-4174-8d1a-4a254a9e2f58" name="no.uio.mLab.decisions.data.laboratoryTest.textualResult">
      <concept id="5315700730334196168" name="no.uio.mLab.decisions.data.laboratoryTest.textualResult.structure.ILaboratoryTestWithTextResultDataValue" flags="ng" index="3a7TxI">
        <child id="7816353213376658518" name="testReference" index="3udjY3" />
      </concept>
      <concept id="5315700730340222515" name="no.uio.mLab.decisions.data.laboratoryTest.textualResult.structure.LaboratoryTestResultAsText" flags="ng" index="3dWSYl" />
    </language>
    <language id="72a150d4-97f8-489b-aded-022e7dabd8b9" name="no.uio.mLab.decisions.data.laboratoryTest.numberResult">
      <concept id="5315700730334196168" name="no.uio.mLab.decisions.data.laboratoryTest.numberResult.structure.ILaboratoryTestWithNumberResultDataValue" flags="ng" index="3a7TxJ">
        <child id="7816353213376658518" name="testReference" index="3udjY4" />
      </concept>
      <concept id="5315700730340863187" name="no.uio.mLab.decisions.data.laboratoryTest.numberResult.structure.LaboratoryTestResultAsNumber" flags="ng" index="3dxklP" />
    </language>
    <language id="8fc9d2a5-9169-4fbe-a02d-1c7f83e86b8e" name="no.furst.mLab.decisions.references">
      <concept id="7282862830178442512" name="no.furst.mLab.decisions.references.structure.AspectCodedInterpretativeCommentForRequestorReference" flags="ng" index="1JR0S" />
      <concept id="7282862830178442514" name="no.furst.mLab.decisions.references.structure.CodedInterpretativeCommentForRequestorVariable" flags="ng" index="1JR0U" />
      <concept id="7282862830178429743" name="no.furst.mLab.decisions.references.structure.EntityCodedInterpretativeCommentForRequestorReference" flags="ng" index="1JSo7" />
      <concept id="5675576922574305301" name="no.furst.mLab.decisions.references.structure.EntityBiochemistryTestWithTextResultReference" flags="ng" index="2NGoGQ" />
      <concept id="7816353213404355252" name="no.furst.mLab.decisions.references.structure.WildcardCodedInterpretativeForRequestorComment" flags="ng" index="3vy__x" />
    </language>
    <language id="598e47df-5b3d-4e8c-94a1-69f953d4de82" name="no.furst.mLab.decisions.tasks.autocommentation">
      <concept id="8466382076844974554" name="no.furst.mLab.decisions.tasks.autocommentation.structure.AddStatisticalLabel" flags="ng" index="EqC1Y">
        <property id="8466382076845004819" name="label" index="EqzAR" />
      </concept>
      <concept id="1973009780647541893" name="no.furst.mLab.decisions.tasks.autocommentation.structure.CodedInterpretativeCommentForRequestor" flags="ng" index="MJVlO">
        <child id="3339892675599611828" name="commentReference" index="1bkD$5" />
      </concept>
    </language>
    <language id="c610a4eb-c47c-4f95-b568-11236971769c" name="no.uio.mLab.decisions.core">
      <concept id="7282862830177553026" name="no.uio.mLab.decisions.core.structure.ContainingActionPointcut" flags="ng" index="1GeuE">
        <child id="7282862830177553028" name="patterns" index="1GeuG" />
      </concept>
      <concept id="7282862830152808304" name="no.uio.mLab.decisions.core.structure.AddConjunctivePreConditionChange" flags="ng" index="6dDDo">
        <child id="7282862830152808305" name="condition" index="6dDDp" />
      </concept>
      <concept id="3086023999827488950" name="no.uio.mLab.decisions.core.structure.CompositePointcut" flags="ng" index="sRZbc">
        <child id="3086023999827811539" name="pointcuts" index="sKHUD" />
      </concept>
      <concept id="3086023999805918204" name="no.uio.mLab.decisions.core.structure.AddDisjunctivePreConditionChange" flags="ng" index="u4cA6">
        <child id="3086023999805918205" name="condition" index="u4cA7" />
      </concept>
      <concept id="3086023999804488539" name="no.uio.mLab.decisions.core.structure.ContainingBaseParameterPointcut" flags="ng" index="uvJ$x">
        <child id="3086023999804488540" name="patterns" index="uvJ$A" />
      </concept>
      <concept id="3086023999820563997" name="no.uio.mLab.decisions.core.structure.AllOfPointcut" flags="ng" index="vskhB" />
      <concept id="5601053190799218360" name="no.uio.mLab.decisions.core.structure.AspectVariable" flags="ng" index="307iqb">
        <child id="7816353213394532844" name="pattern" index="3v97CT" />
      </concept>
      <concept id="1095804092882054890" name="no.uio.mLab.decisions.core.structure.Aspect" flags="ng" index="1c$BFI">
        <child id="1095804092882054893" name="advices" index="1c$BFD" />
      </concept>
      <concept id="1983855924360890576" name="no.uio.mLab.decisions.core.structure.AspectOrdering" flags="ng" index="3iBtEz">
        <child id="1983855924360893862" name="aspects" index="3iBsvl" />
      </concept>
      <concept id="1983855924360890589" name="no.uio.mLab.decisions.core.structure.AspectReference" flags="ng" index="3iBtEI">
        <reference id="1983855924360890590" name="target" index="3iBtEH" />
      </concept>
      <concept id="966389532314249755" name="no.uio.mLab.decisions.core.structure.IRangeConstraint" flags="ng" index="3plAgN">
        <property id="966389532314258796" name="isLowerOpen" index="3plwd4" />
        <property id="966389532314258798" name="isUpperOpen" index="3plwd6" />
        <child id="966389532314249756" name="lower" index="3plAgO" />
        <child id="966389532314249758" name="upper" index="3plAgQ" />
      </concept>
      <concept id="7816353213379876489" name="no.uio.mLab.decisions.core.structure.IEntityReference" flags="ng" index="3u1dls">
        <reference id="5601053190829909215" name="target" index="32LHjG" />
      </concept>
      <concept id="7816353213381703965" name="no.uio.mLab.decisions.core.structure.IAspectVariableReference" flags="ng" index="3uS3F8">
        <reference id="7816353213381836899" name="target" index="3uSGeQ" />
      </concept>
      <concept id="6849753176702132154" name="no.uio.mLab.decisions.core.structure.NumberInRange" flags="ng" index="1vo2A3" />
      <concept id="6849753176702111585" name="no.uio.mLab.decisions.core.structure.NumberLessThan" flags="ng" index="1vov_o" />
      <concept id="6849753176702111658" name="no.uio.mLab.decisions.core.structure.NumberGreaterThan" flags="ng" index="1vovAj" />
      <concept id="6849753176702111662" name="no.uio.mLab.decisions.core.structure.NumberGreaterThanOrEqualTo" flags="ng" index="1vovAn" />
      <concept id="6849753176701719726" name="no.uio.mLab.decisions.core.structure.NumberLiteral" flags="ng" index="1vpYan">
        <property id="6849753176701719727" name="value" index="1vpYam" />
      </concept>
      <concept id="6849753176701235152" name="no.uio.mLab.decisions.core.structure.Rule" flags="ng" index="1vr_BD">
        <property id="1560130832591575186" name="description" index="3UpYCF" />
        <child id="6849753176701243444" name="condition" index="1vrEod" />
        <child id="1560130832615077684" name="actions" index="3$wkmd" />
      </concept>
      <concept id="6849753176701235151" name="no.uio.mLab.decisions.core.structure.RuleSet" flags="ng" index="1vr_BQ">
        <property id="3418641531607173910" name="description" index="mDFXa" />
        <child id="6849753176701236870" name="rules" index="1vr$2Z" />
        <child id="1560130832582752996" name="namedConditions" index="3UB0xt" />
      </concept>
      <concept id="6849753176701243363" name="no.uio.mLab.decisions.core.structure.CompositeCondition" flags="ng" index="1vrFBq">
        <child id="6849753176701243364" name="conditions" index="1vrFBt" />
      </concept>
      <concept id="6849753176701243367" name="no.uio.mLab.decisions.core.structure.AllOf" flags="ng" index="1vrFBu" />
      <concept id="6849753176701243366" name="no.uio.mLab.decisions.core.structure.AnyOf" flags="ng" index="1vrFBv" />
      <concept id="6849753176703006896" name="no.uio.mLab.decisions.core.structure.TextLiteral" flags="ng" index="1vsOU9">
        <property id="6849753176703006897" name="value" index="1vsOU8" />
      </concept>
      <concept id="1560130832603549820" name="no.uio.mLab.decisions.core.structure.SimpleCondition" flags="ng" index="3_REb5">
        <child id="5601053190790161830" name="data" index="30DPml" />
      </concept>
      <concept id="1560130832601083413" name="no.uio.mLab.decisions.core.structure.TextEqualTo" flags="ng" index="3_XdMG">
        <child id="1560130832601083414" name="text" index="3_XdMJ" />
      </concept>
      <concept id="730062693793755051" name="no.uio.mLab.decisions.core.structure.AddRuleChange" flags="ng" index="3HtHId">
        <child id="730062693793755103" name="rule" index="3HtHJT" />
      </concept>
      <concept id="730062693769239680" name="no.uio.mLab.decisions.core.structure.Advice" flags="ng" index="3JKeyA">
        <child id="7282862830152615132" name="effect" index="6dmBO" />
        <child id="7282862830149833946" name="pointcut" index="7UZBM" />
        <child id="3086023999802250468" name="parameters" index="umdqu" />
      </concept>
      <concept id="1560130832588438362" name="no.uio.mLab.decisions.core.structure.TimeSpanLessThanOrEqualTo" flags="ng" index="3UdWBz" />
      <concept id="1560130832594003024" name="no.uio.mLab.decisions.core.structure.Not" flags="ng" index="3UgdrD">
        <child id="1560130832594003025" name="condition" index="3UgdrC" />
      </concept>
      <concept id="1560130832591788817" name="no.uio.mLab.decisions.core.structure.NamedConditionReference" flags="ng" index="3UoyAC">
        <reference id="1560130832591788818" name="target" index="3UoyAF" />
      </concept>
      <concept id="1560130832593535525" name="no.uio.mLab.decisions.core.structure.BooleanDataValueCondition" flags="ng" index="3Uuv2s" />
      <concept id="1560130832593535526" name="no.uio.mLab.decisions.core.structure.NonBooleanDataValueCondition" flags="ng" index="3Uuv2v">
        <child id="1560130832602720002" name="constraint" index="3_NtmV" />
      </concept>
      <concept id="1560130832581330494" name="no.uio.mLab.decisions.core.structure.AtomicNumberConstraint" flags="ng" index="3UwVi7">
        <child id="1560130832580688999" name="number" index="3UJoVu" />
      </concept>
      <concept id="1560130832582716909" name="no.uio.mLab.decisions.core.structure.NamedCondition" flags="ng" index="3UB9Xk">
        <child id="1560130832582716912" name="condition" index="3UB9X9" />
      </concept>
      <concept id="1560130832584811404" name="no.uio.mLab.decisions.core.structure.AtomicTimeSpanConstraint" flags="ng" index="3UZ94P">
        <child id="1560130832584811405" name="timeSpan" index="3UZ94O" />
      </concept>
      <concept id="1560130832584684261" name="no.uio.mLab.decisions.core.structure.Week" flags="ng" index="3UZC1s" />
      <concept id="1560130832584684260" name="no.uio.mLab.decisions.core.structure.TimeSpanLiteral" flags="ng" index="3UZC1t">
        <property id="1560130832584684291" name="value" index="3UZC6U" />
      </concept>
    </language>
    <language id="64b829cb-114b-4b36-813b-6c9497a3e006" name="no.uio.mLab.decisions.references.microbiology">
      <concept id="7282862830136170725" name="no.uio.mLab.decisions.references.microbiology.structure.EntityPathogenReference" flags="ng" index="7e7nd" />
      <concept id="7282862830139162692" name="no.uio.mLab.decisions.references.microbiology.structure.EntitySecondaryMicrobiologyWorkReference" flags="ng" index="7hEPG" />
      <concept id="7282862830145141145" name="no.uio.mLab.decisions.references.microbiology.structure.EntityAntimicrobialReference" flags="ng" index="7CTiL" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
      <concept id="3717301156197626279" name="jetbrains.mps.lang.core.structure.BasePlaceholder" flags="ng" index="3DQ70j" />
      <concept id="709746936026466394" name="jetbrains.mps.lang.core.structure.ChildAttribute" flags="ng" index="3VBwX9">
        <property id="709746936026609031" name="linkId" index="3V$3ak" />
        <property id="709746936026609029" name="linkRole" index="3V$3am" />
      </concept>
    </language>
    <language id="93a27952-3baa-41ca-9e72-e2a9cf1b3f77" name="no.uio.mLab.decisions.data.laboratoryTest">
      <concept id="7282862830137337699" name="no.uio.mLab.decisions.data.laboratoryTest.structure.LaboratoryTestHasRequest" flags="ng" index="7aCDb" />
      <concept id="5315700730334120879" name="no.uio.mLab.decisions.data.laboratoryTest.structure.LaboratoryTestResultIsValid" flags="ng" index="3a7a89" />
      <concept id="5315700730334120876" name="no.uio.mLab.decisions.data.laboratoryTest.structure.LaboratoryTestPreviousResultIsValid" flags="ng" index="3a7a8a" />
      <concept id="5315700730334196168" name="no.uio.mLab.decisions.data.laboratoryTest.structure.ILaboratoryTestDataValue" flags="ng" index="3a7TxK">
        <child id="7816353213376658518" name="testReference" index="3udjY5" />
      </concept>
      <concept id="5315700730339920836" name="no.uio.mLab.decisions.data.laboratoryTest.structure.LaboratoryTestTimeSinceLastRequest" flags="ng" index="3dXM9y" />
      <concept id="966389532307592809" name="no.uio.mLab.decisions.data.laboratoryTest.structure.LaboratoryTestHasBeenPerformed" flags="ng" index="3uGdx1" />
    </language>
    <language id="0e664af0-ccb5-427d-94ac-6f90ac2b6424" name="no.uio.mLab.decisions.references.specialistType">
      <concept id="5601053190830324301" name="no.uio.mLab.decisions.references.specialistType.structure.EntitySpecialistTypeReference" flags="ng" index="32e8DY" />
    </language>
  </registry>
  <node concept="1vr_BQ" id="5Wfdz$0qifV">
    <property role="TrG5h" value="ExampleTask1" />
    <property role="3GE5qa" value="ruleSets" />
    <property role="mDFXa" value="Checking for Drug Abuse" />
    <node concept="1vr_BD" id="1mAGFBJRO8x" role="1vr$2Z">
      <property role="TrG5h" value="Rule 1" />
      <node concept="1vrFBu" id="1Hxyv4F98LU" role="1vrEod">
        <node concept="3Uuv2v" id="6LTgXmMPRpb" role="1vrFBt">
          <node concept="1vovAj" id="6LTgXmMPRpL" role="3_NtmV">
            <node concept="1vpYan" id="6LTgXmMPRpP" role="3UJoVu">
              <property role="1vpYam" value="0.19" />
            </node>
          </node>
          <node concept="3dxklP" id="6PcFLt2m8gY" role="30DPml">
            <node concept="2NFnoM" id="6PcFLt2m8h2" role="3udjY4">
              <ref role="32LHjG" to="1iku:1mAGFBLaKts" resolve="567 S-Etanol" />
            </node>
          </node>
        </node>
        <node concept="3Uuv2v" id="6LTgXmMPRqB" role="1vrFBt">
          <node concept="1vovAn" id="6LTgXmMPRrd" role="3_NtmV">
            <node concept="1vpYan" id="6LTgXmMPRrh" role="3UJoVu">
              <property role="1vpYam" value="0.30" />
            </node>
          </node>
          <node concept="3dxklP" id="6PcFLt2m8gU" role="30DPml">
            <node concept="2NFnoM" id="6PcFLt2m8gV" role="3udjY4">
              <ref role="32LHjG" to="1iku:1mAGFBLaKtq" resolve="A58 PEth" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3$BABF" id="6khVix$FMnA" role="3$wkmd">
        <node concept="MJVlO" id="4B5aqq20kTW" role="3$BWal">
          <node concept="1JSo7" id="6LTgXmMPRrC" role="1bkD$5">
            <ref role="32LHjG" to="1iku:1mAGFBLG8R6" resolve="AA5851" />
          </node>
        </node>
      </node>
      <node concept="EqC1Y" id="7lYCqhv5loZ" role="3$wkmd">
        <property role="EqzAR" value="CDT001" />
      </node>
    </node>
    <node concept="1vr_BD" id="1mAGFBKco8Y" role="1vr$2Z">
      <property role="TrG5h" value="Rule 2" />
      <node concept="EqC1Y" id="7lYCqhv5lpl" role="3$wkmd">
        <property role="EqzAR" value="CDT002" />
      </node>
      <node concept="1vrFBu" id="1mAGFBKcoaY" role="1vrEod">
        <node concept="3UgdrD" id="1mAGFBKcob3" role="1vrFBt">
          <node concept="3UoyAC" id="1mAGFBKcob5" role="3UgdrC">
            <ref role="3UoyAF" node="1mAGFBKafjd" resolve="CDT and PEth is tested before" />
          </node>
        </node>
        <node concept="3UgdrD" id="1mAGFBKcoCD" role="1vrFBt">
          <node concept="3UoyAC" id="1mAGFBKcoCE" role="3UgdrC">
            <ref role="3UoyAF" node="1mAGFBKcog8" resolve="PEth is tested now" />
          </node>
        </node>
        <node concept="3UoyAC" id="4QUW3efuO70" role="1vrFBt">
          <ref role="3UoyAF" node="1mAGFBKcoj$" resolve="CDT is tested now" />
        </node>
        <node concept="3UoyAC" id="1mAGFBKnGx9" role="1vrFBt">
          <ref role="3UoyAF" node="1mAGFBK9_QP" resolve="CDT is negative" />
        </node>
      </node>
    </node>
    <node concept="1vr_BD" id="1mAGFBKcoc3" role="1vr$2Z">
      <property role="TrG5h" value="Rule 3" />
      <node concept="1vrFBu" id="1mAGFBKcocO" role="1vrEod">
        <node concept="3UgdrD" id="1mAGFBKcoD8" role="1vrFBt">
          <node concept="3UoyAC" id="1mAGFBKcoD9" role="3UgdrC">
            <ref role="3UoyAF" node="1mAGFBKafjd" resolve="CDT and PEth is tested before" />
          </node>
        </node>
        <node concept="3UgdrD" id="1mAGFBKcoDa" role="1vrFBt">
          <node concept="3UoyAC" id="1mAGFBKcoDb" role="3UgdrC">
            <ref role="3UoyAF" node="1mAGFBKcog8" resolve="PEth is tested now" />
          </node>
        </node>
        <node concept="3UoyAC" id="4QUW3efuO7f" role="1vrFBt">
          <ref role="3UoyAF" node="1mAGFBKcoj$" resolve="CDT is tested now" />
        </node>
        <node concept="3UoyAC" id="1mAGFBKnGxg" role="1vrFBt">
          <ref role="3UoyAF" node="1mAGFBK9_Rv" resolve="CDT is low positive" />
        </node>
      </node>
      <node concept="3$BABF" id="4B5aqq20kUa" role="3$wkmd">
        <node concept="MJVlO" id="4B5aqq20kUi" role="3$BWal">
          <node concept="1JSo7" id="6LTgXmMPRrO" role="1bkD$5">
            <ref role="32LHjG" to="1iku:1mAGFBLG8R7" resolve="A65811" />
          </node>
        </node>
      </node>
      <node concept="EqC1Y" id="7lYCqhv5lps" role="3$wkmd">
        <property role="EqzAR" value="CDT003" />
      </node>
    </node>
    <node concept="1vr_BD" id="1mAGFBKahnw" role="1vr$2Z">
      <property role="TrG5h" value="Rule 31" />
      <node concept="1vrFBu" id="1mAGFBKahnI" role="1vrEod">
        <node concept="3UoyAC" id="1mAGFBKahnK" role="1vrFBt">
          <ref role="3UoyAF" node="1mAGFBKafjd" resolve="CDT and PEth is tested before" />
        </node>
        <node concept="3UoyAC" id="1mAGFBKco7S" role="1vrFBt">
          <ref role="3UoyAF" node="1mAGFBKco6f" resolve="outside reference range" />
        </node>
      </node>
      <node concept="3_Y6Ak" id="4QUW3efWYHq" role="3$wkmd">
        <node concept="32e8DY" id="6LTgXmMPRrG" role="32e4hD">
          <ref role="32LHjG" to="1iku:1mAGFBLdDxo" resolve="CDT" />
        </node>
      </node>
      <node concept="EqC1Y" id="7lYCqhv5lp5" role="3$wkmd">
        <property role="EqzAR" value="CDT031" />
      </node>
    </node>
    <node concept="1vr_BD" id="1mAGFBKco2y" role="1vr$2Z">
      <property role="TrG5h" value="Rule 32" />
      <node concept="EqC1Y" id="7lYCqhv5lp9" role="3$wkmd">
        <property role="EqzAR" value="CDT032" />
      </node>
      <node concept="1vrFBu" id="1mAGFBKco36" role="1vrEod">
        <node concept="3UoyAC" id="1mAGFBKco37" role="1vrFBt">
          <ref role="3UoyAF" node="1mAGFBKafjd" resolve="CDT and PEth is tested before" />
        </node>
        <node concept="3UgdrD" id="1mAGFBKco80" role="1vrFBt">
          <node concept="3UoyAC" id="1mAGFBKco85" role="3UgdrC">
            <ref role="3UoyAF" node="1mAGFBKco6f" resolve="outside reference range" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1vr_BD" id="1mAGFBKahnM" role="1vr$2Z">
      <property role="TrG5h" value="Rule 0" />
      <node concept="1vrFBu" id="1mAGFBKco8f" role="1vrEod">
        <node concept="3UgdrD" id="1mAGFBKco8L" role="1vrFBt">
          <node concept="3UoyAC" id="1mAGFBKco8M" role="3UgdrC">
            <ref role="3UoyAF" node="1mAGFBKafjd" resolve="CDT and PEth is tested before" />
          </node>
        </node>
        <node concept="1vrFBv" id="1mAGFBKcoBr" role="1vrFBt">
          <node concept="1vrFBu" id="4QUW3efuO5X" role="1vrFBt">
            <node concept="3UoyAC" id="4QUW3efuO5$" role="1vrFBt">
              <ref role="3UoyAF" node="1mAGFBKcog8" resolve="PEth is tested now" />
            </node>
            <node concept="3UoyAC" id="1mAGFBKcoBB" role="1vrFBt">
              <ref role="3UoyAF" node="1mAGFBKcnOR" resolve="PEth is unknown" />
            </node>
          </node>
          <node concept="1vrFBu" id="4QUW3efuO6t" role="1vrFBt">
            <node concept="3UoyAC" id="4QUW3efuO6B" role="1vrFBt">
              <ref role="3UoyAF" node="1mAGFBKcoj$" resolve="CDT is tested now" />
            </node>
            <node concept="3UoyAC" id="1mAGFBKcoBV" role="1vrFBt">
              <ref role="3UoyAF" node="1mAGFBKcnXg" resolve="CDT is unknown" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3_Y6Ak" id="1Hxyv4DSvgy" role="3$wkmd">
        <node concept="32e8DY" id="6LTgXmMPRrK" role="32e4hD">
          <ref role="32LHjG" to="1iku:1mAGFBLdDxo" resolve="CDT" />
        </node>
      </node>
      <node concept="EqC1Y" id="7lYCqhv5lpf" role="3$wkmd">
        <property role="EqzAR" value="CDT000" />
      </node>
    </node>
    <node concept="1vr_BD" id="1mAGFBKco9G" role="1vr$2Z">
      <property role="TrG5h" value="Rule 33" />
      <node concept="EqC1Y" id="7lYCqhv5lpj" role="3$wkmd">
        <property role="EqzAR" value="CDT033" />
      </node>
      <node concept="1vrFBu" id="1mAGFBKcoai" role="1vrEod">
        <node concept="3UgdrD" id="1Hxyv4F99c3" role="1vrFBt">
          <node concept="3UoyAC" id="1Hxyv4F99cb" role="3UgdrC">
            <ref role="3UoyAF" node="1mAGFBKafjd" resolve="CDT and PEth is tested before" />
          </node>
        </node>
        <node concept="3UgdrD" id="1mAGFBKcoB8" role="1vrFBt">
          <node concept="3UoyAC" id="1mAGFBKcoBe" role="3UgdrC">
            <ref role="3UoyAF" node="1mAGFBKcog8" resolve="PEth is tested now" />
          </node>
        </node>
        <node concept="3UgdrD" id="1mAGFBKcoAX" role="1vrFBt">
          <node concept="3UoyAC" id="1mAGFBKcoB1" role="3UgdrC">
            <ref role="3UoyAF" node="1mAGFBKcoj$" resolve="CDT is tested now" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1vr_BD" id="1mAGFBKnGxn" role="1vr$2Z">
      <property role="TrG5h" value="Rule 4" />
      <node concept="1vrFBu" id="1mAGFBKnGyd" role="1vrEod">
        <node concept="3UgdrD" id="6LTgXmMdk$l" role="1vrFBt">
          <node concept="3UoyAC" id="6LTgXmMdk$m" role="3UgdrC">
            <ref role="3UoyAF" node="1mAGFBKafjd" resolve="CDT and PEth is tested before" />
          </node>
        </node>
        <node concept="3UgdrD" id="1mAGFBKnGys" role="1vrFBt">
          <node concept="3UoyAC" id="1mAGFBKnGyy" role="3UgdrC">
            <ref role="3UoyAF" node="1mAGFBKcog8" resolve="PEth is tested now" />
          </node>
        </node>
        <node concept="3UoyAC" id="4QUW3efuO7u" role="1vrFBt">
          <ref role="3UoyAF" node="1mAGFBKcoj$" resolve="CDT is tested now" />
        </node>
        <node concept="3UoyAC" id="1mAGFBKnGyF" role="1vrFBt">
          <ref role="3UoyAF" node="1mAGFBKcnZF" resolve="CDT is positive" />
        </node>
      </node>
      <node concept="3$BABF" id="4B5aqq20kUw" role="3$wkmd">
        <node concept="MJVlO" id="4B5aqq20kUC" role="3$BWal">
          <node concept="1JSo7" id="6LTgXmMPRrS" role="1bkD$5">
            <ref role="32LHjG" to="1iku:1mAGFBLG8R8" resolve="A65812" />
          </node>
        </node>
      </node>
      <node concept="EqC1Y" id="7lYCqhv5lpA" role="3$wkmd">
        <property role="EqzAR" value="CDT004" />
      </node>
    </node>
    <node concept="1vr_BD" id="1mAGFBKnGyM" role="1vr$2Z">
      <property role="TrG5h" value="Rule 5" />
      <node concept="1vrFBu" id="1mAGFBKnGzJ" role="1vrEod">
        <node concept="3UgdrD" id="1mAGFBKnGzK" role="1vrFBt">
          <node concept="3UoyAC" id="1mAGFBKnGzL" role="3UgdrC">
            <ref role="3UoyAF" node="1mAGFBKafjd" resolve="CDT and PEth is tested before" />
          </node>
        </node>
        <node concept="3UgdrD" id="1mAGFBKnGzM" role="1vrFBt">
          <node concept="3UoyAC" id="1mAGFBKnGzN" role="3UgdrC">
            <ref role="3UoyAF" node="1mAGFBKcog8" resolve="PEth is tested now" />
          </node>
        </node>
        <node concept="3UoyAC" id="4QUW3efuO7H" role="1vrFBt">
          <ref role="3UoyAF" node="1mAGFBKcoj$" resolve="CDT is tested now" />
        </node>
        <node concept="3UoyAC" id="1mAGFBKnGzV" role="1vrFBt">
          <ref role="3UoyAF" node="1mAGFBKafhC" resolve="CDT is high positive" />
        </node>
      </node>
      <node concept="3$BABF" id="4B5aqq20kUQ" role="3$wkmd">
        <node concept="MJVlO" id="4B5aqq20kUY" role="3$BWal">
          <node concept="1JSo7" id="6LTgXmMPRrW" role="1bkD$5">
            <ref role="32LHjG" to="1iku:1mAGFBLG8R9" resolve="A65813" />
          </node>
        </node>
      </node>
      <node concept="EqC1Y" id="7lYCqhv5lpK" role="3$wkmd">
        <property role="EqzAR" value="CDT005" />
      </node>
    </node>
    <node concept="1vr_BD" id="1mAGFBKnG_m" role="1vr$2Z">
      <property role="TrG5h" value="Rule 6" />
      <node concept="EqC1Y" id="7lYCqhv5lpP" role="3$wkmd">
        <property role="EqzAR" value="CDT006" />
      </node>
      <node concept="1vrFBu" id="1mAGFBKnGAq" role="1vrEod">
        <node concept="3UgdrD" id="1mAGFBKnGAu" role="1vrFBt">
          <node concept="3UoyAC" id="1mAGFBKnGAy" role="3UgdrC">
            <ref role="3UoyAF" node="1mAGFBKafjd" resolve="CDT and PEth is tested before" />
          </node>
        </node>
        <node concept="3UoyAC" id="1mAGFBKnGAD" role="1vrFBt">
          <ref role="3UoyAF" node="1mAGFBKcog8" resolve="PEth is tested now" />
        </node>
        <node concept="3UoyAC" id="7lYCqhv5O4P" role="1vrFBt">
          <ref role="3UoyAF" node="1mAGFBK6IZV" resolve="PEth is negative" />
        </node>
        <node concept="3UgdrD" id="1mAGFBKnGAT" role="1vrFBt">
          <node concept="3UoyAC" id="1mAGFBKnGAU" role="3UgdrC">
            <ref role="3UoyAF" node="1mAGFBKcoj$" resolve="CDT is tested now" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1vr_BD" id="1mAGFBKnGB0" role="1vr$2Z">
      <property role="TrG5h" value="Rule 7" />
      <node concept="EqC1Y" id="7lYCqhv5lpR" role="3$wkmd">
        <property role="EqzAR" value="CDT007" />
      </node>
      <node concept="1vrFBu" id="1mAGFBKnGCb" role="1vrEod">
        <node concept="3UgdrD" id="1mAGFBKnGCc" role="1vrFBt">
          <node concept="3UoyAC" id="1mAGFBKnGCd" role="3UgdrC">
            <ref role="3UoyAF" node="1mAGFBKafjd" resolve="CDT and PEth is tested before" />
          </node>
        </node>
        <node concept="3UoyAC" id="1mAGFBKnGCe" role="1vrFBt">
          <ref role="3UoyAF" node="1mAGFBKcog8" resolve="PEth is tested now" />
        </node>
        <node concept="3UoyAC" id="7lYCqhv5O55" role="1vrFBt">
          <ref role="3UoyAF" node="1mAGFBK6IZV" resolve="PEth is negative" />
        </node>
        <node concept="3UoyAC" id="4QUW3efuO8a" role="1vrFBt">
          <ref role="3UoyAF" node="1mAGFBKcoj$" resolve="CDT is tested now" />
        </node>
        <node concept="3UoyAC" id="1mAGFBKnGCv" role="1vrFBt">
          <ref role="3UoyAF" node="1mAGFBK9_QP" resolve="CDT is negative" />
        </node>
      </node>
    </node>
    <node concept="1vr_BD" id="1mAGFBKnGC_" role="1vr$2Z">
      <property role="TrG5h" value="Rule 8" />
      <node concept="3_Y6Ak" id="1Hxyv4DSvgU" role="3$wkmd">
        <node concept="32e8DY" id="6LTgXmMPRs0" role="32e4hD">
          <ref role="32LHjG" to="1iku:1mAGFBLdDxo" resolve="CDT" />
        </node>
      </node>
      <node concept="EqC1Y" id="7lYCqhv5lpX" role="3$wkmd">
        <property role="EqzAR" value="CDT008" />
      </node>
      <node concept="1vrFBu" id="1mAGFBKnGDQ" role="1vrEod">
        <node concept="3UgdrD" id="1mAGFBKnGDR" role="1vrFBt">
          <node concept="3UoyAC" id="1mAGFBKnGDS" role="3UgdrC">
            <ref role="3UoyAF" node="1mAGFBKafjd" resolve="CDT and PEth is tested before" />
          </node>
        </node>
        <node concept="3UoyAC" id="1mAGFBKnGDT" role="1vrFBt">
          <ref role="3UoyAF" node="1mAGFBKcog8" resolve="PEth is tested now" />
        </node>
        <node concept="3UoyAC" id="7lYCqhv5O5k" role="1vrFBt">
          <ref role="3UoyAF" node="1mAGFBK6IZV" resolve="PEth is negative" />
        </node>
        <node concept="3UoyAC" id="4QUW3efuO8n" role="1vrFBt">
          <ref role="3UoyAF" node="1mAGFBKcoj$" resolve="CDT is tested now" />
        </node>
        <node concept="3UoyAC" id="1mAGFBKnGE0" role="1vrFBt">
          <ref role="3UoyAF" node="1mAGFBK9_Rv" resolve="CDT is low positive" />
        </node>
      </node>
    </node>
    <node concept="1vr_BD" id="1mAGFBKnGE6" role="1vr$2Z">
      <property role="TrG5h" value="Rule 9" />
      <node concept="3_Y6Ak" id="1Hxyv4DSvgS" role="3$wkmd">
        <node concept="32e8DY" id="6LTgXmMPRs4" role="32e4hD">
          <ref role="32LHjG" to="1iku:1mAGFBLdDxo" resolve="CDT" />
        </node>
      </node>
      <node concept="EqC1Y" id="7lYCqhv5lq5" role="3$wkmd">
        <property role="EqzAR" value="CDT009" />
      </node>
      <node concept="1vrFBu" id="1mAGFBKnGFt" role="1vrEod">
        <node concept="3UgdrD" id="1mAGFBKnGFu" role="1vrFBt">
          <node concept="3UoyAC" id="1mAGFBKnGFv" role="3UgdrC">
            <ref role="3UoyAF" node="1mAGFBKafjd" resolve="CDT and PEth is tested before" />
          </node>
        </node>
        <node concept="3UoyAC" id="1mAGFBKnGFw" role="1vrFBt">
          <ref role="3UoyAF" node="1mAGFBKcog8" resolve="PEth is tested now" />
        </node>
        <node concept="3UoyAC" id="7lYCqhv5O5z" role="1vrFBt">
          <ref role="3UoyAF" node="1mAGFBK6IZV" resolve="PEth is negative" />
        </node>
        <node concept="3UoyAC" id="4QUW3efuO8$" role="1vrFBt">
          <ref role="3UoyAF" node="1mAGFBKcoj$" resolve="CDT is tested now" />
        </node>
        <node concept="3UoyAC" id="1mAGFBKnGFB" role="1vrFBt">
          <ref role="3UoyAF" node="1mAGFBKcnZF" resolve="CDT is positive" />
        </node>
      </node>
    </node>
    <node concept="1vr_BD" id="1mAGFBKnGFH" role="1vr$2Z">
      <property role="TrG5h" value="Rule 10" />
      <node concept="1vrFBu" id="1mAGFBKnGHa" role="1vrEod">
        <node concept="3UgdrD" id="1mAGFBKnGHb" role="1vrFBt">
          <node concept="3UoyAC" id="1mAGFBKnGHc" role="3UgdrC">
            <ref role="3UoyAF" node="1mAGFBKafjd" resolve="CDT and PEth is tested before" />
          </node>
        </node>
        <node concept="3UoyAC" id="1mAGFBKnGHd" role="1vrFBt">
          <ref role="3UoyAF" node="1mAGFBKcog8" resolve="PEth is tested now" />
        </node>
        <node concept="3UoyAC" id="7lYCqhv5O5M" role="1vrFBt">
          <ref role="3UoyAF" node="1mAGFBK6IZV" resolve="PEth is negative" />
        </node>
        <node concept="3UoyAC" id="4QUW3efuO8L" role="1vrFBt">
          <ref role="3UoyAF" node="1mAGFBKcoj$" resolve="CDT is tested now" />
        </node>
        <node concept="3UoyAC" id="1mAGFBKnGHk" role="1vrFBt">
          <ref role="3UoyAF" node="1mAGFBKafhC" resolve="CDT is high positive" />
        </node>
      </node>
      <node concept="3_Y6Ak" id="1Hxyv4DRZIZ" role="3$wkmd">
        <node concept="32e8DY" id="6LTgXmMPRs8" role="32e4hD">
          <ref role="32LHjG" to="1iku:1mAGFBLdDxo" resolve="CDT" />
        </node>
      </node>
      <node concept="EqC1Y" id="7lYCqhv5luN" role="3$wkmd">
        <property role="EqzAR" value="CDT010" />
      </node>
    </node>
    <node concept="1vr_BD" id="2XLt5KX3wmq" role="1vr$2Z">
      <property role="TrG5h" value="Rule 11" />
      <node concept="1vrFBu" id="2XLt5KX3wmr" role="1vrEod">
        <node concept="3UgdrD" id="2XLt5KX3wms" role="1vrFBt">
          <node concept="3UoyAC" id="2XLt5KX3wmt" role="3UgdrC">
            <ref role="3UoyAF" node="1mAGFBKafjd" resolve="CDT and PEth is tested before" />
          </node>
        </node>
        <node concept="3UoyAC" id="2XLt5KX3wmu" role="1vrFBt">
          <ref role="3UoyAF" node="1mAGFBKcog8" resolve="PEth is tested now" />
        </node>
        <node concept="3UoyAC" id="2XLt5KX3wp4" role="1vrFBt">
          <ref role="3UoyAF" node="1mAGFBK6J09" resolve="PEth is borderline" />
        </node>
        <node concept="3UgdrD" id="2XLt5KX3wpc" role="1vrFBt">
          <node concept="3UoyAC" id="2XLt5KX3wpd" role="3UgdrC">
            <ref role="3UoyAF" node="1mAGFBKcoj$" resolve="CDT is tested now" />
          </node>
        </node>
      </node>
      <node concept="3$BABF" id="4B5aqq20kVb" role="3$wkmd">
        <node concept="MJVlO" id="4B5aqq20kVi" role="3$BWal">
          <node concept="1JSo7" id="6LTgXmMPRsc" role="1bkD$5">
            <ref role="32LHjG" to="1iku:2XLt5KX3xG8" resolve="AA5801" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1vr_BD" id="2XLt5KX3wpr" role="1vr$2Z">
      <property role="TrG5h" value="Rule 12" />
      <node concept="1vrFBu" id="2XLt5KX3wps" role="1vrEod">
        <node concept="3UgdrD" id="2XLt5KX3wpt" role="1vrFBt">
          <node concept="3UoyAC" id="2XLt5KX3wpu" role="3UgdrC">
            <ref role="3UoyAF" node="1mAGFBKafjd" resolve="CDT and PEth is tested before" />
          </node>
        </node>
        <node concept="3UoyAC" id="2XLt5KX3wpv" role="1vrFBt">
          <ref role="3UoyAF" node="1mAGFBKcog8" resolve="PEth is tested now" />
        </node>
        <node concept="3UoyAC" id="2XLt5KX3wsg" role="1vrFBt">
          <ref role="3UoyAF" node="1mAGFBK6J09" resolve="PEth is borderline" />
        </node>
        <node concept="3UoyAC" id="2XLt5KX3wpx" role="1vrFBt">
          <ref role="3UoyAF" node="1mAGFBKcoj$" resolve="CDT is tested now" />
        </node>
        <node concept="3UoyAC" id="2XLt5KX3wso" role="1vrFBt">
          <ref role="3UoyAF" node="1mAGFBK9_QP" resolve="CDT is negative" />
        </node>
      </node>
      <node concept="3$BABF" id="4B5aqq20kVu" role="3$wkmd">
        <node concept="MJVlO" id="4B5aqq20kV_" role="3$BWal">
          <node concept="1JSo7" id="6LTgXmMPRsg" role="1bkD$5">
            <ref role="32LHjG" to="1iku:2XLt5KX3xG8" resolve="AA5801" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1vr_BD" id="2XLt5KX3xCU" role="1vr$2Z">
      <property role="TrG5h" value="Rule 13" />
      <node concept="1vrFBu" id="2XLt5KX3xCV" role="1vrEod">
        <node concept="3UgdrD" id="2XLt5KX3xCW" role="1vrFBt">
          <node concept="3UoyAC" id="2XLt5KX3xCX" role="3UgdrC">
            <ref role="3UoyAF" node="1mAGFBKafjd" resolve="CDT and PEth is tested before" />
          </node>
        </node>
        <node concept="3UoyAC" id="2XLt5KX3xCY" role="1vrFBt">
          <ref role="3UoyAF" node="1mAGFBKcog8" resolve="PEth is tested now" />
        </node>
        <node concept="3UoyAC" id="2XLt5KX3xFQ" role="1vrFBt">
          <ref role="3UoyAF" node="1mAGFBK6J09" resolve="PEth is borderline" />
        </node>
        <node concept="3UoyAC" id="2XLt5KX3xD0" role="1vrFBt">
          <ref role="3UoyAF" node="1mAGFBKcoj$" resolve="CDT is tested now" />
        </node>
        <node concept="3UoyAC" id="2XLt5KX3xFY" role="1vrFBt">
          <ref role="3UoyAF" node="1mAGFBK9_Rv" resolve="CDT is low positive" />
        </node>
      </node>
      <node concept="3$BABF" id="4B5aqq20kVO" role="3$wkmd">
        <node concept="MJVlO" id="4B5aqq20kVY" role="3$BWal">
          <node concept="1JSo7" id="6LTgXmMPRsk" role="1bkD$5">
            <ref role="32LHjG" to="1iku:1mAGFBLG8R7" resolve="A65811" />
          </node>
        </node>
      </node>
      <node concept="3$BABF" id="4B5aqq20kWd" role="3$wkmd">
        <node concept="MJVlO" id="4B5aqq20kWq" role="3$BWal">
          <node concept="1JSo7" id="6LTgXmMPRso" role="1bkD$5">
            <ref role="32LHjG" to="1iku:2XLt5KX3xG8" resolve="AA5801" />
          </node>
        </node>
      </node>
    </node>
    <node concept="3UB9Xk" id="1mAGFBKcog8" role="3UB0xt">
      <property role="TrG5h" value="PEth is tested now" />
      <node concept="1vrFBu" id="1mAGFBKethZ" role="3UB9X9">
        <node concept="3Uuv2s" id="6LTgXmMPR4Y" role="1vrFBt">
          <node concept="7aCDb" id="6LTgXmMPR4U" role="30DPml">
            <node concept="32e7uT" id="6PcFLt2m8dw" role="3udjY5">
              <ref role="32LHjG" to="1iku:1mAGFBLaKtq" resolve="A58 PEth" />
            </node>
          </node>
        </node>
        <node concept="3Uuv2s" id="6LTgXmMPR5I" role="1vrFBt">
          <node concept="3a7a89" id="6LTgXmMPR5E" role="30DPml">
            <node concept="32e7uT" id="6PcFLt2m8d_" role="3udjY5">
              <ref role="32LHjG" to="1iku:1mAGFBLaKtq" resolve="A58 PEth" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3UB9Xk" id="1mAGFBK6IZV" role="3UB0xt">
      <property role="TrG5h" value="PEth is negative" />
      <node concept="1vrFBv" id="1mAGFBKnnRR" role="3UB9X9">
        <node concept="3Uuv2v" id="6PcFLt2m8cL" role="1vrFBt">
          <node concept="3dWSYl" id="6PcFLt2m8cJ" role="30DPml">
            <node concept="2NGoGQ" id="6PcFLt2m8dj" role="3udjY3">
              <ref role="32LHjG" to="1iku:1mAGFBLaKtq" resolve="A58 PEth" />
            </node>
          </node>
          <node concept="3_XdMG" id="6LTgXmMPR7K" role="3_NtmV">
            <node concept="1vsOU9" id="6LTgXmMPR7N" role="3_XdMJ">
              <property role="1vsOU8" value="NEG" />
            </node>
          </node>
        </node>
        <node concept="3Uuv2v" id="6LTgXmMPR99" role="1vrFBt">
          <node concept="3_XdMG" id="4B5aqq3SZUI" role="3_NtmV">
            <node concept="1vsOU9" id="4B5aqq3SZUK" role="3_XdMJ">
              <property role="1vsOU8" value="0" />
            </node>
          </node>
          <node concept="3dWSYl" id="6PcFLt2m8dm" role="30DPml">
            <node concept="2NGoGQ" id="6PcFLt2m8dn" role="3udjY3">
              <ref role="32LHjG" to="1iku:1mAGFBLaKtq" resolve="A58 PEth" />
            </node>
          </node>
        </node>
        <node concept="3Uuv2v" id="6PcFLt2m8hh" role="1vrFBt">
          <node concept="3dxklP" id="6PcFLt2m8hf" role="30DPml">
            <node concept="2NFnoM" id="6PcFLt2m8hQ" role="3udjY4">
              <ref role="32LHjG" to="1iku:1mAGFBLaKtq" resolve="A58 PEth" />
            </node>
          </node>
          <node concept="1vo2A3" id="4B5aqq3SZWB" role="3_NtmV">
            <property role="3plwd6" value="true" />
            <node concept="1vpYan" id="PDjyzkxhHt" role="3plAgO">
              <property role="1vpYam" value="0.00" />
            </node>
            <node concept="1vpYan" id="PDjyzkxhHv" role="3plAgQ">
              <property role="1vpYam" value="0.03" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3UB9Xk" id="1mAGFBK6J09" role="3UB0xt">
      <property role="TrG5h" value="PEth is borderline" />
      <node concept="3Uuv2v" id="6PcFLt2m8dE" role="3UB9X9">
        <node concept="3dxklP" id="6PcFLt2m8dC" role="30DPml">
          <node concept="2NFnoM" id="6PcFLt2m8e5" role="3udjY4">
            <ref role="32LHjG" to="1iku:1mAGFBLaKtq" resolve="A58 PEth" />
          </node>
        </node>
        <node concept="1vo2A3" id="4V3GMfXxsME" role="3_NtmV">
          <property role="3plwd6" value="true" />
          <node concept="1vpYan" id="PDjyzkxhHC" role="3plAgO">
            <property role="1vpYam" value="0.03" />
          </node>
          <node concept="1vpYan" id="PDjyzkxhHE" role="3plAgQ">
            <property role="1vpYam" value="0.16" />
          </node>
        </node>
      </node>
    </node>
    <node concept="3UB9Xk" id="1mAGFBK7Sog" role="3UB0xt">
      <property role="TrG5h" value="PEth is low positive" />
      <node concept="3Uuv2v" id="6LTgXmMPRc0" role="3UB9X9">
        <node concept="1vo2A3" id="1mAGFBKOXqj" role="3_NtmV">
          <property role="3plwd6" value="true" />
          <node concept="1vpYan" id="PDjyzkxhHG" role="3plAgO">
            <property role="1vpYam" value="0.16" />
          </node>
          <node concept="1vpYan" id="PDjyzkxhHI" role="3plAgQ">
            <property role="1vpYam" value="0.31" />
          </node>
        </node>
        <node concept="3dxklP" id="6PcFLt2m8e8" role="30DPml">
          <node concept="2NFnoM" id="6PcFLt2m8e9" role="3udjY4">
            <ref role="32LHjG" to="1iku:1mAGFBLaKtq" resolve="A58 PEth" />
          </node>
        </node>
      </node>
    </node>
    <node concept="3UB9Xk" id="1mAGFBK7SoE" role="3UB0xt">
      <property role="TrG5h" value="PEth is positive" />
      <node concept="3Uuv2v" id="6LTgXmMPRcv" role="3UB9X9">
        <node concept="1vo2A3" id="1mAGFBKOXqA" role="3_NtmV">
          <property role="3plwd6" value="true" />
          <node concept="1vpYan" id="PDjyzkxhHK" role="3plAgO">
            <property role="1vpYam" value="0.31" />
          </node>
          <node concept="1vpYan" id="PDjyzkxhHM" role="3plAgQ">
            <property role="1vpYam" value="1.00" />
          </node>
        </node>
        <node concept="3dxklP" id="6PcFLt2m8ec" role="30DPml">
          <node concept="2NFnoM" id="6PcFLt2m8ed" role="3udjY4">
            <ref role="32LHjG" to="1iku:1mAGFBLaKtq" resolve="A58 PEth" />
          </node>
        </node>
      </node>
    </node>
    <node concept="3UB9Xk" id="1mAGFBK86iC" role="3UB0xt">
      <property role="TrG5h" value="PEth is high positive" />
      <node concept="3Uuv2v" id="6LTgXmMPRcY" role="3UB9X9">
        <node concept="1vovAj" id="1mAGFBKOXr2" role="3_NtmV">
          <node concept="1vpYan" id="1mAGFBKOXr4" role="3UJoVu">
            <property role="1vpYam" value="1.01" />
          </node>
        </node>
        <node concept="3dxklP" id="6PcFLt2m8eg" role="30DPml">
          <node concept="2NFnoM" id="6PcFLt2m8eh" role="3udjY4">
            <ref role="32LHjG" to="1iku:1mAGFBLaKtq" resolve="A58 PEth" />
          </node>
        </node>
      </node>
    </node>
    <node concept="3UB9Xk" id="1mAGFBKcnOR" role="3UB0xt">
      <property role="TrG5h" value="PEth is unknown" />
      <node concept="3UgdrD" id="1mAGFBKev_z" role="3UB9X9">
        <node concept="1vrFBv" id="1mAGFBKev_$" role="3UgdrC">
          <node concept="3UoyAC" id="2FjKBCQRhIS" role="1vrFBt">
            <ref role="3UoyAF" node="1mAGFBK6IZV" resolve="PEth is negative" />
          </node>
          <node concept="3UoyAC" id="1mAGFBKev_A" role="1vrFBt">
            <ref role="3UoyAF" node="1mAGFBK6J09" resolve="PEth is borderline" />
          </node>
          <node concept="3UoyAC" id="1mAGFBKev_B" role="1vrFBt">
            <ref role="3UoyAF" node="1mAGFBK7Sog" resolve="PEth is low positive" />
          </node>
          <node concept="3UoyAC" id="1mAGFBKev_C" role="1vrFBt">
            <ref role="3UoyAF" node="1mAGFBK7SoE" resolve="PEth is positive" />
          </node>
          <node concept="3UoyAC" id="1mAGFBKev_D" role="1vrFBt">
            <ref role="3UoyAF" node="1mAGFBK86iC" resolve="PEth is high positive" />
          </node>
        </node>
      </node>
    </node>
    <node concept="3DQ70j" id="1mAGFBK9_Qm" role="lGtFl">
      <property role="3V$3am" value="namedConditions" />
      <property role="3V$3ak" value="c610a4eb-c47c-4f95-b568-11236971769c/6849753176701235151/1560130832582752996" />
    </node>
    <node concept="3UB9Xk" id="1mAGFBKcoj$" role="3UB0xt">
      <property role="TrG5h" value="CDT is tested now" />
      <node concept="1vrFBu" id="1mAGFBKcolv" role="3UB9X9">
        <node concept="3Uuv2s" id="6LTgXmMPRdk" role="1vrFBt">
          <node concept="7aCDb" id="6LTgXmMPRdl" role="30DPml">
            <node concept="32e7uT" id="6PcFLt2m8em" role="3udjY5">
              <ref role="32LHjG" to="1iku:1mAGFBLaKtr" resolve="658 CDT" />
            </node>
          </node>
        </node>
        <node concept="3Uuv2s" id="6LTgXmMPRdn" role="1vrFBt">
          <node concept="3a7a89" id="6LTgXmMPRdo" role="30DPml">
            <node concept="32e7uT" id="6PcFLt2m8ep" role="3udjY5">
              <ref role="32LHjG" to="1iku:1mAGFBLaKtr" resolve="658 CDT" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3UB9Xk" id="1mAGFBK9_QP" role="3UB0xt">
      <property role="TrG5h" value="CDT is negative" />
      <node concept="1vrFBv" id="1mAGFBKkQw6" role="3UB9X9">
        <node concept="3Uuv2v" id="6PcFLt2m8et" role="1vrFBt">
          <node concept="3dWSYl" id="6PcFLt2m8er" role="30DPml">
            <node concept="2NGoGQ" id="6PcFLt2m8eT" role="3udjY3">
              <ref role="32LHjG" to="1iku:1mAGFBLaKtr" resolve="658 CDT" />
            </node>
          </node>
          <node concept="3_XdMG" id="6LTgXmMPRgj" role="3_NtmV">
            <node concept="1vsOU9" id="4V3GMfXNO15" role="3_XdMJ">
              <property role="1vsOU8" value="&lt;1.7" />
            </node>
          </node>
        </node>
        <node concept="3Uuv2v" id="6PcFLt2m8eY" role="1vrFBt">
          <node concept="3dxklP" id="6PcFLt2m8eW" role="30DPml">
            <node concept="2NFnoM" id="6PcFLt2m8fq" role="3udjY4">
              <ref role="32LHjG" to="1iku:1mAGFBLaKtr" resolve="658 CDT" />
            </node>
          </node>
          <node concept="1vov_o" id="4V3GMfXxsOI" role="3_NtmV">
            <node concept="1vpYan" id="4V3GMfXxsOK" role="3UJoVu">
              <property role="1vpYam" value="1.70" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3UB9Xk" id="1mAGFBK9_Rv" role="3UB0xt">
      <property role="TrG5h" value="CDT is low positive" />
      <node concept="3Uuv2v" id="6LTgXmMPRhM" role="3UB9X9">
        <node concept="1vo2A3" id="1mAGFBKOXtl" role="3_NtmV">
          <property role="3plwd4" value="true" />
          <property role="3plwd6" value="true" />
          <node concept="1vpYan" id="PDjyzkxhHO" role="3plAgO">
            <property role="1vpYam" value="1.70" />
          </node>
          <node concept="1vpYan" id="PDjyzkxhHQ" role="3plAgQ">
            <property role="1vpYam" value="2.20" />
          </node>
        </node>
        <node concept="3dxklP" id="6PcFLt2m8ft" role="30DPml">
          <node concept="2NFnoM" id="6PcFLt2m8fu" role="3udjY4">
            <ref role="32LHjG" to="1iku:1mAGFBLaKtr" resolve="658 CDT" />
          </node>
        </node>
      </node>
    </node>
    <node concept="3UB9Xk" id="1mAGFBKcnZF" role="3UB0xt">
      <property role="TrG5h" value="CDT is positive" />
      <node concept="3Uuv2v" id="6LTgXmMPRil" role="3UB9X9">
        <node concept="1vo2A3" id="1mAGFBKOXtC" role="3_NtmV">
          <property role="3plwd6" value="true" />
          <node concept="1vpYan" id="PDjyzkxhHS" role="3plAgO">
            <property role="1vpYam" value="2.20" />
          </node>
          <node concept="1vpYan" id="PDjyzkxhHU" role="3plAgQ">
            <property role="1vpYam" value="9.90" />
          </node>
        </node>
        <node concept="3dxklP" id="6PcFLt2m8fx" role="30DPml">
          <node concept="2NFnoM" id="6PcFLt2m8fy" role="3udjY4">
            <ref role="32LHjG" to="1iku:1mAGFBLaKtr" resolve="658 CDT" />
          </node>
        </node>
      </node>
    </node>
    <node concept="3UB9Xk" id="1mAGFBKafhC" role="3UB0xt">
      <property role="TrG5h" value="CDT is high positive" />
      <node concept="3Uuv2v" id="6LTgXmMPRiO" role="3UB9X9">
        <node concept="1vovAn" id="1mAGFBKOXtV" role="3_NtmV">
          <node concept="1vpYan" id="1mAGFBKOXtX" role="3UJoVu">
            <property role="1vpYam" value="9.90" />
          </node>
        </node>
        <node concept="3dxklP" id="6PcFLt2m8f_" role="30DPml">
          <node concept="2NFnoM" id="6PcFLt2m8fA" role="3udjY4">
            <ref role="32LHjG" to="1iku:1mAGFBLaKtr" resolve="658 CDT" />
          </node>
        </node>
      </node>
    </node>
    <node concept="3UB9Xk" id="1mAGFBKcnXg" role="3UB0xt">
      <property role="TrG5h" value="CDT is unknown" />
      <node concept="3UgdrD" id="1mAGFBKcnYZ" role="3UB9X9">
        <node concept="1vrFBv" id="1mAGFBKcnZ3" role="3UgdrC">
          <node concept="3UoyAC" id="1mAGFBKcnZ6" role="1vrFBt">
            <ref role="3UoyAF" node="1mAGFBK9_QP" resolve="CDT is negative" />
          </node>
          <node concept="3UoyAC" id="1mAGFBKcnZj" role="1vrFBt">
            <ref role="3UoyAF" node="1mAGFBK9_Rv" resolve="CDT is low positive" />
          </node>
          <node concept="3UoyAC" id="1mAGFBKco15" role="1vrFBt">
            <ref role="3UoyAF" node="1mAGFBKcnZF" resolve="CDT is positive" />
          </node>
          <node concept="3UoyAC" id="1mAGFBKcnZb" role="1vrFBt">
            <ref role="3UoyAF" node="1mAGFBKafhC" resolve="CDT is high positive" />
          </node>
        </node>
      </node>
    </node>
    <node concept="3DQ70j" id="1mAGFBKcnSZ" role="lGtFl">
      <property role="3V$3am" value="namedConditions" />
      <property role="3V$3ak" value="c610a4eb-c47c-4f95-b568-11236971769c/6849753176701235151/1560130832582752996" />
    </node>
    <node concept="3UB9Xk" id="1mAGFBKafjd" role="3UB0xt">
      <property role="TrG5h" value="CDT and PEth is tested before" />
      <node concept="1vrFBv" id="1mAGFBKafk0" role="3UB9X9">
        <node concept="1vrFBu" id="4B5aqq2B7yK" role="1vrFBt">
          <node concept="3Uuv2v" id="6LTgXmMPRjx" role="1vrFBt">
            <node concept="3UdWBz" id="6LTgXmMPRk3" role="3_NtmV">
              <node concept="3UZC1s" id="6LTgXmMPRk7" role="3UZ94O">
                <property role="3UZC6U" value="4" />
              </node>
            </node>
            <node concept="3dXM9y" id="6LTgXmMPRjt" role="30DPml">
              <node concept="32e7uT" id="6PcFLt2m8fD" role="3udjY5">
                <ref role="32LHjG" to="1iku:1mAGFBLaKtq" resolve="A58 PEth" />
              </node>
            </node>
          </node>
          <node concept="3Uuv2s" id="6LTgXmMPRkL" role="1vrFBt">
            <node concept="3a7a8a" id="6LTgXmMPRkH" role="30DPml">
              <node concept="32e7uT" id="6PcFLt2m8fG" role="3udjY5">
                <ref role="32LHjG" to="1iku:1mAGFBLaKtq" resolve="A58 PEth" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1vrFBu" id="4B5aqq2BabI" role="1vrFBt">
          <node concept="3Uuv2v" id="6LTgXmMPRls" role="1vrFBt">
            <node concept="3UdWBz" id="6LTgXmMPRlt" role="3_NtmV">
              <node concept="3UZC1s" id="6LTgXmMPRlu" role="3UZ94O">
                <property role="3UZC6U" value="4" />
              </node>
            </node>
            <node concept="3dXM9y" id="6LTgXmMPRlv" role="30DPml">
              <node concept="32e7uT" id="6PcFLt2m8fI" role="3udjY5">
                <ref role="32LHjG" to="1iku:1mAGFBLaKtr" resolve="658 CDT" />
              </node>
            </node>
          </node>
          <node concept="3Uuv2s" id="6LTgXmMPRlx" role="1vrFBt">
            <node concept="3a7a8a" id="6LTgXmMPRly" role="30DPml">
              <node concept="32e7uT" id="6PcFLt2m8fL" role="3udjY5">
                <ref role="32LHjG" to="1iku:1mAGFBLaKtr" resolve="658 CDT" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3UB9Xk" id="1mAGFBKco6f" role="3UB0xt">
      <property role="TrG5h" value="outside reference range" />
      <node concept="1vrFBu" id="1mAGFBKco7x" role="3UB9X9">
        <node concept="3Uuv2v" id="6PcFLt2m8fR" role="1vrFBt">
          <node concept="3dxklP" id="6PcFLt2m8fP" role="30DPml">
            <node concept="2NFnoM" id="6PcFLt2m8gm" role="3udjY4">
              <ref role="32LHjG" to="1iku:1mAGFBLaKtq" resolve="A58 PEth" />
            </node>
          </node>
          <node concept="1vovAn" id="1mAGFBKOXvH" role="3_NtmV">
            <node concept="1vpYan" id="1mAGFBKOXvJ" role="3UJoVu">
              <property role="1vpYam" value="0.30" />
            </node>
          </node>
        </node>
        <node concept="3Uuv2v" id="6PcFLt2m8gr" role="1vrFBt">
          <node concept="3dxklP" id="6PcFLt2m8gp" role="30DPml">
            <node concept="2NFnoM" id="6PcFLt2m8gR" role="3udjY4">
              <ref role="32LHjG" to="1iku:1mAGFBLaKtr" resolve="658 CDT" />
            </node>
          </node>
          <node concept="1vovAn" id="1mAGFBKOXwd" role="3_NtmV">
            <node concept="1vpYan" id="1mAGFBKOXwf" role="3UJoVu">
              <property role="1vpYam" value="1.70" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="1c$BFI" id="4QUW3edt4eX">
    <property role="3GE5qa" value="aspects" />
    <property role="TrG5h" value="V₂ - Stop if all required test results are invalid" />
    <node concept="3JKeyA" id="2FjKBCOFjFV" role="1c$BFD">
      <node concept="307ky3" id="2FjKBCPNaai" role="umdqu">
        <property role="TrG5h" value="TEST" />
        <node concept="3uFIm8" id="2FjKBCPNaas" role="3v97CT" />
      </node>
      <node concept="v7RD4" id="2FjKBCPNjJX" role="umdqu">
        <property role="TrG5h" value="COMMENT" />
        <node concept="v7JTP" id="2FjKBCPOsAH" role="3v97CT" />
      </node>
      <node concept="u4cA6" id="2FjKBCPNaaQ" role="6dmBO">
        <node concept="1vrFBu" id="2FjKBCPNaaW" role="u4cA7">
          <node concept="3Uuv2s" id="2FjKBCPNab4" role="1vrFBt">
            <node concept="7aCDb" id="2FjKBCPNab0" role="30DPml">
              <node concept="32e83T" id="2FjKBCPNab8" role="3udjY5">
                <ref role="3uSGeQ" node="2FjKBCPNaai" resolve="TEST" />
              </node>
            </node>
          </node>
          <node concept="3Uuv2s" id="2FjKBCPNabk" role="1vrFBt">
            <node concept="3a7a89" id="2FjKBCPNabg" role="30DPml">
              <node concept="32e83T" id="2FjKBCPNabr" role="3udjY5">
                <ref role="3uSGeQ" node="2FjKBCPNaai" resolve="TEST" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="vskhB" id="2FjKBCQ8eDl" role="7UZBM">
        <node concept="uvJ$x" id="2FjKBCQ8eDq" role="sKHUD">
          <node concept="32e83T" id="2FjKBCQ8eDs" role="uvJ$A">
            <ref role="3uSGeQ" node="2FjKBCPNaai" resolve="TEST" />
          </node>
        </node>
        <node concept="1GeuE" id="2FjKBCQ8eDy" role="sKHUD">
          <node concept="3$BABF" id="2FjKBCQ8eDB" role="1GeuG">
            <node concept="v0zvr" id="2FjKBCQ8eDD" role="3$BWal">
              <ref role="3uSGeQ" node="2FjKBCPNjJX" resolve="COMMENT" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="1vr_BQ" id="6khVixyaUDV">
    <property role="3GE5qa" value="ruleSets" />
    <property role="TrG5h" value="ExampleTask2" />
    <property role="mDFXa" value="Scheduling Secondary Microbiology Work" />
    <node concept="1vr_BD" id="6khVixyIsef" role="1vr$2Z">
      <property role="TrG5h" value="Staphylococcus agglutination" />
      <node concept="1vrFBv" id="6khVixyKcj7" role="1vrEod">
        <node concept="3Uuv2v" id="6khVixyKcjz" role="1vrFBt">
          <node concept="7ePLY" id="6khVixyKcjt" role="30DPml">
            <node concept="7e0qA" id="6LTgXmMPRtk" role="7fd7B">
              <ref role="32LHjG" to="1iku:6khVixyKcjP" resolve="Eye Swab" />
            </node>
          </node>
          <node concept="78FWj" id="6khVixyKcjT" role="3_NtmV">
            <node concept="7e7nd" id="6LTgXmMPRto" role="78DU7">
              <ref role="32LHjG" to="1iku:6khVixy0$rs" resolve="Staphylococcus aureus" />
            </node>
          </node>
        </node>
        <node concept="3Uuv2v" id="6khVixyIWk0" role="1vrFBt">
          <node concept="7ePLY" id="6khVixyIWjX" role="30DPml">
            <node concept="7e0qA" id="6LTgXmMPRtw" role="7fd7B">
              <ref role="32LHjG" to="1iku:6khVixya4XV" resolve="Wound Swab" />
            </node>
          </node>
          <node concept="78FWj" id="6khVixyIWkz" role="3_NtmV">
            <node concept="7e7nd" id="6LTgXmMPRt$" role="78DU7">
              <ref role="32LHjG" to="1iku:6khVixyHvMd" resolve="Staphylococcus lugdunensis" />
            </node>
          </node>
        </node>
      </node>
      <node concept="7hQq_" id="6khVixyIWkE" role="3$wkmd">
        <node concept="7hEPG" id="6LTgXmMPRtC" role="7h_lv">
          <ref role="32LHjG" to="1iku:6khVixyGx_c" resolve="Staphylococcus agglutination" />
        </node>
      </node>
    </node>
    <node concept="1vr_BD" id="6khVixyIWkL" role="1vr$2Z">
      <property role="TrG5h" value="XV test" />
      <node concept="3Uuv2v" id="6khVixyKcg2" role="1vrEod">
        <node concept="7ePLY" id="6khVixyKcfZ" role="30DPml">
          <node concept="7e0qA" id="6LTgXmMPRtK" role="7fd7B">
            <ref role="32LHjG" to="1iku:6khVixyGx_f" resolve="Nasofarynx" />
          </node>
        </node>
        <node concept="78FWj" id="6khVixyKcgI" role="3_NtmV">
          <node concept="7e7nd" id="6LTgXmMPRtO" role="78DU7">
            <ref role="32LHjG" to="1iku:6khVixyHvMe" resolve="Haemophilus influenzae" />
          </node>
        </node>
      </node>
      <node concept="7hQq_" id="6khVixyKcgP" role="3$wkmd">
        <node concept="7hEPG" id="6LTgXmMPRtS" role="7h_lv">
          <ref role="32LHjG" to="1iku:6khVixyGx_d" resolve="XV test" />
        </node>
      </node>
    </node>
    <node concept="1vr_BD" id="6khVixyKcgW" role="1vr$2Z">
      <property role="TrG5h" value="Tellur test" />
      <node concept="3Uuv2v" id="6khVixyKchD" role="1vrEod">
        <node concept="7ePLY" id="6khVixyKchA" role="30DPml">
          <node concept="7e0qA" id="6LTgXmMPRu0" role="7fd7B">
            <ref role="32LHjG" to="1iku:6khVixyGx_g" resolve="Urine" />
          </node>
        </node>
        <node concept="78FWj" id="6khVixyKciu" role="3_NtmV">
          <node concept="7e7nd" id="6LTgXmMPRu4" role="78DU7">
            <ref role="32LHjG" to="1iku:6khVixyHvMf" resolve="Enterococcus faecalis" />
          </node>
        </node>
      </node>
      <node concept="7hQq_" id="6khVixyKci_" role="3$wkmd">
        <node concept="7hEPG" id="6LTgXmMPRu8" role="7h_lv">
          <ref role="32LHjG" to="1iku:6khVixyGx_e" resolve="Tellur test" />
        </node>
      </node>
    </node>
    <node concept="1vr_BD" id="2FjKBCR3BsV" role="1vr$2Z">
      <property role="TrG5h" value="Van A and Van B" />
      <node concept="7hQq_" id="2FjKBCR3BsW" role="3$wkmd">
        <node concept="7hEPG" id="2FjKBCR3Bot" role="7h_lv">
          <ref role="32LHjG" to="1iku:2FjKBCR26E7" resolve="Vanomycin test A" />
        </node>
      </node>
      <node concept="7hQq_" id="2FjKBCR3Bo$" role="3$wkmd">
        <node concept="7hEPG" id="2FjKBCR3BoG" role="7h_lv">
          <ref role="32LHjG" to="1iku:2FjKBCR26E8" resolve="Vanomycin test B" />
        </node>
      </node>
      <node concept="1vrFBv" id="2FjKBCR3BmS" role="1vrEod">
        <node concept="3Uuv2v" id="2FjKBCR3BmU" role="1vrFBt">
          <node concept="7nPTg" id="2FjKBCR3Bn8" role="3_NtmV">
            <node concept="7nGG7" id="2FjKBCR3Bnb" role="7nHj4" />
          </node>
          <node concept="7mLmx" id="2FjKBCR3BmW" role="30DPml">
            <node concept="7e7nd" id="2FjKBCR3BmX" role="7mLmy">
              <ref role="32LHjG" to="1iku:6khVixyHvMf" resolve="Enterococcus faecalis" />
            </node>
            <node concept="7CTiL" id="2FjKBCR3Bn5" role="7CKO$">
              <ref role="32LHjG" to="1iku:2FjKBCR26E2" resolve="Vancomycin" />
            </node>
          </node>
        </node>
        <node concept="3Uuv2v" id="2FjKBCR3Bno" role="1vrFBt">
          <node concept="7nPTg" id="2FjKBCR3BnJ" role="3_NtmV">
            <node concept="7nGG7" id="2FjKBCR3BnM" role="7nHj4" />
          </node>
          <node concept="7mLmx" id="2FjKBCR3Bnl" role="30DPml">
            <node concept="7e7nd" id="2FjKBCR3BnD" role="7mLmy">
              <ref role="32LHjG" to="1iku:2FjKBCR2CxK" resolve="Enterococcus faecium" />
            </node>
            <node concept="7CTiL" id="2FjKBCR3BnG" role="7CKO$">
              <ref role="32LHjG" to="1iku:2FjKBCR26E2" resolve="Vancomycin" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1vr_BD" id="2FjKBCR3BsX" role="1vr$2Z">
      <property role="TrG5h" value="ESBL A" />
      <node concept="7hQq_" id="2FjKBCR3BsY" role="3$wkmd">
        <node concept="7hEPG" id="2FjKBCR3BsZ" role="7h_lv">
          <ref role="32LHjG" to="1iku:2FjKBCR26E6" resolve="e-test ESBLₐ" />
        </node>
      </node>
      <node concept="1vrFBu" id="2FjKBCR3Bt0" role="1vrEod">
        <node concept="1vrFBv" id="2FjKBCR3Bt1" role="1vrFBt">
          <node concept="3Uuv2v" id="2FjKBCR3Bt2" role="1vrFBt">
            <node concept="7nPTg" id="2FjKBCR3Bt3" role="3_NtmV">
              <node concept="7nGG7" id="2FjKBCR3Bt4" role="7nHj4" />
            </node>
            <node concept="7mLmx" id="2FjKBCR3Bt5" role="30DPml">
              <node concept="7e7nd" id="2FjKBCR3Bt6" role="7mLmy">
                <ref role="32LHjG" to="1iku:2FjKBCR2Cx_" resolve="Escherichia coli" />
              </node>
              <node concept="7CTiL" id="2FjKBCR3Bt7" role="7CKO$">
                <ref role="32LHjG" to="1iku:6khVixyHvbl" resolve="Cefotaxim" />
              </node>
            </node>
          </node>
          <node concept="3Uuv2v" id="2FjKBCR3Bt8" role="1vrFBt">
            <node concept="7nPTg" id="2FjKBCR3Bt9" role="3_NtmV">
              <node concept="7nGG7" id="2FjKBCR3Bta" role="7nHj4" />
            </node>
            <node concept="7mLmx" id="2FjKBCR3Btb" role="30DPml">
              <node concept="7e7nd" id="2FjKBCR3Btc" role="7mLmy">
                <ref role="32LHjG" to="1iku:2FjKBCR2Cx_" resolve="Escherichia coli" />
              </node>
              <node concept="7CTiL" id="2FjKBCR3Btd" role="7CKO$">
                <ref role="32LHjG" to="1iku:2FjKBCR26E3" resolve="Ceftazidim" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3Uuv2v" id="2FjKBCR3Bte" role="1vrFBt">
          <node concept="7nPTg" id="2FjKBCR3Btf" role="3_NtmV">
            <node concept="7nGGo" id="2FjKBCR3Btg" role="7nHj4" />
          </node>
          <node concept="7mLmx" id="2FjKBCR3Bth" role="30DPml">
            <node concept="7e7nd" id="2FjKBCR3Bti" role="7mLmy">
              <ref role="32LHjG" to="1iku:2FjKBCR2Cx_" resolve="Escherichia coli" />
            </node>
            <node concept="7CTiL" id="2FjKBCR3Btj" role="7CKO$">
              <ref role="32LHjG" to="1iku:2FjKBCR26E4" resolve="Cefoxitin" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1vr_BD" id="2FjKBCR3Btk" role="1vr$2Z">
      <property role="TrG5h" value="ESBL M" />
      <node concept="7hQq_" id="2FjKBCR3Btl" role="3$wkmd">
        <node concept="7hEPG" id="2FjKBCR38kV" role="7h_lv">
          <ref role="32LHjG" to="1iku:2FjKBCR26E5" resolve="e-test ESBLₘ" />
        </node>
      </node>
      <node concept="1vrFBu" id="2FjKBCR38iP" role="1vrEod">
        <node concept="1vrFBv" id="2FjKBCR38iQ" role="1vrFBt">
          <node concept="3Uuv2v" id="2FjKBCR38iR" role="1vrFBt">
            <node concept="7nPTg" id="2FjKBCR38iS" role="3_NtmV">
              <node concept="7nGG7" id="2FjKBCR38iT" role="7nHj4" />
            </node>
            <node concept="7mLmx" id="2FjKBCR38iU" role="30DPml">
              <node concept="7e7nd" id="2FjKBCR38iV" role="7mLmy">
                <ref role="32LHjG" to="1iku:2FjKBCR2Cx_" resolve="Escherichia coli" />
              </node>
              <node concept="7CTiL" id="2FjKBCR38iW" role="7CKO$">
                <ref role="32LHjG" to="1iku:6khVixyHvbl" resolve="Cefotaxim" />
              </node>
            </node>
          </node>
          <node concept="3Uuv2v" id="2FjKBCR38iX" role="1vrFBt">
            <node concept="7nPTg" id="2FjKBCR38iY" role="3_NtmV">
              <node concept="7nGG7" id="2FjKBCR38iZ" role="7nHj4" />
            </node>
            <node concept="7mLmx" id="2FjKBCR38j0" role="30DPml">
              <node concept="7e7nd" id="2FjKBCR38j1" role="7mLmy">
                <ref role="32LHjG" to="1iku:2FjKBCR2Cx_" resolve="Escherichia coli" />
              </node>
              <node concept="7CTiL" id="2FjKBCR38j2" role="7CKO$">
                <ref role="32LHjG" to="1iku:2FjKBCR26E3" resolve="Ceftazidim" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3Uuv2v" id="2FjKBCR38j3" role="1vrFBt">
          <node concept="7nPTg" id="2FjKBCR38j4" role="3_NtmV">
            <node concept="7nGG7" id="2FjKBCR38kS" role="7nHj4" />
          </node>
          <node concept="7mLmx" id="2FjKBCR38j6" role="30DPml">
            <node concept="7e7nd" id="2FjKBCR38j7" role="7mLmy">
              <ref role="32LHjG" to="1iku:2FjKBCR2Cx_" resolve="Escherichia coli" />
            </node>
            <node concept="7CTiL" id="2FjKBCR38j8" role="7CKO$">
              <ref role="32LHjG" to="1iku:2FjKBCR26E4" resolve="Cefoxitin" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3iBtEz" id="65epL7MeaJg">
    <property role="3GE5qa" value="aspects" />
    <node concept="3iBtEI" id="2FjKBCOLaOA" role="3iBsvl">
      <ref role="3iBtEH" node="2FjKBCOuw1w" resolve="V₁ - Stop if a required test result has yet to be obtained" />
    </node>
    <node concept="3iBtEI" id="2FjKBCPun6Y" role="3iBsvl">
      <ref role="3iBtEH" node="4QUW3edt4eX" resolve="V₂ - Stop if all required test results are invalid" />
    </node>
    <node concept="3iBtEI" id="2FjKBCOLaOG" role="3iBsvl">
      <ref role="3iBtEH" node="2FjKBCOKS6c" resolve="V₃ - Stop if relevant comments have already been added" />
    </node>
    <node concept="3iBtEI" id="2FjKBCOLy4Q" role="3iBsvl">
      <ref role="3iBtEH" node="2FjKBCOLvEv" resolve="V₄ - Add explanatory comment when interpreting invalid test results" />
    </node>
  </node>
  <node concept="1c$BFI" id="2FjKBCOuw1w">
    <property role="3GE5qa" value="aspects" />
    <property role="TrG5h" value="V₁ - Stop if a required test result has yet to be obtained" />
    <node concept="3JKeyA" id="2FjKBCO$rTW" role="1c$BFD">
      <node concept="6dDDo" id="2FjKBCOFjBV" role="6dmBO">
        <node concept="1vrFBv" id="2FjKBCQdFxf" role="6dDDp">
          <node concept="3Uuv2s" id="2FjKBCQdFxh" role="1vrFBt">
            <node concept="3uGdx1" id="2FjKBCQdFxi" role="30DPml">
              <node concept="32e83T" id="2FjKBCQdFxj" role="3udjY5">
                <ref role="3uSGeQ" node="2FjKBCOKJx1" resolve="TEST" />
              </node>
            </node>
          </node>
          <node concept="3UgdrD" id="2FjKBCQdFxk" role="1vrFBt">
            <node concept="3Uuv2s" id="2FjKBCQdFxl" role="3UgdrC">
              <node concept="7aCDb" id="2FjKBCQdFxm" role="30DPml">
                <node concept="32e83T" id="2FjKBCQdFxn" role="3udjY5">
                  <ref role="3uSGeQ" node="2FjKBCOKJx1" resolve="TEST" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="307ky3" id="2FjKBCOKJx1" role="umdqu">
        <property role="TrG5h" value="TEST" />
        <node concept="3uFIm8" id="2FjKBCOKJx5" role="3v97CT" />
      </node>
      <node concept="v7RD4" id="2FjKBCQPEtz" role="umdqu">
        <property role="TrG5h" value="COMMENT" />
        <node concept="v7JTP" id="2FjKBCQPEtH" role="3v97CT" />
      </node>
      <node concept="vskhB" id="2FjKBCQPEtc" role="7UZBM">
        <node concept="uvJ$x" id="2FjKBCQPEtd" role="sKHUD">
          <node concept="32e83T" id="2FjKBCQPEte" role="uvJ$A">
            <ref role="3uSGeQ" node="2FjKBCOKJx1" resolve="TEST" />
          </node>
        </node>
        <node concept="1GeuE" id="2FjKBCQPEtm" role="sKHUD">
          <node concept="3$BABF" id="2FjKBCQPEtr" role="1GeuG">
            <node concept="v0zvr" id="2FjKBCQPEtN" role="3$BWal">
              <ref role="3uSGeQ" node="2FjKBCQPEtz" resolve="COMMENT" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="1c$BFI" id="2FjKBCOKS6c">
    <property role="3GE5qa" value="aspects" />
    <property role="TrG5h" value="V₃ - Stop if relevant comments have already been added" />
    <node concept="3JKeyA" id="2FjKBCOKS6d" role="1c$BFD">
      <node concept="6dDDo" id="2FjKBCOKS6e" role="6dmBO">
        <node concept="3UgdrD" id="2FjKBCOKS6Y" role="6dDDp">
          <node concept="3Uuv2s" id="2FjKBCOKS6Z" role="3UgdrC">
            <node concept="u485H" id="2FjKBCOKS70" role="30DPml">
              <node concept="MJVlO" id="2FjKBCOKS71" role="u485y">
                <node concept="1JR0S" id="2FjKBCOKS72" role="1bkD$5">
                  <ref role="3uSGeQ" node="2FjKBCOKS6u" resolve="COMMENT" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="1GeuE" id="2FjKBCOKS6s" role="7UZBM">
        <node concept="3$BABF" id="2FjKBCOKS6_" role="1GeuG">
          <node concept="MJVlO" id="2FjKBCOKS6C" role="3$BWal">
            <node concept="1JR0S" id="2FjKBCOKS6B" role="1bkD$5">
              <ref role="3uSGeQ" node="2FjKBCOKS6u" resolve="COMMENT" />
            </node>
          </node>
        </node>
      </node>
      <node concept="1JR0U" id="2FjKBCOKS6u" role="umdqu">
        <property role="TrG5h" value="COMMENT" />
        <node concept="3vy__x" id="2FjKBCOKS6y" role="3v97CT" />
      </node>
    </node>
  </node>
  <node concept="1c$BFI" id="2FjKBCOLvEv">
    <property role="3GE5qa" value="aspects" />
    <property role="TrG5h" value="V₄ - Add explanatory comment when interpreting invalid test results" />
    <node concept="3JKeyA" id="2FjKBCOLvEw" role="1c$BFD">
      <node concept="3HtHId" id="2FjKBCOLvFl" role="6dmBO">
        <node concept="1vr_BD" id="2FjKBCOLvFn" role="3HtHJT">
          <property role="TrG5h" value="Explanatory comment" />
          <property role="3UpYCF" value="Assumes that testing instrument returns &quot;I.S.&quot;" />
          <node concept="3$BABF" id="2FjKBCOLvFE" role="3$wkmd">
            <node concept="3pZa6C" id="2FjKBCOLvFG" role="3$BWal">
              <node concept="1vsOU9" id="2FjKBCOLvFH" role="3pZa69">
                <property role="1vsOU8" value="Insufficuent specimen sample" />
              </node>
            </node>
          </node>
          <node concept="3Uuv2v" id="2FjKBCQQrqW" role="1vrEod">
            <node concept="3_XdMG" id="2FjKBCQQrr_" role="3_NtmV">
              <node concept="1vsOU9" id="2FjKBCQQrrB" role="3_XdMJ">
                <property role="1vsOU8" value="I.S." />
              </node>
            </node>
            <node concept="3dWSYl" id="2FjKBCQQrqU" role="30DPml">
              <node concept="3b72cV" id="2FjKBCQQrry" role="3udjY3">
                <ref role="3uSGeQ" node="2FjKBCQQrri" resolve="TEST" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="vskhB" id="2FjKBCQ8fZq" role="7UZBM">
        <node concept="uvJ$x" id="2FjKBCQQrqh" role="sKHUD">
          <node concept="32e83T" id="2FjKBCQQrrv" role="uvJ$A">
            <ref role="3uSGeQ" node="2FjKBCQQrri" resolve="TEST" />
          </node>
        </node>
        <node concept="1GeuE" id="2FjKBCQ8fZ_" role="sKHUD">
          <node concept="3$BABF" id="2FjKBCQ8fZF" role="1GeuG">
            <node concept="v0zvr" id="2FjKBCQ8fZN" role="3$BWal">
              <ref role="3uSGeQ" node="2FjKBCQ8fZH" resolve="COMMENT" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3b74nw" id="2FjKBCQQrri" role="umdqu">
        <property role="TrG5h" value="TEST" />
        <node concept="2j0jrP" id="2FjKBCQQrrs" role="3v97CT" />
      </node>
      <node concept="v7RD4" id="2FjKBCQ8fZH" role="umdqu">
        <property role="TrG5h" value="COMMENT" />
        <node concept="v7JTP" id="2FjKBCQ8fZL" role="3v97CT" />
      </node>
    </node>
  </node>
</model>

