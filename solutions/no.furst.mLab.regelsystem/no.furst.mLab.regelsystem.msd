<?xml version="1.0" encoding="UTF-8"?>
<solution name="no.furst.mLab.regelsystem" uuid="38a4235e-83dd-4249-b71b-fd73b55abc20" moduleVersion="0" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
  </models>
  <sourcePath />
  <languageVersions>
    <language slang="l:b4d28e19-7d2d-47e9-943e-3a41f97a0e52:com.mbeddr.mpsutil.plantuml.node" version="0" />
    <language slang="l:ceab5195-25ea-4f22-9b92-103b95ca8c0c:jetbrains.mps.lang.core" version="1" />
    <language slang="l:5cb5fdcf-e91c-444e-b5ab-45127e767606:mLabExt.declarations.biochemistry" version="0" />
    <language slang="l:8fc9d2a5-9169-4fbe-a02d-1c7f83e86b8e:no.furst.mLab.decisions.references" version="0" />
    <language slang="l:598e47df-5b3d-4e8c-94a1-69f953d4de82:no.furst.mLab.decisions.tasks.autocommentation" version="0" />
    <language slang="l:1b13d5a1-80e4-47ff-b773-4b4b3c9489d3:no.furst.mLab.decisions.tasks.secondaryWork" version="0" />
    <language slang="l:bdbd2c14-a335-48a1-809b-6b32de6d4748:no.furst.mLab.entities" version="0" />
    <language slang="l:d8a481b1-c5ba-42b8-a542-fafbce063ca2:no.furst.mLab.extensions" version="0" />
    <language slang="l:2cc044e9-6812-4dff-a42e-76f2c6901586:no.uio.mLab.core.aspect" version="0" />
    <language slang="l:36ee5809-1b2d-4e3e-9e5e-53975b57a524:no.uio.mLab.data.biochemistry.aspects" version="0" />
    <language slang="l:c610a4eb-c47c-4f95-b568-11236971769c:no.uio.mLab.decisions.core" version="0" />
    <language slang="l:93a27952-3baa-41ca-9e72-e2a9cf1b3f77:no.uio.mLab.decisions.data.laboratoryTest" version="0" />
    <language slang="l:72a150d4-97f8-489b-aded-022e7dabd8b9:no.uio.mLab.decisions.data.laboratoryTest.numberResult" version="0" />
    <language slang="l:60f87c04-0cd8-4174-8d1a-4a254a9e2f58:no.uio.mLab.decisions.data.laboratoryTest.textualResult" version="0" />
    <language slang="l:6b1bb23e-8155-434e-8611-255c4637394b:no.uio.mLab.decisions.data.microbiology" version="0" />
    <language slang="l:f130674d-23fb-4e9b-a200-548653f5fd0b:no.uio.mLab.decisions.data.patient" version="0" />
    <language slang="l:76146113-297e-4cbe-99cf-714ae175bb83:no.uio.mLab.decisions.references.biochemistry" version="0" />
    <language slang="l:4a652d55-3684-4d2d-98c9-2ef46f124c44:no.uio.mLab.decisions.references.laboratoryTest" version="0" />
    <language slang="l:64b829cb-114b-4b36-813b-6c9497a3e006:no.uio.mLab.decisions.references.microbiology" version="0" />
    <language slang="l:0e664af0-ccb5-427d-94ac-6f90ac2b6424:no.uio.mLab.decisions.references.specialistType" version="0" />
    <language slang="l:ebe710b8-8f46-4b5e-8fb1-221de61f9e7c:no.uio.mLab.decisions.tasks.autocommentation" version="0" />
    <language slang="l:bdfbcabc-8ac0-41dc-aae3-092d151214a5:no.uio.mLab.decisions.tasks.autovalidation" version="0" />
    <language slang="l:284ce324-ec91-4245-a186-fefe6b9a1ea8:no.uio.mLab.entities.biochemistry" version="0" />
    <language slang="l:1d15ae27-4d6d-46bb-bf93-45a09cdab737:no.uio.mLab.entities.biochemistry.aspects" version="0" />
    <language slang="l:24e093e3-c8a7-455d-9219-7414f361993f:no.uio.mLab.entities.comments" version="0" />
    <language slang="l:33c0f773-be29-48e3-9587-40dc4b3c54f0:no.uio.mLab.entities.core" version="0" />
    <language slang="l:cae652fa-7f6e-4122-8bd5-0b26c8b4eec8:no.uio.mLab.entities.laboratoryTest" version="-1" />
    <language slang="l:7de0968d-bd8f-4776-b76d-8d0cb1ebf77b:no.uio.mLab.entities.microbiology" version="0" />
    <language slang="l:0c42db4c-2bfa-4af0-b368-f89dcb171d67:no.uio.mLab.entities.specialistType" version="0" />
    <language slang="l:6adcef38-5cdc-48dc-b225-be76276615fd:no.uio.mLab.shared" version="0" />
  </languageVersions>
  <dependencyVersions>
    <module reference="38a4235e-83dd-4249-b71b-fd73b55abc20(no.furst.mLab.regelsystem)" version="0" />
  </dependencyVersions>
</solution>

