<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:7c3361f6-3c2c-4cdf-8d3e-794e424e582d(no.uio.mLab.build)">
  <persistence version="9" />
  <languages>
    <use id="798100da-4f0a-421a-b991-71f8c50ce5d2" name="jetbrains.mps.build" version="0" />
    <use id="0cf935df-4699-4e9c-a132-fa109541cba3" name="jetbrains.mps.build.mps" version="5" />
  </languages>
  <imports>
    <import index="ffeo" ref="r:874d959d-e3b4-4d04-b931-ca849af130dd(jetbrains.mps.ide.build)" />
    <import index="al5i" ref="r:742f344d-4dc4-4862-992c-4bc94b094870(com.mbeddr.mpsutil.dev.build)" />
  </imports>
  <registry>
    <language id="798100da-4f0a-421a-b991-71f8c50ce5d2" name="jetbrains.mps.build">
      <concept id="5481553824944787378" name="jetbrains.mps.build.structure.BuildSourceProjectRelativePath" flags="ng" index="55IIr" />
      <concept id="2755237150521975431" name="jetbrains.mps.build.structure.BuildVariableMacroInitWithString" flags="ng" index="aVJcg">
        <child id="2755237150521975437" name="value" index="aVJcq" />
      </concept>
      <concept id="244868996532454372" name="jetbrains.mps.build.structure.BuildVariableMacroInitWithDate" flags="ng" index="hHN3E">
        <property id="244868996532454384" name="pattern" index="hHN3Y" />
      </concept>
      <concept id="7321017245476976379" name="jetbrains.mps.build.structure.BuildRelativePath" flags="ng" index="iG8Mu">
        <child id="7321017245477039051" name="compositePart" index="iGT6I" />
      </concept>
      <concept id="3767587139141066978" name="jetbrains.mps.build.structure.BuildVariableMacro" flags="ng" index="2kB4xC">
        <child id="2755237150521975432" name="initialValue" index="aVJcv" />
      </concept>
      <concept id="4993211115183325728" name="jetbrains.mps.build.structure.BuildProjectDependency" flags="ng" index="2sgV4H">
        <reference id="5617550519002745380" name="script" index="1l3spb" />
        <child id="4129895186893471026" name="artifacts" index="2JcizS" />
      </concept>
      <concept id="4380385936562003279" name="jetbrains.mps.build.structure.BuildString" flags="ng" index="NbPM2">
        <child id="4903714810883783243" name="parts" index="3MwsjC" />
      </concept>
      <concept id="8618885170173601777" name="jetbrains.mps.build.structure.BuildCompositePath" flags="nn" index="2Ry0Ak">
        <property id="8618885170173601779" name="head" index="2Ry0Am" />
        <child id="8618885170173601778" name="tail" index="2Ry0An" />
      </concept>
      <concept id="2591537044435828004" name="jetbrains.mps.build.structure.BuildLayout_CompileOutputOf" flags="ng" index="Saw0i">
        <reference id="2591537044435828006" name="module" index="Saw0g" />
      </concept>
      <concept id="6647099934206700647" name="jetbrains.mps.build.structure.BuildJavaPlugin" flags="ng" index="10PD9b" />
      <concept id="7389400916848050074" name="jetbrains.mps.build.structure.BuildLayout_Jar" flags="ng" index="3981dx" />
      <concept id="7389400916848050060" name="jetbrains.mps.build.structure.BuildLayout_NamedContainer" flags="ng" index="3981dR">
        <child id="4380385936562148502" name="containerName" index="Nbhlr" />
      </concept>
      <concept id="7389400916848036984" name="jetbrains.mps.build.structure.BuildLayout_Folder" flags="ng" index="398223" />
      <concept id="7389400916848136194" name="jetbrains.mps.build.structure.BuildFolderMacro" flags="ng" index="398rNT">
        <child id="7389400916848144618" name="defaultPath" index="398pKh" />
      </concept>
      <concept id="7389400916848153117" name="jetbrains.mps.build.structure.BuildSourceMacroRelativePath" flags="ng" index="398BVA">
        <reference id="7389400916848153130" name="macro" index="398BVh" />
      </concept>
      <concept id="5617550519002745364" name="jetbrains.mps.build.structure.BuildLayout" flags="ng" index="1l3spV" />
      <concept id="5617550519002745363" name="jetbrains.mps.build.structure.BuildProject" flags="ng" index="1l3spW">
        <property id="4915877860348071612" name="fileName" index="turDy" />
        <property id="5204048710541015587" name="internalBaseDirectory" index="2DA0ip" />
        <child id="6647099934206700656" name="plugins" index="10PD9s" />
        <child id="7389400916848080626" name="parts" index="3989C9" />
        <child id="5617550519002745381" name="dependencies" index="1l3spa" />
        <child id="5617550519002745378" name="macros" index="1l3spd" />
        <child id="5617550519002745372" name="layout" index="1l3spN" />
      </concept>
      <concept id="4701820937132344003" name="jetbrains.mps.build.structure.BuildLayout_Container" flags="ng" index="1y1bJS">
        <child id="7389400916848037006" name="children" index="39821P" />
      </concept>
      <concept id="841011766566059607" name="jetbrains.mps.build.structure.BuildStringNotEmpty" flags="ng" index="3_J27D" />
      <concept id="4903714810883702019" name="jetbrains.mps.build.structure.BuildTextStringPart" flags="ng" index="3Mxwew">
        <property id="4903714810883755350" name="text" index="3MwjfP" />
      </concept>
      <concept id="4903714810883702017" name="jetbrains.mps.build.structure.BuildVarRefStringPart" flags="ng" index="3Mxwey">
        <reference id="4903714810883702018" name="macro" index="3Mxwex" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="0cf935df-4699-4e9c-a132-fa109541cba3" name="jetbrains.mps.build.mps">
      <concept id="6592112598314586625" name="jetbrains.mps.build.mps.structure.BuildMps_IdeaPluginGroup" flags="ng" index="m$f5U">
        <reference id="6592112598314586626" name="group" index="m$f5T" />
      </concept>
      <concept id="6592112598314498932" name="jetbrains.mps.build.mps.structure.BuildMps_IdeaPlugin" flags="ng" index="m$_wf">
        <property id="6592112598314498927" name="id" index="m$_wk" />
        <child id="6592112598314498931" name="version" index="m$_w8" />
        <child id="6592112598314499050" name="content" index="m$_yh" />
        <child id="6592112598314499028" name="dependencies" index="m$_yJ" />
        <child id="6592112598314499021" name="name" index="m$_yQ" />
        <child id="6592112598314855574" name="containerName" index="m_cZH" />
      </concept>
      <concept id="6592112598314499027" name="jetbrains.mps.build.mps.structure.BuildMps_IdeaPluginDependency" flags="ng" index="m$_yC">
        <reference id="6592112598314499066" name="target" index="m$_y1" />
      </concept>
      <concept id="6592112598314795900" name="jetbrains.mps.build.mps.structure.BuildMpsLayout_PluginDescriptor" flags="ng" index="m_q07">
        <reference id="6592112598314795901" name="plugin" index="m_q06" />
      </concept>
      <concept id="1500819558095907805" name="jetbrains.mps.build.mps.structure.BuildMps_Group" flags="ng" index="2G$12M">
        <child id="1500819558095907806" name="modules" index="2G$12L" />
      </concept>
      <concept id="1265949165890536423" name="jetbrains.mps.build.mps.structure.BuildMpsLayout_ModuleJars" flags="ng" index="L2wRC">
        <reference id="1265949165890536425" name="module" index="L2wRA" />
      </concept>
      <concept id="868032131020265945" name="jetbrains.mps.build.mps.structure.BuildMPSPlugin" flags="ng" index="3b7kt6" />
      <concept id="5253498789149381388" name="jetbrains.mps.build.mps.structure.BuildMps_Module" flags="ng" index="3bQrTs">
        <property id="1500819558096356884" name="doNotCompile" index="2GAjPV" />
        <child id="5253498789149547704" name="dependencies" index="3bR37C" />
      </concept>
      <concept id="5253498789149585690" name="jetbrains.mps.build.mps.structure.BuildMps_ModuleDependencyOnModule" flags="ng" index="3bR9La">
        <property id="5253498789149547713" name="reexport" index="3bR36h" />
        <reference id="5253498789149547705" name="module" index="3bR37D" />
      </concept>
      <concept id="5507251971038816436" name="jetbrains.mps.build.mps.structure.BuildMps_Generator" flags="ng" index="1yeLz9" />
      <concept id="4278635856200794926" name="jetbrains.mps.build.mps.structure.BuildMps_ModuleDependencyExtendLanguage" flags="ng" index="1Busua">
        <reference id="4278635856200794928" name="language" index="1Busuk" />
      </concept>
      <concept id="3189788309731981027" name="jetbrains.mps.build.mps.structure.BuildMps_ModuleSolutionRuntime" flags="ng" index="1E0d5M">
        <reference id="3189788309731981028" name="solution" index="1E0d5P" />
      </concept>
      <concept id="3189788309731840247" name="jetbrains.mps.build.mps.structure.BuildMps_Solution" flags="ng" index="1E1JtA" />
      <concept id="3189788309731840248" name="jetbrains.mps.build.mps.structure.BuildMps_Language" flags="ng" index="1E1JtD">
        <child id="3189788309731917348" name="runtime" index="1E1XAP" />
        <child id="9200313594498201639" name="generator" index="1TViLv" />
      </concept>
      <concept id="322010710375794190" name="jetbrains.mps.build.mps.structure.BuildMps_DevKit" flags="ng" index="3LEwk6">
        <child id="322010710375805250" name="extends" index="3LEz9a" />
        <child id="322010710375832962" name="exports" index="3LEDUa" />
      </concept>
      <concept id="322010710375805242" name="jetbrains.mps.build.mps.structure.BuildMps_DevKitRef" flags="ng" index="3LEz8M">
        <reference id="322010710375805243" name="devkit" index="3LEz8N" />
      </concept>
      <concept id="322010710375832938" name="jetbrains.mps.build.mps.structure.BuildMps_DevKitExportLanguage" flags="ng" index="3LEDTy">
        <reference id="322010710375832947" name="language" index="3LEDTV" />
      </concept>
      <concept id="322010710375871467" name="jetbrains.mps.build.mps.structure.BuildMps_AbstractModule" flags="ng" index="3LEN3z">
        <property id="8369506495128725901" name="compact" index="BnDLt" />
        <property id="322010710375892619" name="uuid" index="3LESm3" />
        <child id="322010710375956261" name="path" index="3LF7KH" />
      </concept>
      <concept id="7259033139236285166" name="jetbrains.mps.build.mps.structure.BuildMps_ExtractedModuleDependency" flags="nn" index="1SiIV0">
        <child id="7259033139236285167" name="dependency" index="1SiIV1" />
      </concept>
    </language>
  </registry>
  <node concept="1l3spW" id="2$xY$aEDy6N">
    <property role="TrG5h" value="no.uio.mLab" />
    <property role="2DA0ip" value="../../" />
    <property role="turDy" value="build_mLab_plugin.xml" />
    <node concept="2kB4xC" id="6oczKrNJPEZ" role="1l3spd">
      <property role="TrG5h" value="date" />
      <node concept="hHN3E" id="6oczKrNJPF0" role="aVJcv">
        <property role="hHN3Y" value="yyyyMMdd" />
      </node>
    </node>
    <node concept="2kB4xC" id="6oczKrNJPF1" role="1l3spd">
      <property role="TrG5h" value="build.number" />
      <node concept="aVJcg" id="6oczKrNJPF2" role="aVJcv">
        <node concept="NbPM2" id="6oczKrNJPF3" role="aVJcq">
          <node concept="3Mxwew" id="6oczKrNJPF4" role="3MwsjC">
            <property role="3MwjfP" value="mLabEngine-182.SNAPSHOT" />
          </node>
        </node>
      </node>
    </node>
    <node concept="10PD9b" id="2$xY$aEDy6O" role="10PD9s" />
    <node concept="3b7kt6" id="2$xY$aEDy6P" role="10PD9s" />
    <node concept="398rNT" id="2$xY$aEDy6Q" role="1l3spd">
      <property role="TrG5h" value="mps_home" />
      <node concept="55IIr" id="2$xY$aEDyAx" role="398pKh">
        <node concept="2Ry0Ak" id="2$xY$aEDyGc" role="iGT6I">
          <property role="2Ry0Am" value=".." />
          <node concept="2Ry0Ak" id="2$xY$aEDyMu" role="2Ry0An">
            <property role="2Ry0Am" value=".." />
            <node concept="2Ry0Ak" id="2$xY$aEDyTn" role="2Ry0An">
              <property role="2Ry0Am" value=".." />
              <node concept="2Ry0Ak" id="2$xY$aEDz0R" role="2Ry0An">
                <property role="2Ry0Am" value=".." />
                <node concept="2Ry0Ak" id="2$xY$aEDz6$" role="2Ry0An">
                  <property role="2Ry0Am" value="Applications" />
                  <node concept="2Ry0Ak" id="2$xY$aEDzch" role="2Ry0An">
                    <property role="2Ry0Am" value="MPS 2018.2.app" />
                    <node concept="2Ry0Ak" id="5NEhRcN3UF4" role="2Ry0An">
                      <property role="2Ry0Am" value="Contents" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2sgV4H" id="2$xY$aEDy6R" role="1l3spa">
      <ref role="1l3spb" to="ffeo:3IKDaVZmzS6" resolve="mps" />
      <node concept="398BVA" id="2$xY$aEDy6S" role="2JcizS">
        <ref role="398BVh" node="2$xY$aEDy6Q" resolve="mps_home" />
      </node>
    </node>
    <node concept="2sgV4H" id="2$xY$aEDywJ" role="1l3spa">
      <ref role="1l3spb" to="al5i:3AVJcIMlF8l" resolve="com.mbeddr.platform" />
      <node concept="398BVA" id="2$xY$aEDyzC" role="2JcizS">
        <ref role="398BVh" node="2$xY$aEDy6Q" resolve="mps_home" />
        <node concept="2Ry0Ak" id="5NEhRcN3UJB" role="iGT6I">
          <property role="2Ry0Am" value="plugins" />
        </node>
      </node>
    </node>
    <node concept="1l3spV" id="2$xY$aEDya6" role="1l3spN">
      <node concept="398223" id="2$xY$aENvLN" role="39821P">
        <node concept="3_J27D" id="2$xY$aENvLP" role="Nbhlr">
          <node concept="3Mxwew" id="2$xY$aENvPe" role="3MwsjC">
            <property role="3MwjfP" value="no.uio.mLab" />
          </node>
        </node>
        <node concept="398223" id="ymnOULBdbM" role="39821P">
          <node concept="398223" id="1Z_UwmPdKcF" role="39821P">
            <node concept="m_q07" id="3qWAZKHimPZ" role="39821P">
              <ref role="m_q06" node="2$xY$aEDy9X" resolve="no.uio.mLab.bundle" />
            </node>
            <node concept="3_J27D" id="1Z_UwmPdKcG" role="Nbhlr">
              <node concept="3Mxwew" id="1Z_UwmPdKcH" role="3MwsjC">
                <property role="3MwjfP" value="META-INF" />
              </node>
            </node>
          </node>
          <node concept="3_J27D" id="ymnOULBdbN" role="Nbhlr">
            <node concept="3Mxwew" id="ymnOULBdbQ" role="3MwsjC">
              <property role="3MwjfP" value="bundle" />
            </node>
          </node>
          <node concept="398223" id="ymnOULBdbS" role="39821P">
            <node concept="L2wRC" id="2$xY$aEFTNV" role="39821P">
              <ref role="L2wRA" node="2$xY$aEDy9q" resolve="no.uio.mLab.shared" />
            </node>
            <node concept="L2wRC" id="2$xY$aEFTXb" role="39821P">
              <ref role="L2wRA" node="2$xY$aEDy75" resolve="no.uio.mLab.shared.runtime" />
            </node>
            <node concept="L2wRC" id="ofcHXfZ43U" role="39821P">
              <ref role="L2wRA" node="ofcHXfYTWj" resolve="no.uio.mLab.shared.translations" />
            </node>
            <node concept="L2wRC" id="ofcHXg3s9q" role="39821P">
              <ref role="L2wRA" node="ofcHXfO28W" resolve="no.uio.mLab.decisions.core.utils" />
            </node>
            <node concept="L2wRC" id="2$xY$aEFRVl" role="39821P">
              <ref role="L2wRA" node="2$xY$aEDy7b" resolve="no.uio.mLab.entities.biochemistry" />
            </node>
            <node concept="L2wRC" id="2$xY$aEFS49" role="39821P">
              <ref role="L2wRA" node="2$xY$aEDy7v" resolve="no.uio.mLab.entities.biochemistry.runtime" />
            </node>
            <node concept="L2wRC" id="ofcHXg40z0" role="39821P">
              <ref role="L2wRA" node="ofcHXfZ3$W" resolve="no.uio.mLab.entities.biochemistry.translations" />
            </node>
            <node concept="L2wRC" id="4QUW3ee1Lhb" role="39821P">
              <ref role="L2wRA" node="4QUW3ee1aEc" resolve="no.uio.mLab.entities.core" />
            </node>
            <node concept="L2wRC" id="6khVixymbgg" role="39821P">
              <ref role="L2wRA" node="6khVixymas6" resolve="no.uio.mLab.entities.core.translations" />
            </node>
            <node concept="L2wRC" id="4QUW3ee1LEb" role="39821P">
              <ref role="L2wRA" node="4QUW3ee1cna" resolve="no.uio.mLab.entities.core.runtime" />
            </node>
            <node concept="L2wRC" id="2$xY$aEFRDN" role="39821P">
              <ref role="L2wRA" node="2$xY$aEDy8x" resolve="no.uio.mLab.entities.laboratoryTest" />
            </node>
            <node concept="L2wRC" id="2$xY$aEFRMz" role="39821P">
              <ref role="L2wRA" node="2$xY$aEDy88" resolve="no.uio.mLab.entities.laboratoryTest.runtime" />
            </node>
            <node concept="L2wRC" id="ofcHXgdI3T" role="39821P">
              <ref role="L2wRA" node="ofcHXgdH7h" resolve="no.uio.mLab.entities.laboratoryTest.translations" />
            </node>
            <node concept="L2wRC" id="6khVixymf5G" role="39821P">
              <ref role="L2wRA" node="6khVixymdUh" resolve="no.uio.mLab.entities.microbiology" />
            </node>
            <node concept="L2wRC" id="6khVixymfiu" role="39821P">
              <ref role="L2wRA" node="6khVixymbJD" resolve="no.uio.mLab.entities.microbiology.runtime" />
            </node>
            <node concept="L2wRC" id="6khVixymfpM" role="39821P">
              <ref role="L2wRA" node="6khVixymcRt" resolve="no.uio.mLab.entities.microbiology.translations" />
            </node>
            <node concept="L2wRC" id="2$xY$aEFSuL" role="39821P">
              <ref role="L2wRA" node="2$xY$aEDy9O" resolve="no.uio.mLab.entities.specialistType" />
            </node>
            <node concept="L2wRC" id="2$xY$aEFSBH" role="39821P">
              <ref role="L2wRA" node="2$xY$aEDy7i" resolve="no.uio.mLab.entities.specialistType.runtime" />
            </node>
            <node concept="L2wRC" id="2TpFF5_24Ec" role="39821P">
              <ref role="L2wRA" node="2TpFF5_245u" resolve="no.uio.mLab.entities.specialistType.translations" />
            </node>
            <node concept="L2wRC" id="6JpuGiQxE13" role="39821P">
              <ref role="L2wRA" node="2$xY$aEDy7U" resolve="no.uio.mLab.decisions.core" />
            </node>
            <node concept="L2wRC" id="2$xY$aEFRop" role="39821P">
              <ref role="L2wRA" node="2$xY$aEDy7O" resolve="no.uio.mLab.decisions.core.runtime" />
            </node>
            <node concept="L2wRC" id="ofcHXfProg" role="39821P">
              <ref role="L2wRA" node="ofcHXfO24e" resolve="no.uio.mLab.decisions.core.translations" />
            </node>
            <node concept="L2wRC" id="5qhhB$gsOs1" role="39821P">
              <ref role="L2wRA" node="7lYCqhu37uZ" resolve="no.uio.mLab.decisions.data.laboratoryTest" />
            </node>
            <node concept="L2wRC" id="5qhhB$gsOPp" role="39821P">
              <ref role="L2wRA" node="7lYCqhu39kg" resolve="no.uio.mLab.decisions.data.laboratoryTest.runtime" />
            </node>
            <node concept="L2wRC" id="5qhhB$gsP3v" role="39821P">
              <ref role="L2wRA" node="7lYCqhu38zh" resolve="no.uio.mLab.decisions.data.laboratoryTest.translations" />
            </node>
            <node concept="L2wRC" id="5qhhB$gsPlz" role="39821P">
              <ref role="L2wRA" node="7lYCqhu3axh" resolve="no.uio.mLab.decisions.data.laboratoryTest.numberResult" />
            </node>
            <node concept="L2wRC" id="5qhhB$gsPya" role="39821P">
              <ref role="L2wRA" node="7lYCqhu3awV" resolve="no.uio.mLab.decisions.data.laboratoryTest.numberResult.runtime" />
            </node>
            <node concept="L2wRC" id="5qhhB$gsPIN" role="39821P">
              <ref role="L2wRA" node="7lYCqhu3ax7" resolve="no.uio.mLab.decisions.data.laboratoryTest.numberResult.translations" />
            </node>
            <node concept="L2wRC" id="5qhhB$gsQ6s" role="39821P">
              <ref role="L2wRA" node="7lYCqhu3b$u" resolve="no.uio.mLab.decisions.data.laboratoryTest.textualResult" />
            </node>
            <node concept="L2wRC" id="5qhhB$gsQja" role="39821P">
              <ref role="L2wRA" node="7lYCqhu3b$8" resolve="no.uio.mLab.decisions.data.laboratoryTest.textualResult.runtime" />
            </node>
            <node concept="L2wRC" id="5qhhB$gsQqq" role="39821P">
              <ref role="L2wRA" node="7lYCqhu3b$k" resolve="no.uio.mLab.decisions.data.laboratoryTest.textualResult.translations" />
            </node>
            <node concept="L2wRC" id="2$xY$aEFT2H" role="39821P">
              <ref role="L2wRA" node="2$xY$aEDy8O" resolve="no.uio.mLab.decisions.data.microbiology" />
            </node>
            <node concept="L2wRC" id="2$xY$aEFTbL" role="39821P">
              <ref role="L2wRA" node="2$xY$aEDy91" resolve="no.uio.mLab.decisions.data.microbiology.runtime" />
            </node>
            <node concept="L2wRC" id="ofcHXfPs3c" role="39821P">
              <ref role="L2wRA" node="ofcHXfO2Te" resolve="no.uio.mLab.decisions.data.microbiology.translations" />
            </node>
            <node concept="L2wRC" id="5qhhB$gsRh4" role="39821P">
              <ref role="L2wRA" node="2$xY$aEDy8I" resolve="no.uio.mLab.decisions.data.patient" />
            </node>
            <node concept="L2wRC" id="5qhhB$gsRzl" role="39821P">
              <ref role="L2wRA" node="ofcHXfPsfZ" resolve="no.uio.mLab.decisions.data.patient.runtime" />
            </node>
            <node concept="L2wRC" id="5qhhB$gsRK8" role="39821P">
              <ref role="L2wRA" node="2$xY$aEDy9V" resolve="no.uio.mLab.decisions.data.patient.translations" />
            </node>
            <node concept="L2wRC" id="5qhhB$gr_1R" role="39821P">
              <ref role="L2wRA" node="4QUW3edwKrx" resolve="no.uio.mLab.decisions.references.laboratoryTest" />
            </node>
            <node concept="L2wRC" id="5qhhB$gr_jc" role="39821P">
              <ref role="L2wRA" node="4QUW3edwL$o" resolve="no.uio.mLab.decisions.references.laboratoryTest.runtime" />
            </node>
            <node concept="L2wRC" id="5qhhB$gr_KN" role="39821P">
              <ref role="L2wRA" node="4QUW3edwKXS" resolve="no.uio.mLab.decisions.references.laboratoryTest.translations" />
            </node>
            <node concept="L2wRC" id="5qhhB$grA2b" role="39821P">
              <ref role="L2wRA" node="4QUW3edwLYq" resolve="no.uio.mLab.decisions.references.biochemistry" />
            </node>
            <node concept="L2wRC" id="5qhhB$grAe7" role="39821P">
              <ref role="L2wRA" node="4QUW3edwLYa" resolve="no.uio.mLab.decisions.references.biochemistry.runtime" />
            </node>
            <node concept="L2wRC" id="5qhhB$grAf6" role="39821P">
              <ref role="L2wRA" node="4QUW3edwLYg" resolve="no.uio.mLab.decisions.references.biochemistry.translations" />
            </node>
            <node concept="L2wRC" id="5qhhB$grAA3" role="39821P">
              <ref role="L2wRA" node="5_LESNW0NMc" resolve="no.uio.mLab.decisions.references.microbiology" />
            </node>
            <node concept="L2wRC" id="5qhhB$grAM5" role="39821P">
              <ref role="L2wRA" node="5_LESNW0LDX" resolve="no.uio.mLab.decisions.references.microbiology.runtime" />
            </node>
            <node concept="L2wRC" id="5qhhB$grANa" role="39821P">
              <ref role="L2wRA" node="5_LESNW0MHz" resolve="no.uio.mLab.decisions.references.microbiology.translations" />
            </node>
            <node concept="L2wRC" id="5qhhB$grBad" role="39821P">
              <ref role="L2wRA" node="4QUW3ee17cn" resolve="no.uio.mLab.decisions.references.specialistType" />
            </node>
            <node concept="L2wRC" id="5qhhB$grBml" role="39821P">
              <ref role="L2wRA" node="4QUW3ee17Q2" resolve="no.uio.mLab.decisions.references.specialistType.runtime" />
            </node>
            <node concept="L2wRC" id="5qhhB$grBBX" role="39821P">
              <ref role="L2wRA" node="4QUW3efZo0Q" resolve="no.uio.mLab.decisions.references.specialistType.translations" />
            </node>
            <node concept="L2wRC" id="5qhhB$gsaNP" role="39821P">
              <ref role="L2wRA" node="2$xY$aEDy9e" resolve="no.uio.mLab.decisions.tasks.autocommentation" />
            </node>
            <node concept="L2wRC" id="5qhhB$gsb5y" role="39821P">
              <ref role="L2wRA" node="2$xY$aEDy7H" resolve="no.uio.mLab.decisions.tasks.autocommentation.runtime" />
            </node>
            <node concept="L2wRC" id="5qhhB$gsbng" role="39821P">
              <ref role="L2wRA" node="ofcHXfOoXC" resolve="no.uio.mLab.decisions.tasks.autocommentation.translations" />
            </node>
            <node concept="L2wRC" id="5qhhB$gsbVq" role="39821P">
              <ref role="L2wRA" node="2$xY$aEDy8k" resolve="no.uio.mLab.decisions.tasks.autovalidation" />
            </node>
            <node concept="L2wRC" id="5qhhB$gscFB" role="39821P">
              <ref role="L2wRA" node="2$xY$aEDy81" resolve="no.uio.mLab.decisions.tasks.autovalidation.runtime" />
            </node>
            <node concept="L2wRC" id="5qhhB$gscMt" role="39821P">
              <ref role="L2wRA" node="ofcHXfO1dE" resolve="no.uio.mLab.decisions.tasks.autovalidation.translations" />
            </node>
            <node concept="L2wRC" id="5_LESNW4dVl" role="39821P">
              <ref role="L2wRA" node="5_LESNW4dK5" resolve="no.uio.mLab.decisions.tasks.secondaryWork" />
            </node>
            <node concept="L2wRC" id="6PcFLt2lgT4" role="39821P">
              <ref role="L2wRA" node="5_LESNW4dJJ" resolve="no.uio.mLab.decisions.tasks.secondaryWork.runtime" />
            </node>
            <node concept="L2wRC" id="6PcFLt2lh6J" role="39821P">
              <ref role="L2wRA" node="5_LESNW4dJV" resolve="no.uio.mLab.decisions.tasks.secondaryWork.translations" />
            </node>
            <node concept="L2wRC" id="5qhhB$gsd9M" role="39821P">
              <ref role="L2wRA" node="5_LESNW3kEy" resolve="no.uio.mLab.decisions.tasks.susceptibilityDetermination" />
            </node>
            <node concept="L2wRC" id="5qhhB$gsdrE" role="39821P">
              <ref role="L2wRA" node="5_LESNW3kEc" resolve="no.uio.mLab.decisions.tasks.susceptibilityDetermination.runtime" />
            </node>
            <node concept="L2wRC" id="5qhhB$gsdC5" role="39821P">
              <ref role="L2wRA" node="5_LESNW3kEq" resolve="no.uio.mLab.decisions.tasks.susceptibilityDetermination.translations" />
            </node>
            <node concept="L2wRC" id="5jVYnMGX7IT" role="39821P">
              <ref role="L2wRA" node="5jVYnMGX1$k" resolve="no.uio.mLab.devkits.laboratoryTest" />
            </node>
            <node concept="L2wRC" id="5jVYnMGX837" role="39821P">
              <ref role="L2wRA" node="5jVYnMGX36d" resolve="no.uio.mLab.devkits.biochemistry" />
            </node>
            <node concept="L2wRC" id="5jVYnMGX8np" role="39821P">
              <ref role="L2wRA" node="5jVYnMGX4XX" resolve="no.uio.mLab.devkits.microbiology" />
            </node>
            <node concept="L2wRC" id="5jVYnMGX8FJ" role="39821P">
              <ref role="L2wRA" node="5jVYnMGX6wb" resolve="no.uio.mLab.devkits.autocommentation" />
            </node>
            <node concept="L2wRC" id="5jVYnMGXNm2" role="39821P">
              <ref role="L2wRA" node="5jVYnMGXN0I" resolve="no.uio.mLab.devkits.microbiology.secondaryWork" />
            </node>
            <node concept="3_J27D" id="ymnOULBdbT" role="Nbhlr">
              <node concept="3Mxwew" id="ymnOULBdbW" role="3MwsjC">
                <property role="3MwjfP" value="languages" />
              </node>
            </node>
          </node>
        </node>
        <node concept="398223" id="2$xY$aEQrRz" role="39821P">
          <node concept="3_J27D" id="2$xY$aEQrR_" role="Nbhlr">
            <node concept="3Mxwew" id="2$xY$aEQs2Y" role="3MwsjC">
              <property role="3MwjfP" value="engine" />
            </node>
          </node>
          <node concept="3981dx" id="2$xY$aENwkY" role="39821P">
            <node concept="3_J27D" id="2$xY$aENwl0" role="Nbhlr">
              <node concept="3Mxwey" id="2$xY$aENwqx" role="3MwsjC">
                <ref role="3Mxwex" node="6oczKrNJPF1" resolve="build.number" />
              </node>
              <node concept="3Mxwew" id="2$xY$aEOWYa" role="3MwsjC">
                <property role="3MwjfP" value=".jar" />
              </node>
            </node>
            <node concept="Saw0i" id="6oczKrOf3U7" role="39821P">
              <ref role="Saw0g" node="2$xY$aEDy7O" resolve="no.uio.mLab.decisions.core.runtime" />
            </node>
            <node concept="Saw0i" id="7lYCqhu3f3W" role="39821P">
              <ref role="Saw0g" node="7lYCqhu39kg" resolve="no.uio.mLab.decisions.data.laboratoryTest.runtime" />
            </node>
            <node concept="Saw0i" id="7lYCqhu3fhJ" role="39821P">
              <ref role="Saw0g" node="7lYCqhu3awV" resolve="no.uio.mLab.decisions.data.laboratoryTest.numberResult.runtime" />
            </node>
            <node concept="Saw0i" id="7lYCqhu3fvz" role="39821P">
              <ref role="Saw0g" node="7lYCqhu3b$8" resolve="no.uio.mLab.decisions.data.laboratoryTest.textualResult.runtime" />
            </node>
            <node concept="Saw0i" id="6oczKrOf4dq" role="39821P">
              <ref role="Saw0g" node="2$xY$aEDy91" resolve="no.uio.mLab.decisions.data.microbiology.runtime" />
            </node>
            <node concept="Saw0i" id="ofcHXg9i2p" role="39821P">
              <ref role="Saw0g" node="ofcHXfPsfZ" resolve="no.uio.mLab.decisions.data.patient.runtime" />
            </node>
            <node concept="Saw0i" id="6oczKrOf4qU" role="39821P">
              <ref role="Saw0g" node="2$xY$aEDy7H" resolve="no.uio.mLab.decisions.tasks.autocommentation.runtime" />
            </node>
            <node concept="Saw0i" id="6oczKrOf4un" role="39821P">
              <ref role="Saw0g" node="2$xY$aEDy81" resolve="no.uio.mLab.decisions.tasks.autovalidation.runtime" />
            </node>
            <node concept="Saw0i" id="5_LESNW4e7m" role="39821P">
              <ref role="Saw0g" node="5_LESNW4dJJ" resolve="no.uio.mLab.decisions.tasks.secondaryWork.runtime" />
            </node>
            <node concept="Saw0i" id="5_LESNW3mol" role="39821P">
              <ref role="Saw0g" node="5_LESNW3kEc" resolve="no.uio.mLab.decisions.tasks.susceptibilityDetermination.runtime" />
            </node>
            <node concept="Saw0i" id="7lYCqhv2v8o" role="39821P">
              <ref role="Saw0g" node="4QUW3edwLYa" resolve="no.uio.mLab.decisions.references.biochemistry.runtime" />
            </node>
            <node concept="Saw0i" id="7lYCqhv2vmh" role="39821P">
              <ref role="Saw0g" node="4QUW3edwL$o" resolve="no.uio.mLab.decisions.references.laboratoryTest.runtime" />
            </node>
            <node concept="Saw0i" id="5_LESNW1BDT" role="39821P">
              <ref role="Saw0g" node="5_LESNW0LDX" resolve="no.uio.mLab.decisions.references.microbiology.runtime" />
            </node>
            <node concept="Saw0i" id="7lYCqhv2v$c" role="39821P">
              <ref role="Saw0g" node="4QUW3ee17Q2" resolve="no.uio.mLab.decisions.references.specialistType.runtime" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="m$_wf" id="2$xY$aEDy9X" role="3989C9">
      <property role="m$_wk" value="no.uio.mLab.bundle" />
      <node concept="3_J27D" id="2$xY$aEDy9Y" role="m$_yQ">
        <node concept="3Mxwew" id="2$xY$aEDy9Z" role="3MwsjC">
          <property role="3MwjfP" value="no.uio.mLab.bundle" />
        </node>
      </node>
      <node concept="3_J27D" id="2$xY$aEDya0" role="m$_w8">
        <node concept="3Mxwew" id="2$xY$aEDya1" role="3MwsjC">
          <property role="3MwjfP" value="1.0" />
        </node>
      </node>
      <node concept="m$f5U" id="2$xY$aEDya2" role="m$_yh">
        <ref role="m$f5T" node="2$xY$aEDy9W" resolve="no.uio.mLab" />
      </node>
      <node concept="m$_yC" id="2$xY$aEDya3" role="m$_yJ">
        <ref role="m$_y1" to="ffeo:4k71ibbKLe8" resolve="jetbrains.mps.core" />
      </node>
      <node concept="m$_yC" id="7pRzn8SU5Ux" role="m$_yJ">
        <ref role="m$_y1" to="al5i:64SK4bcJmGP" resolve="com.mbeddr.mpsutil.plantuml" />
      </node>
      <node concept="3_J27D" id="2$xY$aEDya4" role="m_cZH">
        <node concept="3Mxwew" id="2$xY$aEDya5" role="3MwsjC">
          <property role="3MwjfP" value="no.uio.mLab.bundle" />
        </node>
      </node>
    </node>
    <node concept="2G$12M" id="2$xY$aEDy9W" role="3989C9">
      <property role="TrG5h" value="no.uio.mLab" />
      <node concept="1E1JtA" id="2$xY$aEDy75" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.shared.runtime" />
        <property role="3LESm3" value="39b6c822-5e7e-4849-bc08-d30db203f584" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="2$xY$aEDy6Z" role="3LF7KH">
          <node concept="2Ry0Ak" id="2$xY$aEDy70" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="2$xY$aEDy71" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.shared" />
              <node concept="2Ry0Ak" id="2$xY$aEDy72" role="2Ry0An">
                <property role="2Ry0Am" value="runtime" />
                <node concept="2Ry0Ak" id="ofcHXfYTHR" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.shared.runtime.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="ofcHXfYTWj" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.shared.translations" />
        <property role="3LESm3" value="3d1e5220-5be7-4cad-97be-258d1d3e62dd" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="ofcHXfYTWk" role="3LF7KH">
          <node concept="2Ry0Ak" id="ofcHXfYTWl" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="ofcHXfYTWm" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.shared" />
              <node concept="2Ry0Ak" id="ofcHXfYTWn" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.shared.translations" />
                <node concept="2Ry0Ak" id="ofcHXfYU7J" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.shared.translations.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="1E1JtD" id="2$xY$aEDy9q" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.shared" />
        <property role="3LESm3" value="6adcef38-5cdc-48dc-b225-be76276615fd" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="2$xY$aEDy9l" role="3LF7KH">
          <node concept="2Ry0Ak" id="2$xY$aEDy9m" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="2$xY$aEDy9n" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.shared" />
              <node concept="2Ry0Ak" id="2$xY$aEDy9o" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.shared.mpl" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1yeLz9" id="2$xY$aEDycJ" role="1TViLv">
          <property role="TrG5h" value="no.uio.mLab.shared#01" />
          <property role="3LESm3" value="290c6422-08a8-4071-bfb4-a43e8361e18c" />
          <property role="2GAjPV" value="false" />
        </node>
        <node concept="1E0d5M" id="2$xY$aEDykK" role="1E1XAP">
          <ref role="1E0d5P" node="2$xY$aEDy75" resolve="no.uio.mLab.shared.runtime" />
        </node>
        <node concept="1E0d5M" id="ofcHXfYUaQ" role="1E1XAP">
          <ref role="1E0d5P" node="ofcHXfYTWj" resolve="no.uio.mLab.shared.translations" />
        </node>
      </node>
      <node concept="1E1JtA" id="2$xY$aEDy7O" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.core.runtime" />
        <property role="3LESm3" value="47bf15b2-e4c6-41f8-af94-4c062e947b5f" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="2$xY$aEDy7I" role="3LF7KH">
          <node concept="2Ry0Ak" id="2$xY$aEDy7J" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="2$xY$aEDy7K" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.core" />
              <node concept="2Ry0Ak" id="2$xY$aEDy7L" role="2Ry0An">
                <property role="2Ry0Am" value="runtime" />
                <node concept="2Ry0Ak" id="6zy3YvE45gy" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.decisions.core.runtime.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="2$xY$aEDyaK" role="3bR37C">
          <node concept="3bR9La" id="2$xY$aEDyaL" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="ffeo:mXGwHwhVPj" resolve="JDK" />
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="ofcHXfO24e" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.core.translations" />
        <property role="3LESm3" value="3d057672-91c8-42c2-82b2-0a0e5d5da4af" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="ofcHXfO24f" role="3LF7KH">
          <node concept="2Ry0Ak" id="ofcHXfO24g" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="ofcHXfO24h" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.core" />
              <node concept="2Ry0Ak" id="ofcHXfO24i" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.decisions.core.translations" />
                <node concept="2Ry0Ak" id="6zy3YvEtCLR" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.decisions.core.translations.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="ofcHXfO24k" role="3bR37C">
          <node concept="3bR9La" id="ofcHXfO24l" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="ffeo:mXGwHwhVPj" resolve="JDK" />
          </node>
        </node>
        <node concept="1SiIV0" id="ofcHXfYUhf" role="3bR37C">
          <node concept="3bR9La" id="ofcHXfYUhg" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="ofcHXfYTWj" resolve="no.uio.mLab.shared.translations" />
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="ofcHXfO28W" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.core.utils" />
        <property role="3LESm3" value="a056d4a0-d860-48c3-b68a-2c92f66c4abd" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="ofcHXfO28X" role="3LF7KH">
          <node concept="2Ry0Ak" id="ofcHXfO28Y" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="ofcHXfO28Z" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.core" />
              <node concept="2Ry0Ak" id="ofcHXfO290" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.decisions.core.utils" />
                <node concept="2Ry0Ak" id="6zy3YvEtCRG" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.decisions.core.utils.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="ofcHXfO292" role="3bR37C">
          <node concept="3bR9La" id="ofcHXfO293" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="ffeo:mXGwHwhVPj" resolve="JDK" />
          </node>
        </node>
        <node concept="1SiIV0" id="ofcHXfO2vO" role="3bR37C">
          <node concept="3bR9La" id="ofcHXfO2vP" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="al5i:2N1CSrzSKpi" resolve="com.mbeddr.mpsutil.plantuml.node" />
          </node>
        </node>
        <node concept="1SiIV0" id="ofcHXfO2vQ" role="3bR37C">
          <node concept="3bR9La" id="ofcHXfO2vR" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="ffeo:7Kfy9QB6KXW" resolve="jetbrains.mps.lang.core" />
          </node>
        </node>
        <node concept="1SiIV0" id="ofcHXfO2vS" role="3bR37C">
          <node concept="3bR9La" id="ofcHXfO2vT" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="2$xY$aEDy7U" resolve="no.uio.mLab.decisions.core" />
          </node>
        </node>
      </node>
      <node concept="1E1JtD" id="2$xY$aEDy7U" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.core" />
        <property role="3LESm3" value="c610a4eb-c47c-4f95-b568-11236971769c" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="2$xY$aEDy7P" role="3LF7KH">
          <node concept="2Ry0Ak" id="2$xY$aEDy7Q" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="2$xY$aEDy7R" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.core" />
              <node concept="2Ry0Ak" id="6zy3YvE45N6" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.decisions.core.mpl" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="2$xY$aEDyaR" role="3bR37C">
          <node concept="3bR9La" id="2$xY$aEDyaS" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="ffeo:mXGwHwhVPj" resolve="JDK" />
          </node>
        </node>
        <node concept="1SiIV0" id="2$xY$aEDyaT" role="3bR37C">
          <node concept="3bR9La" id="2$xY$aEDyaU" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="ffeo:7Kfy9QB6LfQ" resolve="jetbrains.mps.kernel" />
          </node>
        </node>
        <node concept="1SiIV0" id="2$xY$aEDyaV" role="3bR37C">
          <node concept="3bR9La" id="2$xY$aEDyaW" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="ffeo:1TaHNgiIbIQ" resolve="MPS.Core" />
          </node>
        </node>
        <node concept="1yeLz9" id="2$xY$aEDyaZ" role="1TViLv">
          <property role="TrG5h" value="no.uio.mLab.decisions.core#01" />
          <property role="3LESm3" value="df584c67-edad-46dc-b7ad-a5d9d88b868e" />
          <property role="2GAjPV" value="false" />
          <node concept="1SiIV0" id="7lYCqhuNPLJ" role="3bR37C">
            <node concept="3bR9La" id="7lYCqhuNPLK" role="1SiIV1">
              <property role="3bR36h" value="false" />
              <ref role="3bR37D" to="ffeo:mXGwHwhVPj" resolve="JDK" />
            </node>
          </node>
          <node concept="1SiIV0" id="6khVixymacb" role="3bR37C">
            <node concept="3bR9La" id="6khVixymacc" role="1SiIV1">
              <property role="3bR36h" value="false" />
              <ref role="3bR37D" node="4QUW3ee1aEc" resolve="no.uio.mLab.entities.core" />
            </node>
          </node>
          <node concept="1SiIV0" id="4B5aqq2l5mv" role="3bR37C">
            <node concept="3bR9La" id="4B5aqq2l5mw" role="1SiIV1">
              <property role="3bR36h" value="false" />
              <ref role="3bR37D" node="ofcHXfO28W" resolve="no.uio.mLab.decisions.core.utils" />
            </node>
          </node>
        </node>
        <node concept="1E0d5M" id="2$xY$aEDyhK" role="1E1XAP">
          <ref role="1E0d5P" node="2$xY$aEDy7O" resolve="no.uio.mLab.decisions.core.runtime" />
        </node>
        <node concept="1SiIV0" id="2$xY$aEDyhL" role="3bR37C">
          <node concept="1Busua" id="2$xY$aEDyhM" role="1SiIV1">
            <ref role="1Busuk" node="2$xY$aEDy9q" resolve="no.uio.mLab.shared" />
          </node>
        </node>
        <node concept="1SiIV0" id="2$xY$aEDzrh" role="3bR37C">
          <node concept="1Busua" id="2$xY$aEDzri" role="1SiIV1">
            <ref role="1Busuk" to="al5i:2N1CSrzSKpi" resolve="com.mbeddr.mpsutil.plantuml.node" />
          </node>
        </node>
        <node concept="1E0d5M" id="ofcHXfO2lK" role="1E1XAP">
          <ref role="1E0d5P" node="ofcHXfO28W" resolve="no.uio.mLab.decisions.core.utils" />
        </node>
        <node concept="1SiIV0" id="ofcHXfO2vZ" role="3bR37C">
          <node concept="3bR9La" id="ofcHXfO2w0" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="ofcHXfO24e" resolve="no.uio.mLab.decisions.core.translations" />
          </node>
        </node>
        <node concept="1SiIV0" id="ofcHXfO2_j" role="3bR37C">
          <node concept="3bR9La" id="ofcHXfO2_k" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="ofcHXfO28W" resolve="no.uio.mLab.decisions.core.utils" />
          </node>
        </node>
        <node concept="1SiIV0" id="4QUW3ee1cYj" role="3bR37C">
          <node concept="3bR9La" id="4QUW3ee1cYk" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="4QUW3ee1aEc" resolve="no.uio.mLab.entities.core" />
          </node>
        </node>
        <node concept="1SiIV0" id="4B5aqq8yzJT" role="3bR37C">
          <node concept="3bR9La" id="4B5aqq8yzJU" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="ffeo:7Kfy9QB6KYK" resolve="jetbrains.mps.baseLanguage.util" />
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="2$xY$aEDy91" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.data.microbiology.runtime" />
        <property role="3LESm3" value="c1b16c84-84e5-42ba-afd6-ea390be399d8" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="2$xY$aEDy8V" role="3LF7KH">
          <node concept="2Ry0Ak" id="2$xY$aEDy8W" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="2$xY$aEDy8X" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.data.microbiology" />
              <node concept="2Ry0Ak" id="2$xY$aEDy8Y" role="2Ry0An">
                <property role="2Ry0Am" value="runtime" />
                <node concept="2Ry0Ak" id="9zS80_vjgS" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.decisions.data.microbiology.runtime.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="ofcHXfO2AB" role="3bR37C">
          <node concept="3bR9La" id="ofcHXfO2AC" role="1SiIV1">
            <property role="3bR36h" value="true" />
            <ref role="3bR37D" node="2$xY$aEDy7O" resolve="no.uio.mLab.decisions.core.runtime" />
          </node>
        </node>
        <node concept="1SiIV0" id="ofcHXgdHuA" role="3bR37C">
          <node concept="3bR9La" id="ofcHXgdHuB" role="1SiIV1">
            <property role="3bR36h" value="true" />
            <ref role="3bR37D" node="2$xY$aEDy88" resolve="no.uio.mLab.entities.laboratoryTest.runtime" />
          </node>
        </node>
        <node concept="1SiIV0" id="6khVixymacm" role="3bR37C">
          <node concept="3bR9La" id="6khVixymacn" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="ffeo:mXGwHwhVPj" resolve="JDK" />
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="ofcHXfO2Te" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.data.microbiology.translations" />
        <property role="3LESm3" value="d3dbf33d-79b3-4b90-b918-dc1dec50f3b2" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="ofcHXfO2Th" role="3LF7KH">
          <node concept="2Ry0Ak" id="ofcHXfO33m" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="ofcHXfO38v" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.data.microbiology" />
              <node concept="2Ry0Ak" id="ofcHXfO3dC" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.decisions.data.microbiology.translations" />
                <node concept="2Ry0Ak" id="9zS80_vj8q" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.decisions.data.microbiology.translations.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="ofcHXfYUhM" role="3bR37C">
          <node concept="3bR9La" id="ofcHXfYUhN" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="ofcHXfYTWj" resolve="no.uio.mLab.shared.translations" />
          </node>
        </node>
      </node>
      <node concept="1E1JtD" id="2$xY$aEDy8O" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.data.microbiology" />
        <property role="3LESm3" value="6b1bb23e-8155-434e-8611-255c4637394b" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="2$xY$aEDy8J" role="3LF7KH">
          <node concept="2Ry0Ak" id="2$xY$aEDy8K" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="2$xY$aEDy8L" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.data.microbiology" />
              <node concept="2Ry0Ak" id="9zS80_viVI" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.decisions.data.microbiology.mpl" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="2$xY$aEDyc1" role="3bR37C">
          <node concept="3bR9La" id="2$xY$aEDyc2" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="ffeo:mXGwHwhVPj" resolve="JDK" />
          </node>
        </node>
        <node concept="1yeLz9" id="2$xY$aEDyc3" role="1TViLv">
          <property role="TrG5h" value="no.uio.mLab.decisions.data.microbiology#01" />
          <property role="3LESm3" value="65e4256a-fb6a-41e7-a574-fc4b1d50e1cf" />
          <property role="2GAjPV" value="false" />
        </node>
        <node concept="1SiIV0" id="2$xY$aEDyjv" role="3bR37C">
          <node concept="3bR9La" id="2$xY$aEDyjw" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="2$xY$aEDy91" resolve="no.uio.mLab.decisions.data.microbiology.runtime" />
          </node>
        </node>
        <node concept="1E0d5M" id="2$xY$aEDyjz" role="1E1XAP">
          <ref role="1E0d5P" node="2$xY$aEDy91" resolve="no.uio.mLab.decisions.data.microbiology.runtime" />
        </node>
        <node concept="1SiIV0" id="ofcHXfO3uU" role="3bR37C">
          <node concept="3bR9La" id="ofcHXfO3uV" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="ofcHXfO2Te" resolve="no.uio.mLab.decisions.data.microbiology.translations" />
          </node>
        </node>
        <node concept="1SiIV0" id="6khVixymacy" role="3bR37C">
          <node concept="3bR9La" id="6khVixymacz" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="5_LESNW0NMc" resolve="no.uio.mLab.decisions.references.microbiology" />
          </node>
        </node>
        <node concept="1SiIV0" id="6khVixymac$" role="3bR37C">
          <node concept="3bR9La" id="6khVixymac_" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="2$xY$aEDy9q" resolve="no.uio.mLab.shared" />
          </node>
        </node>
        <node concept="1SiIV0" id="6khVixymacA" role="3bR37C">
          <node concept="3bR9La" id="6khVixymacB" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="4QUW3edwKrx" resolve="no.uio.mLab.decisions.references.laboratoryTest" />
          </node>
        </node>
        <node concept="1SiIV0" id="6khVixymacC" role="3bR37C">
          <node concept="1Busua" id="6khVixymacD" role="1SiIV1">
            <ref role="1Busuk" node="2$xY$aEDy7U" resolve="no.uio.mLab.decisions.core" />
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="7lYCqhu39kg" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.data.laboratoryTest.runtime" />
        <property role="3LESm3" value="df774b08-5176-447c-9e66-981e9ddc5a27" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="7lYCqhu39kh" role="3LF7KH">
          <node concept="2Ry0Ak" id="7lYCqhu39ki" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="7lYCqhu39kj" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.data.laboratoryTest" />
              <node concept="2Ry0Ak" id="7lYCqhu39kk" role="2Ry0An">
                <property role="2Ry0Am" value="runtime" />
                <node concept="2Ry0Ak" id="5_LESNVZLvx" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.decisions.data.laboratoryTests.runtime.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="7lYCqhu39km" role="3bR37C">
          <node concept="3bR9La" id="7lYCqhu39kn" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="ffeo:mXGwHwhVPj" resolve="JDK" />
          </node>
        </node>
        <node concept="1SiIV0" id="7lYCqhu39ko" role="3bR37C">
          <node concept="3bR9La" id="7lYCqhu39kp" role="1SiIV1">
            <property role="3bR36h" value="true" />
            <ref role="3bR37D" node="2$xY$aEDy7O" resolve="no.uio.mLab.decisions.core.runtime" />
          </node>
        </node>
        <node concept="1SiIV0" id="7lYCqhu39kq" role="3bR37C">
          <node concept="3bR9La" id="7lYCqhu39kr" role="1SiIV1">
            <property role="3bR36h" value="true" />
            <ref role="3bR37D" node="2$xY$aEDy88" resolve="no.uio.mLab.entities.laboratoryTest.runtime" />
          </node>
        </node>
        <node concept="1SiIV0" id="7lYCqhuYHXd" role="3bR37C">
          <node concept="3bR9La" id="7lYCqhuYHXe" role="1SiIV1">
            <property role="3bR36h" value="true" />
            <ref role="3bR37D" node="4QUW3edwL$o" resolve="no.uio.mLab.decisions.references.laboratoryTest.runtime" />
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="7lYCqhu38zh" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.data.laboratoryTest.translations" />
        <property role="3LESm3" value="41ca56ea-79ba-4890-80ad-e9d6ba56f81d" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="7lYCqhu38zk" role="3LF7KH">
          <node concept="2Ry0Ak" id="7lYCqhu38LK" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="7lYCqhu38Tn" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.data.laboratoryTest" />
              <node concept="2Ry0Ak" id="7lYCqhu3a1y" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.decisions.data.laboratoryTests.translations" />
                <node concept="2Ry0Ak" id="6zy3YvEtDr3" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.decisions.data.laboratoryTests.translations.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="7lYCqhu39dh" role="3bR37C">
          <node concept="3bR9La" id="7lYCqhu39di" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="ffeo:mXGwHwhVPj" resolve="JDK" />
          </node>
        </node>
        <node concept="1SiIV0" id="7lYCqhu3ahZ" role="3bR37C">
          <node concept="3bR9La" id="7lYCqhu3ai0" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="ofcHXfYTWj" resolve="no.uio.mLab.shared.translations" />
          </node>
        </node>
      </node>
      <node concept="1E1JtD" id="7lYCqhu37uZ" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.data.laboratoryTest" />
        <property role="3LESm3" value="93a27952-3baa-41ca-9e72-e2a9cf1b3f77" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="7lYCqhu37v2" role="3LF7KH">
          <node concept="2Ry0Ak" id="7lYCqhu37H2" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="7lYCqhu37KJ" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.data.laboratoryTest" />
              <node concept="2Ry0Ak" id="5_LESNVZMN5" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.decisions.data.laboratoryTest.mpl" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="7lYCqhu37WB" role="3bR37C">
          <node concept="3bR9La" id="7lYCqhu37WC" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="2$xY$aEDy9q" resolve="no.uio.mLab.shared" />
          </node>
        </node>
        <node concept="1yeLz9" id="7lYCqhu37WF" role="1TViLv">
          <property role="TrG5h" value="no.uio.mLab.decisions.data.laboratoryTest#01" />
          <property role="3LESm3" value="7550c55a-cacd-4dc4-a10d-c1b3bade96fc" />
          <property role="2GAjPV" value="false" />
        </node>
        <node concept="1E0d5M" id="7lYCqhu39N3" role="1E1XAP">
          <ref role="1E0d5P" node="7lYCqhu38zh" resolve="no.uio.mLab.decisions.data.laboratoryTest.translations" />
        </node>
        <node concept="1E0d5M" id="7lYCqhu39UL" role="1E1XAP">
          <ref role="1E0d5P" node="7lYCqhu39kg" resolve="no.uio.mLab.decisions.data.laboratoryTest.runtime" />
        </node>
        <node concept="1E0d5M" id="7lYCqhu3aQS" role="1E1XAP">
          <ref role="1E0d5P" node="7lYCqhu3awV" resolve="no.uio.mLab.decisions.data.laboratoryTest.numberResult.runtime" />
        </node>
        <node concept="1SiIV0" id="7lYCqhu3bsB" role="3bR37C">
          <node concept="3bR9La" id="7lYCqhu3bsC" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="7lYCqhu38zh" resolve="no.uio.mLab.decisions.data.laboratoryTest.translations" />
          </node>
        </node>
        <node concept="1SiIV0" id="6khVixymacX" role="3bR37C">
          <node concept="3bR9La" id="6khVixymacY" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="ffeo:mXGwHwhVPj" resolve="JDK" />
          </node>
        </node>
        <node concept="1SiIV0" id="6khVixymad1" role="3bR37C">
          <node concept="3bR9La" id="6khVixymad2" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="7lYCqhu37uZ" resolve="no.uio.mLab.decisions.data.laboratoryTest" />
          </node>
        </node>
        <node concept="1SiIV0" id="4B5aqq5Aq3u" role="3bR37C">
          <node concept="3bR9La" id="4B5aqq5Aq3v" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="4QUW3edwKrx" resolve="no.uio.mLab.decisions.references.laboratoryTest" />
          </node>
        </node>
        <node concept="1SiIV0" id="4B5aqq5Aq3w" role="3bR37C">
          <node concept="1Busua" id="4B5aqq5Aq3x" role="1SiIV1">
            <ref role="1Busuk" node="2$xY$aEDy7U" resolve="no.uio.mLab.decisions.core" />
          </node>
        </node>
        <node concept="1SiIV0" id="5uQLXCtup_k" role="3bR37C">
          <node concept="3bR9La" id="5uQLXCtup_l" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="ofcHXfO28W" resolve="no.uio.mLab.decisions.core.utils" />
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="7lYCqhu3awV" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.data.laboratoryTest.numberResult.runtime" />
        <property role="3LESm3" value="92b89cc9-e1b9-4129-b881-e2eface30697" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="7lYCqhu3awW" role="3LF7KH">
          <node concept="2Ry0Ak" id="7lYCqhu3awX" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="7lYCqhu3awY" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.data.laboratoryTest.numberResult" />
              <node concept="2Ry0Ak" id="7lYCqhu3awZ" role="2Ry0An">
                <property role="2Ry0Am" value="runtime" />
                <node concept="2Ry0Ak" id="5_LESNVZN0A" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.decisions.data.laboratoryTest.numberResult.runtime.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="7lYCqhu3ax1" role="3bR37C">
          <node concept="3bR9La" id="7lYCqhu3ax2" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="ffeo:mXGwHwhVPj" resolve="JDK" />
          </node>
        </node>
        <node concept="1SiIV0" id="7lYCqhu3ax3" role="3bR37C">
          <node concept="3bR9La" id="7lYCqhu3ax4" role="1SiIV1">
            <property role="3bR36h" value="true" />
            <ref role="3bR37D" node="2$xY$aEDy7O" resolve="no.uio.mLab.decisions.core.runtime" />
          </node>
        </node>
        <node concept="1SiIV0" id="7lYCqhuYHXy" role="3bR37C">
          <node concept="3bR9La" id="7lYCqhuYHXz" role="1SiIV1">
            <property role="3bR36h" value="true" />
            <ref role="3bR37D" node="4QUW3edwL$o" resolve="no.uio.mLab.decisions.references.laboratoryTest.runtime" />
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="7lYCqhu3ax7" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.data.laboratoryTest.numberResult.translations" />
        <property role="3LESm3" value="4fdfc272-98cd-48af-b8f3-6668bc40eee4" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="7lYCqhu3ax8" role="3LF7KH">
          <node concept="2Ry0Ak" id="7lYCqhu3ax9" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="7lYCqhu3axa" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.data.laboratoryTest.numberResult" />
              <node concept="2Ry0Ak" id="7lYCqhu3axb" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.decisions.data.laboratoryTest.numberResult.translations" />
                <node concept="2Ry0Ak" id="5_LESNVZNoW" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.decisions.data.laboratoryTest.numberResult.translations.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="7lYCqhu3axd" role="3bR37C">
          <node concept="3bR9La" id="7lYCqhu3axe" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="ffeo:mXGwHwhVPj" resolve="JDK" />
          </node>
        </node>
        <node concept="1SiIV0" id="7lYCqhu3axf" role="3bR37C">
          <node concept="3bR9La" id="7lYCqhu3axg" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="ofcHXfYTWj" resolve="no.uio.mLab.shared.translations" />
          </node>
        </node>
      </node>
      <node concept="1E1JtD" id="7lYCqhu3axh" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.data.laboratoryTest.numberResult" />
        <property role="3LESm3" value="72a150d4-97f8-489b-aded-022e7dabd8b9" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="7lYCqhu3axi" role="3LF7KH">
          <node concept="2Ry0Ak" id="7lYCqhu3axj" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="7lYCqhu3axk" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.data.laboratoryTest.numberResult" />
              <node concept="2Ry0Ak" id="5_LESNVZNvW" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.decisions.data.laboratoryTest.numberResult.mpl" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="7lYCqhu3axm" role="3bR37C">
          <node concept="3bR9La" id="7lYCqhu3axn" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="2$xY$aEDy9q" resolve="no.uio.mLab.shared" />
          </node>
        </node>
        <node concept="1yeLz9" id="7lYCqhu3axq" role="1TViLv">
          <property role="TrG5h" value="no.uio.mLab.decisions.data.laboratoryTest.numberResult#01" />
          <property role="3LESm3" value="1fc41d1c-de3b-4d9b-80f9-d0f4ed7beff5" />
          <property role="2GAjPV" value="false" />
          <node concept="1SiIV0" id="4B5aqq5Aq3T" role="3bR37C">
            <node concept="3bR9La" id="4B5aqq5Aq3U" role="1SiIV1">
              <property role="3bR36h" value="false" />
              <ref role="3bR37D" node="7lYCqhu37uZ" resolve="no.uio.mLab.decisions.data.laboratoryTest" />
            </node>
          </node>
        </node>
        <node concept="1E0d5M" id="7lYCqhu3axr" role="1E1XAP">
          <ref role="1E0d5P" node="7lYCqhu3ax7" resolve="no.uio.mLab.decisions.data.laboratoryTest.numberResult.translations" />
        </node>
        <node concept="1E0d5M" id="7lYCqhu3axs" role="1E1XAP">
          <ref role="1E0d5P" node="7lYCqhu3awV" resolve="no.uio.mLab.decisions.data.laboratoryTest.numberResult.runtime" />
        </node>
        <node concept="1E0d5M" id="7lYCqhu3bce" role="1E1XAP">
          <ref role="1E0d5P" node="7lYCqhu39kg" resolve="no.uio.mLab.decisions.data.laboratoryTest.runtime" />
        </node>
        <node concept="1E0d5M" id="7lYCqhu3chu" role="1E1XAP">
          <ref role="1E0d5P" node="7lYCqhu3b$8" resolve="no.uio.mLab.decisions.data.laboratoryTest.textualResult.runtime" />
        </node>
        <node concept="1SiIV0" id="7lYCqhu3cqJ" role="3bR37C">
          <node concept="3bR9La" id="7lYCqhu3cqK" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="7lYCqhu3ax7" resolve="no.uio.mLab.decisions.data.laboratoryTest.numberResult.translations" />
          </node>
        </node>
        <node concept="1SiIV0" id="4B5aqq2l5nq" role="3bR37C">
          <node concept="3bR9La" id="4B5aqq2l5nr" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="7lYCqhu3axh" resolve="no.uio.mLab.decisions.data.laboratoryTest.numberResult" />
          </node>
        </node>
        <node concept="1SiIV0" id="4B5aqq5Aq3P" role="3bR37C">
          <node concept="3bR9La" id="4B5aqq5Aq3Q" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="4QUW3edwKrx" resolve="no.uio.mLab.decisions.references.laboratoryTest" />
          </node>
        </node>
        <node concept="1SiIV0" id="4B5aqq5Aq3R" role="3bR37C">
          <node concept="1Busua" id="4B5aqq5Aq3S" role="1SiIV1">
            <ref role="1Busuk" node="2$xY$aEDy7U" resolve="no.uio.mLab.decisions.core" />
          </node>
        </node>
        <node concept="1SiIV0" id="5uQLXCtup_D" role="3bR37C">
          <node concept="3bR9La" id="5uQLXCtup_E" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="ofcHXfO28W" resolve="no.uio.mLab.decisions.core.utils" />
          </node>
        </node>
        <node concept="1SiIV0" id="5uQLXCtup_F" role="3bR37C">
          <node concept="3bR9La" id="5uQLXCtup_G" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="7lYCqhu37uZ" resolve="no.uio.mLab.decisions.data.laboratoryTest" />
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="7lYCqhu3b$8" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.data.laboratoryTest.textualResult.runtime" />
        <property role="3LESm3" value="4706ad63-cdf1-46ba-8c2f-be83f87cf558" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="7lYCqhu3b$9" role="3LF7KH">
          <node concept="2Ry0Ak" id="7lYCqhu3b$a" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="7lYCqhu3b$b" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.data.laboratoryTest.textualResult" />
              <node concept="2Ry0Ak" id="7lYCqhu3b$c" role="2Ry0An">
                <property role="2Ry0Am" value="runtime" />
                <node concept="2Ry0Ak" id="5_LESNVZNAW" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.decisions.data.laboratoryTest.textualResult.runtime.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="7lYCqhu3b$e" role="3bR37C">
          <node concept="3bR9La" id="7lYCqhu3b$f" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="ffeo:mXGwHwhVPj" resolve="JDK" />
          </node>
        </node>
        <node concept="1SiIV0" id="7lYCqhu3b$g" role="3bR37C">
          <node concept="3bR9La" id="7lYCqhu3b$h" role="1SiIV1">
            <property role="3bR36h" value="true" />
            <ref role="3bR37D" node="2$xY$aEDy7O" resolve="no.uio.mLab.decisions.core.runtime" />
          </node>
        </node>
        <node concept="1SiIV0" id="7lYCqhu3b$i" role="3bR37C">
          <node concept="3bR9La" id="7lYCqhu3b$j" role="1SiIV1">
            <property role="3bR36h" value="true" />
            <ref role="3bR37D" node="2$xY$aEDy88" resolve="no.uio.mLab.entities.laboratoryTest.runtime" />
          </node>
        </node>
        <node concept="1SiIV0" id="7lYCqhuYHXR" role="3bR37C">
          <node concept="3bR9La" id="7lYCqhuYHXS" role="1SiIV1">
            <property role="3bR36h" value="true" />
            <ref role="3bR37D" node="4QUW3edwL$o" resolve="no.uio.mLab.decisions.references.laboratoryTest.runtime" />
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="7lYCqhu3b$k" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.data.laboratoryTest.textualResult.translations" />
        <property role="3LESm3" value="e1982248-fbf3-4b23-85b3-4ecfafde8a77" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="7lYCqhu3b$l" role="3LF7KH">
          <node concept="2Ry0Ak" id="7lYCqhu3b$m" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="7lYCqhu3b$n" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.data.laboratoryTest.textualResult" />
              <node concept="2Ry0Ak" id="7lYCqhu3b$o" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.decisions.data.laboratoryTest.textualResult.translations" />
                <node concept="2Ry0Ak" id="5_LESNVZNLr" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.decisions.data.laboratoryTest.textualResult.translations.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="7lYCqhu3b$q" role="3bR37C">
          <node concept="3bR9La" id="7lYCqhu3b$r" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="ffeo:mXGwHwhVPj" resolve="JDK" />
          </node>
        </node>
        <node concept="1SiIV0" id="7lYCqhu3b$s" role="3bR37C">
          <node concept="3bR9La" id="7lYCqhu3b$t" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="ofcHXfYTWj" resolve="no.uio.mLab.shared.translations" />
          </node>
        </node>
      </node>
      <node concept="1E1JtD" id="7lYCqhu3b$u" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.data.laboratoryTest.textualResult" />
        <property role="3LESm3" value="60f87c04-0cd8-4174-8d1a-4a254a9e2f58" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="7lYCqhu3b$v" role="3LF7KH">
          <node concept="2Ry0Ak" id="7lYCqhu3b$w" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="7lYCqhu3b$x" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.data.laboratoryTest.textualResult" />
              <node concept="2Ry0Ak" id="5_LESNVZNSr" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.decisions.data.laboratoryTest.textualResult.mpl" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="7lYCqhu3b$z" role="3bR37C">
          <node concept="3bR9La" id="7lYCqhu3b$$" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="2$xY$aEDy9q" resolve="no.uio.mLab.shared" />
          </node>
        </node>
        <node concept="1yeLz9" id="7lYCqhu3b$B" role="1TViLv">
          <property role="TrG5h" value="no.uio.mLab.decisions.data.laboratoryTest.textualResult#01" />
          <property role="3LESm3" value="9cfe9184-7dfa-4cdf-b37a-50a97ada17f3" />
          <property role="2GAjPV" value="false" />
          <node concept="1SiIV0" id="4B5aqq5Aq4i" role="3bR37C">
            <node concept="3bR9La" id="4B5aqq5Aq4j" role="1SiIV1">
              <property role="3bR36h" value="false" />
              <ref role="3bR37D" node="7lYCqhu37uZ" resolve="no.uio.mLab.decisions.data.laboratoryTest" />
            </node>
          </node>
        </node>
        <node concept="1E0d5M" id="7lYCqhu3b$C" role="1E1XAP">
          <ref role="1E0d5P" node="7lYCqhu3b$k" resolve="no.uio.mLab.decisions.data.laboratoryTest.textualResult.translations" />
        </node>
        <node concept="1E0d5M" id="7lYCqhu3b$D" role="1E1XAP">
          <ref role="1E0d5P" node="7lYCqhu3b$8" resolve="no.uio.mLab.decisions.data.laboratoryTest.textualResult.runtime" />
        </node>
        <node concept="1E0d5M" id="7lYCqhu3b$G" role="1E1XAP">
          <ref role="1E0d5P" node="7lYCqhu39kg" resolve="no.uio.mLab.decisions.data.laboratoryTest.runtime" />
        </node>
        <node concept="1SiIV0" id="7lYCqhu3cr4" role="3bR37C">
          <node concept="3bR9La" id="7lYCqhu3cr5" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="7lYCqhu3b$k" resolve="no.uio.mLab.decisions.data.laboratoryTest.textualResult.translations" />
          </node>
        </node>
        <node concept="1SiIV0" id="4B5aqq2l5nL" role="3bR37C">
          <node concept="3bR9La" id="4B5aqq2l5nM" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="7lYCqhu3b$u" resolve="no.uio.mLab.decisions.data.laboratoryTest.textualResult" />
          </node>
        </node>
        <node concept="1SiIV0" id="4B5aqq5Aq4e" role="3bR37C">
          <node concept="3bR9La" id="4B5aqq5Aq4f" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="4QUW3edwKrx" resolve="no.uio.mLab.decisions.references.laboratoryTest" />
          </node>
        </node>
        <node concept="1SiIV0" id="4B5aqq5Aq4g" role="3bR37C">
          <node concept="1Busua" id="4B5aqq5Aq4h" role="1SiIV1">
            <ref role="1Busuk" node="2$xY$aEDy7U" resolve="no.uio.mLab.decisions.core" />
          </node>
        </node>
        <node concept="1SiIV0" id="5uQLXCtupA0" role="3bR37C">
          <node concept="3bR9La" id="5uQLXCtupA1" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="ffeo:mXGwHwhVPj" resolve="JDK" />
          </node>
        </node>
        <node concept="1SiIV0" id="5uQLXCtupA2" role="3bR37C">
          <node concept="3bR9La" id="5uQLXCtupA3" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="ofcHXfO28W" resolve="no.uio.mLab.decisions.core.utils" />
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="ofcHXfPsfZ" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.data.patient.runtime" />
        <property role="3LESm3" value="a7eb0f4a-92c2-4388-8385-f6e45689b291" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="ofcHXfPsg0" role="3LF7KH">
          <node concept="2Ry0Ak" id="ofcHXfPsg1" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="ofcHXfPsg2" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.data.patient" />
              <node concept="2Ry0Ak" id="ofcHXfPsg3" role="2Ry0An">
                <property role="2Ry0Am" value="runtime" />
                <node concept="2Ry0Ak" id="6zy3YvEtG1X" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.decisions.data.patient.runtime.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="ofcHXg7g6P" role="3bR37C">
          <node concept="3bR9La" id="ofcHXg7g6Q" role="1SiIV1">
            <property role="3bR36h" value="true" />
            <ref role="3bR37D" node="2$xY$aEDy7O" resolve="no.uio.mLab.decisions.core.runtime" />
          </node>
        </node>
        <node concept="1SiIV0" id="7lYCqhvdaiJ" role="3bR37C">
          <node concept="3bR9La" id="7lYCqhvdaiK" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="ffeo:mXGwHwhVPj" resolve="JDK" />
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="2$xY$aEDy9V" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.data.patient.translations" />
        <property role="3LESm3" value="31c45a1a-5f8c-410d-ab4e-da0200467b2b" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="2$xY$aEDy9P" role="3LF7KH">
          <node concept="2Ry0Ak" id="2$xY$aEDy9Q" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="2$xY$aEDy9R" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.data.patient" />
              <node concept="2Ry0Ak" id="2$xY$aEDy9S" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.decisions.data.patient.translations" />
                <node concept="2Ry0Ak" id="6zy3YvEtG6p" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.decisions.data.patient.translations.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="ofcHXfYUiq" role="3bR37C">
          <node concept="3bR9La" id="ofcHXfYUir" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="ofcHXfYTWj" resolve="no.uio.mLab.shared.translations" />
          </node>
        </node>
      </node>
      <node concept="1E1JtD" id="2$xY$aEDy8I" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.data.patient" />
        <property role="3LESm3" value="f130674d-23fb-4e9b-a200-548653f5fd0b" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="2$xY$aEDy8D" role="3LF7KH">
          <node concept="2Ry0Ak" id="2$xY$aEDy8E" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="2$xY$aEDy8F" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.data.patient" />
              <node concept="2Ry0Ak" id="6zy3YvEtG9n" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.decisions.data.patient.mpl" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="2$xY$aEDybP" role="3bR37C">
          <node concept="3bR9La" id="2$xY$aEDybQ" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="ffeo:mXGwHwhVPj" resolve="JDK" />
          </node>
        </node>
        <node concept="1yeLz9" id="2$xY$aEDybR" role="1TViLv">
          <property role="TrG5h" value="no.uio.mLab.decisions.data.patient#01" />
          <property role="3LESm3" value="be0f8a44-ea87-4d29-9781-c6c500c988bd" />
          <property role="2GAjPV" value="false" />
        </node>
        <node concept="1SiIV0" id="2$xY$aEDyjb" role="3bR37C">
          <node concept="3bR9La" id="2$xY$aEDyjc" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="2$xY$aEDy9V" resolve="no.uio.mLab.decisions.data.patient.translations" />
          </node>
        </node>
        <node concept="1SiIV0" id="2$xY$aEDyjd" role="3bR37C">
          <node concept="3bR9La" id="2$xY$aEDyje" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="2$xY$aEDy9q" resolve="no.uio.mLab.shared" />
          </node>
        </node>
        <node concept="1E0d5M" id="2$xY$aEDyjh" role="1E1XAP">
          <ref role="1E0d5P" node="2$xY$aEDy9V" resolve="no.uio.mLab.decisions.data.patient.translations" />
        </node>
        <node concept="1SiIV0" id="2$xY$aEDyji" role="3bR37C">
          <node concept="1Busua" id="2$xY$aEDyjj" role="1SiIV1">
            <ref role="1Busuk" node="2$xY$aEDy7U" resolve="no.uio.mLab.decisions.core" />
          </node>
        </node>
        <node concept="1E0d5M" id="ofcHXfPxIp" role="1E1XAP">
          <ref role="1E0d5P" node="ofcHXfPsfZ" resolve="no.uio.mLab.decisions.data.patient.runtime" />
        </node>
        <node concept="1SiIV0" id="2FjKBCQxiMP" role="3bR37C">
          <node concept="3bR9La" id="2FjKBCQxiMQ" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="ofcHXfO28W" resolve="no.uio.mLab.decisions.core.utils" />
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="4QUW3ee1cna" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.entities.core.runtime" />
        <property role="3LESm3" value="732ab46e-6b80-4fe7-9b5f-ad36d4719d62" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="4QUW3ee1cnd" role="3LF7KH">
          <node concept="2Ry0Ak" id="4QUW3ee1c$M" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="4QUW3ee1cFV" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.entities.core" />
              <node concept="2Ry0Ak" id="4QUW3ee1cN4" role="2Ry0An">
                <property role="2Ry0Am" value="runtime" />
                <node concept="2Ry0Ak" id="4QUW3ee1cUd" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.entities.core.runtime.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="6khVixymas6" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.entities.core.translations" />
        <property role="3LESm3" value="e375b23a-63f7-4ac0-8e48-28b0ce172a49" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="6khVixymas7" role="3LF7KH">
          <node concept="2Ry0Ak" id="6khVixymas8" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="6khVixymas9" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.entities.core" />
              <node concept="2Ry0Ak" id="6khVixymasa" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.entities.core.translations" />
                <node concept="2Ry0Ak" id="6khVixymaK6" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.entities.core.translations.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="6khVixymaRu" role="3bR37C">
          <node concept="3bR9La" id="6khVixymaRv" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="ofcHXfYTWj" resolve="no.uio.mLab.shared.translations" />
          </node>
        </node>
      </node>
      <node concept="1E1JtD" id="4QUW3ee1aEc" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.entities.core" />
        <property role="3LESm3" value="33c0f773-be29-48e3-9587-40dc4b3c54f0" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="4QUW3ee1aEf" role="3LF7KH">
          <node concept="2Ry0Ak" id="4QUW3ee1bOU" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="4QUW3ee1bVL" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.entities.core" />
              <node concept="2Ry0Ak" id="4QUW3ee1c2C" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.entities.core.mpl" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1yeLz9" id="4QUW3ee1c7k" role="1TViLv">
          <property role="TrG5h" value="no.uio.mLab.entities.core#01" />
          <property role="3LESm3" value="73c315f9-e771-4e27-ada7-fb954e99248b" />
          <property role="2GAjPV" value="false" />
        </node>
        <node concept="1E0d5M" id="4QUW3ee1d71" role="1E1XAP">
          <ref role="1E0d5P" node="4QUW3ee1cna" resolve="no.uio.mLab.entities.core.runtime" />
        </node>
        <node concept="1SiIV0" id="4QUW3ef34pF" role="3bR37C">
          <node concept="1Busua" id="4QUW3ef34pG" role="1SiIV1">
            <ref role="1Busuk" node="2$xY$aEDy9q" resolve="no.uio.mLab.shared" />
          </node>
        </node>
        <node concept="1E0d5M" id="6khVixymaR_" role="1E1XAP">
          <ref role="1E0d5P" node="6khVixymas6" resolve="no.uio.mLab.entities.core.translations" />
        </node>
        <node concept="1SiIV0" id="6khVixymb22" role="3bR37C">
          <node concept="3bR9La" id="6khVixymb23" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="6khVixymas6" resolve="no.uio.mLab.entities.core.translations" />
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="2$xY$aEDy7v" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.entities.biochemistry.runtime" />
        <property role="3LESm3" value="147864cd-42bc-45c4-9c05-6bcaa971c3a9" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="2$xY$aEDy7p" role="3LF7KH">
          <node concept="2Ry0Ak" id="2$xY$aEDy7q" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="2$xY$aEDy7r" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.entities.biochemistry" />
              <node concept="2Ry0Ak" id="2$xY$aEDy7s" role="2Ry0An">
                <property role="2Ry0Am" value="runtime" />
                <node concept="2Ry0Ak" id="ofcHXfZ3rZ" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.entities.biochemistry.runtime.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="ofcHXfZ3$W" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.entities.biochemistry.translations" />
        <property role="3LESm3" value="7e31a67a-a4f2-4350-b189-6eca2c9bb1a9" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="ofcHXfZ3$X" role="3LF7KH">
          <node concept="2Ry0Ak" id="ofcHXfZ3$Y" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="ofcHXfZ3$Z" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.entities.biochemistry" />
              <node concept="2Ry0Ak" id="ofcHXfZ3_0" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.entities.biochemistry.translations" />
                <node concept="2Ry0Ak" id="ofcHXfZ3KB" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.entities.biochemistry.translations.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="ofcHXfZ3Pr" role="3bR37C">
          <node concept="3bR9La" id="ofcHXfZ3Ps" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="ofcHXfYTWj" resolve="no.uio.mLab.shared.translations" />
          </node>
        </node>
      </node>
      <node concept="1E1JtD" id="2$xY$aEDy7b" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.entities.biochemistry" />
        <property role="3LESm3" value="284ce324-ec91-4245-a186-fefe6b9a1ea8" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="2$xY$aEDy76" role="3LF7KH">
          <node concept="2Ry0Ak" id="2$xY$aEDy77" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="2$xY$aEDy78" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.entities.biochemistry" />
              <node concept="2Ry0Ak" id="2$xY$aEDy79" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.entities.biochemistry.mpl" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="2$xY$aEDygL" role="3bR37C">
          <node concept="3bR9La" id="2$xY$aEDygM" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="2$xY$aEDy9q" resolve="no.uio.mLab.shared" />
          </node>
        </node>
        <node concept="1E0d5M" id="2$xY$aEDygN" role="1E1XAP">
          <ref role="1E0d5P" node="2$xY$aEDy7v" resolve="no.uio.mLab.entities.biochemistry.runtime" />
        </node>
        <node concept="1SiIV0" id="2$xY$aEDygO" role="3bR37C">
          <node concept="1Busua" id="2$xY$aEDygP" role="1SiIV1">
            <ref role="1Busuk" node="2$xY$aEDy8x" resolve="no.uio.mLab.entities.laboratoryTest" />
          </node>
        </node>
        <node concept="1SiIV0" id="2$xY$aEDygQ" role="3bR37C">
          <node concept="1Busua" id="2$xY$aEDygR" role="1SiIV1">
            <ref role="1Busuk" node="2$xY$aEDy9O" resolve="no.uio.mLab.entities.specialistType" />
          </node>
        </node>
        <node concept="1E0d5M" id="ofcHXfZ3Py" role="1E1XAP">
          <ref role="1E0d5P" node="ofcHXfZ3$W" resolve="no.uio.mLab.entities.biochemistry.translations" />
        </node>
        <node concept="1SiIV0" id="ofcHXfZ3VR" role="3bR37C">
          <node concept="3bR9La" id="ofcHXfZ3VS" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="ofcHXfZ3$W" resolve="no.uio.mLab.entities.biochemistry.translations" />
          </node>
        </node>
        <node concept="1SiIV0" id="ofcHXgdGZx" role="3bR37C">
          <node concept="3bR9La" id="ofcHXgdGZy" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="2$xY$aEDy7b" resolve="no.uio.mLab.entities.biochemistry" />
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="2$xY$aEDy88" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.entities.laboratoryTest.runtime" />
        <property role="3LESm3" value="c4c71b02-3736-455d-8937-9cdbab70a659" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="2$xY$aEDy82" role="3LF7KH">
          <node concept="2Ry0Ak" id="2$xY$aEDy83" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="2$xY$aEDy84" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.entities.laboratoryTest" />
              <node concept="2Ry0Ak" id="2$xY$aEDy85" role="2Ry0An">
                <property role="2Ry0Am" value="runtime" />
                <node concept="2Ry0Ak" id="2$xY$aEDy86" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.entities.laboratoryTest.runtime.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="ofcHXfO2_N" role="3bR37C">
          <node concept="3bR9La" id="ofcHXfO2_O" role="1SiIV1">
            <property role="3bR36h" value="true" />
            <ref role="3bR37D" node="2$xY$aEDy7O" resolve="no.uio.mLab.decisions.core.runtime" />
          </node>
        </node>
        <node concept="1SiIV0" id="ofcHXfYUj7" role="3bR37C">
          <node concept="3bR9La" id="ofcHXfYUj8" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="2$xY$aEDy75" resolve="no.uio.mLab.shared.runtime" />
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="ofcHXgdH7h" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.entities.laboratoryTest.translations" />
        <property role="3LESm3" value="ab199cc3-2aae-43d9-8420-03393e76915b" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="ofcHXgdH7i" role="3LF7KH">
          <node concept="2Ry0Ak" id="ofcHXgdH7j" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="ofcHXgdH7k" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.entities.laboratoryTest" />
              <node concept="2Ry0Ak" id="ofcHXgdH7l" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.entities.laboratoryTest.translations" />
                <node concept="2Ry0Ak" id="ofcHXgdHjv" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.entities.laboratoryTest.translations.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="ofcHXgdH7n" role="3bR37C">
          <node concept="3bR9La" id="ofcHXgdH7o" role="1SiIV1">
            <property role="3bR36h" value="true" />
            <ref role="3bR37D" node="2$xY$aEDy7O" resolve="no.uio.mLab.decisions.core.runtime" />
          </node>
        </node>
        <node concept="1SiIV0" id="ofcHXgdH7p" role="3bR37C">
          <node concept="3bR9La" id="ofcHXgdH7q" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="2$xY$aEDy75" resolve="no.uio.mLab.shared.runtime" />
          </node>
        </node>
        <node concept="1SiIV0" id="ofcHXgdHoW" role="3bR37C">
          <node concept="3bR9La" id="ofcHXgdHoX" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="ofcHXfYTWj" resolve="no.uio.mLab.shared.translations" />
          </node>
        </node>
      </node>
      <node concept="1E1JtD" id="2$xY$aEDy8x" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.entities.laboratoryTest" />
        <property role="3LESm3" value="cae652fa-7f6e-4122-8bd5-0b26c8b4eec8" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="2$xY$aEDy8s" role="3LF7KH">
          <node concept="2Ry0Ak" id="2$xY$aEDy8t" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="2$xY$aEDy8u" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.entities.laboratoryTest" />
              <node concept="2Ry0Ak" id="2$xY$aEDy8v" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.entities.laboratoryTest.mpl" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="2$xY$aEDyiZ" role="3bR37C">
          <node concept="3bR9La" id="2$xY$aEDyj0" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="2$xY$aEDy9q" resolve="no.uio.mLab.shared" />
          </node>
        </node>
        <node concept="1E0d5M" id="2$xY$aEDyj1" role="1E1XAP">
          <ref role="1E0d5P" node="2$xY$aEDy88" resolve="no.uio.mLab.entities.laboratoryTest.runtime" />
        </node>
        <node concept="1SiIV0" id="ofcHXgdGZY" role="3bR37C">
          <node concept="3bR9La" id="ofcHXgdGZZ" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="ffeo:mXGwHwhVPj" resolve="JDK" />
          </node>
        </node>
        <node concept="1SiIV0" id="ofcHXgdH00" role="3bR37C">
          <node concept="1Busua" id="ofcHXgdH01" role="1SiIV1">
            <ref role="1Busuk" node="2$xY$aEDy9q" resolve="no.uio.mLab.shared" />
          </node>
        </node>
        <node concept="1SiIV0" id="ofcHXgdHp3" role="3bR37C">
          <node concept="3bR9La" id="ofcHXgdHp4" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="ofcHXgdH7h" resolve="no.uio.mLab.entities.laboratoryTest.translations" />
          </node>
        </node>
        <node concept="1E0d5M" id="ofcHXgdHp5" role="1E1XAP">
          <ref role="1E0d5P" node="ofcHXgdH7h" resolve="no.uio.mLab.entities.laboratoryTest.translations" />
        </node>
        <node concept="1SiIV0" id="ofcHXgdHvU" role="3bR37C">
          <node concept="3bR9La" id="ofcHXgdHvV" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="2$xY$aEDy88" resolve="no.uio.mLab.entities.laboratoryTest.runtime" />
          </node>
        </node>
        <node concept="1SiIV0" id="4QUW3ee1cZP" role="3bR37C">
          <node concept="1Busua" id="4QUW3ee1cZQ" role="1SiIV1">
            <ref role="1Busuk" node="4QUW3ee1aEc" resolve="no.uio.mLab.entities.core" />
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="6khVixymbJD" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.entities.microbiology.runtime" />
        <property role="3LESm3" value="7db9e422-98cf-49d8-81dd-7afe7853ce0a" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="6khVixymbJG" role="3LF7KH">
          <node concept="2Ry0Ak" id="6khVixymc3Q" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="6khVixymcej" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.entities.microbiology" />
              <node concept="2Ry0Ak" id="6khVixymcoK" role="2Ry0An">
                <property role="2Ry0Am" value="runtime" />
                <node concept="2Ry0Ak" id="6khVixymczd" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.entities.microbiology.runtime.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="6khVixymcRt" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.entities.microbiology.translations" />
        <property role="3LESm3" value="43e86849-5dfb-4721-a7dc-d2ff6b992000" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="6khVixymcRw" role="3LF7KH">
          <node concept="2Ry0Ak" id="6khVixymdbK" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="6khVixymdmd" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.entities.microbiology" />
              <node concept="2Ry0Ak" id="6khVixymdwE" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.entities.microbiology.translations" />
                <node concept="2Ry0Ak" id="6khVixymd_V" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.entities.microbiology.translations.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="6khVixymeFK" role="3bR37C">
          <node concept="3bR9La" id="6khVixymeFL" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="ofcHXfYTWj" resolve="no.uio.mLab.shared.translations" />
          </node>
        </node>
      </node>
      <node concept="1E1JtD" id="6khVixymdUh" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.entities.microbiology" />
        <property role="3LESm3" value="7de0968d-bd8f-4776-b76d-8d0cb1ebf77b" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="6khVixymdUk" role="3LF7KH">
          <node concept="2Ry0Ak" id="6khVixymeeE" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="6khVixymep7" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.entities.microbiology" />
              <node concept="2Ry0Ak" id="6khVixymez$" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.entities.microbiology.mpl" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="6khVixymeFR" role="3bR37C">
          <node concept="1Busua" id="6khVixymeFS" role="1SiIV1">
            <ref role="1Busuk" node="2$xY$aEDy8x" resolve="no.uio.mLab.entities.laboratoryTest" />
          </node>
        </node>
        <node concept="1SiIV0" id="6khVixymeFT" role="3bR37C">
          <node concept="1Busua" id="6khVixymeFU" role="1SiIV1">
            <ref role="1Busuk" node="2$xY$aEDy9q" resolve="no.uio.mLab.shared" />
          </node>
        </node>
        <node concept="1SiIV0" id="6khVixymeFV" role="3bR37C">
          <node concept="1Busua" id="6khVixymeFW" role="1SiIV1">
            <ref role="1Busuk" node="4QUW3ee1aEc" resolve="no.uio.mLab.entities.core" />
          </node>
        </node>
        <node concept="1yeLz9" id="6khVixymeFX" role="1TViLv">
          <property role="TrG5h" value="no.uio.mLab.entities.microbiology#01" />
          <property role="3LESm3" value="2101947a-7c8e-4f4b-bf8d-e5758578afe0" />
          <property role="2GAjPV" value="false" />
        </node>
        <node concept="1SiIV0" id="6khVixymeR5" role="3bR37C">
          <node concept="3bR9La" id="6khVixymeR6" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="6khVixymcRt" resolve="no.uio.mLab.entities.microbiology.translations" />
          </node>
        </node>
        <node concept="1E0d5M" id="6khVixymeR7" role="1E1XAP">
          <ref role="1E0d5P" node="6khVixymbJD" resolve="no.uio.mLab.entities.microbiology.runtime" />
        </node>
      </node>
      <node concept="1E1JtA" id="2$xY$aEDy7i" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.entities.specialistType.runtime" />
        <property role="3LESm3" value="c0705ff3-e137-472b-8564-4751ea7810eb" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="2$xY$aEDy7c" role="3LF7KH">
          <node concept="2Ry0Ak" id="2$xY$aEDy7d" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="2$xY$aEDy7e" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.entities.specialistType" />
              <node concept="2Ry0Ak" id="2$xY$aEDy7f" role="2Ry0An">
                <property role="2Ry0Am" value="runtime" />
                <node concept="2Ry0Ak" id="5_LESNVZO7W" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.entities.specialistType.runtime.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="2TpFF5_24oU" role="3bR37C">
          <node concept="3bR9La" id="2TpFF5_24oV" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="ffeo:mXGwHwhVPj" resolve="JDK" />
          </node>
        </node>
        <node concept="1SiIV0" id="2TpFF5_24oW" role="3bR37C">
          <node concept="3bR9La" id="2TpFF5_24oX" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="ofcHXfYTWj" resolve="no.uio.mLab.shared.translations" />
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="2TpFF5_245u" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.entities.specialistType.translations" />
        <property role="3LESm3" value="9f9feab9-149d-4473-b88c-16a5fe9f14e3" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="2TpFF5_245v" role="3LF7KH">
          <node concept="2Ry0Ak" id="2TpFF5_245w" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="2TpFF5_245x" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.entities.specialistType" />
              <node concept="2Ry0Ak" id="2TpFF5_245y" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.entities.specialistType.translations" />
                <node concept="2Ry0Ak" id="5_LESNVZOjY" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.entities.specialistType.translations.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="1E1JtD" id="2$xY$aEDy9O" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.entities.specialistType" />
        <property role="3LESm3" value="0c42db4c-2bfa-4af0-b368-f89dcb171d67" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="2$xY$aEDy9J" role="3LF7KH">
          <node concept="2Ry0Ak" id="2$xY$aEDy9K" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="2$xY$aEDy9L" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.entities.specialistType" />
              <node concept="2Ry0Ak" id="5_LESNVZOs0" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.entities.specialistType.mpl" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1E0d5M" id="2$xY$aEDylu" role="1E1XAP">
          <ref role="1E0d5P" node="2$xY$aEDy7i" resolve="no.uio.mLab.entities.specialistType.runtime" />
        </node>
        <node concept="1SiIV0" id="4QUW3ee1d00" role="3bR37C">
          <node concept="1Busua" id="4QUW3ee1d01" role="1SiIV1">
            <ref role="1Busuk" node="4QUW3ee1aEc" resolve="no.uio.mLab.entities.core" />
          </node>
        </node>
        <node concept="1SiIV0" id="2TpFF5_24pa" role="3bR37C">
          <node concept="3bR9La" id="2TpFF5_24pb" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="2$xY$aEDy9q" resolve="no.uio.mLab.shared" />
          </node>
        </node>
        <node concept="1E0d5M" id="2TpFF5_24pc" role="1E1XAP">
          <ref role="1E0d5P" node="2TpFF5_245u" resolve="no.uio.mLab.entities.specialistType.translations" />
        </node>
        <node concept="1SiIV0" id="2TpFF5_24wD" role="3bR37C">
          <node concept="3bR9La" id="2TpFF5_24wE" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="2$xY$aEDy7i" resolve="no.uio.mLab.entities.specialistType.runtime" />
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="2$xY$aEDy7H" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.tasks.autocommentation.runtime" />
        <property role="3LESm3" value="45de78f3-770c-4ff1-b590-596bbb3fdbee" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="2$xY$aEDy7B" role="3LF7KH">
          <node concept="2Ry0Ak" id="2$xY$aEDy7C" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="2$xY$aEDy7D" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.tasks.autocommentation" />
              <node concept="2Ry0Ak" id="2$xY$aEDy7E" role="2Ry0An">
                <property role="2Ry0Am" value="runtime" />
                <node concept="2Ry0Ak" id="6zy3YvEtGcl" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.decisions.tasks.autocommentation.runtime.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="ofcHXfO2$X" role="3bR37C">
          <node concept="3bR9La" id="ofcHXfO2$Y" role="1SiIV1">
            <property role="3bR36h" value="true" />
            <ref role="3bR37D" node="2$xY$aEDy7O" resolve="no.uio.mLab.decisions.core.runtime" />
          </node>
        </node>
        <node concept="1SiIV0" id="7lYCqhvdajW" role="3bR37C">
          <node concept="3bR9La" id="7lYCqhvdajX" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="ffeo:mXGwHwhVPj" resolve="JDK" />
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="ofcHXfOoXC" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.tasks.autocommentation.translations" />
        <property role="3LESm3" value="2a79039a-eeff-4551-8092-0b3eee727155" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="ofcHXfOoXD" role="3LF7KH">
          <node concept="2Ry0Ak" id="ofcHXfOoXE" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="ofcHXfOoXF" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.tasks.autocommentation" />
              <node concept="2Ry0Ak" id="ofcHXfOoXG" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.decisions.tasks.autocommentation.translations" />
                <node concept="2Ry0Ak" id="6zy3YvEtGgL" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.decisions.tasks.autocommentation.translations.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="ofcHXfYUjw" role="3bR37C">
          <node concept="3bR9La" id="ofcHXfYUjx" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="ofcHXfYTWj" resolve="no.uio.mLab.shared.translations" />
          </node>
        </node>
      </node>
      <node concept="1E1JtD" id="2$xY$aEDy9e" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.tasks.autocommentation" />
        <property role="3LESm3" value="ebe710b8-8f46-4b5e-8fb1-221de61f9e7c" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="2$xY$aEDy99" role="3LF7KH">
          <node concept="2Ry0Ak" id="2$xY$aEDy9a" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="2$xY$aEDy9b" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.tasks.autocommentation" />
              <node concept="2Ry0Ak" id="6zy3YvEtGjJ" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.decisions.tasks.autocommentation.mpl" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1yeLz9" id="2$xY$aEDycx" role="1TViLv">
          <property role="TrG5h" value="no.uio.mLab.decisions.tasks.autocommentation#01" />
          <property role="3LESm3" value="53b62dcb-97cb-4e4f-ba3c-7a93d49373b6" />
          <property role="2GAjPV" value="false" />
        </node>
        <node concept="1SiIV0" id="2$xY$aEDykl" role="3bR37C">
          <node concept="3bR9La" id="2$xY$aEDykm" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="2$xY$aEDy9q" resolve="no.uio.mLab.shared" />
          </node>
        </node>
        <node concept="1E0d5M" id="2$xY$aEDykp" role="1E1XAP">
          <ref role="1E0d5P" node="2$xY$aEDy7H" resolve="no.uio.mLab.decisions.tasks.autocommentation.runtime" />
        </node>
        <node concept="1SiIV0" id="2$xY$aEDykq" role="3bR37C">
          <node concept="1Busua" id="2$xY$aEDykr" role="1SiIV1">
            <ref role="1Busuk" node="2$xY$aEDy7U" resolve="no.uio.mLab.decisions.core" />
          </node>
        </node>
        <node concept="1SiIV0" id="ofcHXfOpdR" role="3bR37C">
          <node concept="3bR9La" id="ofcHXfOpdS" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="ofcHXfOoXC" resolve="no.uio.mLab.decisions.tasks.autocommentation.translations" />
          </node>
        </node>
        <node concept="1E0d5M" id="ofcHXfOpdT" role="1E1XAP">
          <ref role="1E0d5P" node="ofcHXfOoXC" resolve="no.uio.mLab.decisions.tasks.autocommentation.translations" />
        </node>
        <node concept="1SiIV0" id="ofcHXfOpjO" role="3bR37C">
          <node concept="3bR9La" id="ofcHXfOpjP" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="2$xY$aEDy7H" resolve="no.uio.mLab.decisions.tasks.autocommentation.runtime" />
          </node>
        </node>
        <node concept="1SiIV0" id="4QUW3efAe0v" role="3bR37C">
          <node concept="3bR9La" id="4QUW3efAe0w" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="4QUW3ee17cn" resolve="no.uio.mLab.decisions.references.specialistType" />
          </node>
        </node>
        <node concept="1SiIV0" id="5uQLXCtupBU" role="3bR37C">
          <node concept="3bR9La" id="5uQLXCtupBV" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="2$xY$aEDy9e" resolve="no.uio.mLab.decisions.tasks.autocommentation" />
          </node>
        </node>
        <node concept="1SiIV0" id="5uQLXCtupBW" role="3bR37C">
          <node concept="3bR9La" id="5uQLXCtupBX" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="ffeo:mXGwHwhVPj" resolve="JDK" />
          </node>
        </node>
        <node concept="1SiIV0" id="5uQLXCtupBY" role="3bR37C">
          <node concept="3bR9La" id="5uQLXCtupBZ" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="ofcHXfO28W" resolve="no.uio.mLab.decisions.core.utils" />
          </node>
        </node>
        <node concept="1SiIV0" id="2FjKBCQxiOq" role="3bR37C">
          <node concept="3bR9La" id="2FjKBCQxiOr" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="4QUW3ee1aEc" resolve="no.uio.mLab.entities.core" />
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="2$xY$aEDy81" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.tasks.autovalidation.runtime" />
        <property role="3LESm3" value="0f0a8fb0-9593-41c4-900c-15c594642394" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="2$xY$aEDy7V" role="3LF7KH">
          <node concept="2Ry0Ak" id="2$xY$aEDy7W" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="2$xY$aEDy7X" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.tasks.autovalidation" />
              <node concept="2Ry0Ak" id="2$xY$aEDy7Y" role="2Ry0An">
                <property role="2Ry0Am" value="runtime" />
                <node concept="2Ry0Ak" id="6zy3YvEtGmH" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.decisions.tasks.autovalidation.runtime.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="ofcHXfO2_u" role="3bR37C">
          <node concept="3bR9La" id="ofcHXfO2_v" role="1SiIV1">
            <property role="3bR36h" value="true" />
            <ref role="3bR37D" node="2$xY$aEDy7O" resolve="no.uio.mLab.decisions.core.runtime" />
          </node>
        </node>
        <node concept="1SiIV0" id="ofcHXgdHws" role="3bR37C">
          <node concept="3bR9La" id="ofcHXgdHwt" role="1SiIV1">
            <property role="3bR36h" value="true" />
            <ref role="3bR37D" node="2$xY$aEDy88" resolve="no.uio.mLab.entities.laboratoryTest.runtime" />
          </node>
        </node>
        <node concept="1SiIV0" id="7lYCqhuYHZJ" role="3bR37C">
          <node concept="3bR9La" id="7lYCqhuYHZK" role="1SiIV1">
            <property role="3bR36h" value="true" />
            <ref role="3bR37D" node="4QUW3edwL$o" resolve="no.uio.mLab.decisions.references.laboratoryTest.runtime" />
          </node>
        </node>
        <node concept="1SiIV0" id="7lYCqhvdakh" role="3bR37C">
          <node concept="3bR9La" id="7lYCqhvdaki" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="ffeo:mXGwHwhVPj" resolve="JDK" />
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="ofcHXfO1dE" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.tasks.autovalidation.translations" />
        <property role="3LESm3" value="4a94e82f-e97a-4b14-9e86-0281f17b1985" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="ofcHXfO1dF" role="3LF7KH">
          <node concept="2Ry0Ak" id="ofcHXfO1dG" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="ofcHXfO1dH" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.tasks.autovalidation" />
              <node concept="2Ry0Ak" id="ofcHXfO1dI" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.decisions.tasks.autovalidation.translations" />
                <node concept="2Ry0Ak" id="6zy3YvEtGr9" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.decisions.tasks.autovalidation.translations.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="ofcHXfYUjP" role="3bR37C">
          <node concept="3bR9La" id="ofcHXfYUjQ" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="ofcHXfYTWj" resolve="no.uio.mLab.shared.translations" />
          </node>
        </node>
      </node>
      <node concept="1E1JtD" id="2$xY$aEDy8k" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.tasks.autovalidation" />
        <property role="3LESm3" value="bdfbcabc-8ac0-41dc-aae3-092d151214a5" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="2$xY$aEDy8f" role="3LF7KH">
          <node concept="2Ry0Ak" id="2$xY$aEDy8g" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="2$xY$aEDy8h" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.tasks.autovalidation" />
              <node concept="2Ry0Ak" id="6zy3YvEtGu7" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.decisions.tasks.autovalidation.mpl" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1yeLz9" id="2$xY$aEDybt" role="1TViLv">
          <property role="TrG5h" value="no.uio.mLab.decisions.tasks.autovalidation#01" />
          <property role="3LESm3" value="680075d9-208a-4da5-812c-b6e8024b9b81" />
          <property role="2GAjPV" value="false" />
        </node>
        <node concept="1SiIV0" id="2$xY$aEDyi$" role="3bR37C">
          <node concept="3bR9La" id="2$xY$aEDyi_" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="2$xY$aEDy9q" resolve="no.uio.mLab.shared" />
          </node>
        </node>
        <node concept="1E0d5M" id="2$xY$aEDyiC" role="1E1XAP">
          <ref role="1E0d5P" node="2$xY$aEDy81" resolve="no.uio.mLab.decisions.tasks.autovalidation.runtime" />
        </node>
        <node concept="1SiIV0" id="2$xY$aEDyiD" role="3bR37C">
          <node concept="1Busua" id="2$xY$aEDyiE" role="1SiIV1">
            <ref role="1Busuk" node="2$xY$aEDy7U" resolve="no.uio.mLab.decisions.core" />
          </node>
        </node>
        <node concept="1SiIV0" id="4QUW3efAe0O" role="3bR37C">
          <node concept="3bR9La" id="4QUW3efAe0P" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="4QUW3edwKrx" resolve="no.uio.mLab.decisions.references.laboratoryTest" />
          </node>
        </node>
        <node concept="1E0d5M" id="5_LESNW3lv$" role="1E1XAP">
          <ref role="1E0d5P" node="5_LESNW3kEc" resolve="no.uio.mLab.decisions.tasks.susceptibilityDetermination.runtime" />
        </node>
        <node concept="1SiIV0" id="5_LESNW3lDZ" role="3bR37C">
          <node concept="3bR9La" id="5_LESNW3lE0" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="2$xY$aEDy81" resolve="no.uio.mLab.decisions.tasks.autovalidation.runtime" />
          </node>
        </node>
        <node concept="1SiIV0" id="5_LESNW3lE1" role="3bR37C">
          <node concept="3bR9La" id="5_LESNW3lE2" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="ofcHXfO1dE" resolve="no.uio.mLab.decisions.tasks.autovalidation.translations" />
          </node>
        </node>
        <node concept="1SiIV0" id="2FjKBCQxiOJ" role="3bR37C">
          <node concept="3bR9La" id="2FjKBCQxiOK" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="ffeo:mXGwHwhVPj" resolve="JDK" />
          </node>
        </node>
        <node concept="1SiIV0" id="2FjKBCQxiOL" role="3bR37C">
          <node concept="3bR9La" id="2FjKBCQxiOM" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="ofcHXfO28W" resolve="no.uio.mLab.decisions.core.utils" />
          </node>
        </node>
        <node concept="1SiIV0" id="2FjKBCQxiON" role="3bR37C">
          <node concept="3bR9La" id="2FjKBCQxiOO" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="2$xY$aEDy8k" resolve="no.uio.mLab.decisions.tasks.autovalidation" />
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="5_LESNW4dJJ" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.tasks.secondaryWork.runtime" />
        <property role="3LESm3" value="193635de-6d27-490c-a39f-3acf40a28a97" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="5_LESNW4dJK" role="3LF7KH">
          <node concept="2Ry0Ak" id="5_LESNW4dJL" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="5_LESNW4dJM" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.tasks.secondaryWork" />
              <node concept="2Ry0Ak" id="5_LESNW4dJN" role="2Ry0An">
                <property role="2Ry0Am" value="runtime" />
                <node concept="2Ry0Ak" id="6PcFLt2kTpQ" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.decisions.tasks.secondaryWork.runtime.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="6khVixy$AIF" role="3bR37C">
          <node concept="3bR9La" id="6khVixy$AIG" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="ffeo:mXGwHwhVPj" resolve="JDK" />
          </node>
        </node>
        <node concept="1SiIV0" id="6khVixy$AIH" role="3bR37C">
          <node concept="3bR9La" id="6khVixy$AII" role="1SiIV1">
            <property role="3bR36h" value="true" />
            <ref role="3bR37D" node="2$xY$aEDy7O" resolve="no.uio.mLab.decisions.core.runtime" />
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="5_LESNW4dJV" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.tasks.secondaryWork.translations" />
        <property role="3LESm3" value="24eb0122-c0de-4514-963b-f8ce9f8443cb" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="5_LESNW4dJW" role="3LF7KH">
          <node concept="2Ry0Ak" id="5_LESNW4dJX" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="5_LESNW4dJY" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.tasks.secondaryWork" />
              <node concept="2Ry0Ak" id="5_LESNW4dJZ" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.decisions.tasks.secondaryWork.translations" />
                <node concept="2Ry0Ak" id="6PcFLt2kTDs" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.decisions.tasks.secondaryWork.translations.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="6khVixym0KX" role="3bR37C">
          <node concept="3bR9La" id="6khVixym0KY" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="ffeo:mXGwHwhVPj" resolve="JDK" />
          </node>
        </node>
        <node concept="1SiIV0" id="6khVixym0KZ" role="3bR37C">
          <node concept="3bR9La" id="6khVixym0L0" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="ofcHXfYTWj" resolve="no.uio.mLab.shared.translations" />
          </node>
        </node>
      </node>
      <node concept="1E1JtD" id="5_LESNW4dK5" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.tasks.secondaryWork" />
        <property role="3LESm3" value="1b13d5a1-80e4-47ff-b773-4b4b3c9489d3" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="5_LESNW4dK6" role="3LF7KH">
          <node concept="2Ry0Ak" id="5_LESNW4dK7" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="5_LESNW4dK8" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.tasks.secondaryWork" />
              <node concept="2Ry0Ak" id="6PcFLt2kTNQ" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.decisions.tasks.secondaryWork.mpl" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1yeLz9" id="5_LESNW4dKa" role="1TViLv">
          <property role="TrG5h" value="no.uio.mLab.decisions.tasks.secondaryWork#01" />
          <property role="3LESm3" value="1017a3bb-5095-419d-99cd-424aaa01b956" />
          <property role="2GAjPV" value="false" />
        </node>
        <node concept="1E0d5M" id="5_LESNW4dKf" role="1E1XAP">
          <ref role="1E0d5P" node="5_LESNW4dJJ" resolve="no.uio.mLab.decisions.tasks.secondaryWork.runtime" />
        </node>
        <node concept="1SiIV0" id="6khVixym0L8" role="3bR37C">
          <node concept="3bR9La" id="6khVixym0L9" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="5_LESNW4dJV" resolve="no.uio.mLab.decisions.tasks.secondaryWork.translations" />
          </node>
        </node>
        <node concept="1SiIV0" id="6khVixym0La" role="3bR37C">
          <node concept="3bR9La" id="6khVixym0Lb" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="2$xY$aEDy9q" resolve="no.uio.mLab.shared" />
          </node>
        </node>
        <node concept="1SiIV0" id="6khVixym0Lc" role="3bR37C">
          <node concept="1Busua" id="6khVixym0Ld" role="1SiIV1">
            <ref role="1Busuk" node="2$xY$aEDy7U" resolve="no.uio.mLab.decisions.core" />
          </node>
        </node>
        <node concept="1SiIV0" id="6PcFLt2kTXe" role="3bR37C">
          <node concept="3bR9La" id="6PcFLt2kTXf" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="5_LESNW0NMc" resolve="no.uio.mLab.decisions.references.microbiology" />
          </node>
        </node>
        <node concept="1SiIV0" id="2FjKBCQxiP8" role="3bR37C">
          <node concept="3bR9La" id="2FjKBCQxiP9" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="ffeo:mXGwHwhVPj" resolve="JDK" />
          </node>
        </node>
        <node concept="1SiIV0" id="2FjKBCQxiPa" role="3bR37C">
          <node concept="3bR9La" id="2FjKBCQxiPb" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="ofcHXfO28W" resolve="no.uio.mLab.decisions.core.utils" />
          </node>
        </node>
        <node concept="1SiIV0" id="2FjKBCQxiPc" role="3bR37C">
          <node concept="3bR9La" id="2FjKBCQxiPd" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="5_LESNW4dK5" resolve="no.uio.mLab.decisions.tasks.secondaryWork" />
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="5_LESNW3kEc" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.tasks.susceptibilityDetermination.runtime" />
        <property role="3LESm3" value="e24f1ca6-1598-45b1-93b8-a442b4bf8837" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="5_LESNW3kEd" role="3LF7KH">
          <node concept="2Ry0Ak" id="5_LESNW3kEe" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="5_LESNW3kEf" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.tasks.susceptibilityDetermination" />
              <node concept="2Ry0Ak" id="5_LESNW3kEg" role="2Ry0An">
                <property role="2Ry0Am" value="runtime" />
                <node concept="2Ry0Ak" id="6PcFLt2kUEF" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.decisions.tasks.susceptibilityDetermination.runtime.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="5_LESNW3kEq" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.tasks.susceptibilityDetermination.translations" />
        <property role="3LESm3" value="66809dd1-3b50-414c-84ec-f54ba6aba81f" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="5_LESNW3kEr" role="3LF7KH">
          <node concept="2Ry0Ak" id="5_LESNW3kEs" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="5_LESNW3kEt" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.tasks.susceptibilityDetermination" />
              <node concept="2Ry0Ak" id="5_LESNW3kEu" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.decisions.tasks.susceptibilityDetermination.translations" />
                <node concept="2Ry0Ak" id="6PcFLt2kUvF" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.decisions.tasks.susceptibilityDetermination.translations.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="1E1JtD" id="5_LESNW3kEy" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.tasks.susceptibilityDetermination" />
        <property role="3LESm3" value="cfc7bfcd-9c12-49e9-80de-d886cbb9491f" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="5_LESNW3kEz" role="3LF7KH">
          <node concept="2Ry0Ak" id="5_LESNW3kE$" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="5_LESNW3kE_" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.tasks.susceptibilityDetermination" />
              <node concept="2Ry0Ak" id="6PcFLt2kUfc" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.decisions.tasks.susceptibilityDetermination.mpl" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1yeLz9" id="5_LESNW3kEB" role="1TViLv">
          <property role="TrG5h" value="no.uio.mLab.decisions.tasks.susceptibilityDetermination#01" />
          <property role="3LESm3" value="65388e70-e70c-4cce-b9dd-d50896f67430" />
          <property role="2GAjPV" value="false" />
        </node>
        <node concept="1E0d5M" id="5_LESNW3kEE" role="1E1XAP">
          <ref role="1E0d5P" node="5_LESNW3kEc" resolve="no.uio.mLab.decisions.tasks.susceptibilityDetermination.runtime" />
        </node>
      </node>
      <node concept="1E1JtA" id="4QUW3edwLYa" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.references.biochemistry.runtime" />
        <property role="3LESm3" value="912d06ac-ba61-4c9d-83de-64565f12e479" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="4QUW3edwLYb" role="3LF7KH">
          <node concept="2Ry0Ak" id="4QUW3edwLYc" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="4QUW3edwLYd" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.references.biochemistry" />
              <node concept="2Ry0Ak" id="4QUW3edwLYe" role="2Ry0An">
                <property role="2Ry0Am" value="runtime" />
                <node concept="2Ry0Ak" id="6zy3YvEtGx5" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.decisions.references.biochemistry.runtime.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="4QUW3edwLYg" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.references.biochemistry.translations" />
        <property role="3LESm3" value="32326140-de40-4a18-bd07-16491f087f22" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="4QUW3edwLYh" role="3LF7KH">
          <node concept="2Ry0Ak" id="4QUW3edwLYi" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="4QUW3edwLYj" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.references.biochemistry" />
              <node concept="2Ry0Ak" id="4QUW3edwLYk" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.decisions.references.biochemistry.translations" />
                <node concept="2Ry0Ak" id="6zy3YvEtG_x" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.decisions.references.biochemistry.translations.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="4QUW3edwLYm" role="3bR37C">
          <node concept="3bR9La" id="4QUW3edwLYn" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="ffeo:mXGwHwhVPj" resolve="JDK" />
          </node>
        </node>
        <node concept="1SiIV0" id="4QUW3edwLYo" role="3bR37C">
          <node concept="3bR9La" id="4QUW3edwLYp" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="ofcHXfYTWj" resolve="no.uio.mLab.shared.translations" />
          </node>
        </node>
      </node>
      <node concept="1E1JtD" id="4QUW3edwLYq" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.references.biochemistry" />
        <property role="3LESm3" value="76146113-297e-4cbe-99cf-714ae175bb83" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="4QUW3edwLYr" role="3LF7KH">
          <node concept="2Ry0Ak" id="4QUW3edwLYs" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="4QUW3edwLYt" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.references.biochemistry" />
              <node concept="2Ry0Ak" id="6zy3YvEtGCv" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.decisions.references.biochemistry.mpl" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1yeLz9" id="4QUW3edwLYx" role="1TViLv">
          <property role="TrG5h" value="no.uio.mLab.decisions.references.biochemistry#01" />
          <property role="3LESm3" value="4a931145-1da0-4ad2-aa4a-46cc36fd9df4" />
          <property role="2GAjPV" value="false" />
        </node>
        <node concept="1E0d5M" id="4QUW3edwLY$" role="1E1XAP">
          <ref role="1E0d5P" node="4QUW3edwLYa" resolve="no.uio.mLab.decisions.references.biochemistry.runtime" />
        </node>
        <node concept="1SiIV0" id="4QUW3edwMgv" role="3bR37C">
          <node concept="3bR9La" id="4QUW3edwMgw" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="2$xY$aEDy7b" resolve="no.uio.mLab.entities.biochemistry" />
          </node>
        </node>
        <node concept="1SiIV0" id="4QUW3edwMgz" role="3bR37C">
          <node concept="1Busua" id="4QUW3edwMg$" role="1SiIV1">
            <ref role="1Busuk" node="4QUW3edwKrx" resolve="no.uio.mLab.decisions.references.laboratoryTest" />
          </node>
        </node>
        <node concept="1SiIV0" id="4QUW3edwMH9" role="3bR37C">
          <node concept="3bR9La" id="4QUW3edwMHa" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="4QUW3edwLYg" resolve="no.uio.mLab.decisions.references.biochemistry.translations" />
          </node>
        </node>
        <node concept="1SiIV0" id="4QUW3ee18tb" role="3bR37C">
          <node concept="1Busua" id="4QUW3ee18tc" role="1SiIV1">
            <ref role="1Busuk" node="4QUW3ee17cn" resolve="no.uio.mLab.decisions.references.specialistType" />
          </node>
        </node>
        <node concept="1SiIV0" id="4QUW3efZnWw" role="3bR37C">
          <node concept="3bR9La" id="4QUW3efZnWx" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="2$xY$aEDy7U" resolve="no.uio.mLab.decisions.core" />
          </node>
        </node>
        <node concept="1SiIV0" id="2FjKBCQxiPO" role="3bR37C">
          <node concept="3bR9La" id="2FjKBCQxiPP" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="4QUW3ee1aEc" resolve="no.uio.mLab.entities.core" />
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="4QUW3edwL$o" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.references.laboratoryTest.runtime" />
        <property role="3LESm3" value="cd9f1852-e6bc-481e-9ffd-bfcf93751d73" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="4QUW3edwL$p" role="3LF7KH">
          <node concept="2Ry0Ak" id="4QUW3edwL$q" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="4QUW3edwL$r" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.references.laboratoryTest" />
              <node concept="2Ry0Ak" id="4QUW3edwL$s" role="2Ry0An">
                <property role="2Ry0Am" value="runtime" />
                <node concept="2Ry0Ak" id="6zy3YvEtGFt" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.decisions.references.laboratoryTest.runtime.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="4QUW3edwKXS" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.references.laboratoryTest.translations" />
        <property role="3LESm3" value="b198ca2e-3e90-403f-bb48-87aad48e251d" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="4QUW3edwKXV" role="3LF7KH">
          <node concept="2Ry0Ak" id="4QUW3edwL8D" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="4QUW3edwLea" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.references.laboratoryTest" />
              <node concept="2Ry0Ak" id="4QUW3edwLjF" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.decisions.references.laboratoryTest.translations" />
                <node concept="2Ry0Ak" id="6zy3YvEtGJT" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.decisions.references.laboratoryTest.translations.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="4QUW3edwLxf" role="3bR37C">
          <node concept="3bR9La" id="4QUW3edwLxg" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" to="ffeo:mXGwHwhVPj" resolve="JDK" />
          </node>
        </node>
        <node concept="1SiIV0" id="4QUW3edwLxh" role="3bR37C">
          <node concept="3bR9La" id="4QUW3edwLxi" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="ofcHXfYTWj" resolve="no.uio.mLab.shared.translations" />
          </node>
        </node>
      </node>
      <node concept="1E1JtD" id="4QUW3edwKrx" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.references.laboratoryTest" />
        <property role="3LESm3" value="4a652d55-3684-4d2d-98c9-2ef46f124c44" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="4QUW3edwKr$" role="3LF7KH">
          <node concept="2Ry0Ak" id="4QUW3edwK_S" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="4QUW3edwKCy" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.references.laboratoryTest" />
              <node concept="2Ry0Ak" id="6zy3YvEtGMR" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.decisions.references.laboratoryTest.mpl" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="4QUW3edwKMX" role="3bR37C">
          <node concept="3bR9La" id="4QUW3edwKMY" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="2$xY$aEDy8x" resolve="no.uio.mLab.entities.laboratoryTest" />
          </node>
        </node>
        <node concept="1yeLz9" id="4QUW3edwKMZ" role="1TViLv">
          <property role="TrG5h" value="no.uio.mLab.decisions.references.laboratoryTest#01" />
          <property role="3LESm3" value="d341ef82-6312-4e96-b325-35a1d771e469" />
          <property role="2GAjPV" value="false" />
        </node>
        <node concept="1SiIV0" id="4QUW3edwLPh" role="3bR37C">
          <node concept="3bR9La" id="4QUW3edwLPi" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="4QUW3edwKXS" resolve="no.uio.mLab.decisions.references.laboratoryTest.translations" />
          </node>
        </node>
        <node concept="1E0d5M" id="4QUW3edwLV7" role="1E1XAP">
          <ref role="1E0d5P" node="4QUW3edwL$o" resolve="no.uio.mLab.decisions.references.laboratoryTest.runtime" />
        </node>
        <node concept="1SiIV0" id="4QUW3ee16eU" role="3bR37C">
          <node concept="1Busua" id="4QUW3ee16eV" role="1SiIV1">
            <ref role="1Busuk" node="2$xY$aEDy7U" resolve="no.uio.mLab.decisions.core" />
          </node>
        </node>
        <node concept="1SiIV0" id="4B5aqq2V8KO" role="3bR37C">
          <node concept="3bR9La" id="4B5aqq2V8KP" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="7lYCqhu3axh" resolve="no.uio.mLab.decisions.data.laboratoryTest.numberResult" />
          </node>
        </node>
        <node concept="1SiIV0" id="4B5aqq2V8KQ" role="3bR37C">
          <node concept="3bR9La" id="4B5aqq2V8KR" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="7lYCqhu3b$u" resolve="no.uio.mLab.decisions.data.laboratoryTest.textualResult" />
          </node>
        </node>
        <node concept="1SiIV0" id="4B5aqq2V8KS" role="3bR37C">
          <node concept="3bR9La" id="4B5aqq2V8KT" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="7lYCqhu37uZ" resolve="no.uio.mLab.decisions.data.laboratoryTest" />
          </node>
        </node>
        <node concept="1SiIV0" id="2FjKBCQxiQ9" role="3bR37C">
          <node concept="3bR9La" id="2FjKBCQxiQa" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="4QUW3ee1aEc" resolve="no.uio.mLab.entities.core" />
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="5_LESNW0LDX" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.references.microbiology.runtime" />
        <property role="3LESm3" value="8dec6cdc-3fcc-4fca-a2be-78326eb2c783" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="5_LESNW0LE0" role="3LF7KH">
          <node concept="2Ry0Ak" id="5_LESNW0LVG" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="5_LESNW0M4N" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.references.microbiology" />
              <node concept="2Ry0Ak" id="5_LESNW0MdU" role="2Ry0An">
                <property role="2Ry0Am" value="runtime" />
                <node concept="2Ry0Ak" id="5_LESNW0Miw" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.decisions.references.microbiology.runtime.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="5_LESNW0MHz" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.references.microbiology.translations" />
        <property role="3LESm3" value="6ffb4f00-a991-465b-b999-465bc3332cb3" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="5_LESNW0MHA" role="3LF7KH">
          <node concept="2Ry0Ak" id="5_LESNW0MZy" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="5_LESNW0N8N" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.references.microbiology" />
              <node concept="2Ry0Ak" id="5_LESNW0Ni4" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.decisions.references.microbiology.translations" />
                <node concept="2Ry0Ak" id="5_LESNW0NmJ" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.decisions.references.microbiology.translations.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="1E1JtD" id="5_LESNW0NMc" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.references.microbiology" />
        <property role="3LESm3" value="64b829cb-114b-4b36-813b-6c9497a3e006" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="5_LESNW0NMf" role="3LF7KH">
          <node concept="2Ry0Ak" id="5_LESNW0O96" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="5_LESNW0Oix" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.references.microbiology" />
              <node concept="2Ry0Ak" id="5_LESNW0OrW" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.decisions.references.microbiology.mpl" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1E0d5M" id="5_LESNW0O_1" role="1E1XAP">
          <ref role="1E0d5P" node="5_LESNW0LDX" resolve="no.uio.mLab.decisions.references.microbiology.runtime" />
        </node>
        <node concept="1yeLz9" id="5_LESNW0O_2" role="1TViLv">
          <property role="TrG5h" value="no.uio.mLab.decisions.references.microbiology#01" />
          <property role="3LESm3" value="c1003ad6-1521-4770-8911-cee338a48b91" />
          <property role="2GAjPV" value="false" />
        </node>
        <node concept="1SiIV0" id="6khVixymagC" role="3bR37C">
          <node concept="3bR9La" id="6khVixymagD" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="2$xY$aEDy9q" resolve="no.uio.mLab.shared" />
          </node>
        </node>
        <node concept="1SiIV0" id="6khVixymagE" role="3bR37C">
          <node concept="1Busua" id="6khVixymagF" role="1SiIV1">
            <ref role="1Busuk" node="4QUW3edwKrx" resolve="no.uio.mLab.decisions.references.laboratoryTest" />
          </node>
        </node>
        <node concept="1SiIV0" id="6khVixymeT8" role="3bR37C">
          <node concept="3bR9La" id="6khVixymeT9" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="6khVixymdUh" resolve="no.uio.mLab.entities.microbiology" />
          </node>
        </node>
        <node concept="1SiIV0" id="5uQLXCtupDv" role="3bR37C">
          <node concept="3bR9La" id="5uQLXCtupDw" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="2$xY$aEDy7U" resolve="no.uio.mLab.decisions.core" />
          </node>
        </node>
        <node concept="1SiIV0" id="6PcFLt2kSPb" role="3bR37C">
          <node concept="3bR9La" id="6PcFLt2kSPc" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="2$xY$aEDy7b" resolve="no.uio.mLab.entities.biochemistry" />
          </node>
        </node>
        <node concept="1SiIV0" id="2FjKBCQxiQu" role="3bR37C">
          <node concept="3bR9La" id="2FjKBCQxiQv" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="4QUW3ee1aEc" resolve="no.uio.mLab.entities.core" />
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="4QUW3ee17Q2" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.references.specialistType.runtime" />
        <property role="3LESm3" value="13b8465a-787e-4c76-858a-173434c12d0e" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="4QUW3ee17Q5" role="3LF7KH">
          <node concept="2Ry0Ak" id="4QUW3ee182X" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="4QUW3ee189E" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.references.specialistType" />
              <node concept="2Ry0Ak" id="4QUW3ee18gn" role="2Ry0An">
                <property role="2Ry0Am" value="runtime" />
                <node concept="2Ry0Ak" id="5_LESNVZOGG" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.decisions.references.specialistType.runtime.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="1E1JtA" id="4QUW3efZo0Q" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.references.specialistType.translations" />
        <property role="3LESm3" value="2f0e57e0-fc71-432c-9a0b-50ec2fe82100" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="4QUW3efZo0R" role="3LF7KH">
          <node concept="2Ry0Ak" id="4QUW3efZo0S" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="4QUW3efZo0T" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.references.specialistType" />
              <node concept="2Ry0Ak" id="4QUW3efZo0U" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.decisions.references.specialistType.translations" />
                <node concept="2Ry0Ak" id="5_LESNVZOTo" role="2Ry0An">
                  <property role="2Ry0Am" value="no.uio.mLab.decisions.references.specialistType.translations.msd" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="4QUW3efZoli" role="3bR37C">
          <node concept="3bR9La" id="4QUW3efZolj" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="ofcHXfYTWj" resolve="no.uio.mLab.shared.translations" />
          </node>
        </node>
      </node>
      <node concept="1E1JtD" id="4QUW3ee17cn" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.decisions.references.specialistType" />
        <property role="3LESm3" value="0e664af0-ccb5-427d-94ac-6f90ac2b6424" />
        <property role="2GAjPV" value="false" />
        <node concept="55IIr" id="4QUW3ee17cq" role="3LF7KH">
          <node concept="2Ry0Ak" id="4QUW3ee17oQ" role="iGT6I">
            <property role="2Ry0Am" value="languages" />
            <node concept="2Ry0Ak" id="4QUW3ee17s6" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.decisions.references.specialistType" />
              <node concept="2Ry0Ak" id="5_LESNVZP1Q" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.decisions.references.specialistType.mpl" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1SiIV0" id="4QUW3ee17CT" role="3bR37C">
          <node concept="3bR9La" id="4QUW3ee17CU" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="2$xY$aEDy9O" resolve="no.uio.mLab.entities.specialistType" />
          </node>
        </node>
        <node concept="1SiIV0" id="4QUW3ee17CV" role="3bR37C">
          <node concept="1Busua" id="4QUW3ee17CW" role="1SiIV1">
            <ref role="1Busuk" node="2$xY$aEDy7U" resolve="no.uio.mLab.decisions.core" />
          </node>
        </node>
        <node concept="1yeLz9" id="4QUW3ee17CX" role="1TViLv">
          <property role="TrG5h" value="no.uio.mLab.decisions.references.specialistType#01" />
          <property role="3LESm3" value="64d670b1-5071-4e14-8e60-d83a30068000" />
          <property role="2GAjPV" value="false" />
        </node>
        <node concept="1E0d5M" id="4QUW3ee1a5X" role="1E1XAP">
          <ref role="1E0d5P" node="4QUW3ee17Q2" resolve="no.uio.mLab.decisions.references.specialistType.runtime" />
        </node>
        <node concept="1E0d5M" id="4QUW3efZolp" role="1E1XAP">
          <ref role="1E0d5P" node="4QUW3efZo0Q" resolve="no.uio.mLab.decisions.references.specialistType.translations" />
        </node>
        <node concept="1SiIV0" id="4QUW3efZos$" role="3bR37C">
          <node concept="3bR9La" id="4QUW3efZos_" role="1SiIV1">
            <property role="3bR36h" value="false" />
            <ref role="3bR37D" node="4QUW3efZo0Q" resolve="no.uio.mLab.decisions.references.specialistType.translations" />
          </node>
        </node>
      </node>
      <node concept="3LEwk6" id="5jVYnMGX1$k" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.devkits.laboratoryTest" />
        <property role="3LESm3" value="12f120c8-c6fd-4509-86cd-4ae6e8681eb7" />
        <node concept="55IIr" id="5jVYnMGX1$n" role="3LF7KH">
          <node concept="2Ry0Ak" id="5jVYnMGX24k" role="iGT6I">
            <property role="2Ry0Am" value="devkits" />
            <node concept="2Ry0Ak" id="5jVYnMGX2fp" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.devkits.laboratoryTest" />
              <node concept="2Ry0Ak" id="5jVYnMGX2qu" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.devkits.laboratoryTest.devkit" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3LEDTy" id="5jVYnMGX2_v" role="3LEDUa">
          <ref role="3LEDTV" node="7lYCqhu37uZ" resolve="no.uio.mLab.decisions.data.laboratoryTest" />
        </node>
        <node concept="3LEDTy" id="5jVYnMGX2_w" role="3LEDUa">
          <ref role="3LEDTV" node="4QUW3edwKrx" resolve="no.uio.mLab.decisions.references.laboratoryTest" />
        </node>
      </node>
      <node concept="3LEwk6" id="5jVYnMGX36d" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.devkits.biochemistry" />
        <property role="3LESm3" value="4cf96a12-8716-4096-97a8-6b6f1ed3bef8" />
        <node concept="55IIr" id="5jVYnMGX36g" role="3LF7KH">
          <node concept="2Ry0Ak" id="5jVYnMGX3An" role="iGT6I">
            <property role="2Ry0Am" value="devkits" />
            <node concept="2Ry0Ak" id="5jVYnMGX3FX" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.devkits.biochemistry" />
              <node concept="2Ry0Ak" id="5jVYnMGX47y" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.devkits.biochemistry.devkit" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3LEz8M" id="5jVYnMGX4t_" role="3LEz9a">
          <ref role="3LEz8N" node="5jVYnMGX1$k" resolve="no.uio.mLab.devkits.laboratoryTest" />
        </node>
        <node concept="3LEDTy" id="5jVYnMGX4tA" role="3LEDUa">
          <ref role="3LEDTV" node="4QUW3edwLYq" resolve="no.uio.mLab.decisions.references.biochemistry" />
        </node>
      </node>
      <node concept="3LEwk6" id="5jVYnMGX4XX" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.devkits.microbiology" />
        <property role="3LESm3" value="31d9cf5b-c624-4494-9d29-ea7395d12dd7" />
        <node concept="55IIr" id="5jVYnMGX4Y0" role="3LF7KH">
          <node concept="2Ry0Ak" id="5jVYnMGX5ul" role="iGT6I">
            <property role="2Ry0Am" value="devkits" />
            <node concept="2Ry0Ak" id="5jVYnMGX5zV" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.devkits.microbiology" />
              <node concept="2Ry0Ak" id="5jVYnMGX5Ov" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.devkits.microbiology.devkit" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3LEz8M" id="5jVYnMGX5Zw" role="3LEz9a">
          <ref role="3LEz8N" node="5jVYnMGX1$k" resolve="no.uio.mLab.devkits.laboratoryTest" />
        </node>
        <node concept="3LEDTy" id="5jVYnMGX5Zx" role="3LEDUa">
          <ref role="3LEDTV" node="2$xY$aEDy8O" resolve="no.uio.mLab.decisions.data.microbiology" />
        </node>
        <node concept="3LEDTy" id="5jVYnMGX5Zy" role="3LEDUa">
          <ref role="3LEDTV" node="5_LESNW0NMc" resolve="no.uio.mLab.decisions.references.microbiology" />
        </node>
      </node>
      <node concept="3LEwk6" id="5jVYnMGX6wb" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.devkits.autocommentation" />
        <property role="3LESm3" value="0150ab26-8e2e-488e-ab46-e8da7ad3fa0c" />
        <node concept="55IIr" id="5jVYnMGX6we" role="3LF7KH">
          <node concept="2Ry0Ak" id="5jVYnMGX70N" role="iGT6I">
            <property role="2Ry0Am" value="devkits" />
            <node concept="2Ry0Ak" id="5jVYnMGX7bS" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.devkits.autocommentation" />
              <node concept="2Ry0Ak" id="5jVYnMGX7mX" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.devkits.autocommentation.devkit" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3LEz8M" id="5jVYnMGX7xY" role="3LEz9a">
          <ref role="3LEz8N" node="5jVYnMGX1$k" resolve="no.uio.mLab.devkits.laboratoryTest" />
        </node>
        <node concept="3LEDTy" id="5jVYnMGX7xZ" role="3LEDUa">
          <ref role="3LEDTV" node="4QUW3ee17cn" resolve="no.uio.mLab.decisions.references.specialistType" />
        </node>
        <node concept="3LEDTy" id="6LTgXmM1YQx" role="3LEDUa">
          <ref role="3LEDTV" node="2$xY$aEDy8I" resolve="no.uio.mLab.decisions.data.patient" />
        </node>
        <node concept="3LEDTy" id="6LTgXmM1YQy" role="3LEDUa">
          <ref role="3LEDTV" node="7lYCqhu3b$u" resolve="no.uio.mLab.decisions.data.laboratoryTest.textualResult" />
        </node>
        <node concept="3LEDTy" id="6LTgXmM1YQz" role="3LEDUa">
          <ref role="3LEDTV" node="7lYCqhu3axh" resolve="no.uio.mLab.decisions.data.laboratoryTest.numberResult" />
        </node>
      </node>
      <node concept="3LEwk6" id="5jVYnMGXN0I" role="2G$12L">
        <property role="BnDLt" value="true" />
        <property role="TrG5h" value="no.uio.mLab.devkits.microbiology.secondaryWork" />
        <property role="3LESm3" value="51cd949b-316f-4eb1-a6af-b5dfaae74a33" />
        <node concept="55IIr" id="5jVYnMGXN0L" role="3LF7KH">
          <node concept="2Ry0Ak" id="5jVYnMGXN7M" role="iGT6I">
            <property role="2Ry0Am" value="devkits" />
            <node concept="2Ry0Ak" id="5jVYnMGXN90" role="2Ry0An">
              <property role="2Ry0Am" value="no.uio.mLab.devkits.microbiology.secondaryWork" />
              <node concept="2Ry0Ak" id="6PcFLt2kSFd" role="2Ry0An">
                <property role="2Ry0Am" value="no.uio.mLab.devkits.microbiology.secondaryWork.devkit" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3LEDTy" id="6PcFLt2kTY_" role="3LEDUa">
          <ref role="3LEDTV" node="5_LESNW4dK5" resolve="no.uio.mLab.decisions.tasks.secondaryWork" />
        </node>
      </node>
    </node>
  </node>
</model>

